<?php include('include/pre_login_header.php'); ?>
<!DOCTYPE html>

<html class="no-js" lang="en-US" dir="ltr">

<head>
    <title>EMMA Login</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" type="text/css" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #C2E0F6;">



<?php include('include/index_top_bar.php'); ?>
<div class="alert_message" style="padding: 10px">
    <?php include('website_alert_message.php');?>
</div>
<div class="row expanded">
    <div class="row" style="height: 80%">
        <object data="resource_folder/emma_faq.pdf" type="application/pdf" width="100%" height="100%">
            <div class="text-center margin-top-1em">
                Unable to display on your browser <a href="resource_folder/emma_faq.pdf" >click to download</a>
            </div>
        </object>
    </div>
</div>
<div class="row expanded">
    <div class="large-12 medium-12 small-12 columns">
        <div class="row expanded">
            <div class="large-6 medium-6 small-12 columns text-center" style="padding-top: 3%">
                <h1>FAQ Coming Soon...</h1>
            </div>
        </div>
    </div>
</div>
<?php include('include/footer.php'); ?>
<?php include('include/pre_login_modals.php'); ?>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>


