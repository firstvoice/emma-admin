<?php include('include/pre_login_header.php'); ?>
<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Login</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
<body style="background-color: #ffffff">


<?php include('include/index_top_bar.php'); ?>

<div class="row expanded the-clock">
    <div class="large-12 medium-12 small-12 columns" style="padding-bottom: 2em;">
        <div class="row expanded" style="padding-bottom: 1em">
            <div class="statementBanner">
                <div class="row column text-center">
                    <div class="textSize" style="text-align: center">
                        Our Mission: Making Minutes Matter
                    </div>
                </div>
            </div>
            <div class="large-6 medium-12 small-12 columns">
                <div class="row expanded">
                    <div class="description_card bottom_gap top_gap">
                    <p class="company_description">Think Safe and our innovative First Voice products and
                        services
                        allow first aid, AED and overall emergency response programs that: improve rescuer skills,
                        provide
                        efficiency, reduce risks and reduce costs
                        versus traditional programs.</p>
                </div>
                    <div class="description_card bottom_gap">
                        <p class="ind_box_title">Improve Skills</p>

                        <p class="company_description">Think Safe's Emergency
                            Instruction Device (EID) and Self-contained
                            Emergency Treatment (SET) Systems are the product innovations of Think
                            Safe.
                            These product innovations are patented and bring
                            technology to the first aid industry. With the simple press of a button, instant
                            detailed
                            American Heart Association/ECC/ILCOR approved instructions on what to do are immediately
                            heard
                            and seen by the
                            responder or lay rescuers providing care. But we did not stop program improvements and
                            skills
                            improvements by just bringing our innovations to market. Additionally, our First Voice
                            products
                            and our First
                            Voice Training <a target="_blank"
                                              href="http://www.firstvoicetraining.com">(www.firstvoicetraining.com)</a>
                            options (online, blended, in-person, in-house) give rescuers and responder teams the
                            capability
                            to refresh on skills anytime and improve access to information and
                            training.</p>
                    </div>

                    <div class="description_card bottom_gap">
                        <p class="ind_box_title" style="">Provide Efficiency</p>
                        <p class="company_description">Think Safe's First Voice products decrease chaos and any
                            unnecessary steps during emergencies by providing any first responders or rescuers on
                            scene with organized and accurate first aid tools, instructions
                            and the personal protective equipment (PPE) necessary for trauma situations during the
                            first few minutes of care. Think Safe software and other services also improve efficiencies
                            in
                            maintenance and upkeep of the total first aid program or emergency response program.</p>
                    </div>

                    <div class="description_card bottom_gap">
                        <p class="ind_box_title" style="">Reduce Risks</p>
                        <p class="company_description">Think Safe's First Voice products and services offer
                            more than
                            standard first aid programs and traditional ANSI unitized first aid kits. First Voice
                            products
                            are designed with easy-to use organized supplies
                            that are ready to protect the responder and treat the victim, according to updated
                            science and
                            OSHA compliance standards. Restocking is simplified through the use of First Voice
                            pre-packaged
                            supply modules and
                            our systems are all designed with easy "ready to run" status viewing designs plus are
                            easy to
                            restock or refill. SET Systems come in ergonomic backpack, EMS jump bag (roll-out), and
                            rugged
                            Pelican lifetime
                            guaranteed dustproof and waterproof hard case configurations to fit your storage and
                            response
                            environmental needs. In addition, all SET Systems can accommodate an Automated External
                            Defibrillator (AED) which
                            improves care and response times during life threatening emergencies. But, Think Safe does
                            not
                            stop
                            at
                            products. Think Safe also provides custom developed software (www.firstvoicemanager.com and
                            our
                            First
                            Voice Apps named EMMA, AED Notify, and HeartSafe) and other services that ensure maintenance
                            and
                            upkeep
                            of the total program and reduce risk of legal liability. </p>
                    </div>

                    <div class="description_card">
                        <p class="ind_box_title" style="">Reduce Costs</p>
                        <p class="company_description">Think Safe has case studies that show the client pays
                            for the
                            First Voice product and service investment in just one year or less. Think Safe products
                            and
                            services are well worth the small up front
                            investment. Better response means decreased worker's compensation insurance, lost time
                            at work,
                            replacement laborer costs and disability costs. Think Safe takes mandated training to a
                            new
                            level by pairing
                            it with our proven technology for any responder team, first aid rescuer or employee use.
                            Yet,
                            the costs of the program are proven to decrease and the products and services are paid
                            for
                            through other program
                            savings. A client recovers the investment made in under one year on average when coupled
                            with a
                            integrated transition to online, in-house or blended training methods. Ask for your
                            case study
                            to see how
                            other companies have saved money every year, even during the initial year of
                            implementation.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div><p>&nbsp;</p><br/><br/></div>

<?php include('include/footer.php'); ?>
</div>
<?php include('include/pre_login_modals.php'); ?>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>


