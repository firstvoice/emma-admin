<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 2/19/2020
 * Time: 1:30 PM
 */

require('include/db.php');


/**
 * To use this make sure only one row is uncommented
 * 1st option is to reset all expired passwords
 * 2nd option is to reset only the passwords of inserted users. See comment on that line how to input.
*/


//1st - Update all expired
//******************************************
$users = $emmadb->query("
    SELECT p.old_password_id, p.user_id, p.date_created
    FROM emma_old_passwords p
    JOIN (
        SELECT MAX(old_password_id) as old_password_id
        FROM emma_old_passwords
        GROUP BY user_id
        ) o ON p.old_password_id = o.old_password_id
    WHERE p.date_created < DATE_SUB(NOW(), INTERVAL 90 DAY)
    ORDER BY p.old_password_id
");
//******************************************

////2nd - Update individuals expired
////******************************************
//$updateusers = ''; // make sure it is in format: '1234,1238,1236' <-no spaces separated by commas
//
//$userarr = array();
//$userarr = explode(',',$updateusers);
//
//$users = $emmadb->query("
//    SELECT p.old_password_id, p.user_id, p.date_created
//    FROM emma_old_passwords p
//    JOIN (
//        SELECT MAX(old_password_id) as old_password_id
//        FROM emma_old_passwords
//        WHERE user_id IN(". implode(',',$userarr) .")
//        GROUP BY user_id
//        ) o ON p.old_password_id = o.old_password_id
//    ORDER BY p.old_password_id
//");
////******************************************

////3rd - Update plan expired
////******************************************
//$updateplans = ''; // make sure it is in format: '1234,1238,1236' <-no spaces separated by commas
//
//$planarr = array();
//$planarr = explode(',',$updateplans);
//
//$users = $emmadb->query("
//    SELECT p.old_password_id, p.user_id, p.date_created
//    FROM emma_old_passwords p
//    JOIN (
//        SELECT MAX(old_password_id) as old_password_id
//        FROM emma_old_passwords
//        WHERE user_id IN(". implode(',',$userarr) .")
//        GROUP BY user_id
//        ) o ON p.old_password_id = o.old_password_id
//    ORDER BY p.old_password_id
//");
////******************************************
//Do not comment out below this line

while($user = $users->fetch_assoc()){
    echo $user['old_password_id'] . ': ' . $user['user_id'] . ' - ' . $user['date_created'] . '<br>';

    $update = $emmadb->query("
        UPDATE emma_old_passwords
        SET date_created = NOW()
        WHERE old_password_id = '". $user['old_password_id'] ."'
    ");

}



