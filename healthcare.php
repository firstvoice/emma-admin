<?php include('include/pre_login_header.php'); ?>
<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Login</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #FFFFFF;">


<?php include('include/index_top_bar.php'); ?>

<div class="row expanded hospital_background">
    <div class="large-12 medium-12 small-12 columns" style="margin: 0 auto;">

    </div>
    <div class="large-12 medium-12 small-12 columns innerclock">
        <div class="row expanded" style="padding-bottom: 5em">
            <div class="large-6 medium-12 small-12 column">
                <img src="img/emma-devices.png" width="75%" height="75%"/>
            </div>
            <div class="large-6 medium-12 small-12 column large-text-center">
                <div class="row expanded description_card">
                    <P class="ind_title" style="text-align: center">Healthcare</P>
                    <P class="ind_description">Protect your staff, patients, guests or other individuals using your facilities using mobile and PC notifications in the most modern cross-platform, real-time, fast, efficient software system designed to both prevent and manage threats in your healthcare network. </P>
                    <p class="ind_tag">Network. Connected. Lifesaving.</P>

                    <div class="row expanded ind_buttons">

                        <div class="large-6 medium-6 small-6 column">
                            <a data-open="contact-us-modal" class="button2 typebutton" data-type="Learn More - Healthcare">Learn
                                More</a>
                        </div>
                        <div class="large-6 medium-6 small-6 column">
                            <a data-open="schedule-demo-modal" class="button2 typebutton"
                               data-type="Schedule Demo - Healthcare">Schedule Demo Now</a>
                        </div>
<!--                        <div class="large-4 medium-4 small-4 column">-->
<!--                            <a target="_blank" href="https://thinksafewebinars.com/content/home.php" class="button2">Webinar Signup</a>-->
<!---->
<!--                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div><p>&nbsp;</p><br/></div>
</div>
<?php include('include/footer.php'); ?>

<?php include('include/pre_login_modals.php'); ?>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>


