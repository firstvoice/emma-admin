<?php include('include/pre_login_header.php'); ?>
<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Technical Sheets</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #FFFFFF;">


<?php include('include/index_top_bar.php'); ?>

<div class="row expanded the-clock">
    <div class="large-12 medium-12 small-12 columns" style="padding-bottom: 2em;height: auto;">
    </div>
    <div class="large-6 medium-6 small-6 columns">
        <div class="row expanded" style="padding-bottom: 1em;text-align: center;">

            <div class="large-4 callout primary">
                <div class="large-12 text-center">
                    <p style="font-weight: bold;color:red;">IMPORTANT!</p>
                </div>
                <div class="large-12 text-center">
                    <p>Please double check your phone settings!</p>
                </div>
                <div class="large-12 text-center">
                    <a href="./resource_folder/help/EMMA_Settings_on_your_ANDROID_DEVICE.pdf">Set up Android device</a>
                </div>
                <br/>
                <div class="large-12 text-center">
                    <a href="./resource_folder/help/EMMA_Settings_on_your_iOS_DEVICE.pdf">Set up IOS device</a>
                </div>
                <br/>
                <div class="large-12 text-center">
                    <a href=""></a>
                </div>
            </div>
            <div class="large-4 columns">
                <div class="large-12 align-center text-center">
                    <a href="https://play.google.com/store/apps/details?id=com.thinksafe.emma">Download Android</a>
                </div>
                <div class="large-12 align-center text-center">
                    <a href="https://play.google.com/store/apps/details?id=com.thinksafe.emma"><img
                                src="images/EMMA Android.png"></a>
                </div>
            </div>
            <div class="large-4 columns">
                <div class="large-12 align-center text-center">
                    <a href="https://itunes.apple.com/us/app/e-m-m-a/id1356773163?ls=1&mt=8">Download IOS</a>
                </div>
                <div class="large-12 align-center text-center">
                    <a href="https://itunes.apple.com/us/app/e-m-m-a/id1356773163?ls=1&mt=8"><img
                                src="images/EMMA IOS.png"></a>
                </div>
            </div>


        </div>
        <div class="large-6 medium-6 small-6 columns">
            <div class="row expanded" style="padding-bottom: 1em;text-align: center;">
            </div>
        </div>

        <div><p>&nbsp;</p><br/><br/><br/><br/><br/><br/><br/></div>
    </div>
    <?php include('include/footer.php'); ?>
    <?php include('include/pre_login_modals.php'); ?>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>


