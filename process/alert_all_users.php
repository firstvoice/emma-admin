<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/23/2017
 * Time: 12:16 PM
 */

require('../include/db.php');
include('../include/processing.php');

$data = array();
$errors = array();

$userId = $fvmdb->real_escape_string($_POST['user-id']);
$eventId = $fvmdb->real_escape_string($_POST['event-id']);
$message = $fvmdb->real_escape_string($_POST['message']);
$groups = $_POST['group'];

$topics = '/topics/emma-error';
$FcmServerKey = 'AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db';

$users = select_user_from_userID($userId);

if (!$user = $users->fetch_assoc()) {
    $errors['user'] = 'Can not find user';
} else {
    $topics = '/topics/emma-' . $user['emma_plan_id'];
}
if (empty($errors)) {
    $insertSystemMessage = insert_system_messages($eventId, $userId, "System Alert: ".$message, "2", '','');
//    $fvmdb->query("
//        INSERT INTO emma_system_messages (
//          emergency_id,
//          message,
//          created_date,
//          type
//        ) VALUES (
//          '" . $eventId . "',
//          'System Alert: " . $message . "',
//          '" . date('Y-m-d H:i:s') . "',
//          '2'
//        )
//    ");
    $messageId = $emmadb->insert_id;

    $postData = array(
        'to' => $topics,
        'time_to_live' => 600,
        'content_available' => true,
        'data' => array(
            'USER_ALERT' => 'true',
            'users' => array(),
            'message' => $message,
            'message_id' => $messageId
        )
    );
    $arr = "'" . implode('\', \'', $_POST['group']) . "'";
    $alertedUsers = select_user_with_groupArray($_POST['group']);
    foreach ($alertedUsers as $alertedUser) {
        $postData['data']['users'][] = $alertedUser['id'];
    }

    $ch = curl_init('https://fcm.googleapis.com/fcm/send');
    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Authorization: key=' . $FcmServerKey,
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postData)
    ));
    $response = curl_exec($ch);

    $responseData = json_decode($response);

    $data['response'] = $responseData;
}
$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);