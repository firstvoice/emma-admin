<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 12/14/2017
 * Time: 11:37 AM
 */
include('../include/db.php');
include('../include/processing.php');

$emmaPlanId = $_GET['emma-plan-id'];

$data = array();
$errors = array();
$data['events-drills-months'] = array();
$data['counts-pc-vs-app'] = array();
$data['events'] = array();
$data['event-type-counts'] = array();
$data['drill-type-counts'] = array();
$data['broadcast-event-response-times'] = array();
$data['user-event-response-times'] = array();
$data['broadcast-drill-response-times'] = array();
$data['user-drill-response-times'] = array();
$data['emma-resources'] = array();
/*testing area begins here ----*/

$data['mass-communication-widget'] = array();
$data['mass-communication-response-time-widget'] = array();
$data['mass-communication-timeline'] = array();
//$data['sos-plan'] = array();
$data['plan-statistics-totals'] = array();
/*testing area ends here --------------- */

$eventMonths = select_planstatistics_eventMonths($emmaPlanId);
//$eventMonths = $fvmdb->query("
//    SELECT year(e.created_date) AS year, month(e.created_date) AS month, count(*) AS count, sum(CASE WHEN e.drill = 1 THEN 1 ELSE 0 END) AS drill_count, sum(CASE WHEN e.drill = 0 THEN 1 ELSE 0 END) AS event_count
//    FROM emergencies e
//    JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
//    WHERE e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
//    GROUP BY year(e.created_date), month(e.created_date)
//");
while ($eventMonth = $eventMonths->fetch_assoc()) {
    $data['events-drills-months'][] = $eventMonth;
}
$countsPcVsApp = select_planstatistics_countsPcVsApp($emmaPlanId);
//$countsPcVsApp = $fvmdb->query("
//    SELECT (CASE er.device_type WHEN 0 THEN 'App' ELSE 'PC' END) AS type, count(*) AS count
//    FROM emergency_responses er
//    JOIN users u ON er.user_id = u.id WHERE u.emma_plan_id IN (". implode(',',$emmaPlanId) .")
//    GROUP BY er.device_type;
//");
while ($countPcVsApp = $countsPcVsApp->fetch_assoc()) {
    $data['counts-pc-vs-app'][] = $countPcVsApp;
}

$events = select_planstatistics_events($emmaPlanId);
//$events = $fvmdb->query("
//    select e.emergency_id, e.drill, et.name, e.created_date, u.username, e.description
//    from emergencies e
//    join users u on e.created_by_id = u.id
//    join emergency_types et on e.emergency_type_id = et.emergency_type_id
//    where e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
//");
while($event = $events->fetch_assoc()) {
    $data['events'][] = $event;
}
$eventTypeCounts = select_planstatistics_eventTypeCounts($emmaPlanId);
//$eventTypeCounts = $fvmdb->query("
//    SELECT et.name, count(*) AS count
//    FROM emergencies e
//    JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id AND e.drill = 0
//    WHERE e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
//    GROUP BY e.emergency_type_id
//");
while ($eventTypeCount = $eventTypeCounts->fetch_assoc()) {
    $data['event-type-counts'][] = $eventTypeCount;
}
$drillTypeCounts = select_planstatistics_drillTypeCounts($emmaPlanId);
//$drillTypeCounts = $fvmdb->query("
//    SELECT et.name, count(*) AS count
//    FROM emergencies e
//    JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id AND e.drill = 1
//    WHERE e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
//    GROUP BY e.emergency_type_id
//");
while ($drillTypeCount = $drillTypeCounts->fetch_assoc()) {
    $data['drill-type-counts'][] = $drillTypeCount;
}
$broadcastEventResponseTimes = select_planstatistics_brodcastEventResponseTimes($emmaPlanId);
//$broadcastEventResponseTimes = $fvmdb->query("
//    SELECT sm.emergency_id, min(sm.created_date) AS first_broadcast, e.created_date AS event_date, TIMESTAMPDIFF(SECOND, e.created_date, min(sm.created_date)) AS time_diff
//    FROM emergencies e
//      JOIN emma_system_messages sm ON e.emergency_id = sm.emergency_id AND sm.type = '1'
//      JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
//    WHERE e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
//    AND e.drill = '0'
//    GROUP BY e.emergency_id
//");
while ($broadcastEventResponseTime = $broadcastEventResponseTimes->fetch_assoc()) {
    $data['broadcast-event-response-times'][] = $broadcastEventResponseTime;
}

$userEventResponseTimes = select_planstatistics_userEventResponseTimes($emmaPlanId);
//$userEventResponseTimes = $fvmdb->query("
//    SELECT
//      e.emergency_id,
//      er.user_id,
//      er.emergency_response_id,
//      min(er.created_date)                                        AS first_response,
//      e.created_date                                              AS event_date,
//      TIMESTAMPDIFF(SECOND, e.created_date, min(er.created_date)) AS time_diff
//    FROM emergencies e
//      JOIN emergency_responses er ON e.emergency_id = er.emergency_id AND er.status != '-1'
//      JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
//    WHERE e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
//    AND e.drill = '0'
//    GROUP BY e.emergency_id, er.user_id
//    HAVING (TIMESTAMPDIFF(SECOND, e.created_date, min(er.created_date)) < (30*60))
//");
while ($userEventResponseTime = $userEventResponseTimes->fetch_assoc()) {
    $data['user-event-response-times'][] = $userEventResponseTime;
}
$broadcastDrillResponseTimes = select_planstatistics_broadcastDrillResponseTimes($emmaPlanId);
//$broadcastDrillResponseTimes = $fvmdb->query("
//    SELECT sm.emergency_id, min(sm.created_date) AS first_broadcast, e.created_date AS event_date, TIMESTAMPDIFF(SECOND, e.created_date, min(sm.created_date)) AS time_diff
//    FROM emergencies e
//      JOIN emma_system_messages sm ON e.emergency_id = sm.emergency_id AND sm.type = '1'
//      JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
//    WHERE e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
//    AND e.drill = '1'
//    GROUP BY e.emergency_id
//");
while ($broadcastDrillResponseTime = $broadcastDrillResponseTimes->fetch_assoc()) {
    $data['broadcast-drill-response-times'][] = $broadcastDrillResponseTime;
}

$userDrillResponseTimes = select_planstatistics_usertDrillResponseTimes($emmaPlanId);
//$userDrillResponseTimes = $fvmdb->query("
//    SELECT
//      e.emergency_id,
//      er.user_id,
//      er.emergency_response_id,
//      min(er.created_date)                                        AS first_response,
//      e.created_date                                              AS event_date,
//      TIMESTAMPDIFF(SECOND, e.created_date, min(er.created_date)) AS time_diff
//    FROM emergencies e
//      JOIN emergency_responses er ON e.emergency_id = er.emergency_id AND er.status != '-1'
//      JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
//    WHERE e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
//    AND e.drill = '1'
//    GROUP BY e.emergency_id, er.user_id
//    HAVING (TIMESTAMPDIFF(SECOND, e.created_date, min(er.created_date)) < (30*60))
//");
while ($userDrillResponseTime = $userDrillResponseTimes->fetch_assoc()) {
    $data['user-drill-response-times'][] = $userDrillResponseTime;
}

$emmaResources = select_planstatistics_emmaResources($emmaPlanId);
//$emmaResources = $fvmdb->query("
//    SELECT er.emma_resource_type_id, rt.resource_name, count(er.emma_resource_id) AS count
//    FROM emma_resources er
//    JOIN emma_resource_types rt ON er.emma_resource_type_id = rt.emma_resource_type_id
//    WHERE er.emma_plan_id IN (". implode(',',$emmaPlanId) .")
//    AND er.active = '1'
//    GROUP BY er.emma_resource_type_id
//");
while ($emmaResource = $emmaResources->fetch_assoc()) {
    $data['emma-resources'][] = $emmaResource;
}

/*Rachel testing charts here ---------------------------------- */

$massCommunication_widgets = select_massCommunications_widget($planID);
//  $massCommunication_widgets = $fvmdb->query("
//        SELECT COUNT(*) as count, year(created_date) AS year, month(created_date) AS month
//        FROM emma_mass_communications
//        WHERE emma_plan_id = '". $planID ."'
//        GROUP BY year(created_date), month(created_date)
//    ");
    while ($massCommunication_widget = $massCommunication_widgets->fetch_assoc()){
        $data['mass-communication-widget'][] = $massCommunication_widget;
}


$massCommunicationResponseTime_widgets = select_massCommunicationsResponseTime_widget($planID);
//    $massCommunicationResponseTime_widgets = $fvmdb->query("
//    SELECT COUNT (response_time_minutes) as count, ep.name
//    FROM emma_mass_communications emc
//    JOIN emma_plans ep on emc.emma_plan_id = ep.name
//    WHERE emma_plan_id = '" . $planID . "'
//    ");
   while ($massCommunicationResponseTime_widget = $massCommunicationResponseTime_widgets->fetch_assoc()){
       $data['mass-communication-response-time-widget'][] = $massCommunicationResponseTime_widget;
}

$massCommunicationTimeline_widgets = select_massCommunicationsTimeline_widget($emmaPlanId);
//  $massCommunicationTimeline_widgets = $fvmdb->query("
//    SELECT emc.emma_mass_communication_id, emc.created_date, u.username, emc.emma_plan_id, emc.notification
//        FROM emma_mass_communications emc
//        join users u on emc.created_by_id = u.id
//        where emc.emma_plan_id IN (". implode(',',$emmaPlanId) .")
//    ");
   while ($massCommunicationTimeline_widget = $massCommunication_widgets->fetch_assoc()){
       $data['mass-communication-timeline'][]= $massCommunicationTimeline_widget;
}



/*
$sosPlan_widgets = select_sosPlan_widget($planID);
// $sosPlan_widgets = $fvmdb->query("
//      SELECT COUNT(*) as count, year(created_date) AS year, month(created_date) AS month
//        FROM emma_sos
//        WHERE plan_id = '". $planID ."'
//        GROUP BY year(created_date), month(created_date)
//    ");
    while ($sosPlan_widget = $sosPlan_widgets->fetch_assoc()){
        $data['sos-plan'][]= $sosPlan_widget;
}
*/

$planStatisticsTotals_widgets = select_planstatistics_totals_widget($emmaPlanId);
//  $planStatisticsTotals_widgets = $fvmdb->query("
//        SELECT et.name, count(e.emergency_type_id) AS count
//        FROM emergencies e
//        JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
//        WHERE e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
//        GROUP BY e.emergency_type_id
//    ");
    while ($planStatisticsTotals_widget = $planStatisticsTotals_widgets->fetch_assoc()){
        $data['plan-statistics-totals'][] = $planStatisticsTotals_widget;
    }


/*end testing area ------------------- */

$data['get'] = $_GET;
$data['success'] = empty($errors);
echo json_encode($data);
