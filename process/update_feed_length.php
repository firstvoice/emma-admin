<?php
session_start();
require('../include/db.php');
include('../include/processing.php');

require('../vendor/php-jwt-master/src/JWT.php');
require('../vendor/php-jwt-master/src/BeforeValidException.php');
require('../vendor/php-jwt-master/src/ExpiredException.php');
require('../vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('../config/config.json'));

$USER = null;

$token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key, array('HS512'));

$USER = $token->data;

$data = array();
$errors = array();

$length = $fvmdb->real_escape_string($_POST['length']);

if(empty($USER)){
    $errors['no user'] = 'No token';
}
if(!isset($length)){
    $errors['no plan'] = 'no length';
}

if(empty($errors)) {
    $path = explode('process/', $_SERVER['PHP_SELF']);

    $token->data->feed_length = $length;

    $jwt = Firebase\JWT\JWT::encode($token, $CONFIG->key, 'HS512');
    setcookie('jwt', $jwt,time() + (9 * 60 * 60), '/');
}

$data['errors'] = $errors;
$data['success'] = empty($errors);


echo json_encode($data);