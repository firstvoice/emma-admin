<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/19/2017
 * Time: 9:40 AM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$id = $fvmdb->real_escape_string($_POST['id']);
$name = $fvmdb->real_escape_string($_POST['name']);
$type = $fvmdb->real_escape_string($_POST['type']);
$main = $fvmdb->real_escape_string($_POST['main']);

$radius = $fvmdb->real_escape_string($_POST['radius']);
$clat = $fvmdb->real_escape_string($_POST['center-lat']);
$clng = $fvmdb->real_escape_string($_POST['center-lng']);

$nwlat = $fvmdb->real_escape_string($_POST['nw-lat']);
$nwlng = $fvmdb->real_escape_string($_POST['nw-lng']);
$nelat = $fvmdb->real_escape_string($_POST['ne-lat']);
$nelng = $fvmdb->real_escape_string($_POST['ne-lng']);
$swlat = $fvmdb->real_escape_string($_POST['sw-lat']);
$swlng = $fvmdb->real_escape_string($_POST['sw-lng']);
$selat = $fvmdb->real_escape_string($_POST['se-lat']);
$selng = $fvmdb->real_escape_string($_POST['se-lng']);


//if (empty($name)){
//    $errors['name'] = 'Name is required';
//}
//if (empty($type)){
//    $errors['type'] = 'Type is required';
//}

if (empty($errors)) {
//    $update = update_geofence_with_geofenceID($id, $name, $type, $main, $radius, $clat, $clng, $nwlat, $nwlng, $nelat, $nelng, $swlat, $swlng, $selat, $selng);
    $update = update_geofencecircle_with_geofenceID($id, $radius, $clat, $clng);
//        $fvmdb->query("
//    UPDATE emma_geofence_locations
//    SET
//        fence_name = '" . $name . "',
//        type= '" . $type . "',
//        main_fence = '" . $main . "',
//        radius = '" . $radius . "',
//        center_lat = '" . $clat . "',
//        center_lng = '" . $clng . "',
//        nw_corner_lat = '" . $nwlat . "',
//        nw_corner_lng = '" . $nwlng . "',
//        ne_corner_lat = '" . $nelat . "',
//        ne_corner_lng = '" . $nelng . "',
//        sw_corner_lat = '" . $swlat . "',
//        sw_corner_lng = '" . $swlng . "',
//        se_corner_lat = '" . $selat . "',
//        se_corner_lng = '" . $selng . "'
//    WHERE id = " . $id . "
//  ");

}

$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);