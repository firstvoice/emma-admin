<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/6/2017
 * Time: 4:32 PM
 */

include('../include/db.php');
include('../include/processing.php');

$columns = $_GET['columns'];
$draw = $fvmdb->real_escape_string($_GET['draw']);
$length = $fvmdb->real_escape_string($_GET['length']);
$order = $_GET['order'];
$search = $_GET['search'];
$start = $fvmdb->real_escape_string($_GET['start']);
$active = $fvmdb->real_escape_string($_GET['active']);
$emmaPlanId = $fvmdb->real_escape_string($_GET['emma-plan-id']);

$orderString = $columns[$order[0]['column']]['data'] . ' ' . $order[0]['dir'];

$data = array();

$sites = select_getsites_sites($emmaPlanId, $orderString, $length, $start, $_GET['columns'][0]['search']['value'], $_GET['columns'][1]['search']['value'], $_GET['columns'][2]['search']['value'], $_GET['columns'][3]['search']['value']);
//    $fvmdb->query("
//    SELECT SQL_CALC_FOUND_ROWS s.emma_site_name, s.emma_site_id, e.emergency_id, MAX(e.active) AS emergency_status, count(DISTINCT e.emergency_id) AS emergencies, count(DISTINCT d.emergency_id) AS drills, null AS admins, null AS students, null AS staff
//    FROM emma_sites AS s
//      LEFT JOIN emergencies AS e ON e.emma_site_id = s.emma_site_id AND e.drill = 0
//      LEFT JOIN emergencies AS d ON d.emma_site_id = s.emma_site_id AND d.drill = 1
//    WHERE (" . ($emmaPlanId != '' ? "s.emma_plan_id = " . $emmaPlanId : "1") . ")
//    AND (" . ($_GET['columns'][0]['search']['value'] != "" ? "s.emma_site_name like ('%" . $_GET['columns'][0]['search']['value'] . "%')" : "1") . ")
//    GROUP BY s.emma_site_id
//    HAVING (
//      (" . ($_GET['columns'][1]['search']['value'] != "" ? "(max(e.active) = '0' AND 'Inactive' like ('" . $_GET['columns'][1]['search']['value'] . "%')) OR (max(e.active) = '1' AND 'Active' like ('" . $_GET['columns'][1]['search']['value'] . "%'))" : "1") . ")
//    AND (" . ($_GET['columns'][2]['search']['value'] != "" ? "count(DISTINCT e.emergency_id) like ('" . $_GET['columns'][2]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][3]['search']['value'] != "" ? "count(DISTINCT d.emergency_id) like ('" . $_GET['columns'][3]['search']['value'] . "%')" : "1") . ")
//    )
//    ORDER BY ". $orderString ."
//    LIMIT " . $length . " OFFSET " . $start . "
//");
//AND (" . ($_GET['columns'][0]['search']['value'] != "" ? "u.username like ('%" . $_GET['columns'][0]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][1]['search']['value'] != "" ? "u.firstname like ('%" . $_GET['columns'][1]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][2]['search']['value'] != "" ? "u.lastname like ('%" . $_GET['columns'][2]['search']['value'] . "%')" : "1") . ")
$found = select_FOUND_ROWS();
//    $fvmdb->query("
//    SELECT FOUND_ROWS()
//");
$count = $found->fetch_assoc();




$data['iTotalRecords'] = $count['FOUND_ROWS()'];
$data['iTotalDisplayRecords'] = $count['FOUND_ROWS()'];
$data['sEcho'] = $draw;
$data['aaData'] = array();
while ($site = $sites->fetch_assoc()) {
    $data['aaData'][] = $site;
}

echo json_encode($data);