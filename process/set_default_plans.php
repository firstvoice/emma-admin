<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 9/30/2019
 * Time: 10:06 AM
 */

session_start();
require('../include/db.php');
include('../include/processing.php');

require('../vendor/php-jwt-master/src/JWT.php');
require('../vendor/php-jwt-master/src/BeforeValidException.php');
require('../vendor/php-jwt-master/src/ExpiredException.php');
require('../vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('../config/config.json'));

$USER = null;

$token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key, array('HS512'));

$USER = $token->data;

$data = array();
$errors = array();
$plans = array();

$default = $_GET['plans'];

if(empty($USER)){
    $errors['no user'] = 'No token';
}

if(empty($errors)) {

    $result = update_user_home_default_plan($USER->id, $default);

    $path = explode('process/', $_SERVER['PHP_SELF']);

    $token->data->emma_default_plans = $default;

    $jwt = Firebase\JWT\JWT::encode($token, $CONFIG->key, 'HS512');
    setcookie('jwt', $jwt, time() + (9 * 60 * 60), '/');

}

$data['errors'] = $errors;
$data['success'] = empty($errors);
$data['result'] = $result;


echo json_encode($data);