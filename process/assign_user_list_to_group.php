<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/10/2017
 * Time: 11:36 PM
 */

include('../include/db.php');
include('../include/processing.php');

$user_ids = $_POST['selected_users'];
$group_ids = $_POST['group-ids'];
$data = array();
$errors = array();

if (empty($user_ids)) {
    $errors[] = 'No users selected';
}
if (empty($group_ids)) {
    $errors[] = 'No group id';
}

if (empty($errors)) {
    $user_list = implode("','", $user_ids);

    foreach ($user_ids AS $user) {

        //get all current groups
        $in_groups = select_groupsArray_with_userID($user);
//            $fvmdb->query("
//            SELECT group_concat(b.emma_group_id) AS groups
//            FROM emma_user_groups AS b
//            WHERE b.user_id = " . $user . "
//            GROUP BY b.user_id
//        ");
        if ($in_groups->num_rows > 0) {
            $in_group_list = $in_groups->fetch_assoc();
        }

        $ingroups_array = array();
        if (isset($in_group_list)) {
            $ingroups_array = explode(',', $in_group_list['groups']);
        }

        if (in_array('0', $ingroups_array)) {
            $delete = delete_user_groups($user, 0);
//                $fvmdb->query("
//                DELETE FROM emma_user_groups
//                WHERE user_id = " . $user . "
//                AND emma_group_id = 0
//            ");

            $data['deleted unassigned'] = true;
        }

        foreach ($group_ids AS $group) {

            if (!in_array($group, $ingroups_array)) {

                $insert = insert_user_groups($user, $group);
//                    $fvmdb->query("
//                    INSERT INTO emma_user_groups (emma_group_id, user_id)
//                    VALUES (" . $group . ", " . $user . ")
//                ");

                if (!$insert) {
                    $errors[] = 'update failed';
                }
            }
        }

    }


}

$data['post'] = $_POST;

$data['success'] = empty($errors);
$data['errors'] = $errors;
echo json_encode($data);