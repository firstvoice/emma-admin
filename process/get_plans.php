<?php

include('../include/db.php');
include('../include/processing.php');

$columns = $_GET['columns'];
$draw = $fvmdb->real_escape_string($_GET['draw']);
$length = $fvmdb->real_escape_string($_GET['length']);
$order = $_GET['order'];
$search = $_GET['search'];
$start = $fvmdb->real_escape_string($_GET['start']);
$active = $fvmdb->real_escape_string($_GET['active']);
$emmaPlanId = $fvmdb->real_escape_string($_GET['emma-plan-id']);

$data = array();

$guestCodes = select_getplans_guestCodes($length, $start, $_GET['columns'][0]['search']['value'], $_GET['columns'][1]['search']['value'], $_GET['columns'][2]['search']['value'], $_GET['columns'][3]['search']['value']);
//    $fvmdb->query("
//    SELECT SQL_CALC_FOUND_ROWS p.*
//    FROM emma_plans p
//    WHERE (" . ($_GET['columns'][0]['search']['value'] != "" ? "p.name like ('%" . $_GET['columns'][0]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][1]['search']['value'] != "" ? "p.date_active like ('%" . $_GET['columns'][1]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][2]['search']['value'] != "" ? "p.date_expired like ('%" . $_GET['columns'][2]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][3]['search']['value'] != "" ? "p.max_sites like ('" . $_GET['columns'][3]['search']['value'] . "%')" : "1") . ")
//    ORDER BY p.name
//    LIMIT " . $length . " OFFSET " . $start . "
//");
$found = select_FOUND_ROWS();
//    $fvmdb->query("
//    SELECT FOUND_ROWS()
//");
$count = $found->fetch_assoc();


$data['iTotalRecords'] = $count['FOUND_ROWS()'];
$data['iTotalDisplayRecords'] = $count['FOUND_ROWS()'];
$data['sEcho'] = $draw;
$data['aaData'] = array();
while ($guestCode = $guestCodes->fetch_assoc()) {
    $data['aaData'][] = $guestCode;
}

echo json_encode($data);
