<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/20/2017
 * Time: 12:28 PM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$planId = $fvmdb->real_escape_string($_POST['plan-id']);
$securityTypes = $_POST['security-types'];

if (empty($errors)) {
  $removePlanSecurities = delete_emma_plan_security_types($planId);
//      $fvmdb->query("
//    delete from emma_plan_security_types
//    where emma_plan_id = '" . $planId . "'
//  ");

  foreach ($securityTypes as $securityTypeId => $val) {
    $insertPlanSecurities = insert_emma_plan_security_types($planId, $securityTypeId);
//        $fvmdb->query("
//      insert into emma_plan_security_types (
//        emma_plan_id,
//        emma_security_type_id
//      ) values (
//        '" . $planId . "',
//        '" . $securityTypeId . "'
//      )
//    ");
  }
}

$data['success'] = empty($errors);
$data['errors'] = $errors;
$data['post'] = $_POST;
echo json_encode($data);