<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/31/2017
 * Time: 12:35 PM
 */

require('../include/db.php');
include('../include/processing.php');

$data = array();
$errors = array();

$typeId = $fvmdb->real_escape_string($_GET['type-id']);

if ($typeId == "") $errors['type-id'] = 'No type id';

$data['sub-types'] = array();

if (empty($errors)) {

  if($typeId == '0') {
    $subTypes = select_AllsecuritiyType();
//        $fvmdb->query("
//      select *
//      from emma_security_types
//    ");
    while ($subType = $subTypes->fetch_assoc()) {
      $data['sub-types'][] = $subType;
    }
  } else {
    $subTypes = select_subEventTypes_with_emergencyTypeID($typeId);
//        $fvmdb->query("
//      select *
//      from emergency_sub_types
//      where emergency_type_id = '" . $typeId . "'
//    ");
    while ($subType = $subTypes->fetch_assoc()) {
      $data['sub-types'][] = $subType;
    }
  }
}
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);
