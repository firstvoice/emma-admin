<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 1/29/2018
 * Time: 3:08 PM
 */

require('../include/db.php');
include('../include/processing.php');
require('../vendor/php-jwt-master/src/JWT.php');
require('../vendor/php-jwt-master/src/BeforeValidException.php');
require('../vendor/php-jwt-master/src/ExpiredException.php');
require('../vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('../config/config.json'));

$errors = array();
$data = array();

$jwt = $fvmdb->real_escape_string($_GET['jwt']);
if (empty($jwt)) {
  $errors['jwt'] = 'no jwt token';
}

/**
 * @param int $length
 * @return string
 * @throws Exception
 */
function randomToken($length = 32) {
  if (!isset($length)) {
    $length = 32;
  }
  if (function_exists('random_bytes')) {
    return random_bytes($length);
  }
  if (function_exists('mcrypt_create_iv')) {
    return mcrypt_create_iv($length, MCRYPT_DEV_URANDOM);
  }
  if (function_exists('openssl_random_pseudo_bytes')) {
    return openssl_random_pseudo_bytes($length);
  }
  throw new Exception("No valid function available");
}

if (empty($errors)) {
  try {
    $token = Firebase\JWT\JWT::decode($jwt, $CONFIG->key, array('HS512'));
    $USER = $token->data;
    $users = select_user_from_userID($USER->id);
//        $fvmdb->query("
//      SELECT *
//      FROM users
//      WHERE id = '" . $USER->id . "'
//    ");
    if ($user = $users->fetch_assoc()) {
      //$data['user'] = $user;
      $events = select_notificationcallback_events($user['emma_plan_id']);
//          $fvmdb->query("
//        SELECT e.*, et.name as type_name
//        FROM emergencies e
//        JOIN emergency_types et on e.emergency_type_id = et.emergency_type_id
//        WHERE e.emma_plan_id = '" . $user['emma_plan_id'] . "'
//      ");
      if ($event = $events->fetch_assoc()) {
        $data['event'] = $event;
      }
    }
  } catch (\Firebase\JWT\BeforeValidException $bve) {
    $errors['jwt'] = $bve->getMessage();
  } catch (\Firebase\JWT\ExpiredException $ee) {
    $errors['jwt'] = $ee->getMessage();
  } catch (\Firebase\JWT\SignatureInvalidException $sie) {
    $errors['jwt'] = $sie->getMessage();
  } catch (Exception $e) {
    $errors['jwt'] = $e->getMessage();
  }
}

$data['cookie'] = $_COOKIE;
$data['post'] = $_POST;
$data['get'] = $_GET;
$data['success'] = empty($errors);
$data['errors'] = $errors;

header('Content-type: application/json');
echo json_encode($data);