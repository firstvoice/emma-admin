<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 8/28/2017
 * Time: 10:04 AM
 */

include('../include/db.php');
include('../include/processing.php');
$type_id = $fvmdb->real_escape_string($_GET['type_id']);

$data = array();
$errors = array();

if(empty($errors))
{
    $items = select_assetstypedisplay($type_id);
//        $fvmdb->query("
//    SELECT eatd.*, eado.name as dropdown_name, eado.id as dropdown_id
//    FROM emma_asset_type_display as eatd
//    LEFT JOIN emma_assets_dropdown_options as eado ON eatd.dropdown_id = eado.id AND eado.display = 1
//    WHERE eatd.asset_type_id = '".$type_id."'
//    AND eatd.active = 1
//    ");
    $images = select_assetstypes($type_id);
//        $fvmdb->query("
//    SELECT eai.image
//    FROM emma_asset_types as eat
//    LEFT JOIN emma_asset_icons as eai on eat.emma_asset_icon_id = eai.asset_icon_id
//    WHERE eat.emma_asset_type_id = '".$type_id."'
//    ");
    while ($item = $items->fetch_assoc()) {
        $data['item'][] = $item;
    }
    while ($image = $images->fetch_assoc())
    {
        $data['image'][] = $image;
    }
}
//$data['success'] = empty($errors);
echo json_encode($data);