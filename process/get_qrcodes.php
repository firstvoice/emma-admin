<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/6/2017
 * Time: 3:58 PM
 */

include('../include/db.php');
include('../include/processing.php');

$userid = $fvmdb->real_escape_string($_POST['user']);


$data = array();
$errors = array();


$qrcode = select_get_qrcodes($userid);
while ($qr = $qrcode->fetch_assoc()){
    $data['qrcodes'][] = $qr;
}


$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);