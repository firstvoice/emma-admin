<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/23/2017
 * Time: 12:16 PM
 */

require('../include/db.php');
include('../include/processing.php');
include('../include/process_cookie.php');

$data = array();
$errors = array();
$userId = $fvmdb->real_escape_string($_POST['user-id']);
$eventId = $fvmdb->real_escape_string($_POST['event-id']);
$description = $fvmdb->real_escape_string($_POST['description']);
$newComments = $fvmdb->real_escape_string($_POST['comments']);
$alertNotification = $fvmdb->real_escape_string($_POST['alert-notification']);
$alertEmail = $fvmdb->real_escape_string($_POST['alert-email']);
$alertText = $fvmdb->real_escape_string($_POST['alert-text']);
$groups = $_POST['group'];

$groups_list = implode(',', $groups);
if(empty($groups)){
    $errors['groups'] = 'Groups are required';
}

if (empty($errors)) {
  $updateEvent = update_emergencies_closeEvent_with_emergencyID($eventId, $description, $newComments, $userId);
//  $fvmdb->query("
//    UPDATE emergencies
//    SET
//      active = '0',
//      description = '" . $description . "',
//      comments = '" . $newComments . "',
//      closed_date = '" . date('Y-m-d H:i:s') . "',
//      closed_by_id = '" . $userId . "'
//    WHERE emergency_id = '" . $eventId . "'
//  ");

  $insertSystemMessage = insert_system_messages($eventId, $userId, 'Event Closed', '', $description, $newComments);
//  $fvmdb->query("
//    INSERT INTO emma_system_messages (
//      emergency_id,
//      message,
//      created_by_id,
//      created_date,
//      description,
//      comments
//    ) VALUES (
//      '" . $eventId . "',
//      'Event Closed',
//      '" . $userId . "',
//      '" . date('Y-m-d H:i:s') . "',
//      '" . $description . "',
//      '" . $newComments . "'
//    )
//  ");
  $messageId = $emmadb->insert_id;

  foreach ($groups as $keyId => $groupId) {
    $insertSystemMessageGroup = insert_emma_system_message_groups($messageId, $groupId);
//        $fvmdb->query("
//      insert into emma_system_message_groups
//      (emma_system_message_id, emma_group_id)
//      values
//      ('" . $messageId . "', '" . $groupId . "')
//    ");
  }

}
$events = select_emergencie_with_eventID($eventId);
//    $fvmdb->query("
//  SELECT *
//  FROM emergencies
//  WHERE emergency_id = '" . $eventId . "'
//");
if ($event = $events->fetch_assoc()) {
  $type = $event['emergency_type_id'];
  $siteId = $event['emma_site_id'];
  $description = $event['description'];
  $comments = $event['comments'];
  $latitude = $event['latitude'];
  $longitude = $event['longitude'];
  $address = $event['nearest_address'];
  $emergencyId = $event['emergency_id'];
  $plan = $event['emma_plan_id'];
} else {
  $errors['event'] = 'Can not find event';
  $type = '';
  $siteId = '';
  $description = '';
  $comments = '';
  $latitude = '';
  $longitude = '';
  $address = '';
  $emergencyId = '';
}

$users = select_user_from_userID($userId);
//    $fvmdb->query("
//  SELECT *
//  FROM users
//  WHERE id = '" . $userId . "'
//");
if (!$user = $users->fetch_assoc()) {
  $errors['user'] = 'Can not find user';
}

$FcmServerKey = 'AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db';
$phone = 'faZ9b8GrO8k:APA91bHkZl8ccQXogP7kxhsyTHl_eM6DcyOQkU3pQ5HZD5Rl0zoIoqnsByq_9aLVqBwK36H-Uf3IpOu6OnScvUi6RYOPs0G9pckLptGZlAdQ1DLTEvxqWFIFFbbIUvi9l7SHdlCNObKn';
if((strpos(dirname($_SERVER['PHP_SELF']),'git-') !== false  || strpos(dirname($_SERVER['PHP_SELF']),'/egor') !== false)){
    $topics = '/topics/betaemma-' . $plan;
    $webTopics = '/topics/betaemma-web-'.$plan;
}
else{
    $topics = '/topics/emma-' . $plan;
    $webTopics = '/topics/emma-web-'.$plan;
}
//$topics = '/topics/emma-' . $user['emma_plan_id'];
$IOSTopics = array();

if (empty($errors)) {
  $emergencyTypeName = "Emergency";
  $emergencyTypes = select_eventTypes_with_eventTypeID($type);
//      $fvmdb->query("
//    SELECT *
//    FROM emergency_types
//    WHERE emergency_type_id = '" . $type . "'
//  ");
  if ($emergencyType = $emergencyTypes->fetch_assoc()) {
    $emergencyTypeName = $emergencyType['name'];
  }

  $site = 'unknown';
  $sites = select_site_with_siteID($siteId);
//  $fvmdb->query("
//    SELECT *
//    FROM emma_sites
//    WHERE emma_site_id = '" . $siteId . "'
//  ");
  if ($site = $sites->fetch_assoc()) {
    $site = $site['emma_site_name'];
  }

  $postData = array(
    'to' => $topics,
//        'condition' => "'alerts' in topics || 'aednotify' in topics || 'heartsafe' in topics || 'testing' in topics'",
    'time_to_live' => 900,
    'content_available' => true,
//        'notification' => array(
//            'title' => 'CPR Emergency: ' . $emergencyLat . ', ' . $emergencyLng,
//            'sound' => 'notification.aiff',
//            'badge' => '1'
//        ),
    'data' => array(
      'GROUP_ALERT' => 'true',
      'status' => 'CLOSING',
      'event' => 'EVENT CLOSED: ' . $emergencyTypeName,
      'sound' => true,
      'vibrate' => true,
      'emergency_lat' => $latitude,
      'emergency_lng' => $longitude,
      'emergency_address' => $address,
      'emergency_id' => $emergencyId,
      'groups' => array(),
      'site' => $site,
      'description' => $description,
      'comments' => $comments,
      'plan_ID' => $plan
    )
  );
  foreach ($groups as $group) {
      insert_emergency_received_groups($emergencyId, $group);
//    $fvmdb->query("
//      INSERT IGNORE INTO emergency_received_groups (emergency_id, emma_group_id)
//      VALUES
//      (
//        '" . $emergencyId . "',
//        '" . $group . "'
//      )
//    ");
    $postData['data']['groups'][] = $group;
      if((strpos(dirname($_SERVER['PHP_SELF']),'git-') !== false  || strpos(dirname($_SERVER['PHP_SELF']),'/egor') !== false)){
          $IOSTopics[] = "'betaemma-plan" . $plan . "-" . $group . "' in topics";
      }
      else{
          $IOSTopics[] = "'emma-plan" . $plan . "-" . $group . "' in topics";
      }
//    $IOSTopics[] = "'emma-plan" . $user['emma_plan_id'] . "-" . $group . "' in topics";
  }
  $nearestAedLat = 'none';
  $nearestAedLng = 'none';

  $jsonData = json_encode($postData);
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: key=AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db',
    'Content-Type: application/json'
  ));

  // $ch = curl_init('https://fcm.googleapis.com/fcm/send');
  // curl_setopt_array($ch, array(
  //   CURLOPT_POST => TRUE,
  //   CURLOPT_RETURNTRANSFER => TRUE,
  //   CURLOPT_HTTPHEADER => array(
  //     'Authorization: key=' . $FcmServerKey,
  //     'Content-Type: application/json'
  //   ),
  //   CURLOPT_POSTFIELDS => json_encode($postData)
  // ));
  $response = curl_exec($ch);

  /* A DIFFERENT WAY TO DO THE SAME THING ("PURE" PHP)
  $context = stream_context_create(array(
     'http' => array(
         'method' => 'POST',
         'head' => "Authorization: key = ".$FcmServerKey."\r\n".
             "Content - Type: application / json\r\n",
         'content' => json_encode($postData)
     )
  ));
  $response = file_get_contents('https://fcm.googleapis.com/fcm/send', false, $context);
  */

  $IOSTopicsString = $IOSTopics[0];
  if (count($IOSTopics) > 0) {
    for ($i = 1; ($i < 5 && $i < count($IOSTopics)); $i++) {
      $IOSTopicsString .= ' || ' . $IOSTopics[$i];
    }
  }

  $data['iosTopicString'] = $IOSTopicsString;
  $data['iosTopics'] = $IOSTopics;

  $postIOSData = array(
    'condition' => $IOSTopicsString,
    'time_to_live' => 600,
    'priority' => 'high',
    'notification' => array(
      'title' => 'EVENT CLOSED: ' . $emergencyTypeName,
      'sound' => 'notification.aiff',
      'badge' => '1'
    ),
    'data' => array(
      'GROUP_ALERT' => 'true',
      'event' => 'EVENT CLOSED: ' . $emergencyTypeName,
      'emergency_lat' => $latitude,
      'emergency_lng' => $longitude,
      'emergency_address' => $address,
      'emergency_id' => $emergencyId,
      'site' => $site,
      'description' => $description,
      'comments' => $comments,
        'ClosedEvent' => true
    )
  );

  if (count($IOSTopics) > 5) {
    $m = 5;
    while ($m < count($IOSTopics)) {
      for ($i = $m; ($i < ($m + 5) && $i < count($IOSTopics)); $i++) {
        if ($i == $m) {
          $IOSTopicsString = $IOSTopics[$i];
        }
        $IOSTopicsString .= ' || ' . $IOSTopics[$i];
      }

      $postIOSData2 = array(
//        'to' => $IOSTopicsString,
        'condition' => $IOSTopicsString,
        'time_to_live' => 600,
        'priority' => 'high',
//            'content_available' => true,
        'notification' => array(
          'title' => 'EVENT CLOSED: ' . $emergencyTypeName,
          'sound' => 'notification.aiff',
          'badge' => '1'
        ),
        'data' => $postIOSData['data']
      );

      $ch = curl_init('https://fcm.googleapis.com/fcm/send');
      curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
          'Authorization: key=' . $FcmServerKey,
          'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postIOSData2)
      ));
      $IOSresponse2 = curl_exec($ch);

      $m += 5;

//        $IOSresponse2Data = json_decode($IOSresponse2);
//        $data['IOSresponse2'] = $IOSresponse2Data;
    }
  }

  $ch = curl_init('https://fcm.googleapis.com/fcm/send');
  curl_setopt_array($ch, array(
    CURLOPT_POST => TRUE,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_HTTPHEADER => array(
      'Authorization: key=' . $FcmServerKey,
      'Content-Type: application/json'
    ),
    CURLOPT_POSTFIELDS => json_encode($postIOSData)
  ));
  $IOSresponse = curl_exec($ch);

  $iosResult = json_decode($IOSresponse);
  $data['iosresponse'] = $iosResult;

  //browser Notification
    sort($postData['data']['groups']);
    $deviceGroupString = implode("-", $postData['data']['groups']);
    $deviceGroupString = 'emma-web-plan'.$plan.'-'.$deviceGroupString;
    $data['deviceGroupString'] = $deviceGroupString;

    $deviceKeyID = '';

    $deviceGroupQuery = select_emmaNotificationDeviceGroups_with_keyName($deviceGroupString);
//    $fvmdb->query("
//        SELECT g.notification_key_name AS nkeyName, g.notification_key AS nkey, g.last_updated
//        FROM emma_notification_device_groups AS g
//        WHERE g.notification_key_name = '". $deviceGroupString ."'
//    ");
    if($deviceGroupQuery->num_rows > 0){
        //add new users to device group
        $deviceGroupResult = $deviceGroupQuery->fetch_assoc();
        $deviceKeyID = $deviceGroupResult['nkey'];

//        $data['deviceGroupResult']['nkeyname'] = $deviceGroupResult['nkeyName'];
//        $data['deviceGroupResult']['nkey'] = $deviceGroupResult['nkey'];
//        $data['deviceGroupResult']['last_updated'] = $deviceGroupResult['last_updated'];


        //find if there are new users to add
        $groupString = implode("','", $postData['data']['groups']);
        $webUsersQuery = select_notificationClients_with_planID_groupString_and_createdDate($plan, $groupString, $deviceGroupResult['last_updated']);
//            $fvmdb->query("
//            Select DISTINCT c.client_id
//            FROM emma_notification_clients AS c
//            JOIN users AS u ON c.user_id = u.id AND u.emma_plan_id = '" . $user['emma_plan_id'] . "'
//            JOIN emma_user_groups AS g ON u.id = g.user_id
//            WHERE g.emma_group_id IN ('" . $groupString . "')
//            AND c.created_date > '".$deviceGroupResult['last_updated']."'
//        ");
        if($webUsersQuery->num_rows > 0){
            //put new users in array
            $payload = array();
            $payload['registration_ids'] = array();
            while($webUsersResult = $webUsersQuery->fetch_assoc()){
                $payload['registration_ids'][] = $webUsersResult['client_id'];
            }
            //add users to new device group
            $payload['operation'] = 'add';
            $payload['notification_key_name'] = $deviceGroupString;
            $payload['notification_key'] = $deviceGroupResult['nkey'];


            //send request to add users
            $ch = curl_init('https://fcm.googleapis.com/fcm/notification');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: key=' . $FcmServerKey,
                    'Content-Type: application/json',
                    'project_id: 742343872016'
                ),
                CURLOPT_POSTFIELDS => json_encode($payload)
            ));
            $addToDeviceGroupResult = curl_exec($ch);
//            $addToDeviceGroupResultData = json_decode($addToDeviceGroupResult);
//            $data['add_to_device_group'] = $addToDeviceGroupResultData;
        }
    }
    else {
            //[Create Device Group]
            //get users for device group
        $emptyWeb = false;
            $groupString = implode("','", $postData['data']['groups']);
            $webUsersQuery = select_notificationClients_with_planID_groupString($plan, $groupString);
//                $fvmdb->query("
//                Select DISTINCT c.client_id
//                FROM emma_notification_clients AS c
//                JOIN users AS u ON c.user_id = u.id AND u.emma_plan_id = '" . $user['emma_plan_id'] . "'
//                JOIN emma_user_groups AS g ON u.id = g.user_id
//                WHERE g.emma_group_id IN ('" . $groupString . "')
//            ");
            if ($webUsersQuery->num_rows > 0) {
                //put users in array
                $payload = array();
                $payload['registration_ids'] = array();
                while ($webUsersResult = $webUsersQuery->fetch_assoc()) {
                    $payload['registration_ids'][] = $webUsersResult['client_id'];
                }
                //other payload info
                $payload['operation'] = 'create';
                $payload['notification_key_name'] = $deviceGroupString;


                //send request
                $ch = curl_init('https://fcm.googleapis.com/fcm/notification');
                curl_setopt_array($ch, array(
                    CURLOPT_POST => TRUE,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HTTPHEADER => array(
                        'Authorization: key=' . $FcmServerKey,
                        'Content-Type: application/json',
                        'project_id: 742343872016'
                    ),
                    CURLOPT_POSTFIELDS => json_encode($payload)
                ));
                $createDeviceGroupResult = curl_exec($ch);
                $createDeviceGroupResponseData = json_decode($createDeviceGroupResult);
//            $data['create_device_group'] = $createDeviceGroupResponseData;
                $deviceKeyID = $createDeviceGroupResponseData->{"notification_key"};
//            $data['deviceKeyTestCheck'] = $deviceKeyID;
//            $data['webResponseRaw'] = $createDeviceGroupResult;

                $insert = insert_emma_notification_device_groups($deviceGroupString, $deviceKeyID);
//                    $fvmdb->query("
//                    INSERT INTO emma_notification_device_groups (notification_key_name, notification_key, last_updated)
//                    VALUES ('" . $deviceGroupString . "', '" . $deviceKeyID . "', NOW())
//                ");
//            $errors['test'] = 'test';
            } else {
                $emptyWeb = true;
            }

    }

    if(empty($errors) && $emptyWeb == false) {

        if (empty($icon_name)) {
            $icon_name = 'emma_icon.png';
        }

        $postWebData = array(
            'to' => $deviceKeyID,
            'time_to_live' => 600,
            "message" => array(
                'webpush' => array(
                    'headers' => array(
                        "Urgency" => 'high'
                    ),
                    'fcm_options' => array(
                        'link' => "https://www.emmaadmin.com"
                    )
                ),
                'notification' => array(
                    'body' => 'Near: ' . $address,
                    'title' => $emergencyTypeName,
                    'sound' => 'notification.aiff',
                    'requireInteraction' => "true",
                    'click_action' => "https://www.emmaadmin.com"
                )
            ),
            'notification' => array(
                'body' => 'Near: ' . $address,
                'title' => $emergencyTypeName,
                'sound' => 'notification.aiff',
                'requireInteraction' => "true",
                'click_action' => "https://www.emmaadmin.com"
            ),
            'data' => array(
                'title' => $emergencyTypeName,
                'body' => ' Near: ' . $address,
                'emergency_id' => $emergencyId,
                'emergency_icon' => $icon_name
            )
        );


        $ch = curl_init('https://fcm.googleapis.com/fcm/send');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Authorization: key=' . $FcmServerKey,
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postWebData)
        ));
        $webResponse = curl_exec($ch);
    }


  $responseData = json_decode($response);

  $data['response'] = $responseData;


    /*
      * Send Emails
      */
    $can_send_email_alerts = false;
    if($alertEmail){
        $valid_email_alerts = select_plan_from_planID($plan);
//            $fvmdb->query("
//            SELECT emma_plan_id, emails
//            FROM emma_plans
//            WHERE emma_plan_id = '".$plan."'
//        ");
        if($valid_email_alerts->num_rows > 0){
            $email_alerts_results = $valid_email_alerts->fetch_assoc();
            if($email_alerts_results['emails']){
                $can_send_email_alerts = true;
            }
        }
    }
    $data['CanEmail'] = $can_send_email_alerts;
    $data['EmailGroupList'] = $groups_list;
    if($can_send_email_alerts){



        $email_recipient_query = select_distinctUser_with_groupString($groups_list);
//        $fvmdb->query("
//            SELECT DISTINCT u.username
//            FROM users AS u
//            JOIN emma_user_groups AS ug ON u.id = ug.user_id AND ug.emma_group_id in ('".$groups_list."')
//            WHERE u.active = 1 AND u.display = 'yes'
//        ");

        if($email_recipient_query->num_rows > 0){
            while($recipient = $email_recipient_query->fetch_assoc()){
                $to = $recipient['username'];
                $data['EmailToList'][] = $to;
                $subject = 'EMMA Alert';

                $body = '<html><head></head><body style="color:#000001;">
                           
                        <h2>'.$emergencyTypeName.'</h2>
                        <hr>
                        <div>Site: '.$site.'</div>
                        <div>Description: '.$event['description'].'</div>
                        <div>Comments: '.$event['comments'].'</div>
						</body></html>';



                try{
                    // Create the Mailer using your created Transport
                    $mailer = Swift_Mailer::newInstance($SMT);
                    $message = Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom(array(FROMEMAIL => PROGRAM))
                        ->setTo(array($to))
                        ->setBody($body, 'text/html');
                    $result = $mailer->send($message);
                    if(!$result){
                        error_log('Failed to sent email to: '. $to);
                    }
                    $mailer->getTransport()->stop();

                }
                catch(Swift_TransportException $e){
                    error_log('Swiftmailer transport exception thrown on email of close_event.php, sending message to: '.$to);
                    error_log($e->getMessage());
                }
                catch(Swift_IoException $e){
                    error_log('Swiftmailer IO exception thrown on email of close_event.php, sending message to: '.$to);
                    error_log($e->getMessage());
                }
                catch(Exception $e){
                    error_log('Exception thrown on email of close_event.php, sending message to: '.$to);
                    error_log($e->getMessage());
                }
            }
        }
    }





}
$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);