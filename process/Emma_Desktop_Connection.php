<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/27/2019
 * Time: 9:27 AM
 */

//Emma desktop (Unity) connects to our server here.

require('../include/db.php');

$key = '1MgQjYCMLPspDZiMKmqjfuF7ojuV7yD3Mb'; //Auth Key
$givenKey = $fvmdb->real_escape_string($_POST['key']);
$type = $fvmdb->real_escape_string($_POST['type']);
$emmaUsername = $fvmdb->real_escape_string($_POST['emma_username']); //Username that already exists that we check for in DB.
$emmaPassword = $fvmdb->real_escape_string($_POST['emma_password']); //Password that already exists that we check for in DB.
$auth = $fvmdb->real_escape_string($_POST['auth']);
$lat = $fvmdb->real_escape_string($_POST['latitude']);
$lng = $fvmdb->real_escape_string($_POST['longitude']);
$pin = $fvmdb->real_escape_string($_POST['pin']);
$sos_id = $fvmdb->real_escape_string($_POST['sos_id']);
$group_id = $fvmdb->real_escape_string($_POST['group_id']); //Highest privileged group (prevents having to re-query).
$platform = $fvmdb->real_escape_string($_POST['platform']); //windows or mac (for checking versions).
$dropdownID = $fvmdb->real_escape_string($_POST['dropdownID']);

$version = $fvmdb->real_escape_string($_POST['version']); //Gets passed so when we log the active users (or detract from) we know which version.


$data = array();
$errors = array();

if(empty($type)) {
    $errors['Missing Type'] = 'ERRORType parameter is missing';
}
if(empty($givenKey))
{
    $errors['Auth Key'] = 'ERRORAuth Key is missing ';
}
if($givenKey !== $key && !empty($givenKey))
{
    $errors['Auth Key Invalid'] = 'ERRORAuth Key is invalid, please contact support ';
}




function getDistance( $latitude1, $longitude1, $latitude2, $longitude2 ) {
    $earth_radius = 6371;

    $dLat = deg2rad( $latitude2 - $latitude1 );
    $dLon = deg2rad( $longitude2 - $longitude1 );

    $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
    $c = 2 * asin(sqrt($a));
    $d = $earth_radius * $c;

    return $d;
}








if(empty($errors)) {
    if ($type == 'login') {
        //$emmaUsername = filter_var($emmaUsername, FILTER_SANITIZE_EMAIL);

        if (empty($errors)) {
            $users = $fvmdb->query("
        SELECT u.*, eg.*
        FROM users as u 
        LEFT JOIN emma_user_groups as eug on u.id = eug.user_id
        LEFT JOIN emma_groups as eg on u.emma_plan_id = eg.emma_plan_id AND eg.emma_group_id = eug.emma_group_id
        WHERE u.username = '".$emmaUsername."'
        AND (u.password = sha1('".$emmaPassword."') or u.password = password('".$emmaPassword."'))
        ");
            $privilegeArray = [0 => 'info_only' , 1 =>  'base' ,  2 => 'emma_sos', 3 => 'geofences', 4 => 'security', 5 => 'admin']; //This is an array with what I believe is lowest to highest privilege settings. Base just means nothing checked (I consider this to be higher privilege than info_only)
            $groupId;
            $highestPrivilege; //Highest privilege user has in all of his groups.
            $firstRun = true;
            while($user = $users->fetch_assoc())
            {
                if($firstRun)
                {
                    //Only runs once, this is t o prevent $groupId from being empty.
                    $groupId = $user['emma_group_id'];
                    if($user['info_only'] == 1)
                    {
                        $highestPrivilege = 'info_only';
                    }
                    if($user['emma_sos'] == 1)
                    {
                        $highestPrivilege = 'emma_sos';
                    }
                    if($user['geofences'] == 1)
                    {
                        $highestPrivilege = 'geofences';
                    }
                    if($user['security'] == 1)
                    {
                        $highestPrivilege = 'security';
                    }
                    if($user['admin'] == 1)
                    {
                        $highestPrivilege = 'admin';
                    }
                    $firstRun = false;
                }


                if($user['info_only'] == 0 && $user['emma_sos'] == 0 && $user['geofences'] == 0 && $user['security'] == 0 && $user['admin'] == 0)
                {
                    //Base user, nothing is checked.
                    if(array_search('base',$privilegeArray) > array_search($highestPrivilege,$privilegeArray))
                    {
                        $highestPrivilege = 'base';
                        $groupId = $user['emma_group_id'];
                    }
                }
                else {
                    if ($user['info_only'] == 1) {
                        if (array_search('info_only', $privilegeArray) > array_search($highestPrivilege, $privilegeArray)) {
                            $highestPrivilege = 'info_only';
                            $groupId = $user['emma_group_id'];
                        }
                    }
                    if ($user['emma_sos'] == 1) {
                        //If the index of emma_sos is greater than the index of the currently given highestPrivilege. Emma_SOS is greater than info only so we go with the Emma_SOS plan.
                        if (array_search('emma_sos', $privilegeArray) > array_search($highestPrivilege, $privilegeArray)) {
                            $highestPrivilege = 'emma_sos';
                            $groupId = $user['emma_group_id'];
                        }
                    }
                    if ($user['geofences'] == 1) {
                        //If the index of emma_sos is greater than the index of the currently given highestPrivilege. Emma_SOS is greater than info only so we go with the Emma_SOS plan.
                        if (array_search('geofences', $privilegeArray) > array_search($highestPrivilege, $privilegeArray)) {
                            $highestPrivilege = 'geofences';
                            $groupId = $user['emma_group_id'];
                        }
                    }
                    if ($user['security'] == 1) {
                        //If the index of emma_sos is greater than the index of the currently given highestPrivilege. Emma_SOS is greater than info only so we go with the Emma_SOS plan.
                        if (array_search('security', $privilegeArray) > array_search($highestPrivilege, $privilegeArray)) {
                            $highestPrivilege = 'security';
                            $groupId = $user['emma_group_id'];
                        }
                    }
                    if ($user['admin'] == 1) {
                        //If the index of emma_sos is greater than the index of the currently given highestPrivilege. Emma_SOS is greater than info only so we go with the Emma_SOS plan.
                        if (array_search('admin', $privilegeArray) > array_search($highestPrivilege, $privilegeArray)) {
                            $highestPrivilege = 'admin';
                            $groupId = $user['emma_group_id'];
                        }
                    }
                }
            }

            $topPrivilegeUser = $fvmdb->query("
            SELECT u.*,ep.sos_duration, eg.geofences
            FROM users as u
            LEFT JOIN emma_groups as eg on eg.emma_group_id = '".$groupId."'
            LEFT JOIN emma_plans as ep on u.emma_plan_id = ep.emma_plan_id
            WHERE u.username = '".$emmaUsername."'
            AND (u.password = sha1('".$emmaPassword."') or u.password = password('".$emmaPassword."'))
            ORDER BY u.id
            ");
            if($topPrivilegeUser->num_rows > 0)
            {
                $dataUser = $topPrivilegeUser->fetch_assoc();
                //One found, success, send back emma_plan_id
                echo $dataUser['auth']; //Send the auth code back to Unity Client
                echo ',';
                if($dataUser['info_only'] == 1)
                {
                    echo '1';
                }
                else{
                    echo '0';
                }
                echo ',';
                echo $dataUser['sos_duration'];
                echo ',';
                echo $groupId;
                echo ',';
                if($dataUser['geofences'] == 1){
                    echo '1';
                }else{
                    echo'0';
                }

            }
            else{
                $errors['Could not validate'] = 'ERRORCould not validate the user';
            }


            foreach($errors as $error)
            {
                echo $error;
            }
        }
    }
    else if ($type == 'GetIcons')
    {
        //This loads on start, and it will get the event icons that the user has access too.
        if (empty($errors)) {
            $users = $fvmdb->query("
            SELECT u.emma_plan_id, eg.info_only, u.id
            FROM users u
            LEFT JOIN emma_user_groups as eug on u.id = eug.user_id
            LEFT JOIN emma_groups as eg on u.emma_plan_id = eg.emma_plan_id AND eg.emma_group_id = '".$group_id."'
            WHERE u.auth = '".$auth."'
            GROUP BY u.id
            ");
            if($users->num_rows > 0)
            {
                $user = $users->fetch_assoc();
                if($user['info_only'] == false) {
                    //Echo response cards that they can have.
                    $planEventTypes = $fvmdb->query("
                    select DISTINCT et.img_filename, et.emergency_type_id, et.name
                    from emma_group_events e
                       join emergency_types et on e.event_id = et.emergency_type_id
                    where e.emma_group_id IN (SELECT u.emma_group_id FROM emma_user_groups AS u JOIN emma_groups AS g ON u.emma_group_id = g.emma_group_id WHERE g.emma_plan_id = '".$user['emma_plan_id']."' AND u.user_id = '".$user['id']."')
                    AND et.emergency_type_id > 0
                    ");
                    while ($planEventType = $planEventTypes->fetch_assoc()) {
                        echo $planEventType['img_filename'];
                        echo ','; //We separate the images by commas in C#
                        echo $planEventType['emergency_type_id'];
                        echo ',';
                        echo $planEventType['name'];
                        echo ',';
                    }
                }
                else{
                    echo 'INFO ONLY';
                }
            }
            else{
                $errors['Auth Key Invalid'] = "ERRORAuth code invalid";
            }
            foreach ($errors as $error) {
                echo $error;
            }

        } else {
            //Send errors back to PC
            foreach ($errors as $error) {
                echo $error;
            }
        }
    }
    else if ($type == 'GetEvents') {
        //Checks for events, also downloads the response card information that the user can access.
        if (empty($errors)) {
            $users = $fvmdb->query("
                SELECT u.emma_plan_id, u.id, eg.info_only
                FROM users u
                LEFT JOIN emma_user_groups as eug on u.id = eug.user_id
                LEFT JOIN emma_groups as eg on u.emma_plan_id = eg.emma_plan_id AND eg.emma_group_id = '".$group_id."'
                WHERE u.auth = '".$auth."'
                GROUP BY u.id
            ");
            if($users->num_rows > 0)
            {
                $user = $users->fetch_assoc();


                $multi_plan = [];
                $multi_group = [];
                $selectPlans = $fvmdb->query("
                    SELECT mp.plan_id, p.name, eug.emma_group_id
                    FROM emma_plans p
                    LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id
                    LEFT JOIN emma_user_groups as eug on eug.user_id = '".$user['id']."'
                    WHERE mp.user_id = '" . $user['id'] . "'
                    AND p.date_expired > NOW()
                ");

                while($plan = $selectPlans->fetch_assoc())
                {
                    array_push($multi_plan,$plan['plan_id']);
                    array_push($multi_group,$plan['emma_group_id']);
                }


                $events = $fvmdb->query("
                SELECT e.*,et.name AS type, et.badge, et.img_filename, es.emma_site_name, es.emma_site_notes, est.name as sub_name
                FROM emergency_received_groups as erg
                JOIN  emergencies as e on e.emergency_id = erg.emergency_id
                JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
                JOIN emergency_sub_types est on est.emergency_sub_type_id = e.emergency_sub_type_id
                JOIN emma_sites es ON es.emma_site_id = e.emma_site_id
                WHERE erg.emma_group_id IN (".implode(',',$multi_group).")
                AND e.active = '1'
                AND e.emma_plan_id IN (".implode(',',$multi_plan).")
                AND e.parent_emergency_id IS NULL
                ");



                if($events->num_rows != 0)
                {
                    $event = $events->fetch_assoc();



                    echo $event['emergency_id'];
                    echo '^%&!5#2%^';
                    echo $event['drill'];
                    echo '^%&!5#2%^';
                    echo $event['img_filename'];
                    echo '^%&!5#2%^';
                    echo $event['type'];
                    echo '^%&!5#2%^';
                    echo $event['sub_name'];
                    echo '^%&!5#2%^';
                    echo $event['emma_site_name'];
                    echo '^%&!5#2%^';
                    echo $event['comments'];
                    echo '^%&!5#2%^';
                    echo $event['description'];
                    echo '^%&!5#2%^';
                    echo $event['created_date'];




                    echo ')#($*!%#^$(%zefz)#_)!(@*#&$^';//Separator




                    if($user['info_only'] == 0) { //Only download the responses if they are able to use them.
                        $response_statuses = $fvmdb->query("
                            SELECT etr.emergency_response_status_id
                            FROM emergency_type_response_statuses etr
                            WHERE etr.emergency_type_id = '" . $event['emergency_type_id'] . "'
                        ");
                        while ($response_status = $response_statuses->fetch_assoc()) {
                            $specific_responses = $fvmdb->query("
                                SELECT e.name,e.color, e.emergency_response_status_id
                                FROM emergency_response_status e
                                WHERE e.emergency_response_status_id = '" . $response_status['emergency_response_status_id'] . "' 
                            ");
                            while ($specific_response = $specific_responses->fetch_assoc()) {

                                echo $specific_response['name'] . '5#2%^%&!^' . $specific_response['emergency_response_status_id']; //The '^%&!5#2%^' string will be a separator so we can get the text and the ID in one string
                                echo '^%&!5#2%^'; //Separator
                                echo $specific_response['color'];
                                echo '^%&!5#2%^'; //Separator
                            }
                        };
                    }
//
//                    $broadcastScripts = $fvmdb->query("
//                        select e.*
//                        from emma_scripts e
//                        WHERE e.emma_plan_id = '" . $user['emma_plan_id'] . "'
//                        AND e.emma_script_type_id = '1'
//                        AND e.emma_script_group_id = '". $group_id ."'
//                        order by name
//                    ");
//                    while($broadcastScript = $broadcastScripts->fetch_assoc()) {
//                        echo $broadcastScript['name'];
//                        echo '^%&!5#2%^';
//                        echo $broadcastScript['text'];
//                        echo '^%&!5#2%^';
//                    }
                }
                else{
                    echo 'empty';
                }
            }
            else{
                $errors['Auth Key Invalid'] = "ERRORAuth code invalid";
            }

            foreach ($errors as $error) {
                echo $error;
            }

        } else {
            //Send errors back to PC
            foreach ($errors as $error) {
                echo $error;
            }
        }
    }
    else if ($type == 'GetDropdowns') {
        //Gets dropdown values for creating an event (sites, sub events, etc)
        $users = $fvmdb->query("
            SELECT u.emma_plan_id, eg.info_only
            FROM users u
            LEFT JOIN emma_groups as eg on u.emma_plan_id = eg.emma_plan_id AND eg.emma_group_id = '".$group_id."'
            WHERE u.auth = '".$auth."'
            ");
        if($users->num_rows > 0)
        {
            $user = $users->fetch_assoc();
            if($user['info_only'] == 0) {

                $groups = $fvmdb->query("
                SELECT *
                FROM emma_groups as eg
                WHERE eg.emma_plan_id = '" . $user['emma_plan_id'] . "'
                AND eg.emma_group_id = '".$group_id."'
                ");
                $planEventTypes = $fvmdb->query("
                  select *
                  from emma_plan_event_types pet
                  join emergency_types et on pet.emma_emergency_type_id = et.emergency_type_id
                  where pet.emma_emergency_type_id = '" . $dropdownID . "'
                ");
                $planEventType = $planEventTypes->fetch_assoc();
                $subTypes = $fvmdb->query("
                  SELECT *
                  FROM emergency_sub_types
                  WHERE emergency_type_id = '" . $planEventType['emergency_type_id'] . "'
                  ");
                while ($subType = $subTypes->fetch_assoc()) {
                    echo $subType['name'];
                    echo '^%&!5#2%^';
                    echo $subType['emergency_sub_type_id'];
                    echo '^%&!5#2%^';
                }
                echo ')#($*!%#^$(%zefz)#_)!(@*#&$^'; //We use this to separate the subtypes vs the site names for C# code. (We can only return one single string)
                $sites = $fvmdb->query("
                  select *
                  from emma_sites
                  where emma_plan_id = '" . $user['emma_plan_id'] . "'
                    ");
                while ($site = $sites->fetch_assoc()) {
                    echo $site['emma_site_name'];
                    echo '^%&!5#2%^';
                    echo $site['emma_site_id'];
                    echo '^%&!5#2%^';
                }
            }
        }
    }
    else if ($type == 'GetScripts') {
        //Gets dropdown values for creating an event (sites, sub events, etc)
        $users = $fvmdb->query("
            SELECT u.emma_plan_id, eg.info_only
            FROM users u
            LEFT JOIN emma_groups AS eg ON u.emma_plan_id = eg.emma_plan_id AND eg.emma_group_id = '" . $group_id . "'
            WHERE u.auth = '" . $auth . "'
            ");
        if ($users->num_rows > 0) {
            $user = $users->fetch_assoc();

            $broadcastScripts = $fvmdb->query("
                select e.*
                from emma_scripts e
                WHERE e.emma_plan_id = '" . $user['emma_plan_id'] . "'
                AND e.emma_script_type_id = '1'
                AND e.emma_script_group_id = '". $group_id ."'
                order by name
            ");
            while($broadcastScript = $broadcastScripts->fetch_assoc()) {
                echo $broadcastScript['name'];
                echo '^%&!5#2%^';
                echo $broadcastScript['text'];
                echo '^%&!5#2%^';
            }

        }
    }
    else if ($type == 'GetGeofences') {
        //Gets dropdown values for creating an event (sites, sub events, etc)
        $users = $fvmdb->query("
            SELECT u.emma_plan_id, eg.info_only
            FROM users u
            LEFT JOIN emma_groups AS eg ON u.emma_plan_id = eg.emma_plan_id AND eg.emma_group_id = '" . $group_id . "'
            WHERE u.auth = '" . $auth . "'
            ");
        if ($users->num_rows > 0) {
            $user = $users->fetch_assoc();

            $broadcastScripts = $fvmdb->query("
                select e.*
                from emma_geofence_locations e
                WHERE e.plan_id = '" . $user['emma_plan_id'] . "'   
                AND e.active = 1        
                order by e.fence_name
            ");
            while($broadcastScript = $broadcastScripts->fetch_assoc()) {
                echo $broadcastScript['fence_name'];
                echo '^%&!5#2%^';
                echo $broadcastScript['id'];
                echo '^%&!5#2%^';
            }

        }
    }
    else if ($type == 'GetNotifications')
    {
        if (empty($errors)) {
            $users = $fvmdb->query("
            SELECT u.emma_plan_id, u.id
            FROM users u
            WHERE u.auth = '" . $auth . "'
            ");
            if ($users->num_rows > 0) {
                $user = $users->fetch_assoc();


                $multi_plan = [];
                $multi_group = [];
                $selectPlans = $fvmdb->query("
                    SELECT mp.plan_id, p.name, eug.emma_group_id
                    FROM emma_plans p
                    LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id
                    LEFT JOIN emma_user_groups as eug on eug.user_id = '".$user['id']."'
                    WHERE mp.user_id = '" . $user['id'] . "'
                    AND p.date_expired > NOW()
                ");

                while($plan = $selectPlans->fetch_assoc())
                {
                    array_push($multi_plan,$plan['plan_id']);
                    array_push($multi_group,$plan['emma_group_id']);
                }
                $notifications = $fvmdb->query("
                SELECT emc.notification, emc.emma_mass_communication_id, emc.created_by_id, emc.created_date as date
                FROM communication_received_groups as crg
                JOIN emma_mass_communications as emc on emc.emma_mass_communication_id = crg.communication_id
                WHERE crg.emma_group_id IN (".implode(',',$multi_group).")
                AND emc.created_date >= NOW()- interval 20 second
                ORDER BY emc.created_date DESC
                ");




                if($notification = $notifications->fetch_assoc()) {
                    $createdByUser = $fvmdb->query("
                SELECT u.username
                FROM users as u 
                WHERE u.id = '".$notification['created_by_id']."'
                ");
                    $crUser = $createdByUser->fetch_assoc();

                    echo 'Message';
                    echo '^%&!5#2%^';
                    echo $crUser['username'];
                    echo '^%&!5#2%^';
                    echo $notification['date'];
                    echo '^%&!5#2%^';
                    echo $notification['notification'];
                    echo '^%&!5#2%^';
                    echo $notification['emma_mass_communication_id'];


                }
                else{
                    echo 'empty';
                }


            }
            else{
                echo 'ERRORUser could not be Authenticated';
            }

        }

    }
    else if ($type == 'GetSystemMessages')
    {
        if (empty($errors)) {
            $users = $fvmdb->query("
            SELECT u.emma_plan_id, u.id
            FROM users u
            WHERE u.auth = '" . $auth . "'
            ");
            if ($users->num_rows > 0) {
                $user = $users->fetch_assoc();


                $multi_plan = [];
                $multi_group = [];
                $selectPlans = $fvmdb->query("
                    SELECT mp.plan_id, p.name, eug.emma_group_id
                    FROM emma_plans p
                    LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id
                    LEFT JOIN emma_user_groups as eug on eug.user_id = '".$user['id']."'
                    WHERE mp.user_id = '" . $user['id'] . "'
                    AND p.date_expired > NOW()
                ");

                while($plan = $selectPlans->fetch_assoc())
                {
                    array_push($multi_plan,$plan['plan_id']);
                    array_push($multi_group,$plan['emma_group_id']);
                }
                $SystemMessages = $fvmdb->query("
                SELECT esm.message, esm.emma_system_message_id, esm.comments, esm.created_by_id as created_id, esm.created_date as creation_date
                FROM emma_system_message_groups as esmg
                JOIN emma_system_messages as esm on esm.emma_system_message_id = esmg.emma_system_message_id
                WHERE esmg.emma_group_id IN (".implode(',',$multi_group).")
                AND esm.created_date >= NOW()- interval 20 second
                ORDER BY esm.created_date DESC                
                ");
                if($SystemMessage = $SystemMessages->fetch_assoc())
                {
                    if($SystemMessage['message'] == 'Event Closed')
                    {

                        $createdUser = $fvmdb->query("
                        SELECT u.username
                        FROM users as u 
                        WHERE u.id = '".$SystemMessage['created_id']."'
                        ");
                        $creUser = $createdUser->fetch_assoc();


                        //We don't need to tell the client that an event was created like this, it does it a better way already.
                        echo "Event Closed";
                        echo '^%&!5#2%^';
                        echo $creUser['username'];
                        echo '^%&!5#2%^';
                        echo $SystemMessage['creation_date'];
                        echo '^%&!5#2%^';
                        echo $SystemMessage['comments'];
                        echo '^%&!5#2%^';
                        echo $SystemMessage['emma_system_message_id'];


                    }
                    else if($SystemMessage['message'] == 'Event Broadcasted')
                    {
                        $createdUser = $fvmdb->query("
                        SELECT u.username
                        FROM users as u 
                        WHERE u.id = '".$SystemMessage['created_id']."'
                        ");
                        $creUser = $createdUser->fetch_assoc();


                        echo "Event Broadcast";
                        echo '^%&!5#2%^';
                        echo $creUser['username'];
                        echo '^%&!5#2%^';
                        echo $SystemMessage['creation_date'];
                        echo '^%&!5#2%^';
                        echo $SystemMessage['comments'];
                        echo '^%&!5#2%^';
                        echo $SystemMessage['emma_system_message_id'];
                    }
                    else{
                        echo 'empty';
                    }
                }
                else{
                    echo 'empty';
                }
            }
            else{
                echo $errors['ERRORR'] = 'ERRORUser could not be Authenticated';
            }

        }

    }
    else if ($type == 'SendResponse')
    {
        if (empty($errors)) {
            $users = $fvmdb->query("
            SELECT u.emma_plan_id
            FROM users u
            LEFT JOIN emma_user_groups as eug on u.id = eug.user_id
            LEFT JOIN emma_groups as eg on u.emma_plan_id = eg.emma_plan_id AND eg.emma_group_id = '".$group_id."'
            WHERE u.auth = '".$auth."'
            ");
            if($users->num_rows > 0) {
                $user = $users->fetch_assoc();
                if($user['info_only'] = 0)
                {

                }
            }
            else{
                $errors['AUTH Invalid'] = 'ERRORYour account could not be authenticated!';
            }
        }
        else{
            $errors['Server Error'] = 'A server error has occurred, check network or contact us at Think-Safe.';
        }
    }
    else if ($type == 'GetUsername')
    {
        if (empty($errors)) {
            $user = $fvmdb->query("
        SELECT u.username, CONCAT(u.firstname, ' ', u.lastname) AS name
        FROM users as u 
        WHERE u.auth = '".$auth."'
        ");
            if ($user->num_rows == 1) {
                //One found, success, send back emma_plan_id
                $dataUser = $user->fetch_assoc();
                echo $dataUser['name']." "."(".$dataUser['username'].")";
            } else {
                $errors['UserInvalid'] = 'ERRORUser not found.';
                echo $errors['UserInvalid']; //Send back to console.
            }
        }
    }
    else if ($type == 'CreateSOS')
    {
        if (empty($errors)) {
            $users = $fvmdb->query("
            SELECT u.emma_plan_id, u.id
            FROM users u
            WHERE u.auth = '" . $auth . "'
            ");
            if ($users->num_rows == 1) {
                $user = $users->fetch_assoc();

                $fvmdb->query("
               INSERT INTO emma_sos
            (
    created_by_id, 
    created_date, 
    sos_date, 
    help_date, 
    help_lat, 
    help_lng, 
    pending_date, 
    pending_lat, 
    pending_lng, 
    cancelled_date, 
    cancelled_lat, 
    cancelled_lng, 
    closed_date, 
    closed_comments, 
    closed_lat, 
    closed_lng, 
    closed_by_id, 
    plan_id, 
    system_pushed)
    VALUES(
    '".$user['id']."',
    '".date("Y-m-d h:i:sa")."',
    null,
    null,
    null,
    null,
    '".date("Y-m-d h:i:sa")."',
    '".$lat."',
    '".$lng."',
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,            
    '".$user['emma_plan_id']."',
    0
    )
    ");
                echo $emmadb->insert_id; //Send back the last insert ID so we can use it later.
            }
            else{
                $error['Could not validate user'] = "ERRORCould not validate user";
            }
        }
        foreach($errors as $error)
        {
            echo $error;
        }
    }
    else if ($type == 'ActivateSOS')
    {
        if (empty($errors)) {
            $users = $fvmdb->query("
            SELECT u.emma_plan_id, u.id, u.emma_pin
            FROM users u
            WHERE u.auth = '" . $auth . "'
            ");
            if ($users->num_rows == 1) {
                $user = $users->fetch_assoc();

                if(empty($pin)) {

                    //If entered pin is empty, they pressed the button and we need to activate the SOS by default.
                    //$url = 'https://tsdemos.com/mobileapi/emma/update_sos.php';
                    $url = 'https://tsdemos.com/mobileapi/emma/update_sos.php';
                    $fields = array(
                        "a" => $auth,
                        "id" => $sos_id,
                        "lat" => $lat,
                        "lng" => $lng
                    );

                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($ch, CURLOPT_ENCODING, "");

                    header('Content-Type: text/html');
                    $var = curl_exec($ch);

                    // $data['var'] = json_decode($var);
                    //$data['success'] = empty($errors);
                    echo 'activated';

                }
                if(!empty($pin))
                {
                    //We need to check to see if the pin entered matches the user pin, if yes then cancel, if no then activate
                    if($pin != $user['emma_pin'])
                    {
                        //activate
                        $url = 'https://tsdemos.com/mobileapi/emma/update_sos.php';
                        $fields = array(
                            "a" => $auth,
                            "id" => $sos_id,
                            "lat" => $lat,
                            "lng" => $lng
                        );

                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                        curl_setopt($ch, CURLOPT_ENCODING, "");

                        header('Content-Type: text/html');
                        $var = curl_exec($ch);
                        // $data['var'] = json_decode($var);
                        //$data['success'] = empty($errors);

                        echo 'activated';

                    }
                    else if($pin == $user['emma_pin'])
                    {
                        //deactivate

                        $fvmdb->query("
                            UPDATE emma_sos
                            SET cancelled_date = '".date('Y/m/d H:i:s')."', cancelled_lat = '".$lat."', cancelled_lng = '".$lng."' 
                            WHERE emma_sos_id = '".$sos_id."'
                            ");

                        echo 'deactivated';
                    }
                }
            }
            else{
                $error['Could not validate user'] = "ERRORCould not validate user";
            }
        }
        foreach($errors as $error)
        {
            echo $error;
        }
    }
    else if ($type == 'CheckVersions')
    {
        $versionCheck = $fvmdb->query("
        SELECT MAX(epv.version) as version
        FROM emma_pc_versions as epv
        WHERE epv.platform = '".$platform."'                
        ");
        echo $versionCheck->fetch_assoc()['version'];
    }
    else if ($type == 'LogUser')
    {
        //Add one active user to the DB table.
        $insert = $fvmdb->query("
        UPDATE emma_pc_versions
        SET active_users = active_users + 1
        WHERE version = '".$version."'
        AND platform = '".$platform."'
        ");
        if($insert)
        {
            $checkRecords = $fvmdb->query("
            SELECT u.id as userid,epu.*
            FROM users as u
            RIGHT JOIN emma_pc_users as epu on epu.userid = u.id 
            WHERE u.auth = '".$auth."'
            ");

            if($checkRecords->num_rows == 0)
            {
                $getUserInfo = $fvmdb->query("
                SELECT u.id, u.emma_plan_id
                FROM users as u 
                WHERE u.auth = '".$auth."'
                ");
                $UserInfo = $getUserInfo->fetch_assoc();
                //Record does not exist yet in emma_pc_users
                $insert = $fvmdb->query("
                INSERT INTO emma_pc_users (version, platform, userid, planid) VALUES (
                '".$version."',
                '".$platform."',
                '".$UserInfo['id']."',
                '".$UserInfo['emma_plan_id']."'
                )
                ");
            }
            else{
                //Update the existing row with new information.
                $records = $checkRecords->fetch_assoc();
                $updateRecords = $fvmdb->query("
                UPDATE emma_pc_users
                SET version = '".$version."', platform = '".$platform."'
                WHERE userid = '".$records['userid']."'
                ");
            }


        }
        else{
            $errors['Insert'] = 'ERRORCould not count User.';
        }

        foreach($errors as $error)
        {
            echo $error;
        }

    }
    else if ($type == 'DeLogUser')
    {
        //Detract one user from the DB table.
        $subtract = $fvmdb->query("
        UPDATE emma_pc_versions
        SET active_users = active_users - 1
        WHERE version = '".$version."'
        AND platform = '".$platform."'
        ");

        if($subtract)
        {
            echo 'success';
        }
        else{
            $errors['Subtract'] = 'ERRORCould not count User.';
        }

        foreach($errors as $error)
        {
            echo $error;
        }
    }
    else if ($type == 'checkSOS')
    {
        $users = $fvmdb->query("
        SELECT u.id
        FROM users as u 
        WHERE u.auth = '".$auth."'
        ");
        if($users->num_rows > 0)
        {

            $user = $users->fetch_assoc();
            //check for active SOS's

            $sosQuery = $fvmdb->query("
                    SELECT es.help_lat, es.help_lng
                    FROM emma_sos as es
                    WHERE es.created_by_id = '".$user['id']."'
                   AND es.help_date is not null
    AND es.closed_date is null
    AND es.cancelled_date is null
                    ");

            if($sosQuery->num_rows == 0)
            {
                echo 'empty';
            }
            else{
                $sos = $sosQuery->fetch_assoc();
                echo 'active';
                echo ',';
                echo $sos['help_lat'];
                echo ',';
                echo $sos['help_lng'];
            }
        }
        else{
            $errors['Auth'] = 'ERRORCould not validate user.';
        }



    } //Checking if they already made an SOS or if one exists already.
    else if ($type == 'recieveSOS')
    {
        $users = $fvmdb->query("
        SELECT u.id, u.emma_plan_id
        FROM users AS u 
        WHERE u.auth = '".$auth."'
        ");
        if($users->num_rows > 0) {
            $user = $users->fetch_assoc();

            $multi_plan = [];
            $multi_group = [];
            $selectPlans = $fvmdb->query("
                    SELECT mp.plan_id, p.name, eug.emma_group_id
                    FROM emma_plans p
                    LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id
                    LEFT JOIN emma_user_groups AS eug ON eug.user_id = '" . $user['id'] . "'
                    WHERE mp.user_id = '" . $user['id'] . "'
                    AND p.date_expired > NOW()
                ");

            while ($plan = $selectPlans->fetch_assoc()) {
                array_push($multi_plan, $plan['plan_id']);
                array_push($multi_group, $plan['emma_group_id']);
            }
            $canReceiveSOS = false;

            $checkCanReceive = $fvmdb->query("
            SELECT eg.emma_sos, eg.admin
            FROM emma_groups as eg
            WHERE eg.emma_group_id IN (" . implode(',', $multi_group) . ") 
            ");
            while ($canrecieve = $checkCanReceive->fetch_assoc()) {
                if ($canrecieve['emma_sos'] == 1 || $canrecieve['admin'] == 1) {
                    $canReceiveSOS = true;
                }
            }

            if ($canReceiveSOS) {
                $userIDs_array = [];
                $sosUSERplans = $fvmdb->query("
            SELECT u.id
            FROM users as u
            WHERE u.emma_plan_id IN (" . implode(',', $multi_plan) . ")
            ");

                while ($usID = $sosUSERplans->fetch_assoc()) {
                    array_push($userIDs_array, $usID['id']);
                }


                $sosMessage = $fvmdb->query("
            SELECT es.help_lat,es.help_lng,es.help_date,es.created_by_id, es.emma_sos_id
            FROM emma_sos AS es   
            WHERE es.help_date >= NOW()- interval 20 second
            AND es.created_by_id IN (" . implode(',', $userIDs_array) . ")    
            ");

                if($sosMessage->num_rows > 0) {
                    $sos = $sosMessage->fetch_assoc();
                    $creatByUser = $fvmdb->query("
                SELECT u.username
                FROM users AS u
                WHERE u.id = '" . $sos['created_by_id'] . "'
                ");
                    $us = $creatByUser->fetch_assoc();
                    echo $sos['emma_sos_id'];
                    echo '^%&!5#2%^';
                    echo $sos['help_lat'];
                    echo '^%&!5#2%^';
                    echo $sos['help_lng'];
                    echo '^%&!5#2%^';
                    echo $sos['help_date'];
                    echo '^%&!5#2%^';
                    echo $us['username'];
                }
                else{
                    echo 'empty';
                }


            }
            else{
                echo 'no_privilege';
            }
        }
        else{
            $errors['auth'] = 'ERRORCannot validate user';
            foreach ($errors as $error) {
                echo $error;
            }
        }

    }
    else if ($type == 'recieveGeofence')
    {
        $users = $fvmdb->query("
        SELECT u.id, u.emma_plan_id
        FROM users AS u 
        WHERE u.auth = '".$auth."'
        ");
        if($users->num_rows > 0) {
            $user = $users->fetch_assoc();

            $getGeomessages = $fvmdb->query("
            SELECT egl.*, egm.*, egm.created_date as createdDate, egm.created_by_id as created_by_person
            FROM emma_geofence_locations AS egl
            LEFT JOIN emma_geofence_messages AS egm on egm.geofence_id = egl.id
            WHERE egm.created_date >= NOW()- interval 20 second
            ");

            if($getGeomessages->num_rows > 0)
            {

                $geomessage = $getGeomessages->fetch_assoc();
                if($geomessage['plan_only'] == 1)
                {
                    //plan specific.

                    $multi_plan = [];
                    $multi_group = [];
                    $selectPlans = $fvmdb->query("
                    SELECT mp.plan_id, p.name, eug.emma_group_id
                    FROM emma_plans p
                    LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id
                    LEFT JOIN emma_user_groups AS eug ON eug.user_id = '" . $user['id'] . "'
                    WHERE mp.user_id = '" . $user['id'] . "'
                    AND p.date_expired > NOW()
                ");

                    while ($plan = $selectPlans->fetch_assoc()) {
                        array_push($multi_plan, $plan['plan_id']);
                        array_push($multi_group, $plan['emma_group_id']);
                    }

                    $getAllUsersInPlan = $fvmdb->query("
                    SELECT u.id
                    FROM users as u 
                    WHERE u.emma_plan_id IN (" . implode(',', $multi_plan) . ")
                    ");

                    $userID_array = [];
                    if($getAllUsersInPlan->num_rows > 0)
                    {
                        while($usersInPlan = $getAllUsersInPlan->fetch_assoc())
                        {
                            array_push($userID_array,$usersInPlan['id']);
                        }

                        if(in_array($user['id'],$userID_array))
                        {
                            //Can receive.
                            if ($geomessage['radius'] == 0) {
                                //rectangle


                                $geoCreatedBy = $fvmdb->query("
                    SELECT u.username
                    FROM users AS u
                    WHERE u.id = '" . $geomessage['created_by_person'] . "'
                    ");


                                $geoOwner = $geoCreatedBy->fetch_assoc();


                                $adj_n_lat = $geomessage['nw_corner_lat'];
                                $adj_w_lng = $geomessage['nw_corner_lng'];
                                $adj_s_lat = $geomessage['se_corner_lat'];
                                $adj_e_lng = $geomessage['se_corner_lng'];

                                $shortList = $fvmdb->query("
                SELECT *
                FROM emma_geofence_user_tracking
                WHERE lat < " . $adj_n_lat . " AND lat > " . $adj_s_lat . "
                AND lng < " . $adj_e_lng . " AND lng > " . $adj_w_lng . "
            ");


                                if ($lat < $adj_n_lat && $lat > $adj_s_lat && $lng < $adj_e_lng && $lng > $adj_w_lng) {
                                    //inside
                                    echo "Event";
                                    echo '^%&!5#2%^';
                                    echo $geoOwner['username'];
                                    echo '^%&!5#2%^';
                                    echo $geomessage['createdDate'];
                                    echo '^%&!5#2%^';
                                    echo $geomessage['description'];
                                    echo '^%&!5#2%^';
                                    echo $geomessage['comments'];
                                    echo '^%&!5#2%^';
                                    echo uniqid("",true);



                                } else {
                                    //outside
                                    echo 'empty';
                                }

                            } else {
                                //circle


                                $geoCreatedBy = $fvmdb->query("
                    SELECT u.username
                    FROM users AS u
                    WHERE u.id = '" . $geomessage['created_by_person'] . "'
                    ");


                                $geoOwner = $geoCreatedBy->fetch_assoc();


                                $kmRadius = $geomessage['radius'] * 1.60934;

                                $lat_adjustment = $kmRadius / 110.574;
                                $lng_adjustment = $kmRadius / (111.320 * cos(($geomessage['center_lat'] * M_PI) / (180)));


                                $distance = getdistance($lat, $lng, $geomessage['center_lat'], $geomessage['center_lng']);

                                if ($distance < $kmRadius) {
                                    //inside
                                    echo "Event";
                                    echo '^%&!5#2%^';
                                    echo $geoOwner['username'];
                                    echo '^%&!5#2%^';
                                    echo $geomessage['createdDate'];
                                    echo '^%&!5#2%^';
                                    echo $geomessage['description'];
                                    echo '^%&!5#2%^';
                                    echo $geomessage['comments'];
                                    echo '^%&!5#2%^';
                                    echo uniqid("",true);
                                } else {
                                    //outside
                                    echo 'empty';
                                }

                            }



                        }


                    }
                    else{
                        echo 'empty';
                    }









                }
                else {
                    //not plan specific.

                    if ($geomessage['radius'] == 0 || empty($geomessage['radius'])) {
                        //rectangle


                        $geoCreatedBy = $fvmdb->query("
                    SELECT u.username
                    FROM users AS u
                    WHERE u.id = '" . $geomessage['created_by_person'] . "'
                    ");


                        $geoOwner = $geoCreatedBy->fetch_assoc();


                        $adj_n_lat = $geomessage['nw_corner_lat'];
                        $adj_w_lng = $geomessage['nw_corner_lng'];
                        $adj_s_lat = $geomessage['se_corner_lat'];
                        $adj_e_lng = $geomessage['se_corner_lng'];


                        if (($lat < $adj_n_lat && $lat > $adj_s_lat) && ($lng < $adj_e_lng && $lng > $adj_w_lng)) {
                            //inside
                            echo $geomessage['description'];
                            echo '^%&!5#2%^';
                            echo $geomessage['comments'];
                            echo '^%&!5#2%^';
                            echo $geomessage['createdDate'];
                            echo '^%&!5#2%^';
                            echo $geoOwner['username'];
                        } else {
                            //outside
                            echo 'empty';
                        }

                    } else {
                        //circle


                        $geoCreatedBy = $fvmdb->query("
                    SELECT u.username
                    FROM users AS u
                    WHERE u.id = '" . $geomessage['created_by_person'] . "'
                    ");


                        $geoOwner = $geoCreatedBy->fetch_assoc();


                        $kmRadius = $geomessage['radius'] * 1.60934;

                        $lat_adjustment = $kmRadius / 110.574;
                        $lng_adjustment = $kmRadius / (111.320 * cos(($geomessage['center_lat'] * M_PI) / (180)));


                        $distance = getdistance($lat, $lng, $geomessage['center_lat'], $geomessage['center_lng']);

                        if ($distance < $kmRadius) {
                            //inside
                            echo $geomessage['description'];
                            echo '^%&!5#2%^';
                            echo $geomessage['comments'];
                            echo '^%&!5#2%^';
                            echo $geomessage['createdDate'];
                            echo '^%&!5#2%^';
                            echo $geoOwner['username'];
                        } else {
                            //outside
                            echo 'empty';
                        }

                    }
                }
            }
            else{
                echo 'empty';
            }






        }
        else{
            $errors['Missing Auth'] = 'ERRORAuth code is invalid or missing.';
            foreach ($errors as $error) {
                echo $error;
            }
        }

    }

}
else{
    foreach ($errors as $error) {
        echo $error;
    }
}

