<?php

require('../include/db.php');

$errors = array();
$data = array();

$user = $emmadb->real_escape_string($_POST['user']);
$planId = $emmadb->real_escape_string($_POST['plan']);
$lat = $emmadb->real_escape_string($_POST['lat']);
$lng = $emmadb->real_escape_string($_POST['lng']);
$drill = $emmadb->real_escape_string($_POST['drill']);

if($drill == 1){
    $senddrill = true;
}else{
    $senddrill = false;
}

$data['lat'] = $lat;
$data['lng'] = $lng;

if(empty($user)){
    $errors['invalid data'] = 'user id';
}
if(empty($planId)){
    $errors['invalid data'] = 'plan id';
}
if(empty($lat) || empty($lng)){
    $errors['invalid data'] = 'latitude / longitude';
}

if(empty($errors)) {


    $create = insert_lockdown($user, $planId, $lat, $lng, $drill);
    if (!$create) {
        $data['create'] = $create;
        $errors['creation'] = 'Error creating message';
    } else {
        $insert_id = $emmadb->insert_id;
    }



    ////////////////////////////////////////Start SmartCop Call/////////////////////////////////////////////////////////

    if($drill != "1") {
        //check if plan has rights to smartcop
        $smartcop_check = select_plan_has_smartcop($planId);
        if ($smartcop_check->num_rows > 0) {
            //create smartcop call

            //get address
//        $address = reverseGeocode($lat, $lng);
            $address_result = null;
            $address_query = select_plan_location_smartcop($planId);
            if ($address_query->num_rows > 0) {
                $address_result = $address_query->fetch_assoc();


                //query for data
                $smartcop_data_query = select_user_with_planID($user, $planId);
                if ($smartcop_data_query->num_rows > 0) {
                    $smartcop_data_result = $smartcop_data_query->fetch_assoc();

                    //build post object
//            $postData = array(
//                'lockdownID' => "'".$insert_id."'",
//                'aptLot' => '',
//                'city' => $address['city'],
//                'state' => $address['state'],
//                'zip' => $address['zip'],
//                'streetdir' => '',
//                'street' => $address['street'],
//                'streetNumber' => $address['streetnumber'],
//                'placeName' => '',
//                'complainantName' => $smartcop_data_result['fullname'],
//                'complainantPhone' => $smartcop_data_result['phone'],
//                'lat' => $lat,
//                'lng' => $lng
//            );


                    $postData = array(
                        'lockdownID' => $insert_id . '',
                        'aptLot' => '',
                        'city' => $address_result['city'],
                        'state' => $address_result['state'],
                        'zip' => $address_result['zip'],
                        'streetdir' => $address_result['street_direction'],
                        'street' => $address_result['street_name'],
                        'streetNumber' => $address_result['street_number'],
                        'placeName' => $address_result['location_name'],
                        'complainantName' => $smartcop_data_result['fullname'],
                        'complainantPhone' => $smartcop_data_result['phone'],
                        'lat' => $lat,
                        'lng' => $lng
                    );

                    error_log('smartcop result'.json_encode($postData));

                    //make call
                    $ch = curl_init('http://3.142.177.208:8081/PanicCall');
                    curl_setopt_array($ch, array(
                        CURLOPT_POST => TRUE,
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HTTPHEADER => array(
                            'Content-Type: application/json'
                        ),
                        CURLOPT_POSTFIELDS => json_encode($postData)
                    ));
                    $response = curl_exec($ch);

//                    $data['smartcop_response'] = json_decode($response);
                    //check response for a successful call
                }

            } else {
                error_log('failed to get plan location: create lockdown smartcop');
            }
        }
    }


    /////////////////////////////////////////End SmartCop Call//////////////////////////////////////////////////////////













    $data['notification_id'] = $insert_id;

    $groupList = array();
    $groupsQuery = select_lockdown_groups($planId);
    if ($groupsQuery->num_rows > 0) {
        while ($groupResult = $groupsQuery->fetch_assoc()) {
            $groupList[] = $groupResult['groupID'];
            $users = select_unreguser_with_group($groupResult['groupID']);
            while($user = $users->fetch_assoc()) {
                $data['userquery'][] = $user;
                $numbersarray[] = $user['phone'];
            }
        }
    }

    $data['numbers'] = $numbersarray;

    if($drill == "1"){
        $notificationQuery = select_lockdown_drill_notification($planId);
        if ($notificationQuery->num_rows > 0) {
            $notificationResult = $notificationQuery->fetch_assoc();
            $notification = $notificationResult['notification'];
            $data['notification'] = $notification;
        } else {
            $errors['notification'] = 'No notification';
        }
        $eventName = 'Lockdown Drill';
    }
    else {
        $notificationQuery = select_lockdown_notification($planId);
        if ($notificationQuery->num_rows > 0) {
            $notificationResult = $notificationQuery->fetch_assoc();
            $notification = $notificationResult['notification'];
            $data['notification'] = $notification;
        } else {
            $errors['notification'] = 'No notification';
        }

        $eventName = 'Lockdown';
    }


    /////////////////////////////////////////////////////////////////////////


    $FcmServerKey = 'AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db';
    $topics = '/topics/emma-' . $planId;
    $IOSTopics = array();


    if (empty($errors)) {

        if($drill == 1) {
            $postData = array(
                'to' => $topics,
                'time_to_live' => 900,
                'content_available' => true,
                'data' => array(
                    'GROUP_ALERT' => 'true',
                    'LOCKDOWN_DRILL' => true,
                    'Lockdown_id' => $insert_id,
                    'event' => $eventName,
                    'description' => $notification,
                    'sound' => true,
                    'vibrate' => true,
                    'groups' => array(),
                    'DRILL' => $senddrill,
                )
            );
        }
        else{
            $postData = array(
                'to' => $topics,
                'time_to_live' => 900,
                'content_available' => true,
                'data' => array(
                    'GROUP_ALERT' => 'true',
                    'LOCKDOWN' => true,
                    'Lockdown_id' => $insert_id,
                    'event' => $eventName,
                    'description' => $notification,
                    'sound' => true,
                    'vibrate' => true,
                    'groups' => array(),
                    'DRILL' => $senddrill,
                )
            );
        }

        if (count($groupList) > 0) {
            foreach ($groupList as $groupId) {
                $postData['data']['groups'][] = $groupId;
                $IOSTopics[] = "'emma-plan" . $planId . "-" . $groupId . "' in topics";
            }
        }


        $ch = curl_init('https://fcm.googleapis.com/fcm/send');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Authorization: key=' . $FcmServerKey,
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        $response = curl_exec($ch);
        $responseData = json_decode($response);
        $data['response'] = $responseData;

        /////////////////////////////
        //    IOS NOTIFICATIONS    //
        /////////////////////////////

        $IOSTopicsString = $IOSTopics[0];
        if (count($IOSTopics) > 0) {
            for ($i = 1; ($i < 5 && $i < count($IOSTopics)); $i++) {
                $IOSTopicsString .= ' || ' . $IOSTopics[$i];
            }
        }

        $data['iosTopicString'] = $IOSTopicsString;
        $data['iosTopics'] = $IOSTopics;

        if($drill == 1) {
            $postIOSData = array(
                'condition' => $IOSTopicsString,
                'time_to_live' => 600,
                'priority' => 'high',
                'notification' => array(
                    'title' => "Lockdown",
                    'body' => $notification,
                    'sound' => 'notification.aiff',
                    'badge' => '1'
                ),
                'data' => array(
                    'GROUP_ALERT' => 'true',
                    'event' => $eventName,
                    'lockdown_id' => $insert_id,
                    'description' => $notification,
                    'LOCKDOWN_DRILL' => true,
                    'DRILL' => $senddrill,
                )
            );
        }
        else{
            $postIOSData = array(
                'condition' => $IOSTopicsString,
                'time_to_live' => 600,
                'priority' => 'high',
                'notification' => array(
                    'title' => "Lockdown",
                    'body' => $notification,
                    'sound' => 'notification.aiff',
                    'badge' => '1'
                ),
                'data' => array(
                    'GROUP_ALERT' => 'true',
                    'event' => $eventName,
                    'lockdown_id' => $insert_id,
                    'description' => $notification,
                    'LOCKDOWN' => true,
                    'DRILL' => $senddrill,
                )
            );
        }

        $ch = curl_init('https://fcm.googleapis.com/fcm/send');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Authorization: key=' . $FcmServerKey,
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postIOSData)
        ));
        $IOSresponse = curl_exec($ch);

        if (count($IOSTopics) > 5) {
            $m = 5;
            while ($m < count($IOSTopics)) {
                for ($i = $m; ($i < ($m + 5) && $i < count($IOSTopics)); $i++) {
                    if ($i == $m) {
                        $IOSTopicsString = $IOSTopics[$i];
                    } else {
                        $IOSTopicsString .= ' || ' . $IOSTopics[$i];
                    }
                }

                $postIOSData2 = array(
                    'condition' => $IOSTopicsString,
                    'time_to_live' => 600,
                    'priority' => 'high',
                    'notification' => array(
                        'title' => "Lockdown",
                        'body' => $notification,
                        'sound' => 'notification.aiff',
                        'badge' => '1'
                    ),
                    'data' => $postIOSData['data']
                );

                $ch = curl_init('https://fcm.googleapis.com/fcm/send');
                curl_setopt_array($ch, array(
                    CURLOPT_POST => TRUE,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HTTPHEADER => array(
                        'Authorization: key=' . $FcmServerKey,
                        'Content-Type: application/json'
                    ),
                    CURLOPT_POSTFIELDS => json_encode($postIOSData2)
                ));
                $IOSresponse2 = curl_exec($ch);

                $m += 5;
            }
        }


    }
}

$data['success'] = empty($errors);
$data['errors'] = $errors;


$data['groups'] = $groupList;
$data['plan'] = $planId;
$data['post'] = $_POST;

echo json_encode($data);