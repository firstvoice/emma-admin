<?php


include('../include/db.php');
include('../include/processing.php');
include('../queries/insert.php');

$errors = array();
$data = array();

//Plan Tab
$planName = $fvmdb->real_escape_string($_POST['plan-name']);
$dateActive = $fvmdb->real_escape_string($_POST['date-active']);
$dateExpired = $fvmdb->real_escape_string($_POST['date-expired']);
$massCommunication = $fvmdb->real_escape_string($_POST['masscommunication']);
$securities = $fvmdb->real_escape_string($_POST['securities']);
$geofences = $fvmdb->real_escape_string($_POST['geofences']);
$maxgeo = $fvmdb->real_escape_string($_POST['maxgeo']);
$panic = $fvmdb->real_escape_string($_POST['panic']);
$duration = $fvmdb->real_escape_string($_POST['sosduration']);
$maxSites = $fvmdb->real_escape_string($_POST['max']);
$userId = $fvmdb->real_escape_string($_POST['user']);

$newPlanId = insert_new_plan($planName,$dateActive,$dateExpired,$maxSites,$massCommunication,$securities,$geofences,$panic,$duration,$maxgeo);
if($newPlanId == 'error'){
    $errors['plan'] = 'Error Inserting Plan';
}
if(empty($errors)) {
    $siteName = $_POST['site-name'];
    $siteAddress = $_POST['site-address'];
    $siteCity = $_POST['site-city'];
    $siteState = $_POST['site-state'];
    $siteZip = $_POST['site-zip'];
    $siteFirst = $_POST['site-first'];
    $siteLast = $_POST['site-last'];
    $siteEmail = $_POST['site-email'];
    $sitePhone = $_POST['site-phone'];

    $newSites = insert_plan_sites($newPlanId, $siteName, $siteAddress, $siteCity, $siteState, $siteZip, $siteFirst, $siteLast, $siteEmail, $sitePhone);
    if($newSites == 'error'){
        $errors['sites'] = 'Error Inserting Sites';
    }
    if(empty($errors)) {
        $groupName = $_POST['group-name'];
        $groupAdmin = $_POST['group-admin'];
        $groupInfo = $_POST['group-info'];
        $groupSecurity = $_POST['group-security'];
        $groupGeofences = $_POST['group-geofences'];
        $groupSOS = $_POST['group-esos'];
        $group911 = $_POST['group-admin911'];

        $newGroup = insert_plan_groups($newPlanId, $groupName, $groupInfo, $groupAdmin, $groupSecurity, $groupGeofences, $groupSOS, $group911);
        if($newGroup == 'error'){
            $errors['groups'] = 'Error Inserting Groups';
        }
        if(empty($errors)) {
            $planAssets = $_POST['assets'];
            $planAssetsLat = $_POST['assets-lat'];
            $planAssetsLon = $_POST['assets-lon'];

            insert_plan_assets($newPlanId, $planAssets, $planAssetsLat, $planAssetsLon, $userId);

            $planEmergencies = $_POST['emergencies'];

            insert_plan_emergencies($newPlanId, $planEmergencies);
        }
    }
}



$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);