<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/23/2017
 * Time: 12:16 PM
 */

require('../include/db.php');
include('../include/processing.php');
include('swiftmailer/swift_required.php');

$data = array();
$errors = array();

$data['test'] = "test";

$userId = $fvmdb->real_escape_string($_POST['user-id']);
$planId = $fvmdb->real_escape_string($_POST['plan-id']);
$notification = $fvmdb->real_escape_string($_POST['notification']);
$pin = $fvmdb->real_escape_string($_POST['pin']);
$groups = $_POST['groups'];
$multiplans = $_POST['mass-notification-multiplan'];
$text = $fvmdb->real_escape_string($_POST['alert-text']);
$email = $fvmdb->real_escape_string($_POST['alert-email']);
$responsetime = $fvmdb->real_escape_string($_POST['mass-notification-responses-amount']);
$responses = $fvmdb->real_escape_string($_POST['mass-notification-responses']);
if($responses == 'on'){
    $responses = 1;
}else{
    $responses = 0;
}
if($text == 'on'){
    $data['text'] = true;
}
$numbersarray = array();


$desktopConnection = $fvmdb->real_escape_string($_POST['desktopConnection']);  //This is so we can differentiate between someone using the download client vs the web browser.
$auth = $fvmdb->real_escape_string($_POST['auth']); //This is the auth code that the downloadable client sends. For security the userId is not saved so we go by auth code.

if(empty($desktopConnection)){
    $groups = $_POST['group'];
}else{
    $groups = array();
}

if(empty($groups)){
    $errors['groups'] = 'Groups are required';
}

if(empty($desktopConnection)) {
    //Web user
    $users = select_user_from_userID($userId);
//    $fvmdb->query("
//    SELECT *
//    FROM users
//    WHERE id = '" . $userId . "'
//");
    if (!$user = $users->fetch_assoc()) {
        $errors['user'] = 'Can not find user';
    } else {
        $FcmServerKey = 'AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db';
        $topics = '/topics/emma-' . $user['emma_plan_id'];
    }
}
else{
    //Desktop standalone client user
    if(empty($auth))
    {
        $errors['Auth Empty'] = "ERROR You did not provide an auth code";
    }
    else {
        $group_id = $_POST['group_id'];

        $users = select_user_from_auth_pin_and_groupID($auth, $pin, $group_id);
        $data['userRowsEcho'] = $users->num_rows;
////        $fvmdb->query("
//        SELECT DISTINCT u.*, eg.info_only
//        FROM users as u
//        LEFT JOIN emma_user_groups as eug on u.id = eug.user_id
//        LEFT JOIN emma_groups as eg on u.emma_plan_id = eg.emma_plan_id AND eg.emma_group_id = '".$group_id."'
//        WHERE u.auth = '" . $auth . "'
//        AND u.emma_pin = '". $pin ."'
//    ");
        if($users->num_rows > 1 || $users->num_rows == 0) {
            echo $users->num_rows;
            $errors['user'] = 'ERRORCould not validate user';
        }
        else {
            $user = $users->fetch_assoc();
            if($user['info_only'] == 0) {
                $FcmServerKey = 'AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db';
                $topics = '/topics/emma-' . $user['emma_plan_id'];

                $planId = $user['emma_plan_id'];
                $userId = $user['id'];

                //Multiplan
                $multi_plan = [];
                $multi_group = [];
                $selectPlans = select_multiPlans_with_userID($userId);
//                $fvmdb->query("
//                      SELECT mp.plan_id, p.name, eug.emma_group_id
//                    FROM emma_plans p
//                    LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id
//                    LEFT JOIN emma_user_groups as eug on eug.user_id = '" . $userId . "'
//                    WHERE mp.user_id = '" . $userId . "'
//                    AND p.date_expired > NOW()
//                ");

                while ($plan = $selectPlans->fetch_assoc()) {
                    array_push($multi_plan, $plan['plan_id']);
                    array_push($multi_group,$plan['emma_group_id']);
                }

                //Get messaging authority groups
                $allgroups = select_groups_with_planArray($multi_plan);
//                $fvmdb->query("
//                                         SELECT g.*
//                                         FROM emma_groups AS g
//                                         WHERE g.emma_plan_id IN (".implode(',',$multi_plan).")
//                                         ORDER BY g.name
//                                        ");
                while ($allgroup = $allgroups->fetch_assoc()) {
                    $group_privileges = select_groupsPrivileges_with_groupArray_and_groupID($multi_group, $allgroup['emma_group_id']);
//                    $fvmdb->query("
//                            SELECT p.*
//                            FROM emma_group_privileges AS p
//                            WHERE p.emma_group_id IN (".implode(',',$multi_group).")
//                            AND p.emma_privilege_id = " .$allgroup['emma_group_id'] . "
//                        ");
                    if ($group_privileges->num_rows > 0) {
                        $privilege_list[] = $allgroup['emma_group_id'];
                        array_push($groups, $allgroup['emma_group_id']);
                    }
                }








                if (empty($groups)) {
                    $errors['EMPTYGROUP'] = "ERROR No groups found!";
                }
            }
            else{
                $errors['Info Only!'] = 'This account is not allowed to create Mass Communications.';
            }

        }
    }
    foreach($errors as $error)
    {
        echo $error;
    }
}

if (empty($errors)) {
    if(count($multiplans) > 1){



        $groupNames = array();
        $arrayKeys = array_keys($groups);
        $data['arrayKeys'] = $arrayKeys;
        for($j = 0; $j < count($groups); $j++){
            $groupQuery = select_group_with_groupID($arrayKeys[$j]);
            if($groupResult = $groupQuery->fetch_assoc()){
                $groupNames[] = $groupResult['name'];
            }
        }
        $groupString = implode("','", $groupNames);

//        $getGroups = select_createMassCommunication_getGroups($multiplans, $groupNames);



        for($i=0; $i<count($multiplans); $i++) {
            $multigroups = array();
            $mplanID = $multiplans[$i];
            $topics = '/topics/emma-' . $mplanID;



//            $data['multigroups'][] = $multigroups;
//            $data['groupString'] = $groupString;
//            $data['groupnames'] = $groupNames;
            $insertMassCommunication = insert_emma_mass_communications($multiplans[$i], $userId, $notification, $responses, $responsetime);

            $messageId = $emmadb->insert_id;
            $data['notification_id'] = $messageId;
//            if (empty($desktopConnection)) {
//                $group = null;
//                foreach ($multigroups as $group) {
//                    $insertReceivedCommunication = insert_communication_received_groups($messageId, $group);
//
//                }
//            }
            $getGroups = select_createMassCommunication_getGroups($multiplans[$i], $groupNames);


            while ($getGroup = $getGroups->fetch_assoc()) {
                $data['GroupsQuery'][] = $getGroup;
                $multigroups[] = $getGroup['emma_group_id'];
                $callusers = select_user_with_group($getGroup['emma_group_id']);
                while($calluser = $callusers->fetch_assoc()) {
                    if($calluser['phone'] !== '') {
                        $numbersarray[] = $calluser['phone'];
                    }
                    if($email == 'on'){
                        $emailmessage = $fvmdb->real_escape_string($_POST['email-notification']);
                        sendEmail($calluser['username'], $emailmessage, $planId);
                    }
                }
            }
            $data['numbers'] = $numbersarray;
            $postData = array(
                'to' => $topics,
                'time_to_live' => 900,
                'content_available' => true,
                'data' => array(
                    'GROUP_ALERT' => 'true',
                    'MASS_COMMUNICATION' => true,
                    'mass_communication_id' => $messageId,
                    'event' => 'Mass Communication',
                    'description' => $notification,
                    'sound' => true,
                    'vibrate' => true,
                    'groups' => array(),
                )
            );
            $group = null;
            foreach ($multigroups as $group) {
                $postData['data']['groups'][] = $group;
                $IOSTopics[] = "'emma-plan" . $mplanID . "-" . $group . "' in topics";
                $insertReceivedCommunication = insert_communication_received_groups($messageId, $group);
            }

            $ch = curl_init('https://fcm.googleapis.com/fcm/send');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: key=' . $FcmServerKey,
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postData)
            ));
            $response = curl_exec($ch);
            $responseData = json_decode($response);
            $data['response'] = $responseData;
            //error_log("mass notifiction event test: ". json_encode($responseData));

        }
            /////////////////////////////
            //    IOS NOTIFICATIONS    //
            /////////////////////////////

            $IOSTopicsString = $IOSTopics[0];
            if (count($IOSTopics) > 0) {
                for ($k = 1; ($k < 5 && $k < count($IOSTopics)); $k++) {
                    $IOSTopicsString .= ' || ' . $IOSTopics[$k];
                }
            }

            $data['iosTopicString'] = $IOSTopicsString;
            $data['iosTopics'] = $IOSTopics;

            $postIOSData = array(
                'condition' => $IOSTopicsString,
                'time_to_live' => 600,
                'priority' => 'high',
                'notification' => array(
                    'title' => $notification,
                    'sound' => 'notification.aiff',
                    'badge' => '1'
                ),
                'data' => array(
                    'GROUP_ALERT' => 'true',
                    'event' => 'Mass Communication',
                    'mass_communication_id' => $messageId,
                    'description' => $notification,
                    'MASS_COMMUNICATION' => true,
                )
            );

            $ch = curl_init('https://fcm.googleapis.com/fcm/send');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: key=' . $FcmServerKey,
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postIOSData)
            ));
            $IOSresponse = curl_exec($ch);

            if (count($IOSTopics) > 5) {
                $m = 5;
                while ($m < count($IOSTopics)) {
                    for ($l = $m; ($l < ($m + 5) && $l < count($IOSTopics)); $l++) {
                        if ($l == $m) {
                            $IOSTopicsString = $IOSTopics[$l];
                        } else {
                            $IOSTopicsString .= ' || ' . $IOSTopics[$l];
                        }
                    }

                    $postIOSData2 = array(
//        'to' => $IOSTopicsString,
                        'condition' => $IOSTopicsString,
                        'time_to_live' => 600,
                        'priority' => 'high',
                        'notification' => array(
                            'title' => $notification,
                            'sound' => 'notification.aiff',
                            'badge' => '1'
                        ),
                        'data' => $postIOSData['data']
                    );



                    $ch = curl_init('https://fcm.googleapis.com/fcm/send');
                    curl_setopt_array($ch, array(
                        CURLOPT_POST => TRUE,
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HTTPHEADER => array(
                            'Authorization: key=' . $FcmServerKey,
                            'Content-Type: application/json'
                        ),
                        CURLOPT_POSTFIELDS => json_encode($postIOSData2)
                    ));
                    $IOSresponse2 = curl_exec($ch);

                    $m += 5;
                }
            }


    }
    else {
        $insertMassCommunication = insert_emma_mass_communications($planId, $userId, $notification,$responses, $responsetime);
//        $fvmdb->query("
//            INSERT INTO emma_mass_communications (
//              emma_plan_id,
//              created_by_id,
//              created_date,
//              notification
//            ) VALUES (
//              '" . $planId . "',
//              '" . $userId . "',
//              '" . date('Y-m-d H:i:s') . "',
//              '" . $notification . "'
//            )
//        ");
        $messageId = $emmadb->insert_id;
        $data['notification_id'] = $messageId;
    if(empty($desktopConnection)) {
        foreach ($groups as $group) {


            $insertReceivedCommunication = insert_communication_received_groups($messageId, $group);

            $callusers = select_user_with_group($group);
            while($calluser = $callusers->fetch_assoc()) {
                $data['userquery'][] = $calluser;
                if($calluser['phone'] !== '') {
                    $numbersarray[] = $calluser['phone'];
                }
                if($email == 'on'){
                    $emailmessage = $fvmdb->real_escape_string($_POST['email-notification']);
                    sendEmail($calluser['username'], $emailmessage, $planId);
                }
            }
//            $fvmdb->query("
//    INSERT INTO communication_received_groups(
//    communication_id, emma_group_id)values(
//    '" . $messageId . "',
//   '" . $group . "'
//    )
//    ");
        }
    }
    $data['numbers'] = $numbersarray;

        $postData = array(
            'to' => $topics,
            'time_to_live' => 900,
            'content_available' => true,
            'data' => array(
                'GROUP_ALERT' => 'true',
                'MASS_COMMUNICATION' => true,
                'mass_communication_id' => $messageId,
                'event' => 'Mass Communication',
                'description' => $notification,
                'sound' => true,
                'vibrate' => true,
                'groups' => array(),
            )
        );
        foreach ($groups as $group) {
            $postData['data']['groups'][] = $group;
            $IOSTopics[] = "'emma-plan" . $user['emma_plan_id'] . "-" . $group . "' in topics";
        }

        $ch = curl_init('https://fcm.googleapis.com/fcm/send');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(

                'Authorization: key=' . $FcmServerKey,
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        $response = curl_exec($ch);
        $responseData = json_decode($response);
        $data['response'] = $responseData;
        //error_log("mass notifiction event test: ". json_encode($responseData));
        /////////////////////////////
        //    IOS NOTIFICATIONS    //
        /////////////////////////////

        $IOSTopicsString = $IOSTopics[0];
        if (count($IOSTopics) > 0) {
            for ($i = 1; ($i < 5 && $i < count($IOSTopics)); $i++) {
                $IOSTopicsString .= ' || ' . $IOSTopics[$i];
            }
        }

        $data['iosTopicString'] = $IOSTopicsString;
        $data['iosTopics'] = $IOSTopics;

        $postIOSData = array(
            'condition' => $IOSTopicsString,
            'time_to_live' => 600,
            'priority' => 'high',
            'notification' => array(
                'title' => $notification,
                'sound' => 'notification.aiff',
                'badge' => '1'
            ),
            'data' => array(
                'GROUP_ALERT' => 'true',
                'event' => 'Mass Communication',
                'mass_communication_id' => $messageId,
                'description' => $notification,
                'MASS_COMMUNICATION' => true,
            )
        );

        $ch = curl_init('https://fcm.googleapis.com/fcm/send');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Authorization: key=' . $FcmServerKey,
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postIOSData)
        ));
        $IOSresponse = curl_exec($ch);
        if (count($IOSTopics) > 5) {
            $m = 5;
            while ($m < count($IOSTopics)) {
                for ($i = $m; ($i < ($m + 5) && $i < count($IOSTopics)); $i++) {
                    if ($i == $m) {
                        $IOSTopicsString = $IOSTopics[$i];
                    } else {
                        $IOSTopicsString .= ' || ' . $IOSTopics[$i];
                    }
                }

                $postIOSData2 = array(
//        'to' => $IOSTopicsString,
                    'condition' => $IOSTopicsString,
                    'time_to_live' => 600,
                    'priority' => 'high',
                    'notification' => array(
                        'title' => $notification,
                        'sound' => 'notification.aiff',
                        'badge' => '1'
                    ),
                    'data' => $postIOSData['data']
                );

                $ch = curl_init('https://fcm.googleapis.com/fcm/send');
                curl_setopt_array($ch, array(
                    CURLOPT_POST => TRUE,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HTTPHEADER => array(
                        'Authorization: key=' . $FcmServerKey,
                        'Content-Type: application/json'
                    ),
                    CURLOPT_POSTFIELDS => json_encode($postIOSData2),
                ));
                $IOSresponse2 = curl_exec($ch);

                $m += 5;
            }
        }
    }
}


$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
//error_log("testing Data Check:". json_encode($data));
if(empty($desktopConnection)){
    echo json_encode($data);
}

error_log("testing error array:". json_encode($errors));

function sendEmail($username, $emailMessage, $planID){
    global $SMT;

//    $notifications = select_massCommunication_with_id($notificationId);
//    $notification = $notifications->fetch_assoc();
    $planquery = select_plan_from_planID($planID);
    $planResult = $planquery->fetch_assoc();


    $to = $username;
    $data['EmailToList'][] = $to;
    $subject = 'EMMA Alert';

    $body = '<html><head></head><body style="color:#000001;">
                           
                        <h2>Mass Notification</h2>
                        <hr>
                        <div>Plan: '.$planResult['name'].'</div>
                        <div>Details: '.$emailMessage.'</div>
						</body></html>';



    try{
        // Create the Mailer using your created Transport
        $mailer = Swift_Mailer::newInstance($SMT);
        $message = Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom(array(FROMEMAIL => PROGRAM))
            ->setTo(array($to))
            ->setBody($body, 'text/html');
        $result = $mailer->send($message);
        if(!$result){
            error_log('Failed to sent email to: '. $to);
        }
        $mailer->getTransport()->stop();

    }
    catch(Swift_TransportException $e){
        error_log('Swiftmailer transport exception thrown on email of create_event.php, sending message to: '.$to);
        error_log($e->getMessage());
    }
    catch(Swift_IoException $e){
        error_log('Swiftmailer IO exception thrown on email of create_event.php, sending message to: '.$to);
        error_log($e->getMessage());
    }
    catch(Exception $e){
        error_log('Exception thrown on email of create_event.php, sending message to: '.$to);
        error_log($e->getMessage());
    }

}

?>

