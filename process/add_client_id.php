<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/19/2017
 * Time: 9:40 AM
 */

include('../include/db.php');

$errors = array();
$data = array();

$subscription = $fvmdb->real_escape_string($_POST['subscription']);
$user_id = $fvmdb->real_escape_string($_POST['user']);

if (empty($subscription)) {
    $errors['subscription'] = 'empty subsciption (client) id';
}
if (empty($user_id)) {
    $errors['user'] = 'empty user id';
}

if (empty($errors)) {
    $plan_id = '';
    $plans = select_basePlanID_with_userID($user_id);
    if ($plans->num_rows > 0) {
        $plan = $plans->fetch_assoc();
        $plan_id = $plan['emma_plan_id'];
        $category = 'web-plan-' . $plan_id;
    }
    else{
        $errors[] = "Could not find plan";
    }
}
if(empty($errors)){
//    $FcmServerKey = 'AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db';

//    $ch = curl_init('https://iid.googleapis.com/iid/v1/'.$subscription.'/rel/topics/'.$topic);
//    curl_setopt_array($ch, array(
//        CURLOPT_POST => TRUE,
//        CURLOPT_RETURNTRANSFER => TRUE,
//        CURLOPT_HTTPHEADER => array(
//            'Authorization: key=' . $FcmServerKey,
//            'Content-Type: application/json',
//            'Content-Length: '. 0 ,
//        )
//    ));
//    $valid = curl_exec($ch);


  $insert = insert_emma_notification_clients($subscription, $user_id,$category);
    if (!$insert) {
        $errors[] = 'update failed';
        $errors['sql'] = $fvmdb->error;
    }
}
//$data['valid'] = $valid;
//$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);