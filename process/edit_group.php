<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/19/2017
 * Time: 9:40 AM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$groupId = $fvmdb->real_escape_string($_POST['id']);
$editId = $fvmdb->real_escape_string($_POST['edit-id']);
$groups = $_POST['groups'];
$feeds = $_POST['groupfeeds'];
$icons = $_POST['icons'];
$eventInfoOnlySwitch = $_POST['event-info-only'];
$administrator = $_POST['administrator'];
$security = $_POST['security'];
$sos = $_POST['sos'];
$admin911 = $_POST['911admin'];
$receivelockdown = $_POST['receivelockdown'];
$planId = $fvmdb->real_escape_string($_POST['plan-id']);

if (empty($groupId)) {
  $errors['group-id'] = 'No Group Id';
}

$oldGroups = select_group_with_groupID($groupId);
//$fvmdb->query("
//  select *
//  from emma_groups
//  where emma_group_id = '" . $groupId . "'
//");
if (!$oldGroup = $oldGroups->fetch_assoc()) {
  $errors[] = 'can not find group';
}

if (empty($errors)) {
  $updateGroup = update_group_with_groupID($groupId, (isset($eventInfoOnlySwitch) ? "1" : "0"), (isset($administrator) ? "1" : "0"),
      (isset($security) ? "1" : "0"), (isset($sos) ? "1" : "0"), (isset($admin911) ? "1" : "0"));
//      $fvmdb->query("
//    update emma_groups
//    set info_only = " . (isset($eventInfoOnlySwitch) ? "1" : "0") . ",
//        admin = " . (isset($administrator) ? "1" : "0") . ",
//        security = " . (isset($security) ? "1" : "0") . ",
//        emma_sos = " . (isset($sos) ? "1" : "0") . ",
//        911admin = " . (isset($admin911) ? "1" : "0") . "
//    where emma_group_id = '" . $groupId . "'
//  ");

  $saveHistory = insert_emma_edit_history_groups($groupId, $oldGroup['name'], $oldGroup['info_only'], $oldGroup['admin'],
      $oldGroup['security'], $oldGroup['emma_sos'], $oldGroup['911admin'], $editId, date('Y-m-d H:i:s'));
//      $fvmdb->query("
//    insert into emma_edit_history_groups (
//      emma_group_id,
//      name,
//      info_only,
//      admin,
//      security,
//      emma_sos,
//      911admin,
//      edit_by_id,
//      edit_date
//    ) VALUES (
//      '" . $groupId . "',
//      '" . $oldGroup['name'] . "',
//      " . $oldGroup['info_only'] . ",
//      " . $oldGroup['admin'] . ",
//      " . $oldGroup['security'] . ",
//      " . $oldGroup['emma_sos'] . ",
//      " . $oldGroup['911admin'] . ",
//      '" . $editId . "',
//      '" . date('Y-m-d H:i:s') . "'
//    )
//  ");
  $saveHistoryId = $emmadb->insert_id;

  if(isset($receivelockdown)){
      insert_lockdown_permission($groupId,$planId);
  }
  else{
      $lockdownperm = select_group_lockdown_permission($groupId);
      if($lockdownperm->num_rows > 0){
          update_lockdown_permission($groupId);
      }
  }

  $oldGroupPrivileges = select_groupPrivileges_with_groupID($groupId);
//      $fvmdb->query("
//    select *
//    from emma_group_privileges
//    where emma_group_id = '" . $groupId . "'
//  ");
  while ($oldGroupPrivilege = $oldGroupPrivileges->fetch_assoc()) {
    $saveOldGroupPrivilege = insert_emma_edit_history_group_privileges($saveHistoryId, $oldGroupPrivilege['emma_privilege_id']);
//        $fvmdb->query("
//      insert into emma_edit_history_group_privileges (
//        emma_edit_history_group_id,
//        emma_privilege_id
//      ) VALUES (
//        '" . $saveHistoryId . "',
//        '" . $oldGroupPrivilege['emma_privilege_id'] . "'
//      )
//    ");
  }

  $removeOldGroups = delete_emma_group_privileges($groupId);
//      $fvmdb->query("
//    DELETE FROM emma_group_privileges
//    WHERE emma_group_id = '" . $groupId . "'
//  ");
  foreach ($groups as $privilegeId) {
    $insertGroup = insert_emma_group_privileges($groupId, $privilegeId);
//        $fvmdb->query("
//      INSERT INTO emma_group_privileges (
//        emma_group_id,
//        emma_privilege_id
//      ) VALUES (
//        '" . $groupId . "',
//        '" . $privilegeId . "'
//      )
//    ");
  }
$removeOldFeeds = delete_emma_groupfeed_privileges($groupId);
foreach ($feeds as $privilegeId) {
    $insertGroup = insert_emma_groupfeed_privileges($groupId, $privilegeId);
//        $fvmdb->query("
//      INSERT INTO emma_group_privileges (
//        emma_group_id,
//        emma_privilege_id
//      ) VALUES (
//        '" . $groupId . "',
//        '" . $privilegeId . "'
//      )
//    ");
  }

  $removeOldIcons = update_emmaGroupEvents_active_with_groupID($groupId);
//      $fvmdb->query("
//    UPDATE emma_group_events
//    SET active = 0
//    WHERE emma_group_id = '". $groupId ."'
//  ");
  foreach ($icons as $icon){
      $insertIcons = insert_emma_group_events($groupId, $icon);
//          $fvmdb->query("
//        INSERT INTO emma_group_events (emma_group_id, event_id) VALUES ('". $groupId ."','". $icon ."')
//      ");
  }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);