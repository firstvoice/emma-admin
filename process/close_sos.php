<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/23/2017
 * Time: 12:16 PM
 */

require('../include/db.php');
include('../include/processing.php');

$data = array();
$errors = array();

$data = array();
$errors = array();
$userId = $fvmdb->real_escape_string($_POST['user-id']);
$sosId = $fvmdb->real_escape_string($_POST['sos-id']);
$comments = $fvmdb->real_escape_string($_POST['comments']);
$callLogSwitch = $fvmdb->real_escape_string($_POST['call-log']);

$soses = select_closeSOS_soses($sosId);
//    $fvmdb->query("
//    SELECT *
//    FROM emma_sos
//    WHERE emma_sos_id = '" . $sosId . "'
//");
if (!$sos = $soses->fetch_assoc()) {
  $errors['sos'] = 'Can not find sos entry';
}

if (empty($errors)) {
  $updateSos = update_Sos_closeSOS_with_sosID($sosId, $comments, $userId);
//  $fvmdb->query("
//    update emma_sos
//    set
//      closed_comments = '" . $comments . "',
//      closed_lat = '',
//      closed_lng = '',
//      closed_date = '" . date('Y-m-d H:i:s') . "',
//      closed_by_id = '" . $userId . "'
//    where emma_sos_id = '" . $sosId . "'
//  ");
  if (!$updateSos) {
    $errors['update'] = 'Could not update SOS: ' . $fvmdb->error;
  }

  if (isset($callLogSwitch) && $callLogSwitch == "on") {
    $reportNum = uniqid('');
    $planId = $fvmdb->real_escape_string($_POST['sos-id']);
    $officerName = $fvmdb->real_escape_string($_POST['officer-name']);
    $incidentDescription = $fvmdb->real_escape_string($_POST['incident-description']);
    $resultingAction = $fvmdb->real_escape_string($_POST['resulting-action']);

    $enterCallReport = insert_emma_sos_call_reports($sosId, $officerName, $incidentDescription, $resultingAction);
//        $fvmdb->query("
//      insert into emma_sos_call_reports(
//        emma_sos_id,
//        officer_name,
//        incident_description,
//        incident_followup
//      ) VALUES (
//        '" . $sosId . "',
//        '" . $officerName . "',
//        '" . $incidentDescription . "',
//        '" . $resultingAction . "'
//      )
//    ");
  }
}
$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);