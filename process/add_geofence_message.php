<?php

include('../include/db.php');
include('../include/processing.php');
include('swiftmailer/swift_required.php');


function getDistance( $latitude1, $longitude1, $latitude2, $longitude2 ) {
    $earth_radius = 6371;

    $dLat = deg2rad( $latitude2 - $latitude1 );
    $dLon = deg2rad( $longitude2 - $longitude1 );

    $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
    $c = 2 * asin(sqrt($a));
    $d = $earth_radius * $c;

    return $d;
}

function sendEmails($userList, $description, $comments){
    global $fvmdb;
    global $SMT;
    foreach ($userList AS $user) {
        $userid = $user['userID'];
        $selectEmail = select_email_with_userID($userid);
        $emailQuery = $selectEmail->fetch_assoc();
        $email = $emailQuery['username'];


        $bodyHTML = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"><head><link rel="stylesheet" type="text/css" href="css/app.css"><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta name="viewport" content="width=device-width"><title>Alert</title></head><body style="-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;box-sizing:border-box;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;min-width:100%;padding:0;text-align:left;width:100%!important"><style>@media only screen{html{min-height:100%;background:#f3f3f3}}@media only screen and (max-width:596px){.small-float-center{margin:0 auto!important;float:none!important;text-align:center!important}.small-text-center{text-align:center!important}.small-text-left{text-align:left!important}.small-text-right{text-align:right!important}}@media only screen and (max-width:596px){.hide-for-large{display:block!important;width:auto!important;overflow:visible!important;max-height:none!important;font-size:inherit!important;line-height:inherit!important}}@media only screen and (max-width:596px){table.body table.container .hide-for-large,table.body table.container .row.hide-for-large{display:table!important;width:100%!important}}@media only screen and (max-width:596px){table.body table.container .callout-inner.hide-for-large{display:table-cell!important;width:100%!important}}@media only screen and (max-width:596px){table.body table.container .show-for-large{display:none!important;width:0;mso-hide:all;overflow:hidden}}@media only screen and (max-width:596px){table.body img{width:auto;height:auto}table.body center{min-width:0!important}table.body .container{width:95%!important}table.body .column,table.body .columns{height:auto!important;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;padding-left:16px!important;padding-right:16px!important}table.body .column .column,table.body .column .columns,table.body .columns .column,table.body .columns .columns{padding-left:0!important;padding-right:0!important}table.body .collapse .column,table.body .collapse .columns{padding-left:0!important;padding-right:0!important}td.small-1,th.small-1{display:inline-block!important;width:8.33333%!important}td.small-2,th.small-2{display:inline-block!important;width:16.66667%!important}td.small-3,th.small-3{display:inline-block!important;width:25%!important}td.small-4,th.small-4{display:inline-block!important;width:33.33333%!important}td.small-5,th.small-5{display:inline-block!important;width:41.66667%!important}td.small-6,th.small-6{display:inline-block!important;width:50%!important}td.small-7,th.small-7{display:inline-block!important;width:58.33333%!important}td.small-8,th.small-8{display:inline-block!important;width:66.66667%!important}td.small-9,th.small-9{display:inline-block!important;width:75%!important}td.small-10,th.small-10{display:inline-block!important;width:83.33333%!important}td.small-11,th.small-11{display:inline-block!important;width:91.66667%!important}td.small-12,th.small-12{display:inline-block!important;width:100%!important}.column td.small-12,.column th.small-12,.columns td.small-12,.columns th.small-12{display:block!important;width:100%!important}table.body td.small-offset-1,table.body th.small-offset-1{margin-left:8.33333%!important;Margin-left:8.33333%!important}table.body td.small-offset-2,table.body th.small-offset-2{margin-left:16.66667%!important;Margin-left:16.66667%!important}table.body td.small-offset-3,table.body th.small-offset-3{margin-left:25%!important;Margin-left:25%!important}table.body td.small-offset-4,table.body th.small-offset-4{margin-left:33.33333%!important;Margin-left:33.33333%!important}table.body td.small-offset-5,table.body th.small-offset-5{margin-left:41.66667%!important;Margin-left:41.66667%!important}table.body td.small-offset-6,table.body th.small-offset-6{margin-left:50%!important;Margin-left:50%!important}table.body td.small-offset-7,table.body th.small-offset-7{margin-left:58.33333%!important;Margin-left:58.33333%!important}table.body td.small-offset-8,table.body th.small-offset-8{margin-left:66.66667%!important;Margin-left:66.66667%!important}table.body td.small-offset-9,table.body th.small-offset-9{margin-left:75%!important;Margin-left:75%!important}table.body td.small-offset-10,table.body th.small-offset-10{margin-left:83.33333%!important;Margin-left:83.33333%!important}table.body td.small-offset-11,table.body th.small-offset-11{margin-left:91.66667%!important;Margin-left:91.66667%!important}table.body table.columns td.expander,table.body table.columns th.expander{display:none!important}table.body .right-text-pad,table.body .text-pad-right{padding-left:10px!important}table.body .left-text-pad,table.body .text-pad-left{padding-right:10px!important}table.menu{width:100%!important}table.menu td,table.menu th{width:auto!important;display:inline-block!important}table.menu.small-vertical td,table.menu.small-vertical th,table.menu.vertical td,table.menu.vertical th{display:block!important}table.menu[align=center]{width:auto!important}table.button.small-expand,table.button.small-expanded{width:100%!important}table.button.small-expand table,table.button.small-expanded table{width:100%}table.button.small-expand table a,table.button.small-expanded table a{text-align:center!important;width:100%!important;padding-left:0!important;padding-right:0!important}table.button.small-expand center,table.button.small-expanded center{min-width:0}}</style><span class="preheader" style="color:#f3f3f3;display:none!important;font-size:1px;line-height:1px;max-height:0;max-width:0;mso-hide:all!important;opacity:0;overflow:hidden;visibility:hidden"></span><table class="body" style="Margin:0;background:#f3f3f3;border-collapse:collapse;border-spacing:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;height:100%;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><td class="center" align="center" valign="top" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><center data-parsed="" style="min-width:580px;width:100%"><table align="center" class="container main float-center" style="Margin:0 auto;background:#fefefe;border:1px solid #00549d;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:580px"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="20px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;hyphens:auto;line-height:20px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" valign="middle" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><h1 id="email-title" class="text-center" style="Margin:0;Margin-bottom:10px;color:#00549d;font-family:Helvetica,Arial,sans-serif;font-size:34px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;padding-top:10px;text-align:center;word-wrap:normal">New Task COPY</h1></th><th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></tbody></table><hr><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="20px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;hyphens:auto;line-height:20px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left">You assigned a new task in First Voice CRM.</p><table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="20px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;hyphens:auto;line-height:20px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><table class="callout" style="Margin-bottom:16px;border-collapse:collapse;border-spacing:0;margin-bottom:16px;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th class="callout-inner primary" style="Margin:0;background:#cae6ff;border:1px solid #444;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:10px;text-align:left;width:100%">
        <table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-3 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><span><b>Description:</b></span></th></tr></table></th><th class="small-12 large-9 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:75%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><span>' . $description . '</span></th></tr></table></th></tr></tbody></table><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-3 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><span><b>Comments: </b></span></th></tr></table></th><th class="small-12 large-9 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:75%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><span>' . $comments . '</span></th></tr></table></th></tr></tbody></table>
        <hr></th><th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></table></th></tr></tbody></table></td></tr></tbody></table></center></td></tr></table><!-- prevent Gmail on iOS font size manipulation --><div style="display:none;white-space:nowrap;font:15px courier;line-height:0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></body></html>';

        $bodyText = $description . "<br/>" . $comments;

        $subject = 'Emma Alert';

        //    $transport = Swift_SmtpTransport::newInstance(SMTP, 25)
//        ->setUsername(FROMEMAIL)
//        ->setPassword(FROMEMAILPASS);
        $transport = $SMT;

        // Create the Mailer using your created Transport
        $mailer = Swift_Mailer::newInstance($transport);
        $message = Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom(array(FROMEMAIL => PROGRAM))
            ->setTo(array($email))
            ->setBody($bodyHTML, 'text/html')
            ->addPart($bodyText, 'text/plain');
        $result = $mailer->send($message);
    }
}

$errors = array();
$data = array();

$id = $fvmdb->real_escape_string($_POST['id']);
$plan = $fvmdb->real_escape_string($_POST['plan']);
$user = $fvmdb->real_escape_string($_POST['user']);
$description = $fvmdb->real_escape_string($_POST['description']);
$comment = $fvmdb->real_escape_string($_POST['comments']);
$desktop = $fvmdb->real_escape_string($_POST['desktopConnection']);

if($desktop == '1'){
    $users = select_addGeofenceMessage_users($id);
//        $fvmdb->query("
//        SELECT u.id
//        FROM users u
//        WHERE u.username = '". $id ."'
//    ");
    $user = $users->fetch_assoc();
    $id = $user['id'];
}

if (empty($description)){
    $errors['description'] = 'Description is required';
}
if (empty($comment)){
    $errors['comments'] = 'Comment is required';
}

$FcmServerKey = 'AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db';

if (empty($errors)) {
    $create = insert_geofence_messages($id, $description, $comment, $user, $plan);
    if(!$create){
        $errors['creation'] = 'Error creating message';
    }
    else{
        $insert_id = $emmadb->insert_id;
    }

}

//send message
if(empty($errors)) {
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///                                          NEW GEOFENCE TYPE                                                      ///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Send parent fence
    $geoTopic = '/topics/emma-GEO-ANDROID-' . $id;
    $AndroidpostData = array(
        'to' => $geoTopic,
        'time_to_live' => 900,
        'content_available' => true,
        'data' => array(
            'GEO_COMMUNICATION' => true,
            'geo_communication_id' => $insert_id,
            'event' => $description,
            'description' => $comment,
            'sound' => true,
            'vibrate' => true,
        )
    );


//ios
    $geoTopic = '/topics/emma-GEO-IOS-' . $id;
    $iospostData = array(
        'to' => $geoTopic,
        'priority' => 'high',
        'time_to_live' => 900,
        'notification' => array(
            'body' => $description,
            'sound' => 'notification.aiff',
            'badge' => '1'
        ),
        'data' => array(
            'GEO_COMMUNICATION' => true,
            'geo_communication_id' => $insert_id,
            'event' => $description,
            'description' => $comment,
            'sound' => true,
            'vibrate' => true,
        )
    );

//send android
    $ch_android = curl_init('https://fcm.googleapis.com/fcm/send');
    curl_setopt_array($ch_android, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_FOLLOWLOCATION => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Authorization: key=' . $FcmServerKey,
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($AndroidpostData)
    ));
    $ANDROID_Response = curl_exec($ch_android);

//send ios
    $ch_ios = curl_init('https://fcm.googleapis.com/fcm/send');
    curl_setopt_array($ch_ios, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_FOLLOWLOCATION => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Authorization: key=' . $FcmServerKey,
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($iospostData)
    ));
    $IOS_Response = curl_exec($ch_ios);

    $data['geoTopic'][] = $geoTopic;
    $data['iosPostData'][] = $iospostData;
    $data['AndroidpostData'][] = $AndroidpostData;

    $data['ios response'][] = $IOS_Response;
    $data['android response'][] = $ANDROID_Response;



//Loop Geofence Children
    $childs = select_getgeofence_children($id);
    while ($children = $childs->fetch_assoc()) {
        //SEND MESSAGE TO GEOFENCE TOPICS
//android
        $geoTopic = '/topics/emma-GEO-ANDROID-' . $children['id'];
        $AndroidpostData = array(
            'to' => $geoTopic,
            'time_to_live' => 900,
            'content_available' => true,
            'data' => array(
                'GEO_COMMUNICATION' => true,
                'geo_communication_id' => $insert_id,
                'event' => $description,
                'description' => $comment,
                'sound' => true,
                'vibrate' => true,
            )
        );


//ios
        $geoTopic = '/topics/emma-GEO-IOS-' . $children['id'];
        $iospostData = array(
            'to' => $geoTopic,
            'priority' => 'high',
            'time_to_live' => 900,
            'notification' => array(
                'body' => $description,
                'sound' => 'notification.aiff',
                'badge' => '1'
            ),
            'data' => array(
                'GEO_COMMUNICATION' => true,
                'geo_communication_id' => $insert_id,
                'event' => $description,
                'description' => $comment,
                'sound' => true,
                'vibrate' => true,
            )
        );

//send android
        $ch_android = curl_init('https://fcm.googleapis.com/fcm/send');
        curl_setopt_array($ch_android, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Authorization: key=' . $FcmServerKey,
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($AndroidpostData)
        ));
        $ANDROID_Response = curl_exec($ch_android);

//send ios
        $ch_ios = curl_init('https://fcm.googleapis.com/fcm/send');
        curl_setopt_array($ch_ios, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Authorization: key=' . $FcmServerKey,
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($iospostData)
        ));
        $IOS_Response = curl_exec($ch_ios);

        $data['geoTopic'][] = $geoTopic;
        $data['iosPostData'][] = $iospostData;
        $data['AndroidpostData'][] = $AndroidpostData;

        $data['ios response'][] = $IOS_Response;
        $data['android response'][] = $ANDROID_Response;

    }

}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///                                          OLD GEOFENCE TYPE                                                      ///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//    $androiddeviceGroupString = 'emma-android-geoFence-' . date("d-m-y-h-i-s") . "--" . $id;
//    $iosdeviceGroupString = 'emma-ios-geoFence-' . date("d-m-y-h-i-s") . "--" . $id;
//
//    $coordSelect = select_geofence_locations_with_id($id);
//    if ($coordSelect->num_rows > 0) {
//        $fence = $coordSelect->fetch_assoc();
//    }
//
//
///////////////////////////////////////////////GATHER USERS//////////////////////////////////////////////////////////////
//    //android
//
//    //<editor-fold desc="android gather users">
//    if (!empty($fence)) {
//        if (!empty($fence['radius']) && $fence['radius'] != 0) {
//            //type is radial
//            //narrow list turn into square
//            $kmRadius = $fence['radius'] * 1.60934;
//
//            $lat_adjustment = $kmRadius / 110.574;
//            $lng_adjustment = $kmRadius / (111.320 * cos(($fence['center_lat'] * M_PI) / (180)));
//
//            $adj_n_lat = $fence['center_lat'] + $lat_adjustment;
//            $adj_w_lng = $fence['center_lng'] - $lng_adjustment;
//            $adj_s_lat = $fence['center_lat'] - $lat_adjustment;
//            $adj_e_lng = $fence['center_lng'] + $lng_adjustment;
//
//            $shortList = select_geofenceUserTracking_with_corners_duration_and_ios($adj_n_lat, $adj_s_lat, $adj_e_lng, $adj_w_lng, 1, 0);
////            $fvmdb->query("
////                SELECT *
////                FROM emma_geofence_user_tracking
////                WHERE lat < " . $adj_n_lat . " AND lat > " . $adj_s_lat . "
////                AND lng < " . $adj_e_lng . " AND lng > " . $adj_w_lng . "
////                AND last_updated >= DATE_SUB(NOW(),INTERVAL 1 HOUR)
////                AND ios_background = 0
////            ");
//            if ($shortList->num_rows > 0) {
//                $TokenListAndroid = array();
//                while ($person = $shortList->fetch_assoc()) {
//                    if (getDistance($person['lat'], $person['lng'], $fence['center_lat'], $fence['center_lng']) < $kmRadius) {
//                        $TokenListAndroid[] = array("token" => $person['token'], "userID" => $person['userID']);
//                    }
//                }
//            }
//        } else {
//            //type is rectangle
//            $adj_n_lat = $fence['nw_corner_lat'];
//            $adj_w_lng = $fence['nw_corner_lng'];
//            $adj_s_lat = $fence['se_corner_lat'];
//            $adj_e_lng = $fence['se_corner_lng'];
//
//            $shortList = select_geofenceUserTracking_with_corners_duration_and_ios($adj_n_lat, $adj_s_lat, $adj_e_lng, $adj_w_lng, 1, 0);
////                $fvmdb->query("
////                SELECT *
////                FROM emma_geofence_user_tracking
////                WHERE lat < " . $adj_n_lat . " AND lat > " . $adj_s_lat . "
////                AND lng < " . $adj_e_lng . " AND lng > " . $adj_w_lng . "
////                AND last_updated >= DATE_SUB(NOW(),INTERVAL 1 HOUR)
////                AND ios_background = 0
////            ");
//            if ($shortList->num_rows > 0) {
//                $TokenListAndroid = array();
//                while ($person = $shortList->fetch_assoc()) {
//                    $TokenListAndroid[] = array("token" => $person['token'], "userID" => $person['userID']);
//                }
//            }
//        }
//    }
//    //</editor-fold>
//
//
//    //ios
//    //<editor-fold desc="Gather Users IOS">
//    if (!empty($fence)) {
//        if (!empty($fence['radius']) && $fence['radius'] != 0) {
//            //type is radial
//            //narrow list turn into square
//            $kmRadius = $fence['radius'] * 1.60934;
//
//            $lat_adjustment = $kmRadius / 110.574;
//            $lng_adjustment = $kmRadius / (111.320 * cos(($fence['center_lat'] * M_PI) / (180)));
//
//            $adj_n_lat = $fence['center_lat'] + $lat_adjustment;
//            $adj_w_lng = $fence['center_lng'] - $lng_adjustment;
//            $adj_s_lat = $fence['center_lat'] - $lat_adjustment;
//            $adj_e_lng = $fence['center_lng'] + $lng_adjustment;
//
//            $shortListIOS = select_geofenceUserTracking_with_corners_duration_and_ios($adj_n_lat, $adj_s_lat, $adj_e_lng, $adj_w_lng, 24, 1);
////                $fvmdb->query("
////                SELECT *
////                FROM emma_geofence_user_tracking
////                WHERE lat < " . $adj_n_lat . " AND lat > " . $adj_s_lat . "
////                AND lng < " . $adj_e_lng . " AND lng > " . $adj_w_lng . "
////                AND last_updated >= DATE_SUB(NOW(),INTERVAL 24 HOUR)
////                AND ios_background = 1
////            ");
//            if ($shortListIOS->num_rows > 0) {
//                $TokenListIOS = array();
//                while ($person = $shortListIOS->fetch_assoc()) {
//                    if (getDistance($person['lat'], $person['lng'], $fence['center_lat'], $fence['center_lng']) < $kmRadius) {
//                        $TokenListIOS[] = array("token" => $person['token'], "userID" => $person['userID']);
//                    }
//                }
//            }
//        } else {
//            //type is rectangle
//            $adj_n_lat = $fence['nw_corner_lat'];
//            $adj_w_lng = $fence['nw_corner_lng'];
//            $adj_s_lat = $fence['se_corner_lat'];
//            $adj_e_lng = $fence['se_corner_lng'];
//
//            $shortListIOS = select_geofenceUserTracking_with_corners_duration_and_ios($adj_n_lat, $adj_s_lat, $adj_e_lng, $adj_w_lng, 24, 1);
////                $fvmdb->query("
////                SELECT *
////                FROM emma_geofence_user_tracking
////                WHERE lat < " . $adj_n_lat . " AND lat > " . $adj_s_lat . "
////                AND lng < " . $adj_e_lng . " AND lng > " . $adj_w_lng . "
////                AND last_updated >= DATE_SUB(NOW(),INTERVAL 24 HOUR)
////                AND ios_background = 1
////            ");
//
//        }
//        if ($shortListIOS->num_rows > 0) {
//            $TokenListIOS = array();
//            while ($person = $shortListIOS->fetch_assoc()) {
//                $TokenListIOS[] = array("token" => $person['token'], "userID" => $person['userID']);
//            }
//        }
//    }
//    //</editor-fold>
//
//
///////////////////////////////////////////////CREATE ID//////////////////////////////////////////////////////////////////
//    //android
//    //<editor-fold desc="Android Create ID">
//    if (!empty($TokenListAndroid)) {
//        //send message
//
//        $deviceKeyID = '';
//
//
//        //[Create Device Group]
//
//        //put users in array
//        $payload = array();
//        $payload['registration_ids'] = array();
//        foreach ($TokenListAndroid AS $tokenUser) {
//            $payload['registration_ids'][] = $tokenUser['token'];
//        }
//        //other payload info
//        $payload['operation'] = 'create';
//        $payload['notification_key_name'] = $androiddeviceGroupString;
//
//
//        //send request
//        $ch = curl_init('https://fcm.googleapis.com/fcm/notification');
//        curl_setopt_array($ch, array(
//            CURLOPT_POST => TRUE,
//            CURLOPT_RETURNTRANSFER => TRUE,
//            CURLOPT_HTTPHEADER => array(
//                'Authorization: key=' . $FcmServerKey,
//                'Content-Type: application/json',
//                'project_id: 742343872016'
//            ),
//            CURLOPT_POSTFIELDS => json_encode($payload)
//        ));
//        $createDeviceGroupResult = curl_exec($ch);
//        $createDeviceGroupResponseData = json_decode($createDeviceGroupResult);
////            $data['create_device_group'] = $createDeviceGroupResponseData;
//        $AndroiddeviceKeyID = $createDeviceGroupResponseData->{"notification_key"};
////            $data['deviceKeyTestCheck'] = $deviceKeyID;
////            $data['webResponseRaw'] = $createDeviceGroupResult;
//
//        $insert = insert_emma_notification_device_groups($androiddeviceGroupString, $AndroiddeviceKeyID);
////        $fvmdb->query("
////                                INSERT INTO emma_notification_device_groups (notification_key_name, notification_key, last_updated)
////                                VALUES ('" . $androiddeviceGroupString . "', '" . $AndroiddeviceKeyID . "', NOW())
////                            ");
//    } else {
////              $errors['empty Groups'] = 'No users in those groups';
//    }
//    //</editor-fold>
//
//    //IOS
//    //<editor-fold desc="IOS Create ID">
//    if (!empty($TokenListIOS)) {
//        //send message
//
//        $deviceKeyID = '';
//
//
//        //[Create Device Group]
//
//        //put users in array
//        $payload = array();
//        $payload['registration_ids'] = array();
//        foreach ($TokenListIOS AS $tokenUser) {
//            $payload['registration_ids'][] = $tokenUser['token'];
//        }
//        //other payload info
//        $payload['operation'] = 'create';
//        $payload['notification_key_name'] = $iosdeviceGroupString;
//
//
//        //send request
//        $ch = curl_init('https://fcm.googleapis.com/fcm/notification');
//        curl_setopt_array($ch, array(
//            CURLOPT_POST => TRUE,
//            CURLOPT_RETURNTRANSFER => TRUE,
//            CURLOPT_HTTPHEADER => array(
//                'Authorization: key=' . $FcmServerKey,
//                'Content-Type: application/json',
//                'project_id: 742343872016'
//            ),
//            CURLOPT_POSTFIELDS => json_encode($payload)
//        ));
//        $createDeviceGroupResult = curl_exec($ch);
//        $createDeviceGroupResponseData = json_decode($createDeviceGroupResult);
////            $data['create_device_group'] = $createDeviceGroupResponseData;
//        $IOSdeviceKeyID = $createDeviceGroupResponseData->{"notification_key"};
////            $data['deviceKeyTestCheck'] = $deviceKeyID;
////            $data['webResponseRaw'] = $createDeviceGroupResult;
//
//        $insert = insert_emma_notification_device_groups($iosdeviceGroupString, $IOSdeviceKeyID);
////        $fvmdb->query("
////                                INSERT INTO emma_notification_device_groups (notification_key_name, notification_key, last_updated)
////                                VALUES ('" . $iosdeviceGroupString . "', '" . $IOSdeviceKeyID . "', NOW())
////                            ");
//    } else {
////              $errors['empty Groups'] = 'No users in those groups';
//    }
//    //</editor-fold>
//
///////////////////////////////////////////////CREATE PAYLOADS////////////////////////////////////////////////////////////
//    //android
//    if (!empty($AndroiddeviceKeyID)) {
//        $AndroidpostData = array(
//            'to' => $AndroiddeviceKeyID,
//            'time_to_live' => 900,
//            'content_available' => true,
//            'data' => array(
//                'GEO_COMMUNICATION' => true,
//                'geo_communication_id' => $id,
//                'event' => $description,
//                'description' => $comment,
//                'sound' => true,
//                'vibrate' => true,
//            )
//        );
//    }
//
//
//    //ios
//    if (!empty($IOSdeviceKeyID)) {
//        $iospostData = array(
//            'to' => $IOSdeviceKeyID,
//            'priority' => 'high',
//            'time_to_live' => 900,
////                        'content_available' => true,
//            'notification' => array(
//                'body' => $description,
//                'sound' => 'notification.aiff',
//                'badge' => '1'
//            ),
//            'data' => array(
//                'GEO_COMMUNICATION' => true,
//                'geo_communication_id' => $id,
//                'event' => $description,
//                'description' => $comment,
//                'sound' => true,
//                'vibrate' => true,
//            )
//        );
//    }
//
///////////////////////////////////////////////SEND PAYLOADS//////////////////////////////////////////////////////////////
//    //send payloads
//    //android
//    if (!empty($AndroidpostData)) {
//        $ch = curl_init('https://fcm.googleapis.com/fcm/send');
//        curl_setopt_array($ch, array(
//            CURLOPT_POST => TRUE,
//            CURLOPT_RETURNTRANSFER => TRUE,
//            CURLOPT_HTTPHEADER => array(
//                'Authorization: key=' . $FcmServerKey,
//                'Content-Type: application/json'
//            ),
//            CURLOPT_POSTFIELDS => json_encode($AndroidpostData)
//        ));
//        $AndroidwebResponse = curl_exec($ch);
//
//        if ($AndroidwebResponse) {
//            $jsonResponse = json_decode($AndroidwebResponse);
//            $count = $jsonResponse->{'success'};
//            $insert = update_geofenceMessages_sentToQnt_with_messageID_and_count($insert_id, $count);
////            $fvmdb->query("
////                                UPDATE emma_geofence_messages
////                                SET sent_to_qnt = sent_to_qnt + '" . $count . "'
////                                WHERE id = '" . $insert_id . "'
////                            ");
//        }
//    }
//
//    //send payloads
//    //ios
//    if (!empty($iospostData)) {
//        $ch_ios = curl_init('https://fcm.googleapis.com/fcm/send');
//        curl_setopt_array($ch_ios, array(
//            CURLOPT_POST => TRUE,
//            CURLOPT_RETURNTRANSFER => TRUE,
//            CURLOPT_FOLLOWLOCATION => TRUE,
//            CURLOPT_HTTPHEADER => array(
//                'Authorization: key=' . $FcmServerKey,
//                'Content-Type: application/json'
//            ),
//            CURLOPT_POSTFIELDS => json_encode($iospostData)
//        ));
//        $IOS_Response = curl_exec($ch_ios);
//
//        if ($IOS_Response) {
//            $IOSjsonResponse = json_decode($IOS_Response);
//            $IOScount = $IOSjsonResponse->{'success'};
//            $insert = update_geofenceMessages_sentToQnt_with_messageID_and_count($insert_id, $IOScount);
////                $fvmdb->query("
////                                UPDATE emma_geofence_messages
////                                SET sent_to_qnt = sent_to_qnt + '" . $IOScount . "'
////                                WHERE id = '" . $insert_id . "'
////                            ");
//        }
//    }
//}
//
//
//
////                if (empty($errors)) {
////
////                    $icon_name = 'icon_emma_sos.png';
////
////
////                    $postData = array(
////                        'to' => $deviceKeyID,
////                        'time_to_live' => 900,
////                        'content_available' => true,
////                        'data' => array(
////                            'GEO_COMMUNICATION' => true,
////                            'geo_communication_id' => $id,
////                            'event' => $description,
////                            'description' => $comment,
////                            'sound' => true,
////                            'vibrate' => true,
////                        )
////                    );
////
////
////                    $ch = curl_init('https://fcm.googleapis.com/fcm/send');
////                    curl_setopt_array($ch, array(
////                        CURLOPT_POST => TRUE,
////                        CURLOPT_RETURNTRANSFER => TRUE,
////                        CURLOPT_HTTPHEADER => array(
////                            'Authorization: key=' . $FcmServerKey,
////                            'Content-Type: application/json'
////                        ),
////                        CURLOPT_POSTFIELDS => json_encode($postData)
////                    ));
////                    $webResponse = curl_exec($ch);
////                }
////            }
////        }
////
////                if (!empty($TokenList)) {
////                    //send message
////
////                    $deviceKeyID = '';
////
////
////                    //[Create Device Group]
////
////                    //put users in array
////                    $payload = array();
////                    $payload['registration_ids'] = array();
////                    foreach ($TokenList AS $tokenUser) {
////                        $payload['registration_ids'][] = $tokenUser['token'];
////                    }
////                    //other payload info
////                    $payload['operation'] = 'create';
////                    $payload['notification_key_name'] = $deviceGroupString;
////
////
////                    //send request
////                    $ch = curl_init('https://fcm.googleapis.com/fcm/notification');
////                    curl_setopt_array($ch, array(
////                        CURLOPT_POST => TRUE,
////                        CURLOPT_RETURNTRANSFER => TRUE,
////                        CURLOPT_HTTPHEADER => array(
////                            'Authorization: key=' . $FcmServerKey,
////                            'Content-Type: application/json',
////                            'project_id: 742343872016'
////                        ),
////                        CURLOPT_POSTFIELDS => json_encode($payload)
////                    ));
////                    $createDeviceGroupResult = curl_exec($ch);
////                    $createDeviceGroupResponseData = json_decode($createDeviceGroupResult);
//////            $data['create_device_group'] = $createDeviceGroupResponseData;
////                    $deviceKeyID = $createDeviceGroupResponseData->{"notification_key"};
//////            $data['deviceKeyTestCheck'] = $deviceKeyID;
//////            $data['webResponseRaw'] = $createDeviceGroupResult;
////
////                    $insert = $fvmdb->query("
////                                INSERT INTO emma_notification_device_groups (notification_key_name, notification_key, last_updated)
////                                VALUES ('" . $deviceGroupString . "', '" . $deviceKeyID . "', NOW())
////                            ");
//////            $errors['test'] = 'test';
////                } else {
//////              $errors['empty Groups'] = 'No users in those groups';
////                }
////
////
////                if (empty($errors)) {
////
////                    $icon_name = 'icon_emma_sos.png';
////
////
////                    $postData = array(
////                        'to' => $deviceKeyID,
////                        'time_to_live' => 900,
////                        'content_available' => true,
////                        'data' => array(
////                            'GEO_COMMUNICATION' => true,
////                            'geo_communication_id' => $id,
////                            'event' => $description,
////                            'description' => $comment,
////                            'sound' => true,
////                            'vibrate' => true,
////                        )
////                    );
////
////                    $iospostData = array(
////                        'to' => $deviceKeyID,
////                        'time_to_live' => 900,
//////                        'content_available' => true,
////                        'notification' => array(
////                            'body' => $description,
////                            'sound' => 'notification.aiff',
////                            'badge' => '1'
////                        ),
////                        'data' => array(
////                            'GEO_COMMUNICATION' => true,
////                            'geo_communication_id' => $id,
////                            'event' => $description,
////                            'description' => $comment,
////                            'sound' => true,
////                            'vibrate' => true,
////                        )
////                    );
////
////
////
////
////                    $ch = curl_init('https://fcm.googleapis.com/fcm/send');
////                    curl_setopt_array($ch, array(
////                        CURLOPT_POST => TRUE,
////                        CURLOPT_RETURNTRANSFER => TRUE,
////                        CURLOPT_HTTPHEADER => array(
////                            'Authorization: key=' . $FcmServerKey,
////                            'Content-Type: application/json'
////                        ),
////                        CURLOPT_POSTFIELDS => json_encode($postData)
////                    ));
////                    $webResponse = curl_exec($ch);
////
////                    if($webResponse){
////                        $jsonResponse = json_decode(curl_exec($ch));
////                        $count = $jsonResponse->{'success'};
////                        $insert = $fvmdb->query("
////                            UPDATE emma_geofence_messages
////                            SET sent_to_qnt = '".$count."'
////                            WHERE id = '".$insert_id."'
////                        ");
////                    }
////                }
////            }
////
////        }
////    }
////
////}

//
//$data['post'] = $_POST;
//$data['count'] = $count + $IOScount;
//$data["AndroidMessageResponse"] = $AndroidwebResponse;
//$data["IOSMessageResponse"] = $IOS_Response;
//
//$data['iosTokens'] = $TokenListIOS;
//$data['iosJsonResponse'] = $IOSjsonResponse;
//$data['iosdevicekey'] = $IOSdeviceKeyID;
//$data['androidDevicekey'] = $AndroiddeviceKeyID;
//$data['iosPostData'] = $iospostData;
//
//$data['fence-n-lat'] = $adj_n_lat;
//$data['fence-w-lng'] = $adj_w_lng;
//$data['fence-s-lat'] = $adj_s_lat;
//$data['fence-e-lng'] = $adj_e_lng;

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);