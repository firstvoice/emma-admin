<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 6/20/2019
 * Time: 12:16 PM
 */

require('../include/db.php');
include('../include/processing.php');

require('../vendor/php-jwt-master/src/JWT.php');
require('../vendor/php-jwt-master/src/BeforeValidException.php');
require('../vendor/php-jwt-master/src/ExpiredException.php');
require('../vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('../config/config.json'));

$USER = null;

$token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key, array('HS512'));

$USER = $token->data;

$errors = array();
$data = array();
$id = $fvmdb->real_escape_string($_POST['id']);
$type = $fvmdb->real_escape_string($_POST['type']);

$log = insert_log_received($USER->id, $id, $type);
if(!$log){
    $errors['error-logging'] = 'Error logging received alert';
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);
