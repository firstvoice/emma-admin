<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 5/29/2019
 * Time: 3:30 PM
 */

global $fvmdb;
include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$id = $fvmdb->real_escape_string($_POST['id']);
$serial = $fvmdb->real_escape_string($_POST['serial']);
$placement = $fvmdb->real_escape_string($_POST['placement']);
$notes = $fvmdb->real_escape_string($_POST['notes']);
$status = $fvmdb->real_escape_string($_POST['status']);
$address = $fvmdb->real_escape_string($_POST['address']);
$latitude = $fvmdb->real_escape_string($_POST['latitude']);
$longitude = $fvmdb->real_escape_string($_POST['longitude']);

$padAttached = $fvmdb->real_escape_string($_POST['pad-attached']);
$pad = $fvmdb->real_escape_string($_POST['pad']);
$padLot = $fvmdb->real_escape_string($_POST['pad-lot']);
$padExpiration = $fvmdb->real_escape_string($_POST['pad-expiration']);
$pedPad = $fvmdb->real_escape_string($_POST['ped-pad']);
$pedPadLot = $fvmdb->real_escape_string($_POST['ped-pad-lot']);
$pedPadExpiration = $fvmdb->real_escape_string($_POST['ped-pad-expiration']);
$sparePad = $fvmdb->real_escape_string($_POST['spare-pad']);
$sparePadLot = $fvmdb->real_escape_string($_POST['spare-pad-lot']);
$sparePadExpiration = $fvmdb->real_escape_string($_POST['spare-pad-expiration']);
$sparePedPad = $fvmdb->real_escape_string($_POST['spare-ped-pad']);
$sparePedPadLot = $fvmdb->real_escape_string($_POST['spare-ped-pad-lot']);
$sparePedPadExpiration = $fvmdb->real_escape_string($_POST['spare-ped-pad-expiration']);

$pakAttached = $fvmdb->real_escape_string($_POST['pak-attached']);
$pak = $fvmdb->real_escape_string($_POST['pak']);
$pakLot = $fvmdb->real_escape_string($_POST['pak-lot']);
$pakExpiration = $fvmdb->real_escape_string($_POST['pak-expiration']);
$pedPak = $fvmdb->real_escape_string($_POST['ped-pak']);
$pedPakLot = $fvmdb->real_escape_string($_POST['ped-pak-lot']);
$pedPakExpiration = $fvmdb->real_escape_string($_POST['ped-pak-expiration']);
$sparePak = $fvmdb->real_escape_string($_POST['spare-pak']);
$sparePakLot = $fvmdb->real_escape_string($_POST['spare-pak-lot']);
$sparePakExpiration = $fvmdb->real_escape_string($_POST['spare-pak-expiration']);
$sparePedPak = $fvmdb->real_escape_string($_POST['spare-ped-pak']);
$sparePedPakLot = $fvmdb->real_escape_string($_POST['spare-ped-pak-lot']);
$sparePedPakExpiration = $fvmdb->real_escape_string($_POST['spare-ped-pak-expiration']);

$batteryAttached = $fvmdb->real_escape_string($_POST['battery-attached']);
$battery = $fvmdb->real_escape_string($_POST['battery']);
$batteryLot = $fvmdb->real_escape_string($_POST['battery-lot']);
$batteryExpiration = $fvmdb->real_escape_string($_POST['battery-expiration']);
$spareBattery = $fvmdb->real_escape_string($_POST['spare-battery']);
$spareBatteryLot = $fvmdb->real_escape_string($_POST['spare-battery-lot']);
$spareBatteryExpiration = $fvmdb->real_escape_string($_POST['spare-battery-expiration']);

$accessory = $fvmdb->real_escape_string($_POST['accessory']);
$accessoryLot = $fvmdb->real_escape_string($_POST['accessory-lot']);
$accessoryExpiration = $fvmdb->real_escape_string($_POST['accessory-expiration']);

if (empty($id)) $errors['id'] = 'The ID could not be found';
if (empty($latitude) ||
  empty($longitude)) $errors['location'] = 'The latitude and or longitude was not processed';

if (empty($errors)) {
  //update_emmaAsset_details_with_assetID($id, $type_id, $latitude, $longitude, $address, $status);
  $fvmdb->query("
   UPDATE aeds as a
   SET
    a.serialnumber = '" . $serial . "',
    a.address = '" . $address . "',
    a.latitude = '" . $latitude . "',
    a.longitude = '" . $longitude . "',
    a.location = '" . $placement . "',
    a.notes = '".$notes."',
    a.current = '" . $status . "'
    WHERE a.aed_id = '" . $id . "'
   ");

  $removeOldPads = $fvmdb->query("
    UPDATE aed_pads ap
    SET ap.display = 0
    WHERE ap.aed_id = ".$id."
  ");
  if(isset($pad) AND $pad != "") {
    $insertNewPad = $fvmdb->query("
      INSERT INTO aed_pads
      (
       aed_id, aed_pad_type_id, expiration, lot, udi, spare, `default`, display, updated
      ) VALUES (
        '".$id."',
        '".$pad."',
        '".$padExpiration."',
        '".$padLot."',
        '',
        '".((isset($padAttached) AND $padAttached == "main") ? '0' : '1' )."',
        '',
        '1',
        '".date('Y-m-d H:i:s', time())."'
      )
    ");
  }
  if(isset($pedPad) AND $pedPad != "") {
    $insertNewPedPad = $fvmdb->query("
      INSERT INTO aed_pads
      (
       aed_id, aed_pad_type_id, expiration, lot, udi, spare, `default`, display, updated
      ) VALUES (
        '".$id."',
        '".$pedPad."',
        '".$pedPadExpiration."',
        '".$pedPadLot."',
        '',
        '".((isset($padAttached) AND $padAttached == "main") ? '0' : '1' )."',
        '',
        '1',
        '".date('Y-m-d H:i:s', time())."'
      )
    ");
  }
  if(isset($sparePad) AND $sparePad != "") {
    $insertNewPad = $fvmdb->query("
      INSERT INTO aed_pads
      (
       aed_id, aed_pad_type_id, expiration, lot, udi, spare, `default`, display, updated
      ) VALUES (
        '".$id."',
        '".$sparePad."',
        '".$sparePadExpiration."',
        '".$sparePadLot."',
        '',
        '".((isset($padAttached) AND $padAttached == "spare") ? '0' : '1' )."',
        '',
        '1',
        '".date('Y-m-d H:i:s', time())."'
      )
    ");
  }
  if(isset($sparePedPad) AND $sparePedPad != "") {
    $insertNewSparePedPad = $fvmdb->query("
      INSERT INTO aed_pads
      (
       aed_id, aed_pad_type_id, expiration, lot, udi, spare, `default`, display, updated
      ) VALUES (
        '".$id."',
        '".$sparePedPad."',
        '".$sparePedPadExpiration."',
        '".$sparePedPadLot."',
        '',
        '".((isset($padAttached) AND $padAttached == "spare") ? '0' : '1' )."',
        '',
        '1',
        '".date('Y-m-d H:i:s', time())."'
      )
    ");
  }

  $removeOldPaks = $fvmdb->query("
    UPDATE aed_paks ap
    SET ap.display = 0
    WHERE ap.aed_id = ".$id."
  ");
  if(isset($pak) AND $pak != "") {
    $insertNewPak = $fvmdb->query("
      INSERT INTO aed_paks
      (
       aed_id, aed_pak_type_id, expiration, lot, udi, spare, `default`, display, updated
      ) VALUES (
        '".$id."',
        '".$pak."',
        '".$pakExpiration."',
        '".$pakLot."',
        '',
        '".((isset($pakAttached) AND $pakAttached == "main") ? '0' : '1' )."',
        '',
        '1',
        '".date('Y-m-d H:i:s', time())."'
      )
    ");
  }
  if(isset($pedPak) AND $pedPak != "") {
    $insertNewPedPak = $fvmdb->query("
      INSERT INTO aed_paks
      (
       aed_id, aed_pak_type_id, expiration, lot, udi, spare, `default`, display, updated
      ) VALUES (
        '".$id."',
        '".$pedPak."',
        '".$pedPakExpiration."',
        '".$pedPakLot."',
        '',
        '".((isset($pakAttached) AND $pakAttached == "main") ? '0' : '1' )."',
        '',
        '1',
        '".date('Y-m-d H:i:s', time())."'
      )
    ");
  }
  if(isset($sparePak) AND $sparePak != "") {
    $insertNewSparePak = $fvmdb->query("
      INSERT INTO aed_paks
      (
       aed_id, aed_pak_type_id, expiration, lot, udi, spare, `default`, display, updated
      ) VALUES (
        '".$id."',
        '".$sparePak."',
        '".$sparePakExpiration."',
        '".$sparePakLot."',
        '',
        '".((isset($pakAttached) AND $pakAttached == "spare") ? '0' : '1' )."',
        '',
        '1',
        '".date('Y-m-d H:i:s', time())."'
      )
    ");
  }
  if(isset($sparePak) AND $sparePak != "") {
    $insertNewSparePedPak = $fvmdb->query("
      INSERT INTO aed_paks
      (
       aed_id, aed_pak_type_id, expiration, lot, udi, spare, `default`, display, updated
      ) VALUES (
        '".$id."',
        '".$sparePedPak."',
        '".$sparePedPakExpiration."',
        '".$sparePedPakLot."',
        '',
        '".((isset($pakAttached) AND $pakAttached == "spare") ? '0' : '1' )."',
        '',
        '1',
        '".date('Y-m-d H:i:s', time())."'
      )
    ");
  }

  $removeOldBatteries = $fvmdb->query("
    UPDATE aed_batteries ab
    SET ab.display = 0
    WHERE ab.aed_id = ".$id."
  ");
  if(isset($battery) AND $battery != "") {
    $insertNewBattery = $fvmdb->query("
      INSERT INTO aed_batteries
      (
       aed_id, aed_battery_type_id, expiration, lot, udi, spare, `default`, display, updated
      ) VALUES (
        '".$id."',
        '".$battery."',
        '".$batteryExpiration."',
        '".$batteryLot."',
        '',
        '".((isset($batteryAttached) AND $batteryAttached == "main") ? '0' : '1' )."',
        '',
        '1',
        '".date('Y-m-d H:i:s', time())."'
      )
    ");
  }
  if(isset($spareBattery) AND $spareBattery != "") {
    $insertNewBattery = $fvmdb->query("
      INSERT INTO aed_batteries
      (
       aed_id, aed_battery_type_id, expiration, lot, udi, spare, `default`, display, updated
      ) VALUES (
        '".$id."',
        '".$spareBattery."',
        '".$spareBatteryExpiration."',
        '".$spareBatteryLot."',
        '',
        '".((isset($batteryAttached) AND $batteryAttached == "spare") ? '0' : '1' )."',
        '',
        '1',
        '".date('Y-m-d H:i:s', time())."'
      )
    ");
  }

  $removeOldAccessories = $fvmdb->query("
    UPDATE aed_accessories aa
    SET aa.display = 0
    WHERE aa.aed_id = ".$id."
  ");
  if(isset($accessory) AND $accessory != "") {
    $insertNewAccessory = $fvmdb->query("
      INSERT INTO aed_accessories
      (
       aed_id, aed_accessory_type_id, expiration, lot, udi, spare, display, updated
      ) VALUES (
        '".$id."',
        '".$accessory."',
        '".$accessoryExpiration."',
        '".$accessoryLot."',
        '',
        '0',
        '1',
        '".date('Y-m-d H:i:s', time())."'
      )
    ");
  }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);