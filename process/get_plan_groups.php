<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/6/2019
 * Time: 3:06 PM
 */


include('../include/db.php');
include('../include/processing.php');
$planId = $emmadb->real_escape_string($_POST['plan-id']);

$data = array();
$errors = array();

if(empty($planId)) {
    $errors['plan-id'] = 'No plan ID';
}

if(empty($errors))
{
    $groups = $emmadb->query("
    SELECT g.*
    FROM emma_groups g
    WHERE g.emma_plan_id = '". $planId ."'
  ");
    while($group = $groups->fetch_assoc())
    {
        $data['groups'][] = $group ;
    }
}
$data['success'] = empty($errors);
$data['errors'] = $errors;
echo json_encode($data);