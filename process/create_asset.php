<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 4/6/2018
 * Time: 1:23 PM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$planId = $fvmdb->real_escape_string($_POST['plan-id']);
$typeId = $fvmdb->real_escape_string($_POST['type-id']);
$userId = $fvmdb->real_escape_string($_POST['user-id']);
$latitude = $fvmdb->real_escape_string($_POST['latitude']);
$longitude = $fvmdb->real_escape_string($_POST['longitude']);
$address = $fvmdb->real_escape_string($_POST['address']);

if (empty($planId) || empty($typeId) || empty($userId)) {
  $errors['empty'] = 'Missing info';
}

if (empty($errors)) {
    $createAsset = insert_emma_assets($planId, $typeId, $lat, $lng, $userId, $address, 0, 1);
//        $fvmdb->query("
//    insert into emma_assets (
//      emma_plan_id,
//      emma_asset_type_id,
//      created_by_id,
//      created_date,
//      latitude,
//      longitude,
//      address,
//      deleted,
//      active
//    ) values (
//      '" . $planId . "',
//      '" . $typeId . "',
//      '" . $userId . "',
//      '" . date('Y-m-d H:i:s', time()) . "',
//      '" . $latitude . "',
//      '" . $longitude . "',
//      '".$address."',
//      0,
//      1
//    )
//  ");
    $asset_id = $emmadb->insert_id;

    $HeaderArray = json_decode($_POST['Headers']);
    $DatesArray = json_decode($_POST['Dates']);
    $FillinArray = json_decode($_POST['Fillins']);

    if (!empty($HeaderArray)) {
        foreach($HeaderArray as $header)
        {
            insert_emma_assets_custom_info($asset_id, null, null, $header->id);
//            $fvmdb->query("
//            INSERT INTO emma_assets_custom_info
//            (emma_asset_id, date_time, info, dropdown)
//            VALUES
//            (
//            '".$asset_id."',
//            null,
//            null,
//            '".$header->id."'
//            )
//            ");
        }
    }
    if (!empty($FillinArray)) {
        foreach($FillinArray as $fillin)
            insert_emma_assets_custom_info($asset_id, null, $fillin->value, null);
//        $fvmdb->query("
//        INSERT INTO emma_assets_custom_info
//        (emma_asset_id, date_time, info,dropdown)
//        VALUES
//        (
//          '".$asset_id."',
//           null,
//          '".$fillin->value."',
//          null
//        )
//        ");
    }
    if (!empty($DatesArray)) {
        foreach($DatesArray as $date)
        {
            insert_emma_assets_custom_info($asset_id, $date->value, null, null);
//            $fvmdb->query("
//            INSERT INTO emma_assets_custom_info
//            (emma_asset_id, date_time, info, dropdown)
//            VALUES
//            (
//            '".$asset_id."',
//            '".$date->value."',
//            null,
//            null
//            )
//            ");
        }
    }






  if (!$createAsset) {
    $errors['sql'] = $fvmdb->error;
  }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);