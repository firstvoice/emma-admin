<?php

require('../vendor/autoload.php');
require('../include/db.php');
include('../include/processing.php');
require('../vendor/php-jwt-master/src/JWT.php');
require('../vendor/php-jwt-master/src/BeforeValidException.php');
require('../vendor/php-jwt-master/src/ExpiredException.php');
require('../vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('../config/config.json'));

$USER = null;

$token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key, array('HS512'));

$USER = $token->data;

$t = $emmadb->real_escape_string($_GET['t']);

$filename = '';
$csv = '';
$csvbr = "\n";
$csvsp = ',';
$ext = '.csv';

if(substr($t,0,-1) == 'sos'){
    if (substr($t,3,1) == 'a'){
        $emma_all = select_reportsos_a($USER->emma_plan_id);
        $type = 'ALL';
    }else if (substr($t,3,1) == 'h'){
        $emma_all = select_reportsos_h($USER->emma_plan_id);
        $type = 'NEEDS HELP';
    }else if (substr($t,3,1) == 'p'){
        $emma_all = select_reportsos_p($USER->emma_plan_id);
        $type = 'PENDING';
    }else if (substr($t,3,1) == 'c'){
        $emma_all = select_reportsos_c($USER->emma_plan_id);
        $type = 'CANCELLED';
    }else if (substr($t,3,1) == 'l'){
        $emma_all = select_reportsos_l($USER->emma_plan_id);
        $type = 'CLOSED';
    }else{
        echo 'Unknown Format';
        exit();
    }


    $filename .= 'EMMA SOS';
    $csv .=
        $type . ' EMMA SOS ' . $csvbr; //csvbr is new line character.
    $csv .=
        '"User"' . $csvsp .
        '"Created Date"' . $csvsp .
        '"SOS Date"' . $csvsp .
        '"Help Alert Date"' . $csvsp .
        '"Help Alert Lat"' . $csvsp .
        '"Help Alert Lng"' . $csvsp .
        '"Initial Lat"' . $csvsp .
        '"Initial Lng"' . $csvsp .
        '"Cancelled Date"' . $csvsp .
        '"Cancelled Lat"' . $csvsp .
        '"Cancelled Lng"' . $csvsp .
        '"Closed By"' . $csvsp .
        '"Closed Date"' . $csvsp .
        '"Closed Comments"' . $csvsp .
        '"Closed Lat"' . $csvsp .
        '"Closed Lng"' . $csvsp .
        '"Plan Name"' . $csvbr;

    while($emma = $emma_all->fetch_assoc())
    {
        $csv .=
            '"' .$emma['user_name']. '"' . $csvsp .
            '"' .$emma['created_date']. '"' . $csvsp .
            '"' .$emma['sos_date']. '"' . $csvsp .
            '"' .$emma['help_date']. '"' . $csvsp .
            '"' .$emma['help_lat']. '"' . $csvsp .
            '"' .$emma['help_lng']. '"' . $csvsp .
            '"' .$emma['pending_lat']. '"' . $csvsp .
            '"' .$emma['pending_lng']. '"' . $csvsp .
            '"' .$emma['cancelled_date']. '"' . $csvsp .
            '"' .$emma['cancelled_lat']. '"' . $csvsp .
            '"' .$emma['cancelled_lng']. '"' . $csvsp .
            '"' .$emma['closed_user_name']. '"' . $csvsp .
            '"' .$emma['closed_date']. '"' . $csvsp .
            '"' .$emma['closed_comments']. '"' . $csvsp .
            '"' .$emma['closed_lat']. '"' . $csvsp .
            '"' .$emma['closed_lng']. '"' . $csvsp .
            '"' .$emma['plan_name']. '"' . $csvbr;
    }
}


if($ext == '.csv') {

    $reportName = $type . '_EMMA_SOS_'.date('m-d-Y H-i-s');
    $fp = fopen('reports/report' . $reportName . $ext, 'w');
    fwrite($fp, $csv);
    fclose($fp);


    $downloadName='report' . $reportName . $ext;
    $download_file = getcwd() . '/reports/' . $downloadName;

    if(file_exists($download_file))
    {
        if($ext == '.csv') {
            header('Content-Type: application/download');
            header('Content-Disposition: attachment; filename="' . basename($download_file) . '"');
            header('Content-Transfer-Encoding: binary');
            readfile($download_file);
        }
        else{
            echo 'File invalid';
        }

    }
    else
    {
        echo 'File does not exist on given path';
    }
}
?>