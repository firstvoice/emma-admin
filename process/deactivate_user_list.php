<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/11/2017
 * Time: 12:11 AM
 */

include('../include/db.php');
include('../include/processing.php');
include('../include/process_cookie.php');

$user_ids = $_POST['selected_users'];
$data = array();
$errors = array();

if (empty($user_ids)) {
  $errors[] = 'No users selected';
}

if (empty($errors)) {
  /*
  $user_list = implode("','", $user_ids);
  $update = $fvmdb->query("
    DELETE FROM emma_user_groups
    WHERE user_id IN ('" . $user_list . "')
  ");

  if (!$update) {
    $errors[] = 'update failed';
  }
  */
  foreach ($user_ids AS $user) {
    $deactivate = update_user_active_with_userID_planID_active($user, $USER->emma_plan_id, 0);
//    $fvmdb->query("
//      update users
//      set active = 0
//      where id = '" . $user . "'
//    ");
  }
}

//$data['post'] = $_POST;
//$data['userlist'] = $user_list;

$data['success'] = empty($errors);
$data['errors'] = $errors;
echo json_encode($data);