<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/6/2017
 * Time: 3:58 PM
 */

include('../include/db.php');
include('../include/processing.php');

$group_id = $fvmdb->real_escape_string($_POST['group-id']);


$data = array();
$errors = array();



$data['id'] = $group_id;


$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);