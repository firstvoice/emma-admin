<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 8/28/2017
 * Time: 10:04 AM
 */

include('../include/db.php');
include('../include/processing.php');

$columns = $_GET['columns'];
$draw = $fvmdb->real_escape_string($_GET['draw']);
$length = $fvmdb->real_escape_string($_GET['length']);
$order = $_GET['order'];
$search = $_GET['search'];
$start = $fvmdb->real_escape_string($_GET['start']);
$active = $fvmdb->real_escape_string($_GET['active']);
$userid = $fvmdb->real_escape_string($_GET['userid']);

$orderString = $columns[$order[0]['column']]['data'] . ' ' . $order[0]['dir'];

$data = array();
$emmaplanarr = array();

$emmaplans = select_multiPlans_with_userID($userid);
while($emmaplan = $emmaplans->fetch_assoc()){
    $emmaplanarr[] = $emmaplan['plan_id'];
}

$users = select_getusers_users_admin($emmaplanarr, $orderString, $length, $start, $_GET['columns'][0]['search']['value'], $_GET['columns'][1]['search']['value'], $_GET['columns'][2]['search']['value'], $_GET['columns'][3]['search']['value'], $_GET['columns'][4]['search']['value']);
//    $fvmdb->query("
//    SELECT SQL_CALC_FOUND_ROWS u.username, u.id, u.firstname, u.lastname, group_concat(DISTINCT eg.name ORDER BY eg.name) AS `group`
//    FROM users u
//    LEFT JOIN emma_user_groups AS gb ON u.id = gb.user_id
//    LEFT JOIN emma_groups eg ON gb.emma_group_id = eg.emma_group_id
//    LEFT JOIN emma_multi_plan mp ON u.id = mp.user_id
//    WHERE u.display = 'yes'
//    AND u.temporary_code IS NULL
//    AND u.active = 1
//    AND mp.plan_id = ". $emmaPlanId ."
//    AND (" . ($_GET['columns'][0]['search']['value'] != "" ? "u.username like ('%" . $_GET['columns'][0]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][1]['search']['value'] != "" ? "u.firstname like ('%" . $_GET['columns'][1]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][2]['search']['value'] != "" ? "u.lastname like ('%" . $_GET['columns'][2]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][3]['search']['value'] != "" ? "eg.name like ('" . $_GET['columns'][3]['search']['value'] . "%')" : "1") . ")
//    GROUP BY u.id
//    ORDER BY ". $orderString ."
//    LIMIT " . $length . " OFFSET " . $start . "
//");
$found = select_FOUND_ROWS();
//$fvmdb->query("
//    SELECT FOUND_ROWS()
//");
$count = $found->fetch_assoc();

$data['iTotalRecords'] = $count['FOUND_ROWS()'];
$data['iTotalDisplayRecords'] = $count['FOUND_ROWS()'];
$data['sEcho'] = $draw;
$data['aaData'] = array();
while ($user = $users->fetch_assoc()) {
    $data['aaData'][] = $user;
}

echo json_encode($data);
