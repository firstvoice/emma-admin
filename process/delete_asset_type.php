<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/7/2019
 * Time: 2:21 PM
 */


include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$assetId = $fvmdb->real_escape_string($_POST['asset-id']);

if (empty($assetId)) {
    $errors['asset-id'] = 'Asset Id not provided';
}

if (empty($errors)) {
    $deleteAsset = update_emmaAssetType_active_with_assetTypeID($assetId, 0);
//    $fvmdb->query("
//    UPDATE emma_asset_types
//    SET active = 0
//    WHERE emma_asset_type_id = '" . $assetId . "'
//  ");
    if (!$deleteAsset) {
        $errors['sql'] = $fvmdb->error;
    }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);