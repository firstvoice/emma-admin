<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 9/30/2019
 * Time: 10:06 AM
 */

session_start();
require('../include/db.php');
include('../include/processing.php');

require('../vendor/php-jwt-master/src/JWT.php');
require('../vendor/php-jwt-master/src/BeforeValidException.php');
require('../vendor/php-jwt-master/src/ExpiredException.php');
require('../vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('../config/config.json'));

$USER = null;

$token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key, array('HS512'));

$USER = $token->data;

$data = array();
$errors = array();
$plans = array();

$id = $fvmdb->real_escape_string($_GET['id']);

if(empty($USER)){
    $errors['no user'] = 'No token';
}

if(empty($errors)) {
    if($id == 'all'){
        $homeplans = array();
        $plans = select_setHomePlans_plans($USER->id);
//            $fvmdb->query("
//                SELECT p.name, p.emma_plan_id
//                FROM emma_multi_plan m
//                LEFT JOIN emma_plans p ON m.plan_id = p.emma_plan_id
//                WHERE m.user_id = '". $USER->id ."'
//            ");
        while($plan = $plans->fetch_assoc()){
            $homeplans[] = $plan['emma_plan_id'];
        }

        $path = explode('process/', $_SERVER['PHP_SELF']);

        $token->data->emma_home_plans = $homeplans;

        $jwt = Firebase\JWT\JWT::encode($token, $CONFIG->key, 'HS512');
        setcookie('jwt', $jwt, time() + (9 * 60 * 60), '/');
    }elseif($id == 'none'){
        $path = explode('process/', $_SERVER['PHP_SELF']);

        $token->data->emma_home_plans = array($USER->emma_plan_id);

        $jwt = Firebase\JWT\JWT::encode($token, $CONFIG->key, 'HS512');
        setcookie('jwt', $jwt, time() + (9 * 60 * 60), '/');
    }
    else {
        $plans = $USER->emma_home_plans;

        if (in_array($id, $plans)) {
            if (count($plans) > 1) {
                array_splice($plans, array_search($id, $plans), 1);
            }
        } else {
            $plans[] = $id;
        }

        $path = explode('process/', $_SERVER['PHP_SELF']);

        $token->data->emma_home_plans = $plans;

        $jwt = Firebase\JWT\JWT::encode($token, $CONFIG->key, 'HS512');
        setcookie('jwt', $jwt, time() + (9 * 60 * 60), '/');
    }

}

$data['errors'] = $errors;
$data['success'] = empty($errors);


echo json_encode($data);