<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/13/2017
 * Time: 11:39 AM
 */

require('../include/db.php');
include('../include/processing.php');

$resource_id = $fvmdb->real_escape_string($_GET['id']);

$resources = select_getpdffile_resources($USER->emma_plan_id ,$resource_id);
//    $fvmdb->query("
//    SELECT r.*
//    FROM emma_resources AS r
//    WHERE r.emma_resource_id = ". $resource_id ."
//    AND r.emma_plan_id = ". $USER->emma_plan_id ."
//    AND r.active = 1
//");
$resource = $resources->fetch_assoc();

$pdf_base64 = $resource['file_string'];


header('Content-Type: application/pdf');
$temppdf = (base64_decode($pdf_base64));
echo $temppdf;
