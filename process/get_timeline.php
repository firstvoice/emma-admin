<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/31/2017
 * Time: 12:35 PM
 */
require('../include/db.php');
include('../include/processing.php');

$data = array();
$errors = array();

$emergencyId = $fvmdb->real_escape_string($_POST['id']);
$received = str_replace(",", "','", $fvmdb->real_escape_string($_POST['received']));

if (empty($emergencyId)) $errors['emergency-id'] = 'No event (emergency) id';

$data['received'] = $received;
$data['incidents'] = array();
$data['sub-events'] = array();
if (empty($errors)) {
  $responses = select_gettimeline_responses($received, $emergencyId);
//      $fvmdb->query("
//    SELECT *
//    FROM
//    (
//      (
//        SELECT
//          CONCAT('usr-', er.emergency_response_id)                    AS id,
//          er.comments                                                 AS message,
//          CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') AS user,
//          u.id as user_id,
//          er.status,
//          ers.name                                                    AS status_name,
//          ers.color                                                   AS status_color,
//          ers.class                                                   AS status_class,
//          CONCAT(u.firstname, ' ', u.lastname)                        AS first_last_name,
//          u.username,
//          er.updated_date,
//          '' as description,
//          '' as comments,
//          '' as emma_system_message_id,
//          er.pin                                                      AS response_pin,
//          u.emma_pin,
//          CONCAT(v.firstname, ' ', v.lastname)                        AS validation_user,
//          validated_time                                              AS validation_time
//        FROM emergency_responses er
//        JOIN users u ON er.user_id = u.id
//        JOIN emergency_response_status ers ON er.status = ers.emergency_response_status_id AND CONCAT('usr-', er.emergency_response_id) NOT IN ('" .
//    $received . "')
//        LEFT JOIN users v ON er.validated_id = v.id
//        WHERE er.emergency_id = '" . $emergencyId . "'
//          AND er.status != '-1'
//      ) UNION ALL (
//        SELECT
//          CONCAT('sys-', sm.emma_system_message_id) AS id,
//          sm.message,
//          CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') AS user,
//          u.id as user_id,
//          '-2' AS status,
//          'Alert'                                   AS status_name,
//          '#ff0000'                                 AS status_color,
//          'system'                                  AS status_class,
//          'System'                                  AS first_last_name,
//          ''                                        AS username,
//          sm.created_date AS updated_date,
//          sm.description,
//          sm.comments,
//          sm.emma_system_message_id,
//          '0000'                                    AS response_pin,
//          '0000'                                    AS emma_pin,
//          ''                                        AS validation_user,
//          ''                                        AS validation_time
//        FROM emma_system_messages sm
//        JOIN users u on sm.created_by_id = u.id
//        WHERE sm.emergency_id = '" . $emergencyId . "'
//          AND CONCAT('sys-', sm.emma_system_message_id) NOT IN ('" . $received . "')
//      )
//    ) AS results
//    ORDER BY updated_date
//  ");
  while ($response = $responses->fetch_assoc()) {
    $response['groups'] = array();
    if ($response['emma_system_message_id']) {
      $messageGroups = select_gettimeline_messageGroups($response['emma_system_message_id']);
//          $fvmdb->query("
//        select g.emma_group_id, g.name
//        from emma_system_message_groups mg
//        join emma_groups g on mg.emma_group_id = g.emma_group_id
//        where mg.emma_system_message_id = '" . $response['emma_system_message_id'] . "'
//      ");
      while ($messageGroup = $messageGroups->fetch_assoc()) {
        $response['groups'][] = $messageGroup;
      }
    }
    $data['incidents'][] = $response;
  }

  $subEvents = select_gettimeline_subEvents($emergencyId, $received);
//      $fvmdb->query("
//    select
//      e.emergency_id as id,
//      et.emergency_type_id as type_id,
//      et.name as type_name,
//      et.badge,
//      es.emma_site_name,
//      e.created_date,
//      e.active,
//      CONCAT(u.firstname, ' ', u.lastname) as user
//    from emergencies e
//    join emergency_types et on e.emergency_type_id = et.emergency_type_id
//    join emma_sites es on e.emma_site_id = es.emma_site_id
//    join users u on e.created_by_id = u.id
//    where e.parent_emergency_id = '" . $emergencyId . "'
//      AND CONCAT('evt-', e.emergency_id) NOT IN ('" . $received . "')
//  ");
  while($subEvent = $subEvents->fetch_assoc()) {
    $data['sub-events'][] = $subEvent;
  }
}
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);
