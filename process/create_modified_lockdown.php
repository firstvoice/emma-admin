<?php

require('../include/db.php');

$errors = array();
$data = array();

$user = $emmadb->real_escape_string($_POST['user']);
$planId = $emmadb->real_escape_string($_POST['plan']);
$lat = $emmadb->real_escape_string($_POST['lat']);
$lng = $emmadb->real_escape_string($_POST['lng']);



$data['lat'] = $lat;
$data['lng'] = $lng;

if(empty($user)){
    $errors['invalid data'] = 'user id';
}
if(empty($planId)){
    $errors['invalid data'] = 'plan id';
}
if(empty($lat) || empty($lng)){
    $errors['invalid data'] = 'latitude / longitude';
}

if(empty($errors)) {


    $create = insert_modified_lockdown($user, $planId, $lat, $lng);
    if (!$create) {
        $data['create'] = $create;
        $errors['creation'] = 'Error creating message';
    } else {
        $insert_id = $emmadb->insert_id;
    }
    $data['notification_id'] = $insert_id;

    $groupList = array();
    $groupsQuery = select_lockdown_groups($planId);
    if ($groupsQuery->num_rows > 0) {
        while ($groupResult = $groupsQuery->fetch_assoc()) {
            $groupList[] = $groupResult['emma_group_id'];
            $users = select_unreguser_with_group($groupResult['emma_group_id']);
            while($user = $users->fetch_assoc()) {
                $data['userquery'][] = $user;
                $numbersarray[] = $user['phone'];
            }
        }
    }

    $data['numbers'] = $numbersarray;

    $notificationQuery = select_modified_lockdown_notification($planId);
    if ($notificationQuery->num_rows > 0) {
        $notificationResult = $notificationQuery->fetch_assoc();
        $notification = $notificationResult['notification'];
        $data['notification'] = $notification;
    } else {
        $errors['notification'] = 'No notification';
    }

    $eventName = 'Modified Lockdown';



    /////////////////////////////////////////////////////////////////////////


    $FcmServerKey = 'AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db';
    $topics = '/topics/emma-' . $planId;
    $IOSTopics = array();


    if (empty($errors)) {

        $postData = array(
            'to' => $topics,
            'time_to_live' => 900,
            'content_available' => true,
            'data' => array(
                'GROUP_ALERT' => 'true',
                'LOCKDOWN' => true,
                'Lockdown_id' => $insert_id,
                'event' => 'Lockdown',
                'description' => $notification,
                'sound' => true,
                'vibrate' => true,
                'groups' => array(),
            )
        );

        if (count($groupList) > 0) {
            foreach ($groupList as $groupId) {
                $postData['data']['groups'][] = $groupId;
                $IOSTopics[] = "'emma-plan" . $planId . "-" . $groupId . "' in topics";
                error_log("sending lockdown to groupid: ".$groupId);
            }
        }


        $ch = curl_init('https://fcm.googleapis.com/fcm/send');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Authorization: key=' . $FcmServerKey,
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        $response = curl_exec($ch);
        $responseData = json_decode($response);
        $data['response'] = $responseData;

        /////////////////////////////
        //    IOS NOTIFICATIONS    //
        /////////////////////////////

        $IOSTopicsString = $IOSTopics[0];
        if (count($IOSTopics) > 0) {
            for ($i = 1; ($i < 5 && $i < count($IOSTopics)); $i++) {
                $IOSTopicsString .= ' || ' . $IOSTopics[$i];
            }
        }

        $data['iosTopicString'] = $IOSTopicsString;
        $data['iosTopics'] = $IOSTopics;

        $postIOSData = array(
            'condition' => $IOSTopicsString,
            'time_to_live' => 600,
            'priority' => 'high',
            'notification' => array(
                'title' => "Lockdown",
                'body' => $notification,
                'sound' => 'notification.aiff',
                'badge' => '1'
            ),
            'data' => array(
                'GROUP_ALERT' => 'true',
                'event' => 'Lockdown',
                'lockdown_id' => $insert_id,
                'description' => $notification,
                'LOCKDOWN' => true,
            )
        );

        $ch = curl_init('https://fcm.googleapis.com/fcm/send');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Authorization: key=' . $FcmServerKey,
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postIOSData)
        ));
        $IOSresponse = curl_exec($ch);

        if (count($IOSTopics) > 5) {
            $m = 5;
            while ($m < count($IOSTopics)) {
                for ($i = $m; ($i < ($m + 5) && $i < count($IOSTopics)); $i++) {
                    if ($i == $m) {
                        $IOSTopicsString = $IOSTopics[$i];
                    } else {
                        $IOSTopicsString .= ' || ' . $IOSTopics[$i];
                    }
                }

                $postIOSData2 = array(
                    'condition' => $IOSTopicsString,
                    'time_to_live' => 600,
                    'priority' => 'high',
                    'notification' => array(
                        'title' => "Lockdown",
                        'body' => $notification,
                        'sound' => 'notification.aiff',
                        'badge' => '1'
                    ),
                    'data' => $postIOSData['data']
                );

                $ch = curl_init('https://fcm.googleapis.com/fcm/send');
                curl_setopt_array($ch, array(
                    CURLOPT_POST => TRUE,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HTTPHEADER => array(
                        'Authorization: key=' . $FcmServerKey,
                        'Content-Type: application/json'
                    ),
                    CURLOPT_POSTFIELDS => json_encode($postIOSData2)
                ));
                $IOSresponse2 = curl_exec($ch);

                $m += 5;
            }
        }


    }
}

$data['success'] = empty($errors);
$data['errors'] = $errors;


$data['groups'] = $groupList;
$data['plan'] = $planId;
$data['post'] = $_POST;

echo json_encode($data);