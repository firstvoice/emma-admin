<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/17/2019
 * Time: 3:42 PM
 */
include('../include/db.php');
include('../include/processing.php');

$data = array();
$errors = array();
$leads = array();
if ($_FILES['import-scripts-file']['error'] == 0 && pathinfo($_FILES['import-scripts-file']['name'], PATHINFO_EXTENSION) == 'csv') {
    if (($handle = fopen($_FILES['import-scripts-file']['tmp_name'], "r")) !== FALSE) {
        $line = 1;
        while (($info = fgetcsv($handle, 1000, ",")) !== FALSE) {
            if ($line == 1) {
                //This line in particular is bugging out, so we are just commenting it out for now.
               // if($info[0] !== 'Script Type')
               // {
               //     $errors['info0'] = $info[0];
               // }
                if($info[1] !== 'Broadcast Type')
                {
                    $errors['info1'] = $info[1];
                }

                if($info[2] !== 'Group')
                {
                    $errors['info2'] = $info[2];

                }
                if($info[3] !== 'Name')
                {
                    $errors['info3'] = $info[3];

                }
                if($info[4] !==  'Text')
                {
                    $errors['info4'] = $info[4];
                }
            } else {
                $details = array();
                $details['Script Type'] = $fvmdb->real_escape_string($info[0]);
                $details['Broadcast Type'] = $fvmdb->real_escape_string($info[1]);
                $details['Group'] = $fvmdb->real_escape_string($info[2]);
                $details['Name'] = $fvmdb->real_escape_string($info[3]);
                $details['Text'] = $fvmdb->real_escape_string($info[4]);
                $scripts[] = $details;

                if (empty($details['Script Type'])) {
                    $errors['row-' . $line] = 'Row ' . $line . ' is missing Script Type';
                }
            }
            $line++;

            $data['item'][] = $info;

        }
        fclose($handle);
    } else {
        $errors['open'] = 'Could not open file for reading';
    }
} else {
    $errors['csv'] = 'Template is not a .csv file';
}
//$data['leads'] = $leads;

if (empty($errors)) {
    //foreach ($leads as $lead) {
    //    $uniqueId = uniqid('');
     //   $insertLead = $crmdb->query("
     //       INSERT INTO leads (
      //        lead_id,
       //       salutation,
       //       first_name,
       //       last_name,
      //       middle_name,
     //         suffix,
      //        title,
       //       company,
      //        phone,
      //        extension,
      //        mobile,
       //       email,
      //        status,
      //        club_status,
     //         website,
     //         industry,
     //         employees,
     //         lead_source,
     //         rating,
     //         description,
     //         referred_by,
     //         owner_id,
     ////         address,
    //          city,
     //         state,
    //          postal_code,
     //         country,
     //         converted,
     //         converted_date,
     //         converted_account_id,
    //          campaign_id,
    //          last_call_date,
    //          created_by_id,
    //          created_date,
    //          modified_by_id,
     //         modified_date,
     //         active
     //       ) VALUES (
     //         '" . $uniqueId . "',
    //          '" . $lead['salutation'] . "',
    //          '" . $lead['first_name'] . "',
    //          '" . $lead['last_name'] . "',
    //          '" . $lead['middle_name'] . "',
    //          '" . $lead['suffix'] . "',
//              '" . $lead['title'] . "',
    //            '" . $lead['company'] . "',
    //        '" . preg_replace("/[^0-9]/", "", $lead['phone']) . "',
    //        '" . $lead['extenstion'] . "',
    //        '" . preg_replace("/[^0-9]/", "", $lead['mobile']) . "',
    //        '" . $lead['email'] . "',
    //        '" . $lead['status'] . "',
    //        '" . $lead['club_status'] . "',
    //        '" . $lead['website'] . "',
    //        '" . $lead['industry'] . "',
    //        '" . $lead['employees'] . "',
    //        '" . $lead['lead_source'] . "',
    //        '" . $lead['rating'] . "',
    //        '" . $lead['description'] . "',
    //'" . $lead['referred_by'] . "',
      //        '" . $ownerId . "',
        ////      '" . $lead['address'] . "',
            //  '" . $lead['city'] . "',
    //            '" . $lead['state'] . "',
    //            '" . $lead['postal_code'] . "',
    //            '" . $lead['country'] . "',
    //            '0',
    //            '',
    //            '',
    //            '" . $lead['campaign_id'] . "',
    //            '" . date('Y-m-d H:i:s', strtotime($lead['last_call_date'])) . "',
    //           '" . $createdById . "',
    //          '" . date('Y-m-d H:i:s') . "',
    ////          '',
    //           '',
    //          '1'
    //         )
    //       ");
    //      if (!$insertLead) {
    //        $errors['lead-insert'] = 'Error inserting lead: ' . $crmdb->error;
    //    } else {
    //        $data['insert-id'] = $uniqueId;
    //     }
    //  }
}
$data['files'] = $_FILES;
$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);