<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/23/2017
 * Time: 12:16 PM
 */

require('../include/db.php');
include('../include/processing.php');
include('../include/process_cookie.php');

$data = array();
$errors = array();

$check = check_active_sos($USER->id);
if($check->num_rows > 0){
    $data['check'] = '1';
    $data['return'][] = $check->fetch_assoc();
}else{
    $data['check'] = '0';
}


$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);