<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 8/28/2017
 * Time: 10:04 AM
 */

include('../include/db.php');
include('../include/processing.php');

$columns = $_GET['columns'];
$draw = $fvmdb->real_escape_string($_GET['draw']);
$length = $fvmdb->real_escape_string($_GET['length']);
$order = $_GET['order'];
$search = $_GET['search'];
$start = $fvmdb->real_escape_string($_GET['start']);
$active = $fvmdb->real_escape_string($_GET['active']);
$emmaPlanId = $fvmdb->real_escape_string($_GET['emma-plan-id']);

$orderString = $columns[$order[0]['column']]['data'] . ' ' . $order[0]['dir'];


$data = array();

$events = select_getclosedSOS_events($emmaPlanId,$orderString,$length,$start);
//    $fvmdb->query("
//    SELECT SQL_CALC_FOUND_ROWS s.emma_sos_id, s.closed_date, CONCAT(u.firstname, ' ', u.lastname) AS user, s.closed_lat as latitude, s.closed_lng as longitude, u.id as user_id, s.closed_comments, '0' as closed_call_report, u.phone, u.username as email
//    FROM emma_sos s
//    JOIN users u ON s.created_by_id = u.id AND u.emma_plan_id = '".$emmaPlanId."'
//    WHERE s.closed_date IS NOT NULL
//    AND s.help_date IS NOT NULL
//    AND s.pending_date IS NOT NULL
//    AND s.cancelled_date IS NULL
//    ORDER BY ". $orderString .", created_date desc
//    LIMIT " . $length . " OFFSET " . $start . "
//");
$found = select_FOUND_ROWS();
//    $fvmdb->query("
//    SELECT FOUND_ROWS()
//");
$count = $found->fetch_assoc();

$data['iTotalRecords'] = $count['FOUND_ROWS()'];
$data['iTotalDisplayRecords'] = $count['FOUND_ROWS()'];
$data['sEcho'] = $draw;
$data['aaData'] = array();
while ($event = $events->fetch_assoc()) {
  $callLogs = select_sosCallReports_with_sosID($event['emma_sos_id']);
//      $fvmdb->query("
//    select *
//    from emma_sos_call_reports
//    where emma_sos_id = '".$event['emma_sos_id']."'
//  ");
  if($callLog = $callLogs->fetch_assoc()) {
    $event['closed_call_report'] = $callLog['emma_sos_call_report_id'];
  }
  $data['aaData'][] = $event;
}

echo json_encode($data);