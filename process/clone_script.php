<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 4/6/2018
 * Time: 1:23 PM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$scriptId = $fvmdb->real_escape_string($_POST['emma-script-id']);
$typeId = $fvmdb->real_escape_string($_POST['type']);
$broadcastId = $fvmdb->real_escape_string($_POST['broadcast']);
$name = $fvmdb->real_escape_string($_POST['name']);
$text = $fvmdb->real_escape_string($_POST['text']);
$group = $fvmdb->real_escape_string($_POST['group']);

if (empty($scriptId) || empty($name) || empty($text)) {
    $errors['empty'] = 'Not all fields have been filled out';
}

$scripts = select_scripts_with_scriptID($scriptId);
//$fvmdb->query("
//    SELECT s.*
//    FROM emma_scripts s
//    WHERE s.emma_script_id = '". $scriptId ."'
//");
$script = $scripts->fetch_assoc();

if (empty($errors)) {
    $updateScript = insert_emma_scripts($typeId, $script['emma_plan_id'], $group, $broadcastId, $name, $text);
//    $fvmdb->query("
//    insert into emma_scripts (
//    emma_script_type_id,
//    emma_plan_id,
//    emma_script_group_id,
//    emma_broadcast_type_id,
//    name,
//    text
//    ) VALUES (
//    '". $typeId ."',
//    '". $script['emma_plan_id'] ."',
//    '". $group ."',
//    '". $broadcastId ."',
//    '". $name ."',
//    '". $text ."'
//    )
//  ");
    if (!$updateScript) {
        $errors['sql'] = $fvmdb->error;
    }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);