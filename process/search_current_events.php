<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 6/20/2019
 * Time: 12:16 PM
 */

require('../include/db.php');
include('../include/processing.php');

require('../vendor/php-jwt-master/src/JWT.php');
require('../vendor/php-jwt-master/src/BeforeValidException.php');
require('../vendor/php-jwt-master/src/ExpiredException.php');
require('../vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('../config/config.json'));

$USER = null;

$token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key, array('HS512'));

$USER = $token->data;

$errors = array();
$data = array();
$currentEvents = array();
$currentSecurity = array();
$currentSOS = array();
$currentNotification = array();
$currentLockdowns = array();
$currentMLockdowns = array();

$events = select_emergencies_with_user($USER->id);
//    $fvmdb->query("
//    SELECT e.*
//    FROM emergencies e
//    WHERE e.emma_plan_id = '". $USER->emma_plan_id ."'
//    AND e.active = '1'
//");
while($event = $events->fetch_assoc()){
    $currentEvents[] = $event['emergency_id'];
}

$securities = select_securitiy_with_planID($USER->emma_plan_id);
//    $fvmdb->query("
//    SELECT e.*
//    FROM emma_securities e
//    WHERE e.emma_plan_id = '". $USER->emma_plan_id ."'
//    AND e.active = '1'
//");
while($security = $securities->fetch_assoc()){
    $currentSecurity[] = $security['emma_security_id'];
}
if($USER->privilege->admin) {
    $soss = select_searchCurrentEvents_soss($USER->id);
//    $fvmdb->query("
//SELECT s.*
//FROM emma_sos AS s
//  JOIN users AS u ON s.created_by_id = u.id AND u.emma_plan_id = '". $USER->emma_plan_id ."'
//WHERE s.cancelled_date IS NULL
//      AND s.help_date IS NOT NULL
//      AND s.closed_date IS NULL
//");
    while ($sos = $soss->fetch_assoc()) {
        $currentSOS[] = $sos['emma_sos_id'];
    }
}

$notifications = select_searchCurrentEvents_notifications($USER->id);
//    $fvmdb->query("
//    SELECT e.*
//    FROM emma_mass_communications e
//    WHERE e.emma_plan_id = '". $USER->emma_plan_id ."'
//");
while($notification = $notifications->fetch_assoc()){
    $currentNotification[] = $notification['emma_mass_communication_id'];
}

$lockdowns = select_searchCurrentEvents_lockdowns($USER->id);
//    $fvmdb->query("
//    SELECT e.*
//    FROM emma_mass_communications e
//    WHERE e.emma_plan_id = '". $USER->emma_plan_id ."'
//");
while($lockdown = $lockdowns->fetch_assoc()){
    $sendlock = false;
    $groups = select_groups_with_userID($USER->id);
    while($group = $groups->fetch_assoc()) {
        $lockdownperm = select_group_lockdown_permission($group['emma_group_id']);
        if($lockdownperm->num_rows > 0){
            $sendlock = true;
        }
    }
    if($sendlock) {
        $currentLockdowns[] = $lockdown['lockdown_id'];
    }
}
$mlockdowns = select_searchCurrentEvents_modified_lockdowns($USER->id);
//    $fvmdb->query("
//    SELECT e.*
//    FROM emma_mass_communications e
//    WHERE e.emma_plan_id = '". $USER->emma_plan_id ."'
//");
while($mlockdown = $mlockdowns->fetch_assoc()){
    $sendlock = false;
    $groups = select_groups_with_userID($USER->id);
    while($group = $groups->fetch_assoc()) {
        $lockdownperm = select_group_lockdown_permission($group['emma_group_id']);
        if($lockdownperm->num_rows > 0){
            $sendlock = true;
        }
    }
    if($sendlock) {
        $currentMLockdowns[] = $mlockdown['m_lockdown_id'];
    }
}

$data['events'] = implode(',',$currentEvents);
$data['securities'] = implode(',',$currentSecurity);
$data['sos'] = implode(',',$currentSOS);
$data['notifications'] = implode(',',$currentNotification);
$data['lockdowns'] = implode(',',$currentLockdowns);
$data['m_lockdowns'] = implode(',',$currentMLockdowns);

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);
