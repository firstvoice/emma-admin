<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/11/2017
 * Time: 12:07 AM
 */

include('../include/db.php');
include('../include/processing.php');

$user_ids = $_POST['selected_users'];
$site_id = $fvmdb->real_escape_string($_POST['site_id']);
$data = array();
$errors = array();

if(empty($user_ids)){
    $errors[] = 'No users selected';
}
if(empty($site_id)){
    $errors[] = 'No site id';
}

if(empty($errors)){
    $user_list = implode("','", $user_ids);

    $update = update_users_siteID_with_userlist($user_list, $site_id);

//    $fvmdb->query("
//        UPDATE users AS u
//        SET u.emma_site_id = ". $site_id ."
//        WHERE u.id IN ('". $user_list ."')
//    ");

    if(!$update){
        $errors[] = 'update failed';
    }
}

$data['post'] = $_POST;
$data['userlist'] = $user_list;


$data['success'] = empty($errors);
$data['errors'] = $errors;
echo json_encode($data);