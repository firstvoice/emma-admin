<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 8/28/2017
 * Time: 10:04 AM
 */

include('../include/db.php');
include('../include/processing.php');
global $fvmdb;

$columns = $_GET['columns'];
$draw = $fvmdb->real_escape_string($_GET['draw']);
$length = $fvmdb->real_escape_string($_GET['length']);
$order = $_GET['order'];
$search = $_GET['search'];
$start = $fvmdb->real_escape_string($_GET['start']);
$planID = $fvmdb->real_escape_string($_GET['plan-id']);

$orderString = $columns[$order[0]['column']]['data'] . ' ' . $order[0]['dir'];
$data = array();

$aeds = $fvmdb->query("
  SELECT SQL_CALC_FOUND_ROWS p.id, CONCAT(p.firstname, ' ', p.lastname) as name, tc.name as class, p.display, l.name as location, o.name as org 
  FROM persons p
  JOIN training_personswithtypes tpwt on tpwt.personid = p.id
  JOIN training_classtypeslang tc on tpwt.typeid = tc.typeid
  JOIN locations l on p.locationid = l.id
  JOIN organizations o on l.orgid = o.id
  JOIN emma_plan_orgs epo on l.orgid = epo.org_id
  WHERE epo.plan_id = " . $planID . "
  AND (" . ($_GET['columns'][0]['search']['value'] != "" ? "o.name like ('%" . $_GET['columns'][0]['search']['value'] . "%')" : "1") . ")
  AND (" . ($_GET['columns'][1]['search']['value'] != "" ? "l.name like ('%" . $_GET['columns'][1]['search']['value'] . "%')" : "1") . ")
  AND (" . ($_GET['columns'][2]['search']['value'] != "" ? "CONCAT(p.firstname, ' ', p.lastname) like ('%" . $_GET['columns'][2]['search']['value'] . "%')" : "1") . ")
  AND (" . ($_GET['columns'][3]['search']['value'] != "" ? "tc.name like ('%" . $_GET['columns'][3]['search']['value'] . "%')" : "1") . ")
  AND (" . ($_GET['columns'][4]['search']['value'] != "" ? "p.display like ('%" . $_GET['columns'][4]['search']['value'] . "%')" : "1") . ")
  ORDER BY " . $orderString . ", name DESC
  LIMIT " . $length . " OFFSET " . $start . "
");
$found = $fvmdb->query("
   SELECT FOUND_ROWS()
");
$count = $found->fetch_assoc();

$data['iTotalRecords'] = $count['FOUND_ROWS()'];
$data['iTotalDisplayRecords'] = $count['FOUND_ROWS()'];
$data['sEcho'] = $draw;
$data['aaData'] = array();
$data['sortString'] = $orderString;
while ($aed = $aeds->fetch_assoc()) {
  $data['aaData'][] = $aed;
}
echo json_encode($data);