<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 5/31/2019
 * Time: 10:42 am
 */



include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$type_id = $fvmdb->real_escape_string($_POST['type-id']);
$user_id = $fvmdb->real_escape_string($_POST['user-id']);
$plan_id = $fvmdb->real_escape_string($_POST['plan-id']);
$date_name = $fvmdb->real_escape_string($_POST['date']);
$fillin = $fvmdb->real_escape_string($_POST['fillin']);
$asset_type = $fvmdb->real_escape_string($_POST['asset-type']);
$image = $fvmdb->real_escape_string($_POST['Image']);


if (empty($errors)) {
    //get the image ID
    $grabIcon = select_emmaAssetIcons_with_image($image);
//    $fvmdb->query("
//        SELECT eac.asset_icon_id
//        FROM emma_asset_icons as eac
//        WHERE eac.image = '".$image."'
//    ");
    $icon = $grabIcon->fetch_assoc();
    $data['testtt'] = $icon['asset_icon_id'];
    insert_emma_asset_types($icon['asset_icon_id'], $asset_type, $plan_id);
//    $fvmdb->query("
//    INSERT INTO emma_asset_types (
//    emma_asset_icon_id,
//    name,
//    emma_plan_id
//    )
//    VALUES(
//    '".$icon['asset_icon_id']."',
//    '".$asset_type."',
//    '".$plan_id."'
//    )
//    ");
    $last_id = $emmadb->insert_id;
    $HeaderArray = json_decode($_POST['Headers']);
    if(!empty($HeaderArray))
    {
        //$data['submitted'] = json_decode($fvmdb->real_escape_string($_POST['Headers']));
        $OptionArray = json_decode($_POST['Options']);
        if(!empty($OptionArray)) {
            foreach ($HeaderArray as $header) {
                if ($header->value != null) {
                    foreach ($OptionArray as $option) {
                        if ($option->count == $header->count) {
                            insert_emma_assets_dropdown_options($last_id, $option->value, 1);
//                            $fvmdb->query("
//                              INSERT INTO emma_assets_dropdown_options
//                                (
//                                  asset_type_id,
//                                  Name,
//                                  display
//                                )VALUES(
//                                  '" . $last_id . "',
//                                  '" . $option->value . "',
//                                  1
//                                )
//                            ");

                            insert_emma_asset_type_display($last_id, $header->value, 0, $emmadb->insert_id, 0 , 1);
//                            $fvmdb->query("
//                            INSERT INTO emma_asset_type_display
//                              (
//                                asset_type_id,
//                                name,
//                                date,
//                                dropdown_id,
//                                fillin,
//                                active
//                               )VALUES(
//                                '" . $last_id . "',
//                                '" . $header->value . "',
//                                0,
//                                '" . $fvmdb->insert_id . "',
//                                0,
//                                1
//                              )
//                            ");
                        }
                    }
                }
            }
        }else {
            foreach ($HeaderArray as $header) {
                if(!empty($header->value)) {
                    //No options for the dropdown header, still need to add it.
                    insert_emma_asset_type_display($last_id, $header->value, 0, 0, 0, 1);
//                    $fvmdb->query("
//            INSERT INTO emma_asset_type_display(
//            asset_type_id,
//            name,
//            date,
//            dropdown_id,
//            fillin
//            )VALUES(
//            '" . $last_id . "',
//            '" . $header->value . "',
//             0,
//             0,
//             0
//            )
//            ");
                }
            }
        }

        }

    foreach ($_POST['fillin'] as $key => $value) {
        insert_emma_asset_type_display($last_id, $value, 0, null, 1, 1);
//        $fvmdb->query("
//        INSERT INTO emma_asset_type_display (
//        asset_type_id,
//        name,
//        date,
//        dropdown_id,
//        fillin
//        )
//        VALUES(
//        '".$last_id."',
//        '".$value."',
//        0,
//        null,
//        1
//        )
//        ");
    }
    foreach ($_POST['date'] as $key => $value) {
        insert_emma_asset_type_display($last_id, $value, 1, null, 0, 1);
//        $fvmdb->query("
//        INSERT INTO emma_asset_type_display (
//        asset_type_id,
//        name,
//        date,
//        dropdown_id,
//        fillin
//        )
//        VALUES(
//        '" . $last_id . "',
//        '".$value."',
//        1,
//        null,
//        0
//        )
//        ");
    }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);