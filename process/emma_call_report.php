<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 6/15/2018
 * Time: 3:42 PM
 */


require_once('../include/fpdf/fpdf.php');
require_once('../include/fpdf/fpdi.php');
include('../include/definitions.php');
include('../include/db.php');




//create pdf
$pdf = new FPDI('P','mm','A4');


//make background image
$pdf->setSourceFile('../documents/blank.pdf');
$tplIdx = $pdf->importPage(1);
$pdf->AddPage();
$pdf->useTemplate($tplIdx, 0, 0, 210, 297, false);
$pdf->SetAutoPageBreak(false);

$clientId = $_POST['client-id'];
$clientName = $_POST['client-name'];
$reportDate = $_POST['report-date'];
$reportTime = $_POST['report-time'];
$reportNum = uniqid('');
$reportPost = $_POST['report-post'];
$locationName = $_POST['incident-location'];
$locationId = $_POST['location-id'];
$incidentAddress = $_POST['incident-address'];
$incidentCity = $_POST['incident-city'];
$incidentState = $_POST['incident-state'];
$incidentZip = $_POST['incident-zip'];
if($_POST['police-yn'] == 'yes'){
    $policeName = $_POST['police-name'];
    $policeTime = $_POST['police-time'];
}else{
    $policeName = '';
    $policeTime = '';
}
if($_POST['reslife-yn'] == 'yes'){
    $reslifeName = $_POST['reslife-name'];
    $reslifeTime = $_POST['reslife-time'];
}else{
    $reslifeName = '';
    $reslifeTime = '';
}
if($_POST['other-yn'] == 'yes'){
    $otherNotified = $_POST['other-notified'];
    $otherName = $_POST['other-name'];
    $otherTime = $_POST['other-time'];
}else{
    $otherNotified = '';
    $otherName = '';
    $otherTime = '';
}
$personType = $_POST['person-type'];
$personName = $_POST['person-name'];
$personId = $_POST['person-id'];
$personOrg = $_POST['person-org'];

$officerName = $_POST['officer-name'];
$incidentDescription = $_POST['incident-description'];
$resultingAction = $_POST['resulting-action'];
for($j = 0; $j<count($personName); $j++){
        $enterCallReportWitnsses = insert_emma_call_report_witness($personType[$j], $personName[$j], $personId[$j], $personOrg[$j], $reportNum);
//            $fvmdb->query("
//                    insert into emma_call_report_witness(
//                    type,
//                    name,
//                    phone,
//                    org,
//                    call_report_number
//                    )VALUES (
//                    $personType[$j],
//                    $personName[$j],
//                    $personId[$j],
//                    $personOrg[$j],
//                    $reportNum
//                    )
//                    ");
}
        $enterCallReport = insert_emma_call_reports_basic($reportNum, $clientId, $reportDate.' '.$reportTime, $locationId, $incidentDescription, $resultingAction);
//            $fvmdb->query("
//                    insert into emma_call_reports(
//                    report_number,
//                    plan_id,
//                    report_date,
//                    site_id,
//                    incident_description,
//                    incident_followup
//                    )VALUES (
//                    $reportNum,
//                    $clientId,
//                    $reportDate.' '.$reportTime,
//                    $locationId,
//                    $incidentDescription,
//                    $resultingAction
//                    )
//                    ");
                //header
                $pdf->SetFont('Arial', '', 20);
                $pdf->SetTextColor(0,120,193);
                $pdf->SetXY(0, 0);
                $pdf->MultiCell(0, 26, $clientName.' Call Report', '', 'C', 0);
                //general info
                $pdf->setFont('Arial', '',10);
                $pdf->SetXY(15, 20);
                $pdf->MultiCell(100, 26, 'Reported By:___________________', '', 'L', 0);
                $pdf->SetXY(75, 20);
                $pdf->MultiCell(100, 26, 'Date of Report:_________________', '', 'L', 0);
                $pdf->SetXY(15, 30);
                $pdf->MultiCell(100, 26, 'Post:______________________', '', 'L', 0);
                $pdf->SetXY(68, 30);
                $pdf->MultiCell(130, 26, 'Location:_________________________________________________________', '', 'L', 0);
                $pdf->SetXY(135, 20);
                $pdf->MultiCell(95, 26, 'Report Number:_________________', '', 'L', 0);

                $pdf->SetTextColor(0,0,0);
                $pdf->SetXY(36, 20);
                $pdf->MultiCell(100, 26, $clientName, '', 'L', 0);
                $pdf->SetXY(100, 20);
                $pdf->MultiCell(100, 26, $reportDate.' '.$reportTime, '', 'L', 0);
                $pdf->SetXY(32, 30);
                $pdf->MultiCell(100, 26, $reportPost, '', 'L', 0);
                $pdf->SetXY(83, 30);
                $pdf->MultiCell(130, 26, $locationName.': '.$incidentAddress.' '.$incidentCity.', '.$incidentState.' '.$incidentZip , '', 'L', 0);
                $pdf->SetXY(161, 20);
                $pdf->MultiCell(95, 26, $reportNum, '', 'L', 0);

                //specific header
                $pdf->SetFont('Arial','',16);
                $pdf->SetTextColor(251,252,252);
                $pdf->SetFillColor(0,120,193);
                $pdf->SetXY(15, 50);
                $pdf->MultiCell(180, 10, 'Incident Information', '', 'C', true);
                //notified services header
                $pdf->setFont('Arial', '',10);
                $pdf->SetTextColor(0,120,193);
                $pdf->SetXY(15,52);
                $pdf->MultiCell(50,26,'Notified Services:','','L',0);
                //police/fire notified
                $pdf->SetFillColor(209,211,212);
                $pdf->SetTextColor(0,0,0);
                $pdf->SetXY(20, 70);
                $pdf->MultiCell(37, 4, 'Police/Fire', '', 'L', true);
                $pdf->SetXY(50, 70);
                $pdf->MultiCell(37, 4, $_POST['police-yn'], '', 'L', true);
                $pdf->SetXY(70, 70);
                $pdf->MultiCell(100, 4, $policeName, '', 'L', true);
                $pdf->SetXY(150, 70);
                $pdf->MultiCell(37, 4, $policeTime, '', 'L', true);
                //reslife notified
                $pdf->SetXY(20, 75);
                $pdf->MultiCell(37, 4, 'Res-Life', '', 'L', 0);
                $pdf->SetXY(50, 75);
                $pdf->MultiCell(37, 4, $_POST['reslife-yn'], '', 'L', 0);
                $pdf->SetXY(70, 75);
                $pdf->MultiCell(100, 4, $reslifeName, '', 'L', 0);
                $pdf->SetXY(150, 75);
                $pdf->MultiCell(37, 4, $reslifeTime, '', 'L', 0);
                //other notified
            if(!empty($otherNotified)) {
                $pdf->SetXY(20, 80);
                $pdf->MultiCell(37, 4, $otherNotified, '', 'L', true);
                $pdf->SetXY(50, 80);
                $pdf->MultiCell(37, 4, $_POST['other-yn'], '', 'L', true);
                $pdf->SetXY(70, 80);
                $pdf->MultiCell(100, 4, $otherName, '', 'L', true);
                $pdf->SetXY(150, 80);
                $pdf->MultiCell(37, 4, $otherTime, '', 'L', true);
            }
                //Witnesses involved header
                $pdf->setFont('Arial', '',10);
                $pdf->SetTextColor(0,120,193);
                $pdf->SetXY(15,82);
                $pdf->MultiCell(50,26,'Witnesses/Parties Involved:','','L',0);

                //Witnesses
                $pdf->setTextColor(0,0,0);
                for($i = 0; $i < count($personName); $i++) {
                    if (!empty($personName[$i])) {
                        if($i % 2 == 0){
                            $fill = 'true';
                        }else{
                            $fill = '0';
                        }
                        $pdf->SetXY(20, 100 + ($i * 5));
                        $pdf->MultiCell(100, 4, $personType[$i], '', 'L', $fill);
                        $pdf->SetXY(50, 100 + ($i * 5));
                        $pdf->MultiCell(37, 4, $personName[$i], '', 'L', $fill);
                        $pdf->SetXY(100, 100 + ($i * 5));
                        $pdf->MultiCell(50, 4, $personId[$i], '', 'L', $fill);
                        $pdf->SetXY(150, 100 + ($i * 5));
                        $pdf->MultiCell(37, 4, $personOrg[$i], '', 'L', $fill);
                    }
                }
                //officer
                $pdf->SetTextColor(0,120,193);
                $pdf->SetXY(15, 100 + ((count($personName)-1)*5));
                $pdf->MultiCell(100, 26, 'Reporting Officer:________________________', '', 'L', 0);

                $pdf->SetTextColor(0,0,0);
                $pdf->SetXY(44, 100 + ((count($personName)-1)*5));
                $pdf->MultiCell(100, 26, $officerName, '', 'L', 0);

                //description
                $pdf->SetTextColor(0,120,193);
                $pdf->SetXY(15,110 + ((count($personName)-1)*5));
                $pdf->MultiCell(100, 26, 'Incident Description:', '', 'L', 0);
                $pdf->SetFillColor(209,211,212);
                $pdf->SetXY(15, 125 + ((count($personName)-1)*5));
                $pdf->MultiCell(180, 40, '', '', 'L', true);

                $pdf->SetTextColor(0,0,0);
                $pdf->SetFillColor(209,211,212);
                $pdf->SetXY(16, 126 + ((count($personName)-1)*5));
                $pdf->MultiCell(178, 4, $incidentDescription, '', 'L', 0);

                //after
                $pdf->SetTextColor(0,120,193);
                $pdf->SetXY(15,160 + ((count($personName)-1)*5));
                $pdf->MultiCell(100, 26, 'Results of Investigation & Action Taken:', '', 'L', 0);
                $pdf->SetXY(15, 175 + ((count($personName)-1)*5));
                $pdf->MultiCell(180, 40, '', '', 'L', true);

                $pdf->SetTextColor(0,0,0);
                $pdf->SetXY(16, 176 + ((count($personName)-1)*5));
                $pdf->MultiCell(178, 4, $resultingAction, '', 'L', true);

                //supervisor sign
                $pdf->SetTextColor(0,120,193);
                $pdf->SetXY(15,215 + ((count($personName)-1)*5));
                $pdf->MultiCell(75, 26, 'Supervisor Name:__________________', '', 'L', 0);
                $pdf->SetXY(80,215 + ((count($personName)-1)*5));
                $pdf->MultiCell(90, 26, 'Supervisor Signature:______________________', '', 'L', 0);
                $pdf->SetXY(160,215 + ((count($personName)-1)*5));
                $pdf->MultiCell(75, 26, 'Date:____________', '', 'L', 0);

                $pdf->Output();

?>
