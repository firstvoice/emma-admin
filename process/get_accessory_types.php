<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 5/29/2019
 * Time: 3:30 PM
 */

global $fvmdb;
include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$modelID = $fvmdb->real_escape_string($_POST['model-id']);

if (empty($modelID)) $errors['model-id'] = 'No Model selected';

if (empty($errors)) {
  $pads = $fvmdb->query("
    SELECT apt.aed_pad_type_id, apt.type, apt.pediatric
    FROM aed_pad_types_on_models pad_tom
    JOIN aed_pad_types apt ON pad_tom.aed_pad_type_id = apt.aed_pad_type_id
    WHERE pad_tom.aed_model_id = ".$modelID."
   ");
  $data['pads'] = array();
  while($pad = $pads->fetch_assoc()) {
    $data['pads'][] = $pad;
  }
  $paks = $fvmdb->query("
    SELECT apt.aed_pak_type_id, apt.type, apt.pediatric
    FROM aed_pak_types_on_models pad_tom
    JOIN aed_pak_types apt ON pad_tom.aed_pak_type_id = apt.aed_pak_type_id
    WHERE pad_tom.aed_model_id = ".$modelID."
   ");
  $data['paks'] = array();
  while($pak = $paks->fetch_assoc()) {
    $data['paks'][] = $pak;
  }
  $batteries = $fvmdb->query("
    SELECT abt.aed_battery_type_id, abt.type
    FROM aed_battery_types_on_models battery_tom
    JOIN aed_battery_types abt ON battery_tom.aed_battery_type_id = abt.aed_battery_type_id
    WHERE battery_tom.aed_model_id = ".$modelID."
   ");
  $data['batteries'] = array();
  while($battery = $batteries->fetch_assoc()) {
    $data['batteries'][] = $battery;
  }
  $accessories = $fvmdb->query("
    SELECT aat.aed_accessory_type_id, aat.type
    FROM aed_accessory_types_on_models accessory_tom
    JOIN aed_accessory_types aat ON accessory_tom.aed_accessory_type_id = aat.aed_accessory_type_id
    WHERE accessory_tom.aed_model_id = ".$modelID."
   ");
  $data['accessories'] = array();
  while($accessory = $accessories->fetch_assoc()) {
    $data['accessories'][] = $accessory;
  }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);