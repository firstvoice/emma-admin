<?php

include('../include/db.php');
include('../include/processing.php');

$columns = $_GET['columns'];
$draw = $fvmdb->real_escape_string($_GET['draw']);
$length = $fvmdb->real_escape_string($_GET['length']);
$order = $_GET['order'];
$search = $_GET['search'];
$start = $fvmdb->real_escape_string($_GET['start']);
$active = $fvmdb->real_escape_string($_GET['active']);
$emmaPlanId = $fvmdb->real_escape_string($_GET['emma-plan-id']);

$data = array();

$guestCodes = select_getguestcodes_guestcodes($emmaPlanId,$length,$start, $_GET['columns'][0]['search']['value'], $_GET['columns'][1]['search']['value'], $_GET['columns'][2]['search']['value'], $_GET['columns'][3]['search']['value']);
//    $fvmdb->query("
//    SELECT SQL_CALC_FOUND_ROWS gc.guest_code_id, gc.start_date, gc.end_date , gc.duration_minutes, group_concat(DISTINCT eg.name ORDER BY eg.name) AS `group`
//    FROM emma_guest_codes gc
//    LEFT JOIN emma_groups eg ON gc.group_id = eg.emma_group_id
//    WHERE gc.emma_plan_id = '" . $emmaPlanId . "'
//    AND (" . ($_GET['columns'][0]['search']['value'] != "" ? "gc.guest_code_id like ('%" . $_GET['columns'][0]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][1]['search']['value'] != "" ? "gc.start_date like ('%" . $_GET['columns'][1]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][2]['search']['value'] != "" ? "gc.duration like ('%" . $_GET['columns'][2]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][3]['search']['value'] != "" ? "eg.name like ('" . $_GET['columns'][3]['search']['value'] . "%')" : "1") . ")
//    GROUP BY gc.guest_code_id
//    ORDER BY gc.start_date, gc.guest_code_id
//    LIMIT " . $length . " OFFSET " . $start . "
//");
$found = select_FOUND_ROWS();
//    $fvmdb->query("
//    SELECT FOUND_ROWS()
//");
$count = $found->fetch_assoc();


$data['iTotalRecords'] = $count['FOUND_ROWS()'];
$data['iTotalDisplayRecords'] = $count['FOUND_ROWS()'];
$data['sEcho'] = $draw;
$data['aaData'] = array();
while ($guestCode = $guestCodes->fetch_assoc()) {
    $durationHours = 0;
    $dateSpan = '';
    //get status
    if((strtotime($guestCode['end_date']) < strtotime('now')) || (strtotime($guestCode['start_date']) > strtotime('now'))){
        $guestCode['status'] = '<span style="color: red">Inactive</span>';
    }else{
        $guestCode['status'] = '<span style="color: green">Active</span>';
    }
    //get duration in hour if over 1 hour
    if($guestCode['duration_minutes'] < 60){
        $durationHours = $guestCode['duration_minutes'] . ' Minutes';
    }else {
        $durationHours = $guestCode['duration_minutes'] / 60 . ' Hours';
    }
    //change time span to easy to read
    if($guestCode['start_date'] != '' && $guestCode['end_date'] != '') {
        $dateSpan = date('m/d/Y',strtotime($guestCode['start_date'])) . ' - ' . date('m/d/Y',strtotime($guestCode['end_date']));
    }

    $guestCode['durationHours'] = $durationHours;
    $guestCode['dateSpan'] = $dateSpan;
    $data['aaData'][] = $guestCode;
}

echo json_encode($data);
