<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/6/2019
 * Time: 3:06 PM
 */

include('../include/db.php');
include('../include/processing.php');
$asset_id = $fvmdb->real_escape_string($_GET['assetid']);
$type_id = $fvmdb->real_escape_string($_GET['typeid']);

$data = array();
$errors = array();



if(empty($errors))
{
    $items = select_assets($asset_id,$type_id);
//        $fvmdb->query("
//    SELECT eatd.*, eado.name as dropdown_name, eado.id as dropdown_id, eaci.info, eaci.date_time, eaci.id as custominfoid, eaci.dropdown as selecteddropdown
//    FROM emma_asset_type_display as eatd
//    LEFT JOIN emma_assets_dropdown_options as eado ON eatd.dropdown_id = eado.id AND eado.display = 1
//    LEFT JOIN emma_assets_custom_info as eaci on eaci.emma_asset_id = '".$asset_id."'
//    WHERE eatd.asset_type_id = '".$type_id."'
//    AND eatd.active = 1
//    ");

    $dropdowngrab = select_assetsdropdowns($asset_id);
//        $fvmdb->query("
//    SELECT eaci.id,eaci.dropdown
//    FROM emma_assets_custom_info as eaci
//    WHERE eaci.emma_asset_id = '".$asset_id."'
//    AND eaci.dropdown IS NOT NULL
//    ");

    $images = select_assetstypes($type_id);
//        $fvmdb->query("
//    SELECT eai.image
//    FROM emma_asset_types as eat
//    LEFT JOIN emma_asset_icons as eai on eat.emma_asset_icon_id = eai.asset_icon_id
//    WHERE eat.emma_asset_type_id = '".$type_id."'
//    ");


   while($item = $items->fetch_assoc())
   {
       $data['item'][] = $item;
   }
    while($dropdowncontent = $dropdowngrab->fetch_assoc())
    {
        $data['content'][] = $dropdowncontent;
    }
    while($image = $images->fetch_assoc())
    {
        $data['image'][] = $image;
    }

}
//$data['success'] = empty($errors);
echo json_encode($data);