<?php

global $fvmdb;
include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$responderId = $fvmdb->real_escape_string($_POST['responder-id']);

if (empty($responderId)) {
  $errors['responder-id'] = 'Responder Id not provided';
}

if (empty($errors)) {
  $deactivateResponder = $fvmdb->query("
   update persons p
   set display = 'no'
   where p.id = '" . $responderId . "'
 ");
  if (!$deactivateResponder) {
    $errors['sql'] = $fvmdb->error;
  }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);