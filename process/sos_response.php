<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/23/2017
 * Time: 12:16 PM
 */

require('../include/db.php');
include('../include/processing.php');

$data = array();
$errors = array();

$data = array();
$errors = array();
$sosId = $fvmdb->real_escape_string($_POST['sos-id']);
$comments = $fvmdb->real_escape_string($_POST['comments']);
$userId = $fvmdb->real_escape_string($_POST['user-id']);


if (empty($errors)) {
    $insertSosResponse = insert_sos_response($userId,$sosId,$comments);

    if (!$insertSosResponse) {
        $errors['insert'] = 'Could not send SOS Response: ' . $fvmdb->error;
    }

}
$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);