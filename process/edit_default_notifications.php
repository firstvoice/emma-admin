<?php

require('../include/db.php');
include('../include/processing.php');
include('../include/process_cookie.php');

$data = array();
$errors = array();
$userId = $emmadb->real_escape_string($_POST['uid']);
$planId = $emmadb->real_escape_string($_POST['pid']);
$push_ = $emmadb->real_escape_string($_POST['default-notification']);
$email_ = $emmadb->real_escape_string($_POST['default-email']);
$text_ = $emmadb->real_escape_string($_POST['default-text']);
$push = 0;
$text = 0;
$email = 0;


if(!empty($push_)){
    $push = 1;
}
if(!empty($email_)){
    $email = 1;
}
if(!empty($text_)){
    $text = 1;
}
//check user is admin of plan
$admincheckQuery = select_userIsInAdminGroup($userId, $planId);
if(!($admincheckQuery->num_rows > 0)){
    $errors['1'] = 'invalid credentials';
}


//update defaults
if(empty($errors)) {
    $update = update_plan_default_notifications($planId, $push, $email, $text);
}

//$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);