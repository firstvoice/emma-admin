<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/21/2019
 * Time: 10:43 AM
 */

include('../include/db.php');
include('../include/processing.php');

require('../vendor/php-jwt-master/src/JWT.php');
require('../vendor/php-jwt-master/src/BeforeValidException.php');
require('../vendor/php-jwt-master/src/ExpiredException.php');
require('../vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('../config/config.json'));

$USER = null;

$token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key, array('HS512'));

$USER = $token->data;



$script = $_POST['script'];
$broadcast = $_POST['broadcast'];
$group = $_POST['group'];
$name = $_POST['name'];
$text = $_POST['text'];

$data = array();
$errors = array();
$leads = array();

//$data['leads'] = $leads;
if(empty($script) || empty($group) || empty($broadcast))
{
    $errors['Empty'] = 'Not all data was submitted successfully';
}

if (empty($errors)) {
    //All the arrays ($script) are of the same length, so we can just say for the length of one array and then we loop through them all.
    for($x = 0; $x < count($script); $x++) {
        $script_val = $script[$x];
        $broadcast_val = $broadcast[$x];
        $group_val = $group[$x];
        $name_val = $name[$x];
        $text_val = $text[$x];

        $updateScript = insert_emma_scripts_with_massNotificationType($script_val, $USER->emma_plan_id, $group_val, null, $name_val, $text_val, $broadcast_val);
//            $fvmdb->query("
//    insert into emma_scripts (
//      emma_plan_id,
//      emma_script_type_id,
//      `name`,
//      text,
//      emma_script_group_id,
//      emma_broadcast_type_id,
//      emma_mass_notification_type_id
//    ) values (
//      '" . $USER->emma_plan_id . "',
//      '" . $script_val . "',
//      '" . $name_val . "',
//      '" . $text_val . "',
//      '" . $group_val . "',
//      null,
//      '". $broadcast_val ."'
//    )
//  ");
        if (!$updateScript) {
            $errors['sql'] = $fvmdb->error;
        }



    }
}
$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);