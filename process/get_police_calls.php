<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/17/2019
 * Time: 10:14 AM
 */

include('../include/db.php');
include('../include/processing.php');

$columns = $_GET['columns'];
$draw = $fvmdb->real_escape_string($_GET['draw']);
$length = $fvmdb->real_escape_string($_GET['length']);
$order = $_GET['order'];
$search = $_GET['search'];
$start = $fvmdb->real_escape_string($_GET['start']);
$active = $fvmdb->real_escape_string($_GET['active']);
$emmaPlanId = $fvmdb->real_escape_string($_GET['emma-plan-id']);

$orderString = $columns[$order[0]['column']]['data'] . ' ' . $order[0]['dir'];

$data = array();

$events = select_policcalls_events($emmaPlanId, $active, $orderString, $length, $start, $_GET['columns'][0]['search']['value'], $_GET['columns'][1]['search']['value'], $_GET['columns'][2]['search']['value'], $_GET['columns'][3]['search']['value'], $_GET['columns'][4]['search']['value'], $_GET['columns'][5]['search']['value']);
//    $fvmdb->query("
//    SELECT SQL_CALC_FOUND_ROWS ecl.call_id, ecl.emergency_id, ecl.call_datetime, ecl.active, ecl.latitude, ecl.longitude, et.name AS type, CONCAT(u.firstname, ' ', u.lastname) AS username, u.id AS userid, u.username AS useremail
//    FROM emma_911_call_log AS ecl
//    JOIN users u on ecl.userid = u.id
//    LEFT JOIN emergencies e on ecl.emergency_id = e.emergency_id
//    LEFT JOIN emergency_types et on et.emergency_type_id = e.emergency_type_id
//    WHERE (" . ($active != '' ? "ecl.active = " . $active : "1") . ")
//    AND (" . ($emmaPlanId != '' ? "ecl.plan_id = " . $emmaPlanId : "1") . ")
//    AND (" . ($_GET['columns'][0]['search']['value'] != "" ? "et.name like ('%" . $_GET['columns'][0]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][1]['search']['value'] != "" ? "CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') like ('%" . $_GET['columns'][1]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][2]['search']['value'] != "" ? "ecl.call_datetime like ('%" . $_GET['columns'][2]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][3]['search']['value'] != "" ? "ecl.latitude like ('%" . $_GET['columns'][3]['search']['value'] . "%')" : "1").")
//    AND (" . ($_GET['columns'][4]['search']['value'] != "" ? "ecl.longitude like ('%" . $_GET['columns'][4]['search']['value'] . "%')" : "1").")
//    AND (" . ($_GET['columns'][5]['search']['value'] != "" ? "(ecl.active = '0' AND 'Closed' like ('%" . $_GET['columns'][5]['search']['value'] . "%')) OR (ecl.active = '1' AND 'Active' like ('%" . $_GET['columns'][5]['search']['value'] . "%'))" : "1") . ")
//    ORDER BY ". $orderString .", ecl.emergency_id desc
//    LIMIT " . $length . " OFFSET " . $start . "
//");
$found = select_FOUND_ROWS();
//    $fvmdb->query("
//    SELECT FOUND_ROWS()
//");
$count = $found->fetch_assoc();

$data['iTotalRecords'] = $count['FOUND_ROWS()'];
$data['iTotalDisplayRecords'] = $count['FOUND_ROWS()'];
$data['sEcho'] = $draw;
$data['aaData'] = array();
$data['sortString'] = $orderString;
while ($event = $events->fetch_assoc()) {
    $data['aaData'][] = $event;
}
echo json_encode($data);