<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/23/2017
 * Time: 12:16 PM
 */

require('../include/db.php');
include('../include/processing.php');
include('swiftmailer/swift_required.php');

$data = array();
$errors = array();

$userId = $fvmdb->real_escape_string($_POST['user-id']);
$securityId = $fvmdb->real_escape_string($_POST['security-id']);
$creatorId = $fvmdb->real_escape_string($_POST['creator-id']);
$message = $fvmdb->real_escape_string($_POST['message']);
$alertNotification = $fvmdb->real_escape_string($_POST['alert-notification']);
$alertEmail = $fvmdb->real_escape_string($_POST['alert-email']);

$topics = '/topics/emma-error';
$FcmServerKey = 'AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db';

$users = select_user_from_userID($userId);
//    $fvmdb->query("
//    SELECT *
//    FROM users
//    WHERE id = '" . $userId . "'
//");
if (!$user = $users->fetch_assoc()) {
  $errors['user'] = 'Can not find user';
} else {
  $topics = '/topics/emma-' . $user['emma_plan_id'];
}
if (empty($errors)) {
  $insertSystemMessage = insert_emma_security_messages_typeWithoutDescriptionComments($securityId, $userId, $message, date('Y-m-d H:i:s'), 0);
//      $fvmdb->query("
//        INSERT INTO emma_security_messages (
//          security_id,
//          message,
//          created_by_id,
//          created_date,
//          type
//        ) VALUES (
//          '" . $securityId . "',
//          '" . $message . "',
//          '" . $userId . "',
//          '" . date('Y-m-d H:i:s') . "',
//          '0'
//        )
//    ");
  $messageId = $emmadb->insert_id;

  if(isset($alertNotification) && $alertNotification == 'on') {
    $postData = array(
      'to' => $topics,
      'time_to_live' => 600,
      'content_available' => true,
      'data' => array(
        'USER_ALERT' => 'true',
        'USER_SECURITY' => 'true',
        'security_id' => $securityId,
        'users' => array(),
        'message' => $message,
        'message_id' => $messageId
      )
    );
    $postData['data']['users'][] = $creatorId;

    $ch = curl_init('https://fcm.googleapis.com/fcm/send');
    curl_setopt_array($ch, array(
      CURLOPT_POST => TRUE,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_HTTPHEADER => array(
        'Authorization: key=' . $FcmServerKey,
        'Content-Type: application/json'
      ),
      CURLOPT_POSTFIELDS => json_encode($postData)
    ));
    $response = curl_exec($ch);
    $responseData = json_decode($response);
    $data['response'] = $responseData;

    //ios notification
      //ios close security APNS
      $IOSCheckquery = select_notifycreator_iosCheckQuery($securityId);
//          $fvmdb->query("
//        SELECT iosFireBaseToken
//        FROM emma_securities
//        WHERE emma_security_id = '". $securityId ."'
//    ");
      if($IOSCheckResult = $IOSCheckquery->fetch_assoc()) {
          if(!empty($IOSCheckResult['iosFireBaseToken'])) {

              $token = $IOSCheckResult['iosFireBaseToken'];

              $postIOSData = array(
                  'to' => $token,
                  'time_to_live' => 600,
                  'priority'=>'high',
                  'notification' => array(
                      'title' => 'Admin Alert',
                      'body' => $message,
                      'sound' => 'notification.aiff',
                      'badge' => '1'
                  ),
                  'data' => array(
                      'GROUP_ALERT' => 'true',
                      'event' => 'Admin Alert',
                      'emergency_lat' => $latitude,
                      'emergency_lng' => $longitude,
                      'emergency_address' => $address,
                      'security_id' => $securityId
                  )
              );


              $ch = curl_init('https://fcm.googleapis.com/fcm/send');
              curl_setopt_array($ch, array(
                  CURLOPT_POST => TRUE,
                  CURLOPT_RETURNTRANSFER => TRUE,
                  CURLOPT_HTTPHEADER => array(
                      'Authorization: key=' . $FcmServerKey,
                      'Content-Type: application/json'
                  ),
                  CURLOPT_POSTFIELDS => json_encode($postIOSData)
              ));
              $IOSresponse = curl_exec($ch);
              $IOSresponseData = json_decode($IOSresponse);
              $data['IOSresponse'] = $IOSresponseData;

          }
      }
  }

  if (isset($alertEmail) && $alertEmail == 'on') {
    $creators = select_notifycreator_creators($creatorId);
//        $fvmdb->query("
//      select *
//      from users
//      where id = '".$creatorId."'
//    ");
    if($creator = $creators->fetch_assoc()) {
//      $transport = Swift_MailTransport::newInstance();
      $mailer = Swift_Mailer::newInstance($SMT);
      $swiftMessage = Swift_Message::newInstance()
        ->setSubject('EMMA Security')
        ->setFrom(array('donotreply@emmaadmin.com' => 'EMMA Admin'))
        ->setTo(array($creator['username']))
        ->setBody($message, 'text/html');
      $result = $mailer->send($swiftMessage);
    }
  }
}
$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);