<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/31/2017
 * Time: 12:35 PM
 */

require('../include/db.php');
include('../include/processing.php');


$data = array();
$errors = array();

$active = $fvmdb->real_escape_string($_GET['active']);

$data['emergencies'] = array();

if (empty($errors)) {
    $emergencies = select_getEmergencies_emergencies($active);
//        $fvmdb->query("
//        SELECT e.emergency_id, et.name AS type, CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') AS user, e.created_date
//        FROM emergencies e
//        JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
//        JOIN users u ON e.created_by_id = u.id
//        WHERE (" . ($active != '' ? "e.active = " . $active : "1") . ")
//        ORDER BY e.created_date DESC
//    ");
    while ($emergency = $emergencies->fetch_assoc()) {
        $data['emergencies'][] = $emergency;
    }
}
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);
