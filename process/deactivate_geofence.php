<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 4/6/2018
 * Time: 1:23 PM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$geofenceId = $fvmdb->real_escape_string($_POST['geofence-id']);

if (empty($geofenceId)) {
    $errors['fence-id'] = 'Geofence Id not provided';
}

if (empty($errors)) {
    $deleteGeofence = update_geofence_active_with_geofenceID($geofenceId, 0);
//    $fvmdb->query("
//    update emma_geofence_locations
//    set active = 0
//    where id = '" . $geofenceId . "'
//  ");
    if (!$deleteGeofence) {
        $errors['sql'] = $fvmdb->error;
    }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);