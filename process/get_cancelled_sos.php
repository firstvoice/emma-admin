<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 8/28/2017
 * Time: 10:04 AM
 */

include('../include/db.php');
include('../include/processing.php');

$columns = $_GET['columns'];
$draw = $fvmdb->real_escape_string($_GET['draw']);
$length = $fvmdb->real_escape_string($_GET['length']);
$order = $_GET['order'];
$search = $_GET['search'];
$start = $fvmdb->real_escape_string($_GET['start']);
$active = $fvmdb->real_escape_string($_GET['active']);
$emmaPlanId = $fvmdb->real_escape_string($_GET['emma-plan-id']);

$orderString = $columns[$order[0]['column']]['data'] . ' ' . $order[0]['dir'];


$data = array();

$events = select_getcancelledSOS_events($emmaPlanId,$orderString,$length,$start);
//    $fvmdb->query("
//    SELECT SQL_CALC_FOUND_ROWS s.emma_sos_id, s.cancelled_date, CONCAT(u.firstname, ' ', u.lastname) AS user, s.cancelled_lat as latitude, s.cancelled_lng as longitude, u.id as user_id, u.phone, u.username as email
//    FROM emma_sos s
//    JOIN users u ON s.created_by_id = u.id AND u.emma_plan_id = '".$emmaPlanId."'
//    AND s.cancelled_date IS NOT NULL
//    ORDER BY ". $orderString .", created_date desc
//    LIMIT " . $length . " OFFSET " . $start . "
//");
$found = select_FOUND_ROWS();
//    $fvmdb->query("
//    SELECT FOUND_ROWS()
//");
$count = $found->fetch_assoc();

$data['iTotalRecords'] = $count['FOUND_ROWS()'];
$data['iTotalDisplayRecords'] = $count['FOUND_ROWS()'];
$data['sEcho'] = $draw;
$data['aaData'] = array();
while ($event = $events->fetch_assoc()) {
  $data['aaData'][] = $event;
}

echo json_encode($data);