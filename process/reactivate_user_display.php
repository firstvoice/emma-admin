<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 4/6/2018
 * Time: 1:23 PM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$userId = $fvmdb->real_escape_string($_POST['user-id']);

if (empty($userId)) {
    $errors['user-id'] = 'User Id not provided';
}

if (empty($errors)) {
    $reactivateUser = update_user_display_with_userID($userId, 'yes');
//  $fvmdb->query("
//    update users
//    set
//      display = 'no'
//    where id = '" . $userId . "'
//  ");
    if (!$reactivateUser) {
        $errors['sql'] = $fvmdb->error;
    }
}


$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);