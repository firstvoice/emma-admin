 <?php

$filename .= 'Assets';
$csv .=
    'Assets' . $csvbr;
$csv .=
    '"EMMA Asset ID"' . $csvsp .
    '"EMMA Plan"' . $csvsp .
    '"EMMA Asset Type"' . $csvsp .
    '"Created By"' . $csvsp .
    '"Created Date"' . $csvsp .
    '"Latitude"' . $csvsp .
    '"Longitude"' . $csvbr;
$assets = select_reportAsset_assets($plan);

//    $fvmdb->query("
//    SELECT s.*, t.name AS type_name, p.name AS plan_name, CONCAT(u.firstname , ' ' , u.lastname) AS created_name
//    FROM emma_assets s
//    JOIN emma_plans p ON s.emma_plan_id = p.emma_plan_id
//    JOIN emma_asset_types t ON s.emma_asset_type_id = t.emma_asset_type_id
//    JOIN users u ON s.created_by_id = u.id
//    WHERE s.emma_plan_id = '". $plan ."'
//    and s.active = '1'
//");
while ($asset = $assets->fetch_assoc()) {
    $csv .=
        '"' . $asset['emma_asset_id'] . '"' . $csvsp .
        '"' . $asset['plan_name'] . '"' . $csvsp .
        '"' . $asset['type_name'] . '"' . $csvsp .
        '"' . $asset['created_name'] . '"' . $csvsp .
        '"' . $asset['created_date'] . '"' . $csvsp .
        '"' . $asset['latitude'] . '"' . $csvsp .
        '"' . $asset['longitude'] . '"' . $csvbr;
}
