<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 6/13/2019
 * Time: 12:53 PM
 */


$filename .= 'EMMA LOCKDOWN!';
$csv .=
    'EMMA LOCKDOWN! ' . $csvbr; //csvbr is new line character.
$csv .=
    '"Creator"' . $csvsp .
    '"Created Date"' . $csvsp .
    '"Created Latitude"' . $csvsp .
    '"Help Alert Longitude"' . $csvsp .
    '"Drill"' . $csvsp .
    '"Plan Name"' . $csvbr;


    $emma_all = select_reportLOCKDOWN_lockdown($plan);


while($emma = $emma_all->fetch_assoc())
{
    if ($emma['drill'] == 0){
        $drill = 'no';
    }else{
        $drill = 'yes';

    }

    $csv .=
        '"' .$emma['user_name']. '"' . $csvsp .
        '"' .$emma['created_date']. '"' . $csvsp .
        '"' .$emma['created_lat']. '"' . $csvsp .
        '"' .$emma['created_lng']. '"' . $csvsp .
        '"' .$drill.'"' . $csvsp .
        '"' .$emma['plan_name']. '"' . $csvbr;
}
