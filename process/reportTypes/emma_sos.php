<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 6/13/2019
 * Time: 12:53 PM
 */


$filename .= 'EMMA SOS';
$csv .=
    'EMMA SOS ' . $csvbr; //csvbr is new line character.
$csv .=
    '"User"' . $csvsp .
    '"Created Date"' . $csvsp .
    '"SOS Date"' . $csvsp .
    '"Help Alert Date"' . $csvsp .
    '"Help Alert Lat"' . $csvsp .
    '"Help Alert Lng"' . $csvsp .
    '"Initial Lat"' . $csvsp .
    '"Initial Lng"' . $csvsp .
    '"Cancelled Date"' . $csvsp .
    '"Cancelled Lat"' . $csvsp .
    '"Cancelled Lng"' . $csvsp .
    '"Closed By"' . $csvsp .
    '"Closed Date"' . $csvsp .
    '"Closed Comments"' . $csvsp .
    '"Closed Lat"' . $csvsp .
    '"Closed Lng"' . $csvsp .
    '"Plan Name"' . $csvbr;


    $emma_all = select_reportSOS_sos($plan);
//        $fvmdb->query("
//    SELECT es.*, concat(u.firstname , ' ' , u.lastname) as user_name, concat(uc.firstname , ' ' , uc.lastname) as closed_user_name, ep.name as plan_name
//    FROM emma_sos es
//    LEFT JOIN users u on u.id = es.created_by_id
//    left JOIN users uc on uc.id = es.closed_by_id
//    JOIN emma_plans ep on ep.emma_plan_id = es.plan_id
//    WHERE es.plan_id = '".$plan."'
//    ");

while($emma = $emma_all->fetch_assoc())
{
    $csv .=
        '"' .$emma['user_name']. '"' . $csvsp .
        '"' .$emma['created_date']. '"' . $csvsp .
        '"' .$emma['sos_date']. '"' . $csvsp .
        '"' .$emma['help_date']. '"' . $csvsp .
        '"' .$emma['help_lat']. '"' . $csvsp .
        '"' .$emma['help_lng']. '"' . $csvsp .
        '"' .$emma['pending_lat']. '"' . $csvsp .
        '"' .$emma['pending_lng']. '"' . $csvsp .
        '"' .$emma['cancelled_date']. '"' . $csvsp .
        '"' .$emma['cancelled_lat']. '"' . $csvsp .
        '"' .$emma['cancelled_lng']. '"' . $csvsp .
        '"' .$emma['closed_user_name']. '"' . $csvsp .
        '"' .$emma['closed_date']. '"' . $csvsp .
        '"' .$emma['closed_comments']. '"' . $csvsp .
        '"' .$emma['closed_lat']. '"' . $csvsp .
        '"' .$emma['closed_lng']. '"' . $csvsp .
        '"' .$emma['plan_name']. '"' . $csvbr;
}
