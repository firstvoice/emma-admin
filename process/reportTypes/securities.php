<?php

$filename .= 'Securities';
$csv .=
    '"Securities"' . $csvbr;
$csv .=
    '"EMMA Security ID"' . $csvsp .
    '"EMMA Plan"' . $csvsp .
    '"EMMA Site"' . $csvsp .
    '"EMMA Security Type"' . $csvsp .
    '"Created By"' . $csvsp .
    '"Created Date"' . $csvsp .
    '"Created PIN"' . $csvsp .
    '"Latitude"' . $csvsp .
    '"Longitude"' . $csvsp .
    '"Closed By"' . $csvsp .
    '"Closed Date"' . $csvsp .
    '"Description"' . $csvsp .
    '"Comments"' . $csvbr;
$securities = select_reportSecurities_securities($plan);
//    $fvmdb->query("
//    SELECT s.*, t.name AS type_name, i.emma_site_name AS site_name, p.name AS plan_name, u.firstname AS created_first, u.lastname AS created_last, uc.firstname AS closed_first, uc.lastname AS closed_last
//    FROM emma_securities s
//    JOIN emma_plans p ON s.emma_plan_id = p.emma_plan_id
//    JOIN emma_security_types t ON s.emma_security_type_id = t.emma_security_type_id
//    LEFT JOIN emma_sites i ON s.emma_site_id = i.emma_site_id
//    Left JOIN users u ON s.created_by_id = u.id
//    LEFT JOIN users uc ON s.closed_by_id = uc.id
//    WHERE s.emma_plan_id = '". $plan ."'
//");
while ($security = $securities->fetch_assoc()) {
    $csv .=
        '"' . $security['emma_security_id'] . '"' . $csvsp .
        '"' . $security['plan_name'] . '"' . $csvsp .
        '"' . $security['site_name'] . '"' . $csvsp .
        '"' . $security['type_name'] . '"' . $csvsp .
        '"' . $security['created_first'] . ' ' . $security['created_last'] . '"' . $csvsp .
        '"' . $security['created_date'] . '"' . $csvsp .
        '"' . $security['created_pin'] . '"' . $csvsp .
        '"' . $security['latitude'] . '"' . $csvsp .
        '"' . $security['longitude'] . '"' . $csvsp .
        '"' . $security['closed_first'] . ' ' . $security['closed_last'] . '"' . $csvsp .
        '"' . $security['closed_date'] . '"' . $csvsp .
        '"' . $security['description'] . '"' . $csvsp .
        '"' . $security['comments'] . '"' . $csvbr;
}