<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/13/2019
 * Time: 12:46 PM
 */


$filename .= 'Drills';
$csv .=
    '"Drills"' . $csvbr; //csvbr is new line character.
$csv .=
    '"Event ID"' . $csvsp . //bumps into next column
    '"Event Type"' . $csvsp .
    '"Plan Name"' . $csvsp .
    '"Description"' . $csvsp .
    '"Comment"' . $csvsp .
    '"Latitude"' . $csvsp .
    '"Longitude"' . $csvsp .
    '"Nearest Address"' . $csvsp .
    '"911 Status"' . $csvsp .
    '"Created Date"' . $csvsp .
    '"Created By"' . $csvsp .
    '"Closed Date"' . $csvsp .
    '"Closed By"' . $csvsp .
    '"Site Street Address"' . $csvsp .
    '"Site City"' . $csvsp .
    '"Site Contact Name"' . $csvsp .
    '"Site Contact Phone"' . $csvsp .
    '"Site Contact Email"' . $csvbr;

$events_all = select_reportDrills_eventsAll($plan);
//    $fvmdb->query("
//SELECT e.*,es.emma_site_street_address,es.emma_site_city,concat(esc.first_name , ' ' , esc.last_name) as site_contact_name, esc.phone,esc.email, et.name as emergency_type_name, et.badge, et.sound, et.vibrate, u.username as user_created_username,concat(u.firstname , ' ' , u.lastname) as user_created_name, uc.username as user_closed_username, concat(uc.firstname , ' ' , uc.lastname) as user_closed_name, ep.name as plan_name
//FROM emergencies e
//LEFT JOIN emma_sites es on es.emma_site_id = e.emma_site_id
//LEFT JOIN emma_site_contacts esc on esc.emma_site_contact_id = es.emma_site_contact_id
//JOIN emergency_types et on et.emergency_type_id = e.emergency_type_id
//JOIN users u on u.id = e.created_by_id
//JOIN emma_plans ep on ep.emma_plan_id = '".$plan."'
//LEFT JOIN users uc on uc.id = e.closed_by_id
//WHERE e.emma_plan_id = '".$plan."'
//AND e.drill = 1
//");//responses will be duplicate


while($events = $events_all->fetch_assoc())
{
    $csv .=
        '"' . $events['emergency_id'] . '"' . $csvsp .
        '"' . $events['emergency_type_name'] . '"' . $csvsp .
        '"' . $events['plan_name'] . '"' . $csvsp .
        '"' . $events['description'] . '"' . $csvsp .
        '"' . $events['comments'] . '"' . $csvsp .
        '"' . $events['latitude'] . '"' . $csvsp .
        '"' . $events['longitude'] . '"' . $csvsp .
        '"' . $events['nearest_address'] . '"' . $csvsp .
        '"' . $events['called911'] . '"' . $csvsp .
        '"' . $events['created_date'] . '"' . $csvsp .
        '"' . $events['user_created_name'] . '"' . $csvsp .
        '"' . $events['closed_date'] . '"' . $csvsp .
        '"' . $events['user_closed_name'] . '"' . $csvsp .
        '"' . $events['emma_site_street_address'] . '"' . $csvsp .
        '"' . $events['emma_site_city'] . '"' . $csvsp .
        '"' . $events['site_contact_name'] . '"' . $csvsp .
        '"' . $events['phone'] . '"' . $csvsp .
        '"' . $events['email'] . '"' . $csvbr;

        $responses = select_reportDrills_responses($events['emergency_id']);
//            $fvmdb->query("
//            SELECT concat(u.firstname , ' ' , u.lastname) as user_name, ep.name as emma_plan_name, ers.name as response_status, es.comments
//            FROM emergency_responses es
//            join emergency_response_status ers on es.status = ers.emergency_response_status_id
//            JOIN users u on u.id = es.user_id
//            JOIN emma_plans ep on u.emma_plan_id = ep.emma_plan_id
//            WHERE es.emergency_id = '".$events['emergency_id']."'
//        ");
        if(!empty($responses)) {
            $csv .= $csvsp . 'Responses ' . $csvbr;
            $csv .= $csvsp .
                '"User Name"' . $csvsp . //bumps into next colum
                '"Comments"' . $csvsp .
                '"Status"' . $csvsp .
                '"Name"' . $csvbr;
            while ($response = $responses->fetch_assoc()) {
                $csv .= $csvsp .
                    '"' . $response['user_name'] . '"' . $csvsp .
                    '"' . $response['comments'] . '"' . $csvsp .
                    '"' . $response['response_status'] . '"' . $csvsp .
                    '"' . $response['user_name'] . '"' .  $csvbr;
            }
        }
        $csv .= $csvbr;
}



