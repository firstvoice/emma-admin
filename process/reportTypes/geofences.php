<?php

$filename .= 'Geofences';
$csv .=
    '"Geofences"' . $csvbr;
$csv .=

    '"EMMA Plan"' . $csvsp .
    '"Name"' . $csvsp .
    '"Geofence Type"' . $csvsp .
    '"Center Latitude"' . $csvsp .
    '"Center Longitude"' . $csvsp .
    '"Radius"' . $csvsp .
    '"NW Latitude"' . $csvsp .
    '"NW Longitude"' . $csvsp .
    '"SE Latitude"' . $csvsp .
    '"SE Longitude"' . $csvsp .
    '"Main Fence"' . $csvsp .
    '"Created Date"' . $csvsp .
    '"Created By"' . $csvbr;
$securities = select_reportGeofences_fences($plan);
//    $fvmdb->query("
//    SELECT s.*, sp.fence_name AS main_fence_name, CONCAT(u.firstname,' ',u.lastname) AS created_name, p.name AS plan_name
//    FROM emma_geofence_locations s
//    JOIN emma_plans p ON s.plan_id = p.emma_plan_id
//    LEFT JOIN emma_geofence_locations sp ON s.main_fence = sp.id
//    JOIN users u ON s.created_by = u.id
//    WHERE s.plan_id = '". $plan ."'
//");
while ($security = $securities->fetch_assoc()) {
    $csv .=

        '"' . $security['plan_name'] . '"' . $csvsp .
        '"' . $security['fence_name'] . '"' . $csvsp .
        '"' . $security['type'] . '"' . $csvsp .
        '"' . $security['center_lat'] . '"' . $csvsp .
        '"' . $security['center_lng'] . '"' . $csvsp .
        '"' . $security['radius'] . '"' . $csvsp .
        '"' . $security['nw_corner_lat'] . '"' . $csvsp .
        '"' . $security['nw_corner_lng'] . '"' . $csvsp .
        '"' . $security['se_corner_lat'] . '"' . $csvsp .
        '"' . $security['se_corner_lng'] . '"' . $csvsp .
        '"' . $security['main_fence_name'] . '"' . $csvsp .
        '"' . $security['created_date'] . '"' . $csvsp .
        '"' . $security['created_name'] . '"' . $csvbr;
}