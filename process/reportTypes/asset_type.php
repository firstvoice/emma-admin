<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/13/2019
 * Time: 3:59 PM
 */


$filename .= $messageformat->rep_plans();
$csv .=
    'Asset Types ' . $csvbr; //csvbr is new line character.
$csv .=
    '"Name"' . $csvsp .
    '"Plan"' . $csvsp .
    '"Address"' . $csvsp .
    '"Latitude"' . $csvsp .
    '"Longitude"' . $csvbr;

    //Pull name, count of how many are in use,
    //If I need to do dropdown options and stuff , it will need to be a few different SQL rows.
    $asset_types = select_reportAssetType_assetTypes($USER->emma_plan_id);
//        $fvmdb->query("
//    SELECT eat.name, eat.emma_asset_type_id, ep.name as plan_name, ea.latitude, ea.longitude, ea.address
//    FROM emma_asset_types as eat
//    JOIN emma_assets ea on ea.emma_asset_type_id = eat.emma_asset_type_id AND ea.active = 1
//    JOIN emma_plans ep on eat.emma_plan_id = ep.emma_plan_id
//    WHERE eat.emma_plan_id = '".$USER->emma_plan_id."'
//    ");



while($asset_type = $asset_types->fetch_assoc()) {
    $csv .=
        '"' . $asset_type['name'] . '"' . $csvsp .
        '"' . $asset_type['plan_name'] . '"' . $csvsp .
        '"' . $asset_type['address'] . '"' . $csvsp .
        '"' . $asset_type['latitude'] . '"' . $csvsp .
        '"' . $asset_type['longitude'] . '"' . $csvbr;


    //need to put these in the while loop for $asset_types
//    $asset_custom_info = $fvmdb->query("
//    SELECT
//    FROM emma_assets_custom_info as eaci
//    JOIN emma_assets_dropdown_options as eado on eado.asset_type_id = eaci.id
//    JOIN emma_asset_type_display as eatd on eatd.asset_type_id = eaci.id
//    WHERE eaci.emma_asset_id = '" . $asset_type['emma_asset_type_id'] . "'
//    ");
    // $asset_dropdown_options = $fvmdb->query("
    //  SELECT
    //  FROM emma_assets_dropdown_options as eado
    //  WHERE eado.asset_type_id = '".$asset_type['emma_asset_type_id']."'
    //  ");
    //  $asset_type_display = $fvmdb->query("
    //  SELECT
    //   FROM emma_asset_type_display as eatd
    //  WHERE eatd.asset_type_id '".$asset_type['emma_asset_type_id']."'
    //   ");
}
