<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/13/2019
 * Time: 3:29 PM
 */


$filename .= 'Users';
$csv .=
    'Users ' . $csvbr; //csvbr is new line character.
$csv .=
    '"User Name"' . $csvsp . //bumps into next column
    '"Title"' . $csvsp .
    '"Type"' . $csvsp .
    '"Phone"' . $csvsp .
//    '"Mobile Type"' . $csvsp .
//    '"EMS Type"' . $csvsp .
    '"Address"' . $csvsp .
    '"City"' . $csvsp .
    '"State"' . $csvsp .
    '"Zip"' . $csvsp .
    '"Country"' . $csvsp .
    '"Code Generator"' . $csvsp .
    '"Login Count"' . $csvsp .
    '"Plan Name"' . $csvsp .
    '"Receives Notifications"' . $csvsp .
    '"Created By"' . $csvsp .
    '"Creation Date"' . $csvbr;


    $users = select_reportUsers_users($plan);
//        $fvmdb->query("
//    SELECT u.*,concat(u.firstname , ' ' , u.lastname) as user_name, u.title, u.type, u.mobile_type, u.phone,u.company,u.emma_code_generator,u.address,u.city,u.country,u.emma_login_count,u.landline_phone,u.zip,u.title,u.ems_type, u.creation,u.emma_code_generator, concat(uc.firstname , ' ' , uc.lastname) as user_created_name, concat(sr.first_name , ' ' , sr.last_name) as sales_rep_name
//    FROM users u
//    JOIN emma_plans ep on ep.emma_plan_id = u.emma_plan_id
//    LEFT JOIN users uc on uc.id = u.creator
//    LEFT JOIN sales_reps sr on sr.rep_id = u.sales_person_id
//    WHERE u.active = 1
//    AND u.emma_plan_id = '".$plan."'
//    GROUP BY u.id
//    ");
$gen = '';
$receive = '';
while($user = $users->fetch_assoc())
{

    if ($user['emma_code_generator'] == 0){
        $gen = 'no';
    }else{
        $gen = 'yes';
        }
    if ($user['receive_notifications'] == 0){
        $receive = 'no';
    }else{
        $receive = 'yes';

    }

    $csv .=
        '"' .$user['user_name']. '"' . $csvsp .
        '"' .$user['title']. '"' . $csvsp .
        '"' .$user['type']. '"' . $csvsp .
        '"' .$user['phone']. '"' . $csvsp .
//        '"' .$user['mobile_type']. '"' . $csvsp .
//        '"' .$user['ems_type']. '"' . $csvsp .
        '"' .$user['address']. '"' . $csvsp .
        '"' .$user['city']. '"' . $csvsp .
        '"' .$user['province']. '"' . $csvsp .
        '"' .$user['zip']. '"' . $csvsp.
        '"' .$user['country']. '"' . $csvsp .
        '"' .$gen. '"' . $csvsp .
        '"' .$user['emma_login_count']. '"' . $csvsp .
        '"' .$user['emma_plan_name']. '"' . $csvsp .
        '"' .$receive. '"' . $csvsp .
        '"' .$user['user_created_name']. '"' . $csvsp .
        '"' .$user['creation']. '"' . $csvbr;
}
