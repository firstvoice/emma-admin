<?php
session_start();
require('../include/db.php');
include('../include/processing.php');

require('../vendor/php-jwt-master/src/JWT.php');
require('../vendor/php-jwt-master/src/BeforeValidException.php');
require('../vendor/php-jwt-master/src/ExpiredException.php');
require('../vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('../config/config.json'));

$USER = null;

$token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key, array('HS512'));

$USER = $token->data;

$data = array();
$errors = array();

$newplan = $fvmdb->real_escape_string($_POST['plan']);

if(empty($USER)){
    $errors['no user'] = 'No token';
}
if(!isset($newplan)){
    $errors['no plan'] = 'no plan';
}
$plansecurity = false;
$plansos = false;

if(empty($errors)) {
    $plans = select_plan_from_planID($newplan);
//    $fvmdb->query("
//        SELECT p.*
//        FROM emma_plans p
//        WHERE p.emma_plan_id = '". $newplan ."'
//    ");
    $plan = $plans->fetch_assoc();

    if ($plan['securities']) $plansecurity = true;
    if ($plan['panic']) $plansos = true;

    $path = explode('process/', $_SERVER['PHP_SELF']);

    $token->data->emma_plan_id = $newplan;
    $token->data->privilege->plansecurity = $plansecurity;
    $token->data->privilege->plansos = $plansos;
    $token->data->emma_home_plans = array($newplan);
    $jwt = Firebase\JWT\JWT::encode($token, $CONFIG->key, 'HS512');
    setcookie('jwt', $jwt,time() + (9 * 60 * 60), '/');
}

$data['errors'] = $errors;
$data['success'] = empty($errors);


echo json_encode($data);