<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/19/2017
 * Time: 3:18 PM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$resource_id = $fvmdb->real_escape_string($_POST['id']);
$planId = $fvmdb->real_escape_string($_POST['plan-id']);

$resources = select_emmaResources_with_resourceID_and_planID($resource_id, $planId);
//$fvmdb->query("
//  SELECT r.*
//  FROM emma_resources AS r
//  WHERE r.emma_resource_id = " . $resource_id . "
//  AND r.emma_plan_id = " . $planId . "
//");
if ($resources->num_rows > 0) {
  $delete = update_emmaResource_active_with_resourceID($resource_id, 0);
//  $fvmdb->query("
//    UPDATE emma_resources
//    SET active = 0
//    WHERE emma_resource_id = " . $resource_id . "
//  ");
  if (!$delete) {
    $errors[] = 'failed to update';
  }
} else {
  $errors[] = 'invalid resource id';
}

$data['success'] = empty($errors);
$data['errors'] = $errors;
echo json_encode($data);
