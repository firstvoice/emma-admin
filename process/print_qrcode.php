<?php

require_once('../include/fpdf/fpdf.php');
require_once('../include/fpdf/fpdi.php');
require_once("../include/phpqrcode/qrlib.php");
include('../include/db.php');

$plan = $emmadb->real_escape_string($_POST['plan']);
$group = $emmadb->real_escape_string($_POST['group']);
$type = $emmadb->real_escape_string($_POST['type']);

$plandetails = select_plan_from_planID($plan);
$planinfo = $plandetails->fetch_assoc();

$groupdetails = select_group_with_groupID($group);
$groupinfo = $groupdetails->fetch_assoc();

$qrcodes = select_get_qrcodes_plans_groups($plan, $group);
$qr = $qrcodes->fetch_assoc();

if($type == 'pdf') {
    //generate qr code png
    QRcode::png($qr['qr_code'], "qrcode.png");

    //create pdf
    $pdf = new FPDF('P', 'mm', array(60,70));
    $pdf->AddPage();

    //insert generated qr code .png
    $pdf->Image('qrcode.png', 10, 10, 40, 40, "png");
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(0,45);
    $pdf->MultiCell(60, 10, $planinfo['name'], '', 'C', 0);
    $pdf->SetXY(0,50);
    $pdf->MultiCell(60, 10, $groupinfo['name'], '', 'C', 0);

    //output to force download
    $pdf->Output('EMMA_QR_' . md5($plan . $group) . '.pdf', 'D');
}elseif ($type == 'png'){
    //set file name
    $fileName = 'EMMA_QR_'.md5($plan . $group).'.png';
    //set file path
    $filePath = '../qrcodes/'.$fileName;

    // generating if doesn't exist
    if (!file_exists($filePath)) {
        QRcode::png($qr['qr_code'], $filePath);
    }

    //output to force download
    if(is_file($filePath))
    {

        if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); }

        switch(strtolower(substr(strrchr($filePath,'.'),1)))
        {
            case 'pdf': $mime = 'application/pdf'; break;
            case 'zip': $mime = 'application/zip'; break;
            case 'jpeg':
            case 'jpg': $mime = 'image/jpg'; break;
            case 'png': $mime = 'image/png'; break;
            default: $mime = 'application/force-download';
        }
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private',false);
        header('Content-Type: '.$mime);
        header('Content-Disposition: attachment; filename="'.basename($filePath).'"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.filesize($filePath));
        readfile($filePath);
    }
}else{
    echo 'invalid type';
}



