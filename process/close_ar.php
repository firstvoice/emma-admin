<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/23/2017
 * Time: 12:16 PM
 */

require('../include/db.php');
include('../include/processing.php');

$data = array();
$errors = array();

$data = array();
$errors = array();
$userId = $fvmdb->real_escape_string($_POST['user-id']);
$arId = $fvmdb->real_escape_string($_POST['ar-id']);
$comments = $fvmdb->real_escape_string($_POST['comments']);


if (empty($errors)) {
    $updatear = update_close_ar_with_id($arId, $comments, $userId);
    if (!$updatear) {
        $errors['update'] = 'Could not update Anonymous Report: ' . $emmadb->error;
    }
}
$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);