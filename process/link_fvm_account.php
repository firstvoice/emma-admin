<?php

global $USER;
global $fvmdb;
global $emmadb;
include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$userId = $fvmdb->real_escape_string($_POST['user-id']);
$username = $fvmdb->real_escape_string($_POST['username']);
$password = $fvmdb->real_escape_string($_POST['password']);

if (empty($userId)) { $errors['user-id'] = 'User Id not provided'; }
if (empty($username)) { $errors['username'] = 'Username not provided'; }
if (empty($password)) { $errors['password'] = 'Password not provided'; }

if (empty($errors)) {
  $hash = $password;
  $fvmUsers = $fvmdb->query("
    SELECT * from users
    WHERE username = '".$username."'
    AND password = PASSWORD('".$password."')
  ");
  if(mysqli_num_rows($fvmUsers) == 1) {
    $fvmUser = $fvmUsers->fetch_assoc();
    $updateUser = $emmadb->query("
      UPDATE users
      SET fvm_id = '".$fvmUser['id']."'
      WHERE id = '".$userId."'
    ");
    $USER['fvm_id'] = $fvmUser['id'];
  } else {
    $errors['password'] = 'Could not find FVM User';
  }
}

$data['user'] = $USER;
$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);