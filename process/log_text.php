<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 6/20/2019
 * Time: 12:16 PM
 */

require('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();
$type = $fvmdb->real_escape_string($_POST['type']);
$id = $fvmdb->real_escape_string($_POST['id']);
$number = $fvmdb->real_escape_string($_POST['number']);
$message = $fvmdb->real_escape_string($_POST['message']);
$success = $fvmdb->real_escape_string($_POST['success']);

$log = insert_log_text($type, $id, $number, $message, $success);
if(!$log){
    $errors['error-logging'] = 'Error logging text';
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);

