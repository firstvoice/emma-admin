<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/20/2017
 * Time: 12:28 PM
 */

include('../include/db.php');
include('../include/processing.php');

function geocode ($address) {
  $address = urlencode($address);
  $url = "http://maps.google.com/maps/api/geocode/json?&address={$address}&sensor=false";
  $resp_json = file_get_contents($url);
  $resp = json_decode($resp_json, true);
  if ($resp['status'] === 'OK') {
    $lat = $resp['results'][0]['geometry']['location']['lat'];
    $lon = $resp['results'][0]['geometry']['location']['lng'];
    $formatted_address = $resp['results'][0]['formatted_address'];
    if ($lat && $lon && $formatted_address) {
      $data_arr = array();
      array_push(
        $data_arr,
        $lat,
        $lon,
        $formatted_address
      );
      return $data_arr;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

$errors = array();
$data = array();

$site_id = $fvmdb->real_escape_string($_POST['id']);
$site_contact_id = $fvmdb->real_escape_string($_POST['cid']);
$userId = $fvmdb->real_escape_string($_POST['user-id']);

$siteName = $fvmdb->real_escape_string($_POST['sitename']);

$firstname = $fvmdb->real_escape_string($_POST['firstname']);
$lastname = $fvmdb->real_escape_string($_POST['lastname']);

$phone = $fvmdb->real_escape_string($_POST['phone']);
$email = $fvmdb->real_escape_string($_POST['email']);
$lat = $fvmdb->real_escape_string($_POST['lat']);
$lng = $fvmdb->real_escape_string($_POST['lng']);
$address = $fvmdb->real_escape_string($_POST['address']);
$state = $fvmdb->real_escape_string($_POST['state']);
$city = $fvmdb->real_escape_string($_POST['city']);
$zip = $fvmdb->real_escape_string($_POST['zip']);
$ignoreAddress = $fvmdb->real_escape_string($_POST['ignore-address']);
//if (!$ignoreAddress) {
//  $coords = geocode($address . ', ' . $city . ', ' . $state . ', ' . $zip);
//  if (!$coords || !$coords[0] || !$coords[1]) {
//    $errors['invalid-address'] = 'Invalid Address';
//  }
//}

if (empty($site_id) || empty($site_contact_id)) {
  $errors[] = 'empty id';
}

if (empty($firstname) || empty($lastname)) {
  $errors[] = 'empty contact name';
}

if (empty($phone)) {
  $errors[] = 'no phone';
}
if (empty($email)) {
  $errors[] = 'no email';
}

$oldSites = select_sitesAndSiteContacts_with_siteID($site_id);
//    $fvmdb->query("
//  SELECT es.*, esc.first_name, esc.last_name, esc.email, esc.phone
//  FROM emma_sites es
//  JOIN emma_site_contacts esc on es.emma_site_contact_id = esc.emma_site_contact_id
//  WHERE es.emma_site_id = '" . $site_id . "'
//");
if (!$oldSite = $oldSites->fetch_assoc()) {
  $errors[] = 'can not find site';
}

if (empty($errors)) {
  $update = update_emmaSites_with_siteID($site_id, $siteName, $address, $city, $state, $zip, $lat, $lng);
//      $fvmdb->query("
//    UPDATE emma_sites
//    SET
//    emma_site_name = '" . $siteName . "',
//    emma_site_street_address = '" . $address . "',
//    emma_site_city = '" . $city . "',
//    emma_site_state = '" . $state . "',
//    emma_site_zip = '" . $zip . "',
//    emma_site_latitude = '".$lat."',
//    emma_site_longitude = '".$lng."'
//    WHERE emma_site_id = " . $site_id . "
//  ");

  if (!$update) {
    $errors[] = 'update failed';
  }

  $update = update_emmaSiteContact_with_siteContactID($site_contact_id, $firstname, $lastname, $phone, $email);
//      $fvmdb->query("
//    UPDATE emma_site_contacts
//    SET first_name = '" . $firstname . "',
//    last_name = '" . $lastname . "',
//    phone = '" . $phone . "',
//    email = '" . $email . "'
//    WHERE emma_site_contact_id = " . $site_contact_id . "
//  ");

  if (!$update) {
    $errors[] = 'update failed';
  }

  $saveHistory = insert_emma_edit_history_sites($site_id, $oldSite['emma_site_name'], $oldSite['emma_site_street_address'],
      $oldSite['emma_site_city'], $oldSite['emma_site_state'], $oldSite['emma_site_zip'], $oldSite['emma_site_latitude'],
      $oldSite['emma_site_longitude'], $oldSite['first_name'], $oldSite['last_name'], $oldSite['email'], $oldSite['phone'], $userId, date('Y-m-d H:i:s'));
//      $fvmdb->query("
//    INSERT INTO emma_edit_history_sites (
//      emma_site_id,
//      emma_site_name,
//      emma_site_street_address,
//      emma_site_city,
//      emma_site_state,
//      emma_site_zip,
//      emma_site_latitude,
//      emma_site_longitude,
//      contact_first_name,
//      contact_last_name,
//      contact_email,
//      contact_phone,
//      edit_by_id,
//      edit_date
//    ) VALUES (
//      '" . $site_id . "',
//      '" . $oldSite['emma_site_name'] . "',
//      '" . $oldSite['emma_site_street_address'] . "',
//      '" . $oldSite['emma_site_city'] . "',
//      '" . $oldSite['emma_site_state'] . "',
//      '" . $oldSite['emma_site_zip'] . "',
//      '" . $oldSite['emma_site_latitude'] . "',
//      '" . $oldSite['emma_site_longitude'] . "',
//      '" . $oldSite['first_name'] . "',
//      '" . $oldSite['last_name'] . "',
//      '" . $oldSite['email'] . "',
//      '" . $oldSite['phone'] . "',
//      '" . $userId . "',
//      '" . date('Y-m-d H:i:s') . "'
//    )
//  ");


}

$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);