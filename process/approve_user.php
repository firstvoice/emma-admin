<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 9/23/2019
 * Time: 9:43 AM
 */


include('../include/db.php');
include('../include/processing.php');
$data = array();
$errors = array();

$rawId = $_POST['id'];


for($i=0; $i<count($rawId); $i++) {
    $rawUser = select_rawUsers_with_userID($rawId[$i]);
    $user = $rawUser->fetch_assoc();

    $password = keygen(10);

    $insertUser = insert_user($user['email'], $password, $user['firstname'], $user['lastname'], $user['mobile'], "", "", "", "", "", "", $_SERVER['REMOTE_ADDR'], "", $user['plan']);
//        $fvmdb->query("
//        INSERT INTO users (
//            username, password, firstname, lastname,
//            phone, company, address, city, province,
//            country, zip, creatorip, creator, creation,
//            auth, emma_plan_id
//        ) VALUES (
//            '" . $user['email'] . "', PASSWORD('" . $password . "'), '" . $user['firstname'] . "', '" . $user['lastname'] . "',
//            '" . $user['mobile'] . "', '', '', '', '',
//            '', '', '" . $_SERVER['REMOTE_ADDR'] . "', '', '" . date("Y-m-d H:i:s") . "',
//            SHA1('" . $user['email'] . "'), '". $user['plan'] ."'
//        )
//    ");
    if(!$insertUser) {
        $errors['error-insert'] = 'Error Inserting';
    }else{
        $userId = $emmadb->insert_id;

        $updateRaw = update_rawUser_active_with_userID_and_active($user['id'], 0);
//        $fvmdb->query("
//            UPDATE raw_users
//            SET active = 0
//            WHERE id = '". $user['id'] ."'
//        ");

        $insertGroup = insert_user_groups($userId, $user['group']);
//            $fvmdb->query("
//            INSERT INTO emma_user_groups (user_id, emma_group_id) VALUES ('". $userId ."', '". $user['group'] ."')
//        ");

        $students = select_rawStudentsToGuardians_with_guardianID( $rawId[$i]);
        while ($student = $students->fetch_assoc()){
            $insertStudent = insert_students_to_guardians($student['student_id'], $student['student_name'], $userId);
//            $fvmdb->query("
//                INSERT INTO emma_students_to_guardians (student_id, student_name, guardian_id)
//                VALUES ('". $student['student_id'] ."', '". $student['student_name'] ."', '". $userId ."')
//            ");
        }

        $link = __ROOT__ . '/verify_email.php?u=' .
            $userId . '&c=' . $password;

        $body = '<div>Click this link to activate your EMMA account - <a href="' .
            $link . '">Activate</a></div> or visit - ' . $link;

        $bodyText = 'Copy this link into your browser to activate your EMMA account - ' .
            $link;

        $transport = Swift_SmtpTransport::newInstance();
        $mailer = Swift_Mailer::newInstance($transport);
        $swiftMessage = Swift_Message::newInstance()
            ->setSubject('EMMA Account Creation')
            ->setFrom(array('donotreply@emmaadmin.com' => 'EMMA Admin'))
            ->setTo(array($user['username'] => $user['firstname'] . ' ' . $user['lastname']))
            ->setBody($body, 'text/html')
            ->addPart($bodyText, 'text/plain');
        $result = $mailer->send($swiftMessage);

        if (!$result) {
            $errors['email'] = 'Failed sending email';
        }

    }
}






function keygen($length = 10)
{
    $key = '';
    list($usec, $sec) = explode(' ', microtime());
    mt_srand((float)$sec + ((float)$usec * 100000));

    $inputs = array_merge(range(2, 9), range('A', 'H'), range('J', 'K'), range('M', 'N'), range('P', 'Z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $inputs{mt_rand(0, 30)};
    }
    return $key;
}

$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);