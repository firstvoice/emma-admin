<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 9/19/2019
 * Time: 3:41 PM
 */


include('../include/db.php');
include('../include/processing.php');
$data = array();
$errors = array();

$firstname = $fvmdb->real_escape_string($_POST['first']);
$lastname = $fvmdb->real_escape_string($_POST['last']);
$email = $fvmdb->real_escape_string($_POST['email']);
$phone = $fvmdb->real_escape_string($_POST['mobile']);
$land = $fvmdb->real_escape_string($_POST['landline']);
$plan = $fvmdb->real_escape_string($_POST['plan']);
$group = $fvmdb->real_escape_string($_POST['group']);


$students = $_POST['childname'];

$insertuser = insert_raw_users($firstname, $lastname, $email, $phone, $land, $plan, $group);
//    $fvmdb->query("
//    INSERT INTO raw_users (firstname, lastname, email, mobile, landline, plan, `group`)
//    VALUES ('". $firstname ."', '". $lastname ."', '". $email ."', '". $phone ."', '". $land ."','". $plan ."','". $group ."')
//");
if(!$insertuser){
    $errors['not_inserted'] = 'The user did not insert.';
}else{

    $userid = $emmadb->insert_id;

    if(!empty($students)){
        for($i=0; $i<count($students); $i++){

            $studentfull = explode(' ',$students[$i]);

            $studentnames = select_userSignup($studentfull[0], $studentfull[1]);
//                $fvmdb->query("
//                SELECT u.*
//                FROM users u
//                WHERE u.firstname = '". $studentfull[0] ."'
//                AND u.lastname = '". $studentfull[1] ."'
//            ");
            if($studentname = $studentnames->fetch_assoc()) {
                $insertStudents = insert_emma_raw_students_to_guardians($studentname['id'], $students[$i], $userid);
//                    $fvmdb->query("
//                    INSERT INTO emma_raw_students_to_guardians (student_id, student_name, guardian_id) VALUES ('" . $studentname['id'] . "', '". $students[$i] ."', '" . $userid . "')
//                ");
            }else{
                $insertStudents = insert_emma_raw_students_to_guardians(0, $students[$i], $userid);
//                    $fvmdb->query("
//                    INSERT INTO emma_raw_students_to_guardians (student_id, student_name, guardian_id) VALUES ('0', '". $students[$i] ."', '" . $userid . "')
//                ");
            }
        }
    }
}



$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);