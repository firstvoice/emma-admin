<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 6/20/2019
 * Time: 12:16 PM
 */

require('../include/db.php');
include('../include/processing.php');

require('../vendor/php-jwt-master/src/JWT.php');
require('../vendor/php-jwt-master/src/BeforeValidException.php');
require('../vendor/php-jwt-master/src/ExpiredException.php');
require('../vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('../config/config.json'));

$USER = null;

$token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key, array('HS512'));

$USER = $token->data;

$currentE = $fvmdb->real_escape_string($_POST['currentEvent']);
$currentS = $fvmdb->real_escape_string($_POST['currentSecurity']);
$currentSO = $fvmdb->real_escape_string($_POST['currentSOS']);
$currentN = $fvmdb->real_escape_string($_POST['currentNotification']);
$currentL = $fvmdb->real_escape_string($_POST['currentLockdown']);
$currentML = $fvmdb->real_escape_string($_POST['currentMLockdown']);

$errors = array();
$data = array();
$currentEvents = explode(',', $currentE);
$currentSecurity = explode(',', $currentS);
$currentSOS = explode(',', $currentSO);
$currentNotification = explode(',',$currentN);
$currentLockdown = explode(',',$currentL);
$currentMLockdown = explode(',',$currentML);
$userGroups = array();
$alert = false;
$new = false;
$data['id'] = '';
$data['event'] = $currentEvents;
$data['transferred'] = $currentE;

$groups = select_searchNewEvents_groups($USER->id);
//    $fvmdb->query("
//    SELECT g.*
//    FROM emma_groups g
//    JOIN emma_user_groups u ON g.emma_group_id = u.emma_group_id
//    WHERE g.emma_plan_id = '". $USER->emma_plan_id ."'
//    AND u.user_id = '". $USER->id ."'
//");
while($group = $groups->fetch_assoc()){
    $userGroups[] = $group['emma_group_id'];
}

$events = select_searchNewEvents_events($USER->id);
//    $fvmdb->query("
//    SELECT e.*, t.name as type_name, t.img_filename, u.username, u.phone
//    FROM emergencies e
//    JOIN emergency_types t ON e.emergency_type_id = t.emergency_type_id
//    LEFT JOIN users u ON e.created_by_id = u.id
//    WHERE e.emma_plan_id = '". $USER->emma_plan_id ."'
//    AND e.active = '1'
//");
while($event = $events->fetch_assoc()){
    if(!in_array($event['emergency_id'], $currentEvents)){
        $new = true;
        $eventGroups = select_searchNewEvents_eventGroups($event['emergency_id']);
//            $fvmdb->query("
//            SELECT e.*
//            FROM emergency_received_groups e
//            WHERE e.emergency_id = '". $event['emergency_id'] ."'
//        ");
        while($eventGroup = $eventGroups->fetch_assoc()){
            if(in_array($eventGroup['emma_group_id'],$userGroups)){
                $alert = true;
                $data['id'] = $event['emergency_id'];
                $data['title'] = $event['type_name'];
                $data['body'] = '<b>Near:</b> ' . $event['nearest_address'] . '<br><b>Username:</b> '. $event['username'] .'<br><b>Time:</b> ' . date('H:i:s m/d/Y', strtotime($event['created_date'], $USER->timezone . ' Hours'));
                $data['icon'] = $event['img_filename'];
                $data['type'] = 'event';
                $data['color'] = 'red';
            }
        }
    }
}

$events = select_searchNewEvents_events_securities($USER->emma_plan_id);
//    $fvmdb->query("
//    SELECT e.*, t.name as type_name, t.img_filename, u.username, u.phone
//    FROM emma_securities e
//    JOIN emma_security_types t ON e.emma_security_type_id = t.emma_security_type_id
//    LEFT JOIN users u ON e.created_by_id = u.id
//    WHERE e.emma_plan_id = '". $USER->emma_plan_id ."'
//    AND e.active = '1'
//");
while($event = $events->fetch_assoc()){
    if(!in_array($event['emma_security_id'], $currentSecurity)){
        $new = true;
        $alert = true;
        $data['id'] = $event['emma_security_id'];
        $data['title'] = $event['type_name'];
        $data['body'] = '<b>Description:</b> ' . $event['description'] . '<br><b>Username:</b> ' . $event['username'] . '<br><b>Time:</b> '. date('H:i:s m/d/Y', strtotime($event['created_date'], $USER->timezone . ' Hours'));
        $data['icon'] = 'icon_security.png';
        $data['type'] = 'security';
        $data['color'] = 'red';
    }
}
if($USER->privilege->admin) {
    $events = select_searchNewEvents_events_sos($USER->id);
//    $fvmdb->query("
//SELECT s.*, u.username, u.phone
//FROM emma_sos AS s
//  JOIN users AS u ON s.created_by_id = u.id AND u.emma_plan_id = '". $USER->emma_plan_id ."'
//WHERE s.cancelled_date IS NULL
//      AND s.help_date IS NOT NULL
//      AND s.closed_date IS NULL
//");
    while ($event = $events->fetch_assoc()) {
        if (!in_array($event['emma_sos_id'], $currentSOS)) {
            $new = true;
            $alert = true;
            $data['id'] = $event['emma_sos_id'];
            $data['title'] = 'EMMA SOS';
            $data['body'] = '<b>SOS Near:</b> Lat: ' . $event['pending_lat'] . ' Lng: ' . $event['pending_lng'] . '<br><b>Username:</b> ' . $event['username'] . '<br><b>Time:</b> ' . date('H:i:s m/d/Y', strtotime($event['help_date'], $USER->timezone . ' Hours'));
            $data['icon'] = 'icon_emma_sos.png';
            $data['type'] = 'sos';
            $data['color'] = 'red';
        }
    }
}

$events = select_searchNewEvents_events_mc($USER->id);
//    $fvmdb->query("
//    SELECT e.*, u.username, u.phone
//    FROM emma_mass_communications e
//    LEFT JOIN users u ON e.created_by_id = u.id
//    WHERE e.emma_plan_id = '". $USER->emma_plan_id ."'
//");
while($event = $events->fetch_assoc()){
    if(!in_array($event['emma_mass_communication_id'],$currentNotification)){

        $alert = true;
        $new = true;
        $data['id'] = $event['emma_mass_communication_id'];
        $data['title'] = $event['type_name'];
        $data['body'] = '<b>Message:</b> ' . $event['notification'] . '<br><b>Username:</b> ' . $event['username'] . '<br><b>Time:</b> '. date('H:i:s m/d/Y', strtotime($event['created_date'], $USER->timezone . ' Hours'));
        $data['icon'] = 'icon_mass.png';
        $data['type'] = 'notification';
        $data['color'] = 'red';
    }
}

$events = select_searchNewEvents_events_l($USER->id);
//    $fvmdb->query("
//    SELECT e.*, u.username, u.phone
//    FROM emma_mass_communications e
//    LEFT JOIN users u ON e.created_by_id = u.id
//    WHERE e.emma_plan_id = '". $USER->emma_plan_id ."'
//");
while($event = $events->fetch_assoc()){
    $sendlock = false;
    $groups = select_groups_with_userID($USER->id);
    while($group = $groups->fetch_assoc()) {
        $lockdownperm = select_group_lockdown_permission($group['emma_group_id']);
        if($lockdownperm->num_rows > 0){
            $sendlock = true;
        }
    }
    if($sendlock) {
        if (!in_array($event['lockdown_id'], $currentLockdown)) {
            if ($event['drill'] == 1) {
                $notifications = select_lockdown_drill_message($event['lockdown_id']);
                $notification = $notifications->fetch_assoc();
                $message = $notification['notification'];
                $icon = 'EMMA_LOCKDOWN_50_px_greyscale.png';
                $drill = 'DRILL!';
                $color = 'gray';
            } else {
                $message = $event['notification'];
                $icon = 'icon_emma_sos.png';
                $drill = '';
                $color = 'red';
            }
            $alert = true;
            $new = true;
            $data['id'] = $event['lockdown_id'];
            $data['title'] = $drill . ' Lockdown ' . $drill;
            $data['body'] = '<b>Message:</b> ' . $message . '<br><b>Username:</b> ' . $event['username'] . '<br><b>Time:</b> ' . date('H:i:s m/d/Y', strtotime($event['created_date'], $USER->timezone . ' Hours'));
            $data['icon'] = $icon;
            $data['type'] = 'lockdown';
            $data['color'] = $color;
        }
    }
}
$events = select_searchNewEvents_events_ml($USER->id);
//    $fvmdb->query("
//    SELECT e.*, u.username, u.phone
//    FROM emma_mass_communications e
//    LEFT JOIN users u ON e.created_by_id = u.id
//    WHERE e.emma_plan_id = '". $USER->emma_plan_id ."'
//");
while($event = $events->fetch_assoc()){
    $sendlock = false;
    $groups = select_groups_with_userID($USER->id);
    while($group = $groups->fetch_assoc()) {
        $lockdownperm = select_group_lockdown_permission($group['emma_group_id']);
        if($lockdownperm->num_rows > 0){
            $sendlock = true;
        }
    }
    if($sendlock) {
        if (!in_array($event['m_lockdown_id'], $currentMLockdown)) {

                $message = $event['notification'];
                $icon = 'emma_lockdown_yellow.png';
                $drill = '';
                $color = 'red';

            $alert = true;
            $new = true;
            $data['id'] = $event['m_lockdown_id'];
            $data['title'] = ' Modified LOCKDOWN! ';
            $data['body'] = '<b>Message:</b> ' . $message . '<br><b>Username:</b> ' . $event['username'] . '<br><b>Time:</b> ' . date('H:i:s m/d/Y', strtotime($event['created_date'], $USER->timezone . ' Hours'));
            $data['icon'] = $icon;
            $data['type'] = 'mlockdown';
            $data['color'] = $color;
        }
    }
}

$data['new'] = $new;
$data['alert'] = $alert;
$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);
