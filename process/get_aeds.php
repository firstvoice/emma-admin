<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 8/28/2017
 * Time: 10:04 AM
 */

include('../include/db.php');
include('../include/processing.php');
global $fvmdb;

$columns = $_GET['columns'];
$draw = $fvmdb->real_escape_string($_GET['draw']);
$length = $fvmdb->real_escape_string($_GET['length']);
$order = $_GET['order'];
$search = $_GET['search'];
$start = $fvmdb->real_escape_string($_GET['start']);
$active = $fvmdb->real_escape_string($_GET['active']);
$userID = $fvmdb->real_escape_string($_GET['user-id']);
$planID = $fvmdb->real_escape_string($_GET['plan-id']);
$drill = $fvmdb->real_escape_string($_GET['drill']);

$orderString = $columns[$order[0]['column']]['data'] . ' ' . $order[0]['dir'];
$data = array();

$aeds = $fvmdb->query("
  SELECT SQL_CALC_FOUND_ROWS o.name as organization, l.name as location, a.aed_id, ab.brand, am.model, a.serialnumber, a.lastcheck, a.current
  FROM aeds a
  JOIN aed_models am on a.aed_model_id = am.aed_model_id
  JOIN aed_brands ab on am.aed_brand_id = ab.aed_brand_id
  JOIN locations l on a.location_id = l.id
  JOIN organizations o on l.orgid = o.id
  JOIN emma_plan_orgs epo on (l.orgid = epo.org_id AND epo.plan_id = ".$planID.")
  WHERE (" . ($_GET['columns'][0]['search']['value'] != "" ? "o.name like ('%" . $_GET['columns'][0]['search']['value'] . "%')" : "1") . ")
  AND (" . ($_GET['columns'][1]['search']['value'] != "" ? "l.name like ('%" . $_GET['columns'][1]['search']['value'] . "%')" : "1") . ")
  AND (" . ($_GET['columns'][2]['search']['value'] != "" ? "ab.brand like ('%" . $_GET['columns'][2]['search']['value'] . "%')" : "1") . ")
  AND (" . ($_GET['columns'][3]['search']['value'] != "" ? "am.model like ('%" . $_GET['columns'][3]['search']['value'] . "%')" : "1") . ")
  AND (" . ($_GET['columns'][4]['search']['value'] != "" ? "a.serialnumber like ('%" . $_GET['columns'][4]['search']['value'] . "%')" : "1") . ")
  AND (" . ($_GET['columns'][5]['search']['value'] != "" ? "a.lastcheck like ('%" . $_GET['columns'][5]['search']['value'] . "%')" : "1") . ")
  AND (" . ($_GET['columns'][6]['search']['value'] != "" ? "(a.current = 1 AND 'Active' like ('" . $_GET['columns'][6]['search']['value'] . "%')) OR (a.current = 0 AND 'Inactive' like ('" . $_GET['columns'][6]['search']['value'] . "%'))" : "1") . ")
  ORDER BY ". $orderString .", ab.brand ASC
  LIMIT " . $length . " OFFSET " . $start . "
");
$found = $fvmdb->query("
   SELECT FOUND_ROWS()
");
$count = $found->fetch_assoc();

$data['iTotalRecords'] = $count['FOUND_ROWS()'];
$data['iTotalDisplayRecords'] = $count['FOUND_ROWS()'];
$data['sEcho'] = $draw;
$data['aaData'] = array();
$data['sortString'] = $orderString;
while ($aed = $aeds->fetch_assoc()) {
  $data['aaData'][] = $aed;
}
echo json_encode($data);