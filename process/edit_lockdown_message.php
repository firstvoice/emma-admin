<?php

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$planId = $fvmdb->real_escape_string($_POST['plan-id']);
$message = $fvmdb->real_escape_string($_POST['message']);
$drill = $fvmdb->real_escape_string($_POST['drill']);

if (empty($message)) {
    $errors['empty'] = 'Message is required';
}

if (empty($errors)) {
    if($drill != 1) {
        $updateMessage = update_lockdown_message($planId, $message);
    }else{
        $updateMessage = update_lockdown_drill_message($planId, $message);
    }

    if (!$updateMessage) {
        $errors['sql'] = $fvmdb->error;
    }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);