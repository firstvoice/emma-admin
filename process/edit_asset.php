<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 5/29/2019
 * Time: 3:30 PM
 */



include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$type_id = $fvmdb->real_escape_string($_POST['type-id']);
$latitude = $fvmdb->real_escape_string($_POST['latitude']);
$longitude = $fvmdb->real_escape_string($_POST['longitude']);
$name = $fvmdb->real_escape_string($_POST['type_raw_name']);
$HeaderArray = json_decode($_POST['Headers']);
$DatesArray = json_decode($_POST['Dates']);
$FillinArray = json_decode($_POST['Fillins']);
$created_date = $_POST['created_date'];
$expiration_date = $_POST['expiration_date'];
$status = $_POST['status'];
$id = $_POST['id'];
$address = $fvmdb->real_escape_string($_POST['address']);
$image = $_POST['image'];

$data['fillin'] = $HeaderArray;


if (empty($id) || empty($type_id)) {
    $errors['id'] = 'The ID could not be found';
}
if (empty($latitude) || empty($longitude)) {
    $errors['location'] = 'The latitude and or longitude was not processed';
}

if (empty($errors)) {

    if($status == 'Current')
    {
        $status = '1';
    }
    else if($status == 'Inactive')
    {
        $status = '0';
    }
    update_emmaAsset_details_with_assetID($id, $type_id, $latitude, $longitude, $address, $status);
//    $fvmdb->query("
//    UPDATE emma_assets as ea
//    SET
//     ea.emma_asset_type_id = '".$type_id."',
//     ea.latitude = '".$latitude."',
//     ea.longitude = '".$longitude."',
//     ea.address = '".$address."',
//     ea.active = '".$status."'
//     WHERE ea.emma_asset_id = '".$id."'
//    ");


    //Update dropdown selected values
    if(!empty($HeaderArray)){
        foreach($HeaderArray as $header)
        {
            update_assetsCustomInfo_dropdown_with_assetCustomInfoID($header->selecteddropdown, $header->value);
//            $fvmdb->query("
//            UPDATE emma_assets_custom_info
//            SET dropdown = '".$header->value."'
//            WHERE id = '".$header->selecteddropdown."'
//            ");
        }
    }
    //Update descriptions (fillins)
    if (!empty($FillinArray)) {
        foreach($FillinArray as $fillin)
        {
            update_assetsCustomInfo_info_with_assetCustomInfoID($fillin->customid, $fillin->value);
//            $fvmdb->query("
//            UPDATE emma_assets_custom_info
//            SET info = '".$fillin->value."'
//            WHERE id = '".$fillin->customid."'
//            ");
        }
    }
    //Update dates
    if(!empty($DatesArray)){
        foreach($DatesArray as $date)
        {
            update_assetsCustomInfo_dateTime_with_assetCustomInfoID($date->customid, $date->value);
//            $fvmdb->query("
//            UPDATE emma_assets_custom_info
//            SET date_time = '".$date->value."'
//            WHERE id = '".$date->customid."'
//            ");
        }
    }


}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);