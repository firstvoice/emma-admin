<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 5/21/2019
 * Time: 1:07 PM
 */


require('../include/db.php');
include('../include/processing.php');
$data = array();
$errors = array();


$userId = $fvmdb->real_escape_string($_POST['user']);
$isDrill = $fvmdb->real_escape_string($_POST['drill']);
$pin = $fvmdb->real_escape_string($_POST['pin']);
$type = $fvmdb->real_escape_string($_POST['type']);
$subType = $fvmdb->real_escape_string($_POST['sub-type']);
$siteId = $fvmdb->real_escape_string($_POST['site']);
$description = $fvmdb->real_escape_string($_POST['description']);
$comments = $fvmdb->real_escape_string($_POST['comments']);
$groups = $_POST['groups'];
$latitude = $fvmdb->real_escape_string($_POST['lat']);
$longitude = $fvmdb->real_escape_string($_POST['lng']);
$address = $fvmdb->real_escape_string($_POST['address']);


$desktop_connection = $fvmdb->real_escape_string($_POST['desktopConnection']); //This is so we can differentiate between someone using the download client vs the web browser.
$auth = $fvmdb->real_escape_string($_POST['auth']); //This is the auth code that the downloadable client sends. For security the userId is not saved so we go by auth code.
$group_id = $fvmdb->real_escape_string($_POST['group_id']); //Emma Desktop variable that gets passed.


if($isDrill == 'on'){
    $isDrill = 1;
}else{
    $isDrill = 0;
}

if(empty($desktop_connection)) {
    $users = select_user_from_userID($userId);
//        $fvmdb->query("
//    SELECT DISTINCT u.auth
//    FROM users u
//    where u.id='" . $userId . "'
//");
    if($users->num_rows == 1) {
        $user = $users->fetch_assoc();
    }
    else{
        $errors['Invalid Users'] = 'User could not be Authenticated!';
    }
}
else{
    //We still select the u.auth because it gets passed later and it is worth it to just grab it now so its the same instead of making more IF statements.
    $users = select_userANDinfoOnly_from_auth($auth);
//    $fvmdb->query("
//    SELECT DISTINCT u.id, u.auth, eg.info_only
//    FROM users u
//    LEFT JOIN emma_user_groups as eug on u.id = eug.user_id
//    LEFT JOIN emma_groups as eg on u.emma_plan_id = eg.emma_plan_id AND eg.emma_group_id = eug.emma_group_id
//    WHERE u.auth = '".$auth."'
//    ");
    if($users->num_rows != 0)
    {
        $user = $users->fetch_assoc();
        if($user['info_only'] == 0) {
            $userId = $user['id'];
        }
        else{
            $errors['Info only!'] = 'ERRORThis account is not allowed to create events';
        }
    }
    else{
        $errors['Invalid Users'] = 'ERRORUser could not be Authenticated!';
    }
    foreach ($errors as $error) {
        echo $error;
    }
}


if (empty($errors)) {
    if((strpos(dirname($_SERVER['PHP_SELF']),'git-') !== false  || strpos(dirname($_SERVER['PHP_SELF']),'/egor') !== false)){
        $url = 'http://www.tsdemos.com/mobileapi/beta_emma/send_alert.php';
    }
    else{
        $url = 'http://www.tsdemos.com/mobileapi/emma/send_alert.php';
    }
//    $url = 'http://www.tsdemos.com/mobileapi/emma/send_alert.php';
    $fields = array(
        "a" => $user['auth'],
        "user" => $userId,
        "drill" => $isDrill,
        "pin" => $pin,
        "type" => $type,
        "sub-type" => $subType,
        "site" => $siteId,
        "description" => $description,
        "comments" => $comments,
        "groups" => $groups,
        "lat" => $latitude,
        "lng" => $longitude);

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_ENCODING, "");

    header('Content-Type: text/html');
    curl_exec($ch);
    $data['success'] = empty($errors);
}




if(empty($desktop_connection)){json_encode($data);}
