<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 8/28/2017
 * Time: 10:04 AM
 */

include('../include/db.php');
include('../include/processing.php');

$columns = $_GET['columns'];
$draw = $fvmdb->real_escape_string($_GET['draw']);
$length = $fvmdb->real_escape_string($_GET['length']);
$order = $_GET['order'];
$search = $_GET['search'];
$start = $fvmdb->real_escape_string($_GET['start']);
$emmaPlanId = $fvmdb->real_escape_string($_GET['emma-plan-id']);
$active = $fvmdb->real_escape_string($_GET['active']);

$orderString = $columns[$order[0]['column']]['data'] . ' ' . $order[0]['dir'];

$data = array();

$assets = select_getAssets_assets($active, $emmaPlanId, $orderString, $length, $start, $_GET['columns'][0]['search']['value'], $_GET['columns'][1]['search']['value'], $_GET['columns'][2]['search']['value'], $_GET['columns'][3]['search']['value'], $_GET['columns'][4]['search']['value'], $_GET['columns'][5]['search']['value']);
   $fvmdb->query("
   SELECT SQL_CALC_FOUND_ROWS a.*, at.name as asset_type_name, concat(u.firstname, ' ', u.lastname) as user_name, u.id as userid, u.username as useremail
   FROM emma_assets a
   JOIN emma_asset_types at ON a.emma_asset_type_id = at.emma_asset_type_id
   JOIN users u ON a.created_by_id = u.id
   WHERE (" . ($active != '' ? "a.active = " . $active : "1") . ")
   AND a.deleted = 0
   AND a.emma_plan_id = '".$emmaPlanId."'
   AND (" . ($_GET['columns'][0]['search']['value'] != "" ? "at.name like ('%" . $_GET['columns'][0]['search']['value'] . "%')" : "1") . ")
   AND (" . ($_GET['columns'][1]['search']['value'] != "" ? "concat(u.firstname, ' ', u.lastname) like ('%" . $_GET['columns'][1]['search']['value'] . "%')" : "1") . ")
   AND (" . ($_GET['columns'][2]['search']['value'] != "" ? "a.created_date like ('%" . $_GET['columns'][2]['search']['value'] . "%')" : "1") . ")
   AND (" . ($_GET['columns'][3]['search']['value'] != "" ? "a.latitude like ('%" . $_GET['columns'][3]['search']['value'] . "%')" : "1") . ")
   AND (" . ($_GET['columns'][4]['search']['value'] != "" ? "a.longitude like ('%" . $_GET['columns'][4]['search']['value'] . "%')" : "1") . ")
   AND (" . ($_GET['columns'][5]['search']['value'] != "" ? "(a.active = '0' AND 'Inactive' like ('%" . $_GET['columns'][5]['search']['value'] . "%')) OR (a.active = '1' AND 'Current' like ('%" . $_GET['columns'][5]['search']['value'] . "%'))" : "1") . ")
   ORDER BY ". $orderString .", created_date desc
   LIMIT " . $length . " OFFSET " . $start . "
");
$found = select_FOUND_ROWS();
//    $fvmdb->query("
//    SELECT FOUND_ROWS()
//");
$count = $found->fetch_assoc();

$data['iTotalRecords'] = $count['FOUND_ROWS()'];
$data['iTotalDisplayRecords'] = $count['FOUND_ROWS()'];
$data['sEcho'] = $draw;
$data['aaData'] = array();
while ($asset = $assets->fetch_assoc()) {
  $data['aaData'][] = $asset;
}
echo json_encode($data);