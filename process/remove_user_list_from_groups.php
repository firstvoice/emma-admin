<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/18/2017
 * Time: 4:35 PM
 */

include('../include/db.php');
include('../include/processing.php');

$user_ids = $_POST['selected_users'];
$group_ids = $_POST['group-ids'];
$data = array();
$errors = array();

if (empty($user_ids)) {
    $errors[] = 'No users selected';
}
if (empty($group_ids)) {
    $errors[] = 'No group id';
}

if (empty($errors)) {
    $user_list = implode("','", $user_ids);
    $group_list = implode("','", $group_ids);

    $delete = delete_emma_user_groups_with_groupString_userString($user_list, $group_list);
//        $fvmdb->query("
//        DELETE FROM emma_user_groups
//        WHERE user_id IN ('" . $user_list . "')
//        AND emma_group_id IN ('" . $group_list . "')
//    ");
    foreach ($user_ids AS $user) {

        $user_groups = select_userGroups_with_userID($user);
//            $fvmdb->query("
//            SELECT *
//            FROM emma_user_groups
//            WHERE user_id = " . $user . "
//        ");
        if ($user_groups->num_rows == 0) {
            $insert = insert_user_groups($user, 0);
//                $fvmdb->query("
//                INSERT INTO emma_user_groups (emma_group_id, user_id)
//                VALUES (0, " . $user . ")
//            ");
        }
    }

}

$data['post'] = $_POST;

$data['success'] = empty($errors);
$data['errors'] = $errors;
echo json_encode($data);