<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/23/2017
 * Time: 12:16 PM
 */

require('../include/db.php');
include('../include/processing.php');

$data = array();
$errors = array();
$userId = $fvmdb->real_escape_string($_POST['user-id']);
$securityId = $fvmdb->real_escape_string($_POST['security-id']);
$description = $fvmdb->real_escape_string($_POST['description']);
$newComments = $fvmdb->real_escape_string($_POST['comments']);
$callLogSwitch = $fvmdb->real_escape_string($_POST['call-log']);

if (empty($errors)) {
  $updateEvent = update_securities_closeSecurity_with_securityID($securityId, $description, $newComments, $userId);
//  $fvmdb->query("
//    UPDATE emma_securities
//    SET
//      active = '0',
//      closed_date = '" . date('Y-m-d H:i:s') . "',
//      description = '" . $description . "',
//      comments = '" . $newComments . "',
//      closed_by_id = '" . $userId . "'
//    WHERE emma_security_id = '" . $securityId . "'
//  ");

  $insertSystemMessage = insert_emma_security_messages($securityId, $userId, 'Security Closed', $description, $newComments);
//  $fvmdb->query("
//    INSERT INTO emma_security_messages (
//      security_id,
//      message,
//      created_by_id,
//      created_date,
//      description,
//      comments
//    ) VALUES (
//      '" . $securityId . "',
//      'Security Closed',
//      '" . $userId . "',
//      '" . date('Y-m-d H:i:s') . "',
//      '" . $description . "',
//      '" . $newComments . "'
//    )
//  ");
  $messageId = $emmadb->insert_id;

  if (isset($callLogSwitch) && $callLogSwitch == "on") {
    $reportNum = uniqid('');
    $planId = $fvmdb->real_escape_string($_POST['plan-id']);
    $reportPost = $fvmdb->real_escape_string($_POST['report-post']);
    $siteId = $fvmdb->real_escape_string($_POST['site-id']);
    $officerName = $fvmdb->real_escape_string($_POST['officer-name']);
    $incidentDescription = $fvmdb->real_escape_string($_POST['incident-description']);
    $resultingAction = $fvmdb->real_escape_string($_POST['resulting-action']);
    $policeName = $fvmdb->real_escape_string($_POST['police-name']);
    $policeTime = $fvmdb->real_escape_string($_POST['police-time']);
    $policeYN = $fvmdb->real_escape_string($_POST['police-yn']);
    $resLifeName = $fvmdb->real_escape_string($_POST['reslife-name']);
    $resLifeTime = $fvmdb->real_escape_string($_POST['reslife-time']);
    $resLifeYN = $fvmdb->real_escape_string($_POST['reslife-yn']);
    $otherNotified = $fvmdb->real_escape_string($_POST['other-notified']);
    $otherName = $fvmdb->real_escape_string($_POST['other-name']);
    $otherTime = $fvmdb->real_escape_string($_POST['other-time']);
    $otherYN = $fvmdb->real_escape_string($_POST['other-yn']);

    $enterCallReport = insert_emma_call_reports($securityId, $reportPost, $reportNum, $planId, $siteId, $officerName, $incidentDescription,
        $resultingAction, $policeYN, $policeTime, $policeName, $resLifeYN, $resLifeTime, $resLifeName,
        $otherNotified, $otherYN, $otherTime, $otherName);
//    $fvmdb->query("
//      insert into emma_call_reports(
//        security_id,
//        report_post,
//        report_number,
//        plan_id,
//        report_date,
//        site_id,
//        officer_name,
//        incident_description,
//        incident_followup,
//        police_yn,
//        police_time,
//        police_name,
//        reslife_yn,
//        reslife_time,
//        reslife_name,
//        other_service,
//        other_service_yn,
//        other_service_time,
//        other_service_name
//      ) VALUES (
//        '" . $securityId . "',
//        '" . $reportPost . "',
//        '" . $reportNum . "',
//        '" . $planId . "',
//        '" . date('Y-m-d H:i:s') . "',
//        '" . $siteId . "',
//        '" . $officerName . "',
//        '" . $incidentDescription . "',
//        '" . $resultingAction . "',
//        '" . $policeYN . "',
//        '" . $policeTime . "',
//        '" . $policeName . "',
//        '" . $resLifeYN . "',
//        '" . $resLifeTime . "',
//        '" . $resLifeName . "',
//        '" . $otherNotified . "',
//        '" . $otherYN . "',
//        '" . $otherTime . "',
//        '" . $otherName . "'
//      )
//    ");

    $personType = $_POST['person-type'];
    $personName = $_POST['person-name'];
    $personPhone = $_POST['person-phone'];
    $personOrg = $_POST['person-org'];
    for ($j = 0; $j < count($personName); $j++) {
      $enterCallReportWitnsses = insert_emma_call_report_witness($personType[$j], $personName[$j], $personPhone[$j], $personOrg[$j], $reportNum);
//          $fvmdb->query("
//        insert into emma_call_report_witness(
//          type,
//          name,
//          phone,
//          org,
//          call_report_number
//        )VALUES (
//          '" . $personType[$j] . "',
//          '" . $personName[$j] . "',
//          '" . $personPhone[$j] . "',
//          '" . $personOrg[$j] . "',
//          '" . $reportNum . "'
//        )
//      ");
    }
  }
}

$securities = select_securitiy_with_securityID($securityId);
//$fvmdb->query("
//  SELECT *
//  FROM emma_securities
//  WHERE emma_security_id = '" . $securityId . "'
//");
if ($security = $securities->fetch_assoc()) {
  $type = $security['emma_security_type_id'];
  $siteId = $security['emma_site_id'];
  $description = $security['description'];
  $comments = $security['comments'];
  $latitude = $security['latitude'];
  $longitude = $security['longitude'];
  $address = '';
  $securityId = $security['emma_security_id'];
} else {
  $errors['security'] = 'Can not find security';
  $type = '';
  $siteId = '';
  $description = '';
  $comments = '';
  $latitude = '';
  $longitude = '';
  $address = '';
  $securityId = '';
}

$users = select_user_from_userID($userId);
//$fvmdb->query("
//  SELECT *
//  FROM users
//  WHERE id = '" . $userId . "'
//");
if (!$user = $users->fetch_assoc()) {
  $errors['user'] = 'Can not find user';
}

$FcmServerKey = 'AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db';
$phone = 'faZ9b8GrO8k:APA91bHkZl8ccQXogP7kxhsyTHl_eM6DcyOQkU3pQ5HZD5Rl0zoIoqnsByq_9aLVqBwK36H-Uf3IpOu6OnScvUi6RYOPs0G9pckLptGZlAdQ1DLTEvxqWFIFFbbIUvi9l7SHdlCNObKn';
//$topics = '/topics/emma-' . $user['emma_plan_id'];
if((strpos(dirname($_SERVER['PHP_SELF']),'git-') !== false  || strpos(dirname($_SERVER['PHP_SELF']),'/egor') !== false)){
    $topics = '/topics/betaemma-' . $user['emma_plan_id'];
    $webTopics = '/topics/betaemma-web-'.$user['emma_plan_id'];
}
else{
    $topics = '/topics/emma-' . $user['emma_plan_id'];
    $webTopics = '/topics/emma-web-'.$user['emma_plan_id'];
}

if (empty($errors)) {
  $securityTypeName = "Security";
  $securityTypes = select_securitiyType_with_typeID($type);
//  $fvmdb->query("
//    SELECT *
//    FROM emma_security_types
//    WHERE emma_security_type_id = '" . $type . "'
//  ");
  if ($securityType = $securityTypes->fetch_assoc()) {
    $securityTypeName = $securityType['name'];
  }

  $siteName = 'unknown';
  $sites = select_site_with_siteID($siteId);
//  $fvmdb->query("
//    SELECT *
//    FROM emma_sites
//    WHERE emma_site_id = '" . $siteId . "'
//  ");
  if ($site = $sites->fetch_assoc()) {
    $siteName = $site['emma_site_name'];
  }

  $postData = array(
    'to' => $topics,
    'time_to_live' => 900,
    'content_available' => true,
    'data' => array(
      'GROUP_ALERT' => 'true',
      'SECURITY' => 'true',
      'event' => 'SECURITY CLOSED: ' . $securityTypeName,
      'emergency_lat' => $latitude,
      'emergency_lng' => $longitude,
      'emergency_address' => $address,
      'security_id' => $securityId,
      'groups' => array(),
      'site' => $siteName,
      'description' => $description,
      'comments' => $comments
    )
  );
  $groups = select_groups_with_planID($user['emma_plan_id']);
//  $fvmdb->query("
//    select *
//    from emma_groups
//    where emma_plan_id = '" . $user['emma_plan_id'] . "'
//    and security = 1
//  ");
  while ($group = $groups->fetch_assoc()) {
    $postData['data']['groups'][] = $group['emma_group_id'];
      if((strpos(dirname($_SERVER['PHP_SELF']),'git-') !== false  || strpos(dirname($_SERVER['PHP_SELF']),'/egor') !== false)){
          $IOSTopics[] = "'betaemma-plan" . $user['emma_plan_id'] . "-" . $group ."' in topics";
      }
      else{
          $IOSTopics[] = "'emma-plan" . $user['emma_plan_id'] . "-" . $group ."' in topics";
      }
//    $IOSTopics[] = "'emma-plan" . $user['emma_plan_id'] . "-" . $group['emma_group_id'] . "' in topics";
  }

  $jsonData = json_encode($postData);
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: key=AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db',
    'Content-Type: application/json'
  ));
  $response = curl_exec($ch);

  $responseData = json_decode($response);

  $data['response-security'] = $responseData;

  $postData = array(
    'to' => $topics,
    'time_to_live' => 900,
    'content_available' => true,
    'data' => array(
      'USER_ALERT' => 'true',
      'USER_SECURITY' => 'true',
      'sound' => true,
      'vibrate' => true,
      'event' => 'SECURITY CLOSED: ' . $securityTypeName,
      'emergency_address' => $address,
      'security_id' => $securityId,
      'users' => array(),
      'message' => 'SECURITY CLOSED: ' . $securityTypeName,
      'message_id' => $messageId
    )
  );
  $postData['data']['users'][] = $security['created_by_id'];

  $ch = curl_init('https://fcm.googleapis.com/fcm/send');
  curl_setopt_array($ch, array(
    CURLOPT_POST => TRUE,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_HTTPHEADER => array(
      'Authorization: key=' . $FcmServerKey,
      'Content-Type: application/json'
    ),
    CURLOPT_POSTFIELDS => json_encode($postData)
  ));
  $response = curl_exec($ch);
  $responseData = json_decode($response);
  $data['response-creator'] = $responseData;


  //ios close security APNS

    $IOSTopicsString = $IOSTopics[0];
    if (count($IOSTopics) > 0) {
        for ($i = 1; ($i < 5 && $i < count($IOSTopics)); $i++) {
            $IOSTopicsString .= ' || ' . $IOSTopics[$i];
        }
    }

    $data['iosTopicString'] = $IOSTopicsString;
    $data['iosTopics'] = $IOSTopics;

    $postIOSData = array(
        'condition' => $IOSTopicsString,
        'time_to_live' => 600,
        'priority' => 'high',
        'notification' => array(
            'title' => 'SECURITY CLOSED: ' . $securityTypeName,
            'sound' => 'notification.aiff',
            'badge' => '1'
        ),
        'data' => array(
            'GROUP_ALERT' => 'true',
            'event' => 'SECURITY CLOSED: ' . $securityTypeName,
            'emergency_lat' => $latitude,
            'emergency_lng' => $longitude,
            'emergency_address' => $address,
            'security_id' => $securityId
        )
    );

    $ch = curl_init('https://fcm.googleapis.com/fcm/send');
    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Authorization: key=' . $FcmServerKey,
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postIOSData)
    ));
    $IOSresponse = curl_exec($ch);

    $iosResult = json_decode($IOSresponse);
    $data['iosresponse'] = $iosResult;

}
$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);