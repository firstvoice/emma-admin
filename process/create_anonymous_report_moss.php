<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/12/2019
 * Time: 2:36 PM
 */


require('../include/db.php');
include('../include/processing.php');


$data = array();
$errors = array();
$userId = $emmadb->real_escape_string($_POST['user']);
$planId = $emmadb->real_escape_string($_POST['plan']);
$description = $emmadb->real_escape_string($_POST['description']);


if(empty($userId) || empty($planId))
{
    $errors['User Error'] = 'Could not gather user Information from server';
}

if (empty($errors)) {
    insert_anonymous_report_moss($userId, $planId, $description);
}


$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);