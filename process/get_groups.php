<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/6/2017
 * Time: 10:56 AM
 */

include('../include/db.php');
include('../include/processing.php');

$columns = $_GET['columns'];
$draw = $fvmdb->real_escape_string($_GET['draw']);
$length = $fvmdb->real_escape_string($_GET['length']);
$order = $_GET['order'];
$search = $_GET['search'];
$start = $fvmdb->real_escape_string($_GET['start']);
$active = $fvmdb->real_escape_string($_GET['active']);
$emmaPlanId = $fvmdb->real_escape_string($_GET['emma-plan-id']);

$orderString = $columns[$order[0]['column']]['data'] . ' ' . $order[0]['dir'];

$data = array();

$groups = select_getgroups_groups($emmaPlanId,$orderString,$length,$start);
//    $fvmdb->query("
//    SELECT SQL_CALC_FOUND_ROWS eg.name, eg.emma_group_id, eg.info_only, eg.admin, eg.security, eg.emma_sos, eg.`911admin`
//    FROM emma_groups AS eg
//    WHERE eg.emma_plan_id = " . $emmaPlanId . "
//    GROUP BY eg.emma_group_id
//    ORDER BY ". $orderString ."
//    LIMIT " . $length . " OFFSET " . $start . "
//");

//, group_concat(DISTINCT s.emma_site_name ORDER BY s.emma_site_name)  AS sites
//JOIN emma_group_site_bridge AS b ON b.group_id = eg.emma_group_id
//    JOIN emma_sites AS s ON s.emma_site_id = b.site_id
//AND (" . ($_GET['columns'][0]['search']['value'] != "" ? "u.username like ('%" . $_GET['columns'][0]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][1]['search']['value'] != "" ? "u.firstname like ('%" . $_GET['columns'][1]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][2]['search']['value'] != "" ? "u.lastname like ('%" . $_GET['columns'][2]['search']['value'] . "%')" : "1") . ")
$found = select_FOUND_ROWS();
//    $fvmdb->query("
//    SELECT FOUND_ROWS()
//");
$count = $found->fetch_assoc();

$data['iTotalRecords'] = $count['FOUND_ROWS()'];
$data['iTotalDisplayRecords'] = $count['FOUND_ROWS()'];
$data['sEcho'] = $draw;
$data['aaData'] = array();
while ($group = $groups->fetch_assoc()) {
  $inactiveUsers = select_getgroups_inactiveusers($group['emma_group_id']);
//      $fvmdb->query("
//    select count(*) as number
//    from emma_user_groups ug
//    join users u on ug.user_id = u.id
//    where ug.emma_group_id = '".$group['emma_group_id']."'
//    and u.active = 0
//  ");
  $inactiveUser = $inactiveUsers->fetch_assoc();
  $activeUsers = select_getgroups_activeusers($group['emma_group_id']);
//      $fvmdb->query("
//    select count(*) as number
//    from emma_user_groups ug
//    join users u on ug.user_id = u.id
//    where ug.emma_group_id = '".$group['emma_group_id']."'
//    and u.active = 1
//  ");
  $activeUser = $activeUsers->fetch_assoc();
  $group['inactive_users'] = $inactiveUser['number'];
  $group['active_users'] = $activeUser['number'];
  $data['aaData'][] = $group;
}

echo json_encode($data);
