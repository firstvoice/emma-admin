<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/23/2017
 * Time: 12:16 PM
 */

require('../include/db.php');
include('../include/processing.php');

function geocode ($address) {
  $address = urlencode($address);
  $url = "http://maps.google.com/maps/api/geocode/json?&address={$address}";
  $resp_json = file_get_contents($url);
  $resp = json_decode($resp_json, true);
  if ($resp['status'] === 'OK') {
    $lat = $resp['results'][0]['geometry']['location']['lat'];
    $lon = $resp['results'][0]['geometry']['location']['lng'];
    $formatted_address = $resp['results'][0]['formatted_address'];
    if ($lat && $lon && $formatted_address) {
      $data_arr = array();
      array_push(
        $data_arr,
        $lat,
        $lon,
        $formatted_address
      );
      return $data_arr;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

function reverseGeocode ($lat, $lng) {
  $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng={$lat},{$lng}&key=AIzaSyDKgdasUySqdwbJx3Al3V6F4HvlnFVi2kk";
  $resp_json = file_get_contents($url);
  $resp = json_decode($resp_json, true);
  if ($resp['status'] === 'OK') {
    $address = $resp['results'][0]['formatted_address'];
    return $address;
  } else {
    return "UNKNOWN ADDRESS";
  }
}

$data = array();
$errors = array();
$userId = $fvmdb->real_escape_string($_POST['user-id']);
$planId = $fvmdb->real_escape_string($_POST['plan-id']);
$typeId = $fvmdb->real_escape_string($_POST['type-id']);
$siteId = $fvmdb->real_escape_string($_POST['site-id']);
$pin = $fvmdb->real_escape_string($_POST['pin']);
$description = $fvmdb->real_escape_string($_POST['description']);
$comments = $fvmdb->real_escape_string($_POST['comments']);
$latitude = $fvmdb->real_escape_string($_POST['latitude']);
$longitude = $fvmdb->real_escape_string($_POST['longitude']);
$address = "Unkown Address";

if(empty($siteId)){$errors['site'] = 'Site is required.';}
if(empty($typeId)){$errors['security'] = 'Security is required.';}
if(empty($description)){$errors['description'] = 'Emergency Details is required.';}

if (empty($latitude) || empty($longitude) || $latitude == "" || $longitude == "") {
  $sites = select_site_with_siteID($siteId);
//  $fvmdb->query("
//    SELECT *
//    FROM emma_sites
//    WHERE emma_site_id = '" . $siteId . "'
//  ");
  if ($site = $sites->fetch_assoc()) {
    $address = $site['emma_site_street_address'] . ', ' .
      $site['emma_site_city'] . ', ' .
      $site['emma_site_state'] . ', ' .
      $site['emma_site_zip'];
    $coords = geocode($address);
    if ($coords && $coords[0] && $coords[1]) {
      $latitude = $coords[0];
      $longitude = $coords[1];
    }
  }
} else {
  $address = reverseGeocode($latitude, $longitude);
}

$users = select_user_from_userID($userId);
//$fvmdb->query("
//  SELECT *
//  FROM users
//  WHERE id = '" . $userId . "'
//");
if (!$user = $users->fetch_assoc()) {
  $errors['user'] = 'Can not find user';
}

$FcmServerKey = 'AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db';
$phone = 'faZ9b8GrO8k:APA91bHkZl8ccQXogP7kxhsyTHl_eM6DcyOQkU3pQ5HZD5Rl0zoIoqnsByq_9aLVqBwK36H-Uf3IpOu6OnScvUi6RYOPs0G9pckLptGZlAdQ1DLTEvxqWFIFFbbIUvi9l7SHdlCNObKn';
$topics = '/topics/emma-' . $user['emma_plan_id'];
$IOSTopics = array();

if (empty($errors)) {
  $sound = true;
  $vibrate = true;
  $securityTypeName = "Security";
  $securityTypes = select_securitiyType_with_typeID($typeId);
//  $fvmdb->query("
//    SELECT *
//    FROM emma_security_types
//    WHERE emma_security_type_id = '" . $typeId . "'
//  ");
  if ($securityType = $securityTypes->fetch_assoc()) {
    $securityTypeName = $securityType['name'];
  }
  $insertSecurity = insert_emma_security($planId, $siteId, $typeId, $userId, date('Y-m-d H:i:s'), $pin,
      $latitude, $longitude, 1, '', '', $description, $comments);
//    $fvmdb->query("
//    INSERT INTO emma_securities (
//      emma_plan_id,
//      emma_site_id,
//      emma_security_type_id,
//      created_by_id,
//      created_date,
//      created_pin,
//      latitude,
//      longitude,
//      active,
//      closed_by_id,
//      closed_date,
//      description,
//      comments
//    ) VALUES (
//      '" . $planId . "',
//      '" . $siteId . "',
//      '" . $typeId . "',
//      '" . $userId . "',
//      '" . date('Y-m-d H:i:s') . "',
//      '" . $pin . "',
//      '" . $latitude . "',
//      '" . $longitude . "',
//      1,
//      '',
//      '',
//      '" . $description . "',
//      '" . $comments . "'
//    )
//  ");
  $securityId = $emmadb->insert_id;

  $insertSecurityMessage = insert_emma_security_messages($securityId, $userId, 'Security Created', $description, $comments);
//  $fvmdb->query("
//    INSERT INTO emma_security_messages (
//      security_id,
//      message,
//      created_by_id,
//      created_date,
//      type,
//      description,
//      comments
//    ) VALUES (
//      '" . $securityId . "',
//      'Security Created',
//      '" . $userId . "',
//      '" . date('Y-m-d H:i:s') . "',
//      '0',
//      '" . $description . "',
//      '" . $comments . "'
//    )
//  ");
  $messageId = $emmadb->insert_id;

  $siteName = 'unknown';
  $sites = select_site_with_siteID($siteId);
//  $fvmdb->query("
//    SELECT *
//    FROM emma_sites
//    WHERE emma_site_id = '" . $siteId . "'
//  ");
  if ($site = $sites->fetch_assoc()) {
    $siteName = $site['emma_site_name'];
  }

  $postData = array(
    'to' => $topics,
    'time_to_live' => 900,
    'content_available' => true,
    'data' => array(
      'GROUP_ALERT' => 'true',
      'SECURITY' => 'true',
      'event' => $securityTypeName . ' Description: ' . $description,
      'sound' => $sound,
      'vibrate' => $vibrate,
      'emergency_lat' => $latitude,
      'emergency_lng' => $longitude,
      'emergency_address' => $address,
      'security_id' => $securityId,
      'groups' => array(),
      'site' => $siteName,
      'description' => $description,
      'comments' => $comments
    )
  );
  $groups = select_securityGroups_with_planID($planId);
//  $fvmdb->query("
//    select *
//    from emma_groups
//    where security = 1
//    and emma_plan_id = '" . $planId . "'
//  ");
  while ($group = $groups->fetch_assoc()) {
    $postData['data']['groups'][] = $group['emma_group_id'];
    $IOSTopics[] = "'emma-plan" . $user['emma_plan_id'] . "-" . $group['emma_group_id'] . "' in topics";
  }

  $jsonData = json_encode($postData);

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: key=AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db',
    'Content-Type: application/json'
  ));
  $res = curl_exec($ch);
  if (!$res) {
    $errors['curl'] = curl_error($ch);
    $errors['curlno'] = curl_errno($ch);
  }

  $data['response'] = json_decode($res);

//------------------ios noticfication-----------------------
    $IOSTopicsString = $IOSTopics[0];
    if (count($IOSTopics) > 0) {
        for ($i = 1; ($i < 5 && $i < count($IOSTopics)); $i++) {
            $IOSTopicsString .= ' || ' . $IOSTopics[$i];
        }
    }

    $data['iosTopicString'] = $IOSTopicsString;
    $data['iosTopics'] = $IOSTopics;

    $postIOSData = array(
//        'to' => $IOSTopicsString,
        'condition' => $IOSTopicsString,
        'time_to_live' => 600,
        'priority' => 'high',
        'notification' => array(
            'body' => $securityTypeName . ' Description: ' . $description,
            'sound' => 'notification.aiff',
            'badge' => '1'
        ),
        'data' => array(
            'GROUP_ALERT' => 'true',
            'event' => $securityTypeName . ' Description: ' . $description,
            'emergency_lat' => $latitude,
            'emergency_lng' => $longitude,
            'emergency_address' => $address,
            'security_id' => $securityId,
            'groups' => array(),
            'site' => $site,
            'description' => $description,
            'comments' => $comments
        )
    );
    if(!$sound){
        $postIOSData['notification']['sound'] = 'silent.aiff';
    }

    $ch = curl_init('https://fcm.googleapis.com/fcm/send');
    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Authorization: key=' . $FcmServerKey,
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postIOSData)
    ));
    $IOSresponse = curl_exec($ch);
    $data['IOSresponse'] = json_decode($IOSresponse);

    if (count($IOSTopics) > 5) {
        for ($i = 5; ($i < 10 && $i < count($IOSTopics)); $i++) {
            if ($i == 5) {
                $IOSTopicsString = $IOSTopics[$i];
            }
            $IOSTopicsString .= ' || ' . $IOSTopics[$i];
        }

        $postIOSData['condition'] = $IOSTopicsString;

        $ch = curl_init('https://fcm.googleapis.com/fcm/send');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Authorization: key=' . $FcmServerKey,
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postIOSData)
        ));
        $IOSresponse2 = curl_exec($ch);

        $data['IOSresponse'. $i] = json_decode($IOSresponse2);

    }


}
$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);