<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 4/6/2018
 * Time: 1:23 PM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$scriptId = $fvmdb->real_escape_string($_POST['emma-script-id']);

if (empty($scriptId)) {
  $errors['script-id'] = 'Script Id not provided';
}

if (empty($errors)) {
  $deleteScript = delete_emmaScript($scriptId);
//  $fvmdb->query("
//    delete from emma_scripts
//    where emma_script_id = '" . $scriptId . "'
//  ");
  if (!$deleteScript) {
    $errors['sql'] = $fvmdb->error;
  }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);