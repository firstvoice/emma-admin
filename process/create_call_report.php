<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 6/15/2018
 * Time: 3:42 PM
 */

include('../include/db.php');
include('../include/processing.php');
require_once('../include/fpdf/fpdf.php');
require_once('../include/fpdf/fpdi.php');

$id = $fvmdb->real_escape_string($_GET['id']);

$callReports = select_callReportsBySite_with_reportID($id);
//    $fvmdb->query("
//  select ecr.*, es.emma_site_name, es.emma_site_street_address, es.emma_site_city, es.emma_site_state, es.emma_site_zip
//  from emma_call_reports ecr
//  join emma_sites es on ecr.site_id = es.emma_site_id
//  where id = '" . $id . "'
//");
if ($callReport = $callReports->fetch_assoc()) {
  // echo '<pre>' . print_r($callReport, true) . '</pre>';

//create pdf
  $pdf = new FPDI('P', 'mm', 'A4');

//make background image
  $pdf->setSourceFile('../documents/blank.pdf');
  $tplIdx = $pdf->importPage(1);
  $pdf->AddPage();
  $pdf->useTemplate($tplIdx, 0, 0, 210, 297, false);
  $pdf->SetAutoPageBreak(false);

  $clientId = $callReport['client_id'];
  $clientName = $callReport['client-name'];
  $reportDate = date('Y-m-d', strtotime($callReport['report_date']));
  $reportTime = date('H:i:s', strtotime($callReport['report_date']));
  $reportPost = $callReport['report_post'];
  $locationName = $callReport['emma_site_name'];
  $locationId = $callReport['site_id'];
  $incidentAddress = $callReport['emma_site_street_address'];
  $incidentCity = $callReport['emma_site_city'];
  $incidentState = $callReport['emma_site_state'];
  $incidentZip = $callReport['emma_site_zip'];
  $policeName = $callReport['police_name'];
  $policeTime = $callReport['police_time'];
  $reslifeName = $callReport['reslife-name'];
  $reslifeTime = $callReport['reslife-time'];
  $otherNotified = $callReport['other_service'];
  $otherName = $callReport['other_service_name'];
  $otherTime = $callReport['other_service_time'];
  $officerName = $callReport['officer_name'];
  $incidentDescription = $callReport['incident_description'];
  $resultingAction = $callReport['incident_followup'];

//header
  $pdf->SetFont('Arial', '', 20);
  $pdf->SetTextColor(0, 120, 193);
  $pdf->SetXY(0, 0);
  $pdf->MultiCell(0, 26, $clientName . 'Closed Security Call Report', '', 'C', 0);
//general info
  $pdf->setFont('Arial', '', 10);
  $pdf->SetXY(15, 20);
  $pdf->MultiCell(100, 26, 'Reported By:___________________', '', 'L', 0);
  $pdf->SetXY(75, 20);
  $pdf->MultiCell(100, 26, 'Date of Report:_________________', '', 'L', 0);
  $pdf->SetXY(15, 30);
  $pdf->MultiCell(100, 26, 'Post:______________________', '', 'L', 0);
  $pdf->SetXY(68, 30);
  $pdf->MultiCell(130, 26,
    'Location:_________________________________________________________', '',
    'L', 0);
  $pdf->SetXY(135, 20);
  $pdf->MultiCell(95, 26, 'Report Number:_________________', '', 'L', 0);

  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetXY(36, 20);
  $pdf->MultiCell(100, 26, $clientName, '', 'L', 0);
  $pdf->SetXY(100, 20);
  $pdf->MultiCell(100, 26, $reportDate . ' ' . $reportTime, '', 'L', 0);
  $pdf->SetXY(32, 30);
  $pdf->MultiCell(100, 26, $reportPost, '', 'L', 0);
  $pdf->SetXY(83, 30);
  $pdf->MultiCell(130, 26,
    $locationName . ': ' . $incidentAddress . ' ' . $incidentCity . ', ' .
    $incidentState . ' ' . $incidentZip, '', 'L', 0);
  $pdf->SetXY(161, 20);
  $pdf->MultiCell(95, 26, $reportNum, '', 'L', 0);

//specific header
  $pdf->SetFont('Arial', '', 16);
  $pdf->SetTextColor(251, 252, 252);
  $pdf->SetFillColor(0, 120, 193);
  $pdf->SetXY(15, 50);
  $pdf->MultiCell(180, 10, 'Incident Information', '', 'C', true);
//notified services header
  $pdf->setFont('Arial', '', 10);
  $pdf->SetTextColor(0, 120, 193);
  $pdf->SetXY(15, 52);
  $pdf->MultiCell(50, 26, 'Notified Services:', '', 'L', 0);
//police/fire notified
  $pdf->SetFillColor(209, 211, 212);
  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetXY(20, 70);
  $pdf->MultiCell(37, 4, 'Police/Fire', '', 'L', true);
  $pdf->SetXY(50, 70);
  $pdf->MultiCell(37, 4, $callReport['police_yn'], '', 'L', true);
  $pdf->SetXY(70, 70);
  $pdf->MultiCell(100, 4, $policeName, '', 'L', true);
  $pdf->SetXY(150, 70);
  $pdf->MultiCell(37, 4, $policeTime, '', 'L', true);
//reslife notified
  $pdf->SetXY(20, 75);
  $pdf->MultiCell(37, 4, 'Res-Life', '', 'L', 0);
  $pdf->SetXY(50, 75);
  $pdf->MultiCell(37, 4, $callReport['reslife_yn'], '', 'L', 0);
  $pdf->SetXY(70, 75);
  $pdf->MultiCell(100, 4, $reslifeName, '', 'L', 0);
  $pdf->SetXY(150, 75);
  $pdf->MultiCell(37, 4, $reslifeTime, '', 'L', 0);
//other notified
  if (!empty($otherNotified)) {
    $pdf->SetXY(20, 80);
    $pdf->MultiCell(37, 4, $otherNotified, '', 'L', true);
    $pdf->SetXY(50, 80);
    $pdf->MultiCell(37, 4, $callReport['other_service_yn'], '', 'L', true);
    $pdf->SetXY(70, 80);
    $pdf->MultiCell(100, 4, $otherName, '', 'L', true);
    $pdf->SetXY(150, 80);
    $pdf->MultiCell(37, 4, $otherTime, '', 'L', true);
  }
//Witnesses involved header
  $pdf->setFont('Arial', '', 10);
  $pdf->SetTextColor(0, 120, 193);
  $pdf->SetXY(15, 82);
  $pdf->MultiCell(50, 26, 'Witnesses/Parties Involved:', '', 'L', 0);

//Witnesses
  $pdf->setTextColor(0, 0, 0);
  $reportWitnesses = select_witnessReport_with_reportNumber($callReport['report_number']);
//  $fvmdb->query("
//    select @pos=@pos+1 as pos, ecrw.*
//    from emma_call_report_witness ecrw
//    join (select @pos:=0) p
//    where call_report_number = '" . $callReport['report_number'] . "'
//  ");
  while ($reportWitness = $reportWitnesses->fetch_assoc()) {
    if (!empty($reportWitness['name'])) {
      if ($reportWitness['pos'] % 2 == 0) {
        $fill = 'true';
      } else {
        $fill = '0';
      }
      $pdf->SetXY(20, 100 + ($reportWitness['pos'] * 5));
      $pdf->MultiCell(100, 4, $reportWitness['type'], '', 'L', $fill);
      $pdf->SetXY(50, 100 + ($reportWitness['pos'] * 5));
      $pdf->MultiCell(37, 4, $reportWitness['name'], '', 'L', $fill);
      $pdf->SetXY(100, 100 + ($reportWitness['pos'] * 5));
      $pdf->MultiCell(50, 4, $reportWitness['phone'], '', 'L', $fill);
      $pdf->SetXY(150, 100 + ($reportWitness['pos'] * 5));
      $pdf->MultiCell(37, 4, $reportWitness['org'], '', 'L', $fill);
    }
  }
//officer
  $pdf->SetTextColor(0, 120, 193);
  $pdf->SetXY(15, 100 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(100, 26, 'Reporting Officer:________________________', '',
    'L', 0);

  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetXY(44, 100 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(100, 26, $officerName, '', 'L', 0);

//description
  $pdf->SetTextColor(0, 120, 193);
  $pdf->SetXY(15, 110 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(100, 26, 'Incident Description:', '', 'L', 0);
  $pdf->SetFillColor(209, 211, 212);
  $pdf->SetXY(15, 125 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(180, 40, '', '', 'L', true);

  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetFillColor(209, 211, 212);
  $pdf->SetXY(16, 126 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(178, 4, $incidentDescription, '', 'L', 0);

//after
  $pdf->SetTextColor(0, 120, 193);
  $pdf->SetXY(15, 160 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(100, 26, 'Results of Investigation & Action Taken:', '', 'L',
    0);
  $pdf->SetXY(15, 175 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(180, 40, '', '', 'L', true);

  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetXY(16, 176 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(178, 4, $resultingAction, '', 'L', true);

//supervisor sign
  $pdf->SetTextColor(0, 120, 193);
  $pdf->SetXY(15, 215 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(75, 26, 'Supervisor Name:__________________', '', 'L', 0);
  $pdf->SetXY(80, 215 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(90, 26, 'Supervisor Signature:______________________', '',
    'L', 0);
  $pdf->SetXY(160, 215 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(75, 26, 'Date:____________', '', 'L', 0);

  $pdf->Output();
} else {
  echo 'Can not find call report id: ' . $id;
}
