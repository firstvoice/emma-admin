<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/20/2017
 * Time: 12:28 PM
 */

include('../include/db.php');
include('../include/processing.php');
$data = array();

function geocode ($address) {
  $address = urlencode($address);
  $url = "http://maps.google.com/maps/api/geocode/json?&address={$address}&sensor=false";
  $resp_json = file_get_contents($url);
  $resp = json_decode($resp_json, true);
  if ($resp['status'] === 'OK') {
    $lat = $resp['results'][0]['geometry']['location']['lat'];
    $lon = $resp['results'][0]['geometry']['location']['lng'];
    $formatted_address = $resp['results'][0]['formatted_address'];
    if ($lat && $lon && $formatted_address) {
      $data_arr = array();
      array_push(
        $data_arr,
        $lat,
        $lon,
        $formatted_address
      );
      return $data_arr;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

$errors = array();


$planId = $fvmdb->real_escape_string($_POST['plan-id']);
$name = $fvmdb->real_escape_string($_POST['name']);
$firstName = $fvmdb->real_escape_string($_POST['first-name']);
$lastName = $fvmdb->real_escape_string($_POST['last-name']);
$notes = $fvmdb->real_escape_string($_POST['notes']);
$phone = $fvmdb->real_escape_string($_POST['phone']);
$email = $fvmdb->real_escape_string($_POST['email']);
$address = $fvmdb->real_escape_string($_POST['address']);
$state = $fvmdb->real_escape_string($_POST['state']);
$city = $fvmdb->real_escape_string($_POST['city']);
$zip = $fvmdb->real_escape_string($_POST['zip']);
$lat = $fvmdb->real_escape_string($_POST['lat']);
$lng = $fvmdb->real_escape_string($_POST['lng']);
$ignoreAddress = $fvmdb->real_escape_string($_POST['ignore-address']);
$contact = $fvmdb->real_escape_string($_POST['contact']);
//if(!$ignoreAddress) {
//  $coords = geocode($address . ', ' . $city . ', ' . $state . ', ' . $zip);
//  if (!$coords || !$coords[0] || !$coords[1]) {
//    $errors['invalid-address'] = 'Invalid Address';
//  }
//}

if (empty($planId)) {
  $errors['plan-id'] = 'Empty Plan Id';
}

//check for amount of sites already created.
$COUNT_FIND = select_siteCount_with_planID($planId);
//$fvmdb->query("
//SELECT COUNT(1) as total
//FROM emma_sites
//WHERE emma_sites.emma_plan_id = '".$planId."'
//");
$COUNT = $COUNT_FIND->fetch_assoc();
//Check how many they are allowed to have
$MAX_AMT = select_plan_from_planID($planId);
//$fvmdb->query("
//SELECT emma_plans.max_sites
//FROM emma_plans
//WHERE emma_plans.emma_plan_id = '".$planId."'
//");
$MAX = $MAX_AMT->fetch_assoc();
if($COUNT['total'] >= $MAX['max_sites'])
{
    $errors['Too-Many'] = 'You have already reached your Site limit!';
}
$data['COUNT'] = $COUNT;
$data['MAX'] = $MAX['max_sites'];




if (empty($errors)) {
    //User chose pre-existing contact instead of making a new one.
    if(empty($firstName) && empty($lastName) && empty($email) && empty($phone))
    {
        $insertSite = insert_emma_sites($planId, $name, $address, $city, $state, $zip, $notes, $contact, $lat, $lng);
//        $fvmdb->query("
//    INSERT INTO emma_sites (
//      emma_plan_id,
//      emma_site_name,
//      emma_site_street_address,
//      emma_site_city,
//      emma_site_state,
//      emma_site_zip,
//      emma_site_notes,
//      emma_site_contact_id,
//      emma_site_latitude,
//      emma_site_longitude
//    ) VALUES (
//      '" . $planId . "',
//      '" . $name . "',
//      '" . $address . "',
//      '" . $city . "',
//      '" . $state . "',
//      '" . $zip . "',
//      '" . $notes . "',
//      '" . $contact . "',
//      '".$lat."',
//      '".$lng."'
//    )
//    ");
        if (!$insertSite) {
            $errors['create-site'] = 'Site creation failed: ' . $fvmdb->error;
        }
    }
    else{
        //user chose to make a new one
        $insertContact = insert_emma_site_contacts($planId, $firstName, $lastName, $email, $phone, 1);
//        $fvmdb->query("
//            INSERT INTO emma_site_contacts (
//              emma_plan_id,
//              first_name,
//              last_name,
//              email,
//              phone,
//              active
//            ) VALUES (
//              '" . $planId . "',
//              '" . $firstName . "',
//              '" . $lastName . "',
//              '" . $email . "',
//              '" . $phone . "',
//              1
//            )
//          ");
        $contactId = $emmadb->insert_id;

        $insertSite = insert_emma_sites($planId, $name, $address, $city, $state, $zip, $notes, $contactId, $lat, $lng);
//        $fvmdb->query("
//    INSERT INTO emma_sites (
//      emma_plan_id,
//      emma_site_name,
//      emma_site_street_address,
//      emma_site_city,
//      emma_site_state,
//      emma_site_zip,
//      emma_site_notes,
//      emma_site_contact_id,
//      emma_site_latitude,
//      emma_site_longitude
//    ) VALUES (
//      '" . $planId . "',
//      '" . $name . "',
//      '" . $address . "',
//      '" . $city . "',
//      '" . $state . "',
//      '" . $zip . "',
//      '" . $notes . "',
//      '" . $contactId . "',
//      '".$lat."',
//      '".$lng."'
//    )
//  ");

        if (!$insertSite) {
            $errors['create-site'] = 'Site creation failed: ' . $fvmdb->error;
        }
    }



}

$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);