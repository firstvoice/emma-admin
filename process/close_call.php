<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/17/2019
 * Time: 1:18 PM
 */

require('../include/db.php');
include('../include/processing.php');

$call_id = $fvmdb->real_escape_string($_POST['call_id']);

$data = array();
$errors = array();

if(empty($call_id))
{
   $errors['Invalid'] = 'Call ID was empty';
}

if(empty($errors))
{
    update_emma911CallLog_active_with_callID($call_id, 0);
//    $fvmdb->query("
//    UPDATE emma_911_call_log
//    SET active = 0
//    WHERE call_id = '".$call_id."'
//    ");
}

$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);