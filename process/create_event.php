<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/23/2017
 * Time: 12:16 PM
 */
require('../include/db.php');
include('../include/processing.php');
include('../include/process_cookie.php');

//function geocode ($address) {
//  $address = urlencode($address);
//  $url = "https://maps.google.com/maps/api/geocode/json?&address={$address}&key=AIzaSyDKgdasUySqdwbJx3Al3V6F4HvlnFVi2kk";
//  $resp_json = file_get_contents($url);
//  $resp = json_decode($resp_json, true);
//  if ($resp['status'] === 'OK') {
//    $lat = $resp['results'][0]['geometry']['location']['lat'];
//    $lon = $resp['results'][0]['geometry']['location']['lng'];
//    $formatted_address = $resp['results'][0]['formatted_address'];
//    if ($lat && $lon && $formatted_address) {
//      $data_arr = array();
 //     array_push(
 //       $data_arr,
  //      $lat,
    //    $lon,
      //  $formatted_address,
        //$url
      //);
      //return $data_arr;
    //} else {
//      return false;
//    }
//  } else {
//    return false;
//  }
//}

//function reverseGeocode ($lat, $lng) {
//  $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng={$lat},{$lng}&key=AIzaSyDKgdasUySqdwbJx3Al3V6F4HvlnFVi2kk";
//  $resp_json = file_get_contents($url);
//  $resp = json_decode($resp_json, true);
//  if ($resp['status'] === 'OK') {
//    $address = $resp['results'][0]['formatted_address'];
//    return $address;
//  } else {
//    return "UNKNOWN ADDRESS";
//  }
//}

$data = array();
$errors = array();
$userId = $fvmdb->real_escape_string($_POST['user']);
$isDrill = $fvmdb->real_escape_string($_POST['drill']);
$pin = $fvmdb->real_escape_string($_POST['pin']);
$type = $fvmdb->real_escape_string($_POST['type']);
$subType = $fvmdb->real_escape_string($_POST['sub-type']);
$siteId = $fvmdb->real_escape_string($_POST['site']);
$description = $fvmdb->real_escape_string($_POST['description']);
$comments = $fvmdb->real_escape_string($_POST['comments']);
$groups = $_POST['groups'];
$latitude = $fvmdb->real_escape_string($_POST['lat']);
$longitude = $fvmdb->real_escape_string($_POST['lng']);
$address = $fvmdb->real_escape_string($_POST['address']);
$nearestaddress = $fvmdb->real_escape_string($_POST['nearestaddress']);
$alertEmail = $fvmdb->real_escape_string($_POST['alert-email']);
$alertText = $fvmdb->real_escape_string($_POST['alert-text']);

$desktopConnection = $fvmdb->real_escape_string($_POST['desktopConnection']);
$auth = $fvmdb->real_escape_string($_POST['auth']);

$groups_list = implode(',', $groups);

if(empty($groups)){
    $errors['groups'] = 'Groups are required';
}

if($isDrill == 'on'){
    $isDrill = 1;
}else{
    $isDrill = 0;
}

if(empty($siteId))$errors['site'] = 'Site is required';
if(empty($description) && empty($desktopConnection))$errors['location-description'] = 'Emergency Description is required';
if(empty($comments) && empty($desktopConnection))$errors['comments'] = 'Comments are required';
$siteName = '';
$siteInfos = select_site_with_siteID($siteId);
if($siteInfos->num_rows > 0){
    $siteInfo = $siteInfos->fetch_assoc();
    $siteName = $siteInfo['emma_site_name'];
}

if (empty($latitude) || empty($longitude)) {
  //$sites = $fvmdb->query("
 //   SELECT *
 ///   FROM emma_sites
 //   WHERE emma_site_id = '" . $siteId . "'
 // ");
 // if ($site = $sites->fetch_assoc()) {
 //   $address = $site['emma_site_street_address'] . ', ' .
 //     $site['emma_site_city'] . ', ' .
 //     $site['emma_site_state'] . ', ' .
  //    $site['emma_site_zip'];
    //$coords = geocode($address);
   // if ($coords && $coords[0] && $coords[1]) {
    //  $latitude = $coords[0];
    //  $longitude = $coords[1];
    //}
  //}
//} else {
//  $address = reverseGeocode($latitude, $longitude);
}

//$data['coords'][] = $coords;`
//$data['coords'][] = $coords[0];
//$data['coords'][] = $coords[1];
//$data['coordsUrl'] = $coords[3];

if(empty($address)){

    if(!empty($nearestaddress)){
        $address = $nearestaddress;

    }
    else {
        $address = 'unknown';
        $sitelookup = select_site_with_siteID($siteId);
//        $fvmdb->query("
//            SELECT *
//            FROM emma_sites
//            WHERE emma_site_id = '" . $siteId . "'
//          ");
        if ($site_result = $sitelookup->fetch_assoc()) {
            $address = $site_result['emma_site_name'];
        }
    }
}



if(empty($desktopConnection)) {
    $users = select_user_from_userID($userId);
//    $fvmdb->query("
//      SELECT *
//      FROM users
//      WHERE id = '" . $userId . "'
//    ");

    if (!$user = $users->fetch_assoc()) {
        $errors['user'] = 'Can not find user';
    }
}
else{
    $users = select_user_from_auth($auth);
//    $fvmdb->query("
//    SELECT u.*
//    FROM users as u
//    WHERE u.auth = '".$auth."'
//    ");

    if($users->num_rows > 0)
    {
        $user = $users->fetch_assoc();
        $userId = $user['id'];
    }
}


if(empty($groups)){
    $groups = array();
    $tempGroups = array();
    $groupQuery = select_groupIDs_with_userID_and_planID($user['id'], $USER->emma_plan_id);
//    $fvmdb->query("
//        SELECT g.emma_group_id
//        FROM emma_user_groups AS g
//        WHERE g.user_id = '".$user['id']."'
//    ");
    while($groupResult = $groupQuery->fetch_assoc()){
        $tempGroups[] = $groupResult['emma_group_id'];
    }
    $tempGroups = implode("','", $tempGroups);
    unset($groupQuery);
    unset($groupResult);
    $groupQuery = select_groupsPrivileges_with_groupString($tempGroups);
//    $fvmdb->query("
//        SELECT DISTINCT emma_privilege_id
//        FROM emma_group_privileges
//        WHERE emma_group_id IN ('".$tempGroups."')
//    ");
    while($groupResult = $groupQuery->fetch_assoc()){
        $groups[] = $groupResult['emma_privilege_id'];
    }

}


$FcmServerKey = 'AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db';
$phone = 'faZ9b8GrO8k:APA91bHkZl8ccQXogP7kxhsyTHl_eM6DcyOQkU3pQ5HZD5Rl0zoIoqnsByq_9aLVqBwK36H-Uf3IpOu6OnScvUi6RYOPs0G9pckLptGZlAdQ1DLTEvxqWFIFFbbIUvi9l7SHdlCNObKn';
//$topics = '/topics/emma-' . $user['emma_plan_id'];
if((strpos(dirname($_SERVER['PHP_SELF']),'git-') !== false  || strpos(dirname($_SERVER['PHP_SELF']),'/egor') !== false)){
    $topics = '/topics/betaemma-' . $USER->emma_plan_id;
    $webTopics = '/topics/betaemma-web-'.$USER->emma_plan_id;
}
else{
    $topics = '/topics/emma-' . $USER->emma_plan_id;
    $webTopics = '/topics/emma-web-'.$USER->emma_plan_id;
}
$IOSTopics = array();

if (empty($errors)) {
  $sound = true;
  $vibrate = true;
  $emergencyTypeName = "Emergency";
  $emergencyTypes = select_eventTypes_with_eventTypeID($type);
//  $fvmdb->query("
//    SELECT *
//    FROM emergency_types
//    WHERE emergency_type_id = '" . $type . "'
//  ");
  if ($emergencyType = $emergencyTypes->fetch_assoc()) {
    $emergencyTypeName = $emergencyType['name'];
    $sound = $emergencyType['sound'] == "1";
    $vibrate = $emergencyType['vibrate'] == "1";
  }
  $activeEvents = select_activeParentEmergencies_with_planID($USER->emma_plan_id);
//  $fvmdb->query("
//    select *
//    from emergencies
//    where emma_plan_id = '" . $user['emma_plan_id'] . "'
//    and parent_emergency_id is null
//    and active = '1'
//  ");
  if ($activeEvents->num_rows > 0) {
    $activeEvent = $activeEvents->fetch_assoc();
    $insertEmergency = insert_emergencies($USER->emma_plan_id, $activeEvent['emergency_id'], $siteId, $isDrill, $type, $subType, $description, 1,
        $latitude, $longitude, $address, $comments, date('Y-m-d H:i:s'), $userId, $pin, '', '');
//    $fvmdb->query("
//      INSERT INTO emergencies (
//        emma_plan_id,
//        parent_emergency_id,
//        emma_site_id,
//        drill,
//        emergency_type_id,
//        emergency_sub_type_id,
//        description,
//        active,
//        latitude,
//        longitude,
//        nearest_address,
//        comments,
//        created_date,
//        created_by_id,
//        created_pin,
//        closed_date,
//        closed_by_id
//      ) VALUES (
//        '" . $user['emma_plan_id'] . "',
//        '" . $activeEvent['emergency_id'] . "',
//        '" . $siteId . "',
//        " . $isDrill . ",
//        '" . $type . "',
//        '" . $subType . "',
//        '" . $description . "',
//        '1',
//        '" . $latitude . "',
//        '" . $longitude . "',
//        '" . $address . "',
//        '" . $comments . "',
//        '" . date('Y-m-d H:i:s') . "',
//        '" . $userId . "',
//        '" . $pin . "',
//        '',
//        ''
//      )
//    ");
    $emergencyId = $activeEvent['emergency_id'];

    $insertSystemMessage = insert_system_messages($emergencyId, $userId, 'Sub-Event Created', 0, $description, $comments);
//    $fvmdb->query("
//      INSERT INTO emma_system_messages (
//        emergency_id,
//        message,
//        created_by_id,
//        created_date,
//        type,
//        description,
//        comments
//      ) VALUES (
//        '" . $emergencyID . "',
//        'Sub-Event Created',
//        '" . $userId . "',
//        '" . date('Y-m-d H:i:s') . "',
//        '0',
//        '" . $description . "',
//        '" . $comments . "'
//      )
//    ");
  }
  else {
    $insertEmergency = insert_emergencies_nullParent($USER->emma_plan_id, $siteId, $isDrill, $type, $subType, $description, 1,
        $latitude, $longitude, $address, $comments, date('Y-m-d H:i:s'), $userId, $pin, '', '');
//        $fvmdb->query("
//      INSERT INTO emergencies (
//        emma_plan_id,
//        emma_site_id,
//        drill,
//        emergency_type_id,
//        emergency_sub_type_id,
//        description,
//        active,
//        latitude,
//        longitude,
//        nearest_address,
//        comments,
//        created_date,
//        created_by_id,
//        created_pin,
//        closed_date,
//        closed_by_id
//      ) VALUES (
//        '" . $user['emma_plan_id'] . "',
//        '" . $siteId . "',
//        " . $isDrill . ",
//        '" . $type . "',
//        '" . $subType . "',
//        '" . $description . "',
//        '1',
//        '" . $latitude . "',
//        '" . $longitude . "',
//        '" . $address . "',
//        '" . $comments . "',
//        '" . date('Y-m-d H:i:s') . "',
//        '" . $userId . "',
//        '" . $pin . "',
//        '',
//        ''
//      )
//    ");
    $emergencyId = $emmadb->insert_id;

    $data['notification_id'] = $emergencyId;

    $insertSystemMessage = insert_system_messages($emergencyId, $userId, 'Event Created', 0, $description, $comments);
//    $fvmdb->query("
//      INSERT INTO emma_system_messages (
//        emergency_id,
//        message,
//        created_by_id,
//        created_date,
//        type,
//        description,
//        comments
//      ) VALUES (
//        '" . $emergencyId . "',
//        'Event Created',
//        '" . $userId . "',
//        '" . date('Y-m-d H:i:s') . "',
//        '0',
//        '" . $description . "',
//        '" . $comments . "'
//      )
//    ");
  }
  $messageId = $emmadb->insert_id;

  foreach ($groups as $keyId => $groupId) {
    $insertSystemMessageGroup = insert_emma_system_message_groups($messageId, $groupId);

      $users = select_unreguser_with_group($groupId);
      while($user = $users->fetch_assoc()) {
          $data['userquery'][] = $user;
          $numbersarray[] = $user['phone'];
      }
//    $fvmdb->query("
//      insert into emma_system_message_groups
//      (emma_system_message_id, emma_group_id)
//      values
//      ('" . $messageId . "', '" . $groupId . "')
//    ");
  }
    $data['numbers'] = $numbersarray;

  $aeds = select_closestAEDs($latitude, $longitude);
//  $fvmdb->query("
//    SELECT
//      a.aed_id,
//      a.location_id,
//      a.serialnumber as serial,
//      c.name as contact,
//      c.phone as contact_phone,
//      m.model,
//      m.mobile_image,
//      b.brand,
//      a.location as placement,
//      l.name AS location,
//      o.name AS organization,
//      a.latitude,
//      a.longitude,
//      'public' as type,
//      ACOS(SIN(RADIANS(a.latitude)) * SIN(RADIANS('" . $latitude . "')) +
//           COS(RADIANS(a.latitude)) * COS(RADIANS('" . $longitude .
//      "')) * COS(RADIANS(a.longitude) - RADIANS('" .
//      $longitude . "'))) *
//      6380   AS 'distance'
//    from aeds a
//    join locations l on a.location_id = l.id
//    JOIN organizations o ON l.orgid = o.id
//    JOIN aed_models m on a.aed_model_id = m.aed_model_id
//    JOIN aed_brands b on m.aed_brand_id = b.aed_brand_id
//    LEFT JOIN contacts c on a.site_coordinator_contact_id = c.id
//    WHERE l.display = 'yes'
//    AND a.display = '1'
//    AND a.approved = '1'
//    order by distance
//    limit 1
//  ");

  $site = 'unknown';
  $sites = select_site_with_siteID($siteId);
//  $fvmdb->query("
//    SELECT *
//    FROM emma_sites
//    WHERE emma_site_id = '" . $siteId . "'
//  ");
  if ($site = $sites->fetch_assoc()) {
    $site = $site['emma_site_name'];
  }

  $postData = array(
    'to' => $topics,
//        'condition' => "'alerts' in topics || 'aednotify' in topics || 'heartsafe' in topics || 'testing' in topics'",
    'time_to_live' => 900,
    'content_available' => true,
//        'notification' => array(
//            'title' => 'CPR Emergency: ' . $emergencyLat . ', ' . $emergencyLng,
//            'sound' => 'notification.aiff',
//            'badge' => '1'
//        ),
    'data' => array(
      'GROUP_ALERT' => 'true',
      'event' => ($isDrill ? '(DRILL) ' : '') . $emergencyTypeName . ' Near: ' . $address,
      'sound' => $sound,
      'vibrate' => $vibrate,
      'emergency_lat' => $latitude,
      'emergency_lng' => $longitude,
      'emergency_address' => $address,
      'emergency_id' => $emergencyId,
      'groups' => array(),
      'site' => $site,
      'description' => $description,
      'comments' => $comments,
      'plan_ID' => $USER->emma_plan_id
    )
  );
  foreach ($groups as $group) {
      insert_emergency_received_groups($emergencyId, $group);
//    $fvmdb->query("
//      INSERT IGNORE INTO emergency_received_groups (emergency_id, emma_group_id)
//      VALUES
//      (
//        '" . $emergencyId . "',
//        '" . $group . "'
//      )
//    ");
    $postData['data']['groups'][] = $group;
//    $IOSTopics[] = "'emma-plan" . $user['emma_plan_id'] . "-" . $group . "' in topics";
      if((strpos(dirname($_SERVER['PHP_SELF']),'git-') !== false  || strpos(dirname($_SERVER['PHP_SELF']),'/egor') !== false)){
          $IOSTopics[] = "'betaemma-plan" . $USER->emma_plan_id . "-" . $group ."' in topics";
      }
      else{
          $IOSTopics[] = "'emma-plan" . $USER->emma_plan_id . "-" . $group ."' in topics";
      }

//        $associateGroup = $fvmdb->query("
//            INSERT INTO emergency_groups (
//              emergency_id,
//              emma_group_id
//            ) VALUES (
//              '" . $emergencyId . "',
//              '" . $group['emma_group_id'] . "'
//            )
//        ");
  }

    /*
     * Generate IOS information
     */
//    $allgroupArray = array();
//    $IOS_emma_topic_array = array();
//    $allgroups = $fvmdb->query("
//        SELECT emma_group_id
//        FROM emma_groups
//        WHERE emma_plan_id = '". $user['emma_plan_id'] ."'
//    ");
//    while($allgroup = $allgroups->fetch_assoc()){
//        $allgroupArray[] = $allgroup['emma_group_id'];
//    }
//    sort($allgroupArray);
//    foreach($postData['data']['groups'] AS $IOSTopic){
//        $IOS_emma_topic_array[] = "'emma-plan".$user['emma_plan_id']."-".$IOSTopic."' in topics";;
//        foreach($allgroupArray AS $allgroupid){
//            $tempArray = array();
//            $tempArray[] = $IOSTopic.'';
//            if($IOSTopic != $allgroupid) {
//                $i = array_search($allgroupid, $allgroupArray);
//                for (; $i < count($allgroupArray); $i++) {
//                    if(array_search($allgroupArray[$i], $tempArray) === false){
//                        $tempArray[] = $allgroupArray[$i];
//                    }
//                }
//                sort($tempArray);
//                $tempString = $tempArray[0];
//                for($j = 1; $j < count($tempArray);$j++){
//                    $tempString = $tempString."-".$tempArray[$j];
//                    if(array_search($tempString, $IOS_emma_topic_array) === false){
//                        $IOS_emma_topic_array[] = "'emma-plan".$user['emma_plan_id']."-".$tempString."' in topics";
//                    }
//                }
//            }
//        }
//    }

    $IOSTopicsString = $IOSTopics[0];
    if (count($IOSTopics) > 0) {
        for ($i = 1; ($i < 5 && $i < count($IOSTopics)); $i++) {
            $IOSTopicsString .= ' || ' . $IOSTopics[$i];
        }
    }

  $data['iosTopicString'] = $IOSTopicsString;
  $data['iosTopics'] = $IOSTopics;

  $postIOSData = array(
//        'to' => $IOSTopicsString,
    'condition' => $IOSTopicsString,
    'time_to_live' => 600,
    'priority' => 'high',
//        'content_available' => true,
    'notification' => array(
      'body' => ($isDrill ? '(DRILL) ' : '') . $emergencyTypeName . ' Near: ' . $address,
      'sound' => 'notification.aiff',
      'badge' => '1'
    ),
    'data' => array(
      'GROUP_ALERT' => 'true',
      'event' => ($isDrill ? '(DRILL) ' : '') . $emergencyTypeName . ' Near: ' . $address,
      'emergency_lat' => $latitude,
      'emergency_lng' => $longitude,
      'emergency_address' => $address,
      'emergency_id' => $emergencyId,
      'groups' => array(),
      'site' => $site,
      'description' => $description,
      'comments' => $comments,
      'plan_ID' => $USER->emma_plan_id
    )
  );
  if (!$sound) {
    $postIOSData['notification']['sound'] = 'silent.aiff';
    $data['silent'] = true;
  }

  if ($aed = $aeds->fetch_assoc()) {
    $distanceMiles = $aed['aed_distance'] * 0.621371;
    $postData['data']['details'] = "";
    $postData['data']['aed_id'] = $aed['aed_id'];
    $postData['data']['aed_location_id'] = $aed['location_id'];
    $postData['data']['aed_serial'] = $aed['serial'];
    $postData['data']['aed_contact'] = $aed['contact'];
    $postData['data']['aed_contact_phone'] = $aed['contact_phone'];
    $postData['data']['aed_model'] = $aed['model'];
    $postData['data']['aed_mobile_image'] = $aed['mobile_image'];
    $postData['data']['aed_brand'] = $aed['brand'];
    $postData['data']['aed_placement'] = $aed['placement'];
    $postData['data']['aed_location'] = $aed['location'];
    $postData['data']['aed_organization'] = $aed['organization'];
    $postData['data']['aed_lat'] = $aed['latitude'];
    $postData['data']['aed_lng'] = $aed['longitude'];
    $postData['data']['aed_type'] = $aed['type'];
    $postData['data']['aed_distance'] = $aed['distance'];

    $postIOSData['data']['details'] = "";
    $postIOSData['data']['aed_id'] = $aed['aed_id'];
    $postIOSData['data']['aed_location_id'] = $aed['location_id'];
    $postIOSData['data']['aed_serial'] = $aed['serial'];
    $postIOSData['data']['aed_contact'] = $aed['contact'];
    $postIOSData['data']['aed_contact_phone'] = $aed['contact_phone'];
    $postIOSData['data']['aed_model'] = $aed['model'];
    $postIOSData['data']['aed_mobile_image'] = $aed['mobile_image'];
    $postIOSData['data']['aed_brand'] = $aed['brand'];
    $postIOSData['data']['aed_placement'] = $aed['placement'];
    $postIOSData['data']['aed_location'] = $aed['location'];
    $postIOSData['data']['aed_organization'] = $aed['organization'];
    $postIOSData['data']['aed_lat'] = $aed['latitude'];
    $postIOSData['data']['aed_lng'] = $aed['longitude'];
    $postIOSData['data']['aed_type'] = $aed['type'];
    $postIOSData['data']['aed_distance'] = $aed['distance'];

  } else {
    $nearestAedLat = 'none';
    $nearestAedLng = 'none';
  }

  $jsonData = json_encode($postData);

  $ch = curl_init('https://fcm.googleapis.com/fcm/send');
  curl_setopt_array($ch, array(
    CURLOPT_POST => TRUE,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_HTTPHEADER => array(
      'Authorization: key=' . $FcmServerKey,
      'Content-Type: application/json'
    ),
    CURLOPT_POSTFIELDS => json_encode($postIOSData)
  ));
  $IOSresponse = curl_exec($ch);
  $data['IOSresponse'] = json_decode($IOSresponse);

//  if (count($IOSTopics) > 5) {
//    for ($i = 5; ($i < 10 && $i < count($IOSTopics)); $i++) {
//      if ($i == 5) {
//        $IOSTopicsString = $IOSTopics[$i];
//      }
//      $IOSTopicsString .= ' || ' . $IOSTopics[$i];
//    }
//
//    $postIOSData['condition'] = $IOSTopicsString;
//
//    $ch = curl_init('https://fcm.googleapis.com/fcm/send');
//    curl_setopt_array($ch, array(
//      CURLOPT_POST => TRUE,
//      CURLOPT_RETURNTRANSFER => TRUE,
//      CURLOPT_HTTPHEADER => array(
//        'Authorization: key=' . $FcmServerKey,
//        'Content-Type: application/json'
//      ),
//      CURLOPT_POSTFIELDS => json_encode($postIOSData)
//    ));
//    $IOSresponse2 = curl_exec($ch);
//    $data['IOSresponse' . $i] = json_decode($IOSresponse2);
//
//  }

    if (count($IOSTopics) > 5) {
        $m = 5;
        while ($m < count($IOSTopics)) {
            $data['m'][] = $m;
            for ($i = $m; ($i < ($m + 5) && $i < count($IOSTopics)); $i++) {
                if ($i == $m) {
                    $IOSTopicsString = $IOSTopics[$i];
                }
                $IOSTopicsString .= ' || ' . $IOSTopics[$i];
            }

            $postIOSData['condition'] = $IOSTopicsString;

            $ch = curl_init('https://fcm.googleapis.com/fcm/send');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: key=' . $FcmServerKey,
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postIOSData)
            ));
//            sleep(1);
            $IOSresponse2 = curl_exec($ch);

//            $IOSresponse2Data = json_decode($IOSresponse2);
//            $number = $m;
//            $data['IOSresponse'. $number] = $IOSresponse2Data;
//            $data['iosTopics'][] = $IOSTopicsString;

            $m += 5;
        }
    }

    //browser notifications
    sort($postData['data']['groups']);
    $deviceGroupString = implode("-", $postData['data']['groups']);
    $deviceGroupString = 'emma-web-plan'.$USER->emma_plan_id.'-'.$deviceGroupString;
    $data['deviceGroupString'] = $deviceGroupString;

    $deviceKeyID = '';

    $deviceGroupQuery = select_emmaNotificationDeviceGroups_with_keyName($deviceGroupString);
//    $fvmdb->query("
//        SELECT g.notification_key_name AS nkeyName, g.notification_key AS nkey, g.last_updated
//        FROM emma_notification_device_groups AS g
//        WHERE g.notification_key_name = '". $deviceGroupString ."'
//    ");
    if($deviceGroupQuery->num_rows > 0){
        //add new users to device group
        $deviceGroupResult = $deviceGroupQuery->fetch_assoc();
        $deviceKeyID = $deviceGroupResult['nkey'];

        $data['deviceGroupResult']['nkeyname'] = $deviceGroupResult['nkeyName'];
        $data['deviceGroupResult']['nkey'] = $deviceGroupResult['nkey'];
        $data['deviceGroupResult']['last_updated'] = $deviceGroupResult['last_updated'];


        //find if there are new users to add
        $groupString = implode("','", $postData['data']['groups']);
        $webUsersQuery = select_notificationClients_with_planID_groupString_and_createdDate($USER->emma_plan_id, $groupString, $deviceGroupResult['last_updated']);
//        $fvmdb->query("
//            Select DISTINCT c.client_id
//            FROM emma_notification_clients AS c
//            JOIN users AS u ON c.user_id = u.id AND u.emma_plan_id = '" . $user['emma_plan_id'] . "'
//            JOIN emma_user_groups AS g ON u.id = g.user_id
//            WHERE g.emma_group_id IN ('" . $groupString . "')
//            AND c.created_date > '".$deviceGroupResult['last_updated']."'
//        ");
        if($webUsersQuery->num_rows > 0){
            //put new users in array
            $payload = array();
            $payload['registration_ids'] = array();
            while($webUsersResult = $webUsersQuery->fetch_assoc()){
                $payload['registration_ids'][] = $webUsersResult['client_id'];
            }
            //add users to new device group
            $payload['operation'] = 'add';
            $payload['notification_key_name'] = $deviceGroupString;
            $payload['notification_key'] = $deviceGroupResult['nkey'];


            //send request to add users
            $ch = curl_init('https://fcm.googleapis.com/fcm/notification');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: key=' . $FcmServerKey,
                    'Content-Type: application/json',
                    'project_id: 742343872016'
                ),
                CURLOPT_POSTFIELDS => json_encode($payload)
            ));
            $addToDeviceGroupResult = curl_exec($ch);
            $addToDeviceGroupResultData = json_decode($addToDeviceGroupResult);
            $data['add_to_device_group'] = $addToDeviceGroupResultData;
        }
    }
    else {
        //[Create Device Group]
        //get users for device group
        $emptyWeb = false;
        $groupString = implode("','", $postData['data']['groups']);
        $webUsersQuery = select_notificationClients_with_planID_groupString($USER->emma_plan_id, $groupString);
//        $fvmdb->query("
//            Select DISTINCT c.client_id
//            FROM emma_notification_clients AS c
//            JOIN users AS u ON c.user_id = u.id AND u.emma_plan_id = '" . $user['emma_plan_id'] . "'
//            JOIN emma_user_groups AS g ON u.id = g.user_id
//            WHERE g.emma_group_id IN ('" . $groupString . "')
//        ");
        if($webUsersQuery->num_rows > 0){
            //put users in array
            $payload = array();
            $payload['registration_ids'] = array();
            while($webUsersResult = $webUsersQuery->fetch_assoc()){
                $payload['registration_ids'][] = $webUsersResult['client_id'];
            }
            //other payload info
            $payload['operation'] = 'create';
            $payload['notification_key_name'] = $deviceGroupString;


            //send request
            $ch = curl_init('https://fcm.googleapis.com/fcm/notification');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: key=' . $FcmServerKey,
                    'Content-Type: application/json',
                    'project_id: 742343872016'
                ),
                CURLOPT_POSTFIELDS => json_encode($payload)
            ));
            $createDeviceGroupResult = curl_exec($ch);
            $createDeviceGroupResponseData = json_decode($createDeviceGroupResult);
            $data['create_device_group'] = $createDeviceGroupResponseData;
            $deviceKeyID = $createDeviceGroupResponseData->{"notification_key"};
            $data['deviceKeyTestCheck'] = $deviceKeyID;
            $data['webResponseRaw'] = $createDeviceGroupResult;

            $insert = insert_emma_notification_device_groups($deviceGroupString, $deviceKeyID);
//            $fvmdb->query("
//                INSERT INTO emma_notification_device_groups (notification_key_name, notification_key, last_updated)
//                VALUES ('".$deviceGroupString."', '".$deviceKeyID."', NOW())
//            ");
//            $errors['test'] = 'test';
        }
        else{
            $emptyWeb = true;
        }

    }

    if(empty($errors) && $emptyWeb == false) {

        if (empty($icon_name)) {
            $icon_name = 'emma_icon.png';
        }

        $postWebData = array(
            'to' => $deviceKeyID,
            'time_to_live' => 600,
            "message" => array(
                'webpush' => array(
                    'headers' => array(
                        "Urgency" => 'high'
                    ),
                    'fcm_options' => array(
                        'link' => "https://www.emmaadmin.com"
                    )
                ),
                'notification' => array(
                    'body' => 'Near: ' . $address,
                    'title' => $emergencyTypeName,
                    'sound' => 'notification.aiff',
                    'requireInteraction' => "true",
                    'click_action' => "https://www.emmaadmin.com"
                )
            ),
            'notification' => array(
                'body' => 'Near: ' . $address,
                'title' => $emergencyTypeName,
                'sound' => 'notification.aiff',
                'requireInteraction' => "true",
                'click_action' => "https://www.emmaadmin.com"
            ),
            'data' => array(
                'title' => $emergencyTypeName,
                'body' => ' Near: ' . $address,
                'emergency_id' => $emergencyId,
                'emergency_icon' => $icon_name
            )
        );

        $data['postwebdata'] = $postWebData;


        $ch = curl_init('https://fcm.googleapis.com/fcm/send');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Authorization: key=' . $FcmServerKey,
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postWebData)
        ));
        $webResponse = curl_exec($ch);
        $data['webresponse'] = $webResponse;
    }
//  $payload = array(
//    "registration_ids" => array()
//  );
//  $notificationClients = $fvmdb->query("
//    SELECT *
//    FROM emma_notification_clients
//  ");
//  while ($notificationClient = $notificationClients->fetch_assoc()) {
//    $payload['registration_ids'][] = $notificationClient['client_id'];
//  }
//  $payload_string = json_encode($payload);
//  $ch = curl_init();
//  curl_setopt($ch, CURLOPT_URL, "https://android.googleapis.com/gcm/send");
//  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
//  curl_setopt($ch, CURLOPT_POSTFIELDS, $payload_string);
//  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//    'Authorization: key=AAAAmOlRT0g:APA91bGnjPDlCV7X5dFmif80eEtpB2fgK9_1QxTvaDxxJO-1jHmqtX_oo35d1qjSzr9Cj4JQ5vd5tyDAvzoWrvHnOjZPANlgl8rkE_lBuDvHWTVkBuj5hfUKXIvvk7DXbNokPdAPcrzq',
//    'Content-Type: application/json'
//  ));
//  $data['notification-resposne'] = json_decode(curl_exec($ch));
//  $ch = curl_init();
//  curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
//  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
//  curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
//  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//    'Authorization: key=AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db',
//    'Content-Type: application/json'
//  ));

   $ch = curl_init('https://fcm.googleapis.com/fcm/send');
   curl_setopt_array($ch, array(
     CURLOPT_POST => TRUE,
     CURLOPT_RETURNTRANSFER => TRUE,
     CURLOPT_HTTPHEADER => array(
       'Authorization: key=' . $FcmServerKey,
       'Content-Type: application/json'
     ),
     CURLOPT_POSTFIELDS => json_encode($postData)
   ));
  $res = curl_exec($ch);
  if (!$res) {
    $errors['curl'] = curl_error($ch);
    $errors['curlno'] = curl_errno($ch);
  }

  $data['response'] = json_decode($res);

  // $payload = array(
  //   "registration_ids" => array()
  // );
  // $notificationClients = $fvmdb->query("
  //   SELECT *
  //   FROM emma_notification_clients
  // ");
  // while ($notificationClient = $notificationClients->fetch_assoc()) {
  //   $payload['registration_ids'][] = $notificationClient['client_id'];
  // }
  // $payload_string = json_encode($payload);
  // $ch = curl_init();
  // curl_setopt($ch, CURLOPT_URL, "https://android.googleapis.com/gcm/send");
  // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  // curl_setopt($ch, CURLOPT_POSTFIELDS, $payload_string);
  // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  // curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  //   'Authorization: key=AAAAmOlRT0g:APA91bGnjPDlCV7X5dFmif80eEtpB2fgK9_1QxTvaDxxJO-1jHmqtX_oo35d1qjSzr9Cj4JQ5vd5tyDAvzoWrvHnOjZPANlgl8rkE_lBuDvHWTVkBuj5hfUKXIvvk7DXbNokPdAPcrzq',
  //   'Content-Type: application/json'
  // ));
  // $data['notification-resposne'] = json_decode(curl_exec($ch));

    /*
    * Send Emails
    */
    $can_send_email_alerts = false;
    if($alertEmail == 'on'){
        $can_send_email_alerts = true;
    }
    $data['CanEmail'] = $can_send_email_alerts;
    $data['EmailGroupList'] = $groups_list;
    if($can_send_email_alerts){



        $email_recipient_query = select_distinctUser_with_groupString($groups_list);
//        $fvmdb->query("
//            SELECT DISTINCT u.username
//            FROM users AS u
//            JOIN emma_user_groups AS ug ON u.id = ug.user_id AND ug.emma_group_id in ('".$groups_list."')
//            WHERE u.active = 1 AND u.display = 'yes'
//        ");

        if($email_recipient_query->num_rows > 0){
            while($recipient = $email_recipient_query->fetch_assoc()){
                $to = $recipient['username'];
                $data['EmailToList'][] = $to;
                $subject = 'EMMA Alert';

                $body = '<html><head></head><body style="color:#000001;">
                           
                        <h2>'.$emergencyTypeName.'</h2>
                        <hr>
                        <div>Site: '.$siteName.'</div>
                        <div>Event Details: '.$description.'</div>
                        <div>Comments: '.$comments.'</div>
						</body></html>';



                try{
                    // Create the Mailer using your created Transport
                    $mailer = Swift_Mailer::newInstance($SMT);
                    $message = Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom(array(FROMEMAIL => PROGRAM))
                        ->setTo(array($to))
                        ->setBody($body, 'text/html');
                    $result = $mailer->send($message);
                    if(!$result){
                        error_log('Failed to sent email to: '. $to);
                    }
                    $mailer->getTransport()->stop();

                }
                catch(Swift_TransportException $e){
                    error_log('Swiftmailer transport exception thrown on email of create_event.php, sending message to: '.$to);
                    error_log($e->getMessage());
                }
                catch(Swift_IoException $e){
                    error_log('Swiftmailer IO exception thrown on email of create_event.php, sending message to: '.$to);
                    error_log($e->getMessage());
                }
                catch(Exception $e){
                    error_log('Exception thrown on email of create_event.php, sending message to: '.$to);
                    error_log($e->getMessage());
                }
            }
        }
    }
}

$data['planID'] = $USER->emma_plan_id;
$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);