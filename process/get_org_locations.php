<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/6/2019
 * Time: 3:06 PM
 */

global $fvmdb;
include('../include/db.php');
include('../include/processing.php');
$orgId = $fvmdb->real_escape_string($_GET['org-id']);

$data = array();
$errors = array();

if(empty($orgId)) {
  $errors['org-id'] = 'No Organization ID';
}

if(empty($errors))
{
  $locations = $fvmdb->query("
    SELECT l.id, l.name
    FROM locations l
    WHERE l.orgid = ".$orgId."
  ");
  while($location = $locations->fetch_assoc())
  {
    $data['locations'][] = $location ;
  }
}
$data['success'] = empty($errors);
$data['errors'] = $errors;
echo json_encode($data);