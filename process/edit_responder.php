<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 5/29/2019
 * Time: 3:30 PM
 */

global $fvmdb;
include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$id = $fvmdb->real_escape_string($_POST['id']);
$firstName = $fvmdb->real_escape_string($_POST['first-name']);
$lastName = $fvmdb->real_escape_string($_POST['last-name']);
$email = $fvmdb->real_escape_string($_POST['email']);
$phone = $fvmdb->real_escape_string($_POST['phone']);
$address = $fvmdb->real_escape_string($_POST['address']);
$city = $fvmdb->real_escape_string($_POST['city']);
$state = $fvmdb->real_escape_string($_POST['state']);
$zip = $fvmdb->real_escape_string($_POST['zip']);
$notes = $fvmdb->real_escape_string($_POST['notes']);
$status = $fvmdb->real_escape_string($_POST['status']);

if (empty($id)) $errors['id'] = 'The ID could not be found';

if (empty($errors)) {
  //update_emmaAsset_details_with_assetID($id, $type_id, $latitude, $longitude, $address, $status);
  $fvmdb->query("
   UPDATE persons as p
   SET
    p.firstname = '" . $firstName . "',
    p.lastname = '" . $lastName . "',
    p.email = '" . $email . "',
    p.phone = '" . $phone . "',
    p.address = '" . $address . "',
    p.city = '" . $city . "',
    p.state = '" . $state . "',
    p.zip = '" . $zip . "',
    p.notes = '" . $notes . "',
    p.display = '" . $status . "'
    WHERE p.id = '" . $id . "'
   ");

}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);