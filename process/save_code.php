<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 4/6/2018
 * Time: 1:23 PM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$code = $fvmdb->real_escape_string($_POST['code']);
$planId = $fvmdb->real_escape_string($_POST['plan-id']);
$startDate = $fvmdb->real_escape_string($_POST['start-date']);
$endDate = $fvmdb->real_escape_string($_POST['end-date']);
$groupId = $fvmdb->real_escape_string($_POST['group-id']);
$duration = $fvmdb->real_escape_string($_POST['duration']);

if(empty($startDate))$errors['start-date'] = 'Start date is required';
if(empty($endDate))$errors['end-date'] = 'End date is required';
if(empty($groupId))$errors['group-id'] = 'Group is required';
if(strtotime($startDate) >= strtotime($endDate))$errors['time-span'] = 'End Date must be after Start Date';

if (empty($errors)) {
  $start = date('Y-m-d', strtotime($startDate));
  $end = date('Y-m-d', strtotime($endDate));
  $insertCode = insert_emma_guest_codes($code, $planId, $groupId, $start, $end, $duration);
//      $fvmdb->query("
//    insert into emma_guest_codes (
//      guest_code_id,
//      emma_plan_id,
//      group_id,
//      start_date,
//      end_date,
//      duration_minutes
//    ) values (
//      '" . $code . "',
//      '" . $planId . "',
//      '" . $groupId . "',
//      '" . $start . "',
//      '" . $end . "',
//      '" . $duration . "'
//    )
//  ");
  if (!$insertCode) {
    $errors['sql'] = $fvmdb->error;
  }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);