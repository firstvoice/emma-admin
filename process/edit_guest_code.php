<?php

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$guest_code_id = $fvmdb->real_escape_string($_POST['guest-code-id']);
$groupId = $fvmdb->real_escape_string($_POST['group-id']);
$duration = $fvmdb->real_escape_string($_POST['duration']);
$start = $fvmdb->real_escape_string($_POST['start-date']);
$end = $fvmdb->real_escape_string($_POST['end-date']);

if (empty($errors)) {
    $update = update_guestCode_with_guestCodeID($guest_code_id, $groupId, $duration, $start, $end);
//        $fvmdb->query("
//        UPDATE emma_guest_codes
//        SET group_id = '" . $groupId . "',
//        duration_minutes= '" . $duration. "',
//        start_date = '" . $start . "',
//        end_date = '" . $end . "'
//        WHERE guest_code_id = '" . $guest_code_id . "'
//    ");
    if (!$update) {
        $errors[] = 'update failed';
    }
}

$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);