<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/19/2017
 * Time: 9:40 AM
 */

include('../include/db.php');
include('../include/processing.php');

//require('../vendor/php-jwt-master/src/JWT.php');
//require('../vendor/php-jwt-master/src/BeforeValidException.php');
//require('../vendor/php-jwt-master/src/ExpiredException.php');
//require('../vendor/php-jwt-master/src/SignatureInvalidException.php');
require('../include/process_cookie.php');

$errors = array();
$data = array();

$user_id = $fvmdb->real_escape_string($_POST['id']);
$username = $fvmdb->real_escape_string($_POST['username']);
$usernamecurrent = $fvmdb->real_escape_string($_POST['username-current']);
$editId = $fvmdb->real_escape_string($_POST['edit-id']);

$firstname = $fvmdb->real_escape_string($_POST['firstname']);
$middlename = $fvmdb->real_escape_string($_POST['middlename']);
$lastname = $fvmdb->real_escape_string($_POST['lastname']);

$landline = $fvmdb->real_escape_string($_POST['landline']);
$phone = $fvmdb->real_escape_string($_POST['phone']);
$title = $fvmdb->real_escape_string($_POST['title']);

$address = $fvmdb->real_escape_string($_POST['address']);
$state = $fvmdb->real_escape_string($_POST['state']);
$city = $fvmdb->real_escape_string($_POST['city']);
$zip = $fvmdb->real_escape_string($_POST['zip']);

$groups = $_POST['group-ids'];
$plans = $_POST['plan-ids'];

$opass = $fvmdb->real_escape_string($_POST['old-password']);
$npass = $fvmdb->real_escape_string($_POST['new-password']);
$conpass = $fvmdb->real_escape_string($_POST['con-password']);

$pin = $fvmdb->real_escape_string($_POST['pin']);

$mainplan = $fvmdb->real_escape_string($_POST['main-plan']);


if (empty($user_id)) {
  $errors[] = 'empty id';
}

if (empty($usernamecurrent) && empty($username)) {
  $errors[] = 'empty username';
}else{
    if(empty($username)) {
        $usernamefinal = $usernamecurrent;
    }else{
        $usernamefinal = $username;
    }
    $usernamecheck = select_user_from_username($usernamefinal);
    if($usernamecheck->num_rows > 0){
        $usernameuser = $usernamecheck->fetch_assoc();
        if($usernameuser['id'] != $user_id){
            $errors[] = 'Username already exists';
        }
    }
}


if (empty($firstname) || empty($lastname)) {
  $errors[] = 'empty name';
}

//if (empty($phone)) {
//  $errors[] = 'no phone';
//}

$oldUsers = select_user_from_userID($user_id);
//    $fvmdb->query("
//  select *
//  from users
//  where id = '" . $user_id . "'
//");
if (!$oldUser = $oldUsers->fetch_assoc()) {
  $errors[] = 'could not find user';
}

if($conpass != $npass){
    $errors[] = 'Passwords do not match';
}



if (empty($errors)) {
  $update = update_userDetails_generalDetails($user_id, $usernamefinal, $firstname, $middlename, $lastname, $landline,
      $phone, $title, $address, $city, $state, $zip, $pin, $mainplan);
//      $fvmdb->query("
//    UPDATE users
//    SET firstname = '" . $firstname . "',
//    middlename= '" . $middlename . "',
//    lastname = '" . $lastname . "',
//    phone = '" . $phone . "',
//    title = '" . $title . "',
//    address = '" . $address . "',
//    city = '" . $city . "',
//    province = '" . $state . "',
//    zip = '" . $zip . "',
//    landline_phone = '" . $landline . "'
//    WHERE id = " . $user_id . "
//  ");

  $saveUserHistory = insert_emma_edit_history_users($user_id, $oldUser['firstname'], $oldUser['middlename'], $oldUser['lastname'], $oldUser['landline_phone'],
      $oldUser['phone'], $oldUser['title'], $oldUser['address'], $oldUser['city'], $oldUser['province'], $oldUser['zip'], $editId, date('Y-m-d H:i:s'));
//      $fvmdb->query("
//    INSERT INTO emma_edit_history_users (
//      user_id,
//      firstname,
//      middlename,
//      lastname,
//      landline_phone,
//      phone,
//      title,
//      address,
//      city,
//      province,
//      zip,
//      edit_by_id,
//      edit_date
//    ) VALUES (
//      '" . $user_id . "',
//      '" . $oldUser['firstname'] . "',
//      '" . $oldUser['middlename'] . "',
//      '" . $oldUser['lastname'] . "',
//      '" . $oldUser['landline_phone'] . "',
//      '" . $oldUser['phone'] . "',
//      '" . $oldUser['title'] . "',
//      '" . $oldUser['address'] . "',
//      '" . $oldUser['city'] . "',
//      '" . $oldUser['province'] . "',
//      '" . $oldUser['zip'] . "',
//      '" . $editId . "',
//      '" . date('Y-m-d H:i:s') . "'
//    )
//  ");
  $saveHistoryId = $emmadb->insert_id;
  if(!empty($groups)) {

      $oldGroups = select_groupIDs_with_userID_and_planID($user_id, $USER->emma_plan_id);
//      $fvmdb->query("
//    SELECT *
//    FROM emma_user_groups
//    WHERE user_id = '" . $user_id . "'
//  ");
      while ($oldGroup = $oldGroups->fetch_assoc()) {
          $saveGroupHistory = insert_emma_edit_history_user_groups($saveHistoryId, $oldGroup['emma_group_id']);
//        $fvmdb->query("
//      INSERT INTO emma_edit_history_user_groups (
//        emma_edit_history_user_id,
//        emma_group_id
//      ) VALUES (
//        '" . $saveHistoryId . "',
//        '" . $oldGroup['emma_group_id'] . "'
//      )
//    ");
          if (count($groups) > 0) {
              $removeOldGroups = delete_emma_user_groups($user_id, $oldGroup['emma_group_id']);
//      $fvmdb->query("
//    DELETE FROM emma_user_groups
//    WHERE user_id = '" . $user_id . "'
//  ");
          }
      }

      if (count($plans) > 0) {
          foreach ($groups as $groupId) {
              $groupInfos = select_group_with_groupID($groupId);
              $groupInfo = $groupInfos->fetch_assoc();
              foreach ($plans as $planId) {
                  $plangroups = select_plan_group_with_name($planId, $groupInfo['name']);
                  $plangroup = $plangroups->fetch_assoc();
                  $insertGroup = insert_user_groups($user_id, $plangroup['emma_group_id']);
//        $fvmdb->query("
//      INSERT INTO emma_user_groups (
//        user_id,
//        emma_group_id
//      ) VALUES (
//        '" . $user_id . "',
//        '" . $groupId . "'
//      )
//    ");

                  $data['groups'][] = $groupId;
              }
          }
      } else {
          foreach ($groups as $groupId) {
              $insertGroup = insert_user_groups($user_id, $groupId);
//        $fvmdb->query("
//      INSERT INTO emma_user_groups (
//        user_id,
//        emma_group_id
//      ) VALUES (
//        '" . $user_id . "',
//        '" . $groupId . "'
//      )
//    ");

              $data['groups'][] = $groupId;
          }
      }
      if (!empty($plans)) {

          $deactivatePlans = update_remove_multiplans($user_id);
          foreach ($plans as $planId) {
              $insertPlan = insert_user_plans($user_id, $planId);
    //        $fvmdb->query("
    //      INSERT INTO emma_user_groups (
    //        user_id,
    //        emma_group_id
    //      ) VALUES (
    //        '" . $user_id . "',
    //        '" . $groupId . "'
    //      )
    //    ");
              $data['plans'][] = $planId;
          }
      }
  }

  if (!$update) {
    $errors[] = 'update failed';
  }

    if (!empty($conpass) && !empty($npass)) {

        $specialChar = false;
        $number = false;
        $uppercase = false;
        $lowercase = false;
        $length = false;

        $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/';
        if (preg_match($pattern, $npass)){
            $specialChar = true;
        }
        if(1 === preg_match('~[0-9]~', $npass)){
            $number = true;
        }
        if (preg_match('/[A-Z]/', $npass)) {
            $uppercase = true;
        }
        if (preg_match('/[a-z]/', $npass)) {
            $lowercase = true;
        }
        if (strlen($npass) >= 8) {
            $length = true;
        }

        if(!$specialChar || !$number || !$uppercase || !$lowercase || !$length){
            $errors[] = 'Password requires an uppercase, lowercase, number, a special character, and atleast 8 characters long.';
        }
    }





  if (!empty($npass) && !empty($conpass)  && empty($errors)){
      $users = login_with_user_id($user_id);
//          $fvmdb->query("
//        SELECT u.*
//        FROM users u
//        WHERE u.id = '". $user_id ."'
//        AND u.password = PASSWORD('". $opass ."')
//      ");
      if($user = $users->fetch_assoc()){
          //check for old passwords
          $oldPassDuplicationCheck = select_oldPasswords_with_userID_and_password($user_id, $npass);

          if(!($oldPassDuplication = $oldPassDuplicationCheck->fetch_assoc())) {


              $updatePass = update_users_password_with_userID($user_id, $npass);
//              $fvmdb->query("
//              UPDATE users u
//              SET u.password = PASSWORD('". $npass ."')
//              WHERE u.id = '". $user_id ."'
//             ");
              $unlock = insert_emma_login_log($user['username'], 1, $_SERVER['REMOTE_ADDR'], 'EmmaAdmin-ForgotPassword', 0);


              $insertOldPassword = insert_emma_old_passwords($user_id, $npass);

          }
          else{
              $errors[] = 'Password Update Failed: You have used that password before.';
          }
      }else{
          $errors[] = 'Password Update Failed: Incorrect Password.';
      }
  }


}

$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);