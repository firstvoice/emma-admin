<?php


include('../include/db.php');
include('../include/processing.php');

$start = $fvmdb->real_escape_string($_POST['start']);
$end = $fvmdb->real_escape_string($_POST['end']);


$data = array();

if((strtotime($end) < strtotime('now')) || (strtotime($start) > strtotime('now'))){
    $status = 'Inactive';
    $color = 'red';
}else{
    $status = 'Active';
    $color = 'green';}

$data['color'] = $color;
$data['status'] = $status;

echo json_encode($data);

