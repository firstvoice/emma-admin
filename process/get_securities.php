<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 8/28/2017
 * Time: 10:04 AM
 */

include('../include/db.php');
include('../include/processing.php');

$columns = $_GET['columns'];
$draw = $fvmdb->real_escape_string($_GET['draw']);
$length = $fvmdb->real_escape_string($_GET['length']);
$order = $_GET['order'];
$search = $_GET['search'];
$start = $fvmdb->real_escape_string($_GET['start']);
$active = $fvmdb->real_escape_string($_GET['active']);
$emmaPlanId = $fvmdb->real_escape_string($_GET['emma-plan-id']);

$orderString = $columns[$order[0]['column']]['data'] . ' ' . $order[0]['dir'];

$data = array();

$events = select_getresponces_events($emmaPlanId, $active, $drill, $orderString, $length, $start, $_GET['columns'][0]['search']['value'], $_GET['columns'][1]['search']['value'], $_GET['columns'][2]['search']['value'], $_GET['columns'][3]['search']['value']);
//    $fvmdb->query("
//    SELECT SQL_CALC_FOUND_ROWS e.emma_security_id, et.name AS type, CONCAT(u.firstname, ' ', u.lastname) AS userName, e.active, e.created_date, '0' as call_log, u.id as userid, u.username as useremail
//    FROM emma_securities e
//    JOIN emma_security_types et ON e.emma_security_type_id = et.emma_security_type_id
//    JOIN users u ON e.created_by_id = u.id
//    WHERE (" . ($active != '' ? "e.active = " . $active : "1") . ")
//    AND (" . (isset($drill) && $drill != "" ? "e.drill = '" . $drill . "'" : "1") . ")
//    AND (" . ($emmaPlanId != '' ? "e.emma_plan_id = " . $emmaPlanId : "1") . ")
//    AND (" . ($_GET['columns'][0]['search']['value'] != "" ? "et.name like ('%" . $_GET['columns'][0]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][1]['search']['value'] != "" ? "CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') like ('%" . $_GET['columns'][1]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][2]['search']['value'] != "" ? "(e.active = '0' AND 'Closed' like ('%" . $_GET['columns'][2]['search']['value'] . "%')) OR (e.active = '1' AND 'Active' like ('%" . $_GET['columns'][2]['search']['value'] . "%'))" : "1") . ")
//    AND (" . ($_GET['columns'][3]['search']['value'] != "" ? "e.created_date like ('%" . $_GET['columns'][3]['search']['value'] . "%')" : "1") . ")
//    ORDER BY ". $orderString .", created_date desc
//    LIMIT " . $length . " OFFSET " . $start . "
//");
$found = select_FOUND_ROWS();
//    $fvmdb->query("
//    SELECT FOUND_ROWS()
//");
$count = $found->fetch_assoc();

$data['iTotalRecords'] = $count['FOUND_ROWS()'];
$data['iTotalDisplayRecords'] = $count['FOUND_ROWS()'];
$data['sEcho'] = $draw;
$data['aaData'] = array();
while ($event = $events->fetch_assoc()) {
  $callLogs = select_getresponces_callLogs($event['emma_security_id']);
//      $fvmdb->query("
//    select *
//    from emma_call_reports
//    where security_id = '".$event['emma_security_id']."'
//  ");
  if($callLog = $callLogs->fetch_assoc()) {
    $event['call_log'] = $callLog['id'];
  }
  $data['aaData'][] = $event;
}

echo json_encode($data);