<?php

include('../include/db.php');
include('../include/processing.php');

$columns = $_GET['columns'];
$draw = $fvmdb->real_escape_string($_GET['draw']);
$length = $fvmdb->real_escape_string($_GET['length']);
$order = $_GET['order'];
$search = $_GET['search'];
$start = $fvmdb->real_escape_string($_GET['start']);
$active = $fvmdb->real_escape_string($_GET['active']);
$emmaPlanId = $fvmdb->real_escape_string($_GET['emma-plan-id']);

$orderString = $columns[$order[0]['column']]['data'] . ' ' . $order[0]['dir'];

$data = array();

$geoFences = select_getgeofences_geofences($emmaPlanId,$orderString,$length,$start);
//    $fvmdb->query("
//    SELECT SQL_CALC_FOUND_ROWS gf.*
//    FROM emma_geofence_locations gf
//    WHERE gf.plan_id = '" . $emmaPlanId . "'
//    GROUP BY gf.id
//    ORDER BY ". $orderString ."
//    LIMIT " . $length . " OFFSET " . $start . "
//");
$found = select_FOUND_ROWS();
//    $fvmdb->query("
//    SELECT FOUND_ROWS()
//");
$count = $found->fetch_assoc();


$data['iTotalRecords'] = $count['FOUND_ROWS()'];
$data['iTotalDisplayRecords'] = $count['FOUND_ROWS()'];
$data['sEcho'] = $draw;
$data['aaData'] = array();
while ($geoFence = $geoFences->fetch_assoc()) {
    //combine coordinates
    if($geoFence['nw_corner_lat'] != '' && $geoFence['nw_corner_lng'] != '') {
        $geoFence['nwc'] = $geoFence['nw_corner_lat'] . ', ' . $geoFence['nw_corner_lng'];
    }else{
        $geoFence['nwc'] = 'N/A';
    }
    if($geoFence['se_corner_lat'] != '' && $geoFence['se_corner_lng'] != '') {
        $geoFence['sec'] = $geoFence['se_corner_lat'] . ', ' . $geoFence['se_corner_lng'];
    }else{
        $geoFence['sec'] = 'N/A';
    }
    if($geoFence['center_lat'] != '' && $geoFence['center_lng'] != '') {
        $geoFence['cc'] = $geoFence['center_lat'] . ', ' . $geoFence['center_lng'];
    }else{
        $geoFence['cc'] = 'N/A';
    }
    if($geoFence['radius'] != ''){
        $geoFence['rad'] = $geoFence['radius'] . ' Miles';
    }
    if($geoFence['active'] == 1){
        $geoFence['status'] = '<span style="color: green">Active</span>';
    }else{
        $geoFence['status'] = '<span style="color: red">Inactive</span>';
    }

    $data['aaData'][] = $geoFence;
}

echo json_encode($data);
