<?php

global $fvmdb;
include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$aedId = $fvmdb->real_escape_string($_POST['aed-id']);

if (empty($aedId)) {
  $errors['aed-id'] = 'AED Id not provided';
}

if (empty($errors)) {
  $deactivateAed = $fvmdb->query("
   update aeds
   set display = '0'
   where aed_id = '" . $aedId . "'
 ");
  if (!$deactivateAed) {
    $errors['sql'] = $fvmdb->error;
  }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);