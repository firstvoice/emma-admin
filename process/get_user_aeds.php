<?php
include('../include/db.php');
require('../vendor/php-jwt-master/src/JWT.php');
require('../vendor/php-jwt-master/src/BeforeValidException.php');
require('../vendor/php-jwt-master/src/ExpiredException.php');
require('../vendor/php-jwt-master/src/SignatureInvalidException.php');
include('../include/processing.php');
$CONFIG = json_decode(file_get_contents('../config/config.json'));

$jwt = $fvmdb->real_escape_string($_POST['jwt']);
$latitude = $fvmdb->real_escape_string($_POST['lat']);
$longitude = $fvmdb->real_escape_string($_POST['lng']);

$data = array();
$data['error'] = array();

try {
  //$token = Firebase\JWT\JWT::decode($jwt, $CONFIG->key, array('HS512'));
  //$data['token'] = $token;
  $data['latitude'] = $latitude;
  $data['longitude'] = $longitude;
  $public = $fvmdb->query("
    select a.aed_id, a.latitude, a.longitude,
     a.location as placement, a.notes, a.serialnumber, a.lastcheck, l.name as location_name, o.name as organization_name, l.address, 
     SQRT((".$latitude."-a.latitude)*(".$latitude."-a.latitude)+(".$longitude."-a.longitude)*(".$longitude."-a.longitude)) as distance
    from aeds a
    join locations l on a.location_id = l.id
    join organizations o on l.orgid = o.id
    WHERE (a.latitude IS NOT NULL AND a.longitude IS NOT NULL)
    AND accessibility = 'public'
    Order by distance
    LIMIT 10
  ");

  $private = $fvmdb->query("
     select a.aed_id, a.latitude, a.longitude,
     a.location as placement, a.notes, a.serialnumber, a.lastcheck, l.name as location_name, o.name as organization_name, l.address, 
     SQRT((".$latitude."-a.latitude)*(".$latitude."-a.latitude)+(".$longitude."-a.longitude)*(".$longitude."-a.longitude)) as distance
    from aeds a
    join locations l on a.location_id = l.id
    join organizations o on l.orgid = o.id
    where (a.latitude IS NOT NULL AND a.longitude IS NOT NULL AND a.latitude != 0.0 AND a.longitude != 0.0)
    LIMIT 100
  ");

  $data['aeds'] = array();
  while($aed = $public->fetch_assoc()) {
    $data['aeds'][$aed['aed_id']] = $aed;
  }

  while($aed = $private->fetch_assoc()) {
    $data['aeds'][$aed['aed_id']] = $aed;
  }

} catch (Exception $e) {
  $data['error']['jwt'] = $e->getMessage();
}


echo json_encode($data);
?>

