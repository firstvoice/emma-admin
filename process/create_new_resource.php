<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/8/2017
 * Time: 4:01 PM
 */

include('../include/db.php');
include('../include/processing.php');

$data = array();
$errors = array();

$filename = $fvmdb->real_escape_string($_POST['file_name']);
$fileType = $fvmdb->real_escape_string($_POST['file_type']);
$siteId = $fvmdb->real_escape_string($_POST['site_id']);
$planId = $fvmdb->real_escape_string($_POST['plan_id']);
$userId = $fvmdb->real_escape_string($_POST['user_id']);

if (isset($_FILES['file'])) {

  $allowed = array('gif', 'png', 'jpg', 'pdf', 'jpeg', 'xlsx', 'docx', 'doc', 'xls', 'csv');
  $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
  if (!in_array($ext, $allowed)) {
    $errors[] = 'invalid data type';
    $data['invalid-file-type'] = true;
  }

  if (empty($errors)) {
    $info = pathinfo($_FILES['file']['name']);
    do {
      $newname = uniqid() . '.' . $info['extension'];
      $file_path = 'resource_folder/' . $newname;
      $check_for_duplocate_name = select_emmaResources_with_fileString($file_path);
//          $fvmdb->query("
//        SELECT *
//        FROM emma_resources
//        WHERE file_string = " . $file_path . "
//      ");
    } while ($check_for_duplocate_name->num_rows > 0);

    $destination = 'resource_folder/' . $newname;
    $file_tmp_name = $_FILES['file']['tmp_name'];
    $savefile = move_uploaded_file($_FILES['file']['tmp_name'],
      '../' . $destination);

    if (!$savefile) {
      $errors['save_error'] = error_get_last();
    } else {
      $insert = insert_emma_resources($planId, $siteId, $fileType, $filename, $_FILES['file']['type'], $file_path, $userId, $info['extension']);
//          $fvmdb->query("
//        INSERT INTO emma_resources(
//          emma_plan_id,
//          emma_site_id,
//          emma_resource_type_id,
//          file_name,
//          file_mime_type,
//          file_string,
//          created_by,
//          created_date,
//          file_extension
//        ) VALUES (
//          '" . $planId . "',
//          '" . $siteId . "',
//          '" . $fileType . "',
//          '" . $filename . "',
//          '" . $_FILES['file']['type'] . "',
//          '" . $file_path . "',
//          '" . $userId . "',
//          NOW(),
//          '" . $info['extension'] . "'
//        )
//      ");
      if (!$insert) {
        $errors['insert-error'] = 'Error inserting: ' . $emmadb->error;
      }
    }
  }
}

$data['success'] = empty($errors);
$data['errors'] = $errors;
echo json_encode($data);
