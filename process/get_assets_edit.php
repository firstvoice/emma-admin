<?php
/**
 * Created by PhpStorm.
 * User: Phineas Fuhrmeister
 * Date: 6/3/2019
 * Time: 2:26 PM
 */

include('../include/db.php');
include('../include/processing.php');

$columns = $_GET['columns'];
$draw = $fvmdb->real_escape_string($_GET['draw']);
$length = $fvmdb->real_escape_string($_GET['length']);
$order = $_GET['order'];
$search = $_GET['search'];
$start = $fvmdb->real_escape_string($_GET['start']);
$emmaPlanId = $fvmdb->real_escape_string($_GET['emma-plan-id']);
$active = $fvmdb->real_escape_string($_GET['active']);
$orderString = $columns[$order[0]['column']]['data'] . ' ' . $order[0]['dir'];
$data = array();

$assets = select_getAssetsEdit_assets($emmaPlanId,$orderString,$length,$start,$_GET['columns'][0]['search']['value']);
//    $fvmdb->query("
//    SELECT SQL_CALC_FOUND_ROWS a.*, eac.image
//    FROM emma_asset_types a
//    LEFT JOIN emma_asset_icons eac on a.emma_asset_icon_id = eac.asset_icon_id
//    WHERE a.emma_plan_id = '".$emmaPlanId."'
//    AND a.active = 1
//    AND (" . ($_GET['columns'][0]['search']['value'] != "" ? "a.name like ('%" . $_GET['columns'][0]['search']['value'] . "%')" : "1") . ")
//    ORDER BY ". $orderString .", name desc
//    LIMIT " . $length . " OFFSET " . $start . "
//");
$found = select_FOUND_ROWS();
//    $fvmdb->query("
//    SELECT FOUND_ROWS()
//");
$count = $found->fetch_assoc();

$data['iTotalRecords'] = $count['FOUND_ROWS()'];
$data['iTotalDisplayRecords'] = $count['FOUND_ROWS()'];
$data['sEcho'] = $draw;
$data['aaData'] = array();
while ($asset = $assets->fetch_assoc()) {
    $data['aaData'][] = $asset;
}
echo json_encode($data);