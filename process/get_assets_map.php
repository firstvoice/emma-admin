<?php


include('../include/db.php');
include('../include/processing.php');

$emmaPlanId = $fvmdb->real_escape_string($_GET['emma-plan-id']);

$data = array();
$errors = array();

$assets = select_getAssetsmap_assets($emmaPlanId);
//    $fvmdb->query("
//    SELECT e.*, t.name, i.image
//    FROM emma_assets e
//    JOIN emma_asset_types t ON e.emma_asset_type_id = t.emma_asset_type_id
//    LEFT JOIN emma_asset_icons i ON t.emma_asset_icon_id = i.asset_icon_id
//    JOIN users u ON e.created_by_id = u.id
//    WHERE e.emma_plan_id = '".$emmaPlanId."'
//");
while ($asset = $assets->fetch_assoc()) {
    $data['assets'][] = $asset;
}

$data['success'] = empty($errors);
$data['errors'] = $errors;
echo json_encode($data);