<?php

include('../include/db.php');
include('../include/processing.php');

$group = $fvmdb->real_escape_string($_POST['group-id']);
$type = $fvmdb->real_escape_string($_POST['type']);
$emmaPlanId = $fvmdb->real_escape_string($_POST['plan-id']);
$scriptType = $fvmdb->real_escape_string($_POST['script-type']);

$data = array();
$errors = array();
$data['groups'] = array();

$broadcastScripts = select_getgroupscript_broadcastscripts($emmaPlanId,$scriptType,$type,$group);
//    $fvmdb->query("
//select e.*
//from emma_scripts e
//WHERE e.emma_plan_id = '" . $emmaPlanId . "'
//AND e.emma_script_type_id = '". $scriptType ."'
//AND e.emma_broadcast_type_id = '". $type ."'
//AND e.emma_script_group_id = '". $group ."'
//order by name
//");
while($broadcastScript = $broadcastScripts->fetch_assoc()) {
    $data['groups'][] = $broadcastScript;
}

$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);