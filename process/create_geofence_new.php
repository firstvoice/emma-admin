<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/19/2017
 * Time: 9:40 AM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$user = $fvmdb->real_escape_string($_POST['user-id']);
$plan = $fvmdb->real_escape_string($_POST['plan-id']);
$fence_name = $fvmdb->real_escape_string($_POST['geoname']);
$lat = $_POST['geolat'];
$lng = $_POST['geolng'];
$rad = $_POST['georad'];
$first = '';


if (empty($errors)) {
    for($i=0; $i<count($lat); $i++) {
        if($i == 0) {
            $first = insert_emma_geofence_locations_new_first($plan, $user, $fence_name, $lat[$i], $lng[$i], $rad[$i]);
        }else{
            $insert = insert_emma_geofence_locations_new($first, $plan, $user, $fence_name, $lat[$i], $lng[$i], $rad[$i]);
            if(!$insert){
                $errors['geofence_insert'] = 'Failed to insert Geofence';
            }
        }
    }
}

$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);