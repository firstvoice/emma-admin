<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/14/2019
 * Time: 2:24 PM
 */

include('../include/db.php');
include('../include/processing.php');

$columns = $_GET['columns'];
$draw = $fvmdb->real_escape_string($_GET['draw']);
$length = $fvmdb->real_escape_string($_GET['length']);
$order = $_GET['order'];
$search = $_GET['search'];
$start = $fvmdb->real_escape_string($_GET['start']);
$active = $fvmdb->real_escape_string($_GET['active']);
$emmaPlanId = $fvmdb->real_escape_string($_GET['emma-plan-id']);

$orderString = $columns[$order[0]['column']]['data'] . ' ' . $order[0]['dir'];

$data = array();

$users = select_getusersinactive_users($emmaPlanId, $orderString, $length, $start, $_GET['columns'][0]['search']['value'], $_GET['columns'][1]['search']['value'], $_GET['columns'][2]['search']['value'], $_GET['columns'][3]['search']['value']);
//    $fvmdb->query("
//    SELECT SQL_CALC_FOUND_ROWS u.username, u.id, u.firstname, u.lastname, group_concat(DISTINCT eg.name ORDER BY eg.name) AS `group`
//    FROM users u
//    LEFT JOIN emma_user_groups AS gb ON u.id = gb.user_id
//    LEFT JOIN emma_groups eg ON gb.emma_group_id = eg.emma_group_id
//    LEFT JOIN emma_guest_codes egc on u.temporary_code = egc.guest_code_id
//    LEFT JOIN emma_multi_plan mp on u.id = mp.user_id
//    WHERE u.display = 'yes'
//    AND u.active = 0
//    AND mp.plan_id = " . $emmaPlanId . "
//    AND (" . ($_GET['columns'][0]['search']['value'] != "" ? "u.username like ('%" . $_GET['columns'][0]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][1]['search']['value'] != "" ? "u.firstname like ('%" . $_GET['columns'][1]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][2]['search']['value'] != "" ? "u.lastname like ('%" . $_GET['columns'][2]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][3]['search']['value'] != "" ? "eg.name like ('" . $_GET['columns'][3]['search']['value'] . "%')" : "1") . ")
//    GROUP BY u.id
//    ORDER BY ". $orderString ."
//    LIMIT " . $length . " OFFSET " . $start . "
//");
$found = select_FOUND_ROWS();
//    $fvmdb->query("
//    SELECT FOUND_ROWS()
//");
$count = $found->fetch_assoc();

$data['iTotalRecords'] = $count['FOUND_ROWS()'];
$data['iTotalDisplayRecords'] = $count['FOUND_ROWS()'];
$data['sEcho'] = $draw;
$data['aaData'] = array();
while ($user = $users->fetch_assoc()) {
    $data['aaData'][] = $user;
}

echo json_encode($data);
