<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/31/2017
 * Time: 12:35 PM
 */

require('../include/db.php');
include('../include/processing.php');

$data = array();
$errors = array();

$emergencyId = $fvmdb->real_escape_string($_GET['id']);

if (empty($emergencyId)) $errors['emergency-id'] = 'No event (emergency) id';

$data['responses'] = array();
$data['subevents'] = array();
if (empty($errors)) {
    $responses = select_getresponces_responses($emergencyId);
//        $fvmdb->query("
//        SELECT er.*, CONCAT(u.firstname, ' ', u.lastname) AS user, u.phone, CONCAT(u.firstname, ' ', u.lastname) AS first_last_name, u.username, u.id as user_id, u.emma_pin, er.pin as response_pin, ers.*, CONCAT(v.firstname, ' ', v.lastname) as validation_user
//        FROM emergency_responses er
//        JOIN users u ON er.user_id = u.id
//        JOIN emergency_response_status ers ON er.status = ers.emergency_response_status_id
//        LEFT JOIN users v ON er.validated_id = v.id
//        INNER JOIN (
//           SELECT
//             user_id,
//             MAX(timestamp(created_date)) created_date
//           FROM emergency_responses
//           WHERE emergency_id = '" . $emergencyId . "'
//           GROUP BY user_id
//        ) b ON er.user_id = b.user_id AND er.created_date = b.created_date
//        WHERE u.display = 'yes'
//    ");
    while ($response = $responses->fetch_assoc()) {
        $data['responses'][] = $response;
    }

    $subevents = select_getresponces_subevents($emergencyId);
//        $fvmdb->query("
//        SELECT e.*, et.img_filename
//        FROM emergencies as e
//        LEFT JOIN emergency_types as et on e.emergency_type_id = et.emergency_type_id
//        WHERE parent_emergency_id = '". $emergencyId ."'
//        AND active = '1'
//    ");
    while($subevent = $subevents->fetch_assoc()){
        $data['subevents'][] = $subevent;
    }

}

$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);
