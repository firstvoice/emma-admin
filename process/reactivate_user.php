<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/11/2017
 * Time: 12:11 AM
 */

include('../include/db.php');
include('../include/processing.php');
$user_ids = $emmadb->real_escape_string($_POST['selected_users']);
$data = array();
$errors = array();

if (empty($user_ids)) {
    $errors[] = 'No users selected';
}

if (empty($errors)) {
    /*
    $user_list = implode("','", $user_ids);
    $update = $fvmdb->query("
      DELETE FROM emma_user_groups
      WHERE user_id IN ('" . $user_list . "')
    ");

    if (!$update) {
      $errors[] = 'update failed';
    }
    */
        $userinfos = select_user_from_userID($user_ids);
        $userinfo = $userinfos->fetch_assoc();

        $deactivate = update_user_active_with_userID($user_ids, 1);
        $unlock = insert_emma_login_log($userinfo['username'], 1, $_SERVER['REMOTE_ADDR'], 'EmmaAdmin-ForgotPassword', 0);
//        $fvmdb->query("
//      update users
//      set active = 1
//      where id = '" . $user . "'
//    ");

}

//$data['post'] = $_POST;
//$data['userlist'] = $user_list;

$data['success'] = empty($errors);
$data['errors'] = $errors;
echo json_encode($data);