<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/6/2019
 * Time: 3:06 PM
 */

global $fvmdb;
include('../include/db.php');
include('../include/processing.php');
$brandID = $fvmdb->real_escape_string($_GET['brand-id']);

$data = array();
$errors = array();

if(empty($brandID)) {
  $errors['brand-id'] = 'No Brand ID';
}

if(empty($errors))
{
  $models = $fvmdb->query("
    SELECT am.aed_model_id, am.model
    FROM aed_models am
    WHERE am.aed_brand_id = ".$brandID."
  ");
  while($model = $models->fetch_assoc())
  {
    $data['models'][] = $model ;
  }
}
$data['success'] = empty($errors);
$data['errors'] = $errors;
echo json_encode($data);