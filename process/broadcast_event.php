<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/23/2017
 * Time: 12:16 PM
 */

require('../include/db.php');
include('../include/processing.php');

$data = array();
$errors = array();
$userId = $fvmdb->real_escape_string($_POST['user-id']);
$eventId = $fvmdb->real_escape_string($_POST['event-id']);
$description = $fvmdb->real_escape_string($_POST['description']);
$newComments = $fvmdb->real_escape_string($_POST['comments']);
$alertNotification = $fvmdb->real_escape_string($_POST['alert-notification']);
$alertEmail = $fvmdb->real_escape_string($_POST['alert-email']);
$alertText = $fvmdb->real_escape_string($_POST['alert-text']);
$groups = $_POST['group'];
$plan = '';

$groups_list = implode(',', $groups);
if(empty($groups)){
    $errors['groups'] = 'Groups are required';
}
//needs validation for user/groups/plan/admin status
$plan_check_query = select_planID_from_userID_and_groupList($userId, $groups_list);
//$fvmdb->query("
//    SELECT p.emma_plan_id
//    FROM emma_plans AS p
//    JOIN emma_groups AS g ON p.emma_plan_id = g.emma_plan_id
//    JOIN emma_multi_plan AS m ON p.emma_plan_id = m.plan_id AND m.user_id = '".$userId."' AND m.active = 1
//    WHERE g.emma_group_id in ('".$groups_list."')
//");

if($plan_check_query->num_rows > 0){
    $plan_check_result = $plan_check_query->fetch_assoc();
    $plan = $plan_check_result['emma_plan_id'];
}
//else if($plan_check_query->num_rows > 0){
//
//}
else{
    $errors['invalid'] = 'invalid  plan-group-user';
    $data['post'] = $_POST;
    $data['errors'] = $errors;
    $data['success'] = empty($errors);
    echo json_encode($data);
    exit(1);
}

$updateEvent = update_emergencies_descriptionAndComments_with_emergencyID($eventId, $description, $newComments);
//$fvmdb->query("
//  UPDATE emergencies
//  SET
//    description = '" . $description . "',
//    comments = '" . $newComments . "'
//  WHERE emergency_id = '" . $eventId . "'
//");

$insertSystemMessage = insert_system_messages($eventId, $userId, 'Event Broadcasted', '1', $description, $newComments);
//$fvmdb->query("
//  INSERT INTO emma_system_messages (
//    emergency_id,
//    message,
//    created_by_id,
//    created_date,
//    type,
//    description,
//    comments
//  ) VALUES (
//    '" . $eventId . "',
//    'Event Broadcasted',
//    '" . $userId . "',
//    '" . date('Y-m-d H:i:s') . "',
//    '1',
//    '" . $description . "',
//    '" . $newComments . "'
//  )
//");
$messageId = $emmadb->insert_id;

foreach ($groups as $keyId => $groupId) {
  $insertSystemMessageGroup = insert_emma_system_message_groups($messageId, $groupId);
//      $fvmdb->query("
//    insert into emma_system_message_groups
//    (emma_system_message_id, emma_group_id)
//    values
//    ('" . $messageId . "', '" . $groupId . "')
//  ");
}

$events = select_emergencie_with_eventID($eventId);
//    $fvmdb->query("
//  SELECT *
//  FROM emergencies
//  WHERE emergency_id = '" . $eventId . "'
//");
if ($event = $events->fetch_assoc()) {
  $type = $event['emergency_type_id'];
  $siteId = $event['emma_site_id'];
  $description = $event['description'];
  $comments = $event['comments'];
  $latitude = $event['latitude'];
  $longitude = $event['longitude'];
  $address = $event['nearest_address'];
  $emergencyId = $event['emergency_id'];
} else {
    $errors['event'] = 'Can not find event';
    $type = '';
    $siteId = '';
    $description = '';
    $comments = '';
    $latitude = '';
    $longitude = '';
    $address = '';
    $emergencyId = '';
}

$users = select_user_from_userID($userId);
//$fvmdb->query("
//  SELECT *
//  FROM users
//  WHERE id = '" . $userId . "'
//");
if (!$user = $users->fetch_assoc()) {
    $errors['user'] = 'Can not find user';
}

if((strpos(dirname($_SERVER['PHP_SELF']),'git-') !== false  || strpos(dirname($_SERVER['PHP_SELF']),'/egor') !== false)){
    $topics = '/topics/betaemma-' . $plan;
    $webTopics = '/topics/betaemma-web-'.$plan;
}
else{
    $topics = '/topics/emma-' . $plan;
    $webTopics = '/topics/emma-web-'.$plan;
}


$FcmServerKey = 'AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db';
$phone = 'faZ9b8GrO8k:APA91bHkZl8ccQXogP7kxhsyTHl_eM6DcyOQkU3pQ5HZD5Rl0zoIoqnsByq_9aLVqBwK36H-Uf3IpOu6OnScvUi6RYOPs0G9pckLptGZlAdQ1DLTEvxqWFIFFbbIUvi9l7SHdlCNObKn';
//$topics = '/topics/emma-' . $user['emma_plan_id'];
//$webTopics = '/topics/emma-web-'.$user['emma_plan_id'];
$IOSTopics = array();

if (empty($errors)) {
    $sound = true;
    $vibrate = true;
    $emergencyTypeName = "Emergency";
    $emergencyTypes = select_eventTypes_with_eventTypeID($type);
//    $fvmdb->query("
//        SELECT *
//        FROM emergency_types
//        WHERE emergency_type_id = '" . $type . "'
//    ");
    if ($emergencyType = $emergencyTypes->fetch_assoc()) {
        $emergencyTypeName = $emergencyType['name'];
        $sound = $emergencyType['sound'] == "1";
        $vibrate = $emergencyType['vibrate'] == "1";
        $icon_name = $emergencyType['img_filename'];
    }

    $aeds = select_closestAEDs($latitude, $longitude);
//    $fvmdb->query("
//    SELECT
//      a.aed_id,
//      a.location_id,
//      a.serialnumber as serial,
//      c.name as contact,
//      c.phone as contact_phone,
//      m.model,
//      m.mobile_image,
//      b.brand,
//      a.location as placement,
//      l.name AS location,
//      o.name AS organization,
//      a.latitude,
//      a.longitude,
//      'public' as type,
//      ACOS(SIN(RADIANS(a.latitude)) * SIN(RADIANS('" . $latitude . "')) +
//           COS(RADIANS(a.latitude)) * COS(RADIANS('" . $longitude .
//    "')) * COS(RADIANS(a.longitude) - RADIANS('" . $longitude . "'))) *
//      6380   AS 'distance'
//    from aeds a
//    join locations l on a.location_id = l.id
//    JOIN organizations o ON l.orgid = o.id
//    JOIN aed_models m on a.aed_model_id = m.aed_model_id
//    JOIN aed_brands b on m.aed_brand_id = b.aed_brand_id
//    LEFT JOIN contacts c on a.site_coordinator_contact_id = c.id
//    WHERE l.display = 'yes'
//    AND a.display = '1'
//    AND a.approved = '1'
//    order by distance
//    limit 1
//    ");

      $site = 'unknown';
      $sites = select_site_with_siteID($siteId);
//      $fvmdb->query("
//        SELECT *
//        FROM emma_sites
//        WHERE emma_site_id = '" . $siteId . "'
//      ");
      if ($site = $sites->fetch_assoc()) {
        $site = $site['emma_site_name'];
      }

      $postData = array(
        'to' => $topics,
    //        'condition' => "'alerts' in topics || 'aednotify' in topics || 'heartsafe' in topics || 'testing' in topics'",
        'time_to_live' => 900,
        'content_available' => true,
    //        'priority'=>'high',
    //        'notification' => array(
    //            'title' => 'CPR Emergency: ' . $emergencyLat . ', ' . $emergencyLng,
    //            'sound' => 'notification.aiff',
    //            'badge' => '1'
    //        ),
        'data' => array(
          'GROUP_ALERT' => 'true',
          'event' => $emergencyTypeName . ' Near: ' . $address,
          'sound' => $sound,
          'vibrate' => $vibrate,
          'emergency_lat' => $latitude,
          'emergency_lng' => $longitude,
          'emergency_address' => $address,
          'emergency_id' => $emergencyId,
          'groups' => array(),
          'site' => $site,
          'description' => $description,
          'comments' => $comments,
            'plan_ID' => $plan
        )
      );
      foreach ($groups as $group) {
          insert_emergency_received_groups($emergencyId, $group);
//        $fvmdb->query("
//          INSERT IGNORE INTO emergency_received_groups (emergency_id, emma_group_id)
//          VALUES
//          (
//            '" . $emergencyId . "',
//            '" . $group . "'
//          )
//        ");
        $postData['data']['groups'][] = $group;

          if((strpos(dirname($_SERVER['PHP_SELF']),'git-') !== false  || strpos(dirname($_SERVER['PHP_SELF']),'/egor') !== false)){
              $IOSTopics[] = "'betaemma-plan" . $plan . "-" . $group . "' in topics";
          }
          else{
              $IOSTopics[] = "'emma-plan" . $plan . "-" . $group . "' in topics";
          }
//        $IOSTopics[] = "'emma-plan" . $user['emma_plan_id'] . "-" . $group . "' in topics";
      }

   /*
   * Generate IOS information
   */
//    $allgroupArray = array();
//    $IOS_emma_topic_array = array();
//    $allgroups = $fvmdb->query("
//        SELECT emma_group_id
//        FROM emma_groups
//        WHERE emma_plan_id = '". $user['emma_plan_id'] ."'
//    ");
//    while($allgroup = $allgroups->fetch_assoc()){
//        $allgroupArray[] = $allgroup['emma_group_id'];
//    }
//    sort($allgroupArray);
//    foreach($postData['data']['groups'] AS $IOSTopic){
//        $IOS_emma_topic_array[] = "'emma-plan".$user['emma_plan_id']."-".$IOSTopic."' in topics";;
//        foreach($allgroupArray AS $allgroupid){
//            $tempArray = array();
//            $tempArray[] = $IOSTopic.'';
//            if($IOSTopic != $allgroupid) {
//                $i = array_search($allgroupid, $allgroupArray);
//                for (; $i < count($allgroupArray); $i++) {
//                    if(array_search($allgroupArray[$i], $tempArray) === false){
//                        $tempArray[] = $allgroupArray[$i];
//                    }
//                }
//                sort($tempArray);
//                $tempString = $tempArray[0];
//                for($j = 1; $j < count($tempArray);$j++){
//                    $tempString = $tempString."-".$tempArray[$j];
//                    if(array_search($tempString, $IOS_emma_topic_array) === false){
//                        $IOS_emma_topic_array[] = "'emma-plan".$user['emma_plan_id']."-".$tempString."' in topics";
//                    }
//                }
//            }
//        }
//    }

//    $IOSTopicsString = $IOS_emma_topic_array[0];
//    if (count($IOS_emma_topic_array) > 0) {
//        for ($i = 1; ($i < 5 && $i < count($IOS_emma_topic_array)); $i++) {
//            $IOSTopicsString .= ' || ' . $IOS_emma_topic_array[$i];
//        }
//    }
    $IOSTopicsString = $IOSTopics[0];
    if (count($IOSTopics) > 1) {
        for ($i = 1; ($i < 5 && $i < count($IOSTopics)); $i++) {
            $IOSTopicsString .= ' || ' . $IOSTopics[$i];
        }
    }

//  $data['IOSTopicsArray'] = $IOS_emma_topic_array;
  $data['iosTopics'][] = $IOSTopicsString;

    $postIOSData = array(
    //        'to' => $IOSTopicsString,
        'condition' => $IOSTopicsString,
        'time_to_live' => 900,
        'priority' => 'high',
    //        'content_available' => true,
        'notification' => array(
          'body' => $emergencyTypeName . ' Near: ' . $address,
          'sound' => 'notification.aiff',
          'badge' => '1'
        ),
        'data' => array(
          'GROUP_ALERT' => 'true',
          'event' => $emergencyTypeName . ' Near: ' . $address,
          'emergency_lat' => $latitude,
          'emergency_lng' => $longitude,
          'emergency_address' => $address,
          'emergency_id' => $emergencyId,
          'groups' => array(),
          'site' => $site,
          'description' => $description,
          'comments' => $comments,
          'plan_ID' => $plan
        )
    );
    if(!$sound){
        $postIOSData['notification']['sound'] = 'silent.aiff';
    }

    if ($aed = $aeds->fetch_assoc()) {
        $distanceMiles = $aed['aed_distance'] * 0.621371;
        $postData['data']['details'] = "";
        $postData['data']['aed_id'] = $aed['aed_id'];
        $postData['data']['aed_location_id'] = $aed['location_id'];
        $postData['data']['aed_serial'] = $aed['serial'];
        $postData['data']['aed_contact'] = $aed['contact'];
        $postData['data']['aed_contact_phone'] = $aed['contact_phone'];
        $postData['data']['aed_model'] = $aed['model'];
        $postData['data']['aed_mobile_image'] = $aed['mobile_image'];
        $postData['data']['aed_brand'] = $aed['brand'];
        $postData['data']['aed_placement'] = $aed['placement'];
        $postData['data']['aed_location'] = $aed['location'];
        $postData['data']['aed_organization'] = $aed['organization'];
        $postData['data']['aed_lat'] = $aed['latitude'];
        $postData['data']['aed_lng'] = $aed['longitude'];
        $postData['data']['aed_type'] = $aed['type'];
        $postData['data']['aed_distance'] = $aed['distance'];
    } else {
        $nearestAedLat = 'none';
        $nearestAedLng = 'none';
    }

    $ch = curl_init('https://fcm.googleapis.com/fcm/send');
    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
          'Authorization: key=' . $FcmServerKey,
          'Content-Type: application/json'
    ),
    CURLOPT_POSTFIELDS => json_encode($postData)
    ));
    $response = curl_exec($ch);

  /* A DIFFERENT WAY TO DO THE SAME THING ("PURE" PHP)
  $context = stream_context_create(array(
     'http' => array(
         'method' => 'POST',
         'head' => "Authorization: key = ".$FcmServerKey."\r\n".
             "Content - Type: application / json\r\n",
         'content' => json_encode($postData)
     )
  ));
  $response = file_get_contents('https://fcm.googleapis.com/fcm/send', false, $context);
  */

    $ch = curl_init('https://fcm.googleapis.com/fcm/send');
    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
        'Authorization: key=' . $FcmServerKey,
        'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postIOSData)
    ));
    $IOSresponse = curl_exec($ch);

    if (count($IOSTopics) > 5) {
        $m = 5;
        while ($m < count($IOSTopics)) {
            $data['m'][] = $m;
            for ($i = $m; ($i < ($m + 5) && $i < count($IOSTopics)); $i++) {
                if ($i == $m) {
                  $IOSTopicsString = $IOSTopics[$i];
                }
                else {
                    $IOSTopicsString .= ' || ' . $IOSTopics[$i];
                }
            }

            $postIOSData['condition'] = $IOSTopicsString;

            $ch = curl_init('https://fcm.googleapis.com/fcm/send');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                  'Authorization: key=' . $FcmServerKey,
                  'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postIOSData)
            ));
//            sleep(1);
            $IOSresponse2 = curl_exec($ch);

            $IOSresponse2Data = json_decode($IOSresponse2);
            $number = $m;
            $data['IOSresponse'. $number] = $IOSresponse2Data;
            $data['iosTopics'][] = $IOSTopicsString;

            $m += 5;
        }
    }


    //browser notifications
    sort($postData['data']['groups']);
    $deviceGroupString = implode("-", $postData['data']['groups']);
    $deviceGroupString = 'emma-web-plan'.$plan.'-'.$deviceGroupString;
    $data['deviceGroupString'] = $deviceGroupString;

    $deviceKeyID = '';

    $deviceGroupQuery = select_emmaNotificationDeviceGroups_with_keyName($deviceGroupString);
//    $fvmdb->query("
//        SELECT g.notification_key_name AS nkeyName, g.notification_key AS nkey, g.last_updated
//        FROM emma_notification_device_groups AS g
//        WHERE g.notification_key_name = '". $deviceGroupString ."'
//    ");
    if($deviceGroupQuery->num_rows > 0){
        //add new users to device group
        $deviceGroupResult = $deviceGroupQuery->fetch_assoc();
        $deviceKeyID = $deviceGroupResult['nkey'];

//        $data['deviceGroupResult']['nkeyname'] = $deviceGroupResult['nkeyName'];
//        $data['deviceGroupResult']['nkey'] = $deviceGroupResult['nkey'];
//        $data['deviceGroupResult']['last_updated'] = $deviceGroupResult['last_updated'];


        //find if there are new users to add
        $groupString = implode("','", $postData['data']['groups']);
        $webUsersQuery = select_notificationClients_with_planID_groupString_and_createdDate($plan, $groupString, $deviceGroupResult['last_updated']);
//        $fvmdb->query("
//            Select DISTINCT c.client_id
//            FROM emma_notification_clients AS c
//            JOIN users AS u ON c.user_id = u.id AND u.emma_plan_id = '" . $user['emma_plan_id'] . "'
//            JOIN emma_user_groups AS g ON u.id = g.user_id
//            WHERE g.emma_group_id IN ('" . $groupString . "')
//            AND c.created_date > '".$deviceGroupResult['last_updated']."'
//        ");
        if($webUsersQuery->num_rows > 0){
            //put new users in array
            $payload = array();
            $payload['registration_ids'] = array();
            while($webUsersResult = $webUsersQuery->fetch_assoc()){
                $payload['registration_ids'][] = $webUsersResult['client_id'];
            }
            //add users to new device group
            $payload['operation'] = 'add';
            $payload['notification_key_name'] = $deviceGroupString;
            $payload['notification_key'] = $deviceGroupResult['nkey'];


            //send request to add users
            $ch = curl_init('https://fcm.googleapis.com/fcm/notification');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: key=' . $FcmServerKey,
                    'Content-Type: application/json',
                    'project_id: 742343872016'
                ),
                CURLOPT_POSTFIELDS => json_encode($payload)
            ));
            $addToDeviceGroupResult = curl_exec($ch);
//            $addToDeviceGroupResultData = json_decode($addToDeviceGroupResult);
//            $data['add_to_device_group'] = $addToDeviceGroupResultData;
        }
    }
    else {
        //[Create Device Group]
        //get users for device group
        $emptyWeb = false;
        $groupString = implode("','", $postData['data']['groups']);
        $data['webGroups'] = $groupString;
        $webUsersQuery = select_notificationClients_with_planID_groupString($plan, $groupString);
//        $fvmdb->query("
//            Select DISTINCT c.client_id
//            FROM emma_notification_clients AS c
//            JOIN users AS u ON c.user_id = u.id AND u.emma_plan_id = '" . $user['emma_plan_id'] . "'
//            JOIN emma_user_groups AS g ON u.id = g.user_id
//            WHERE g.emma_group_id IN ('" . $groupString . "')
//        ");
        if($webUsersQuery->num_rows > 0){
            //put users in array
            $payload = array();
            $payload['registration_ids'] = array();
            while($webUsersResult = $webUsersQuery->fetch_assoc()){
                $payload['registration_ids'][] = $webUsersResult['client_id'];
            }
            //other payload info
            $payload['operation'] = 'create';
            $payload['notification_key_name'] = $deviceGroupString;


            //send request
            $ch = curl_init('https://fcm.googleapis.com/fcm/notification');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: key=' . $FcmServerKey,
                    'Content-Type: application/json',
                    'project_id: 742343872016'
                ),
                CURLOPT_POSTFIELDS => json_encode($payload)
            ));
            $createDeviceGroupResult = curl_exec($ch);
            $createDeviceGroupResponseData = json_decode($createDeviceGroupResult);
//            $data['create_device_group'] = $createDeviceGroupResponseData;
            $deviceKeyID = $createDeviceGroupResponseData->{"notification_key"};
//            $data['deviceKeyTestCheck'] = $deviceKeyID;
//            $data['webResponseRaw'] = $createDeviceGroupResult;

            $insert = insert_emma_notification_device_groups($deviceGroupString, $deviceKeyID);
//            $fvmdb->query("
//                INSERT INTO emma_notification_device_groups (notification_key_name, notification_key, last_updated)
//                VALUES ('".$deviceGroupString."', '".$deviceKeyID."', NOW())
//            ");
//            $errors['test'] = 'test';
        }
        else{
            $emptyWeb = true;
        }

    }

    if(empty($errors) && $emptyWeb == false) {

        if(empty($icon_name)){
            $icon_name = 'emma_icon.png';
        }

        $postWebData = array(
            'to' => $deviceKeyID,
            'time_to_live' => 600,
            "message" => array(
                'webpush' => array(
                    'headers' => array(
                        "Urgency" => 'high'
                    ),
                    'fcm_options' => array(
                        'link' => "https://www.emmaadmin.com"
                    )
                ),
                'notification' => array(
                    'body' => 'Near: ' . $address,
                    'title' => $emergencyTypeName,
                    'sound' => 'notification.aiff',
                    'requireInteraction' => "true",
                    'click_action' => "https://www.emmaadmin.com"
                )
            ),
            'notification' => array(
                'body' => 'Near: ' . $address,
                'title' => $emergencyTypeName,
                'sound' => 'notification.aiff',
                'requireInteraction' => "true",
                'click_action' => "https://www.emmaadmin.com"
            ),
            'data' => array(
                'title' => $emergencyTypeName,
                'body' => ' Near: '.$address,
                'emergency_id' => $emergencyId,
                'emergency_icon' => $icon_name
            )
        );




        $ch = curl_init('https://fcm.googleapis.com/fcm/send');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Authorization: key=' . $FcmServerKey,
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postWebData)
        ));
        $webResponse = curl_exec($ch);


//$data['iosTopicCount'] = count($IOS_emma_topic_array);
//    $data ['IOSTopics'] = $IOS_emma_topic_array;


        /* A DIFFERENT WAY TO DO THE SAME THING ("PURE" PHP)
        $context = stream_context_create(array(
           'http' => array(
               'method' => 'POST',
               'head' => "Authorization: key = ".$FcmServerKey."\r\n".
                   "Content - Type: application / json\r\n",
               'content' => json_encode($postData)
           )
        ));
        $response = file_get_contents('https://fcm.googleapis.com/fcm/send', false, $context);
        */


        $responseData = json_decode($response);
        $IOSresponseData = json_decode($IOSresponse);
        $webResponseData = json_decode($webResponse);

        $data['response'] = $responseData;
        $data['IOSresponse'] = $IOSresponseData;
        $data['webResponse'] = $webResponseData;
    }



    /*
    * Send Emails
    */
    $can_send_email_alerts = false;
    if($alertEmail){
        $valid_email_alerts = select_plan_from_planID($plan);
//        $fvmdb->query("
//            SELECT emma_plan_id, emails
//            FROM emma_plans
//            WHERE emma_plan_id = '".$plan."'
//        ");
        if($valid_email_alerts->num_rows > 0){
            $email_alerts_results = $valid_email_alerts->fetch_assoc();
            if($email_alerts_results['emails']){
                $can_send_email_alerts = true;
            }
        }
    }
    $data['CanEmail'] = $can_send_email_alerts;
    $data['EmailGroupList'] = $groups_list;
    if($can_send_email_alerts){
        $email_recipient_query = select_distinctUser_with_groupString($groups_list);
//        $fvmdb->query("
//            SELECT DISTINCT u.username
//            FROM users AS u
//            JOIN emma_user_groups AS ug ON u.id = ug.user_id AND ug.emma_group_id in ('".$groups_list."')
//            WHERE u.active = 1 AND u.display = 'yes'
//        ");

        if($email_recipient_query->num_rows > 0){
            while($recipient = $email_recipient_query->fetch_assoc()){
                $to = $recipient['username'];
                $data['EmailToList'][] = $to;
                $subject = 'EMMA Alert';

                $body = '<html><head></head><body style="color:#000001;">
                           
                        <h2>'.$emergencyTypeName.'</h2>
                        <hr>
                        <div>Site: '.$event['emma_site_id'].'</div>
                        <div>Description: '.$event['description'].'</div>
                        <div>Comments: '.$event['comments'].'</div>
						</body></html>';

                $body = '<html><head></head><body style="color:#000001;">
								<div style="margin:0 auto;width:970px;background-color:' . EMAILBACKGROUND . ';color:#000001;padding:10px 0">
									<div style="width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid ' . EMAILFRAME . ';margin: 5px auto;">
										<h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
											<img alt="' . PROGRAM . '" src="' . URL . EMMALOGO . '">
										</h1>
										<div style="width:100%;height:30px;margin-bottom:5px;background-color:' . EMAILFRAME . ';float:left; margin-top:5px;"></div>
										<h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;">
                    ' . $messageformat->nag_planheading() . '
										</h2>
										<br />
										<div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
                    ' . $daysleft . '
										</div>
									</div>
								</div>
								<div style="width:970px;margin:0 auto;color:#000001;background-color:white;">
                    ' . $messageformat->mes_pleasedonotreply() . '
								</div>
								<br />
								<div style="width:970px;margin:0 auto;color:#000001;">
                    ' . $messageformat->mes_tscopyright() . '
								</div>
							</body></html>';



                try{
                    // Create the Mailer using your created Transport
                    $mailer = Swift_Mailer::newInstance($SMT);
                    $message = Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom(array(FROMEMAIL => PROGRAM))
                        ->setTo(array($to))
                        ->setBody($body, 'text/html');
                    $result = $mailer->send($message);
                    if(!$result){
                        error_log('Failed to sent email to: '. $to);
                    }
                    $mailer->getTransport()->stop();

                }
                catch(Swift_TransportException $e){
                    error_log('Swiftmailer transport exception thrown on email of broadcast_event.php, sending message to: '.$to);
                    error_log($e->getMessage());
                }
                catch(Swift_IoException $e){
                    error_log('Swiftmailer IO exception thrown on email of broadcast_event.php, sending message to: '.$to);
                    error_log($e->getMessage());
                }
                catch(Exception $e){
                    error_log('Exception thrown on email of broadcast_event.php, sending message to: '.$to);
                    error_log($e->getMessage());
                }
            }
        }
    }
}
$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);