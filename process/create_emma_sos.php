<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/12/2019
 * Time: 2:36 PM
 */


require('../include/db.php');
include('../include/processing.php');

require('../vendor/php-jwt-master/src/JWT.php');
require('../vendor/php-jwt-master/src/BeforeValidException.php');
require('../vendor/php-jwt-master/src/ExpiredException.php');
require('../vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('../config/config.json'));

$USER = null;

$token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key, array('HS512'));

$USER = $token->data;
$data = array();
$errors = array();
$userId = $USER->id;
$planId = $USER->emma_plan_id;
$lat = $fvmdb->real_escape_string($_POST['latitude']);
$lng = $fvmdb->real_escape_string($_POST['longitude']);

if(empty($userId) || empty($planId))
{
    $errors['User Error'] = 'Could not gather user Information from server';
}
if(empty($lat) || empty($lng))
{
    $errors['Coordinates Error'] = 'Could not gather latitude or longitude information from browser';
}

if (empty($errors)) {
    insert_emma_sos($userId, $lat, $lng, $USER->emma_plan_id);
//    $fvmdb->query("
//    INSERT INTO emma_sos
//    (
//    created_by_id,
//    created_date,
//    sos_date,
//    help_date,
//    help_lat,
//    help_lng,
//    pending_date,
//    pending_lat,
//    pending_lng,
//    cancelled_date,
//    cancelled_lat,
//    cancelled_lng,
//    closed_date,
//    closed_comments,
//    closed_lat,
//    closed_lng,
//    closed_by_id,
//    plan_id,
//    system_pushed)
//    VALUES(
//    '".$userId."',
//    '".date("Y-m-d h:i:sa")."',
//    null,
//    null,
//    null,
//    null,
//    '".date("Y-m-d h:i:sa")."',
//    '".$lat."',
//    '".$lng."',
//    null,
//    null,
//    null,
//    null,
//    null,
//    null,
//    null,
//    null,
//    '".$USER->emma_plan_id."',
//    0
//    )
//    ");
    $data['lastid'] = $emmadb->insert_id;
}


$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);