<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/11/2017
 * Time: 3:48 PM
 */

require('../include/db.php');
include('../include/processing.php');
require('swiftmailer/swift_required.php');

$createdby = $fvmdb->real_escape_string($_POST['user_id']);
$emmaPlanId = $fvmdb->real_escape_string($_POST['plan-id']);

$username = $fvmdb->real_escape_string($_POST['username']);

$firstname = $fvmdb->real_escape_string($_POST['firstname']);
$middlename = $fvmdb->real_escape_string($_POST['middlename']);
$lastname = $fvmdb->real_escape_string($_POST['lastname']);

$landline = $fvmdb->real_escape_string($_POST['landline']);
$phone = $fvmdb->real_escape_string($_POST['phone']);
$title = $fvmdb->real_escape_string($_POST['title']);

$address = $fvmdb->real_escape_string($_POST['address']);
$state = $fvmdb->real_escape_string($_POST['state']);
$city = $fvmdb->real_escape_string($_POST['city']);
$zip = $fvmdb->real_escape_string($_POST['zip']);

$groups = $_POST['group-ids'];

$data = array();
$errors = array();

if (empty($username)) {
  $errors[] = 'empty username';
}

if (empty($firstname) || empty($lastname)) {
  $errors[] = 'empty name';
}

if (empty($phone)) {
  $errors[] = 'no phone';
}

if (empty($errors)) {
    $users = select_user_from_username($username);
//    $fvmdb->query("
//        SELECT u.*
//        FROM users u
//        WHERE u.username = '" . $username . "'
//    ");
    $user = $users->fetch_assoc();
    if (empty($user)) {
        $password = hash('ripemd128', $username);

        $insertUser = insert_user_full($username, $password, 'standard', 'private', 'user', $title, $firstname, $middlename, $lastname,
            $phone, '', 'en', $address, $city, $state, 'United States', $zip, 'yes', '%',
            '%', $createdby, '', '0', date('Y-m-d H:i:s'), date('Y-m-d H:i:s'), 'yes', '0', '0', '0',
            '0', '1', '10', 'normal', '', '0',
            $emmaPlanId, '0000', $username, $auth, $landline);
//            $fvmdb->query("
//    INSERT INTO users (
//      username,
//      password,
//      type,
//      mobile_type,
//      ems_type,
//      title,
//      firstname,
//      middlename,
//      lastname,
//      phone,
//      company,
//      preferredlanguage,
//      address,
//      city,
//      province,
//      country,
//      zip,
//      firstlogin,
//      filterorg,
//      filterlocation,
//      creator,
//      creatorip,
//      sales_person_id,
//      creation,
//      updated,
//      display,
//      private_program_head,
//      pad_program,
//      email_alerts,
//      text_alerts,
//      receive_notifications,
//      notification_distance,
//      map_type,
//      program_details,
//      needs_assistance,
//      emma_plan_id,
//      emma_pin,
//      emma_subscription_id,
//      auth,
//      landline_phone
//    ) VALUES (
//      '" . $username . "',
//      '" . $password . "',
//      'standard',
//      'private',
//      'user',
//      '" . $title . "',
//      '" . $firstname . "',
//      '" . $middlename . "',
//      '" . $lastname . "',
//      '" . $phone . "',
//      '',
//      'en',
//      '" . $address . "',
//      '" . $city . "',
//      '" . $state . "',
//      'United States',
//      '" . $zip . "',
//      'yes',
//      '%',
//      '%',
//      '" . $createdby . "',
//      '',
//      '0',
//      '" . date('Y-m-d H:i:s') . "',
//      '" . date('Y-m-d H:i:s') . "',
//      'yes',
//      '0',
//      '0',
//      '0',
//      '0',
//      '1',
//      '10',
//      'normal',
//      '',
//      '0',
//      '" . $emmaPlanId . "',
//      '0000',
//      '',
//      sha1('" . $username . "'),
//      '" . $landline . "'
//    )
//  ");
        $userid = $emmadb->insert_id;

        if (empty($insertUser)) {
            $errors[] = 'failed to insert: ' . $emmadb->error;
        }

        $insertPrivilege = insert_privileges($userid);
//        $fvmdb->query("
//            INSERT INTO privileges (
//              userid
//            ) VALUES (
//              '" . $userid . "'
//            )
//        ");

        $addMultiPlan = insert_emma_multi_plan_nullExpires($userid, $emmaPlanId, 1, '');
//            $fvmdb->query("
//            INSERT INTO emma_multi_plan (
//            user_id,
//            plan_id,
//            expires,
//            active,
//            guest_code
//            ) VALUES (
//            '". $userid ."',
//            '". $emmaPlanId ."',
//            '',
//            1,
//            ''
//            )
//        ");

        foreach ($groups as $k => $groupId) {
            $insertGroup = insert_user_groups($userid, $groupId);
//            $fvmdb->query("
//              INSERT INTO emma_user_groups (
//                user_id,
//                emma_group_id
//              ) VALUES (
//                '" . $userid . "',
//                '" . $groupId . "'
//              )
//            ");
        }

        if (empty($errors)) {

            $link ='https://'. __ROOT__ . '/verify_email.php?u=' .
                $userid . '&c=' . $password;

            $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"><head><link rel="stylesheet" type="text/css" href="css/app.css"><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta name="viewport" content="width=device-width"><title>EMMA User Account Activation</title></head><body style="-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;box-sizing:border-box;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;min-width:100%;padding:0;text-align:left;width:100%!important"><style>@media only screen{html{min-height:100%;background:#f3f3f3}}@media only screen and (max-width:596px){.small-float-center{margin:0 auto!important;float:none!important;text-align:center!important}.small-text-center{text-align:center!important}.small-text-left{text-align:left!important}.small-text-right{text-align:right!important}}@media only screen and (max-width:596px){.hide-for-large{display:block!important;width:auto!important;overflow:visible!important;max-height:none!important;font-size:inherit!important;line-height:inherit!important}}@media only screen and (max-width:596px){table.body table.container .hide-for-large,table.body table.container .row.hide-for-large{display:table!important;width:100%!important}}@media only screen and (max-width:596px){table.body table.container .callout-inner.hide-for-large{display:table-cell!important;width:100%!important}}@media only screen and (max-width:596px){table.body table.container .show-for-large{display:none!important;width:0;mso-hide:all;overflow:hidden}}@media only screen and (max-width:596px){table.body img{width:auto;height:auto}table.body center{min-width:0!important}table.body .container{width:95%!important}table.body .column,table.body .columns{height:auto!important;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;padding-left:16px!important;padding-right:16px!important}table.body .column .column,table.body .column .columns,table.body .columns .column,table.body .columns .columns{padding-left:0!important;padding-right:0!important}table.body .collapse .column,table.body .collapse .columns{padding-left:0!important;padding-right:0!important}td.small-1,th.small-1{display:inline-block!important;width:8.33333%!important}td.small-2,th.small-2{display:inline-block!important;width:16.66667%!important}td.small-3,th.small-3{display:inline-block!important;width:25%!important}td.small-4,th.small-4{display:inline-block!important;width:33.33333%!important}td.small-5,th.small-5{display:inline-block!important;width:41.66667%!important}td.small-6,th.small-6{display:inline-block!important;width:50%!important}td.small-7,th.small-7{display:inline-block!important;width:58.33333%!important}td.small-8,th.small-8{display:inline-block!important;width:66.66667%!important}td.small-9,th.small-9{display:inline-block!important;width:75%!important}td.small-10,th.small-10{display:inline-block!important;width:83.33333%!important}td.small-11,th.small-11{display:inline-block!important;width:91.66667%!important}td.small-12,th.small-12{display:inline-block!important;width:100%!important}.column td.small-12,.column th.small-12,.columns td.small-12,.columns th.small-12{display:block!important;width:100%!important}table.body td.small-offset-1,table.body th.small-offset-1{margin-left:8.33333%!important;Margin-left:8.33333%!important}table.body td.small-offset-2,table.body th.small-offset-2{margin-left:16.66667%!important;Margin-left:16.66667%!important}table.body td.small-offset-3,table.body th.small-offset-3{margin-left:25%!important;Margin-left:25%!important}table.body td.small-offset-4,table.body th.small-offset-4{margin-left:33.33333%!important;Margin-left:33.33333%!important}table.body td.small-offset-5,table.body th.small-offset-5{margin-left:41.66667%!important;Margin-left:41.66667%!important}table.body td.small-offset-6,table.body th.small-offset-6{margin-left:50%!important;Margin-left:50%!important}table.body td.small-offset-7,table.body th.small-offset-7{margin-left:58.33333%!important;Margin-left:58.33333%!important}table.body td.small-offset-8,table.body th.small-offset-8{margin-left:66.66667%!important;Margin-left:66.66667%!important}table.body td.small-offset-9,table.body th.small-offset-9{margin-left:75%!important;Margin-left:75%!important}table.body td.small-offset-10,table.body th.small-offset-10{margin-left:83.33333%!important;Margin-left:83.33333%!important}table.body td.small-offset-11,table.body th.small-offset-11{margin-left:91.66667%!important;Margin-left:91.66667%!important}table.body table.columns td.expander,table.body table.columns th.expander{display:none!important}table.body .right-text-pad,table.body .text-pad-right{padding-left:10px!important}table.body .left-text-pad,table.body .text-pad-left{padding-right:10px!important}table.menu{width:100%!important}table.menu td,table.menu th{width:auto!important;display:inline-block!important}table.menu.small-vertical td,table.menu.small-vertical th,table.menu.vertical td,table.menu.vertical th{display:block!important}table.menu[align=center]{width:auto!important}table.button.small-expand,table.button.small-expanded{width:100%!important}table.button.small-expand table,table.button.small-expanded table{width:100%}table.button.small-expand table a,table.button.small-expanded table a{text-align:center!important;width:100%!important;padding-left:0!important;padding-right:0!important}table.button.small-expand center,table.button.small-expanded center{min-width:0}}</style><span class="preheader" style="color:#f3f3f3;display:none!important;font-size:1px;line-height:1px;max-height:0;max-width:0;mso-hide:all!important;opacity:0;overflow:hidden;visibility:hidden"></span><table class="body" style="Margin:0;background:#f3f3f3;border-collapse:collapse;border-spacing:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;height:100%;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><td class="center" align="center" valign="top" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><center data-parsed="" style="min-width:580px;width:100%"><table align="center" class="container main float-center" style="Margin:0 auto;background:#fefefe;border:1px solid #00549d;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:580px"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="20px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;hyphens:auto;line-height:20px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" valign="middle" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><h1 id="email-title" class="text-center" style="Margin:0;Margin-bottom:10px;color:#00549d;font-family:Helvetica,Arial,sans-serif;font-size:34px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;padding-top:10px;text-align:center;word-wrap:normal">EMMA User Account Activation</h1></th><th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></tbody></table><hr><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="20px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;hyphens:auto;line-height:20px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="20px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;hyphens:auto;line-height:20px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><table class="callout" style="Margin-bottom:16px;border-collapse:collapse;border-spacing:0;margin-bottom:16px;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th class="callout-inner primary" style="Margin:0;background:#cae6ff;border:1px solid #444;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:10px;text-align:left;width:100%"><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
<p><b>You have been invited to use EMMA by your organization’s safety administrator.</b></p>
<p>A login username and password will be created for you.</p>
<p>Please click on this link to activate your EMMA account:   <a href="'. $link .'">Activate</a></p>
<p>Or visit: <a href="'. $link .'">'. $link .'</a></p>   
<p>NOTE: This link is for activating this account only.</p>  
<p><b>Thank you for making minutes matter!</b></p>
<p>If you have any questions or need technical support please email <a href="mailto:emma@think-safe.com">emma@think-safe.com</a> or call 319-377-5125 from 8-5pm Central Time or leave a message for a return call by our tech support team.</p>
<a href="https://www.emmaadmin.com/product_sheets.php"><b><i>What is EMMA?</i></b></a>
<p>Think Safe, Inc. <br/>
    <a href="https://www.think-safe.com">www.think-safe.com</a> <br/>
    The Creators of EMMA <br/>
    A First Voice app</p>
<hr><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><div class="comments" style="white-space:pre-line"></div></th><th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></tbody></table></th><th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></table></th></tr></tbody></table></td></tr></tbody></table></center></td></tr></table><!-- prevent Gmail on iOS font size manipulation --><div style="display:none;white-space:nowrap;font:15px courier;line-height:0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></body></html>';

            $bodyText = 'Copy this link into your browser to activate your EMMA account - ' .
                $link;

//            $transport = Swift_SmtpTransport::newInstance();
            $mailer = Swift_Mailer::newInstance($SMT);
            $swiftMessage = Swift_Message::newInstance()
                ->setSubject('EMMA Account Creation')
                ->setFrom(array('donotreply@emmaadmin.com' => 'EMMA Admin'))
                ->setTo(array($username => $firstname . ' ' . $lastname))
                ->setBody($body, 'text/html')
                ->addPart($bodyText, 'text/plain');
            $result = $mailer->send($swiftMessage);

            if (!$result) {
                $errors['email'] = 'Failed sending email';
            }
        }
    }else{
        if($user['emma_plan_id'] == '0'){
            $addEmmaUser = update_users_planID_with_userID($user['id'], $emmaPlanId);
//            $fvmdb->query("
//                UPDATE users
//                SET emma_plan_id = '". $emmaPlanId ."'
//                WHERE id = '". $user['id'] ."'
//            ");
            $addMultiPlan = insert_emma_multi_plan_nullExpires($user['id'], $emmaPlanId, 1, '');
//            $fvmdb->query("
//                INSERT INTO emma_multi_plan (
//                user_id,
//                plan_id,
//                expires,
//                active,
//                guest_code
//                ) VALUES (
//                '". $user['id'] ."',
//                '". $emmaPlanId ."',
//                '',
//                1,
//                ''
//                )
//            ");
        }else{
            $addMultiPlan = insert_emma_multi_plan_nullExpires($user['id'], $emmaPlanId, 1, '');
//            $fvmdb->query("
//                INSERT INTO emma_multi_plan (
//                user_id,
//                plan_id,
//                expires,
//                active,
//                guest_code
//                ) VALUES (
//                '". $user['id'] ."',
//                '". $emmaPlanId ."',
//                '',
//                1,
//                ''
//                )
//            ");
        }
    }
}

$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);
