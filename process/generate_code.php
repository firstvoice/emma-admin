<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 4/6/2018
 * Time: 1:23 PM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();


function generate() {
  $pool = '123456789ABCDEFGHJKMNPQRSTWXYZ';
  $str = '';
  for($i = 0; $i < 10; $i++) {
    $next = substr($pool, rand(0, 30), 1);
    $str .= $next;
  }
  return $str;
}

function exists($code) {
  $codes = select_guestCodes_with_guestCodeID($code);
//      $fvmdb->query("
//    select *
//    from emma_guest_codes
//    where guest_code_id = '".$code."'
//  ");
  return $codes->num_rows > 0;
}

if (empty($errors)) {
  do {
    $code = generate();
  } while(exists($code));
  $data['code'] = $code;
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);