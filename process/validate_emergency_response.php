<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 4/6/2018
 * Time: 1:23 PM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$messageId = $fvmdb->real_escape_string($_POST['message-id']);
$validatedId = $fvmdb->real_escape_string($_POST['validated-id']);

if (!isset($messageId) || empty($messageId)) {
  $errors['message-id'] = 'Message Id required';
}

if (empty($errors)) {
  $messages = select_validateEmergencyResponse_messages($messageId);
//      $fvmdb->query("
//    select er.*, u.emma_pin as user_pin, ers.color, ers.class, ers.table, ers.icon, CONCAT(u.firstname, ' ', u.lastname) AS user, u.phone, CONCAT(u.firstname, ' ', u.lastname) AS first_last_name, u.username, u.id as user_id
//    from emergency_responses er
//    join users u on er.user_id = u.id
//    join emergency_response_status ers on er.status = ers.emergency_response_status_id
//    where er.emergency_response_id = '" . $messageId . "'
//  ");
  if ($message = $messages->fetch_assoc()) {
    $validationTime = date('Y-m-d H:i:s');
    $data['message'] = $message;
    $validationUsers = select_validateEmergencyResponse_validationUsers($validatedId);
//        $fvmdb->query("
//      select u.*, CONCAT(u.firstname, ' ', u.lastname) AS full_name
//      from users u
//      where u.id = '" . $validatedId . "'
//    ");
    if ($validationUser = $validationUsers->fetch_assoc()) {
      $data['message']['validation_user'] = $validationUser['full_name'];
      $data['message']['validation_time'] = $validationTime;
    }

    $updateMessage = update_emergencyResponses_with_responseID($messageId, $validatedId, $validationTime, $message['user_pin']);
//        $fvmdb->query("
//      update emergency_responses er
//      set pin = '" . $message['user_pin'] . "',
//          validated_id = '" . $validatedId . "',
//          validated_time = '".$validationTime."'
//      where er.emergency_response_id = '" . $messageId . "'
//    ");
    if (!$updateMessage) {
      $errors['validate-message'] = $fvmdb->error;
    }
  }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);