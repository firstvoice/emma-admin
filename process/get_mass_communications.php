<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 8/28/2017
 * Time: 10:04 AM
 */

include('../include/db.php');
include('../include/processing.php');

$columns = $_GET['columns'];
$draw = $fvmdb->real_escape_string($_GET['draw']);
$length = $fvmdb->real_escape_string($_GET['length']);
$order = $_GET['order'];
$search = $_GET['search'];
$start = $fvmdb->real_escape_string($_GET['start']);
$active = $fvmdb->real_escape_string($_GET['active']);
$emmaPlanId = $fvmdb->real_escape_string($_GET['emma-plan-id']);

$data = array();

$orderString = $columns[$order[0]['column']]['data'] . ' ' . $order[0]['dir'];

$events = select_getmasscommunications_events($emmaPlanId,$length,$start, $orderString, $_GET['columns'][0]['search']['value'], $_GET['columns'][1]['search']['value'], $_GET['columns'][2]['search']['value']);
//    $fvmdb->query("
//    SELECT SQL_CALC_FOUND_ROWS mc.created_date, CONCAT(u.firstname, ' ', u.lastname) AS creator, mc.notification, mc.created_by_id, u.username as useremail, mc.emma_mass_communication_id
//    FROM emma_mass_communications mc
//    JOIN users u ON mc.created_by_id = u.id
//    WHERE (" . ($emmaPlanId != '' ? "mc.emma_plan_id = " . $emmaPlanId : "1") . ")
//    AND (" . ($_GET['columns'][0]['search']['value'] != "" ? "mc.created_date like ('%" . $_GET['columns'][0]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][1]['search']['value'] != "" ? "CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') like ('%" . $_GET['columns'][1]['search']['value'] . "%')" : "1") . ")
//    AND (" . ($_GET['columns'][2]['search']['value'] != "" ? "mc.notification like ('%" . $_GET['columns'][2]['search']['value'] . "%')" : "1") . ")
//    ORDER BY ". $orderString .", created_date desc
//    LIMIT " . $length . " OFFSET " . $start . "
//");

/*
$events = $fvmdb->query("
    SELECT SQL_CALC_FOUND_ROWS mc.created_date, CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') AS creator, mc.notification, mc.created_by_id
    FROM emma_mass_communications mc
    JOIN users u ON mc.created_by_id = u.id
    WHERE mc.emma_plan_id = 1
    ORDER BY mc.created_date DESC
");
*/
$found = select_FOUND_ROWS();
//    $fvmdb->query("
//    SELECT FOUND_ROWS()
//");
$count = $found->fetch_assoc();

$data['iTotalRecords'] = $count['FOUND_ROWS()'];
$data['iTotalDisplayRecords'] = $count['FOUND_ROWS()'];
$data['sEcho'] = $draw;
$data['aaData'] = array();
while ($event = $events->fetch_assoc()) {
  $data['aaData'][] = $event;
}

echo json_encode($data);