<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 4/6/2018
 * Time: 1:23 PM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$scriptId = $fvmdb->real_escape_string($_POST['emma-script-id']);
$typeId = $fvmdb->real_escape_string($_POST['type']);
$broadcastId = $fvmdb->real_escape_string($_POST['broadcast']);
$name = $fvmdb->real_escape_string($_POST['name']);
$text = $fvmdb->real_escape_string($_POST['text']);
$group = $fvmdb->real_escape_string($_POST['group']);

if (empty($scriptId) || empty($name) || empty($text)) {
  $errors['empty'] = 'Not all fields have been filled out';
}

if (empty($errors)) {
  $updateScript = update_emmaScripts_with_scriptID($scriptId, $name, $text, $group, $broadcastId, $typeId);
//      $fvmdb->query("
//    update emma_scripts as s
//    set
//      s.name = '" . $name . "',
//      s.text = '" . $text . "',
//      s.emma_script_group_id = '". $group ."',
//      s.emma_broadcast_type_id = '". $broadcastId ."',
//      s.emma_script_type_id = '". $typeId ."'
//    where s.emma_script_id = '" . $scriptId . "'
//  ");
  if (!$updateScript) {
    $errors['sql'] = $fvmdb->error;
  }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);