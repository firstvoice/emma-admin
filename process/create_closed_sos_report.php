<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 6/15/2018
 * Time: 3:42 PM
 */

include('../include/db.php');
include('../include/processing.php');
require_once('../include/fpdf/fpdf.php');
require_once('../include/fpdf/fpdi.php');

$id = $fvmdb->real_escape_string($_GET['id']);

$callReports = select_sosCallReports_with_reportID($id);
//$fvmdb->query("
//  select scr.*, s.closed_date, CONCAT(u.firstname, ' ', u.lastname) as closed_name
//  from emma_sos_call_reports scr
//  join emma_sos s on scr.emma_sos_id = s.emma_sos_id
//  join users u on s.closed_by_id = u.id
//  where emma_sos_call_report_id = '" . $id . "'
//");
if ($callReport = $callReports->fetch_assoc()) {
  // echo '<pre>' . print_r($callReport, true) . '</pre>';

//create pdf
  $pdf = new FPDI('P', 'mm', 'A4');

//make background image
  $pdf->setSourceFile('../documents/blank.pdf');
  $tplIdx = $pdf->importPage(1);
  $pdf->AddPage();
  $pdf->useTemplate($tplIdx, 0, 0, 210, 297, false);
  $pdf->SetAutoPageBreak(false);

  $clientId = $callReport['client_id'];
  $clientName = $callReport['closed_name'];
  $reportDate = date('Y-m-d', strtotime($callReport['closed_date']));
  $reportTime = date('H:i:s', strtotime($callReport['closed_date']));
  $reportPost = $callReport['report_post'];
  $locationName = $callReport['emma_site_name'];
  $locationId = $callReport['site_id'];
  $incidentAddress = $callReport['emma_site_street_address'];
  $incidentCity = $callReport['emma_site_city'];
  $incidentState = $callReport['emma_site_state'];
  $incidentZip = $callReport['emma_site_zip'];
  $policeName = $callReport['police_name'];
  $policeTime = $callReport['police_time'];
  $reslifeName = $callReport['reslife-name'];
  $reslifeTime = $callReport['reslife-time'];
  $otherNotified = $callReport['other_service'];
  $otherName = $callReport['other_service_name'];
  $otherTime = $callReport['other_service_time'];
  $officerName = $callReport['officer_name'];
  $incidentDescription = $callReport['incident_description'];
  $resultingAction = $callReport['incident_followup'];

//header
  $pdf->SetFont('Arial', '', 20);
  $pdf->SetTextColor(0, 120, 193);
  $pdf->SetXY(0, 0);
  $pdf->MultiCell(0, 26, 'Closed Security Call Report', '', 'C', 0);
//general info
  $pdf->setFont('Arial', '', 10);
  $pdf->SetXY(15, 20);
  $pdf->MultiCell(100, 26, 'Closed By:___________________', '', 'L', 0);
  $pdf->SetXY(75, 20);
  $pdf->MultiCell(100, 26, 'Date Closed:_________________', '', 'L', 0);
  $pdf->SetXY(15, 30);
  $pdf->MultiCell(100, 26, 'Post:______________________', '', 'L', 0);
  $pdf->SetXY(68, 30);
  $pdf->MultiCell(130, 26,
    'Location:_________________________________________________________', '',
    'L', 0);
  $pdf->SetXY(135, 20);
  $pdf->MultiCell(95, 26, 'Report Number:_________________', '', 'L', 0);

  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetXY(36, 20);
  $pdf->MultiCell(100, 26, $clientName, '', 'L', 0);
  $pdf->SetXY(100, 20);
  $pdf->MultiCell(100, 26, $reportDate . ' ' . $reportTime, '', 'L', 0);
  $pdf->SetXY(32, 30);
  $pdf->MultiCell(100, 26, $reportPost, '', 'L', 0);
  $pdf->SetXY(83, 30);
  $pdf->MultiCell(130, 26,
    $locationName . ': ' . $incidentAddress . ' ' . $incidentCity . ', ' .
    $incidentState . ' ' . $incidentZip, '', 'L', 0);
  $pdf->SetXY(161, 20);
  $pdf->MultiCell(95, 26, $reportNum, '', 'L', 0);

//specific header
  $pdf->SetFont('Arial', '', 16);
  $pdf->SetTextColor(251, 252, 252);
  $pdf->SetFillColor(0, 120, 193);
  $pdf->SetXY(15, 50);
  $pdf->MultiCell(180, 10, 'Incident Information', '', 'C', true);
//officer
  $pdf->setFont('Arial', '', 10);
  $pdf->SetTextColor(0, 120, 193);
  $pdf->SetXY(15, 100 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(100, 26, 'Reporting Officer:________________________', '',
    'L', 0);

  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetXY(44, 100 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(100, 26, $officerName, '', 'L', 0);

//description
  $pdf->SetTextColor(0, 120, 193);
  $pdf->SetXY(15, 110 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(100, 26, 'Incident Description:', '', 'L', 0);
  $pdf->SetFillColor(209, 211, 212);
  $pdf->SetXY(15, 125 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(180, 40, '', '', 'L', true);

  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetFillColor(209, 211, 212);
  $pdf->SetXY(16, 126 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(178, 4, $incidentDescription, '', 'L', 0);

//after
  $pdf->SetTextColor(0, 120, 193);
  $pdf->SetXY(15, 160 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(100, 26, 'Results of Investigation & Action Taken:', '', 'L',
    0);
  $pdf->SetXY(15, 175 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(180, 40, '', '', 'L', true);

  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetXY(16, 176 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(178, 4, $resultingAction, '', 'L', true);

//supervisor sign
  $pdf->SetTextColor(0, 120, 193);
  $pdf->SetXY(15, 215 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(75, 26, 'Supervisor Name:__________________', '', 'L', 0);
  $pdf->SetXY(80, 215 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(90, 26, 'Supervisor Signature:______________________', '',
    'L', 0);
  $pdf->SetXY(160, 215 + ((count($personName) - 1) * 5));
  $pdf->MultiCell(75, 26, 'Date:____________', '', 'L', 0);

  $pdf->Output();
} else {
  echo 'Can not find call report id: ' . $id;
}
