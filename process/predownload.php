<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 9/5/2019
 * Time: 2:29 PM
 */
session_start();
require('../include/db.php');
require('../include/processing.php');

$errors = array();
$data = array();

$user = $fvmdb->real_escape_string($_POST['user-id']);
$type = $fvmdb->real_escape_string($_POST['type']);
$link = $fvmdb->real_escape_string($_POST['link']);

$data['link'] = $link;

if($user == null) {
    if (empty($firstName = $fvmdb->real_escape_string($_POST['first-name']))) $errors['first-name'] = 'First Name is required';
    if (empty($lastName = $fvmdb->real_escape_string($_POST['last-name']))) $errors['last-name'] = 'Last Name is required';
    $company = $fvmdb->real_escape_string($_POST['org']);
    $phone = $fvmdb->real_escape_string($_POST['landline']);
    $mobile = $fvmdb->real_escape_string(($_POST['mobile']));
    if (empty($phone) && empty($mobile)) {
        $errors['mobile'] = 'Phone is required';
    }
    if (empty($email = $fvmdb->real_escape_string($_POST['email']))) $errors['email'] = 'Email is required';


    $description = $type;

    if (empty($errors)) {
        $uniqueId = uniqid('');
        $insertRawLead = insert_raw_leads_full($uniqueId, $firstName, $lastName, $company, $phone, $mobile, $email, $description, date('Y-m-d H:i:s'), 'Active', 1, 0);
//            $crmdb->query("
//            INSERT INTO raw_leads (
//              lead_id,
//              first_name,
//              last_name,
//              company,
//              phone,
//              mobile,
//              email,
//              description,
//              created_date,
//              `status`,
//              active,
//              approved
//            ) VALUES (
//              '" . $uniqueId . "',
//              '" . $firstName . "',
//              '" . $lastName . "',
//              '" . $company . "',
//              '" . $phone . "',
//              '" . preg_replace("/[^0-9]/", "", $mobile) . "',
//              '" . $email . "',
//              '" . $description . "',
//              '". date('Y-m-d H:i:s') ."',
//              'Active',
//              '1',
//              '0'
//            )
//        ");
    }
    $leadid = $uniqueId;
}else{
    $leadid = $user;
}

$trackdownload = insert_emma_downloads($leadid, $type);
//    $fvmdb->query("
//    INSERT INTO emma_downloads (lead_id, download) VALUES ('". $leadid ."','". $type ."')
//");

$_SESSION['user'] = $leadid;

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);
