<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 12/19/2016
 * Time: 15:01
 */
include('../include/db.php');
require('../vendor/php-jwt-master/src/JWT.php');
require('../vendor/php-jwt-master/src/BeforeValidException.php');
require('../vendor/php-jwt-master/src/ExpiredException.php');
require('../vendor/php-jwt-master/src/SignatureInvalidException.php');
$jwt = $fvmdb->real_escape_string($_POST['jwt']);
$reportType = $fvmdb->real_escape_string($_POST['report']);
$CONFIG = json_decode(file_get_contents('../config/config.json'));

$USER = null;
try {
  $token = Firebase\JWT\JWT::decode($jwt, $CONFIG->key,
    array('HS512'));
} catch (\Firebase\JWT\BeforeValidException $bve) {
  //echo $bve->getMessage();
  header('Location: ../index.php');
} catch (\Firebase\JWT\ExpiredException $ee) {
  //echo $ee->getMessage();
  header('Location: ../index.php');
} catch (\Firebase\JWT\SignatureInvalidException $sie) {
  //echo $sie->getMessage();
  header('Location: ../index.php');
} catch (Exception $e) {
  //echo 'UNKNOWN EXCEPTION: ';
  //echo $e->getMessage();
  header('Location: ../index.php');
}
$USER = $token->data;
header('Content-Type: text/csv');
header('Content-Disposition: attachment;filename=' . $reportType .
  '_report.csv');

$output = fopen('php://output', 'w');
fputcsv($output, array(
  'Site',
  'Type',
  'Creator',
  'Created Date',
  'Latitude',
  'Longitude',
  'Closer',
  'Closed Date',
  'Description',
  'Comments',
  'Security Messages'));

if ($reportType == 'security') {
  $securities = select_securites_for_report($USER->emma_plan_id);
//      $fvmdb->query("
//    select s.*, site.emma_site_name, st.name as security_type, CONCAT(c.firstname, ' ', c.lastname) as creator_name, CONCAT(u.firstname, ' ', u.lastname) as closer_name
//    from emma_securities s
//    join emma_sites site on s.emma_site_id = site.emma_site_id
//    join emma_security_types st on s.emma_security_type_id = st.emma_security_type_id
//    join users c on s.created_by_id = c.id
//    join users u on s.closed_by_id = u.id
//    where s.emma_plan_id = '" . $USER->emma_plan_id . "'
//  ");
  while ($security = $securities->fetch_assoc()) {
    $messages = '';
    $systemMessages = select_securityMessages_with_securityID($security['emma_security_id']);
//        $fvmdb->query("
//      select *
//      from emma_security_messages
//      where security_id = '" . $security['emma_security_id'] . "'
//    ");
    while ($systemMessage = $systemMessages->fetch_assoc()) {
      $messages .= $systemMessage['message'] . ', ';
    }

    fputcsv($output, array(
      $security['emma_site_name'],
      $security['security_type'],
      $security['creator_name'],
      $security['created_date'],
      $security['latitude'],
      $security['longitude'],
      $security['closer_name'],
      $security['closed_date'],
      $security['description'],
      $security['comments'],
      $messages
    ));
  }
}

fclose($output);
