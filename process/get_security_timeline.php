<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/31/2017
 * Time: 12:35 PM
 */
require('../include/db.php');
include('../include/processing.php');

$data = array();
$errors = array();

$securityId = $fvmdb->real_escape_string($_POST['id']);
$received = str_replace(",", "','",
  $fvmdb->real_escape_string($_POST['received']));

if (empty($securityId)) $errors['security-id'] = 'No security id';

$data['received'] = $received;
$data['incidents'] = array();
$data['sub-events'] = array();
if (empty($errors)) {
  $responses = select_getsecurityTimeline_responses($securityId, $received);
//      $fvmdb->query("
//    SELECT *
//    FROM
//    (
//      (
//        SELECT
//          CONCAT('usr-', sr.emma_security_response_id)                AS id,
//          sr.comments                                                 AS message,
//          CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') AS user,
//          u.id as user_id,
//          ''                                                          AS status,
//          ''                                                          AS status_name,
//          '#3adb76'                                                   AS status_color,
//          'success'                                                    AS status_class,
//          CONCAT(u.firstname, ' ', u.lastname)                        AS first_last_name,
//          u.username,
//          sr.updated_date,
//          '' as description,
//          '' as comments,
//          '' as emma_security_message_id
//        FROM emma_security_responses sr
//        JOIN users u ON sr.user_id = u.id
//        WHERE sr.security_id = '" . $securityId . "'
//          AND CONCAT('usr-', sr.emma_security_response_id) NOT IN ('" .
//    $received . "')
//      ) UNION ALL (
//        SELECT
//          CONCAT('sys-', sm.emma_security_message_id) AS id,
//          sm.message,
//          CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') AS user,
//          u.id as user_id,
//          ''                                        AS status,
//          'Alert'                                   AS status_name,
//          '#1779ba'                                 AS status_color,
//          'system'                                  AS status_class,
//          'System'                                  AS first_last_name,
//          ''                                        AS username,
//          sm.created_date AS updated_date,
//          sm.description,
//          sm.comments,
//          sm.emma_security_message_id
//        FROM emma_security_messages sm
//        JOIN users u on sm.created_by_id = u.id
//        WHERE sm.security_id = '" . $securityId . "'
//          AND CONCAT('sys-', sm.emma_security_message_id) NOT IN ('" . $received . "')
//      )
//    ) AS results
//    ORDER BY updated_date
//  ");
  while ($response = $responses->fetch_assoc()) {
    $data['incidents'][] = $response;
  }
}
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);
