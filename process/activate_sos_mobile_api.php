<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/12/2019
 * Time: 3:56 PM
 */



require('../include/db.php');
include('../include/processing.php');

require('../vendor/php-jwt-master/src/JWT.php');
require('../vendor/php-jwt-master/src/BeforeValidException.php');
require('../vendor/php-jwt-master/src/ExpiredException.php');
require('../vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('../config/config.json'));

$USER = null;

$token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key, array('HS512'));

$USER = $token->data;


$data = array();
$errors = array();

$id = $fvmdb->real_escape_string($_POST['id']);
$lat = $fvmdb->real_escape_string($_POST['lat']);
$lng = $fvmdb->real_escape_string($_POST['lng']);
$entered_pin = $fvmdb->real_escape_string($_POST['pin']);


//If $entered_pin is empty, it means they clicked the button and need to activate the SOS.
if (empty($errors) && empty($entered_pin)) {
    $url = 'https://retrac.xyz/mobile-api/emma/update_sos.php';
    $fields = array(
        "a" => $USER->auth,
        "id" => $id,
        "lat" => $lat,
        "lng" => $lng
    );

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_ENCODING, "");

    header('Content-Type: text/html');
    $var = curl_exec($ch);
    //save curl exec , it returns variable so store that, then, parse the json object.
    $data['var'] = json_decode($var);
    $data['success'] = empty($errors);
}
//If $entered_pin has a pin, it means they are attempting to cancel the SOS, so we need to check if it is valid or not
if(empty($errors) && !empty($entered_pin))
{
    //Send a SOS because pin was wrong
    if($entered_pin != $USER->emma_pin) {
        $url = 'https://retrac.xyz/mobile-api/emma/update_sos.php';
        $fields = array(
            "a" => $USER->auth,
            "id" => $id,
            "lat" => $lat,
            "lng" => $lng
        );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");

        header('Content-Type: text/html');
        $var = curl_exec($ch);
        //save curl exec , it returns variable so store that, then, parse the json object.
        $data['var'] = json_decode($var);
        $data['invalid'] = true;
    }
    else{
        //it will show a success modal saying the pin was correct and the notifications dont' go out.
        update_SOSCanceled_with_sosID_Lat_lng($id, $lat, $lng);
//        $fvmdb->query("
//        UPDATE emma_sos
//        SET cancelled_date = '".date('Y/m/d H:i:s')."', cancelled_lat = '".$lat."', cancelled_lng = '".$lng."'
//        WHERE emma_sos_id = '".$id."'
//        ");
        $data['success'] = empty($errors);
    }
}


echo json_encode($data);