<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/21/2019
 * Time: 10:17 AM
 */
include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$planId = $fvmdb->real_escape_string($_POST['plan-id']);
$name = $fvmdb->real_escape_string($_POST['name']);
$text = $fvmdb->real_escape_string($_POST['text']);

if (empty($planId) || empty($name) || empty($text)) {
    $errors['empty'] = 'Not all fields have been filled out';
}

if (empty($errors)) {
    $updateScript = insert_text_script($planId,$name, $text);
//    $fvmdb->query("
//    insert into emma_scripts (
//      emma_plan_id,
//      emma_script_type_id,
//      `name`,
//      text,
//      emma_script_group_id,
//      emma_broadcast_type_id,
//      emma_mass_notification_type_id
//    ) values (
//      '" . $planId . "',
//      '" . $typeId . "',
//      '" . $name . "',
//      '" . $text . "',
//      '" . $group . "',
//      null,
//      '". $broadcast ."'
//    )
//  ");
    if (!$updateScript) {
        $errors['sql'] = $fvmdb->error;
    }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);