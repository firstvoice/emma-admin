<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 9/24/2019
 * Time: 9:43 AM
 */

include('../include/db.php');
include('../include/processing.php');
$data = array();
$errors = array();

$student = $fvmdb->real_escape_string($_POST['student']);
$parent = $fvmdb->real_escape_string($_POST['parent']);

if(empty($parent)){$errors['no-parent'] = 'Parent/Guardian is required.';}

if(empty($errors)){

    $students = select_studentUser_with_userID($student);
    $s = $students->fetch_assoc();

    $insertParent = insert_students_to_guardians($student, $s['name'], $parent);
}

$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);