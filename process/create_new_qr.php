<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 4/6/2018
 * Time: 1:23 PM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$planId = $fvmdb->real_escape_string($_POST['qr-plan']);
$groupId = $fvmdb->real_escape_string($_POST['qr-group']);
$user = $fvmdb->real_escape_string($_POST['user-id']);

if (empty($planId) || empty($groupId) || empty($user)) {
    $errors['empty'] = 'Not all fields have been filled out';
}

if (empty($errors)) {
    $qrcode = select_get_qrcodes_plans_groups($planId,$groupId);
    if($qrcode->num_rows > 0){
        $errors['duplicate'] = 'This QR Code already exists.';
    }else {
        $newqr = insert_qr_code($planId, $groupId);
        if (!$newqr) {
            $errors['sql'] = $fvmdb->error;
        }
    }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);