<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/7/2019
 * Time: 4:31 PM
 */


include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$assetId = $fvmdb->real_escape_string($_POST['asset-id']);

if (empty($assetId)) {
    $errors['asset-id'] = 'Asset Id not provided';
}

if (empty($errors)) {
    $deleteAsset = update_emmaAsset_activeDeleted_with_assetID($assetId, 0, 1);
//        $fvmdb->query("
//    update emma_assets
//    set deleted = 1, active = 0
//    where emma_asset_id = '" . $assetId . "'
//  ");
    if (!$deleteAsset) {
        $errors['sql'] = $fvmdb->error;
    }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);