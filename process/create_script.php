<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 4/6/2018
 * Time: 1:23 PM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$planId = $fvmdb->real_escape_string($_POST['plan-id']);
$typeId = $fvmdb->real_escape_string($_POST['script-type']);
$broadcast = $fvmdb->real_escape_string($_POST['broadcast']);
$group = $fvmdb->real_escape_string($_POST['group']);
$name = $fvmdb->real_escape_string($_POST['name']);
$text = $fvmdb->real_escape_string($_POST['text']);

if (empty($planId) || empty($typeId) || empty($name) || empty($text)) {
  $errors['empty'] = 'Not all fields have been filled out';
}

if (empty($errors)) {
  $updateScript = insert_emma_scripts($typeId, $planId, $group, $broadcast, $name, $text);
//  $fvmdb->query("
//    insert into emma_scripts (
//      emma_plan_id,
//      emma_script_type_id,
//      `name`,
//      text,
//      emma_script_group_id,
//      emma_broadcast_type_id
//    ) values (
//      '" . $planId . "',
//      '" . $typeId . "',
//      '" . $name . "',
//      '" . $text . "',
//      '" . $group . "',
//      '". $broadcast ."'
//    )
//  ");
  if (!$updateScript) {
    $errors['sql'] = $fvmdb->error;
  }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);