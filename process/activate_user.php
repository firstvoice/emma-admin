<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/14/2017
 * Time: 8:09 PM
 */

include('../include/db.php');
//include('../include/processing.php');
require('swiftmailer/swift_required.php');

$errors = array();
$data = array();

$userid = $fvmdb->real_escape_string($_POST['u']);
$password = $fvmdb->real_escape_string($_POST['p']);
$confirmpassword = $fvmdb->real_escape_string($_POST['cp']);
$oldPassord = $fvmdb->real_escape_string($_POST['c']);
$firstName = $fvmdb->real_escape_string($_POST['first-name']);
$middleName = $fvmdb->real_escape_string($_POST['middle-name']);
$lastName = $fvmdb->real_escape_string($_POST['last-name']);
$landline = $fvmdb->real_escape_string($_POST['landline']);
$mobile = $fvmdb->real_escape_string($_POST['mobile']);
$title = $fvmdb->real_escape_string($_POST['title']);
$address = $fvmdb->real_escape_string($_POST['address']);
$city = $fvmdb->real_escape_string($_POST['city']);
$state = $fvmdb->real_escape_string($_POST['state']);
$zip = $fvmdb->real_escape_string($_POST['zip']);
$timezone = $fvmdb->real_escape_string($_POST['timezone']);
$pin = $fvmdb->real_escape_string($_POST['pin']);

if (empty($userid)) {
  $errors[] = 'No user';
}

if (empty($oldPassord)) {
  $errors[] = 'No old password';
}

if (empty($password)) {
  $errors[] = 'No password';
}else{
    $specialChar = false;
    $number = false;
    $uppercase = false;
    $lowercase = false;
    $length = false;

    $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/';
    if (preg_match($pattern, $password)){
        $specialChar = true;
    }
    else{
        $errors[] = 'Missing Special Character';
    }
    if(1 === preg_match('~[0-9]~', $password)){
        $number = true;
    }
    else{
        $errors[] = 'Missing Number';
    }
    if (preg_match('/[A-Z]/', $password)) {
        $uppercase = true;
    }
    else{
        $errors[] = 'Missing Uppercase Character';
    }
    if (preg_match('/[a-z]/', $password)) {
        $lowercase = true;
    }
    else{
        $errors[] = 'Missing Lowercase Character';
    }
    if (strlen($password) >= 8) {
        $length = true;
    }
    else{
        $errors[] = 'Not Long Enough';
    }

    if(!$specialChar || !$number || !$uppercase || !$lowercase || !$length){
        $errors[] = 'Password requires an uppercase, lowercase, number, a special character, and atleast 8 characters long.';
    }
}

if ($password != $confirmpassword) {
  $errors[] = 'passwords don\'t match';
}

if($pin == '0000'){
    $errors[] = 'PIN 0000 is reserved';
}
if($pin == ''){
    $errors[] = 'PIN is required';
}

if (empty($errors)) {
  $usernames = select_user_from_userID($userid);
//  $fvmdb->query("
//    SELECT u.username, u.firstname, u.lastname
//    FROM users AS u
//    WHERE u.id = " . $userid . "
//  ");
  $username = $usernames->fetch_assoc();
  $hashed_userid = hash('ripemd128', $username['username']);

  if ($oldPassord == $hashed_userid) {
    $update = update_userDetails($userid, $firstName, $middleName, $lastName, $landline, $mobile, $title, $address, $city, $state, $zip, $pin, $timezone, $password);
    if (!$update) {
      $errors[] = 'Update Failed';
    } else {
        insert_emma_old_passwords($userid, $password);
      $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"><head><link rel="stylesheet" type="text/css" href="css/app.css"><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta name="viewport" content="width=device-width"><title>EMMA User Account Activation</title></head><body style="-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;box-sizing:border-box;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;min-width:100%;padding:0;text-align:left;width:100%!important"><style>@media only screen{html{min-height:100%;background:#f3f3f3}}@media only screen and (max-width:596px){.small-float-center{margin:0 auto!important;float:none!important;text-align:center!important}.small-text-center{text-align:center!important}.small-text-left{text-align:left!important}.small-text-right{text-align:right!important}}@media only screen and (max-width:596px){.hide-for-large{display:block!important;width:auto!important;overflow:visible!important;max-height:none!important;font-size:inherit!important;line-height:inherit!important}}@media only screen and (max-width:596px){table.body table.container .hide-for-large,table.body table.container .row.hide-for-large{display:table!important;width:100%!important}}@media only screen and (max-width:596px){table.body table.container .callout-inner.hide-for-large{display:table-cell!important;width:100%!important}}@media only screen and (max-width:596px){table.body table.container .show-for-large{display:none!important;width:0;mso-hide:all;overflow:hidden}}@media only screen and (max-width:596px){table.body img{width:auto;height:auto}table.body center{min-width:0!important}table.body .container{width:95%!important}table.body .column,table.body .columns{height:auto!important;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;padding-left:16px!important;padding-right:16px!important}table.body .column .column,table.body .column .columns,table.body .columns .column,table.body .columns .columns{padding-left:0!important;padding-right:0!important}table.body .collapse .column,table.body .collapse .columns{padding-left:0!important;padding-right:0!important}td.small-1,th.small-1{display:inline-block!important;width:8.33333%!important}td.small-2,th.small-2{display:inline-block!important;width:16.66667%!important}td.small-3,th.small-3{display:inline-block!important;width:25%!important}td.small-4,th.small-4{display:inline-block!important;width:33.33333%!important}td.small-5,th.small-5{display:inline-block!important;width:41.66667%!important}td.small-6,th.small-6{display:inline-block!important;width:50%!important}td.small-7,th.small-7{display:inline-block!important;width:58.33333%!important}td.small-8,th.small-8{display:inline-block!important;width:66.66667%!important}td.small-9,th.small-9{display:inline-block!important;width:75%!important}td.small-10,th.small-10{display:inline-block!important;width:83.33333%!important}td.small-11,th.small-11{display:inline-block!important;width:91.66667%!important}td.small-12,th.small-12{display:inline-block!important;width:100%!important}.column td.small-12,.column th.small-12,.columns td.small-12,.columns th.small-12{display:block!important;width:100%!important}table.body td.small-offset-1,table.body th.small-offset-1{margin-left:8.33333%!important;Margin-left:8.33333%!important}table.body td.small-offset-2,table.body th.small-offset-2{margin-left:16.66667%!important;Margin-left:16.66667%!important}table.body td.small-offset-3,table.body th.small-offset-3{margin-left:25%!important;Margin-left:25%!important}table.body td.small-offset-4,table.body th.small-offset-4{margin-left:33.33333%!important;Margin-left:33.33333%!important}table.body td.small-offset-5,table.body th.small-offset-5{margin-left:41.66667%!important;Margin-left:41.66667%!important}table.body td.small-offset-6,table.body th.small-offset-6{margin-left:50%!important;Margin-left:50%!important}table.body td.small-offset-7,table.body th.small-offset-7{margin-left:58.33333%!important;Margin-left:58.33333%!important}table.body td.small-offset-8,table.body th.small-offset-8{margin-left:66.66667%!important;Margin-left:66.66667%!important}table.body td.small-offset-9,table.body th.small-offset-9{margin-left:75%!important;Margin-left:75%!important}table.body td.small-offset-10,table.body th.small-offset-10{margin-left:83.33333%!important;Margin-left:83.33333%!important}table.body td.small-offset-11,table.body th.small-offset-11{margin-left:91.66667%!important;Margin-left:91.66667%!important}table.body table.columns td.expander,table.body table.columns th.expander{display:none!important}table.body .right-text-pad,table.body .text-pad-right{padding-left:10px!important}table.body .left-text-pad,table.body .text-pad-left{padding-right:10px!important}table.menu{width:100%!important}table.menu td,table.menu th{width:auto!important;display:inline-block!important}table.menu.small-vertical td,table.menu.small-vertical th,table.menu.vertical td,table.menu.vertical th{display:block!important}table.menu[align=center]{width:auto!important}table.button.small-expand,table.button.small-expanded{width:100%!important}table.button.small-expand table,table.button.small-expanded table{width:100%}table.button.small-expand table a,table.button.small-expanded table a{text-align:center!important;width:100%!important;padding-left:0!important;padding-right:0!important}table.button.small-expand center,table.button.small-expanded center{min-width:0}}</style><span class="preheader" style="color:#f3f3f3;display:none!important;font-size:1px;line-height:1px;max-height:0;max-width:0;mso-hide:all!important;opacity:0;overflow:hidden;visibility:hidden"></span><table class="body" style="Margin:0;background:#f3f3f3;border-collapse:collapse;border-spacing:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;height:100%;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><td class="center" align="center" valign="top" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><center data-parsed="" style="min-width:580px;width:100%"><table align="center" class="container main float-center" style="Margin:0 auto;background:#fefefe;border:1px solid #00549d;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:580px"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="20px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;hyphens:auto;line-height:20px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" valign="middle" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><h1 id="email-title" class="text-center" style="Margin:0;Margin-bottom:10px;color:#00549d;font-family:Helvetica,Arial,sans-serif;font-size:34px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;padding-top:10px;text-align:center;word-wrap:normal">EMMA User Account Activation</h1></th><th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></tbody></table><hr><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="20px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;hyphens:auto;line-height:20px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="20px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;hyphens:auto;line-height:20px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><table class="callout" style="Margin-bottom:16px;border-collapse:collapse;border-spacing:0;margin-bottom:16px;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th class="callout-inner primary" style="Margin:0;background:#cae6ff;border:1px solid #444;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:10px;text-align:left;width:100%"><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
<p>Your EMMA username and password has been created!<br/>
To use your credentials, please follow the EMMA download or website login instructions provided by your EMMA Administrator.  
<br/><br/>
<b><u>PC User Access:</u></b><br/>
The website for the system is <a href="https://www.emmaadmin.com">www.emmaadmin.com.</a>  <b><i>(Remember to bookmark EMMA in your favorite browser for quick and easy access!)</i></b>
<br/><br/>
<b><u>Mobile Device Access:</u></b><br/>
The app can be found on iPhones or Androids in their application stores under the name E.M.M.A. for the Apple App store, and EMMA for the Google Play store. There are links below for your convenience of downloading. 
<br/>
App Store (iPhone) <a href="https://apps.apple.com/us/app/e-m-m-a/id1356773163">https://apps.apple.com/us/app/e-m-m-a/id1356773163</a>
<br/>
Google Play (Android) <a href="https://play.google.com/store/apps/details?id=com.thinksafe.emma&hl=en_US">https://play.google.com/store/apps/details?id=com.thinksafe.emma&hl=en_US</a>
<br/><br/> 
Thank you for making minutes matter!
<br/><br/> 
If you have any questions or need technical support please email <a href="mailto:emma@think-safe.com">emma@think-safe.com</a> or call 319-377-5125 from 8-5pm Central Time or leave a message for a return call by our tech support team.
<br/><br/>
<a href="https://www.emmaadmin.com/product_sheets.php"><b><i>What is EMMA?</i></b></a>
<br/><br/>
Think Safe, Inc.<br/>
<a href="https://www.think-safe.com">www.think-safe.com</a><br/>
The Creators of EMMA<br/>
A First Voice app<br/>
</p>
<hr><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><div class="comments" style="white-space:pre-line"></div></th><th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></tbody></table></th><th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></table></th></tr></tbody></table></td></tr></tbody></table></center></td></tr></table><!-- prevent Gmail on iOS font size manipulation --><div style="display:none;white-space:nowrap;font:15px courier;line-height:0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></body></html>';

        $bodyText .= 'You can log in to EMMA Admin. Login at - ' . $link;
      }

      //$body = 'test';
      //$bodyText = 'test';
//      $transport = Swift_SmtpTransport::newInstance($SMT);
      $mailer = Swift_Mailer::newInstance($SMT);
      $swiftMessage = Swift_Message::newInstance()
        ->setSubject('Your EMMA user access is confirmed & ready!')
        ->setFrom(array('donotreply@emmaadmin.com' => 'EMMA Admin'))
        ->setTo(array(
          $username['username'] => $username['firstname'] . ' ' .
            $username['lastname']))
        ->setBody($body, 'text/html')
        ->addPart($bodyText, 'text/plain');
      $result = $mailer->send($swiftMessage);
      $data['email-result'] = $result;
    }

}

$data['success'] = empty($errors);
$data['errors'] = $errors;
echo json_encode($data);