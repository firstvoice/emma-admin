<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 12/19/2016
 * Time: 15:01
 */
include('../include/db.php');
require('../vendor/php-jwt-master/src/JWT.php');
require('../vendor/php-jwt-master/src/BeforeValidException.php');
require('../vendor/php-jwt-master/src/ExpiredException.php');
require('../vendor/php-jwt-master/src/SignatureInvalidException.php');
$jwt = $fvmdb->real_escape_string($_POST['jwt']);
$securityId = $fvmdb->real_escape_string($_POST['security-id']);
$CONFIG = json_decode(file_get_contents('../config/config.json'));

$USER = null;
try {
  $token = Firebase\JWT\JWT::decode($jwt, $CONFIG->key,
    array('HS512'));
} catch (\Firebase\JWT\BeforeValidException $bve) {
  //echo $bve->getMessage();
  header('Location: ../index.php');
} catch (\Firebase\JWT\ExpiredException $ee) {
  //echo $ee->getMessage();
  header('Location: ../index.php');
} catch (\Firebase\JWT\SignatureInvalidException $sie) {
  //echo $sie->getMessage();
  header('Location: ../index.php');
} catch (Exception $e) {
  //echo 'UNKNOWN EXCEPTION: ';
  //echo $e->getMessage();
  header('Location: ../index.php');
}
$USER = $token->data;
header('Content-Type: text/csv');
header('Content-Disposition: attachment;filename=call_report.csv');


$output = fopen('php://output', 'w');

fputcsv($output, array('Info'));

fputcsv($output, array('Info'));

fclose($output);
