<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/31/2017
 * Time: 12:35 PM
 */

require('../include/db.php');
include('../include/processing.php');

$data = array();
$errors = array();

$planId = $fvmdb->real_escape_string($_GET['plan-id']);

if (empty($planId)) $errors['plan-id'] = 'No plan id';

$data['sos'] = array();
if (empty($errors)) {
  $soses = select_getsosmap_soses($planId);
//      $fvmdb->query("
//    SELECT s.*, CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') AS user, u.phone, u.landline_phone, u.id as user_id
//    FROM emma_sos s
//    JOIN users u ON s.created_by_id = u.id AND u.emma_plan_id = '" . $planId . "'
//    WHERE s.closed_date IS NULL
//    AND s.cancelled_date IS NULL
//  ");
  while ($sos = $soses->fetch_assoc()) {
    $data['sos'][] = $sos;
  }
}
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);
