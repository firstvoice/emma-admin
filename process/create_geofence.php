<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/19/2017
 * Time: 9:40 AM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$id = $fvmdb->real_escape_string($_POST['id']);
$plan = $fvmdb->real_escape_string($_POST['plan-id']);
$createdBy = $fvmdb->real_escape_string($_POST['user']);
$name = $fvmdb->real_escape_string($_POST['name']);
$type = $fvmdb->real_escape_string($_POST['type']);
$main = $fvmdb->real_escape_string($_POST['main']);

$radius = $fvmdb->real_escape_string($_POST['radius']);
$clat = $fvmdb->real_escape_string($_POST['center-lat']);
$clng = $fvmdb->real_escape_string($_POST['center-lng']);

$nwlat = $fvmdb->real_escape_string($_POST['nw-lat']);
$nwlng = $fvmdb->real_escape_string($_POST['nw-lng']);
$nelat = $fvmdb->real_escape_string($_POST['ne-lat']);
$nelng = $fvmdb->real_escape_string($_POST['ne-lng']);
$swlat = $fvmdb->real_escape_string($_POST['sw-lat']);
$swlng = $fvmdb->real_escape_string($_POST['sw-lng']);
$selat = $fvmdb->real_escape_string($_POST['se-lat']);
$selng = $fvmdb->real_escape_string($_POST['se-lng']);

if (empty($name)){
    $errors['name'] = 'Name is required';
}
if (empty($type)){
    $errors['type'] = 'Type is required';
}


if (empty($errors)) {
    $create = insert_emma_geofence_locations($plan, $name, $type, $clat, $clng, $radius, $nwlat, $nwlng, $nelat, $nelng, $selat, $selng, $swlat, $swlng,
        $main, date('Y-m-d'), $createdBy);
//    $fvmdb->query("
//    INSERT INTO emma_geofence_locations (
//        plan_id,
//        fence_name,
//        type,
//        center_lat,
//        center_lng,
//        radius,
//        nw_corner_lat,
//        nw_corner_lng,
//        ne_corner_lat,
//        ne_corner_lng,
//        se_corner_lat,
//        se_corner_lng,
//        sw_corner_lat,
//        sw_corner_lng,
//        main_fence,
//        created_date,
//        created_by
//        )
//    VALUES
//        ('". $plan ."',
//        '" . $name . "',
//        '" . $type . "',
//        '" . $clat . "',
//        '" . $clng . "',
//        '" . $radius . "',
//        '" . $nwlat . "',
//        '" . $nwlng . "',
//        '" . $nelat . "',
//        '" . $nelng . "',
//        '" . $selat . "',
//        '" . $selng . "',
//        '" . $swlat . "',
//        '" . $swlng . "',
//        '" . $main . "',
//        '". date('Y-m-d') ."',
//        '". $createdBy ."'
//        )
//  ");
    if(!$create){
        $errors['creation'] = 'Error creating geofence';
    }

}

$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);