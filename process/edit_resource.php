<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/19/2017
 * Time: 9:40 AM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$planId = $fvmdb->real_escape_string($_POST['plan_id']);
$userId = $fvmdb->real_escape_string($_POST['user_id']);
$resourceId = $fvmdb->real_escape_string($_POST['id']);
$filename = $fvmdb->real_escape_string($_POST['file_name']);
$typeId = $fvmdb->real_escape_string($_POST['file_type']);
$siteId = $fvmdb->real_escape_string($_POST['site_id']);

if (empty($resourceId)) {
  $errors['resource-id'] = 'No Resource Id';
}

if (empty($errors)) {
  $updateResource = update_emmaResource_with_resourceID($resourceId, $siteId, $filename, $typeId);
//      $fvmdb->query("
//    UPDATE emma_resources
//    SET
//      emma_site_id = '" . $siteId . "',
//      file_name = '" . $filename . "',
//      emma_resource_type_id = '" . $typeId . "'
//    WHERE emma_resource_id = '" . $resourceId . "'
//  ");
  if(!$updateResource) {
    $errors['update'] = 'Error updating resource';
  }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);