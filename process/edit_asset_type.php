<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/4/2019
 * Time: 9:24 AM
 */



include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();


$asset_id = $fvmdb->real_escape_string($_POST['asset_type_id']);
$new_name = $fvmdb->real_escape_string($_POST['asset-name']);
$fillin = $fvmdb->real_escape_string($_POST['fillin']);
$deleting = $fvmdb->real_escape_string($_POST['deleting']); //Array of things being deleted.
$dropdown = $_POST['dropdown'];
$date = $_POST['date'];
$status = $_POST['status'];
$image = $_POST['Image'];

if (empty($asset_id)) {
    $errors['id'] = 'The ID could not be found';
}

if (empty($errors)) {
    //get the image id based on $image (src)
    $iconSrc = select_emmaAssetIcons_with_image($image);
//    $fvmdb->query("
//    SELECT eac.asset_icon_id
//    FROM emma_asset_icons as eac
//    WHERE eac.image = '".$image."'
//    ");
    $icon = $iconSrc->fetch_assoc();
    //insert
    update_assetTypes_name_and_iconID_with_assetTypeID($asset_id, $new_name, $icon['asset_icon_id']);
//    $fvmdb->query("
//        UPDATE emma_asset_types
//        SET name = '".$new_name."',
//        emma_asset_icon_id = '".$icon['asset_icon_id']."'
//        WHERE emma_asset_type_id = '".$asset_id."'
//    ");


    $RemoveArray = json_decode($_POST['Remove']);
    if(!empty($RemoveArray))
    {
        foreach($RemoveArray as $remove)
        {
            //Remove dropdowns.
            if($remove->name == 'fillin') //DATES ARE SET AS FILLINS TOO.
            {
                //remove
                update_assetTypeDisplay_active_with_assetTypeDisplayID($remove->id, 0);
//                $fvmdb->query("
//                UPDATE emma_asset_type_display
//                SET active = 0
//                WHERE id = '".$remove->id."'
//                ");
            }
            elseif($remove->name == 'option')
            {
                update_assetDropdownOption_active_with_assetDropdownOptionID($remove->id, 0);
//                $fvmdb->query("
//                UPDATE emma_assets_dropdown_options
//                SET display = 0
//                WHERE id = '".$remove->id."'
//                ");

                //delete parent too (set inactive)
                update_assetTypeDisplay_active_with_assetTypeDisplayID($remove->parentID, 0);
//                $fvmdb->query("
//                UPDATE emma_asset_type_display
//                SET active = 0
//                WHERE id='".$remove->parentID."'
//                ");

            }
        }
    }


    $OptionArray = json_decode($_POST['Options']);
    $HeaderArray = json_decode($_POST['Headers']);
    if(!empty($HeaderArray))
    {
        $UnusedArray = json_decode($_POST['Unused']);
        $IDArray = array();
        foreach($HeaderArray as $header)
        {
            if(empty($header->id))
            {
                //This means it is a new dropdown entry that needs added.
                foreach($OptionArray as $option)
                {
                    //Loop through the dropdown options that need added and look for matching
                    //childOption and parentHeader values.
                    if($option->name == 'newOption')
                    {
                        //This is one we are looking for.
                        if($option->parentHeader == $header->childOption)
                        {
                            //Matching. Insert Options first
                            insert_emma_assets_dropdown_options($asset_id, $option->value, 1);
//                            $fvmdb->query("
//                            INSERT INTO emma_assets_dropdown_options
//                            (asset_type_id, Name, display)
//                            VALUES(
//                            '".$asset_id."',
//                            '".$option->value."',
//                            1
//                            )
//                            ");
                            $last_option_inserted = $emmadb->insert_id;
                            insert_emma_asset_type_display($asset_id, $header->value, 0, $last_option_inserted, 0, 1);
//                            $fvmdb->query("
//                            INSERT INTO emma_asset_type_display
//                            (asset_type_id, name, date, dropdown_id, fillin)
//                            VALUES(
//                            '".$asset_id."',
//                            '".$header->value."',
//                            0,
//                            '".$last_option_inserted."',
//                            0
//                            )
//                            ");


                        }
                    }
                }



            }
            else {
                foreach ($UnusedArray as $unused) {
                    if($unused->value != $header->value) {
                        //Names don't match (User might have edited the names, so we need to see if we need to update.
                        if ($unused->value == $header->originalname) {
                            //Need to update multiple fields. Match found, and names are different (been edited)
                            array_push($IDArray, $unused->parentid); //Array of IDS that we need to change based on new name.
                        }
                    }
                }

                //Here we use our IDArray and update names
                foreach ($IDArray as $ID) {
                    update_assetTypeDisplay_name_with_assetTypeDisplayID($ID, $header->value);
//                    $fvmdb->query("
//                UPDATE emma_asset_type_display
//                SET name = '" . $header->value . "'
//                WHERE id = '" . $ID . "'
//                ");
                }
                $IDArray = array(); //Clear.
            }
        }

    }
    if(!empty($OptionArray))
    {
        foreach($OptionArray as $option){
            if(!empty($option->id))
            {
                //Updating one that already existed.
                update_assetDropdownOption_name_with_assetDropdownOptionID($option->id, $option->value);
//                $fvmdb->query("
//                UPDATE emma_assets_dropdown_options
//                SET Name = '".$option->value."'
//                WHERE id= '".$option->id."'
//                ");
            }
            else{
                insert_emma_assets_dropdown_options($asset_id, $option->value, 1);
//                $fvmdb->query("
//                INSERT INTO emma_assets_dropdown_options
//                (asset_type_id, Name, display)
//                VALUES
//                (
//                 '".$asset_id."',
//                 '".$option->value."',
//                 1
//                )
//                ");
                $last_inserted = $emmadb->insert_id;
                //Grab a reference of the name of what we should call it from the DB table.
                $getName = select_assetTypeDisplay_with_assetTypeDisplayID($option->parentid);
//                $fvmdb->query("
//                SELECT eatd.name
//                FROM emma_asset_type_display as eatd
//                WHERE eatd.id = '".$option->parentid."'
//                ");
                while($name = $getName->fetch_assoc()) {
                    $data['assett'] = $asset_id;
                    $data['namee'] = $name['name'];
                    $data['last_inserted'] = $last_inserted;
                    insert_emma_asset_type_display($asset_id, $name['name'], 0, $last_inserted, 0, 1);
//                    $fvmdb->query("
//                INSERT INTO emma_asset_type_display
//                (asset_type_id,
//                name,
//                date,
//                dropdown_id,
//                fillin,
//                active)
//                VALUES
//                (
//                '" . $asset_id . "',
//                '" . $name['name'] . "',
//                0,
//                '" . $last_inserted . "',
//                0,
//                1
//                )
//                ");
                    //Runs only once!
                    break;
                }
            }
        };
    }
    $UpdateArray = json_decode($_POST['Update']);
    if(!empty($UpdateArray))
    {
        foreach($UpdateArray as $update)
        {
            if($update->name == 'fillin' || $update->name == 'date') {
                //Update table with new values.
                update_assetTypeDisplay_name_with_assetTypeDisplayID($update->id, $update->value);
//                $fvmdb->query("
//            UPDATE emma_asset_type_display
//            SET name = '" . $update->value . "'
//            WHERE id = '".$update->id."'
//            ");
            }
        }
    }




    foreach ($_POST['fillin'] as $key => $value) {
        insert_emma_asset_type_display($asset_id, $value, 0, null, 1, 1);
//        $fvmdb->query("
//        INSERT INTO emma_asset_type_display (
//        asset_type_id,
//        name,
//        date,
//        dropdown_id,
//        fillin
//        )
//        VALUES(
//        '".$asset_id."',
//        '".$value."',
//        0,
//        null,
//        1
//        )
//       ");
    }
    foreach ($_POST['date'] as $key => $value) {
        insert_emma_asset_type_display($asset_id, $value, 1, null, 0, 1);
//        $fvmdb->query("
//        INSERT INTO emma_asset_type_display (
//        asset_type_id,
//        name,
//        date,
//        dropdown_id,
//        fillin
//        )
//        VALUES(
//        '" . $asset_id . "',
//        '".$value."',
//        1,
//        null,
//        0
//        )
//        ");
    }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);