<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 4/6/2018
 * Time: 1:23 PM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$usernames = $fvmdb->real_escape_string($_POST['usernames']);

if (empty($usernames)) {
    $errors['usernames'] = 'Users not provided';
}

$userarr = explode(',',$usernames);

if (empty($errors)) {
    foreach($userarr as $username) {
        $users = select_user_from_username($username);
        $user = $users->fetch_assoc();
        $deleteUser = update_user_display_with_userID($user['id'], 'no');
//  $fvmdb->query("
//    update users
//    set
//      display = 'no'
//    where id = '" . $userId . "'
//  ");
        if (!$deleteUser) {
            $errors['sql'] = $fvmdb->error;
        }
    }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);