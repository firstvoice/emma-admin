<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/20/2017
 * Time: 11:17 AM
 */


include('../include/db.php');
include('../include/processing.php');

$emmaPlanId = $fvmdb->real_escape_string($_GET['emma-plan-id']);
$site_id = $fvmdb->real_escape_string($_GET['emma-site-id']);

$data = array();
$errors = array();

$eventMonths = select_getsitestatistics_eventMonths($emmaPlanId, $site_id);
//    $fvmdb->query("
//    SELECT year(e.created_date) AS year, month(e.created_date) AS month, count(*) AS count, sum(CASE WHEN e.drill = 1 THEN 1 ELSE 0 END) AS drill_count, sum(CASE WHEN e.drill = 0 THEN 1 ELSE 0 END) AS event_count
//    FROM emergencies e
//    JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
//    WHERE e.emma_plan_id = '" . $emmaPlanId . "'
//    AND e.emma_site_id = ". $site_id ."
//    GROUP BY year(e.created_date), month(e.created_date)
//");
while ($eventMonth = $eventMonths->fetch_assoc()) {
    $data['events-drills-months'][] = $eventMonth;
}

$data['success'] = empty($errors);
$data['errors'] = $errors;
echo json_encode($data);