<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/19/2017
 * Time: 9:40 AM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$user_id = $fvmdb->real_escape_string($_POST['id']);
$password = $fvmdb->real_escape_string($_POST['password']);


if (empty($user_id)) {
    $errors[] = 'empty id';
}

if (empty($password)) {
    $errors[] = 'empty password';
}

$confirmPass = select_confirm_user_password($user_id, $password);
//    $fvmdb->query("
//  select *
//  from users
//  where id = '" . $user_id . "'
//");
if (!$confirm = $confirmPass->fetch_assoc()) {
    $errors[] = 'Password incorrect, please re-enter current password';//Old Message:Could not authenticate password
}else{
    $data['id'] = $user_id;
}



$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);