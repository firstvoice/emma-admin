<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 5/29/2019
 * Time: 3:30 PM
 */

global $fvmdb;
global $USER;
include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$locationID = $fvmdb->real_escape_string($_POST['loc-id']);
$modelID = $fvmdb->real_escape_string($_POST['model-id']);
$serial = $fvmdb->real_escape_string($_POST['serial']);
$placement = $fvmdb->real_escape_string($_POST['placement']);
$address = $fvmdb->real_escape_string($_POST['address']);
$notes = $fvmdb->real_escape_string($_POST['notes']);
$latitude = $fvmdb->real_escape_string($_POST['latitude']);
$longitude = $fvmdb->real_escape_string($_POST['longitude']);
$pad = $fvmdb->real_escape_string($_POST['pad']);
$padLot = $fvmdb->real_escape_string($_POST['pad-lot']);
$padExpiration = $fvmdb->real_escape_string($_POST['pad-expiration']);
$pedPad = $fvmdb->real_escape_string($_POST['ped-pad']);
$pedPadLot = $fvmdb->real_escape_string($_POST['ped-pad-lot']);
$pedPadExpiration = $fvmdb->real_escape_string($_POST['ped-pad-expiration']);
$sparePad = $fvmdb->real_escape_string($_POST['spare-pad']);
$sparePadLot = $fvmdb->real_escape_string($_POST['spare-pad-lot']);
$sparePadExpiration = $fvmdb->real_escape_string($_POST['spare-pad-expiration']);
$sparePedPad = $fvmdb->real_escape_string($_POST['spare-ped-pad']);
$sparePedPadLot = $fvmdb->real_escape_string($_POST['spare-ped-pad-lot']);
$sparePedPadExpiration = $fvmdb->real_escape_string($_POST['spare-ped-pad-expiration']);
$pak = $fvmdb->real_escape_string($_POST['pak']);
$pakLot = $fvmdb->real_escape_string($_POST['pak-lot']);
$pakExpiration = $fvmdb->real_escape_string($_POST['pak-expiration']);
$pedPak = $fvmdb->real_escape_string($_POST['ped-pak']);
$pedPakLot = $fvmdb->real_escape_string($_POST['ped-pak-lot']);
$pedPakExpiration = $fvmdb->real_escape_string($_POST['ped-pak-expiration']);
$sparePak = $fvmdb->real_escape_string($_POST['spare-pak']);
$sparePakLot = $fvmdb->real_escape_string($_POST['spare-pak-lot']);
$sparePakExpiration = $fvmdb->real_escape_string($_POST['spare-pak-expiration']);
$sparePedPak = $fvmdb->real_escape_string($_POST['spare-ped-pak']);
$sparePedPakLot = $fvmdb->real_escape_string($_POST['spare-ped-pak-lot']);
$sparePedPakExpiration = $fvmdb->real_escape_string($_POST['spare-ped-pak-expiration']);
$battery = $fvmdb->real_escape_string($_POST['battery']);
$batteryLot = $fvmdb->real_escape_string($_POST['battery-lot']);
$batteryExpiration = $fvmdb->real_escape_string($_POST['battery-expiration']);
$spareBattery = $fvmdb->real_escape_string($_POST['spare-battery']);
$spareBatteryLot = $fvmdb->real_escape_string($_POST['spare-battery-lot']);
$spareBatteryExpiration = $fvmdb->real_escape_string($_POST['spare-battery-expiration']);
$accessory = $fvmdb->real_escape_string($_POST['accessory']);
$accessoryLot = $fvmdb->real_escape_string($_POST['accessory-lot']);
$accessoryExpiration = $fvmdb->real_escape_string($_POST['accessory-expiration']);

if (empty($locationID)) $errors['location-id'] = 'No Location selected';
if (empty($modelID)) $errors['model-id'] = 'No Brand selected';
if (empty($serial)) $errors['serial'] = 'No Serial';
if (empty($address)) $errors['address'] = 'No Address';
if (empty($latitude) ||
  empty($longitude)) $errors['location'] = 'The latitude and or longitude was not processed';

if (empty($errors)) {
  //update_emmaAsset_details_with_assetID($id, $type_id, $latitude, $longitude, $address, $status);
  $insert = $fvmdb->query("
    INSERT INTO aeds (
        aed_model_id,
        serialnumber,
        installdate,
        sitecoordinator,
        purchasetype,
        purchasedate,
        warranty,
        location,
        plantype,
        plandate,
        planexpiration,
        current,
        lastcheck,
        notes,
        location_id,
        created_by_id,
        creation,
        updated,
        address,
        latitude,
        longitude,
        display
    ) VALUES (
        '".$modelID."',
        '".$serial."',
        '".date('Y-m-d H:i:s', time())."',
        '',
        '',
        '".date('Y-m-d H:i:s', time())."',
        '',
        '".$placement."',
        'none',
        '',
        '',
        0,
        '".date('Y-m-d H:i:s', time())."',
        '".$notes."',
        '".$locationID."',
        '".$USER->id."',
        '".date('Y-m-d H:i:s', time())."',
        '".date('Y-m-d H:i:s', time())."',
        '".$address."',
        '".$latitude."',
        '".$longitude."',
        '1'
    )
   ");
  if(!$insert) {
    $errors['insert'] = $fvmdb->error;
  } else {
    $aedId = $fvmdb->insert_id;
    if(isset($pad) AND $pad != "") {
      $insertNewPad = $fvmdb->query("
        INSERT INTO aed_pads
        (
         aed_id, aed_pad_type_id, expiration, lot, udi, spare, `default`, display, updated
        ) VALUES (
          '".$aedId."',
          '".$pad."',
          '".$padExpiration."',
          '".$padLot."',
          '',
          '0',
          '',
          '1',
          '".date('Y-m-d H:i:s', time())."'
        )
      ");
    }
    if(isset($pedPad) AND $pedPad != "") {
      $insertNewPedPad = $fvmdb->query("
        INSERT INTO aed_pads
        (
         aed_id, aed_pad_type_id, expiration, lot, udi, spare, `default`, display, updated
        ) VALUES (
          '".$aedId."',
          '".$pedPad."',
          '".$pedPadExpiration."',
          '".$pedPadLot."',
          '',
          '0',
          '',
          '1',
          '".date('Y-m-d H:i:s', time())."'
        )
      ");
    }
    if(isset($sparePad) AND $sparePad != "") {
      $insertNewSparePad = $fvmdb->query("
        INSERT INTO aed_pads
        (
         aed_id, aed_pad_type_id, expiration, lot, udi, spare, `default`, display, updated
        ) VALUES (
          '".$aedId."',
          '".$sparePad."',
          '".$sparePadExpiration."',
          '".$sparePadLot."',
          '',
          '1',
          '',
          '1',
          '".date('Y-m-d H:i:s', time())."'
        )
      ");
    }
    if(isset($sparePedPad) AND $sparePedPad != "") {
      $insertNewSparePedPad = $fvmdb->query("
        INSERT INTO aed_pads
        (
         aed_id, aed_pad_type_id, expiration, lot, udi, spare, `default`, display, updated
        ) VALUES (
          '".$aedId."',
          '".$sparePedPad."',
          '".$sparePedPadExpiration."',
          '".$sparePedPadLot."',
          '',
          '1',
          '',
          '1',
          '".date('Y-m-d H:i:s', time())."'
        )
      ");
    }
    if(isset($pak) AND $pak != "") {
      $insertNewPak = $fvmdb->query("
        INSERT INTO aed_paks
        (
         aed_id, aed_pak_type_id, expiration, lot, udi, spare, `default`, display, updated
        ) VALUES (
          '".$aedId."',
          '".$pak."',
          '".$pakExpiration."',
          '".$pakLot."',
          '',
          '0',
          '',
          '1',
          '".date('Y-m-d H:i:s', time())."'
        )
      ");
    }
    if(isset($pedPak) AND $pedPak != "") {
      $insertNewPedPak = $fvmdb->query("
        INSERT INTO aed_paks
        (
         aed_id, aed_pak_type_id, expiration, lot, udi, spare, `default`, display, updated
        ) VALUES (
          '".$aedId."',
          '".$pedPak."',
          '".$pedPakExpiration."',
          '".$pedPakLot."',
          '',
          '0',
          '',
          '1',
          '".date('Y-m-d H:i:s', time())."'
        )
      ");
    }
    if(isset($sparePak) AND $sparePak != "") {
      $insertNewSparePak = $fvmdb->query("
        INSERT INTO aed_paks
        (
         aed_id, aed_pak_type_id, expiration, lot, udi, spare, `default`, display, updated
        ) VALUES (
          '".$aedId."',
          '".$sparePak."',
          '".$sparePakExpiration."',
          '".$sparePakLot."',
          '',
          '1',
          '',
          '1',
          '".date('Y-m-d H:i:s', time())."'
        )
      ");
    }
    if(isset($sparePedPak) AND $sparePedPak != "") {
      $insertNewSparePedPak = $fvmdb->query("
        INSERT INTO aed_paks
        (
         aed_id, aed_pak_type_id, expiration, lot, udi, spare, `default`, display, updated
        ) VALUES (
          '".$aedId."',
          '".$sparePedPak."',
          '".$sparePedPakExpiration."',
          '".$sparePedPakLot."',
          '',
          '1',
          '',
          '1',
          '".date('Y-m-d H:i:s', time())."'
        )
      ");
    }
    if(isset($battery) AND $battery != "") {
      $insertNewBattery = $fvmdb->query("
        INSERT INTO aed_batteries
        (
         aed_id, aed_battery_type_id, expiration, lot, udi, spare, `default`, display, updated
        ) VALUES (
          '".$aedId."',
          '".$battery."',
          '".$batteryExpiration."',
          '".$batteryLot."',
          '',
          '0',
          '',
          '1',
          '".date('Y-m-d H:i:s', time())."'
        )
      ");
    }
    if(isset($spareBattery) AND $spareBattery != "") {
      $insertNewSpareBattery = $fvmdb->query("
        INSERT INTO aed_batteries
        (
         aed_id, aed_battery_type_id, expiration, lot, udi, spare, `default`, display, updated
        ) VALUES (
          '".$aedId."',
          '".$spareBattery."',
          '".$spareBatteryExpiration."',
          '".$spareBatteryLot."',
          '',
          '1',
          '',
          '1',
          '".date('Y-m-d H:i:s', time())."'
        )
      ");
    }
    if(isset($accessory) AND $accessory != "") {
      $insertNewAccessory = $fvmdb->query("
        INSERT INTO aed_accessories
        (
         aed_id, aed_accessory_type_id, expiration, lot, udi, spare, display, updated
        ) VALUES (
          '".$aedId."',
          '".$accessory."',
          '".$accessoryExpiration."',
          '".$accessoryLot."',
          '',
          '0',
          '1',
          '".date('Y-m-d H:i:s', time())."'
        )
      ");
    }
  }
}

$data['post'] = $_POST;
$data['success'] = empty($errors);
$data['errors'] = $errors;

echo json_encode($data);