<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 5/24/2019
 * Time: 9:36 AM
 */
require('../include/db.php');
include('../include/processing.php');
$response = $fvmdb->real_escape_string($_POST['response']);
$comment = $fvmdb->real_escape_string($_POST['comments']);
$userId = $fvmdb->real_escape_string($_POST['user_id']);
$pin = $fvmdb->real_escape_string($_POST['pin']);
$emergency_Id = $fvmdb->real_escape_string($_POST['emergency_id']);
$latitude = $fvmdb->real_escape_string($_POST['latitude']);
$longitude = $fvmdb->real_escape_string($_POST['longitude']);

$desktop_connection = $fvmdb->real_escape_string($_POST['desktopConnection']); //This is so we can differentiate between someone using the download client vs the web browser.
$auth = $fvmdb->real_escape_string($_POST['auth']); //This is the auth code that the downloadable client sends. For security the userId is not saved so we go by auth code.
$data = array();
$errors = array();


if($response == '')
{
    $errors['No Response'] = 'A response status needs to be selected';
}


if(empty($errors))
{
    if(empty($desktop_connection)) {
        //Regular browser
        insert_emergency_responses($emergency_Id, $userId, $response, $comment, 0, date("Y-m-d H:m:s"), date("Y-m-d H:m:s"),
            $latitude, $longitude, 0, $pin, null, null);
//        $fvmdb->query("
//    INSERT INTO emergency_responses
//    (
//    emergency_id,
//    user_id,
//    status,
//    comments,
//    mobile,
//    created_date,
//    updated_date,
//    latitude,
//    longitude,
//    device_type,
//    pin,
//    validated_id,
//    validated_time
//    )
//    VALUES(
//    '" . $emergency_Id . "',
//    '" . $userId . "',
//    '" . $response . "',
//    '" . $comment . "',
//    0,
//    '" . date("Y/m/d H/m/s") . "',
//    '" . date("Y/m/d H/m/s") . "',
//    '" . $latitude . ",
//    '" . $longitude . ",
//    0,
//    '" . $pin . "',
//    null,
//    null
//    )
//    ");
    }
    else{
        //Connection by the downloadable client
        $users = select_userANDinfoOnly_from_auth($auth);
//        $fvmdb->query("
//            SELECT u.id, eg.info_only
//            FROM users u
//            LEFT JOIN emma_user_groups as eug on u.id = eug.user_id
//            LEFT JOIN emma_groups as eg on u.emma_plan_id = eg.emma_plan_id AND eg.emma_group_id = eug.emma_group_id
//            WHERE u.auth = '".$auth."'
//            ");
        if($users->num_rows > 0) {
            $user = $users->fetch_assoc();
            if ($user['info_only'] == 0) {
                $insert = insert_emergency_responses($emergency_Id, $user['id'], $response, $comment, 0, date("Y/m/d H/m/s"), date("Y/m/d H/m/s"),
                    $latitude, $longitude, 0, $pin, null, null);
//                    $fvmdb->query("
//                    INSERT INTO emergency_responses
//                    (
//                    emergency_id,
//                    user_id,
//                    status,
//                    comments,
//                    mobile,
//                    created_date,
//                    updated_date,
//                    latitude,
//                    longitude,
//                    device_type,
//                    pin,
//                    validated_id,
//                    validated_time
//                    )
//                    VALUES(
//                    '" . $emergency_Id . "',
//                    '" . $user['id'] . "',
//                    '" . $response . "',
//                    '" . $comment . "',
//                    0,
//                    '" . date("Y-m-d H:i:s") . "',
//                    '" . date("Y-m-d H:i:s") . "',
//                    '" . $latitude . "',
//                    '" . $longitude . "',
//                    0,
//                    '" . $pin . "',
//                    null,
//                    null
//                    )
//                ");
                if ($insert) {
                    echo 'Success'; //This echo statement gets sent back to the client.
                } else {
                    echo $errors['Query Failed'] = "ERRORThere was a problem submitting your response";
                }
            }
            else{
                $errors['Info Only!'] = 'ERRORThis account is not allowed to create responses.';
            }
        }
        else {
            $errors['AUTH Invalid'] = 'ERRORYour account could not be authenticated!';
        }

        foreach ($errors as $error) {
            echo $error;
        }


    }
}


$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
if(empty($desktop_connection)){echo json_encode($data);};