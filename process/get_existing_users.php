<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 8/28/2017
 * Time: 10:04 AM
 */

include('../include/db.php');
include('../include/processing.php');

$data = array();

$username = $emmadb->real_escape_string($_POST['username']);
$adminuser = $emmadb->real_escape_string($_POST['user']);
$exists = false;
$in = false;
$inplan = false;

if(!empty($username)) {
    $users = select_user_from_username($username);
    $adminplans = select_multiPlans_with_userID($adminuser);
    while($adminplan = $adminplans->fetch_assoc()) {
        $plansarray[] = $adminplan['plan_id'];
    }
    if($users->num_rows > 0){
        $exists = true;
        $user = $users->fetch_assoc();
        $userplanarr = array();
        $userplans = select_multiPlans_with_userID($user['id']);
        while($userplan = $userplans->fetch_assoc()){
            $userplanarr[] = $userplan['plan_id'];
        }
        if($user['active'] == 0) {
            $data['inactive'] = true;
            for ($p = 0; $p < count($plansarray); $p++) {
                if (in_array($plansarray[$p], $userplanarr)) {
                    $inplan = true;
                }
            }
            if($inplan == false){
                $data['deleted'] = false;
            }else{
                $data['deleted'] = true;
            }
        }else {
            $data['inactive'] = false;
            for ($p = 0; $p < count($plansarray); $p++) {
                if (in_array($plansarray[$p], $userplanarr)) {
                    $inplan = true;
                }
            }
            if($inplan == false){
                $data['move'] = true;
            }else{
                $data['move'] = false;
            }
        }
    }
}
$data['exists'] = $exists;
$data['creatorplans'] = $plansarray;
$data['userplans'] = $userplanarr;
$data['user'] = $user;
echo json_encode($data);
