<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/20/2017
 * Time: 12:28 PM
 */

include('../include/db.php');
include('../include/processing.php');

$errors = array();
$data = array();

$planId = $fvmdb->real_escape_string($_POST['plan-id']);

if (empty($errors)) {

}

$data['success'] = empty($errors);
$data['errors'] = $errors;
$data['post'] = $_POST;
echo json_encode($data);