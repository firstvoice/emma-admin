<?php include('include/pre_login_header.php'); ?>
<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Login</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #FFFFFF;">


<?php include('include/index_top_bar.php'); ?>

<div class="row expanded the-clock" style="height: 100%">
    <div class="large-12 medium-12 small-12 columns" style="padding-top: 20vh;">
<!--        <div class="row expanded" style="padding-bottom: 1em">-->
<!--            <div class="large-6 medium-12 small-12 columns" style="text-align:center; padding-top: 2em">-->
<!--                <p>Become an EMMA advocate for a safe environment by requesting your EMMA Advocate Starter Kit below. This-->
<!--                    kit includes a flyer and social media graphics that you can use to share the importance of keeping-->
<!--                    our environment protected in active shooter situations. Share with your community, bring to your next-->
<!--                    board meeting, and send to your other concerned individuals.</p>-->
<!--            </div>-->
<!--        </div>-->
        <div class="row expanded" style="padding-bottom: 1em">
            <div class="large-6 medium-12 small-12 columns" style="text-align:center; padding-top: 2em">
                <div class="row expanded" style="padding-bottom: 1em">
                    <div class="large-offset-1 large-3 medium-6 small-6 column" style="text-align: center;">
                        <div class="clear_card">
                            <a data-open="become-advocate-modal" class="typebutton"
                               data-type="Become a Advocate - Partners"><i class="fas fa-user-shield fa-3x"></i>
                            <div class="card_section">
                                <p>Become an Advocate</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="large-offset-1 large-3 medium-6 small-6 column" style="text-align: center;">
                        <div class="clear_card">
                            <a data-open="become-partner-modal" class="typebutton"
                               data-type="Become A Partner - Partners"><i class="fas fa-handshake fa-3x"></i>
                            <div class="card_section">
                                <p>Become a Partner</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="large-offset-1 large-3 medium-6 small-6 column" style="text-align: center;">
                        <div class="clear_card">
                            <a data-open="find-partner-modal" class=" typebutton" data-type="Find A Partner - Partners"><i
                                        class="fas fa-search fa-3x"></i>
                            <div class="card_section">
                                <p>Find a Partner</p>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div><p>&nbsp;</p><br/><br/><br/><br/><br/><br/><br/></div>
</div>

<?php include('include/footer.php'); ?>
</div>
<?php include('include/pre_login_modals.php'); ?>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>


