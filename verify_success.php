<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/14/2017
 * Time: 6:55 PM
 */

session_start();
require('include/db.php');
$USER = array();
$content = 'verify_success';

?>

<html class="no-js" lang="en">
<head>
  <title>EMMA | User Setup Success</title>
  <?php include('include/head.php'); ?>
  <link rel="stylesheet" href="css/<?php echo $content; ?>.css"/>

</head>
<body>
<div class="app-dashboard shrink-large">
  <div class="row expanded app-dashboard-top-nav-bar">
    <div class="columns medium-2">
      <button data-toggle="app-dashboard-sidebar"
        class="menu-icon hide-for-medium"></button>
      <a href="./dashboard.php?content=home" class="app-dashboard-logo"><img src="img/emma_icon.png" style="width:20px;height:20px;" /> EMMA</a>
    </div>
    <div class="columns show-for-medium">
    </div>
  </div>
  <div class="app-dashboard-body">
    <div class="app-dashboard-body-content"
      data-off-canvas-content>
      <div class="content-container-div">
        <div class="content-body-div">
          <div class="row expanded align-center">
            <div class="large-6 small-12 align-center column">
              <h1 class="text-center" style="margin:2rem;">User Setup Success</h1>
              <h3 class="text-center" style="margin:2rem;">Please check your Email.</h3>
            </div>
          </div>
        </div>
        <div class="content-footer-div">
          <div class="text-center">
            <p>Copyright Think Safe, Inc. 2017 Patent Pending</p>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

</body>

<?php include('include/scripts.php'); ?>
<script src="js/<?php echo $content; ?>.js"></script>


</html>