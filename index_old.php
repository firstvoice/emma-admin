<?php
session_start();
include('include/db.php');
require('vendor/php-jwt-master/src/JWT.php');
require('vendor/php-jwt-master/src/BeforeValidException.php');
require('vendor/php-jwt-master/src/ExpiredException.php');
require('vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('config/config.json'));

$errorMessage = '';
if (isset($_COOKIE['jwt'])) {
  try {
    $token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key,
      array('HS512'));
    header('Location: dashboard.php');
  } catch (\Firebase\JWT\BeforeValidException $bve) {
    $errorMessage = $bve->getMessage();
  } catch (\Firebase\JWT\ExpiredException $ee) {
    $errorMessage = $ee->getMessage();
  } catch (\Firebase\JWT\SignatureInvalidException $sie) {
    $errorMessage = $sie->getMessage();
  } catch (Exception $e) {
    $errorMessage = $e->getMessage();
  }
}
if (isset($_POST['login']) && !empty($_POST['username'])) {
  try {
    $username = $fvmdb->real_escape_string($_POST['username']);
    $password = $fvmdb->real_escape_string($_POST['password']);
    $users = $fvmdb->query("
        SELECT u.id, CONCAT(u.firstname, ' ', u.lastname) AS full_name, u.username, u.emma_plan_id, u.emma_pin, u.auth, u.emma_code_generator
        FROM users u
        WHERE u.username = '" . $username . "'
        AND u.password = PASSWORD('" . $password . "')
        AND u.emma_plan_id IS NOT NULL
    ");
    if ($users->num_rows > 0) {
      $user = $users->fetch_assoc();

      $admin = false;
      $security = false;
      $groups = $fvmdb->query("
        select g.*
        from emma_user_groups ug
        join emma_groups g on ug.emma_group_id = g.emma_group_id
        where ug.user_id = '".$user['id']."'
      ");
      while ($group = $groups->fetch_assoc()) {
        if($group['admin']) $admin = true;
        if($group['security']) $security = true;
        if($group['911admin']) $admin911 = true;
      }

//      if ($admin || $security) {
        $tokenId = base64_encode(randomToken(32));
        $issuedAt = time();
        $notBefore = $issuedAt;
        $expire = $notBefore + (10 * 365 * 24 * 60 * 60);
        $serverName = 'tsdemos.com';

        $data = [
          'iat' => $issuedAt,
          'jti' => $tokenId,
          'iss' => $serverName,
          'nbf' => $notBefore,
          'exp' => $expire,
          'data' => [
            'id' => $user['id'],
            'full_name' => $user['full_name'],
            'username' => $user['username'],
            'emma_plan_id' => $user['emma_plan_id'],
            'emma_pin' => $user['emma_pin'],
            'auth' => $user['auth'],
            'privilege' => [
              'admin' => $admin,
              'security' => $security,
              'admin911' => $admin911,
              'code_generator' => $user['emma_code_generator'] == 1
            ]
          ]
        ];

        $jwt = Firebase\JWT\JWT::encode($data, $CONFIG->key, 'HS512');
        setcookie('jwt', $jwt);
        header('Location: dashboard.php');
//      } else {
//        $errorMessage = 'User is not in an Admin or Security group';
//      }
    } else {
      $errorMessage = 'Invalid Username or Password';
    }
  } catch (Exception $e) {
    $e->getMessage();
  }
}
?>

<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
  <title>EMMA Login</title>
  <?php include('include/head.php'); ?>
  <link rel="stylesheet" href="css/index.css"/>
  <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
</head>

<body>
<div id="main" class="row">
  <div class="large-4 medium-2 small-1 columns"></div>
  <div class="large-4 medium-8 small-10 columns border">
    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>"
      method="post">
      <div class="row">
        <div class="medium-12 columns">
          <h3 class="center text text-center">EMMA</h3>
        </div>
      </div>
      <div class="row">
        <div class="medium-12 columns">
          <label class="text">Username
            <input type="text" name="username" id="username"/>
          </label>
        </div>
      </div>
      <div class="row">
        <div class="medium-12 columns">
          <label class="text">Password
            <input type="password" name="password" id="password"/>
          </label>
        </div>
      </div>
<!--        <div class="row">-->
<!--            <div class="medium-8 columns " style="padding-top: .25rem;">-->
<!--                Remember Me-->
<!--            </div>-->
<!--            <div class="switch tiny columns">-->
<!--                <input class="switch-input" id="remember" type="checkbox">-->
<!--                <label class="switch-paddle" style="float: right;" for="remember">-->
<!--                    <span class="show-for-sr"></span>-->
<!--                </label>-->
<!--            </div>-->
<!--        </div>-->
      <?php if (isset($errorMessage) && $errorMessage) echo '<div class="callout small alert alert-message">' .
        $errorMessage . '</div>'; ?>
      <div class="row">
        <div class="medium-12 columns">
          <input class="button expanded" type="submit" name="login" id="login"
            value="Login"/>
        </div>
      </div>
    </form>
  </div>
  <div class="large-4 medium-2 small-1 columns"></div>
</div>


</body>

</html>
