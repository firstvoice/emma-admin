<?php include('include/pre_login_header.php'); ?>
<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Technical Sheets</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #FFFFFF;">


<?php include('include/index_top_bar.php'); ?>

<div class="row expanded the-clock">
    <div class="large-12 medium-12 small-12 columns" style="padding-bottom: 2em;height: auto;">
    </div>
    <div class="large-12 medium-12 small-12 columns">
        <div class="row expanded" style="padding-bottom: 1em;text-align: center;">
            <div class="large-4 medium-6 small-6 column">
                <div class="clear_card">
                    <a target="_blank" href="documents/Emma_dispatch_ARCH_Diagram.pdf"><img
                                src="img/Emma_dispatch_ARCH_Diagram.jpg"
                                width="50%" height="50%"/>
                        <div class="card_section">
                        <p>Dispatch Architectural Diagram</p>
                        </div>
                    </a>
                </div>
            </div>

            <div class="large-4 medium-6 small-6 column">
                <div class="clear_card">
                    <a target="_blank" href="documents/Emma_EMAIL_ARCH_Diagram.pdf"><img
                                src="img/Emma_EMAIL_ARCH_Diagram.jpg"
                                width="50%" height="50%"/>
                        <div class="card_section">
                            <p>Email Architectural Diagram</p>
                        </div>
                    </a>
                </div>
            </div>

            <div class="large-4 medium-6 small-6 column">
                <div class="clear_card">
                    <a target="_blank" href="documents/Emma_SMS_ARCH_Diagram.pdf"><img
                                src="img/Emma_SMS_ARCH_Diagram.jpg"
                                width="50%" height="50%"/>
                        <div class="card_section">
                            <p>SMS Architectural Diagram</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div><p>&nbsp;</p><br/><br/><br/><br/><br/><br/><br/></div>
</div>
<?php include('include/footer.php'); ?>
<?php include('include/pre_login_modals.php'); ?>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>


