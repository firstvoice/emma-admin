<?php include('include/pre_login_header.php'); ?>
<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Login</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #FFFFFF;">


<?php include('include/index_top_bar.php'); ?>

<div class="row expanded the-clock" style="height: 100%">
    <div class="large-12 medium-12 small-12 columns">
        <div class="large-12 medium-12 small-12 columns" style="padding-bottom: 2em;">
        </div>
        <div class="large-6 medium-12 small-12 columns">
           <!-- <div class="row small-up-2 medium-up-3">-->
            <div class="row expanded" style="padding-bottom: 1em">
               <div class="large-4 medium-4 small-4 column">
                    <div class="clear_card">
                        <a href="business.php"><i class="fas fa-briefcase fa-3x"></i>
                            <div class="card_section">
                            <p>Business</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="large-4 medium-4 small-4 column">
                    <div class="clear_card">
                        <a href="community.php"><i class="fas fa-city fa-3x"></i>
                           <div class="card_section">
                            <p>Community</p>
                           </div>
                        </a>
                    </div>
                </div>

                <div class="large-4 medium-4 small-4 column">
                    <div class="clear_card">
                        <a href="education.php"><i class="fas fa-graduation-cap fa-3x"></i>
                        <div class="card_section">
                            <p>Education</p>
                        </div>
                        </a>
                    </div>
               </div>
            </div>
            <div class="row expanded" style="padding-bottom: 1em">
            <!--<div class="row small-up-2 medium-up-3">-->
               <div class="large-4 medium-4 small-4 column">
                    <div class="clear_card">
                        <a href="family.php"><i class="fas fa-home fa-3x"></i>
                        <div class="card_section">
                            <p>Family</p>
                        </div>
                        </a>
                    </div>
               </div>

               <div class="large-4 medium-4 small-4 column">
                    <div class="clear_card">
                        <a href="healthcare.php"><i class="fas fa-hospital fa-3x"></i>
                        <div class="card_section">
                            <p>Healthcare</p>
                        </div>
                        </a>
                    </div>
                </div>

               <div class="large-4 medium-4 small-4 column">
                    <div class="clear_card">
                        <a href="hospitality.php"><i class="fas fa-hotel fa-3x"></i>
                        <div class="card_section">
                            <p>Hospitality</p>
                        </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="row expanded" style="padding-bottom: 1em">
            <!--<div class="row small-up-2 medium-up-3">-->
                <div class="large-offset-2 large-4 medium-4 small-4 column">
                    <div class="clear_card">
                        <a href="public_safety.php"><i class="fas fa-user-shield fa-3x"></i>
                        <div class="card_section">
                            <p>Public Safety</p>
                        </div>
                        </a>
                    </div>
               </div>

                <div class="large-4 medium-4 small-4 column">
                    <div class="clear_card">
                        <a href="religion.php"><i class="fas fa-church fa-3x"></i>
                        <div class="card_section">
                            <p>Religion</p>
                        </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>


<?php include('include/footer.php'); ?>
</div>
<?php include('include/pre_login_modals.php'); ?>

</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>

