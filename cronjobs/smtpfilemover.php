<?php

use Dotenv\Dotenv;
include('../include/db.php');

require_once('../vendor/autoload.php');
$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

$dirPath = '/resources/emma/uploads/user_csv_files/';

$API_KEY = $_ENV['EV_KEY'];
$ACCESS_TOKEN = $_ENV['EV_TOKEN'];
$ACCOUNT_URL = $_ENV['ACCOUNT_URL'];

$resourcesApi = new ExaVault\Api\ResourcesApi(
    new GuzzleHttp\Client(),
    (new ExaVault\Configuration())->setHost($ACCOUNT_URL)
);

$downloadApi = new ExaVault\Api\DownloadApi(
    new GuzzleHttp\Client(),
    (new ExaVault\Configuration())->setHost($ACCOUNT_URL)
);


//--------------------------------------------------------------------------------------------------------------------
//loop through folder list
//--------------------------------------------------------------------------------------------------------------------

$folderlist = array();
$folderQuery = select_smpt_folders();
if($folderQuery->num_rows > 0){
    for($q=0; $q < $folderQuery->num_rows; $q++){
        $folderResult = $folderQuery->fetch_assoc();
        $folderlist[] = $folderResult['folder_name'];

        echo 'Folder Added to list: '. $folderResult['folder_name'].'<br/>';
    }
}

for($i=0;$i<count($folderlist);$i++) {
    $foldername = $folderlist[$i];


    try {
        $listResult = $resourcesApi->listResources($API_KEY, $ACCESS_TOKEN, "/" . $foldername, null, 0, null, 'file', '*.csv');

        if ($listResult->getReturnedResults() == 0) {
            echo "Found no files to download" . PHP_EOL;
            continue;
        } else {
            echo "Found " . $listResult->getReturnedResults() . " CSV files to download" . PHP_EOL;
        }

    } catch (Exception $e) {
        echo 'Exception when getting resources: ', $e->getMessage(), PHP_EOL;
        continue;
    }

    $downloads = [];
    $downloadIDs = [];
    $listed_files = $listResult->getData();
    foreach ($listed_files as $listed_file) {
        $downloads[] = "id:" . $listed_file->getId();
        $downloadIDs[] = $listed_file->getId();
        $downloadPaths[] = $listed_file->getAttributes()->getPath();
        echo ($listed_file->getAttributes()->getPath()) . PHP_EOL;
    }

    for($j=0;$j<count($downloads);$j++) {
        $fileID = $downloads[$j];
        $filePath = $downloadPaths[$j];


        try{
            $download = $resourcesApi->download($API_KEY, $ACCESS_TOKEN, $filePath);

            echo '<br/><br/>File Downloaded.';
            echo '<br/><br/>';
            echo $download;
            echo '<br/><br/>';

            if(dirname($_SERVER['DOCUMENT_ROOT']) == "/home/admin/public_html"){
                $saveFilePath = dirname($_SERVER['DOCUMENT_ROOT']).$dirPath.$foldername;
            }
            else{
                $saveFilePath = dirname($_SERVER['DOCUMENT_ROOT']).'/public_html'.$dirPath.$foldername;
            }

            $insertTime = (new \DateTime())->format("Y-m-d");
            $filename = $foldername . $insertTime . ".csv";

            //insert into database
            $check = insert_user_csv_upload($foldername, $filename, -1, 'smtp_mover', 0);
            if($check != null) {
                echo '<br/><br/>Upload Logged.';
            }
            else{
                echo '<br/><br/>insert Failed';
            }
            $uploadQuery = select_csv_upload_not_run($foldername, $filename, -1, 'smtp_mover');
            echo '<br/>Count: '. $uploadQuery->num_rows;
            if($uploadQuery->num_rows > 0) {
                $uploadResult = $uploadQuery->fetch_assoc();
                $uploadID = $uploadResult['upload_id'];
                insert_user_csv_upload_file($uploadID, $download);
                echo '<br/><br/>File Inserted.';
            }

            $saveLocation = $saveFilePath. "/". $filename;

//            chown($saveFilePath,'root');

//            $file = fopen($saveLocation, "w");
//            fwrite($file, $download);
//            fclose($file);

            file_put_contents($saveLocation, $download);

            echo '<br/><br/>File Saved.';

        }
        catch (Exception $e) {
            echo 'Exception when Downloading: ', $e->getMessage(), PHP_EOL;
            continue;
        }

        try{
            echo '<br/>';
            try {
                $create_folder = $resourcesApi->addFolder($API_KEY, $ACCESS_TOKEN, new \ExaVault\Model\AddFolderRequestBody(['path' => "/Transfered/".$foldername]));

            }
            catch (Exception $e){
                echo 'Exception when Creating Folder: ', $e->getMessage(), PHP_EOL;
                echo '<br/>';
            }



            $moveObject  = new \ExaVault\Model\MoveResourcesRequestBody(["resources"=>[$filePath],
                "parentResource"=>"/Transfered/".$foldername]);

            $moveResults = $resourcesApi->moveResourcesWithHttpInfo($API_KEY, $ACCESS_TOKEN, $moveObject);

            echo $moveObject;

        } catch (Exception $e) {
            echo 'Exception when Moving: ', $e->getMessage(), PHP_EOL;
            error_log("Exception when Moving: ".$e->getMessage());
            continue;
        }

        echo '<br/><br/>File Moved.';

    }
}