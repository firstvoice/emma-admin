<?php

include('../include/db.php');
include('../include/processing.php');

$sos_query = select_activeSOS();


if($sos_query->num_rows > 0){
    while ($sos = $sos_query->fetch_assoc()){
        echo $sos['emma_sos_id'];
        echo '<br/>';
        echo 'Time Over: '.$sos['time_over'];
        echo '<br/><br/>';

        $update = update_SOSPushed_with_sosID($sos['emma_sos_id']);

        $post = [
            'lat' => $sos['pending_lat'],
            'lng' => $sos['pending_lng'],
            'id' => $sos['emma_sos_id'],
            'a' => $sos['auth']
        ];
        $url = 'www.tsdemos.com/mobileapi/emma/update_sos.php';
        $result = CallAPiPOST($url, $post);
//        var_dump($result);
    }
}



function CallAPiPOST($url, $postData){
//    $jsonData = json_encode($postData);
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($curl);

    curl_close($curl);

    return $response;
}
