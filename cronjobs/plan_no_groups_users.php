<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 3/19/2019
 * Time: 11:20 AM
 */


$data = array();
$upcoming = array();

define('__ROOT__', dirname(dirname(__FILE__)));

require_once(__ROOT__.'/include/db.php');
require_once(__ROOT__.'/process/swiftmailer/swift_required.php');
require_once(__ROOT__.'/vendor/Classes/PHPExcel.php');
require_once(__ROOT__.'/vendor/Classes/PHPExcel/IOFactory.php');

// ------create the email setup------------

$transport = $SMT;

// Create the Mailer using your created Transport
$mailer = Swift_Mailer::newInstance($transport);
$from_email = FROMEMAIL;
$reportName = date('Y-m-d');

$objPHPExcel = new PHPExcel();

// Create a first sheet
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'EMMA Plan Groups With No Users');
$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Plan Name');
$objPHPExcel->getActiveSheet()->setCellValue('B2', 'Plan Id');
$objPHPExcel->getActiveSheet()->setCellValue('C2', 'Group Name');


$count = 2;

$orders = select_getplans_nogroupusers();
while($order = $orders->fetch_assoc()){
    $count++;
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$count, $order['name']);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$count, $order['emma_plan_id']);
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$count, $order['group_name']);
}


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save(__ROOT__.'/reports/plan-nogroupusers-report-'.$reportName.'.xlsx');

//// Testing output stream
//header('Content-type: application/vnd.ms-excel');
//header('Content-Disposition: attachment; filename="'. __ROOT__.'/reports/plan-nogroupusers-report-'.$reportName.'.xlsx"');
//$objWriter->save('php://output');


//
$emailBody = 'EMMA Users Missing Groups.';
$message = (new Swift_Message("EMMA User Group Report"))
    ->setFrom($from_email)
    ->setTo(array('pwickham@think-safe.com','cullrich@think-safe.com','mscroggs2@think-safe.com'))
//    ->setTo(array('smithpug@gmail.com'))
    ->setBody($emailBody)
    ->attach(Swift_Attachment::fromPath(__ROOT__.'/reports/plan-nogroupusers-report-' . $reportName . '.xlsx')
        ->setFilename('UserGroupReport'.$reportName . '.xlsx'));
// Send the message
$result = $mailer->send($message);







