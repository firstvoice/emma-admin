<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 3/19/2019
 * Time: 11:20 AM
 */


$data = array();
$upcoming = array();

define('__ROOT__', dirname(dirname(__FILE__)));

require_once(__ROOT__.'/include/db.php');
require_once(__ROOT__.'/process/swiftmailer/swift_required.php');
require_once(__ROOT__.'/vendor/Classes/PHPExcel.php');
require_once(__ROOT__.'/vendor/Classes/PHPExcel/IOFactory.php');

// ------create the email setup------------

$transport = $SMT;

// Create the Mailer using your created Transport
$mailer = Swift_Mailer::newInstance($transport);
$from_email = FROMEMAIL;
$reportName = date('Y-m-d');

$i = 0;
$objPHPExcel = new PHPExcel();
$plans = select_allActivePlans();
while($plan = $plans->fetch_assoc()){

    //Group History
    $objPHPExcel->setActiveSheetIndex($i);
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'EMMA Plan Group Changes');
    $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Group Name');
    $objPHPExcel->getActiveSheet()->setCellValue('B2', 'Edit Date');
    $objPHPExcel->getActiveSheet()->setCellValue('C2', 'Edit By ');

    $count = 2;
    $orders = select_getgroup_edited($plan['emma_plan_id']);
    while($order = $orders->fetch_assoc()){
        $count++;
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$count, $order['name']);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$count, $order['edit_date']);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$count, $order['username']);
    }


    //Site History
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$count, 'EMMA Plan Site Changes');
    $count++;
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$count, 'Plan Name');
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$count, 'User Id');
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$count, 'Username');
    $objPHPExcel->getActiveSheet()->setCellValue('D'.$count, 'First Name');
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$count, 'Last Name');

    $count++;
    $orders = select_getallsetupusers_users();
    while($order = $orders->fetch_assoc()){
        $count++;
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$count, $order['name']);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$count, $order['id']);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$count, $order['username']);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$count, $order['firstname']);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$count, $order['lastname']);
    }


    //User History
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$count, 'EMMA Users Without Groups');
    $count++;
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$count, 'Plan Name');
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$count, 'User Id');
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$count, 'Username');
    $objPHPExcel->getActiveSheet()->setCellValue('D'.$count, 'First Name');
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$count, 'Last Name');

    $orders = select_getallsetupusers_users();
    while($order = $orders->fetch_assoc()){
        $count++;
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$count, $order['name']);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$count, $order['id']);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$count, $order['username']);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$count, $order['firstname']);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$count, $order['lastname']);
    }



    $objPHPExcel->getActiveSheet()->setTitle($plan['name']);
    $objPHPExcel->createSheet();
    $i++;
}


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save(__ROOT__.'/reports/plan-change-report-'.$reportName.'.xlsx');

// Testing output stream
header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="'. __ROOT__ .'/reports/plan-change-report-'. $reportName .'.xlsx"');
$objWriter->save('php://output');


////
//$emailBody = 'EMMA Plan Change Log.';
//$message = (new Swift_Message("EMMA Plan Change Report"))
//    ->setFrom($from_email)
////    ->setTo(array('pwickham@think-safe.com','cullrich@think-safe.com','mscroggs2@think-safe.com'))
//    ->setTo(array('smithpug@gmail.com'))
//    ->setBody($emailBody)
//    ->attach(Swift_Attachment::fromPath(__ROOT__.'/reports/plan-change-report-' . $reportName . '.xlsx')
//        ->setFilename('PlanChangeReport'.$reportName . '.xlsx'));
//// Send the message
//$result = $mailer->send($message);







