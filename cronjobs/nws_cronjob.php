<?php

define('__ROOT__', dirname(dirname(__FILE__)));

require_once(__ROOT__.'/include/db.php');
require_once(__ROOT__.'/process/swiftmailer/swift_required.php');
require_once(__ROOT__.'/vendor/autoload.php');



$locations = array();


////query all locations
//$locationsQuery = select_nsw_locations();
//
////set results to location array,  location[state][county] = code
//while($locationResult = $locationsQuery->fetch_assoc()){
//
//    $locations[$locationResult['state']][$locationResult['zone_name']] = $locationResult['zone_id'];
//
//}

$locations["NW"][] = "NJC001";

//loop state
foreach($locations AS $state) {

    //loop counties
    foreach($state AS $county) {
        $sendAlert = false;
        //get result for county

        echo 'County code'. $county . '<br/>';
//        https://alerts.weather.gov/cap/wwaatmget.php?x=NJC001&amp;y=0
//        https://alerts.weather.gov/cap/wwaatmget.php?x='.$county.'&y=0


        $feed = simplexml_load_file('https://alerts.weather.gov/cap/wwaatmget.php?x='.$county.'&y=0');

//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, 'https://alerts.weather.gov/cap/wwaatmget.php?x='.$county.'&amp;y=0');
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        $output = curl_exec($ch);
//        curl_close($ch);
//
//        $data['response'] = json_decode($output);

        //parse out alert string

        echo $feed->title;
//        echo $output;

        exit(0);



        $currentAlertString = "";
        $currentAlertSevereStatus = 0;




        //query current county status
        $countyStatusQuery = select_nsw_county_severe_status($county);

        if($countyStatusQuery->num_rows > 0){
            //currently severe alerts active
            $countyStatusResult = $countyStatusQuery->fetch_assoc();

            //check if alert is the same
            $DBAlertString = $countyStatusResult["weather_report"];

            //if alert is different delete and insert
            if($DBAlertString == $currentAlertString){
                delete_nws_report($county, $DBAlertString);
                insert_nws_report($county, $currentAlertString, $currentAlertSevereStatus);
                if($currentAlertSevereStatus == 1){
                    $sendAlert = true;
                }
            }
            //else update last_pulled
            else {
                update_nws_status_time($county, $DBAlertString);
            }
        }

        //if weather is severe send alert
        if($sendAlert){

            $FcmServerKey = 'AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db';
            $topics = '/topics/' . 'emma_nws-' . $county;
            $IOStopic = '/topics/' . 'IOS-' . 'emma_nws-' . $county;
            $IOSTopics = array();


            if (empty($errors)) {

                $postData = array(
                    'to' => $topics,
                    'time_to_live' => 900,
                    'content_available' => true,
                    'data' => array(
                        'NWS' => true,
                        'event' => 'Severe Weather',
                        'description' => $notification,
                        'sound' => true,
                        'vibrate' => true
                    )
                );


                $ch = curl_init('https://fcm.googleapis.com/fcm/send');
                curl_setopt_array($ch, array(
                    CURLOPT_POST => TRUE,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HTTPHEADER => array(
                        'Authorization: key=' . $FcmServerKey,
                        'Content-Type: application/json'
                    ),
                    CURLOPT_POSTFIELDS => json_encode($postData)
                ));
                $response = curl_exec($ch);
                $responseData = json_decode($response);
                $data['response'] = $responseData;

                /////////////////////////////
                //    IOS NOTIFICATIONS    //
                /////////////////////////////


                $postIOSData = array(
                    'to' => $IOStopic,
                    'time_to_live' => 600,
                    'priority' => 'high',
                    'notification' => array(
                        'title' => "Severe Weather",
                        'body' => $notification,
                        'sound' => 'notification.aiff',
                        'badge' => '1'
                    ),
                    'data' => array(
                        'NWS' => true,
                        'event' => 'Severe Weather',
                        'description' => $notification,
                        'sound' => true,
                        'vibrate' => true
                    )
                );

                $ch = curl_init('https://fcm.googleapis.com/fcm/send');
                curl_setopt_array($ch, array(
                    CURLOPT_POST => TRUE,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HTTPHEADER => array(
                        'Authorization: key=' . $FcmServerKey,
                        'Content-Type: application/json'
                    ),
                    CURLOPT_POSTFIELDS => json_encode($postIOSData)
                ));
                $IOSresponse = curl_exec($ch);


            }
        }
    }
}
