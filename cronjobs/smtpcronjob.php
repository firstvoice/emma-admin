<?php

define('__ROOT__', dirname(dirname(__FILE__)));

require_once(__ROOT__.'/include/db.php');
require_once(__ROOT__.'/process/swiftmailer/swift_required.php');
require_once(__ROOT__.'/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;

$rootPath = "../../resources/emma/uploads/user_csv_files";

error_log("Starting upload read");

//get all uploads that happened in the last 10 hours
$uploadQuery = select_distict_user_csv_exavault_files_in_last_10_hours();
if($uploadQuery != null && $uploadQuery->num_rows > 0) {


    //loop through uploads {
    for ($i = 0; $i < $uploadQuery->num_rows; $i++) {
        $uploadResult = $uploadQuery->fetch_assoc();

        $folderPath = $rootPath . "/". $uploadResult['folder_name'];
        $filePath = $folderPath. '/' . $uploadResult['file_name'];


        $folderName = $uploadResult['folder_name'];

        global $emmadb;
        if(file_exists($filePath)) {

            //set to false when running on live
            $debug = true;

            //arrays for spreadsheet
            $userCreationArray = array();
            $userModificationArray = array();
            $userAdjustedPlansArray = array();
            $userAdjustedGroupsArray = array();
            $userDeletedArray = array();
            $userErrorArray = array();


            //read in csv file
            $csvData = array();
            $row = 0;

            $userMasterArray = array();

            if (($handle = fopen($filePath, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
                    if($data[0] != 'username/email' && $data[0] != '' && $data[0] != 'email') {
                        $csvData[$row] = array();
                        $columns = count($data);
                        for ($c = 0; $c < $columns; $c++) {
                            //put csv lines into searchable arrays
                            switch ($c) {
                                case 0:
                                {
                                    $csvData[$row]['user_email'] = strtolower($emmadb->real_escape_string($data[$c]));
                                    break;
                                }
                                case 1:
                                {
                                    $csvData[$row]['user_first_name'] = $emmadb->real_escape_string($data[$c]);
                                    break;
                                }
                                case 2:
                                {
                                    $csvData[$row]['user_middle_name'] = $emmadb->real_escape_string($data[$c]);
                                    break;
                                }
                                case 3:
                                {
                                    $csvData[$row]['user_last_name'] = $emmadb->real_escape_string($data[$c]);
                                    break;
                                }
                                case 4:
                                {
                                    $csvData[$row]['title'] = $emmadb->real_escape_string($data[$c]);
                                    break;
                                }
                                case 5:
                                {
                                    $csvData[$row]['user_phone'] = $emmadb->real_escape_string($data[$c]);
                                    break;
                                }
                                case 6:
                                {
                                    $csvData[$row]['user_landline'] = $emmadb->real_escape_string($data[$c]);
                                    break;
                                }
                                case 7:
                                {
                                    $csvData[$row]['company'] = $emmadb->real_escape_string($data[$c]);
                                    break;
                                }
                                case 8:
                                {
                                    $csvData[$row]['address'] = $emmadb->real_escape_string($data[$c]);
                                    break;
                                }
                                case 9:
                                {
                                    $csvData[$row]['city'] = $emmadb->real_escape_string($data[$c]);
                                    break;
                                }
                                case 10:
                                {
                                    $csvData[$row]['province'] = $emmadb->real_escape_string($data[$c]);
                                    break;
                                }
                                case 11:
                                {
                                    $csvData[$row]['country'] = $emmadb->real_escape_string($data[$c]);
                                    break;
                                }
                                case 12:
                                {
                                    $csvData[$row]['zip'] = $emmadb->real_escape_string($data[$c]);
                                    break;
                                }
                                case 13:
                                {
                                    if($emmadb->real_escape_string($data[$c]) == 1){
                                        $csvData[$row]['activate_email'] = 1;
                                    }
                                    else{
                                        $csvData[$row]['activate_email'] = 0;
                                    }
                                    break;
                                }
                                case 14:
                                {
                                    if($emmadb->real_escape_string($data[$c]) == 1){
                                        $csvData[$row]['no_logout'] = 1;
                                    }
                                    else{
                                        $csvData[$row]['no_logout'] = 0;
                                    }
                                    break;
                                }
                                case 15:
                                {
                                    if($emmadb->real_escape_string($data[$c]) == 1){
                                        $csvData[$row]['admin_user'] = 1;
                                    }
                                    else{
                                        $csvData[$row]['admin_user'] = 0;
                                    }
                                    break;
                                }
                                case 16:
                                {
                                    //plan ids
                                    $csvData[$row]['plan_name_string'] = $emmadb->real_escape_string($data[$c]);

                                    $CSVplanArray = array();
                                    $CSVplanArray = explode(';', $data[$c]);
                                    $escapedPlanArray = array();
                                    foreach($CSVplanArray AS $arrayItem){
                                        $escapedPlanArray[] = trim($emmadb->real_escape_string($arrayItem));
                                    }
                                    $csvData[$row]['plan_array'] = $escapedPlanArray;
                                    break;
                                }
                                case 17:
                                {
                                    //group ids
                                    $csvData[$row]['group_names'] = $emmadb->real_escape_string($data[$c]);
                                    $CSVgroupArray = array();
                                    $CSVgroupArray = explode(';', $data[$c]);
                                    $escapedGroupArray = array();
                                    foreach($CSVgroupArray AS $arrayItem){
                                        $escapedGroupArray[] = trim($emmadb->real_escape_string($arrayItem));
                                    }
                                    $csvData[$row]['group_array'] = $escapedGroupArray;
                                    break;
                                }
                                case 18:
                                {
                                    $csvData[$row]['edit'] = $emmadb->real_escape_string($data[$c]);
                                }
                                case 19:
                                {
                                    $csvData[$row]['delete'] = $emmadb->real_escape_string($data[$c]);
                                }
                                case 20:
                                {
                                    $csvData[$row]['default_plan'] = $emmadb->real_escape_string($data[$c]);
                                }
                                default:
                                {
                                    //overflow
                                    $csvData[$row]['overflow'][] = $emmadb->real_escape_string($data[$c]);
                                }
                            }
                        }
                    }
                    $row++;
                }
                fclose($handle);
            }
            else{
                error_log("failed to open: ". $filePath. " file uploaded around: ". date("y_m_d"));
            }


            //get upload options
            $uploadOptionsQuery = select_upload_options($uploadResult['folder_name']);
            $uploadOptionsResults = $uploadOptionsQuery->fetch_assoc();


            //get ids for plan names
            $planIDArray = array();
            $planNameArray = array();
            $planNameQuery = select_plans_with_user_folder($folderName);
            while($planNameResult = $planNameQuery->fetch_assoc()){
                $planNameArray[] = $planNameResult['name'];
                $planIDArray[] = $planNameResult['emma_plan_id'];
                if($debug){
                    echo 'Added Plan to global list: '.$planNameResult['name'].' Plan ID: '.$planNameResult['emma_plan_id'].'<br/>';
                }
            }



            //loop through users
            foreach($csvData AS $user){
                if($debug){
                    echo '<hr/>';
                    echo 'WORKING ON USER: '.$user['user_email'].'<br/>';
                    echo 'Plans: '.$user['plan_name_string']. ' Count: '.count($user['plan_array']).'<br/>';
                }

                //check if user exists
                $dbUser = null;

                //get planIDs for user
                $userPlanIDs = array();
                foreach($user['plan_array'] AS $planName) {
                    if($debug){
                        echo 'Looping Plan: '.$planName.'<br/>';
                    }
                    $planNameArrayIndex = array_search($planName, $planNameArray);
                    if ($planNameArrayIndex !== false) {
                        $userPlanIDs[] = $planIDArray[$planNameArrayIndex];
                        if($debug){
                            echo 'Added plan to userPlanIDs: '.$planName.' PlanID: '.$planIDArray[$planNameArrayIndex].'<br/>';
                        }
                    }
                    else{
                        if($debug){
                            echo 'Plan Name was not found: '.$planName.'<br/>';
                        }
                        error_log('Plan Name: '.$planName.' was not found for user: '.$user['user_email']);
                    }

                    //add user to masterarray
                    $userMasterArray[$planIDArray[$planNameArrayIndex]][] = $user['user_email'];
                }

                if(empty($userPlanIDs)){
                    error_log('User: '.$user['user_email'].' has an empty userPlanIDs array');
                }

                $userExistsQuery = select_users_by_email($user['user_email']);
                if($userExistsQuery->num_rows > 0){
                    $dbUser = $userExistsQuery->fetch_assoc();
                    if($dbUser['deleted_by_cronjob'] == 1){
                        reactivate_user_csv($dbUser['id']);
                    }
                }
                else{
                    //------------create user

                    if($userPlanIDs[0] != 0) {
                        $password = hash('ripemd128', $user['user_email']);
                        //insert into db
                        $createUser = insert_user_from_csv($user['user_email'], $password, $user['user_first_name'],
                            $user['user_middle_name'], $user['user_last_name'], $user['title'],
                            $user['user_phone'], $user['user_landline'], $user['company'],
                            $user['address'], $user['city'], $user['province'], $user['country'],
                            $user['zip'], 'CSV API', -1, $userPlanIDs[0], $user['activate_email'], $user['no_logout'], $user['admin_user']);

                        //add privileges and get dbuser object
                        if ($createUser) {
                            $newUserCreation = select_users_by_email($user['user_email']);
                            if ($newUserCreation->num_rows > 0) {
                                $dbUser = $newUserCreation->fetch_assoc();
                                insert_privileges($dbUser['id']);


                                //send email to set user
                                $sendActivationEmails = $uploadOptionsResults['email_new_users'];
                                if ($sendActivationEmails) {
                                    $link = 'https://' . __ROOT__ . '/verify_email.php?u=' .
                                        $dbUser['id'] . '&c=' . $password;

                                    $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"><head><link rel="stylesheet" type="text/css" href="css/app.css"><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta name="viewport" content="width=device-width"><title>EMMA User Account Activation</title></head><body style="-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;box-sizing:border-box;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;min-width:100%;padding:0;text-align:left;width:100%!important"><style>@media only screen{html{min-height:100%;background:#f3f3f3}}@media only screen and (max-width:596px){.small-float-center{margin:0 auto!important;float:none!important;text-align:center!important}.small-text-center{text-align:center!important}.small-text-left{text-align:left!important}.small-text-right{text-align:right!important}}@media only screen and (max-width:596px){.hide-for-large{display:block!important;width:auto!important;overflow:visible!important;max-height:none!important;font-size:inherit!important;line-height:inherit!important}}@media only screen and (max-width:596px){table.body table.container .hide-for-large,table.body table.container .row.hide-for-large{display:table!important;width:100%!important}}@media only screen and (max-width:596px){table.body table.container .callout-inner.hide-for-large{display:table-cell!important;width:100%!important}}@media only screen and (max-width:596px){table.body table.container .show-for-large{display:none!important;width:0;mso-hide:all;overflow:hidden}}@media only screen and (max-width:596px){table.body img{width:auto;height:auto}table.body center{min-width:0!important}table.body .container{width:95%!important}table.body .column,table.body .columns{height:auto!important;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;padding-left:16px!important;padding-right:16px!important}table.body .column .column,table.body .column .columns,table.body .columns .column,table.body .columns .columns{padding-left:0!important;padding-right:0!important}table.body .collapse .column,table.body .collapse .columns{padding-left:0!important;padding-right:0!important}td.small-1,th.small-1{display:inline-block!important;width:8.33333%!important}td.small-2,th.small-2{display:inline-block!important;width:16.66667%!important}td.small-3,th.small-3{display:inline-block!important;width:25%!important}td.small-4,th.small-4{display:inline-block!important;width:33.33333%!important}td.small-5,th.small-5{display:inline-block!important;width:41.66667%!important}td.small-6,th.small-6{display:inline-block!important;width:50%!important}td.small-7,th.small-7{display:inline-block!important;width:58.33333%!important}td.small-8,th.small-8{display:inline-block!important;width:66.66667%!important}td.small-9,th.small-9{display:inline-block!important;width:75%!important}td.small-10,th.small-10{display:inline-block!important;width:83.33333%!important}td.small-11,th.small-11{display:inline-block!important;width:91.66667%!important}td.small-12,th.small-12{display:inline-block!important;width:100%!important}.column td.small-12,.column th.small-12,.columns td.small-12,.columns th.small-12{display:block!important;width:100%!important}table.body td.small-offset-1,table.body th.small-offset-1{margin-left:8.33333%!important;Margin-left:8.33333%!important}table.body td.small-offset-2,table.body th.small-offset-2{margin-left:16.66667%!important;Margin-left:16.66667%!important}table.body td.small-offset-3,table.body th.small-offset-3{margin-left:25%!important;Margin-left:25%!important}table.body td.small-offset-4,table.body th.small-offset-4{margin-left:33.33333%!important;Margin-left:33.33333%!important}table.body td.small-offset-5,table.body th.small-offset-5{margin-left:41.66667%!important;Margin-left:41.66667%!important}table.body td.small-offset-6,table.body th.small-offset-6{margin-left:50%!important;Margin-left:50%!important}table.body td.small-offset-7,table.body th.small-offset-7{margin-left:58.33333%!important;Margin-left:58.33333%!important}table.body td.small-offset-8,table.body th.small-offset-8{margin-left:66.66667%!important;Margin-left:66.66667%!important}table.body td.small-offset-9,table.body th.small-offset-9{margin-left:75%!important;Margin-left:75%!important}table.body td.small-offset-10,table.body th.small-offset-10{margin-left:83.33333%!important;Margin-left:83.33333%!important}table.body td.small-offset-11,table.body th.small-offset-11{margin-left:91.66667%!important;Margin-left:91.66667%!important}table.body table.columns td.expander,table.body table.columns th.expander{display:none!important}table.body .right-text-pad,table.body .text-pad-right{padding-left:10px!important}table.body .left-text-pad,table.body .text-pad-left{padding-right:10px!important}table.menu{width:100%!important}table.menu td,table.menu th{width:auto!important;display:inline-block!important}table.menu.small-vertical td,table.menu.small-vertical th,table.menu.vertical td,table.menu.vertical th{display:block!important}table.menu[align=center]{width:auto!important}table.button.small-expand,table.button.small-expanded{width:100%!important}table.button.small-expand table,table.button.small-expanded table{width:100%}table.button.small-expand table a,table.button.small-expanded table a{text-align:center!important;width:100%!important;padding-left:0!important;padding-right:0!important}table.button.small-expand center,table.button.small-expanded center{min-width:0}}</style><span class="preheader" style="color:#f3f3f3;display:none!important;font-size:1px;line-height:1px;max-height:0;max-width:0;mso-hide:all!important;opacity:0;overflow:hidden;visibility:hidden"></span><table class="body" style="Margin:0;background:#f3f3f3;border-collapse:collapse;border-spacing:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;height:100%;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><td class="center" align="center" valign="top" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><center data-parsed="" style="min-width:580px;width:100%"><table align="center" class="container main float-center" style="Margin:0 auto;background:#fefefe;border:1px solid #00549d;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:580px"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="20px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;hyphens:auto;line-height:20px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" valign="middle" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><h1 id="email-title" class="text-center" style="Margin:0;Margin-bottom:10px;color:#00549d;font-family:Helvetica,Arial,sans-serif;font-size:34px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;padding-top:10px;text-align:center;word-wrap:normal">EMMA User Account Activation</h1></th><th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></tbody></table><hr><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="20px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;hyphens:auto;line-height:20px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="20px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;hyphens:auto;line-height:20px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><table class="callout" style="Margin-bottom:16px;border-collapse:collapse;border-spacing:0;margin-bottom:16px;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th class="callout-inner primary" style="Margin:0;background:#cae6ff;border:1px solid #444;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:10px;text-align:left;width:100%"><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
<p><b>You have been invited to use EMMA by your organization’s safety administrator.</b></p>
<p>A login username and password will be created for you.</p>
<p>Please click on this link to activate your EMMA account:   <a href="' . $link . '">Activate</a></p>
<p>Or visit: <a href="' . $link . '">' . $link . '</a></p>
<p>NOTE: This link is for activating this account only.</p>
<p><b>Thank you for making minutes matter!</b></p>
<p>If you have any questions or need technical support please email <a href="mailto:emma@think-safe.com">emma@think-safe.com</a> or call 319-377-5125 from 8-5pm Central Time or leave a message for a return call by our tech support team.</p>
<a href="https://www.emmaadmin.com/product_sheets.php"><b><i>What is EMMA?</i></b></a>
<p>Think Safe, Inc. <br/>
    <a href="https://www.think-safe.com">www.think-safe.com</a> <br/>
    The Creators of EMMA <br/>
    A First Voice app</p>
<hr><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><div class="comments" style="white-space:pre-line"></div></th><th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></tbody></table></th><th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></table></th></tr></tbody></table></td></tr></tbody></table></center></td></tr></table><!-- prevent Gmail on iOS font size manipulation --><div style="display:none;white-space:nowrap;font:15px courier;line-height:0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></body></html>';

                                    $bodyText = 'Copy this link into your browser to activate your EMMA account - ' .
                                        $link;

                                    $mailer = Swift_Mailer::newInstance($SMT);
                                    $swiftMessage = Swift_Message::newInstance()
                                        ->setSubject('EMMA Account Creation')
                                        ->setFrom(array('donotreply@emmaadmin.com' => 'EMMA Admin'))
                                        ->setTo(array($user['user_email'] => $user['user_first_name'] . ' ' . $user['user_last_name']))
                                        ->setBody($body, 'text/html')
                                        ->addPart($bodyText, 'text/plain');
                                    $result = $mailer->send($swiftMessage);
                                    if ($debug) {
                                        echo 'Sending email to: ' . $user['user_email'] . '<br/>';
                                    }

                                    if (!$result) {
                                        error_log("Failed to send activation Email to: " . $user['user_email'] . " password: " . $password . " Link: " . $link);
                                    }

                                }
                            }
                        }

                        foreach ($userPlanIDs AS $addToCreationPlanID) {
                            $userCreationArray[$addToCreationPlanID][] = $user;
                        }
                    }
                    else{
                        foreach($userPlanIDs AS $userPlanID){
                            $userErrorArray[$userPlanID][] = $user;
                        }
                        error_log('user: '.$user['user_email'].' creation failed due to default plan being 0');
                    }
                }




                //remove plans that it shouldnt be in
                if(!empty($userPlanIDs)) {
                    //get plans user is currently in
                    $currentPlansQuery = select_all_user_multiplan($dbUser['id']);
                    if ($currentPlansQuery->num_rows > 0) {
                        $userCurrentPlanArray = array();
                        while ($currentPlansResult = $currentPlansQuery->fetch_assoc()) {
                            $userCurrentPlanArray[] = $currentPlansResult['plan_id'];
                            if ($debug) {
                                echo 'Currently in plan: ' . $currentPlansResult['plan_id'] . '<br/>';
                            }
                        }
                        $checkRemovedPlan = false;
                        foreach ($userCurrentPlanArray AS $currentPlan) {
                            if (array_search($currentPlan, $planIDArray) !== false) {
                                if(array_search($currentPlan, $userPlanIDs) === false) {
                                    if($debug){
                                        echo 'Removing from plan: '.$currentPlan.'<br/>';
                                    }
                                    disable_emma_user_multiplan($dbUser['id'], $currentPlan);
                                    disable_emma_user_groups($dbUser['id'], $currentPlan);
                                    if(array_search($user['user_email'], $userAdjustedPlansArray) === false){
                                        $userAdjustedPlansArray[$currentPlan][] = $user;
                                    }
                                    $checkRemovedPlan = true;
                                }
                            }
                        }
                        if(!$checkRemovedPlan){
                            if($debug){
                                echo 'No Plans to remove<br/>';
                            }
                        }
                    }
                }


                //add plan it should be in
                if(!empty($userPlanIDs)) {
                    //get plans user is currently in
                    $currentPlansQuery = select_all_user_multiplan($dbUser['id']);
                    if($currentPlansQuery->num_rows > 0){
                        $userCurrentPlanArray = array();
                        while($currentPlansResult = $currentPlansQuery->fetch_assoc()){
                            $userCurrentPlanArray[] = $currentPlansResult['plan_id'];
                            if($debug){
                                echo 'Currently in plan: '.$currentPlansResult['plan_id'].'<br/>';
                            }
                        }
                        foreach($userPlanIDs AS $planNeededID) {
                            if (array_search($planNeededID, $userCurrentPlanArray) === false) {
                                $insertIntoMultiplan = insert_emma_multi_plan_nullExpires($dbUser['id'], $planNeededID, 1, '');
                                if ($debug) {
                                    echo 'Added user to multiplan: ' . $planNeededID . '<br/>';
                                }
                                if (array_search($user['user_email'], $userAdjustedPlansArray) === false) {
                                    $userAdjustedPlansArray[$planNeededID][] = $user;
                                }
                            }
                        }
                    }
                    else{
                        //add all plans
                        foreach($userPlanIDs AS $addPlanID) {
                            $insertIntoMultiplan = insert_emma_multi_plan_nullExpires($dbUser['id'], $addPlanID, 1, '');
                            if($debug){
                                echo 'Added user to multiplan: '.$addPlanID.'<br/>';
                            }
                            if(array_search($user['user_email'], $userAdjustedPlansArray) === false){
                                $userAdjustedPlansArray[$addPlanID][] = $user;
                            }
                        }
                    }
                }
                else{
                    $userErrorArray['unknown_plan'][] = $user;

                    if($debug){
                        echo 'User Plan IDs was empty!<br/>';
                    }
                }

                if(!empty($userPlanIDs)) {
                    if ($dbUser['emma_plan_id'] != $userPlanIDs[0] && $userPlanIDs[0] != 0) {
                        $updateUserPlanID = update_user_default_plan($dbUser['id'], $userPlanIDs[0]);
                    }
                }



                //get all groups user should be in
                $allGroupQuery = select_csv_all_user_groups($folderName, $user['group_array'], $user['plan_array']);
                if($debug){
                    echo 'User Group query count: '.$allGroupQuery->num_rows.'<br/>';
                }
                if($allGroupQuery->num_rows > 0){
                    $userGroupArray = array();
                    while($allGroupResult = $allGroupQuery->fetch_assoc()){
                        $userGroupArray[] = $allGroupResult['emma_group_id'];
                    }

                    //get current groups
                    $currentUserGroups = array();
                    $currentGroupQuery = select_all_current_user_groups_in_plangroup($folderName, $dbUser['id']);
                    if($currentGroupQuery->num_rows > 0){
                        while($currentGroupResult = $currentGroupQuery->fetch_assoc()){
                            $currentUserGroups[] = $currentGroupResult['emma_group_id'];
                        }

                        //add groups
                        foreach($userGroupArray AS $requiredGroup){
                            if(array_search($requiredGroup, $currentUserGroups) === false){
                                //user is not in group
                                insert_user_groups($dbUser['id'], $requiredGroup);
                                if($debug){
                                    echo 'Added user to group: '.$requiredGroup.'<br/>';
                                }
                            }
                        }
                        //remove groups
                        foreach($currentUserGroups AS $inGroup){
                            if(array_search($inGroup, $userGroupArray) === false){
                                //user should not be in group
                                delete_emma_user_groups($dbUser['id'],$inGroup);
                                if($debug){
                                    echo 'Removed user from group: '.$inGroup.'<br/>';
                                }
                            }
                        }

                    }
                    else{
                        //add all groups
                        foreach($userGroupArray AS $requiredGroup){
                            insert_user_groups($dbUser['id'], $requiredGroup);
                            if($debug){
                                echo 'Added user to group: '.$requiredGroup.'<br/>';
                            }
                        }
                    }
                }
                else{
                    foreach($userPlanIDs AS $addPlanID) {
                        $userErrorArray[$addPlanID][] = $user;
                    }
                }



                if($user['edit'] == 1 || $user['edit'] == true || $user['edit'] == 'true'){
                    //update user
                    $updateUserDetails = update_csv_userDetails($dbUser['id'], $user['user_first_name'],
                        $user['user_middle_name'], $user['user_last_name'], $user['user_landline'], $user['user_phone'],
                        $user['title'], $user['company'], $user['address'], $user['city'], $user['province'],
                        $user['country'], $user['zip'], $user['activate_email'], $user['no_logout'], $user['admin_user']);
                    if($debug){
                        echo 'Edited User<br/>';
                    }

                    foreach($userPlanIDs AS $modifyPlanID) {
                        $userModificationArray[$modifyPlanID][] = $user;
                    }
                }


             //Delete User
                if($user['delete'] == 1 || $user['delete'] == true || $user['delete'] == 'true'){
                    //remove user from plan, but leave flag for re-enable
                    $delete = true;
                    foreach($planIDArray as $deletePlanID){
                        if(array_search($deletePlanID, $userPlanIDs) === false){
                            $delete = false;
                        }

                        //remove user from groups
                        disable_emma_user_groups($dbUser['id'], $deletePlanID);
                        //remove user from multiplan
                        disable_emma_user_multiplan($dbUser['id'], $deletePlanID);

                        //add removed users to excel sheet
                        $userDeletedArray[$deletePlanID][] = $user;
                        if($debug){
                            echo 'Removed user from all plans and groups<br/>';
                        }

                    }
                    if($delete){
                        disable_emma_user($dbUser['id']);
                        if($debug){
                            echo 'Disabled User<br/>';
                        }
                    }
                }

            } //end loop USER

            if($debug){
                echo '<hr/>';
            }


            if($debug){
                echo 'UsersNotInAccount<br/>';
            }
            //Find all users in the accountPlans thats not on the csv
            $usersNotInAccount = array();
            foreach($planIDArray AS $planIDItem){
                if($debug){
                    echo 'Plan ID: '.$planIDItem.'<br/>';
                }
                $usersNotInAccount[$planIDItem] = array();
                //if $userMasterArray[] index
                if(array_key_exists($planIDItem, $userMasterArray)) {
                    $userMasterArray_PlanBased = $userMasterArray[$planIDItem];
                    //get all users in plan
                    $allUsersQuery = select_all_plan_users($planIDItem);
                    if ($allUsersQuery->num_rows > 0) {
                        while ($allUsersResult = $allUsersQuery->fetch_assoc()) {
                            if (array_search($allUsersResult['username'], $userMasterArray_PlanBased) === false) {
                                //user found that not in the csv
                                if(array_search($allUsersResult['username'], $usersNotInAccount[$planIDItem]) === false) {
                                    $usersNotInAccount[$planIDItem][] = $allUsersResult['username'];

                                    //disable user



                                    //remove user from plans




                                    //remove user from groups





                                }
                                if ($debug) {
                                    echo 'User added to not in account: ' . $allUsersResult['username'] . "<br/>";
                                }
                            }
                        }
                    }
                }
                else{
                    $allUsersQuery = select_all_plan_users($planIDItem);
                    if ($allUsersQuery->num_rows > 0) {
                        while ($allUsersResult = $allUsersQuery->fetch_assoc()) {
                            $usersNotInAccount[$planIDItem][] = $allUsersResult['username'];
                            if ($debug) {
                                echo 'User added to not in account: ' . $allUsersResult['username'] . "<br/>";
                            }
                        }
                    }
                }
            }






            if($debug){
                echo '<hr/>Building xlsx<hr/>';
            }

            //create excel sheet for this upload
            $spreadsheet = new Spreadsheet();

            for($p = 0; $p < count($planNameArray); $p++) {
                $sheetPlanID = null;
                $sheetPlanID = $planIDArray[$p];
                $aplan = $planNameArray[$p];
                if(count_chars($aplan) > 30){
                    $splitarray = str_split($aplan, 30);
                    $aplan = $splitarray[0];
                }
                $aplan = preg_replace('/[^A-Za-z0-9. -]/', '', $aplan);
                //create header for csv section or maybe new page for a workbook
                try {
                    $myWorkSheet = new PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $aplan);
                    $spreadsheet->addSheet($myWorkSheet);

                    if ($spreadsheet->sheetNameExists('Worksheet')) {
                        $sheetIndex = $spreadsheet->getIndex(
                            $spreadsheet->getSheetByName('Worksheet')
                        );
                        $spreadsheet->removeSheetByIndex($sheetIndex);
                    }

                    $spreadsheet->getSheet($p)->setCellValue('A1', $aplan);
                    $spreadsheet->getSheet($p)->setCellValue('A3', 'Created Users');

                    if ($debug) {
                        echo 'User Creation Array Count: ' . count($userCreationArray[$sheetPlanID]) . '<br/>';
                    }

                    if(array_key_exists($sheetPlanID, $userCreationArray)) {
                        for ($q = 0; $q < count($userCreationArray[$sheetPlanID]); $q++) {
                            $spreadsheet->getSheet($p)->setCellValue('A' . ($q + 4), $userCreationArray[$sheetPlanID][$q]['user_email']);
                        }

                        $point = count($userCreationArray[$sheetPlanID]) + 5;
                    }
                    else{
                        $point = 5;
                    }


                    $spreadsheet->getSheet($p)->setCellValue('A' . $point, 'Edited Users Info');
                    $point++;

                    if ($debug) {
                        echo 'User Edit Array Count: ' . count($userModificationArray[$sheetPlanID]) . '<br/>';
                    }

                    if(array_key_exists($sheetPlanID, $userModificationArray)) {
                        for ($q = 0; $q < count($userModificationArray[$sheetPlanID]); $q++) {
                            $spreadsheet->getSheet($p)->setCellValue('A' . ($q + $point), $userModificationArray[$sheetPlanID][$q]['user_email']);
                        }
                        $point = $point + count($userModificationArray[$sheetPlanID]) + 1;
                    }
                    else{
                        $point = $point + 1;
                    }




                    $spreadsheet->getSheet($p)->setCellValue('A' . $point, 'Adjusted Users Plans');
                    $point++;

                    if ($debug) {
                        echo 'User Adjusted Plan Array Count: ' . count($userAdjustedPlansArray[$sheetPlanID]) . '<br/>';
                    }

                    if(array_key_exists($sheetPlanID, $userAdjustedPlansArray)) {
                        for ($q = 0; $q < count($userAdjustedPlansArray[$sheetPlanID]); $q++) {
                            $spreadsheet->getSheet($p)->setCellValue('A' . ($q + $point), $userAdjustedPlansArray[$sheetPlanID][$q]['user_email']);
                        }

                        $point = $point + count($userAdjustedPlansArray[$sheetPlanID]) + 1;
                    }
                    else{
                        $point = $point + 1;
                    }

                    $spreadsheet->getSheet($p)->setCellValue('A' . $point, 'Deleted Users');
                    $point++;

                    if ($debug) {
                        echo 'User Delete Array Count: ' . count($userDeletedArray[$sheetPlanID]) . '<br/>';
                    }

                    if(array_key_exists($sheetPlanID, $userDeletedArray)) {
                        for ($q = 0; $q < count($userDeletedArray[$sheetPlanID]); $q++) {
                            $spreadsheet->getSheet($p)->setCellValue('A' . ($q + $point), $userDeletedArray[$sheetPlanID][$q]['user_email']);
                        }

                        $point = $point + count($userDeletedArray[$sheetPlanID]) + 1;
                    }
                    else{
                        $point = $point + 1;
                    }

                    $spreadsheet->getSheet($p)->setCellValue('A' . $point, 'Users Not On CSV File');
                    $point++;

                    if ($debug) {
                        echo 'User Not In CSV Array Count: ' . count($usersNotInAccount[$sheetPlanID]) . '<br/>';
                    }

                    if(array_key_exists($sheetPlanID, $usersNotInAccount)) {
                        for ($q = 0; $q < count($usersNotInAccount[$sheetPlanID]); $q++) {
                            $spreadsheet->getSheet($p)->setCellValue('A' . ($q + $point), $usersNotInAccount[$sheetPlanID][$q]);
                        }

                        $point = $point + count($usersNotInAccount[$sheetPlanID]) + 1;
                    }
                    else{
                        $point = $point + 1;
                    }

                    $spreadsheet->getSheet($p)->setCellValue('A' . $point, 'Errors');
                    $point++;
                    $point++;

                    if ($debug) {
                        echo 'User Error Array Count: ' . count($userErrorArray[$sheetPlanID]) . '<br/>';
                    }

                    if(array_key_exists($sheetPlanID, $userErrorArray)) {
                        for ($q = -1; $q < count($userErrorArray[$sheetPlanID]); $q++) {

                            if ($q == -1) {
                                $spreadsheet->getSheet($p)->setCellValue('A' . ($q + $point), 'Username');
                                $spreadsheet->getSheet($p)->setCellValue('B' . ($q + $point), 'First Name');
                                $spreadsheet->getSheet($p)->setCellValue('C' . ($q + $point), 'Middle Name');
                                $spreadsheet->getSheet($p)->setCellValue('D' . ($q + $point), 'Last Name');
                                $spreadsheet->getSheet($p)->setCellValue('E' . ($q + $point), 'Title');
                                $spreadsheet->getSheet($p)->setCellValue('F' . ($q + $point), 'Mobile');
                                $spreadsheet->getSheet($p)->setCellValue('G' . ($q + $point), 'Landline');
                                $spreadsheet->getSheet($p)->setCellValue('H' . ($q + $point), 'Company');
                                $spreadsheet->getSheet($p)->setCellValue('I' . ($q + $point), 'Address');
                                $spreadsheet->getSheet($p)->setCellValue('J' . ($q + $point), 'City');
                                $spreadsheet->getSheet($p)->setCellValue('K' . ($q + $point), 'State/Province');
                                $spreadsheet->getSheet($p)->setCellValue('L' . ($q + $point), 'Country');
                                $spreadsheet->getSheet($p)->setCellValue('M' . ($q + $point), 'Zip');
                                $spreadsheet->getSheet($p)->setCellValue('N' . ($q + $point), 'Plans');
                                $spreadsheet->getSheet($p)->setCellValue('O' . ($q + $point), 'Groups');
                                $spreadsheet->getSheet($p)->setCellValue('P' . ($q + $point), 'Edit');
                                $spreadsheet->getSheet($p)->setCellValue('Q' . ($q + $point), 'Delete');
                            } else {
                                $spreadsheet->getSheet($p)->setCellValue('A' . ($q + $point), $userErrorArray[$sheetPlanID][$q]['user_email']);
                                $spreadsheet->getSheet($p)->setCellValue('B' . ($q + $point), $userErrorArray[$sheetPlanID][$q]['user_first_name']);
                                $spreadsheet->getSheet($p)->setCellValue('C' . ($q + $point), $userErrorArray[$sheetPlanID][$q]['user_middle_name']);
                                $spreadsheet->getSheet($p)->setCellValue('D' . ($q + $point), $userErrorArray[$sheetPlanID][$q]['user_last_name']);
                                $spreadsheet->getSheet($p)->setCellValue('E' . ($q + $point), $userErrorArray[$sheetPlanID][$q]['title']);
                                $spreadsheet->getSheet($p)->setCellValue('F' . ($q + $point), $userErrorArray[$sheetPlanID][$q]['user_phone']);
                                $spreadsheet->getSheet($p)->setCellValue('G' . ($q + $point), $userErrorArray[$sheetPlanID][$q]['user_landline']);
                                $spreadsheet->getSheet($p)->setCellValue('H' . ($q + $point), $userErrorArray[$sheetPlanID][$q]['company']);
                                $spreadsheet->getSheet($p)->setCellValue('I' . ($q + $point), $userErrorArray[$sheetPlanID][$q]['address']);
                                $spreadsheet->getSheet($p)->setCellValue('J' . ($q + $point), $userErrorArray[$sheetPlanID][$q]['city']);
                                $spreadsheet->getSheet($p)->setCellValue('K' . ($q + $point), $userErrorArray[$sheetPlanID][$q]['province']);
                                $spreadsheet->getSheet($p)->setCellValue('L' . ($q + $point), $userErrorArray[$sheetPlanID][$q]['country']);
                                $spreadsheet->getSheet($p)->setCellValue('M' . ($q + $point), $userErrorArray[$sheetPlanID][$q]['zip']);
                                $spreadsheet->getSheet($p)->setCellValue('N' . ($q + $point), $userErrorArray[$sheetPlanID][$q]['plan_name_string']);
                                $spreadsheet->getSheet($p)->setCellValue('O' . ($q + $point), $userErrorArray[$sheetPlanID][$q]['group_names']);
                                $spreadsheet->getSheet($p)->setCellValue('P' . ($q + $point), $userErrorArray[$sheetPlanID][$q]['edit']);
                                $spreadsheet->getSheet($p)->setCellValue('Q' . ($q + $point), $userErrorArray[$sheetPlanID][$q]['delete']);
                            }
                        }
                    }
                }
                catch (PhpOffice\PhpSpreadsheet\Exception $e){
                    global $errors;
                    $errors[] = 'Failed to create spreadsheet with title'. $aplan;
                    error_log('Failed to create spreadsheet with title: ' . $aplan . ' Original title: '.$planNameArray[$p]);
                    error_log($e->getMessage());
                }
                catch (Exception $e){
                    error_log($e->getMessage());
                }
            }

            $writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
            $resultFile = $rootPath.'/'.$folderName."/result_".date("Y_m_d_h_i").".xlsx";
            $writer->save($resultFile);


            $updatecsvdb = update_csv_uploads($uploadResult['upload_id']);

            //send email to uploader with excel attachment
            $contactEmail = $uploadOptionsResults['contact_email'];
            $contactEmail2 = $uploadOptionsResults['secondary_contact_email'];

            $mailer = Swift_Mailer::newInstance($SMT);
            $emailBody = 'EMMA user upload results are attached in the xlsx file';
            $message = (new Swift_Message("User Upload Results"))
                ->setFrom(FROMEMAIL)
                ->setTo(array($contactEmail))
                ->setBody($emailBody)
                ->attach(Swift_Attachment::fromPath($resultFile)
                    ->setFilename('upload_result.xlsx'));
            $result = $mailer->send($message);
            if(!$result){
                $validSMT = "valid";
                if($SMT == null){
                    $validSMT = "null";
                }
                if(FROMEMAIL == null || FROMEMAIL == ""){
                    $fromMail = 'empty';
                }
                else{
                    $fromMail = FROMEMAIL;
                }
//            echo "finished, But failed to send email with xlsx";
                error_log("failed to send email for csv upload to: ". $contactEmail . " around: ". date("y-m-d"). " SMT was: ". $validSMT." FROMEMAIL: ".$fromMail);
            }
            else{
//            echo "finished.";
                error_log('FINISHED CSV READ ---------------------------------------------------------------------------------------------------------------------------->>>>>>>');
            }

            if(!empty($contactEmail2)) {
                $mailer = Swift_Mailer::newInstance($SMT);
                $emailBody = 'EMMA user upload results are attached in the xlsx file';
                $message = (new Swift_Message("User Upload Results"))
                    ->setFrom(FROMEMAIL)
                    ->setTo(array($contactEmail2))
                    ->setBody($emailBody)
                    ->attach(Swift_Attachment::fromPath($resultFile)
                        ->setFilename('upload_result.xlsx'));
                $result2 = $mailer->send($message);
                if (!$result2) {
                    $validSMT = "valid";
                    if ($SMT == null) {
                        $validSMT = "null";
                    }
                    if (FROMEMAIL == null || FROMEMAIL == "") {
                        $fromMail = 'empty';
                    } else {
                        $fromMail = FROMEMAIL;
                    }
//            echo "finished, But failed to send email with xlsx";
                    error_log("failed to send email for csv upload to: " . $contactEmail . " around: " . date("y-m-d") . " SMT was: " . $validSMT . " FROMEMAIL: " . $fromMail);
                }
            }



            $mailer = Swift_Mailer::newInstance($SMT);
            $emailBody = 'EMMA user upload results are attached in the xlsx file';
            $message = (new Swift_Message("User Upload Results"))
                ->setFrom(FROMEMAIL)
                ->setTo(array('donotreply@emmauser.com'))
                ->setBody($emailBody)
                ->attach(Swift_Attachment::fromPath($resultFile)
                    ->setFilename('upload_result.xlsx'));
            $result = $mailer->send($message);



        } //end if file exists







    }
}
else{
    error_log('No records were found for today');
}