<?php


define('__ROOT__', dirname(dirname(__FILE__)));

require_once(__ROOT__.'/include/db.php');
require_once(__ROOT__.'/process/swiftmailer/swift_required.php');

require (__ROOT__.'/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;

$folderPath = "../../";


//get all uploads that happened in the last 24 hours
$uploadQuery = select_distict_user_csv_files_in_last_24_hours();
if($uploadQuery != null && $uploadQuery->num_rows > 0) {


//loop through uploads {
    for ($i = 0; $i < $uploadQuery->num_rows; $i++) {
        $uploadResult = $uploadQuery->fetch_assoc();



        //check if file exists
        if(file_exists($folderPath . $uploadResult['folder_name'] . '/' . $uploadResult['file_name'])) {

            //create excel sheet for this upload
            $spreadsheet = new Spreadsheet();

            //arrays for spreadsheet
            $userCreationArray = array();
            $userModificationArray = array();
            $userDeletedArray = array();
            $userErrorArray = array();


            //read in csv file
            $csvData = array();
            $row = -1;

            if (($handle = fopen($folderPath . $uploadResult['folder_name'] . '/' . $uploadResult['file_name'], "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
                    $row++;
                    $csvData[$row] = array();
                    $columns = count($data);
                    for ($c = 0; $c < $columns; $c++) {
                        //put csv lines into searchable arrays
                        switch ($c){
                            case 0: {
                                $csvData[$row]['user_email'] = $data[$c];
                                break;
                            }
                            case 1: {
                                $csvData[$row]['user_first_name'] = $data[$c];
                                break;
                            }
                            case 2: {
                                $csvData[$row]['user_middle_name'] = $data[$c];
                                break;
                            }
                            case 3: {
                                $csvData[$row]['user_last_name'] = $data[$c];
                                break;
                            }
                            case 4: {
                                $csvData[$row]['title'] = $data[$c];
                                break;
                            }
                            case 5: {
                                $csvData[$row]['user_phone'] = $data[$c];
                                break;
                            }
                            case 6: {
                                $csvData[$row]['user_landline'] = $data[$c];
                                break;
                            }
                            case 7: {
                                $csvData[$row]['company'] = $data[$c];
                            }
                            case 8: {
                                $csvData[$row]['address'] = $data[$c];
                            }
                            case 9: {
                                $csvData[$row]['city'] = $data[$c];
                                break;
                            }
                            case 10: {
                                $csvData[$row]['province'] = $data[$c];
                                break;
                            }
                            case 11: {
                                $csvData[$row]['country'] = $data[$c];
                                break;
                            }
                            case 12: {
                                $csvData[$row]['zip'] = $data[$c];
                                break;
                            }
                            case 13: {
                                //plan ids
                                $csvData[$row]['plan_name_string'] = $data[$c];

                                $CSVplanArray = array();
                                $CSVplanArray = explode(';', $data[$c]);
                                $csvData[$row]['plan_array'] = $CSVplanArray;
                                break;
                            }
                            case 14: {
                                //group ids
                                $csvData[$row]['group_names'] = $data[$c];
                                $CSVgroupArray = array();
                                $CSVgroupArray = explode(';', $data[$c]);
                                $csvData[$row]['group_array'] = $CSVgroupArray;


                                break;
                            }
                            case 15: {
                                $csvData[$row]['edit'] = $data[$c];
                            }
                            case 16: {
                                $csvData[$row]['delete'] = $data[$c];
                            }
                            default:{
                                //overflow
                                $csvData[$row]['overflow'][] = $data[$c];
                            }
                        }

                    }
                }
                fclose($handle);
            }






            //get all plans for this upload
            $planListQuery = select_plans_with_user_folder($uploadResult['folder_name']);

            $account_plans = array();
            $planListCheck = select_plans_with_user_folder($uploadResult['folder_name']);
            if($planListCheck->num_rows > 0){
                while($planListResult = $planListCheck->fetch_assoc()){
                    $account_plans[] = $planListResult;
                }
            }


            //loop through plans {
            for($j = 0; $j < $planListQuery->num_rows; $j++) {
                $planResults = $planListQuery->fetch_assoc();
                $planID = null;
                $planName = null;
                $planID = $planResults['emma_plan_id'];
                $planName = $planResults['name'];
                $groupListQuery = select_groups_with_planID($planID);


                //users in plan
                $csvUsersInPlan = array();
                for($k = 0; $k < count($csvData); $k++){
                    if(array_search($planName, $csvData[$k]['plan_array'])){
                        $csvUsersInPlan[] = $csvData[$k];
                    }
                }

                //get all users in plan
                $dbUsersInPlan = array();
                $dbUsersInPlanEmails = array();
                $planUserQuery = select_users_in_plan($planID);
                if($planUserQuery->num_rows > 0){
                    while($planUserResult = $planUserQuery->fetch_assoc()){
                        $dbUsersInPlan[] = $planUserResult;
                        $dbUsersInPlanEmails[] = $planUserResult['username'];
                    }
                }
//                else{
//                    $errors[] = 'No users found in plan: '.$planID;
//                    error_log('No users found in plan: '.$planID);
//                }

                //find users that are new
                $newPlanUsers = array();

                for($l = 0; $l < count($csvUsersInPlan); $l++){
                    $L_email = $csvUsersInPlan[$l]['user_email'];
                    if(!array_search($L_email, $dbUsersInPlanEmails)){
                        $newPlanUsers[] = $csvUsersInPlan[$l];
                    }
                }

                //add users that are not in system
                for($m = 0; $m < count($newPlanUsers);$m++){
                    $userEmailQuery = select_users_by_email($newPlanUsers[$m]['user_email']);
                    $userEmailResult = null;
                    if($userEmailQuery->num_rows > 0){
                        //user exists just not in plan
                        //move to next step
                        $userEmailResult = $userEmailQuery->fetch_assoc();
                    }
                    else{
                        //user does not exist
                        //create user
                        $createUser = insert_user_from_csv($newPlanUsers[$m]['user_email'], $newPlanUsers[$m]['user_first_name'],
                            $newPlanUsers[$m]['user_middle_name'], $newPlanUsers[$m]['user_last_name'], $newPlanUsers[$m]['title'],
                            $newPlanUsers[$m]['user_phone'], $newPlanUsers[$m]['user_landline'], $newPlanUsers[$m]['company'],
                            $newPlanUsers[$m]['address'], $newPlanUsers[$m]['city'], $newPlanUsers[$m]['province'], $newPlanUsers[$m]['country'],
                            $newPlanUsers[$m]['zip'], 'CSV API', $uploadResult['user_id'], $planID);
                        if($createUser){
                            $newUserCreation = select_users_by_email($newPlanUsers[$m]['user_email']);
                            if($newUserCreation->num_rows > 0){
                                $userEmailResult = $newUserCreation->fetch_assoc();
                                insert_privileges($userEmailResult['id']);
                            }
                        }
                        else{
                            $userErrorArray[$planID][] = $newPlanUsers[$m];
                        }
                    }
                    //add multiplan entry for user
                    $insertIntoMultiplan = insert_emma_multi_plan_nullExpires($userEmailResult['id'], $planID, 1, '');
                    //add all user groups
                    foreach($newPlanUsers[$m]['group_array'] as $group){
                        $group_id_query = select_group_with_name_and_planid($group, $planID);
                        if($group_id_query->num_rows > 0) {
                            $group_id_result = $group_id_query->fetch_assoc();
                            $insertIntoUserGroups = insert_user_groups($userEmailResult['id'], $group_id_result['emma_group_id']);
                        }

                    }
                    //add created users to excel sheet
                    $userCreationArray[$planID][] = $newPlanUsers[$m];
                }


                //find users that are modify
                $usersToEdit = array();
                foreach($csvUsersInPlan as $csvEditUser){
                    if($csvEditUser['edit'] == 1 || $csvEditUser['edit'] == true || $csvEditUser['edit'] == 'true'){
                        $usersToEdit[] = $csvEditUser;
                    }
                }

                foreach($usersToEdit as $editingUser){
                    $emailIndex = array_search($editingUser['user_email'], $dbUsersInPlanEmails);
                    $editingUserdb = $dbUsersInPlan[$emailIndex];

                    //update user info
                    $updateUserDetails = update_csv_userDetails($editingUserdb['id'], $editingUser['user_first_name'],
                        $editingUser['user_middle_name'], $editingUser['user_last_name'], $editingUser['user_landline'], $editingUser['user_phone'],
                        $editingUser['title'], $editingUser['address'], $editingUser['$city'], $editingUser['province'],
                        $editingUser['country'], $editingUser['zip']);

                    //update/insert user multiplan
                    $multiplanCheckQuery = select_user_with_multiplan($editingUserdb['id'], $planID);
                    if($multiplanCheckQuery->num_rows == 0) {
                        $insertIntoMultiplan = insert_emma_multi_plan_nullExpires($editingUserdb['id'], $planID, 1, '');
                    }

                    //update/insert user groups
                    $groupCheck = select_user_groupList($editingUserdb['id'], $planID);
                    if($groupCheck->num_rows > 0){
                        $userGroupList = array();
                        while($groupCheckResult = $groupCheck->fetch_assoc()){
                            if(array_search($groupCheckResult['name'], $editingUser['group_array'])){
                                //is in group and should be
                                $userGroupList[] = $groupCheckResult['name'];
                            }
                            else{
                                //is in group and shouldn't be
                                $removeGroup = delete_emma_user_groups($editingUserdb['id'], $groupCheckResult['emma_group_id']);
                            }
                        }
                        foreach($editingUser['group_array'] AS $userGroupArrayItem){
                            if(array_search($userGroupArrayItem, $userGroupList)){
                                //is in group and should be
                            }
                            else{
                                //isn't in group and should be
                                $addGroup = insert_user_groups($editingUserdb['id'], $groupCheckResult['emma_group_id']);
                            }
                        }

                    }
                    else{
                        //user is in no groups, insert all
                        foreach($editingUser['group_array'] as $group){
                            $group_id_query = select_group_with_name_and_planid($group, $planID);
                            if($group_id_query->num_rows > 0) {
                                $group_id_result = $group_id_query->fetch_assoc();
                                $insertIntoUserGroups = insert_user_groups($editingUserdb['id'], $group_id_result['emma_group_id']);
                            }
                        }
                    }
                    //add updated users to excel sheet
                    $userModificationArray[] = $editingUser;
                }



                //find user that are delete or missing from list
                $usersToDelete = array();
                foreach($csvUsersInPlan as $csvDeleteUser){
                    if($csvDeleteUser['delete'] == 1 || $csvDeleteUser['delete'] == true || $csvDeleteUser['delete'] == 'true'){
                        $usersToDelete[] = $csvDeleteUser;
                    }
                }

                foreach($usersToDelete as $deletingUser){
                    $emailIndex = array_search($deletingUser['user_email'], $dbUsersInPlanEmails);
                    $deletingUserdb = $dbUsersInPlan[$emailIndex];

                    //remove user from plan, but leave flag for re-enable
                    $delete = true;
                    foreach($account_plans as $aplan){
                        if(!array_search($aplan['name'], $deletingUser['plan_array'])){
                            $delete = false;
                        }

                        //remove user from groups
                        disable_emma_user_groups($deletingUserdb['id'], $aplan['emma_plan_id']);

                    }
                    if($delete){
                        disable_emma_user($deletingUserdb['id']);
                    }

                    //add removed users to excel sheet
                    $userDeletedArray[$planID] = $deletingUser;

                }
            }
            //}

            //build excel workbook
            for($p = 0; $p < count($account_plans); $p++) {
                $aplan = $account_plans[$p];
                //create header for csv section or maybe new page for a workbook
                $myWorkSheet = new PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $aplan['name']);
                $spreadsheet->addSheet($myWorkSheet);

                $spreadsheet->getSheet($p)->setCellValue('A1', $aplan['name']);
                $spreadsheet->getSheet($p)->setCellValue('A3', 'Created Users');
                for($q = 0; $q < count($userCreationArray); $q++){
                    $spreadsheet->getSheet($p)->setCellValue('A'.($q+4), $userCreationArray[$q]['user_email']);
                }

                $point = count($userCreationArray) + 4;


                $spreadsheet->getSheet($p)->setCellValue('A'. $point , 'Edited Users');
                $point++;
                for($q = 0; $q < count($userModificationArray); $q++){
                    $spreadsheet->getSheet($p)->setCellValue('A'.($q+$point), $userModificationArray[$q]['user_email']);
                }

                $point = $point + count($userModificationArray) + 1;

                $spreadsheet->getSheet($p)->setCellValue('A'. $point , 'Deleted Users');
                $point++;
                for($q = 0; $q < count($userDeletedArray); $q++){
                    $spreadsheet->getSheet($p)->setCellValue('A'.($q+$point), $userDeletedArray[$q]['user_email']);
                }

                $point = $point + count($userDeletedArray) + 1;

                $spreadsheet->getSheet($p)->setCellValue('A'. $point , 'Errors');
                $point++;
                for($q = 0; $q < count($userErrorArray); $q++){

                    $spreadsheet->getSheet($p)->setCellValue('A'.($q+$point), 'Username');
                    $spreadsheet->getSheet($p)->setCellValue('B'.($q+$point), 'First Name');
                    $spreadsheet->getSheet($p)->setCellValue('C'.($q+$point), 'Middle Name');
                    $spreadsheet->getSheet($p)->setCellValue('D'.($q+$point), 'Last Name');
                    $spreadsheet->getSheet($p)->setCellValue('E'.($q+$point), 'Title');
                    $spreadsheet->getSheet($p)->setCellValue('F'.($q+$point), 'Mobile');
                    $spreadsheet->getSheet($p)->setCellValue('G'.($q+$point), 'Landline');
                    $spreadsheet->getSheet($p)->setCellValue('H'.($q+$point), 'Company');
                    $spreadsheet->getSheet($p)->setCellValue('I'.($q+$point), 'Address');
                    $spreadsheet->getSheet($p)->setCellValue('J'.($q+$point), 'City');
                    $spreadsheet->getSheet($p)->setCellValue('K'.($q+$point), 'State/Province');
                    $spreadsheet->getSheet($p)->setCellValue('L'.($q+$point), 'Country');
                    $spreadsheet->getSheet($p)->setCellValue('M'.($q+$point), 'Zip');
                    $spreadsheet->getSheet($p)->setCellValue('N'.($q+$point), 'Plans');
                    $spreadsheet->getSheet($p)->setCellValue('O'.($q+$point), 'Groups');
                    $spreadsheet->getSheet($p)->setCellValue('P'.($q+$point), 'Edit');
                    $spreadsheet->getSheet($p)->setCellValue('Q'.($q+$point), 'Delete');


                    $spreadsheet->getSheet($p)->setCellValue('A'.($q+$point), $userErrorArray[$q]['user_email']);
                    $spreadsheet->getSheet($p)->setCellValue('B'.($q+$point), $userErrorArray[$q]['user_first_name']);
                    $spreadsheet->getSheet($p)->setCellValue('C'.($q+$point), $userErrorArray[$q]['user_middle_name']);
                    $spreadsheet->getSheet($p)->setCellValue('D'.($q+$point), $userErrorArray[$q]['user_last_name']);
                    $spreadsheet->getSheet($p)->setCellValue('E'.($q+$point), $userErrorArray[$q]['title']);
                    $spreadsheet->getSheet($p)->setCellValue('F'.($q+$point), $userErrorArray[$q]['user_phone']);
                    $spreadsheet->getSheet($p)->setCellValue('G'.($q+$point), $userErrorArray[$q]['user_landline']);
                    $spreadsheet->getSheet($p)->setCellValue('H'.($q+$point), $userErrorArray[$q]['company']);
                    $spreadsheet->getSheet($p)->setCellValue('I'.($q+$point), $userErrorArray[$q]['address']);
                    $spreadsheet->getSheet($p)->setCellValue('J'.($q+$point), $userErrorArray[$q]['city']);
                    $spreadsheet->getSheet($p)->setCellValue('K'.($q+$point), $userErrorArray[$q]['province']);
                    $spreadsheet->getSheet($p)->setCellValue('L'.($q+$point), $userErrorArray[$q]['country']);
                    $spreadsheet->getSheet($p)->setCellValue('M'.($q+$point), $userErrorArray[$q]['zip']);
                    $spreadsheet->getSheet($p)->setCellValue('N'.($q+$point), $userErrorArray[$q]['plan_name_string']);
                    $spreadsheet->getSheet($p)->setCellValue('O'.($q+$point), $userErrorArray[$q]['group_names']);
                    $spreadsheet->getSheet($p)->setCellValue('P'.($q+$point), $userErrorArray[$q]['edit']);
                    $spreadsheet->getSheet($p)->setCellValue('Q'.($q+$point), $userErrorArray[$q]['delete']);
                }

            }

            $writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
            $writer->save($folderPath.'/'.$uploadResult['folder_name']."/result_".date("YYYY_mm_dd").".xlsx");

            $updatecsvdb = update_csv_uploads($uploadResult['upload_id']);

            //send email to head of plan with excel attachment

            $mailer = Swift_Mailer::newInstance($SMT);
            $emailBody = 'User Upload Results';
            $message = (new Swift_Message("User Upload Results"))
                ->setFrom(FROMEMAIL)
                ->setTo(array($uploadResult['username']))
                ->setBody($emailBody)
                ->attach(Swift_Attachment::fromPath($folderPath.'/'.$uploadResult['folder_name']."/result_".date("YYYY_mm_dd").".xlsx")
                    ->setFilename('upload_result.xlsx'));
            $result = $mailer->send($message);




        }
        else{
            //cant find file.
            //send email to conrad will error file not found, $uploadResult['upload_id']

//            $mailer = Swift_Mailer::newInstance($SMT);
//            $emailBody = 'CSV Upload failed to find file.  file name: '.$uploadResult['upload_id'];
//            $message = (new Swift_Message("CSV Upload failed to find file"))
//                ->setFrom(FROMEMAIL)
//                ->setTo(array('cullrich@think-safe.com'))
//                ->setBody($emailBody);
//            $result = $mailer->send($message);
            echo 'failed to find '. $uploadResult['upload_id'];
        }
// )
    }
}