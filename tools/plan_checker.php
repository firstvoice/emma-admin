<?php

define('__ROOT__', dirname(dirname(__FILE__)));

require_once(__ROOT__.'/include/db.php');
require_once(__ROOT__.'/process/swiftmailer/swift_required.php');
require_once(__ROOT__.'/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Color;

$rootPath = "../../resources/emma/tools/plan_checks";

$data = array();
$errors = array();
$planId = $fvmdb->real_escape_string($_POST['planID']);

//query all uses in plan by multiplan
$multiplanQuery = select_all_plan_users_distinct($planId);

//query all users in plan by user_groups
$groupQuery = select_all_users_in_plan_groups($planId);


//make array of usernames without duplicates from both queries
$userIDs = array();
if($multiplanQuery->num_rows > 0){
    for($i=0; $i<$multiplanQuery->num_rows; $i++){
        $multiplanResult = $multiplanQuery->fetch_assoc();
        $userIDs[] = $multiplanResult['id'];
    }
}
if($groupQuery->num_rows > 0){
    for($i=0; $i<$groupQuery->num_rows; $i++){
        $groupResult = $groupQuery->fetch_assoc();
        if(array_search($groupResult['id'], $userIDs) !== false){
            $userIDs[] = $groupResult['id'];
        }
    }
}


//create array of plan-ids and names
$allPlansQuery = select_all_plan_names_and_ids();


//print out report of all users related to selected plan and thier plans and groups.  highlighting groups without plans as as red and plans without groups as orange.

//user info
//plans name(id)
//groups  plan-name(id); group-name(id)

//make sheet and fill out key
$spreadsheet = new Spreadsheet();

$aplan = $planId;
$rowCounter = 0;
if(count_chars($aplan) > 30){
    $splitarray = str_split($aplan, 30);
    $aplan = $splitarray[0];
}
$aplan = preg_replace('/[^A-Za-z0-9. -]/', '', $aplan);
//create header for csv section or maybe new page for a workbook
try {
    $myWorkSheet = new PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $aplan);
    $spreadsheet->addSheet($myWorkSheet);

    if ($spreadsheet->sheetNameExists('Worksheet')) {
        $sheetIndex = $spreadsheet->getIndex(
            $spreadsheet->getSheetByName('Worksheet')
        );
        $spreadsheet->removeSheetByIndex($sheetIndex);
    }

    $spreadsheet->getSheet(0)->setCellValue('A1', $planId);
    $spreadsheet->getSheet(0)->setCellValue('B1', $aplan);

    $rowCounter = 3;

    //query for all groups in plan
    $groupCountQuery = select_group_user_count($planId);
    if($groupCountQuery->num_rows > 0){
        $spreadsheet->getSheet(0)->setCellValue('A'.$rowCounter, "Group Counts");
        $rowCounter++;
        $spreadsheet->getSheet(0)->setCellValue('A'.$rowCounter, "Group ID");
        $spreadsheet->getSheet(0)->setCellValue('B'.$rowCounter, "Group Name");
        $spreadsheet->getSheet(0)->setCellValue('C'.$rowCounter, "User Count");
        for($i=0; $i<$groupCountQuery->num_rows; $i++){
            $rowCounter++;
            $groupCountResult = $groupCountQuery->fetch_assoc();
//select all users in group
            $usersInGroupQuery = select_all_users_in_group($groupCountResult['emma_group_id']);

            $groupCountMatch = true;
            if($usersInGroupQuery->num_rows != $groupCountResult['userCount']) {
                $groupCountMatch = false;
            }

            if(!$groupCountMatch){
                $spreadsheet->getActiveSheet()->getStyle('B'.$rowCounter)->getFont()->getColor()->setARGB(Color::COLOR_RED);
            }

            $spreadsheet->getSheet(0)->setCellValue('A'.$rowCounter, $groupCountResult['emma_group_id']);
            $spreadsheet->getSheet(0)->setCellValue('B'.$rowCounter, $groupCountResult['name']);
            $spreadsheet->getSheet(0)->setCellValue('C'.$rowCounter, $groupCountResult['userCount']);
        }
    }
    else{
        $spreadsheet->getSheet(0)->setCellValue('A'.$rowCounter, "No groups in this plan.");
    }

    $rowCounter++;


//loop through users array and query all groups and plans



    foreach($userIDs AS $userid){

        $userQuery = select_user_from_userID($userid);
        if($userQuery->num_rows > 0){
            //add to sheet
            $userInfo = $userQuery->fetch_assoc();
            $spreadsheet->getSheet(0)->setCellValue('A'.$rowCounter, $userInfo['username']);
            $spreadsheet->getSheet(0)->setCellValue('B'.$rowCounter, $userInfo['firstname']);
            $spreadsheet->getSheet(0)->setCellValue('C'.$rowCounter, $userInfo['lastname']);
            $rowCounter++;

            //plan query

            $userPlans = select_multiPlans_with_userID($userid);
            if($userPlans->num_rows > 0){
//add to sheet
                $spreadsheet->getSheet(0)->setCellValue('A'.$rowCounter, "Plans");
                $rowCounter++;
                $planinfo = null;
                $columnCount = 'A';
                while($planinfo = $userPlans->fetch_assoc()){
                    $spreadsheet->getSheet(0)->setCellValue($columnCount.$rowCounter, $planinfo['name']."(".$planinfo['plan_id'].")");
                    $columnCount++;
                }
                $rowCounter++;
            }

//group query
            $userGroups = select_all_users_group_info($userid);
            if($userGroups->num_rows > 0){
//add to sheet

                $spreadsheet->getSheet(0)->setCellValue('A'.$rowCounter, "Groups");
                $rowCounter++;
                $groupinfo = null;
                $columnCount = 'A';
                while($groupinfo = $userGroups->fetch_assoc()){
                    $spreadsheet->getSheet(0)->setCellValue($columnCount.$rowCounter, $groupinfo['name']."(".$groupinfo['plan_id'].")");
                    $columnCount++;
                }
                $rowCounter++;
            }
        }

    }

}
catch (PhpOffice\PhpSpreadsheet\Exception $e){
    global $errors;
    $errors[] = 'Failed to create spreadsheet with title'. $aplan;
    error_log($e->getMessage());
}
catch (Exception $e){
    error_log($e->getMessage());
}




