<?php

require getcwd() . '/vendor/autoload.php';
require('include/db.php');
include('include/processing.php');



$plan = $_POST['plan'];
$filename = '';
$csv = '';
$csvbr = "\n";
$csvsp = ',';
$ext = '.csv';

include('process/reportTypes/' . $_POST['report']. '.php');


if($ext == '.csv') {
    $reportName = '_' . $_POST['report'];
    $fp = fopen('reports/report' . $reportName . $ext, 'w');
    fwrite($fp, $csv);
    fclose($fp);


    $downloadName='report' . $reportName . $ext;
    $download_file = getcwd() . '/reports/' . $downloadName;

    if(file_exists($download_file))
    {
        if($ext == '.csv') {
            header('Content-Type: application/download');
            header('Content-Disposition: attachment; filename="' . basename($download_file) . '"');
            header('Content-Transfer-Encoding: binary');
            readfile($download_file);
        }
        else{
            echo 'File invalid';
        }

    }
    else
    {
        echo 'File does not exists on given path';
    }
}
?>