<?php include('include/pre_login_header.php'); ?>
<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Login</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #FFFFFF;">


<?php include('include/index_top_bar.php'); ?>


<div class="row expanded the-clock" style="height: 100%">
     <div class="large-12 medium-12 small-12 columns" style="padding-top: 2em">
        <div class="row expanded" style="padding-bottom: 1em">
            <div class="large-2 medium-4 small-4 column">
                <div class="clear_card">
                    <a href="case_study.php"><i class="fas fa-folder-open fa-3x"></i>
                    <div class="card_section">
                        <p>Case<br />Studies</p>
                    </div>
                    </a>
                </div>
            </div>
            <div class="large-2 medium-4 small-4 column">
                <div class="clear_card">
                    <a href="product_sheets.php"><i class="fas fa-file-alt fa-3x"></i>
                    <div class="card_section">
                        <p>Product <br /> Sheets</p>
                    </div>
                    </a>
                </div>
            </div>

            <div class="large-2 medium-4 small-4 column">
                <div class="clear_card">
                    <a href="help_center.php"><i class="fas fa-question fa-3x"></i>
                    <div class="card_section">
                        <p>Help <br /> Center</p>
                    </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="large-12 medium-12 small-12 columns">
            <div class="row expanded" style="padding-bottom: 1em">
                <div class="large-2 medium-4 small-4 column">
                    <div class="clear_card">
                        <a href="emma_videos.php"><i class="fas fa-play-circle fa-3x"></i>
                        <div class="card_section">
                            <p>Videos</p>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="large-2 medium-4 small-4 column">
                    <div class="clear_card">
                        <a href="technical_sheets.php"><i class="fas fa-cogs fa-3x"></i>
                            <p>Technical Sheets</p>
                        </a>
                    </div>
                </div>

                <div class="large-2 medium-4 small-4 column">
                    <div class="clear_card">
                        <a data-open="pricing-modal" class="typebutton"
                              data-type="Pricing Requested - Resources"><i class="fas fa-dollar-sign fa-3x"></i>
                        <div class="card_section">
                            <p>Pricing</p>
                        </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php include('include/footer.php'); ?>
</div>
<?php include('include/pre_login_modals.php'); ?>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>


