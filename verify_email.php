<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/14/2017
 * Time: 6:55 PM
 */

session_start();
require('include/db.php');
$USER = array();
$content = 'verify_email';

?>

<html class="no-js" lang="en">
<head>
  <title>EMMA | User Setup</title>
  <?php include('include/head.php'); ?>
  <link rel="stylesheet" href="css/<?php echo $content; ?>.css"/>

</head>
<body>
<div class="app-dashboard shrink-large">
  <div class="row expanded app-dashboard-top-nav-bar">
    <div class="columns medium-2">
      <button data-toggle="app-dashboard-sidebar"
        class="menu-icon hide-for-medium"></button>
      <a href="./dashboard.php?content=home" class="app-dashboard-logo"><img src="img/emma_icon.png" style="width:20px;height:20px;" /> EMMA</a>
    </div>
    <div class="columns show-for-medium">
    </div>
  </div>
  <div class="app-dashboard-body">
    <div class="app-dashboard-body-content"
      data-off-canvas-content>
      <div class="content-container-div">
        <div class="content-body-div">
          <?php include('./content/' . $content . '.php'); ?>
        </div>

        <div class="content-footer-div">
          <div class="text-center">
            <p>Copyright Think Safe, Inc. 2017 Patent Pending</p>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<div id="thinking-modal" class="reveal callout text-center tiny" data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4 style="color:#e5e5e5;">Working...</h4>
  <img style="width:2rem;height:2rem;" src="img/ajax_loader_blue.gif"/>
</div>
<div id="success-modal" class="reveal callout success text-center tiny"
  data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>User Activated!</h4>
  <p>You can now log in to the mobile application.</p>
  <p id="admin-login-info">You can log in to the administrator portal <a href="./dashboard.php">here</a></p>
  <a href="./verify_success.php" data-close="" class="button success">OK</a>
</div>
<div id="fail-modal" class="reveal callout alert text-center tiny" data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4 style="color:darkred">Failed...</h4>
  <div id="error-list" class="text-left" style="padding:1rem;"></div>
  <a class="button alert" data-close>OK</a>
</div>

</body>

<?php include('include/scripts.php'); ?>
<script src="js/<?php echo $content; ?>.js"></script>


</html>