<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 11/26/2019
 * Time: 11:23 AM
 */

require('include/db.php');


$password = 'password';   //New password to be changed to

$userid = '8746';   //User Id of person whose password is being changed


if(!empty($password) && !empty($userid)) {

    $hashedPassword = hash("sha512", $password);
    $result = $emmadb->query("
        UPDATE users u
        SET u.big_password = '" . $hashedPassword . "',
        u.password = password('" . $password . "')
        WHERE u.id = '" . $userid . "'
    ");
    if ($result) {
        $oldpassword = $emmadb->query("
            INSERT INTO emma_old_passwords (user_id, password_hash, date_created) VALUES ('". $userid ."', password('". $password ."'), '". date('Y-m-d H:i:s') ."')
        ");
        $users = $emmadb->query("
            SELECT u.*
            FROM users u 
            WHERE u.id = '". $userid ."'
        ");
        $user = $users->fetch_assoc();
        echo'Successfully changed '. $user['username'] .'\'s password to: '. $password;
    }else{
        echo'The query didn\'t run correctly';
    }
}else{
    echo'You forgot to fill in User Id or Password!';
}