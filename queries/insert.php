<?php

function insert_new_plan($name,$active,$expires,$max,$mass,$security,$geofences,$panic,$duration,$maxgeo){
    global $emmadb;
    $insertPlan = $emmadb->query("
        INSERT INTO emma_plans (
        `name`, 
        date_active, 
        date_expired, 
        created_date, 
        max_sites, 
        mass_communication, 
        securities, 
        geofencing, 
        panic, 
        sos_duration
        ) VALUES (
        '". $name ."',
        '". $active ."',
        '". $expires ."',
        '". date('Y-m-d H:i:s') ."',
        '". $max ."',
        '". ($mass == 'on' ? "1" : "0") ."',
        '". ($security == 'on' ? "1" : "0") ."',
        '". ($geofences == 'on' ? "1" : "0") ."',
        '". ($panic == 'on' ? "1" : "0") ."',
        '". $duration ."'
        )
    ");
    if($insertPlan) {
        return $emmadb->insert_id;
    }else{
        return 'error';
    }
}

function insert_log_received($user,$id,$type){
    global $emmadb;
    $insertPlan = $emmadb->query("
        INSERT INTO emma_alert_received (
          user_id, alert_type, alert_id, date_received
        ) VALUES (
          '". $user ."','". $type ."','". $id ."','". date('Y-m-d H:i:s') ."'
        )
    ");
    if($insertPlan) {
        return $emmadb->insert_id;
    }else{
        return 'error';
    }
}

function insert_plan_sites($planId,$name,$address,$city,$state,$zip,$first,$last,$email,$phone){
    global $emmadb;
    for($i=0; $i<count($name); $i++) {
        $insertSiteContacts = $emmadb->query("
            INSERT INTO emma_site_contacts (
            emma_plan_id, 
            first_name, 
            last_name, 
            email, 
            phone, 
            active
            ) VALUES (
            '". $planId ."',
            '". $first[$i] ."',
            '". $last[$i] ."',
            '". $email[$i] ."',
            '". $phone[$i] ."',
            '1'
            )
        ");
        $contactId = $emmadb->insert_id;
        $insertSites = $emmadb->query("
            INSERT INTO emma_sites (
            emma_plan_id, 
            emma_site_name, 
            emma_site_street_address, 
            emma_site_city, 
            emma_site_state, 
            emma_site_zip, 
            emma_site_notes, 
            emma_site_contact_id
            )VALUES(
            '". $planId ."',
            '". $name[$i] ."',
            '". $address[$i] ."',
            '". $city[$i] ."',
            '". $state[$i]."',
            '". $zip[$i] ."',
            '',
            '". $contactId ."'
            )
        ");
    }
    if($insertSiteContacts && $insertSites){
        return '';
    }else{
        return 'error';
    }
}

function insert_plan_groups($planId,$name,$info,$admin,$security,$geofences,$sos,$admin911){
    global $emmadb;
    for($i=0; $i<count($name); $i++){
        $insertGroup = $emmadb->query("
            INSERT INTO emma_groups (
            emma_plan_id, 
            name, 
            info_only, 
            admin, 
            security, 
            geofences, 
            emma_sos, 
            `911admin`
            )VALUE (
            '". $planId ."',
            '". $name[$i] ."',
            '". ($info[$i] == 'on' ? "1" : "0") ."',
            '". ($admin[$i] == 'on' ? "1" : "0") ."',
            '". ($security[$i] == 'on' ? "1" : "0") ."',
            '". ($geofences[$i] == 'on' ? "1" : "0") ."',
            '". ($sos[$i] == 'on' ? "1" : "0") ."',
            '". ($admin911[$i] == 'on' ? "1" : "0") ."'
            )
        ");
    }
    if($insertGroup){
        return '';
    }else{
        return 'error';
    }
}

function insert_plan_assets($planId,$type,$lat,$lon,$user){
    global $emmadb;
    for($i=0; $i<count($type); $i++){
        $insertAsset = $emmadb->query("
            INSERT INTO emma_assets (
            emma_plan_id, 
            emma_asset_type_id, 
            created_by_id, 
            created_date, 
            latitude, 
            longitude
            ) VALUES (
            '". $planId ."',
            '". $type[$i] ."',
            '". $user ."',
            '". date('Y-m-d H:i:s') ."',
            '". $lat[$i] ."',
            '". $lon[$i] ."'
            )
        ");
    }
}
function insert_emma_assets($planID, $typeID, $lat, $lng, $userID, $address, $deleted, $active){
    global $emmadb;
    $result = $emmadb->query("
        insert into emma_assets (
          emma_plan_id, 
          emma_asset_type_id, 
          created_by_id, 
          created_date, 
          latitude, 
          longitude,
          address,
          deleted,
          active
        ) values (
          '" . $planID . "',
          '" . $typeID . "',
          '" . $userID . "',
          '" . date('Y-m-d H:i:s', time()) . "',
          '" . $lat . "',
          '" . $lng . "',
          '".$address."',
          '".$deleted."',
          '".$active."'
        )
    ");
    return $result;
}
function insert_emma_sites($planID, $name, $address, $city, $state, $zip, $notes, $contact, $lat, $lng){
    global $emmadb;
    $result = $emmadb->query("
    INSERT INTO emma_sites (
      emma_plan_id, 
      emma_site_name, 
      emma_site_street_address, 
      emma_site_city, 
      emma_site_state, 
      emma_site_zip, 
      emma_site_notes, 
      emma_site_contact_id,
      emma_site_latitude,
      emma_site_longitude
    ) VALUES (
      '" . $planID . "',
      '" . $name . "',
      '" . $address . "',
      '" . $city . "',
      '" . $state . "',
      '" . $zip . "',
      '" . $notes . "',
      '" . $contact . "',
      '".$lat."',
      '".$lng."'
    )
    ");
    return $result;
}
function insert_emma_site_contacts($planID, $firstName, $lastName, $email, $phone, $active){
    global $emmadb;
    $result = $emmadb->query("
            INSERT INTO emma_site_contacts (
              emma_plan_id, 
              first_name, 
              last_name, 
              email, 
              phone, 
              active
            ) VALUES (
              '" . $planID . "',
              '" . $firstName . "',
              '" . $lastName . "',
              '" . $email . "',
              '" . $phone . "',
              '" . $active . "'
            )
          ");
    return $result;
}
function insert_plan_emergencies($planId,$type){
    global $emmadb;
    for($i=0; $i<count($type); $i++){
        $insertEmergencies = $emmadb->query("
            INSERT INTO emma_plan_event_types (
            emma_plan_id, 
            emma_emergency_type_id
            )VALUES (
            '". $planId ."',
            '". $type[$i] ."'
            )
        ");
    }
}

function insert_emma_notification_clients($subscription,$user_id, $category){
    global $emmadb;
    $result = $emmadb->query("
          INSERT IGNORE INTO emma_notification_clients (
            client_id,
            user_id,
            category,
            created_date
          ) VALUES (
            '" . $subscription . "',
            '". $user_id ."',
            '".$category."',
            NOW()
          )
      ");
    return $result;
}

function insert_geofence_messages($fenceID, $description, $comment, $userID, $planID){
    global $emmadb;
    $result = $emmadb->query("
        INSERT INTO emma_geofence_messages (
            geofence_id, 
            description, 
            comments, 
            created_by_id, 
            created_date,
            plan_id
            ) 
        VALUES 
            ('". $fenceID ."',
            '" . $description . "',
            '" . $comment . "',
            '" . $userID . "',
            '" . date('Y-m-d H:i:s') . "',
            '". $planID ."'
            )
    ");
    return $result;
}

function insert_students_to_guardians($studentID, $studentName, $parentID){
    global $emmadb;
    $result = $emmadb->query("
        INSERT INTO emma_students_to_guardians (student_id, student_name, guardian_id) 
        VALUES ('". $studentID ."', '". $studentName ."', '". $parentID ."')
    ");
    return $result;
}

//function insert_system_messages($eventId, $message){
//    global $fvmdb;
//    $result = $fvmdb->query("
//        INSERT INTO emma_system_messages (
//          emergency_id,
//          message,
//          created_date,
//          type
//        ) VALUES (
//          '" . $eventId . "',
//          'System Alert: " . $message . "',
//          '" . date('Y-m-d H:i:s') . "',
//          '2'
//        )
//    ");
//    return $result;
//}
function insert_system_messages($eventId, $userID, $message, $type, $description, $comments){
    global $emmadb;
    $result = $emmadb->query("
        INSERT INTO emma_system_messages (
        emergency_id, 
        message, 
        created_by_id,
        created_date, 
        type,
        description,
        comments
        ) VALUES (
        '" . $eventId . "', 
        '" . $message . "', 
        '" . $userID . "', 
        '" . date('Y-m-d H:i:s') . "', 
        '" . $type . "',
        '" . $description . "',
        '" . $comments . "'
        )
    ");
    return $result;
}
function insert_emma_system_message_groups($messageID, $groupID){
    global $emmadb;
    $result = $emmadb->query("
        insert into emma_system_message_groups(emma_system_message_id, emma_group_id) 
        values('" . $messageID . "', '" . $groupID . "')
    ");
    return $result;
}

function insert_user($username, $password, $first, $last, $phone, $company, $address, $city, $province, $country, $zip, $creatorIP, $creatorID, $planID){
    global $emmadb;
    $result = $emmadb->query("
        INSERT INTO users (
            username, password, firstname, lastname, 
            phone, company, address, city, province, 
            country, zip, creatorip, creator, creation, 
            auth, emma_plan_id
        ) VALUES (
            '" . $username . "', PASSWORD('" . $password . "'), '" . $first . "', '" . $last . "', 
            '" . $phone . "', '" . $company . "', '" . $address . "', '" . $city . "', '" . $province . "', 
            '" . $country . "', '" . $zip . "', '" . $creatorIP . "', '" . $creatorID . "', '" . date("Y-m-d H:i:s") . "', 
            SHA1('" . $username . "'), '". $planID ."'
        )
    ");
    return $result;
}
function insert_user_full($username, $password, $type, $mobile_type, $ems_type, $title, $firstname, $middlename, $lastname,
                          $phone, $company, $preferredlanguage, $address, $city, $province, $country, $zip, $firstlogin, $filterorg,
                          $filterlocation, $creator, $creatorip, $sales_person_id, $creation, $updated, $display, $private_program_head, $pad_program, $email_alerts,
                          $text_alerts, $receive_notifications, $notification_distance, $map_type, $program_details, $needs_assistance,
                          $emma_plan_id, $emma_pin, $emma_subscription_id, $auth, $landline_phone){
    global $emmadb;
    $result = $emmadb->query("
    INSERT INTO users (
      username,
      password, 
      type,
      mobile_type,
      ems_type,
      title,
      firstname,
      middlename,
      lastname,
      phone,
      company,
      preferredlanguage,
      address,
      city,
      province,
      country,
      zip,
      firstlogin,
      filterorg,
      filterlocation,
      creator,
      creatorip,
      sales_person_id,
      creation,
      updated,
      display,
      private_program_head,
      pad_program,
      email_alerts,
      text_alerts,
      receive_notifications,
      notification_distance,
      map_type,
      program_details,
      needs_assistance,
      emma_plan_id,
      emma_pin,
      emma_subscription_id,
      auth,
      landline_phone
    ) VALUES (
      '".$username."',
      '".$password."',
      '".$type."',
      '".$mobile_type."',
      '".$ems_type."',
      '".$title."',
      '".$firstname."',
      '".$middlename."',
      '".$lastname."',
      '".$phone."',
      '".$company."',
      '".$preferredlanguage."',
      '".$address."',
      '".$city."',
      '".$province."',
      '".$country."',
      '".$zip."',
      '".$firstlogin."',
      '".$filterorg."',
      '".$filterlocation."',
      '".$creator."',
      '".$creatorip."',
      '".$sales_person_id."',
      '".$creation."',
      '".$updated."',
      '".$display."',
      '".$private_program_head."',
      '".$pad_program."',
      '".$email_alerts."',
      '".$text_alerts."',
      '".$receive_notifications."',
      '".$notification_distance."',
      '".$map_type."',
      '".$program_details."',
      '".$needs_assistance."',
      '".$emma_plan_id."',
      '".$emma_pin."',
      '".$emma_subscription_id."',
      sha1('" . $username . "'),
      '".$landline_phone."'
        )
    ");
    return $result;
}
function insert_privileges($userID){
    global $emmadb;
    $result = $emmadb->query("
            INSERT INTO privileges (userid) 
            VALUES ('" . $userID . "')
        ");
    return $result;
}
function insert_emma_multi_plan($userID, $emmaPlanID, $expires, $active, $guest_code){
    global $emmadb;
    $result = $emmadb->query("
            INSERT INTO emma_multi_plan (
            user_id, 
            plan_id, 
            expires, 
            active, 
            guest_code
            ) VALUES (
            '". $userID ."',
            '". $emmaPlanID ."',
            '". $expires ."',
            '". $active ."',
            '". $guest_code ."'
            )
        ");
    return $result;
}
function insert_emma_multi_plan_nullExpires($userID, $emmaPlanID, $active, $guest_code){
    global $emmadb;
    $result = $emmadb->query("
            INSERT INTO emma_multi_plan (
            user_id, 
            plan_id,  
            active, 
            guest_code
            ) VALUES (
            '". $userID ."',
            '". $emmaPlanID ."',
            '". $active ."',
            '". $guest_code ."'
            )
        ");
    return $result;
}
function insert_user_groups($userID, $groupID){
    global $emmadb;
    $result = $emmadb->query("
            INSERT INTO emma_user_groups (user_id, emma_group_id) 
            VALUES ('". $userID ."', '". $groupID ."')
        ");
    return $result;
}
function insert_user_plans($userID, $planID){
    global $emmadb;

//    $delete = $emmadb->query("
//        UPDATE emma_multi_plan
//        SET active = FALSE
//        WHERE user_id = '". $userID ."'
//    ");
//    if($delete){
        $result = $emmadb->query("
                INSERT INTO emma_multi_plan (user_id, plan_id) 
                VALUES ('". $userID ."', '". $planID ."')
            ");
        return $result;
//    }else{
//        return $delete;
//    }
}
function insert_emergency_received_groups($emergencyID, $groupID){
    global $emmadb;
    $result = $emmadb->query("
          INSERT INTO emergency_received_groups (emergency_id, emma_group_id) 
          VALUES('" . $emergencyID . "', '" . $groupID . "')
        ");
    return $result;
}
function insert_communication_received_groups($communicationID, $groupID){
    global $emmadb;
    $result = $emmadb->query("
        INSERT INTO communication_received_groups (communication_id, emma_group_id) 
        VALUES ('".$communicationID."','".$groupID."')
    ");
    return $result;
}
function insert_emma_notification_device_groups($deviceGroupString, $deviceKeyID){
    global $emmadb;
    $result = $emmadb->query("
                INSERT INTO emma_notification_device_groups (notification_key_name, notification_key, last_updated)
                VALUES ('".$deviceGroupString."', '".$deviceKeyID."', NOW())
            ");
    return $result;
}
function insert_emma_mass_communications($planID, $userID, $notification, $response, $time){
    global $emmadb;
    $result = $emmadb->query("
    INSERT INTO emma_mass_communications (
      emma_plan_id,
      created_by_id,
      created_date,
      notification,
      allow_response,
      response_time_minutes
    ) VALUES (
      '" . $planID . "',
      '" . $userID . "',
      '" . date('Y-m-d H:i:s') . "',
      '" . $notification . "',
      '" . $response . "',
      '" . $time . "'
    )
  ");
    return $result;
}
function insert_emma_scripts($typeID, $planID, $groupID, $broadcastID, $name, $text){
    global $emmadb;
    $result = $emmadb->query("
        insert into emma_scripts (
        emma_script_type_id, 
        emma_plan_id, 
        emma_script_group_id, 
        emma_broadcast_type_id, 
        name, 
        text
        ) VALUES (
        '". $typeID ."',
        '". $planID ."',
        '". $groupID ."',
        '". $broadcastID ."',
        '". $name ."',
        '". $text ."'
        )
    ");
    return $result;
}
function insert_notification_script($planID, $name, $text){
    global $emmadb;
    $result = $emmadb->query("
        insert into emma_mass_notification_scripts (
        emma_plan_id,  
        name, 
        text
        ) VALUES (
        '". $planID ."',
        '". $name ."',
        '". $text ."'
        )
    ");
    return $result;
}
function insert_email_script($planID, $name, $text){
    global $emmadb;
    $result = $emmadb->query("
        insert into emma_email_scripts (
        plan_id,  
        name, 
        script
        ) VALUES (
        '". $planID ."',
        '". $name ."',
        '". $text ."'
        )
    ");
    return $result;
}
function insert_text_script($planID, $name, $text){
    global $emmadb;
    $result = $emmadb->query("
        insert into emma_text_scripts (
        plan_id,  
        name, 
        script
        ) VALUES (
        '". $planID ."',
        '". $name ."',
        '". $text ."'
        )
    ");
    return $result;
}
function insert_emma_scripts_with_massNotificationType($typeID, $planID, $groupID, $broadcastID, $name, $text, $massNoticationTypeID){
    global $emmadb;
    $result = $emmadb->query("
    insert into emma_scripts (
      emma_plan_id, 
      emma_script_type_id, 
      `name`, 
      text,
      emma_script_group_id,
      emma_broadcast_type_id,
      emma_mass_notification_type_id
    ) values (
      '" . $planID . "',
      '" . $typeID . "',
      '" . $name . "',
      '" . $text . "',
      '" . $groupID . "',
      '".$broadcastID."',
      '". $massNoticationTypeID ."'      
    )
  ");
    return $result;
}
function insert_emma_security_messages($securityID, $userID, $message, $description, $comments){
    global $emmadb;
    $result = $emmadb->query("
        INSERT INTO emma_security_messages (
        security_id, 
        message, 
        created_by_id,
        created_date,
        description,
        comments
        ) VALUES (
        '" . $securityID . "', 
        '" . $message . "', 
        '" . $userID . "',
        '" . date('Y-m-d H:i:s') . "',
        '" . $description . "',
        '" . $comments . "'
        )
    ");
    return $result;
}
function insert_emma_security_messages_typeWithoutDescriptionComments($securityID, $userID, $message, $date, $type){
    global $emmadb;
    $result = $emmadb->query("
        INSERT INTO emma_security_messages (
          security_id, 
          message, 
          created_by_id,
          created_date, 
          type
        ) VALUES (
          '" . $securityID . "', 
          '" . $message . "', 
          '" . $userID . "', 
          '" . $date . "', 
          '".$type."'
        )
    ");
    return $result;
}
function insert_emma_call_reports($securityID, $reportPost, $reportNum, $planID, $siteID, $officerName, $incidentDescription,
                                  $resultingAction, $policeYN, $policeTime, $policeName, $resLifeYN, $resLifeTime, $resLifeName,
                                  $otherNotified, $otherYN, $otherTime, $otherName){
    global $emmadb;
    $result = $emmadb->query("
      insert into emma_call_reports(
        security_id,
        report_post,
        report_number,
        plan_id,
        report_date,
        site_id,
        officer_name,
        incident_description,
        incident_followup,
        police_yn,
        police_time,
        police_name,
        reslife_yn,
        reslife_time,
        reslife_name,
        other_service,
        other_service_yn,
        other_service_time,
        other_service_name
      ) VALUES (
        '" . $securityID . "',
        '" . $reportPost . "',
        '" . $reportNum . "',
        '" . $planID . "',
        '" . date('Y-m-d H:i:s') . "',
        '" . $siteID . "',
        '" . $officerName . "',
        '" . $incidentDescription . "',
        '" . $resultingAction . "',
        '" . $policeYN . "',
        '" . $policeTime . "',
        '" . $policeName . "',
        '" . $resLifeYN . "',
        '" . $resLifeTime . "',
        '" . $resLifeName . "',
        '" . $otherNotified . "',
        '" . $otherYN . "',
        '" . $otherTime . "',
        '" . $otherName . "'
      )
    ");
    return $result;
}
function insert_emma_call_reports_basic($reportNum, $planID, $reportDate, $siteID, $incidentDescription, $resultingAction){
    global $emmadb;
    $result = $emmadb->query("
      insert into emma_call_reports(
        report_number,
        plan_id,
        report_date,
        site_id,
        incident_description,
        incident_followup
      ) VALUES (
        '" . $reportNum . "',
        '" . $planID . "',
        '" . $reportDate . "',
        '" . $siteID . "',
        '" . $incidentDescription . "',
        '" . $resultingAction . "'
      )
    ");
    return $result;
}
function insert_emma_call_report_witness($type, $name, $phone, $org, $reportNum){
    global $emmadb;
    $result = $emmadb->query("
        insert into emma_call_report_witness(
          type,
          name,
          phone,
          org,
          call_report_number
        )VALUES (
          '" . $type . "',
          '" . $name . "',
          '" . $phone . "',
          '" . $org . "',
          '" . $reportNum . "'
        )
    ");
    return $result;
}
function insert_emma_sos_call_reports($sosID, $officerName, $incidentDescription, $resultingAction){
    global $emmadb;
    $result = $emmadb->query("
      insert into emma_sos_call_reports(
        emma_sos_id, 
        officer_name,
        incident_description,
        incident_followup
      ) VALUES (
        '" . $sosID . "',
        '" . $officerName . "',
        '" . $incidentDescription . "',
        '" . $resultingAction . "'
      )
    ");
    return $result;
}
function insert_emma_security($planID, $siteID, $securityTypeID, $createdByID, $createdDate, $pin,
                              $latitude, $longitude, $active, $closed_by_id, $closed_date, $description, $comments){
    global $emmadb;
    $result = $emmadb->query("
    INSERT INTO emma_securities (
      emma_plan_id,
      emma_site_id,
      emma_security_type_id,
      created_by_id,
      created_date,
      created_pin,
      latitude,
      longitude,
      active,
      closed_by_id,
      closed_date,
      description,
      comments
    ) VALUES (
      '" . $planID . "',
      '" . $siteID . "',
      '" . $securityTypeID . "',
      '" . $createdByID . "',
      '" . $createdDate . "',
      '" . $pin . "',
      '" . $latitude . "',
      '" . $longitude . "',
      '".$active."',
      '".$closed_by_id."',
      '".$closed_date."',
      '" . $description . "',
      '" . $comments . "'
    )
  ");
    return $result;
}
function insert_emma_assets_custom_info($assetID, $date, $info, $dropdown){
    global $emmadb;
    $result = $emmadb->query("
            INSERT INTO emma_assets_custom_info
            (emma_asset_id, date_time, info, dropdown)
            VALUES 
            ('".$assetID."',".$date.",".$info.",'".$dropdown."')
            ");
    return $result;
}
function insert_emma_asset_types($assetIconID, $name, $planID){
    global $emmadb;
    $result = $emmadb->query("
    INSERT INTO emma_asset_types (
        emma_asset_icon_id,
        name,
        emma_plan_id
        )
        VALUES(
        '".$assetIconID."',
        '".$name."',
        '".$planID."'    
        )
    ");
    return $result;
}
function insert_emma_assets_dropdown_options($typeID, $name, $display){
    global $emmadb;
    $result = $emmadb->query("
      INSERT INTO emma_assets_dropdown_options
        (
          asset_type_id,
          Name,
          display
        )VALUES(
          '" . $typeID . "',
          '" . $name . "',
          ".$display."      
        )          
    ");
    return $result;
}
function insert_emma_asset_type_display($assetTypeID, $name, $date, $dropdownID, $fillin, $active){
    global $emmadb;
    $result = $emmadb->query("
    INSERT INTO emma_asset_type_display
      (
        asset_type_id,
        name,
        date,
        dropdown_id,
        fillin,
        active
       )VALUES(
        '" . $assetTypeID . "',
        '" . $name . "',
        ".$date.",
        '" . $dropdownID . "',
        ".$fillin.",
        ".$active."
      )
    ");
    return $result;
}
function insert_emergencies($planID, $emergencyID, $siteID, $isDrill, $typeID, $subTypeID, $description, $active,
                            $latitude, $longitude, $address, $comments, $created_date, $userID, $pin, $closed_date, $closed_by_id){
    global $emmadb;
    $result = $emmadb->query("
      INSERT INTO emergencies (
        emma_plan_id, 
        parent_emergency_id,
        emma_site_id, 
        drill, 
        emergency_type_id, 
        emergency_sub_type_id, 
        description, 
        active, 
        latitude, 
        longitude, 
        nearest_address, 
        comments, 
        created_date, 
        created_by_id, 
        created_pin, 
        closed_date, 
        closed_by_id
      ) VALUES (
        '" . $planID . "', 
        '" . $emergencyID . "',
        '" . $siteID . "', 
        " . $isDrill . ", 
        '" . $typeID . "', 
        '" . $subTypeID . "', 
        '" . $description . "', 
        '".$active."', 
        '" . $latitude . "', 
        '" . $longitude . "', 
        '" . $address . "', 
        '" . $comments . "', 
        '" . $created_date . "', 
        '" . $userID . "', 
        '" . $pin . "', 
        '".$closed_date."',
        '".$closed_by_id."'
      )
    ");
    return $result;
}
function insert_emergencies_nullParent($planID, $siteID, $isDrill, $typeID, $subTypeID, $description, $active,
                            $latitude, $longitude, $address, $comments, $created_date, $userID, $pin, $closed_date, $closed_by_id){
    global $emmadb;
    $result = $emmadb->query("
      INSERT INTO emergencies (
        emma_plan_id, 
        emma_site_id, 
        drill, 
        emergency_type_id, 
        emergency_sub_type_id, 
        description, 
        active, 
        latitude, 
        longitude, 
        nearest_address, 
        comments, 
        created_date, 
        created_by_id, 
        created_pin, 
        closed_date, 
        closed_by_id
      ) VALUES (
        '" . $planID . "', 
        '" . $siteID . "', 
        " . $isDrill . ", 
        '" . $typeID . "', 
        '" . $subTypeID . "', 
        '" . $description . "', 
        '".$active."', 
        '" . $latitude . "', 
        '" . $longitude . "', 
        '" . $address . "', 
        '" . $comments . "', 
        '" . $created_date . "', 
        '" . $userID . "', 
        '" . $pin . "', 
        '".$closed_date."',
        '".$closed_by_id."'
      )
    ");
    return $result;
}
function insert_emergency_responses($emergencyID, $userID, $status, $comments, $mobile, $createdDate, $updatedDate,
                            $latitude, $longitude, $deviceType, $pin, $validatedID, $validatedTime){
    global $emmadb;
    $result = $emmadb->query("
    INSERT INTO emergency_responses
    (
    emergency_id,
    user_id,
    status,
    comments,
    mobile,
    created_date,
    updated_date,
    latitude,
    longitude,
    device_type,
    pin,
    validated_id,
    validated_time
    )
    VALUES(
    '" . $emergencyID . "',
    '" . $userID . "',
    '" . $status . "',
    '" . $comments . "',
    '".$mobile."',
    '" . $createdDate . "',
    '" . $updatedDate . "',
    '" . $latitude . "',
    '" . $longitude . "',
    '".$deviceType."', 
    '" . $pin . "',
    '" . $validatedID . "',
    '" . $validatedTime . "'
    )
    ");
    return $result;
}
function insert_emma_geofence_locations($planID, $name, $type, $clat, $clng, $radius, $nwlat, $nwlng, $nelat, $nelng, $selat, $selng, $swlat, $swlng,
                         $mainFence, $createdDate, $createdBy){
    global $emmadb;
    $result = $emmadb->query("
    INSERT INTO emma_geofence_locations (
        plan_id, 
        fence_name, 
        type, 
        center_lat, 
        center_lng, 
        radius, 
        nw_corner_lat, 
        nw_corner_lng, 
        ne_corner_lat, 
        ne_corner_lng, 
        se_corner_lat, 
        se_corner_lng, 
        sw_corner_lat, 
        sw_corner_lng, 
        main_fence, 
        created_date, 
        created_by
        ) 
    VALUES 
        ('". $planID ."',
        '" . $name . "',
        '" . $type . "',
        '" . $clat . "',
        '" . $clng . "',
        '" . $radius . "',
        '" . $nwlat . "',
        '" . $nwlng . "',
        '" . $nelat . "',
        '" . $nelng . "',
        '" . $selat . "',
        '" . $selng . "',
        '" . $swlat . "',
        '" . $swlng . "',
        '" . $mainFence . "',
        '". $createdDate ."',
        '". $createdBy ."'
        )
    ");
    $data['lastid'] = $emmadb->insert_id;
    return $result;
}
function insert_emma_geofence_locations_new_first($planID, $user, $name, $clat, $clng, $radius){
    global $emmadb;
    $result = $emmadb->query("
    INSERT INTO emma_geofence_locations (
        plan_id, 
        fence_name, 
        type, 
        center_lat, 
        center_lng, 
        radius,  
        created_date, 
        created_by,
        main_fence
        ) 
    VALUES 
        ('". $planID ."',
        '" . $name . "',
        'Circle',
        '" . $clat . "',
        '" . $clng . "',
        '" . $radius * 0.000621371 . "',
        '". date('Y-m-d H:i:s') ."',
        '". $user ."',
        '0'
        )
    ");
    return $emmadb->insert_id;
}
function insert_emma_geofence_locations_new($main, $planID, $user, $name, $clat, $clng, $radius){
    global $emmadb;
    $result = $emmadb->query("
    INSERT INTO emma_geofence_locations (
        plan_id, 
        fence_name, 
        type, 
        center_lat, 
        center_lng, 
        radius,  
        created_date, 
        created_by,
        main_fence
        ) 
    VALUES 
        ('". $planID ."',
        '" . $name . "',
        'Circle',
        '" . $clat . "',
        '" . $clng . "',
        '" . $radius * 0.000621371 . "',
        '". date('Y-m-d H:i:s') ."',
        '". $user ."',
        '". $main ."'
        )
    ");
    return $emmadb->insert_id;
}
function insert_emma_resources($planID, $siteID, $typeID, $filename, $mimeType, $pathString, $userID, $extension){
    global $emmadb;
    $result = $emmadb->query("
        INSERT INTO emma_resources(
          emma_plan_id, 
          emma_site_id, 
          emma_resource_type_id, 
          file_name, 
          file_mime_type, 
          file_string, 
          created_by, 
          created_date, 
          file_extension
        ) VALUES (
          '" . $planID . "', 
          '" . $siteID . "', 
          '" . $typeID . "', 
          '" . $filename . "', 
          '" . $mimeType . "', 
          '" . $pathString . "', 
          '" . $userID . "', 
          NOW(), 
          '" . $extension . "'
        )
      ");
    return $result;
}
function insert_emma_edit_history_groups($groupID, $name, $infoOnly, $admin, $security, $sos, $admin911, $editorID, $dateEditied){
    global $emmadb;
    $result = $emmadb->query("
    insert into emma_edit_history_groups (
      emma_group_id, 
      name, 
      info_only, 
      admin, 
      security, 
      emma_sos,
      911admin,                                  
      edit_by_id, 
      edit_date
    ) VALUES (
      '" . $groupID . "',
      '" . $name . "',
      " . $infoOnly . ",
      " . $admin . ",
      " . $security . ",
      " . $sos . ",
      " . $admin911 . ",
      '" . $editorID . "',
      '" . $dateEditied . "'
    )
  ");
    return $result;
}
function insert_emma_edit_history_group_privileges($saveHistoryID, $privilegeID){
    global $emmadb;
    $result = $emmadb->query("
      insert into emma_edit_history_group_privileges (
        emma_edit_history_group_id, 
        emma_privilege_id
      ) VALUES (
        '" . $saveHistoryID . "',
        '" . $privilegeID . "'
      )
    ");
    return $result;
}
function insert_emma_group_privileges($groupID, $privilegeID){
    global $emmadb;
    $result = $emmadb->query("
      INSERT INTO emma_group_privileges (
        emma_group_id, 
        emma_privilege_id
      ) VALUES (
        '" . $groupID . "',
        '" . $privilegeID . "'
      )
    ");
    return $result;
}
function insert_emma_groupfeed_privileges($groupID, $privilegeID){
    global $emmadb;
    $result = $emmadb->query("
      INSERT INTO emma_feed_view_privilege (
        group_id, 
        group_privilege_id,
        active
      ) VALUES (
        '" . $groupID . "',
        '" . $privilegeID . "',
        '1'
      )
    ");
    return $result;
}
function insert_emma_plan_security_types($planID, $securityTypeID){
    global $emmadb;
    $result = $emmadb->query("
      insert into emma_plan_security_types (
        emma_plan_id, 
        emma_security_type_id
      ) values (
        '" . $planID . "',
        '" . $securityTypeID . "'
      )
    ");
    return $result;
}
function insert_emma_edit_history_sites($siteID, $name, $address, $city, $state, $zip, $lat, $lng, $contactFirstname,
                                        $contactLastname, $contactEmail, $contactPhone, $editedUserID, $editedDate){
    global $emmadb;
    $result = $emmadb->query("
    INSERT INTO emma_edit_history_sites (
      emma_site_id, 
      emma_site_name, 
      emma_site_street_address, 
      emma_site_city, 
      emma_site_state, 
      emma_site_zip, 
      emma_site_latitude, 
      emma_site_longitude, 
      contact_first_name, 
      contact_last_name, 
      contact_email, 
      contact_phone, 
      edit_by_id, 
      edit_date
    ) VALUES (
      '" . $siteID . "',
      '" . $name . "',
      '" . $address . "',
      '" . $city . "',
      '" . $state . "',
      '" . $zip . "',
      '" . $lat . "',
      '" . $lng . "',
      '" . $contactFirstname . "',
      '" . $contactLastname . "',
      '" . $contactEmail . "',
      '" . $contactPhone . "',
      '" . $editedUserID . "',
      '" . $editedDate . "'
    )
  ");
    return $result;
}
function insert_emma_edit_history_users($userID, $firstname, $middlename, $lastname, $landlinePhone, $phone, $title,
                                        $address, $city, $province, $zip, $editedID, $editedDate){
    global $emmadb;
    $result = $emmadb->query("
    INSERT INTO emma_edit_history_users (
      user_id, 
      firstname, 
      middlename, 
      lastname, 
      landline_phone, 
      phone, 
      title, 
      address, 
      city, 
      province, 
      zip, 
      edit_by_id, 
      edit_date
    ) VALUES (
      '" . $userID . "',
      '" . $firstname . "',
      '" . $middlename . "',
      '" . $lastname . "',
      '" . $landlinePhone . "',
      '" . $phone . "',
      '" . $title . "',
      '" . $address . "',
      '" . $city . "',
      '" . $province . "',
      '" . $zip . "',
      '" . $editedID . "',
      '" . $editedDate . "'
    )
  ");
    return $result;
}
function insert_emma_edit_history_user_groups($saveHistoryID, $groupID){
    global $emmadb;
    $result = $emmadb->query("
      INSERT INTO emma_edit_history_user_groups (
        emma_edit_history_user_id,
        emma_group_id
      ) VALUES (
        '" . $saveHistoryID . "',
        '" . $groupID . "'
      )
    ");
    return $result;
}
function insert_emma_downloads($leadID, $type){
    global $emmadb;
    $result = $emmadb->query("
    INSERT INTO emma_downloads (lead_id, download) 
    VALUES ('". $leadID ."','". $type ."')
");
    return $result;
}
function insert_emma_guest_codes($code, $planId, $groupId, $start, $end, $duration){
    global $emmadb;
    $result = $emmadb->query("
    insert into emma_guest_codes (
      guest_code_id, 
      emma_plan_id, 
      group_id, 
      start_date, 
      end_date,
      duration_minutes
    ) values (
      '" . $code . "',
      '" . $planId . "',
      '" . $groupId . "',
      '" . $start . "',
      '" . $end . "',
      '" . $duration . "'
    )
  ");
    return $result;
}
function insert_raw_users($firstname, $lastname, $email, $phone, $land, $plan, $group){
    global $emmadb;
    $result = $emmadb->query("
        INSERT INTO raw_users (firstname, lastname, email, mobile, landline, plan, `group`) 
        VALUES ('". $firstname ."', '". $lastname ."', '". $email ."', '". $phone ."', '". $land ."','". $plan ."','". $group ."')
    ");
    return $result;
}
function insert_emma_raw_students_to_guardians($studentID, $studentName, $userID){
    global $emmadb;
    $result = $emmadb->query("
        INSERT INTO emma_raw_students_to_guardians (student_id, student_name, guardian_id) 
        VALUES ('" . $studentID . "', '". $studentName ."', '" . $userID . "')
    ");
    return $result;
}
function insert_emma_group_events($groupID, $eventID){
    global $emmadb;
    $result = $emmadb->query("
        INSERT INTO emma_group_events (emma_group_id, event_id) 
        VALUES ('". $groupID ."','". $eventID ."')
      ");
    return $result;
}
function insert_emma_login_log($username, $success, $ip, $loginSource, $attemptNumber){
    global $emmadb;
    $result = $emmadb->query("
        INSERT INTO emma_login_log (username, login_datetime, success, ip, login_source, attempt_since_success) 
        VALUES ('". $username ."',NOW(),". $success .",'". $ip ."','". $loginSource ."','". $attemptNumber ."')
      ");
    return $result;
}
function insert_emma_old_passwords($userID, $password){
    global $emmadb;
    $hashedpassword = hash('sha512', $password);
    $result = $emmadb->query("
        INSERT INTO emma_old_passwords (user_id, password_hash, date_created)
        VALUES ('". $userID ."','". $hashedpassword ."',NOW())
      ");
    return $result;
}



function insert_emma_sos($created_by_id, $pending_lat, $pending_lng, $planID){
    global $emmadb;
    $result = $emmadb->query("
    INSERT INTO emma_sos
    (
    created_by_id, 
    created_date, 
    sos_date, 
    help_date, 
    help_lat, 
    help_lng, 
    pending_date, 
    pending_lat, 
    pending_lng, 
    cancelled_date, 
    cancelled_lat, 
    cancelled_lng, 
    closed_date, 
    closed_comments, 
    closed_lat, 
    closed_lng, 
    closed_by_id, 
    plan_id, 
    system_pushed)
    VALUES(
    '".$created_by_id."',
    '".date("Y-m-d h:i:sa")."',
    null,
    null,
    null,
    null,
    '".date("Y-m-d h:i:sa")."',
    '".$pending_lat."',
    '".$pending_lng."',
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,            
    '".$planID."',
    0
    )
    ");
    return $result;
}

function insert_log_text($type, $id, $number, $message, $success){
    global $emmadb;
    $result = $emmadb->query("
        INSERT INTO emma_texts (notification_type, notification_id, phone, message, success, date) VALUE ('". $type ."', '". $id ."', '". $number ."', '". $message ."', ". $success .", '". date('Y-m-d H:i:s') ."')
    ");
    return $result;
}


function insert_lockdown($user, $planId, $lat, $lng, $drill){
    global $emmadb;
    $result = $emmadb->query("
            INSERT INTO emma_lockdowns (
              plan_id,
              created_by_id,
              created_date,
              created_lat,
              created_lng,
              drill
            ) VALUES (
              '" . $planId . "', 
              '" . $user . "', 
              NOW(),
              '" . $lat . "',
              '" . $lng . "',
              '" . $drill . "'
            )
        ");
    return $result;
}
function insert_modified_lockdown($user, $planId, $lat, $lng){
    global $emmadb;
    $result = $emmadb->query("
            INSERT INTO emma_modified_lockdowns (
              plan_id,
              created_by_id,
              created_date,
              created_lat,
              created_lng
        
            ) VALUES (
              '" . $planId . "', 
              '" . $user . "', 
              NOW(),
              '" . $lat . "',
              '" . $lng . "'
            )
        ");
    return $result;
}

function insert_user_csv_upload($foldername, $filename, $userID, $ip, $hasrun){
    global $emmadb;
    $result = $emmadb->query("
            INSERT INTO user_csv_uploads (folder_name, file_name, user_id, upload_datetime, upload_ipaddress, has_run) 
            VALUES ('".$foldername."', '".$filename."', '".$userID."', NOW(), '".$ip."', ".$hasrun.")
        ");
    return $result;
}
function insert_user_from_csv($username, $password, $firstname, $middlename, $lastname, $title, $phone, $landline, $company, $address, $city, $province, $country, $zip, $creatorIP, $creatorID, $planID, $activate_email, $no_logout, $admin_user){
    global $emmadb;

    $result = $emmadb->query("
           INSERT INTO users (
            username, password, firstname, middlename, lastname, title,
            phone, landline_phone, company, address, city, province, 
            country, zip, creatorip, creator, creation, 
            auth, emma_plan_id, activate_email, no_logout, admin_user
        ) VALUES (
            '" . $username . "', '".$password."', '" . $firstname . "', '" . $middlename . "', '" . $lastname . "', '" . $title . "',
            '" . $phone . "', '" . $landline . "', '" . $company . "', '" . $address . "', '" . $city . "', '" . $province . "', 
            '" . $country . "', '" . $zip . "', '" . $creatorIP . "', '" . $creatorID . "', '" . date("Y-m-d H:i:s") . "', 
            SHA1('" . $username . "'), '". $planID ."', ". $activate_email .", ". $no_logout .", ". $admin_user ."
        )
               ");
    return $result;
}
function insert_user_csv_upload_file($uploadID, $fileData){
    global $emmadb;
    $result = $emmadb->query("
            INSERT INTO user_csv_upload_files (upload_id, file) 
            VALUES ('".$uploadID."', '".$fileData."')
        ");
    return $result;
}

function insert_anonymous_report($user, $plan, $description){
    global $emmadb;
    $result = $emmadb->query("
            INSERT INTO emma_ar (created_by_id, created_date, description, plan_id) 
            VALUES ('".$user."', NOW(), '". $description ."', '". $plan ."')
        ");
    return $result;
}
function insert_anonymous_report_moss($user, $plan, $description){
    global $emmadb;
    $result = $emmadb->query("
            INSERT INTO emma_moss (created_by_id, created_date, description, plan_id) 
            VALUES ('".$user."', NOW(), '". $description ."', '". $plan ."')
        ");
    return $result;
}

function insert_sos_response($userId, $sosId, $comments){
    global $emmadb;
    $result = $emmadb->query("
        INSERT INTO emma_sos_responses (sos_id, message, created_by, created_date) VALUE ('". $sosId ."', '". $comments ."', '". $userId ."', NOW())
    ");
    return $result;
}













//CRM-------------------------------------------------------------------------------------------------------------------------------------------------$crmdb
function insert_raw_leads($uniqueID, $firstName, $lastName, $company, $phone, $mobile, $email, $description){
    global $crmdb;
    $result = $crmdb->query("
        INSERT INTO raw_leads (
          lead_id,
          first_name,
          last_name,
          company,
          phone,
          mobile,
          email,                     
          description,                     
          created_date,              
          `status`,              
          active,
          approved
        ) VALUES (
          '" . $uniqueID . "', 
          '" . $firstName . "', 
          '" . $lastName . "',
          '" . $company . "',
          '" . $phone . "',
          '" . preg_replace("/[^0-9]/", "", $mobile) . "', 
          '" . $email . "',
          '" . $description . "',
          '". date('Y-m-d H:i:s') ."',
          'Active',
          '1',
          '0'
        )
    ");
    return $result;
}
function insert_raw_leads_full($uniqueID, $firstName, $lastName, $company, $phone, $mobile, $email, $description, $createdDate, $status, $active, $approved){
    global $crmdb;
    $result = $crmdb->query("
            INSERT INTO raw_leads (
              lead_id,
              first_name,
              last_name,
              company,
              phone,
              mobile,
              email,                     
              description,                     
              created_date,              
              `status`,              
              active,
              approved
            ) VALUES (
              '" . $uniqueID . "', 
              '" . $firstName . "', 
              '" . $lastName . "',
              '" . $company . "',
              '" . $phone . "',
              '" . preg_replace("/[^0-9]/", "", $mobile) . "', 
              '" . $email . "',
              '" . $description . "',
              '". $createdDate ."',
              '".$status."',
              '".$active."',
              '".$approved."'
            )
        ");
    return $result;
}

function insert_lockdown_permission($groupId, $planId){
    global $emmadb;
    $insert = $emmadb->query("
        INSERT INTO emma_lockdown_privileges (receiving_group_id, plan_id)
        VALUE ('".$groupId."', '".$planId."')
    ");
}

function insert_nws_report($county, $currentAlertString, $currentAlertSevereStatus){
    global $emmadb;
    $result = $emmadb->query("
            INSERT INTO nws_current_weather_status (zone_id, weather_report, severe_weather, last_pulled) 
            VALUES ('".$county."', '".$currentAlertString."', ".$currentAlertSevereStatus.", NOW())
        ");
    return $result;
}

function insert_qr_code($plan,$group){
    global $emmadb;
    $planinfo = $emmadb->query("
        SELECT name
        FROM emma_plans
        WHERE emma_plan_id = '". $plan ."'
    ");
    $p = $planinfo->fetch_assoc();
    $result = $emmadb->query("
        INSERT INTO emma_guest_qr_codes (qr_code, plan_id, group_id) 
        VALUES ('" . $p['name'] . '-' . uniqid('') . "', '" . $plan . "', '" . $group . "')
    ");
    return $result;
}



?>