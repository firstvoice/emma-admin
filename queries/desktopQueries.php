<?php


//---------------------------------------------------------------------------------------------------------------------------------------------------emmaDesktopConnection
function select_edc_users($username, $password){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.*, eg.*
        FROM users as u 
        LEFT JOIN emma_user_groups as eug on u.id = eug.user_id
        LEFT JOIN emma_groups as eg on u.emma_plan_id = eg.emma_plan_id AND eg.emma_group_id = eug.emma_group_id
        WHERE u.username = '".$username."'
        AND (u.password = sha1('".$password."') or u.password = password('".$password."'))
    ");
    return $result;
}
function select_edc_topPrivilegeUser($groupID, $username, $password){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.*,ep.sos_duration, eg.geofences
        FROM users as u
        LEFT JOIN emma_groups as eg on eg.emma_group_id = '".$groupID."'
        LEFT JOIN emma_plans as ep on u.emma_plan_id = ep.emma_plan_id
        WHERE u.username = '".$username."'
        AND (u.password = sha1('".$password."') or u.password = password('".$password."'))
        ORDER BY u.id
    ");
    return $result;
}
function select_edc_usersAuth($groupID, $auth){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.emma_plan_id, eg.info_only, u.id
        FROM users u
        LEFT JOIN emma_user_groups as eug on u.id = eug.user_id
        LEFT JOIN emma_groups as eg on u.emma_plan_id = eg.emma_plan_id AND eg.emma_group_id = '".$groupID."'
        WHERE u.auth = '".$auth."'
        GROUP BY u.id
    ");
    return $result;
}
function select_edc_planEventTypes($planID, $userID){
    global $emmadb;
    $result = $emmadb->query("
        select DISTINCT et.img_filename, et.emergency_type_id, et.name
        from emma_group_events e
           join emergency_types et on e.event_id = et.emergency_type_id
        where e.emma_group_id IN (SELECT u.emma_group_id FROM emma_user_groups AS u JOIN emma_groups AS g ON u.emma_group_id = g.emma_group_id WHERE g.emma_plan_id = '".$planID."' AND u.user_id = '".$userID."')
        AND et.emergency_type_id > 0
    ");
    return $result;
}
function select_edc_selectPlans($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT mp.plan_id, p.name, eug.emma_group_id
        FROM emma_plans p
        LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id
        LEFT JOIN emma_user_groups as eug on eug.user_id = '".$userID."'
        WHERE mp.user_id = '" . $userID . "'
        AND p.date_expired > NOW()
    ");
    return $result;
}
function select_edc_events($planArray, $groupArray){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*,et.name AS type, et.badge, et.img_filename, es.emma_site_name, es.emma_site_notes, est.name as sub_name
        FROM emergency_received_groups as erg
        JOIN  emergencies as e on e.emergency_id = erg.emergency_id
        JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
        JOIN emergency_sub_types est on est.emergency_sub_type_id = e.emergency_sub_type_id
        JOIN emma_sites es ON es.emma_site_id = e.emma_site_id
        WHERE erg.emma_group_id IN (".implode(',',$groupArray).")
        AND e.active = '1'
        AND e.emma_plan_id IN (".implode(',',$planArray).")
        AND e.parent_emergency_id IS NULL
    ");
    return $result;
}
function select_edc_responseStatuses($emergencyTypeID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT etr.emergency_response_status_id
        FROM emergency_type_response_statuses etr
        WHERE etr.emergency_type_id = '" . $emergencyTypeID . "'
    ");
    return $result;
}
function select_edc_specificResposnces($responseStatusID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.name,e.color, e.emergency_response_status_id
        FROM emergency_response_status e
        WHERE e.emergency_response_status_id = '" . $responseStatusID . "' 
    ");
    return $result;
}
function select_edc_groups($planID, $groupID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_groups as eg
        WHERE eg.emma_plan_id = '" . $planID . "'
        AND eg.emma_group_id = '".$groupID."'
    ");
    return $result;
}
function select_edc_planEventTypesDropDown($dropdownID){
    global $emmadb;
    $result = $emmadb->query("
      select *
      from emma_plan_event_types pet
      join emergency_types et on pet.emma_emergency_type_id = et.emergency_type_id
      where pet.emma_emergency_type_id = '" . $dropdownID . "'
    ");
    return $result;
}
function select_edc_subTypes($emergencyTypeID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emergency_sub_types
        WHERE emergency_type_id = '" . $emergencyTypeID . "'
    ");
    return $result;
}
function select_edc_sites($planID){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_sites
        where emma_plan_id = '" . $planID . "'
    ");
    return $result;
}
function select_edc_broadcastScripts($planID, $groupID){
    global $emmadb;
    $result = $emmadb->query("
        select e.*
        from emma_scripts e
        WHERE e.emma_plan_id = '" . $planID . "'
        AND e.emma_script_type_id = '1'
        AND e.emma_script_group_id = '". $groupID."'
        order by name
    ");
    return $result;
}
function select_edc_geofenceLocations($planID){
    global $emmadb;
    $result = $emmadb->query("
        select e.*
        from emma_geofence_locations e
        WHERE e.plan_id = '" . $planID . "'   
        AND e.active = 1        
        order by e.fence_name
    ");
    return $result;
}
function select_edc_usersWithAuth($auth){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.emma_plan_id, u.id, u.emma_pin
        FROM users u
        WHERE u.auth = '" . $auth . "'
    ");
    return $result;
}
function select_edc_notifications($groupArray){
    global $emmadb;
    $result = $emmadb->query("
        SELECT emc.notification, emc.emma_mass_communication_id, emc.created_by_id, emc.created_date as date
        FROM communication_received_groups as crg
        JOIN emma_mass_communications as emc on emc.emma_mass_communication_id = crg.communication_id
        WHERE crg.emma_group_id IN (".implode(',',$groupArray).")
        AND emc.created_date >= NOW()- interval 20 second
        ORDER BY emc.created_date DESC
    ");
    return $result;
}
function select_edc_userWithID($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.username
        FROM users as u 
        WHERE u.id = '".$userID."'
    ");
    return $result;
}
function select_edc_systemMessages($groupArray){
    global $emmadb;
    $result = $emmadb->query("
        SELECT esm.message, esm.emma_system_message_id, esm.comments, esm.created_by_id as created_id, esm.created_date as creation_date
        FROM emma_system_message_groups as esmg
        JOIN emma_system_messages as esm on esm.emma_system_message_id = esmg.emma_system_message_id
        WHERE esmg.emma_group_id IN (".implode(',',$groupArray).")
        AND esm.created_date >= NOW()- interval 20 second
        ORDER BY esm.created_date DESC                
    ");
    return $result;
}
function select_edc_username($auth){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.username, CONCAT(u.firstname, ' ', u.lastname) AS name
        FROM users as u 
        WHERE u.auth = '".$auth."'
        ");
    return $result;
}
function select_edc_versionCheck($platform){
    global $emmadb;
    $result = $emmadb->query("
        SELECT MAX(epv.version) as version
        FROM emma_pc_versions as epv
        WHERE epv.platform = '".$platform."'                
    ");
    return $result;
}
function select_edc_checkRecords($auth){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.id as userid,epu.*
        FROM users as u
        RIGHT JOIN emma_pc_users as epu on epu.userid = u.id 
        WHERE u.auth = '".$auth."'
    ");
    return $result;
}
function select_edc_sosQuery($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT es.help_lat, es.help_lng
        FROM emma_sos as es
        WHERE es.created_by_id = '".$userID."'
        AND es.help_date is not null
        AND es.closed_date is null
        AND es.cancelled_date is null
    ");
    return $result;
}
function select_edc_checkCanReceive($groupArray){
    global $emmadb;
    $result = $emmadb->query("
        SELECT eg.emma_sos, eg.admin
        FROM emma_groups as eg
        WHERE eg.emma_group_id IN (" . implode(',', $groupArray) . ") 
    ");
    return $result;
}
function select_edc_sosUserPlans($planArray){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.id
        FROM users as u
        WHERE u.emma_plan_id IN (" . implode(',', $planArray) . ")
    ");
    return $result;
}
function select_edc_sosMessage($userIDArray){
    global $emmadb;
    $result = $emmadb->query("
        SELECT es.help_lat,es.help_lng,es.help_date,es.created_by_id, es.emma_sos_id
        FROM emma_sos AS es   
        WHERE es.help_date >= NOW()- interval 20 second
        AND es.created_by_id IN (" . implode(',', $userIDArray) . ")    
    ");
    return $result;
}
function select_edc_geoMessages(){
    global $emmadb;
    $result = $emmadb->query("
        SELECT egl.*, egm.*, egm.created_date as createdDate, egm.created_by_id as created_by_person
        FROM emma_geofence_locations AS egl
        LEFT JOIN emma_geofence_messages AS egm on egm.geofence_id = egl.id
        WHERE egm.created_date >= NOW()- interval 20 second
    ");
    return $result;
}
function select_edc_geoShortList($adj_n_lat, $adj_s_lat, $adj_e_lng, $adj_w_lng){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_geofence_user_tracking
        WHERE lat < " . $adj_n_lat . " AND lat > " . $adj_s_lat . "
        AND lng < " . $adj_e_lng . " AND lng > " . $adj_w_lng . "
    ");
    return $result;
}




//---------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------
//--------------------------------INSERTS------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

function insert_edc_emmaSOS($userID, $lat, $lng, $planID){
    global $emmadb;
    $result = $emmadb->query("
       INSERT INTO emma_sos(
        created_by_id, 
        created_date, 
        sos_date, 
        help_date, 
        help_lat, 
        help_lng, 
        pending_date, 
        pending_lat, 
        pending_lng, 
        cancelled_date, 
        cancelled_lat, 
        cancelled_lng, 
        closed_date, 
        closed_comments, 
        closed_lat, 
        closed_lng, 
        closed_by_id, 
        plan_id, 
        system_pushed)
        VALUES(
        '".$userID."',
        '".date("Y-m-d h:i:sa")."',
        null,
        null,
        null,
        null,
        '".date("Y-m-d h:i:sa")."',
        '".$lat."',
        '".$lng."',
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,            
        '".$planID."',
        0
        )
    ");
    return $result;
}
function insert_edc_emmaPCUsers($version, $platform, $userID, $planID){
    global $emmadb;
    $result = $emmadb->query("
        INSERT INTO emma_pc_users (version, platform, userid, planid) VALUES (
        '".$version."',
        '".$platform."',
        '".$userID."',
        '".$planID."'
        )
    ");
    return $result;
}







//---------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------
//--------------------------------UPDATES------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

function update_edc_emmaSOS_with_canceled($lat, $lng, $sosID){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_sos
        SET cancelled_date = '".date('Y/m/d H:i:s')."', cancelled_lat = '".$lat."', cancelled_lng = '".$lng."' 
        WHERE emma_sos_id = '".$sosID."'
    ");
    return $result;
}
function update_edc_emmaPCVersions($version, $platform){
    global $emmadb;
    $result =  $emmadb->query("
        UPDATE emma_pc_versions
        SET active_users = active_users + 1
        WHERE version = '".$version."'
        AND platform = '".$platform."'
    ");
    return $result;
}
function update_edc_emmaPCUsers($version, $platform, $userID){
    global $emmadb;
    $result =  $emmadb->query("
        UPDATE emma_pc_users
        SET version = '".$version."', platform = '".$platform."'
        WHERE userid = '".$userID."'
    ");
    return $result;
}
function update_edc_emmaPCVersions_Minus($version, $platform){
    global $emmadb;
    $result =  $emmadb->query("
        UPDATE emma_pc_versions
        SET active_users = active_users - 1
        WHERE version = '".$version."'
        AND platform = '".$platform."'
    ");
    return $result;
}










