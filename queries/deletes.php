<?php

function delete_user_groups($userID, $groupID){
    global $emmadb;
    $result = $emmadb->query("
                DELETE FROM emma_user_groups
                WHERE user_id = " . $userID . "
                AND emma_group_id = ".$groupID."
            ");
    return $result;
}
function delete_emmaScript($scriptID){
    global $emmadb;
    $result = $emmadb->query("
        delete from emma_scripts
        where emma_script_id = '" . $scriptID . "'
    ");
    return $result;
}
function delete_notification_script($scriptID){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_mass_notification_scripts
        SET active = 0
        WHERE mass_notification_script_id = '" . $scriptID . "'
    ");
    return $result;
}
function delete_email_script($scriptID){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_email_scripts
        SET active = 0
        WHERE emma_email_script_id = '" . $scriptID . "'
    ");
    return $result;
}
function delete_text_script($scriptID){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_text_scripts
        SET active = 0
        WHERE emma_text_script_id = '" . $scriptID . "'
    ");
    return $result;
}
function delete_emma_group_privileges($groupID){
    global $emmadb;
    $result = $emmadb->query("
        DELETE FROM emma_group_privileges
        WHERE emma_group_id = '" . $groupID . "'
    ");
    return $result;
}
function delete_emma_groupfeed_privileges($groupID){
    global $emmadb;
    $result = $emmadb->query("
        update emma_feed_view_privilege
        SET active = '0'
        WHERE group_id = '" . $groupID . "'
    ");
    return $result;
}
function delete_emma_plan_security_types($planID){
    global $emmadb;
    $result = $emmadb->query("
        delete from emma_plan_security_types
        where emma_plan_id = '" . $planID . "'
    ");
    return $result;
}
function delete_emma_user_groups($userID, $groupID){
    global $emmadb;
        $result = $emmadb->query("
        DELETE FROM emma_user_groups
        WHERE user_id = '" . $userID . "'
        AND emma_group_id = '". $groupID ."'
    ");
    return $result;
}
function delete_emma_user_groups_with_groupString_userString($userString, $groupString){
    global $emmadb;
    $result = $emmadb->query("
        DELETE FROM emma_user_groups
        WHERE user_id IN ('" . $userString . "')
        AND emma_group_id IN ('" . $groupString . "')
    ");
    return $result;
}


function disable_emma_user($userID){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE users
        SET display = 'no',
            active = 0,
            deleted_by_cronjob = 1
        WHERE id = '".$userID."'
    ");
    return $result;
}
function disable_emma_user_groups($userID, $planID){
    global $emmadb;
    $result = $emmadb->query("
        DELETE FROM emma_user_groups
        WHERE user_id = '" . $userID . "'
        AND emma_group_id IN (SELECT emma_group_id FROM emma_groups WHERE emma_plan_id = '".$planID."')
    ");
    return $result;
}
function disable_emma_user_multiplan($userID, $planID){
    global $emmadb;
    $result = $emmadb->query("
        DELETE FROM emma_multi_plan
        WHERE user_id = '" . $userID . "'
        AND plan_id = '".$planID."'
    ");
    return $result;
}

function delete_nws_report($county, $DBAlertString){
    global $emmadb;
    $result = $emmadb->query("
        DELETE FROM nws_current_weather_status
        WHERE zone_id = '" . $county . "'
        AND weather_report = '".$DBAlertString."'
    ");
    return $result;
}