<?php

function update_SOSPushed_with_sosID($SOSID){
    global $emmadb;
    $result = $emmadb->query("
            UPDATE emma_sos
            SET system_pushed = 1
            WHERE emma_sos_id = '".$SOSID."'
        ");
    return $result;
}

function update_SOSCanceled_with_sosID_Lat_lng($SOSID, $lat, $lng){
    global $emmadb;
    $result = $emmadb->query("
                UPDATE emma_sos
                SET cancelled_date = '".date('Y/m/d H:i:s')."', cancelled_lat = '".$lat."', cancelled_lng = '".$lng."' 
                WHERE emma_sos_id = '".$SOSID."'
            ");
    return $result;
}


function update_userDetails($userid, $firstName, $middleName, $lastName, $landline, $mobile, $title, $address, $city, $state, $zip, $pin, $timezone, $password){
    global $emmadb;
    $hashedpassword = hash('sha512', $password);
    $result = $emmadb->query("
      UPDATE users
      SET 
        firstname = '" . $firstName . "',
        middlename = '" . $middleName . "',
        lastname = '" . $lastName . "',
        landline_phone = '" . $landline . "',
        phone = '" . $mobile . "',
        title = '" . $title . "',
        address = '" . $address . "',
        city = '" . $city . "',
        province = '" . $state . "',
        zip = '" . $zip . "',
        emma_pin = '" . $pin . "',
        timezone = '" . $timezone . "',
        big_password = '" . $hashedpassword . "',
        password = password('".$password."')
      WHERE id = " . $userid . "
    ");
    return $result;
}
function update_userDetails_generalDetails($userid, $username, $firstName, $middleName, $lastName, $landline,
                                           $mobile, $title, $address, $city, $state, $zip, $pin, $plan){
    global $emmadb;
    $result = $emmadb->query("
      UPDATE users
      SET username = '". $username ."',
          firstname = '" . $firstName . "',
        middlename = '" . $middleName . "',
        lastname = '" . $lastName . "',
        landline_phone = '" . $landline . "',
        phone = '" . $mobile . "',
        title = '" . $title . "',
        address = '" . $address . "',
        city = '" . $city . "',
        province = '" . $state . "',
        zip = '" . $zip . "',
        emma_pin = '". $pin ."',
        emma_plan_id = '". $plan ."',
        auth = sha1('". $username ."')
      WHERE id = " . $userid . "
    ");
    return $result;
}

function update_remove_multiplans($userID){
    global $emmadb;

    $delete = $emmadb->query("
        UPDATE emma_multi_plan
        SET active = FALSE
        WHERE user_id = '". $userID ."'
    ");
    return $delete;

}

function update_geofenceMessages_sentToQnt_with_messageID_and_count($messageID, $count){
    global $emmadb;
    $result = $emmadb->query("
                UPDATE emma_geofence_messages
                SET sent_to_qnt = sent_to_qnt + '" . $count . "'
                WHERE id = '" . $messageID . "'
            ");
    return $result;
}

function update_rawUser_active_with_userID_and_active($userID, $active){
    global $emmadb;
    $result = $emmadb->query("
            UPDATE raw_users
            SET active = '".$active."'
            WHERE id = '". $userID ."'
        ");
    return $result;
}

function update_users_siteID_with_userlist($userlist, $siteID){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE users AS u
        SET u.emma_site_id = ". $siteID ."
        WHERE u.id IN ('". $userlist ."')
    ");
    return $result;
}

function update_emergencies_descriptionAndComments_with_emergencyID($emergencyID, $description, $comments){
    global $emmadb;
    $result = $emmadb->query("
      UPDATE emergencies
      SET 
        description = '" . $description . "',
        comments = '" . $comments . "'
      WHERE emergency_id = '" . $emergencyID . "'
    ");
    return $result;
}
function update_emergencies_closeEvent_with_emergencyID($emergencyID, $description, $comments, $userID){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emergencies
        SET 
          active = '0',
          description = '" . $description . "',
          comments = '" . $comments . "',
          closed_date = '" . date('Y-m-d H:i:s') . "',
          closed_by_id = '" . $userID . "'
        WHERE emergency_id = '" . $emergencyID . "'
    ");
    return $result;
}
function update_securities_closeSecurity_with_securityID($securityID, $description, $comments, $userID){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_securities
        SET 
          active = '0',
          closed_date = '" . date('Y-m-d H:i:s') . "',
          description = '" . $description . "',
          comments = '" . $comments . "',
          closed_by_id = '" . $userID . "'
        WHERE emma_security_id = '" . $securityID . "'
    ");
    return $result;
}
function update_Sos_closeSOS_with_sosID($sosID, $comments, $userID){
    global $emmadb;
    $result = $emmadb->query("
        update emma_sos
        set 
          closed_comments = '" . $comments . "',
          closed_lat = '',
          closed_lng = '',
          closed_date = '" . date('Y-m-d H:i:s') . "',
          closed_by_id = '" . $userID . "'
        where emma_sos_id = '" . $sosID . "'
    ");
    return $result;
}

function update_emma911CallLog_active_with_callID($callID, $active){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_911_call_log
        SET active = '".$active."'
        WHERE call_id = '".$callID."'
    ");
    return $result;
}
function update_users_planID_with_userID($userID, $planID){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE users
        SET emma_plan_id = '". $planID ."'
        WHERE id = '". $userID ."'
    ");
    return $result;
}
function update_users_password_with_userID($userID, $password){
    global $emmadb;
    $hashedPassword = hash("sha512", $password);
    $result = $emmadb->query("
        UPDATE users u
        SET u.big_password = '". $hashedPassword ."',
        u.password = password('".$password."')
        WHERE u.id = '". $userID ."'
    ");
    return $result;
}
function update_users_pin_with_userID($userID, $pin){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE users u
        SET u.emma_pin = '". $pin ."'
        WHERE u.id = '". $userID ."'
    ");
    return $result;
}
function update_emmaAsset_details_with_assetID($assetID, $typeID, $latitude, $longitude, $address, $status){
    global $emmadb;
    $result = $emmadb->query("
    UPDATE emma_assets as ea
    SET
     ea.emma_asset_type_id = '".$typeID."',
     ea.latitude = '".$latitude."',
     ea.longitude = '".$longitude."',
     ea.address = '".$address."',
     ea.active = '".$status."'
     WHERE ea.emma_asset_id = '".$assetID."'
    ");
    return $result;
}
function update_assetsCustomInfo_dropdown_with_assetCustomInfoID($assetCustomInfoID, $dropdown){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_assets_custom_info
        SET dropdown = '".$dropdown."'
        WHERE id = '".$assetCustomInfoID."'
    ");
    return $result;
}
function update_assetsCustomInfo_info_with_assetCustomInfoID($assetCustomInfoID, $info){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_assets_custom_info
        SET info = '".$info."'
        WHERE id = '".$assetCustomInfoID."'
    ");
    return $result;
}
function update_assetsCustomInfo_dateTime_with_assetCustomInfoID($assetCustomInfoID, $dateTime){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_assets_custom_info
        SET date_time = '".$dateTime."'
        WHERE id = '".$assetCustomInfoID."'
    ");
    return $result;
}
function update_assetTypes_name_and_iconID_with_assetTypeID($assetTypeID, $name, $iconID){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_asset_types
        SET name = '".$name."', 
        emma_asset_icon_id = '".$iconID."'
        WHERE emma_asset_type_id = '".$assetTypeID."'
    ");
    return $result;
}
function update_assetTypeDisplay_name_with_assetTypeDisplayID($assetTypeDisplayID, $name){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_asset_type_display
        SET name = '" . $name . "'
        WHERE id = '" . $assetTypeDisplayID . "'
    ");
    return $result;
}
function update_assetDropdownOption_name_with_assetDropdownOptionID($assetDropdownOptionID, $name){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_assets_dropdown_options
        SET Name = '".$name."'
        WHERE id= '".$assetDropdownOptionID."'
    ");
    return $result;
}
function update_geofence_with_geofenceID($geofenceID, $name, $type, $mainFence, $radius, $clat, $clng, $nwlat, $nwlng, $nelat, $nelng, $swlat, $swlng, $selat, $selng){
    global $emmadb;
    $result = $emmadb->query("
    UPDATE emma_geofence_locations
    SET 
        fence_name = '" . $name . "',
        type= '" . $type . "',
        main_fence = '" . $mainFence . "',
        radius = '" . $radius . "',
        center_lat = '" . $clat . "',
        center_lng = '" . $clng . "',
        nw_corner_lat = '" . $nwlat . "',
        nw_corner_lng = '" . $nwlng . "',
        ne_corner_lat = '" . $nelat . "',
        ne_corner_lng = '" . $nelng . "',
        sw_corner_lat = '" . $swlat . "',
        sw_corner_lng = '" . $swlng . "',
        se_corner_lat = '" . $selat . "',
        se_corner_lng = '" . $selng . "'
    WHERE id = " . $geofenceID . "
  ");
    return $result;
}
function update_geofencecircle_with_geofenceID($geofenceID, $radius, $clat, $clng){
    global $emmadb;
    $result = $emmadb->query("
    UPDATE emma_geofence_locations
    SET 
        radius = '" . $radius . "',
        center_lat = '" . $clat . "',
        center_lng = '" . $clng . "'
    WHERE id = " . $geofenceID . "
  ");
    return $result;
}
function update_group_with_groupID($groupID, $infoOnly, $admin, $security, $sos, $admin911){
    global $emmadb;
    $result = $emmadb->query("
    update emma_groups
    set info_only = " . $infoOnly . ",
        admin = " . $admin . ",
        security = " . $security . ",
        emma_sos = " . $sos . ",
        911admin = " . $admin911 . "
    where emma_group_id = '" . $groupID . "'
  ");
    return $result;
}
function update_guestCode_with_guestCodeID($guestCodeID, $groupID, $duration, $start, $end){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_guest_codes
        SET group_id = '" . $groupID . "',
        duration_minutes= '" . $duration. "',
        start_date = '" . $start . "',
        end_date = '" . $end . "'
        WHERE guest_code_id = '" . $guestCodeID . "'
    ");
    return $result;
}
function update_emmaResource_with_resourceID($resourceID, $siteID, $filename, $typeID){
    global $emmadb;
    $result = $emmadb->query("
    UPDATE emma_resources
    SET 
      emma_site_id = '" . $siteID . "',
      file_name = '" . $filename . "',
      emma_resource_type_id = '" . $typeID . "'
    WHERE emma_resource_id = '" . $resourceID . "'
  ");
    return $result;
}
function update_emmaScripts_with_scriptID($scriptID, $name, $text, $groupID, $broadcastID, $typeID){
    global $emmadb;
    $result = $emmadb->query("
        update emma_scripts as s
        set 
          s.name = '" . $name . "',
          s.text = '" . $text . "',
          s.emma_script_group_id = '". $groupID ."',
          s.emma_broadcast_type_id = '". $broadcastID ."',
          s.emma_script_type_id = '". $typeID ."'
        where s.emma_script_id = '" . $scriptID . "'
    ");
    return $result;
}
function updateNotificationScript($scriptID, $name, $text){
    global $emmadb;
    $result = $emmadb->query("
        update emma_mass_notification_scripts as s
        set 
          s.name = '" . $name . "',
          s.text = '" . $text . "'
        where s.mass_notification_script_id = '" . $scriptID . "'
    ");
    return $result;
}
function updateEmailScript($scriptID, $name, $text){
    global $emmadb;
    $result = $emmadb->query("
        update emma_email_scripts as s
        set 
          s.name = '" . $name . "',
          s.script = '" . $text . "'
        where s.emma_email_script_id = '" . $scriptID . "'
    ");
    return $result;
}
function updateTextScript($scriptID, $name, $text){
    global $emmadb;
    $result = $emmadb->query("
        update emma_text_scripts as s
        set 
          s.name = '" . $name . "',
          s.script = '" . $text . "'
        where s.emma_text_script_id = '" . $scriptID . "'
    ");
    return $result;
}
function update_emmaSites_with_siteID($siteID, $name, $address, $city, $state, $zip, $lat, $lng){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_sites
        SET 
        emma_site_name = '" . $name . "',
        emma_site_street_address = '" . $address . "',
        emma_site_city = '" . $city . "',
        emma_site_state = '" . $state . "',
        emma_site_zip = '" . $zip . "',
        emma_site_latitude = '".$lat."',
        emma_site_longitude = '".$lng."'
        WHERE emma_site_id = " . $siteID . "
    ");
    return $result;
}
function update_emmaSiteContact_with_siteContactID($siteContactID, $firstname, $lastname, $phone, $email){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_site_contacts
        SET first_name = '" . $firstname . "',
        last_name = '" . $lastname . "',
        phone = '" . $phone . "',
        email = '" . $email . "'
        WHERE emma_site_contact_id = " . $siteContactID . "
    ");
    return $result;
}
function update_emergencyResponses_with_responseID($messageID, $validatedID, $validationTime, $pin){
    global $emmadb;
    $result = $emmadb->query("
      update emergency_responses er
      set pin = '" . $pin . "',
          validated_id = '" . $validatedID . "',
          validated_time = '".$validationTime."'
      where er.emergency_response_id = '" . $messageID . "'
    ");
    return $result;
}
function update_emmaHelpResources_with_resourceID($resourceID){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_help_resources
        SET view_count = view_count + 1
        WHERE emma_resource_id = " . $resourceID . "
    ");
    return $result;
}
function update_emmaGroupEvents_active_with_groupID($groupID){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_group_events
        SET active = 0
        WHERE emma_group_id = '". $groupID ."'
    ");
    return $result;
}


function update_csv_userDetails($userid, $firstName, $middleName, $lastName, $landline,
                                           $mobile, $title, $company, $address, $city, $state, $country, $zip, $actiavte_email, $no_logout, $admin_user){
    global $emmadb;
    $result = $emmadb->query("
      UPDATE users
      SET firstname = '" . $firstName . "',
        middlename = '" . $middleName . "',
        lastname = '" . $lastName . "',
        landline_phone = '" . $landline . "',
        phone = '" . $mobile . "',
        title = '" . $title . "',
        company = '".$company."',
        address = '" . $address . "',
        city = '" . $city . "',
        province = '" . $state . "',
        zip = '" . $zip . "',
        country = '". $country ."',
        activate_email = ".$actiavte_email.",
        no_logout = ".$no_logout.",
        admin_user = ".$admin_user."
      WHERE id = " . $userid . "
    ");
    return $result;
}
function update_csv_uploads($uploadID){
    global $emmadb;
    $result = $emmadb->query("
      UPDATE user_csv_uploads
      SET has_run = 1
      WHERE upload_id = '" . $uploadID . "'
    ");
    return $result;

}
function reactivate_user_csv($userID){
    global $emmadb;
    $result = $emmadb->query("
      UPDATE users
      SET active = 1,
          display = 'yes',
          deleted_by_cronjob = 0            
      WHERE id = '" . $userID . "'
    ");
    return $result;
}
function update_user_default_plan($userID, $planID){
    global $emmadb;
    $result = $emmadb->query("
      UPDATE users
      SET emma_plan_id = '".$planID."'         
      WHERE id = '" . $userID . "'
    ");
    return $result;
}

function update_user_home_default_plan($userID, $planID){
    global $emmadb;
    $plans = implode(',',$planID);
    $result = $emmadb->query("
      UPDATE users
      SET default_plans = '".$plans."'         
      WHERE id = '" . $userID . "'
    ");
    return $result;
}










function update_geofence_active_with_geofenceID($geofenceID, $active){
    global $emmadb;
    $result = $emmadb->query("
        update emma_geofence_locations
        set active = ".$active."
        where id = '" . $geofenceID . "'
    ");
    return $result;
}
function update_user_active_with_userID($userID, $active){
    global $emmadb;
    $result = $emmadb->query("
      update users
      set active = '".$active."'
      where id = '" . $userID . "'
    ");
    return $result;
}
function update_user_active_with_userID_planID_active($userID, $planID, $active){
    global $emmadb;
    $result = $emmadb->query("
      update users AS u 
      JOIN emma_multi_plan AS mp ON u.id = mp.user_id
      SET mp.active = ".$active."
      where id = " . $userID . "
      AND mp.plan_id = ".$planID."
    ");
    return $result;
}
function update_user_display_with_userID($userID, $display){
    global $emmadb;
    $result = $emmadb->query("
    update users
    set display = '".$display."'
    where id = '" . $userID . "'
  ");
    return $result;
}
function update_emmaAsset_active_with_assetID($assetID, $active){
    global $emmadb;
        $result = $emmadb->query("
        update emma_assets
        set active = ".$active."
        where emma_asset_id = '" . $assetID . "'
    ");
    return $result;
}
function update_emmaAsset_activeDeleted_with_assetID($assetID, $active, $deleted){
    global $emmadb;
    $result = $emmadb->query("
        update emma_assets
        set deleted = ".$deleted.", active = ".$active."
        where emma_asset_id = '" . $assetID . "'
    ");
    return $result;
}
function update_emmaAssetType_active_with_assetTypeID($assetTypeID, $active){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_asset_types
        SET active = ".$active."
        WHERE emma_asset_type_id = '" . $assetTypeID . "'
    ");
    return $result;
}
function update_emmaResource_active_with_resourceID($resourceID, $active){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_resources
        SET active = ".$active."
        WHERE emma_resource_id = " . $resourceID . "
    ");
    return $result;
}
function update_emmaResource_viewCount_with_resourceID($resourceID){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_resources
        SET view_count = view_count + 1
        WHERE emma_resource_id = " . $resourceID . "
    ");
    return $result;
}
function update_assetTypeDisplay_active_with_assetTypeDisplayID($assetTypeDisplayID, $active){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_asset_type_display
        SET active = '".$active."'
        WHERE id = '".$assetTypeDisplayID."'
    ");
    return $result;
}
function update_assetDropdownOption_active_with_assetDropdownOptionID($assetDropdownOptionID, $active){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_assets_dropdown_options
        SET display = '".$active."'
        WHERE id = '".$assetDropdownOptionID."'
    ");
    return $result;
}


function update_resetNewUserDefaultPassword_password_with_userID($password, $userID){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE users
        SET password = '". $password ."'
        Where id = '". $userID ."'
    ");
    return $result;
}

function update_lockdown_permission($groupId){
    global $emmadb;
    $insert = $emmadb->query("
        UPDATE emma_lockdown_privileges
        SET active = 0
        WHERE receiving_group_id = '". $groupId ."'
    ");
}

function update_close_ar_with_id($arId, $comments, $userId){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_ar
        SET closed = 1,
            closed_by_id = '". $userId ."',
            closed_date = NOW(),
            closed_comments = '". $comments ."'
        WHERE emma_ar_id = '". $arId ."'
    ");
    return $result;
}
function update_close_mar_with_id($arId, $comments, $userId){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_moss
        SET closed = 1,
            closed_by_id = '". $userId ."',
            closed_date = NOW(),
            closed_comments = '". $comments ."'
        WHERE emma_moss_id = '". $arId ."'
    ");
    return $result;
}
function update_lockdown_message($planId, $message){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_lockdown_notifications
        SET notification = '". $message ."'
        WHERE plan_id = '". $planId ."'
    ");
    return $result;
}

function update_lockdown_drill_message($planId, $message){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_lockdown_drill_notifications
        SET notification = '". $message ."'
        WHERE plan_id = '". $planId ."'
    ");
    return $result;
}

function update_nws_status_time($county, $DBAlertString){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE nws_current_weather_status
        SET last_pulled = NOW()
        WHERE zone_id = '". $county ."'
        AND weather_report = '".$DBAlertString."'
    ");
    return $result;
}

function update_plan_default_notifications($plan, $push, $email, $text){
    global $emmadb;
    $result = $emmadb->query("
        UPDATE emma_plan_settings
        SET notificationType_appNotification = ".$push.",
            notificationType_email = ".$email.",
            notificationType_SMStext = ".$text."
        WHERE plan_id = '". $plan ."'
    ");
    return $result;
}



?>