<?php

function get_emma_plan($id){
    global $emmadb;
    $plans = $emmadb->query("
      SELECT p.*, ep.package_name
      FROM emma_plans p
      LEFT JOIN emma_packages ep ON p.package_id = ep.package_id
      WHERE p.emma_plan_id = '" . $id . "'
    ");
    return $plans->fetch_assoc();
}

function get_active_event_types($id){
    global $emmadb;
    $eventTypes = $emmadb->query("
      select et.*
      from emergency_types et
      join emma_plan_event_types pet on et.emergency_type_id = pet.emma_emergency_type_id
      where pet.emma_plan_id = '" . $id . "'
    ");
//    while($eventType = $eventTypes->fetch_assoc()) {
//        return $eventType;
//    }
    while ($eventType = $eventTypes->fetch_assoc()){

        echo '<li><a href="#" style="justify-content:start;" class="event-type-link" data-img-filename="' .
            $eventType['img_filename'] . '" data-type-name="' .
            $eventType['name'] . '" data-type-id="' .
            $eventType['emergency_type_id'] . '">' .
            $eventType['name'] . '</a></li>';
    }
}

function get_upgrade_event_types($id){
    global $emmadb;
    $eventTypes = $emmadb->query("
      select et.*
      from emergency_types et
      where et.emergency_type_id not in (
        select pet.emma_emergency_type_id
        from emma_plan_event_types pet
        where emma_plan_id = '" . $id . "'
      )
    ");
    while ($eventType = $eventTypes->fetch_assoc()) {
        echo '<li><a href="#" style="justify-content:start;" class="event-type-link" data-img-filename="' .
            $eventType['img_filename'] . '" data-type-name="' .
            $eventType['name'] . '" data-type-id="' .
            $eventType['emergency_type_id'] . '">' .
            $eventType['name'] . '</a></li>';
    }
}

function get_asset_types($id){
    global $emmadb;
    $count=0;
    $assetTypes = $emmadb->query("
        SELECT e.*
        FROM emma_asset_types e 
        WHERE e.emma_plan_id = '". $id ."'
        ORDER BY e.name
    ");
    while ($assetType = $assetTypes->fetch_assoc()){
        echo'<div class="large-6 medium-8 small-12 columns">
                <div class="row">
                    <div class="large-6 medium-6 small-6 columns">
                        <label> 
                            <input type="checkbox" data-count="'. $count .'" class="plan-asset" name="assets[]" value="'. $assetType['emma_asset_type_id'] .'">'. $assetType['name'] .'
                        </label>
                    </div>
                    <div class="large-3 medium-3 small-3 columns" id="plan-asset-lat'. $count .'" hidden>
                        <input type="text" id="plan-asset-lat-in'. $count .'" style="height: 20px; font-size: 75%" placeholder="Latitude" name="assets-lat[]">
                    </div>
                    <div class="large-3 medium-3 small-3 columns" id="plan-asset-lon'. $count .'" hidden>
                        <input type="text" id="plan-asset-lon-in'. $count .'" style="height: 20px; font-size: 75%" placeholder="Longitude" name="assets-lon[]">
                    </div>
                </div>
            </div>';
        $count++;
    }
}

function get_emergency_types(){
    global $emmadb;
    $emergencyTypes = $emmadb->query("
        SELECT e.*
        FROM emergency_types e 
        ORDER BY e.name
    ");
    while($emergencyType = $emergencyTypes->fetch_assoc()){
        echo'<div class="large-6 medium-8 small-12 columns"><label><input type="checkbox" name="emergencies[]" value="'. $emergencyType['emergency_type_id'] .'">'. $emergencyType['name'] .'</label></div>';
    }
}

function list_active_events($plan){
    global $emmadb;
    $activeLives = $emmadb->query("
      SELECT e.emergency_id, et.name, e.drill
      FROM emergencies e
      JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
      WHERE e.emma_plan_id = '" . $plan . "'
      AND e.active = '1'
      AND e.drill = '0'
      AND e.parent_emergency_id IS NULL
    ");
    if($activeLives->num_rows > 0) {
        while ($activeLive = $activeLives->fetch_assoc()) {
            echo '<li><a href="./dashboard.php?content=event&id=' .
                $activeLive['emergency_id'] . '">' . $activeLive['name'] .
                '</a></li>';
        }
    }else{
        echo '<div class="row">No Active Events</div>';
    }
}
function list_active_help($plan){
    global $emmadb;
    global $USER;
    $sos_needs_query = $emmadb->query("
        SELECT emma_sos_id, CONCAT(u.firstname ,' ', u.lastname) AS sos_name
        FROM emma_sos AS s
        JOIN users AS u ON s.created_by_id = u.id AND u.emma_plan_id = '" . $plan . "'
        WHERE s.cancelled_date IS NULL
        AND s.help_date IS NOT NULL
        AND s.closed_date IS NULL
    ");
    if($sos_needs_query->num_rows > 0) {
        while ($sos = $sos_needs_query->fetch_assoc()) {
            echo '<li><a href="./dashboard.php?content=sos_details&id=' . $sos['emma_sos_id'] . '">' . $sos['sos_name'] . '</a></li>';
        }
    }else{
        echo '<div class="row">No Active SOS For '. $USER->emma_plan_name .' Plan.</div>';
    }
}
function list_active_sos($plan){
    global $emmadb;
    global $USER;
    $sos_query = $emmadb->query("
        SELECT emma_sos_id, CONCAT(u.firstname ,' ', u.lastname) AS sos_name
        FROM emma_sos AS s
        JOIN users AS u ON s.created_by_id = u.id AND u.emma_plan_id = '" . $plan . "'
        WHERE s.cancelled_date IS NULL
        AND s.help_date IS NULL
        AND s.pending_date IS NOT NULL
        AND s.closed_date IS NULL
    ");
    if ($sos_query->num_rows > 0) {
        while ($sos = $sos_query->fetch_assoc()) {
            echo '<li><a href="./dashboard.php?content=sos_details&id=' . $sos['emma_sos_id'] . '">' . $sos['sos_name'] . '</a></li>';
        }
    }else{
        echo '<div class="row">No Active SOS\'s For '. $USER->emma_plan_name .' Plan.</div>';
    }
}
function list_active_securities($plan){
    global $emmadb;
    global $USER;
    $activeSecurities = $emmadb->query("
        SELECT *
        FROM emma_securities s
        JOIN emma_security_types st ON s.emma_security_type_id = st.emma_security_type_id
        WHERE s.emma_plan_id = '" . $plan . "'
        AND s.active = '1'
    ");
    if($activeSecurities->num_rows > 0) {
        while ($activeSecurity = $activeSecurities->fetch_assoc()) {
            echo '<li><a href="./dashboard.php?content=security&id=' . $activeSecurity['emma_security_id'] . '">' . $activeSecurity['name'] . '</a></li>';
        }
    }else{
        echo '<div class="row">No Active Securities For '. $USER->emma_plan_name .'</div>';
    }
}


function select_plan_from_token($token){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*
        FROM emma_plan_tokens e 
        WHERE e.token = '". $token ."'
    ");
    return $result;
}

function login_with_token($loginToken){
    global $emmadb;
    $result = $emmadb->query("
                    SELECT u.id, CONCAT(u.firstname, ' ', u.lastname) AS full_name, u.username, u.emma_plan_id, u.emma_pin, u.auth, u.emma_code_generator, u.emma_plans, u.plan_god, t.plan_id AS token_plan, p.emails AS plan_emails, p.texts AS plan_texts, p.unregistered_texts, u.admin_user, u.no_logout, u.default_plans, u.fvm_id
                    FROM users u
                    JOIN emma_admin_login_tokens AS t ON t.user_id = u.id
                    JOIN emma_plans AS p ON t.plan_id = p.emma_plan_id
                    WHERE t.token = '" . $loginToken . "'
                    AND t.token_datetime > NOW()-INTERVAL 15 MINUTE
                    AND u.emma_plan_id IS NOT NULL
                ");
    return $result;


}
function login_with_username_and_password($username, $password){
    global $emmadb;
    $hashedPassword = hash('sha512', $password);
    $result = $emmadb->query("
                SELECT u.id, CONCAT(u.firstname, ' ', u.lastname) AS full_name, u.username, u.emma_plan_id, u.emma_pin, u.auth, u.emma_code_generator, u.emma_plans, u.plan_god, p.emails AS plan_emails, p.texts AS plan_texts, u.firstname, p.name AS emma_plan_name, u.activate_email, p.unregistered_texts, u.admin_user, u.no_logout, u.default_plans, u.fvm_id
                FROM users u
                JOIN emma_plans AS p ON p.emma_plan_id = u.emma_plan_id
                WHERE u.username = '" . $username . "'
                AND u.big_password = '" . $hashedPassword . "'
                AND u.emma_plan_id IS NOT NULL
            ");
    if($result->num_rows == 0){
        $result = $emmadb->query("
                SELECT u.id, CONCAT(u.firstname, ' ', u.lastname) AS full_name, u.username, u.emma_plan_id, u.emma_pin, u.auth, u.emma_code_generator, u.emma_plans, u.plan_god, p.emails AS plan_emails, p.texts AS plan_texts, u.firstname, p.name AS emma_plan_name, u.activate_email, p.unregistered_texts, u.admin_user, u.no_logout, u.default_plans, u.fvm_id
                FROM users u
                JOIN emma_plans AS p ON p.emma_plan_id = u.emma_plan_id
                WHERE u.username = '" . $username . "'
                AND u.password = PASSWORD('" . $password . "')
                AND u.emma_plan_id IS NOT NULL
                AND u.big_password IS NULL
            ");
        if($result->num_rows == 1){
            //update password to sha512
            $update = $emmadb->query("
                UPDATE users AS u
                SET u.big_password = '".$hashedPassword."'
                WHERE u.username = '" . $username . "'
                AND u.password = PASSWORD('" . $password . "')
                AND u.emma_plan_id IS NOT NULL
            ");
        }
    }

    return $result;


}
function login_with_user_id($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.id, CONCAT(u.firstname, ' ', u.lastname) AS full_name, u.username, u.emma_plan_id, u.emma_pin, u.auth, u.emma_code_generator, u.emma_plans, u.plan_god, p.emails AS plan_emails, p.texts AS plan_texts, u.firstname, p.name AS emma_plan_name, u.activate_email, p.unregistered_texts, u.admin_user
        FROM users u
        JOIN emma_plans AS p ON p.emma_plan_id = u.emma_plan_id
        WHERE u.id = '" . $userID . "'
        AND u.emma_plan_id IS NOT NULL
    ");
    return $result;
}
function login_with_username_and_pin($username, $pin){
    global $emmadb;
    $result = $emmadb->query("
                SELECT u.id, CONCAT(u.firstname, ' ', u.lastname) AS full_name, u.username, u.emma_plan_id, u.emma_pin, u.auth, u.emma_code_generator, u.emma_plans, u.plan_god, p.emails AS plan_emails, p.texts AS plan_texts, u.firstname, p.name AS emma_plan_name, u.activate_email
                FROM users u
                JOIN emma_plans AS p ON p.emma_plan_id = u.emma_plan_id
                WHERE u.username = '" . $username . "'
                AND u.emma_pin = '" . $pin . "'
                AND u.emma_plan_id IS NOT NULL
            ");
    return $result;


}
function select_oldPasswords_with_userID_and_password($userID, $password){
    global $emmadb;
    $hashedPassword = hash('sha512', $password);
    $result = $emmadb->query("
        SELECT p.*
        FROM emma_old_passwords AS p
        WHERE p.user_id = '".$userID."'
        AND p.password_hash = '".$hashedPassword."'
    ");
    return $result;
}
function select_planID_from_userID_and_groupList($userID, $groupList){
    global $emmadb;
    $result = $emmadb->query("
        SELECT DISTINCT p.emma_plan_id
        FROM emma_plans AS p
        JOIN emma_groups AS g ON p.emma_plan_id = g.emma_plan_id
        JOIN emma_multi_plan AS m ON p.emma_plan_id = m.plan_id AND m.user_id = '".$userID."' AND m.active = 1 
        WHERE g.emma_group_id in ('".$groupList."')
    ");
    return $result;
}
function select_plan_from_planID($planID){
    global $emmadb;
    $result =  $emmadb->query("
            SELECT *
            FROM emma_plans
            WHERE emma_plan_id = '".$planID."'
        ");
    return $result;
}
function select_user_from_userID($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM users
        WHERE id = '" . $userID . "'
    ");
    return $result;
}
function select_user_from_userID_arr($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.*
        FROM users u
        WHERE u.id IN ('" . implode('\', \'', $userID) . "')
    ");
    return $result;
}
function select_user_from_username($username){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.*
        FROM users u 
        WHERE u.username = '" . $username . "'
    ");
    return $result;
}
function select_user_from_auth($auth){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.*
        FROM users as u 
        WHERE u.auth = '".$auth."'
    ");
    return $result;
}
function select_user_from_auth_pin_and_groupID($auth, $pin, $groupID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT DISTINCT u.*, eg.info_only
        FROM users as u 
        LEFT JOIN emma_user_groups as eug on u.id = eug.user_id
        LEFT JOIN emma_groups as eg on u.emma_plan_id = eg.emma_plan_id AND eg.emma_group_id = '".$groupID."'
        WHERE u.auth = '" . $auth . "'
        AND u.emma_pin = '". $pin ."'
    ");
    return $result;
}
function select_userANDinfoOnly_from_auth($auth){
    global $emmadb;
    $result = $emmadb->query("
        SELECT DISTINCT u.id, u.auth, eg.info_only
        FROM users u
        LEFT JOIN emma_user_groups as eug on u.id = eug.user_id
        LEFT JOIN emma_groups as eg on u.emma_plan_id = eg.emma_plan_id AND eg.emma_group_id = eug.emma_group_id
        WHERE u.auth = '".$auth."'
    ");
    return $result;
}
function select_user_with_groupArray($groupArray){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.*
        FROM users u
        JOIN emma_user_groups ug on u.id = ug.user_id
        WHERE ug.emma_group_id IN ('" . implode('\', \'', $groupArray) . "')
        ");
    return $result;
}
function select_user_with_group($group){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.*
        FROM users u
        JOIN emma_user_groups ug on u.id = ug.user_id
        WHERE ug.emma_group_id = '" . $group . "'
        ");
    return $result;
}
function select_unreguser_with_group($group){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.*
        FROM users u
        JOIN emma_user_groups ug on u.id = ug.user_id
        JOIN emma_login_log l on u.username = l.username
        WHERE ug.emma_group_id = '" . $group . "'
        AND u.active = 1
        GROUP BY l.username
        HAVING COUNT(l.username) < 2
        ");
    return $result;
}
function select_lockdown_groups($plan){
    global $emmadb;
    $result = $emmadb->query("
        SELECT receiving_group_id AS groupID
        FROM emma_lockdown_privileges
        WHERE plan_id = '" . $plan . "'
    ");
    return $result;
}

function select_user_with_planID($userID, $planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT CONCAT(u.firstname, ' ', u.lastname) AS fullname, u.phone
        FROM users AS u
        JOIN emma_multi_plan emp on u.id = emp.user_id
        WHERE emp.plan_id = '".$planID."'
        AND u.id = '" . $userID . "'
        AND emp.active = 1
    ");
    return $result;
}

function select_plan_has_smartcop($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT p.*
        FROM emma_plans AS p
        WHERE p.emma_plan_id = '".$planID."'
        AND p.smartcop_lockdowns = 1
    ");
    return $result;
}
function select_plan_location_smartcop($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT p.*
        FROM emma_plan_locations AS p
        WHERE p.emma_plan_id = '".$planID."'
    ");
    return $result;
}


function select_distinctUser_with_groupString($groupString){
    global $emmadb;
    $result = $emmadb->query("
        SELECT DISTINCT u.username
        FROM users AS u
        JOIN emma_user_groups AS ug ON u.id = ug.user_id AND ug.emma_group_id in ('".$groupString."')
        WHERE u.active = 1 AND u.display = 'yes'
    ");
    return $result;
}
function select_rawUsers_with_userID($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT r.*
        FROM raw_users r 
        WHERE r.id = '" . $userID . "'
    ");
    return $result;
}

function select_groups_with_userID_and_planID($userID, $planID){
    global $emmadb;
    $result = $emmadb->query("
                SELECT g.*, e.securities, e.panic, e.lockdown, e.ar, e.mar
                FROM emma_user_groups ug
                JOIN emma_groups g ON ug.emma_group_id = g.emma_group_id
                LEFT JOIN emma_plans e ON g.emma_plan_id = e.emma_plan_id
                WHERE ug.user_id = '" . $userID . "'
                AND e.emma_plan_id = '" . $planID . "'
              ");
    return $result;
}
function select_groups_with_userID($userID){
    global $emmadb;
    $result = $emmadb->query("
                    SELECT g.*, e.securities, e.panic, e.lockdown, e.ar, e.mar
                    FROM emma_user_groups ug
                    JOIN emma_groups g ON ug.emma_group_id = g.emma_group_id
                    LEFT JOIN emma_plans e ON g.emma_plan_id = e.emma_plan_id
                    WHERE ug.user_id = '" . $userID . "'
                  ");
    return $result;
}
function select_userGroups_with_userID($userID){
    global $emmadb;
        $result = $emmadb->query("
        SELECT *
        FROM emma_user_groups
        WHERE user_id = '" . $userID . "'
    ");
    return $result;
}
function select_securityGroups_with_planID($planID){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_groups
        where security = 1
        and emma_plan_id = '" . $planID . "'
      ");
    return $result;
}
function select_allGroups_with_userID($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.emma_group_id
        FROM emma_user_groups AS g
        WHERE g.user_id = '".$userID."'
    ");
    return $result;
}
function select_groupIDs_with_userID_and_planID($userID, $planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.emma_group_id
        FROM emma_user_groups AS u
        JOIN emma_groups AS g ON u.emma_group_id = g.emma_group_id
        WHERE u.user_id = '".$userID."'
        AND g.emma_plan_id = '".$planID."'
    ");
    return $result;
}
function select_groups_with_planID($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.*
        FROM emma_groups AS g 
        WHERE g.emma_plan_id = " . $planID . "
        Order BY g.name
    ");
    return $result;
}
function select_group_with_groupID($groupID){
    global $emmadb;
    $result = $emmadb->query("
      select *
      from emma_groups
      where emma_group_id = '" . $groupID . "'
    ");
    return $result;
}
function select_groups_with_planArray($planArray){
    global $emmadb;
    $result = $emmadb->query("
                SELECT g.*
                FROM emma_groups AS g 
                WHERE g.emma_plan_id IN (".implode(',',$planArray).")
                ORDER BY g.name
            ");
    return $result;
}
function select_groups_with_groupArray($groupArray){
    global $emmadb;
    $result = $emmadb->query("
                SELECT g.*
                FROM emma_groups AS g 
                WHERE g.emma_group_id IN (".implode(',',$groupArray).")
                ORDER BY g.name
            ");
    return $result;
}
function select_groupsArray_with_userID($userID){
    global $emmadb;
    $result = $emmadb->query("
                SELECT group_concat(b.emma_group_id) AS groups
                FROM emma_user_groups AS b
                WHERE b.user_id = " . $userID . "
                GROUP BY b.user_id
            ");
    return $result;
}
function select_groupsPrivileges_with_groupArray_and_groupID($groupArray, $groupID){
    global $emmadb;
    $result = $emmadb->query("
                SELECT p.*
                FROM emma_group_privileges AS p
                WHERE p.emma_group_id IN (".implode(',',$groupArray).")
                AND p.emma_privilege_id = " . $groupID . "
            ");
    return $result;
}
function select_groupsPrivileges_with_groupString($groupString){
    global $emmadb;
    $result = $emmadb->query("
        SELECT DISTINCT emma_privilege_id
        FROM emma_group_privileges
        WHERE emma_group_id IN ('".$groupString."')
    ");
    return $result;
}
function select_groupPrivileges_with_groupID($groupID){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_group_privileges
        where emma_group_id = '" . $groupID . "'
    ");
    return $result;
}
function select_groupPrivileges_with_userID_and_planID($userID, $planID){
    global $emmadb;
    $result = $emmadb->query("
        select gp.*
        from emma_user_groups ug
          JOIN emma_groups g ON ug.emma_group_id = g.emma_group_id
          JOIN emma_group_privileges p ON g.emma_group_id = p.emma_group_id
          JOIN emma_groups gp ON p.emma_privilege_id = gp.emma_group_id
        where g.emma_plan_id = '". $planID ."'
        AND ug.user_id = '". $userID ."'
        ORDER BY gp.name
    ");
    return $result;
}
function select_sites_with_planID($planID){
    global $emmadb;
    $result = $emmadb->query("
              SELECT *
              FROM emma_sites
              WHERE emma_plan_id = '" . $planID . "'
            ");
    return $result;
}
function select_sitesAndSiteContacts_with_siteID($siteID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT es.*, esc.first_name, esc.last_name, esc.email, esc.phone
        FROM emma_sites es
        JOIN emma_site_contacts esc on es.emma_site_contact_id = esc.emma_site_contact_id
        WHERE es.emma_site_id = '" . $siteID . "'
    ");
    return $result;
}
function select_site_with_siteID($siteID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_sites
        WHERE emma_site_id = '" . $siteID . "'
      ");
    return $result;
}
function select_scripts_with_planID($planID){
    global $emmadb;
    $result = $emmadb->query("
                select *
                from emma_scripts
                where emma_script_type_id = 1
                and emma_plan_id = '" . $planID . "'
              ");
    return $result;
}
function select_scripts_with_scriptID($scriptID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT s.*
        FROM emma_scripts s 
        WHERE s.emma_script_id = '". $scriptID ."'
    ");
    return $result;
}
function select_broadcastTypes(){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_broadcast_types
        where `create` = 1
        order by name
    ");
    return $result;
}

function select_emergencies_with_planID($planID){
    global $emmadb;
    $result = $emmadb->query("
                    SELECT e.*
                    FROM emergencies e
                    WHERE e.emma_plan_id = '" . $planID . "'
                    AND e.active = '1'
                ");
    return $result;
}
function select_emergencies_with_user($userID){
    global $emmadb;
    $result = $emmadb->query("
                    SELECT e.*
                    FROM users u 
                    JOIN emma_multi_plan mp ON u.id = mp.user_id AND mp.active = 1
                    JOIN emergencies e ON mp.plan_id = e.emma_plan_id
                    WHERE u.id = '" . $userID . "'
                    AND e.active = '1'
                ");
    return $result;
}
function select_feed_emergencies_with_user($userID, $limit){
    global $emmadb;
    $result = $emmadb->query("
                    SELECT e.*
                    FROM users u 
                    JOIN emma_multi_plan mp ON u.id = mp.user_id
                    JOIN emergencies e ON mp.plan_id = e.emma_plan_id
                    WHERE u.id = '" . $userID . "'
                    AND mp.active = 1
                    AND e.active = '1'
                    AND e.created_date BETWEEN timestamp(DATE_SUB(NOW(), INTERVAL ". $limit ." MINUTE)) AND timestamp(NOW())
                ");
    return $result;
}
function select_emergencie_with_eventID($eventID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emergencies
        WHERE emergency_id = '" . $eventID . "'
    ");
    return $result;
}
function select_studentUser_with_userID($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT CONCAT(u.firstname, ' ', u.lastname) AS name
        FROM users u 
        WHERE u.id = '". $userID ."'
    ");
    return $result;
}
function select_parent_users_with_planID($planID){
    global $emmadb;
    $result = $emmadb->query("
                SELECT u.*
                FROM users u 
                WHERE u.emma_plan_id = '". $planID ."'
                AND u.student = 0
            ");
    return $result;
}
function select_securtityTypes_with_planID($planID){
    global $emmadb;
    $result = $emmadb->query("
              select st.*
              from emma_security_types st
              join emma_plan_security_types pst on st.emma_security_type_id = pst.emma_security_type_id
              where pst.emma_plan_id = '" . $planID . "'
              order by st.name
            ");
    return $result;
}
function select_eventTypes_with_planID($planID){
    global $emmadb;
    $result = $emmadb->query("
                select *
                from emma_plan_event_types pet
                join emergency_types et on pet.emma_emergency_type_id = et.emergency_type_id
                where pet.emma_plan_id = '" . $planID . "'
            ");
    return $result;
}
function select_eventTypes_with_multiplans($planID){
    global $emmadb;
    $result = $emmadb->query("
                select *
                from emma_plan_event_types pet
                join emergency_types et on pet.emma_emergency_type_id = et.emergency_type_id
                where pet.emma_plan_id IN (". implode(',', $planID) .")
            ");
    return $result;
}
function select_eventTypes_with_eventTypeID($eventTypeID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emergency_types
        WHERE emergency_type_id = '" . $eventTypeID . "'
    ");
    return $result;
}
function select_subEventTypes_with_emergencyTypeID($emergencyTypeID){
    global $emmadb;
    $result = $emmadb->query("
                SELECT *
                FROM emergency_sub_types
                WHERE emergency_type_id = '" . $emergencyTypeID . "'
                ORDER BY name
              ");
    return $result;
}
function select_emergencyResponseStatusIDs_with_emergencyTypeID($emergencyTypeID){
    global $emmadb;
    $result = $emmadb->query("
                SELECT etr.emergency_response_status_id
                FROM emergency_type_response_statuses etr
                WHERE etr.emergency_type_id = '".$emergencyTypeID."'
            ");
    return $result;
}
function select_emergencyResponseStatuses_with_emergencyResponseID($emergencyResponseID){
    global $emmadb;
    $result = $emmadb->query("
                SELECT e.name,e.color, e.emergency_response_status_id
                FROM emergency_response_status e
                WHERE e.emergency_response_status_id = '".$emergencyResponseID."' 
            ");
    return $result;
}

function select_activeEmergencies_with_planID($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*
        FROM emergencies e
        WHERE e.active = '1'
        AND e.emma_plan_id = '". $planID ."'
      ");
    return $result;
}
function select_activeParentEmergencies_with_planID($planID){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emergencies
        where emma_plan_id = '" . $planID . "'
        and parent_emergency_id is null
        and active = '1'
    ");
    return $result;
}

function select_allActivePlans(){
    global $emmadb;
    $result = $emmadb->query("
                SELECT p.*
                FROM emma_plans p
                WHERE p.date_expired > NOW()
                ORDER BY p.name
            ");
    return $result;
}
function select_allActivePlansCount(){
    global $emmadb;
    $result = $emmadb->query("
                SELECT COUNT(*) AS count
                FROM emma_plans 
                WHERE date_expired > NOW()
                ORDER BY name
            ");
    return $result;
}
function select_basePlanID_with_userID($userID){
    global $emmadb;
    $result = $emmadb->query("
        SElECT u.emma_plan_id
        FROM users AS u
        WHERE u.id = " . $userID . "
    ");
    return $result;
}
function select_multiPlanIDs_with_userID($userID){
    global $emmadb;
    $result = $emmadb->query("
            SELECT e.plan_id
            FROM emma_multi_plan e 
            WHERE e.user_id = '".$userID."'
            AND e.active = 1
        ");
    return $result;
}
function select_multiPlanCVS_with_userID($userID){
    global $emmadb;
    $result = $emmadb->query("
            SELECT e.plan_id, p.user_upload_folder
            FROM emma_multi_plan e 
            JOIN emma_plans AS p ON e.plan_id = p.emma_plan_id
            WHERE e.user_id = '" .$userID."'
            AND (e.guest_code = '' OR e.guest_code IS NULL)
            AND p.user_upload_folder IS NOT NULL
            AND e.active = 1
        ");
    return $result;
}
function select_multiPlanInfo_with_userID($userID){
    global $emmadb;
    $result = $emmadb->query("
            SELECT p.*
            FROM emma_plans p
            LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id AND mp.active = 1
            WHERE mp.user_id = '" . $userID . "'
            AND p.date_expired > NOW()
            AND p.active = 1
            ORDER BY p.name
        ");
    return $result;
}
function select_regions_with_planID($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT t.*
        FROM emma_plan_regions r 
        JOIN emma_plan_region_types t ON r.emma_region_id = t.plan_region_id
        WHERE r.emma_plan_id = '". $planID ."'
    ");
    return $result;
}
function select_multiPlans_with_userID($userID){
    global $emmadb;
    $result = $emmadb->query("
                SELECT mp.plan_id, p.name, eug.emma_group_id
                FROM emma_plans p
                LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id
                LEFT JOIN emma_user_groups as eug ON eug.user_id = '" . $userID . "'
                WHERE mp.user_id = '" . $userID . "'
                AND p.date_expired > NOW()
                AND mp.active = 1
                GROUP BY p.emma_plan_id
            ");
    return $result;
}
function select_regions_with_userID($userID){
    global $emmadb;
    $result = $emmadb->query("
                SELECT mp.plan_id, p.name, eug.emma_group_id, r.*
                FROM emma_plans p
                LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id AND mp.active = 1
                LEFT JOIN emma_user_groups as eug ON eug.user_id = '" . $userID . "'
                LEFT JOIN emma_plan_regions as pr ON mp.plan_id = pr.emma_plan_id
                JOIN emma_plan_region_types r ON pr.emma_region_id = r.plan_region_id
                WHERE mp.user_id = '" . $userID . "'
                AND p.date_expired > NOW()
                GROUP BY r.plan_region_id
            ");
    return $result;
}
function select_SOSDuration_with_planID($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT ep.sos_duration
        FROM emma_plans as ep
        WHERE ep.emma_plan_id = '".$planID."'
        ");
    return $result;
}

function select_activeSOS(){
    global $emmadb;
    $result = $emmadb->query("
                SELECT emma_sos_id, TIMEDIFF(NOW(), (ADDTIME(s.pending_date, (p.sos_duration / 60 * 100)))) AS time_over, s.pending_lat, s.pending_lng, u.auth
                FROM emma_sos AS s
                JOIN emma_plans AS p ON s.plan_id = p.emma_plan_id
                JOIN users AS u ON s.created_by_id = u.id
                WHERE s.cancelled_date IS NULL
                AND s.sos_date IS NULL
                AND s.help_date IS NULL
                AND s.pending_date IS NOT NULL
                AND s.closed_date IS NULL
                AND ADDTIME(s.pending_date, (p.sos_duration / 60 * 100)) < NOW();
            ");
    return $result;
}
function select_activeEmergencies_and_emergencyType_with_planID($planID){
    global $emmadb;
    $result = $emmadb->query("
                  SELECT e.emergency_id, et.name, e.drill
                  FROM emergencies e
                  JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
                  WHERE e.emma_plan_id = '" . $planID . "'
                  AND e.active = '1'
                  AND e.drill = '0'
                  AND e.parent_emergency_id IS NULL
                ");
    return $result;
}

function select_email_with_userID($userID){
    global $emmadb;
    $result = $emmadb->query("
            SELECT username
            FROM users
            WHERE id = '".$userID."'
        ");
    return $result;
}

function select_geofence_locations_with_id($ID){
    global $emmadb;
    $result = $emmadb->query("
            SELECT *
            FROM emma_geofence_locations
            WHERE id = '" . $ID . "'
        ");
    return $result;
}

function select_geofenceUserTracking_with_corners_duration_and_ios($adj_n_lat, $adj_s_lat, $adj_e_lng, $adj_w_lng, $duration, $ios){
    global $emmadb;
    $result = $emmadb->query("
                SELECT *
                FROM emma_geofence_user_tracking
                WHERE lat < " . $adj_n_lat . " AND lat > " . $adj_s_lat . "
                AND lng < " . $adj_e_lng . " AND lng > " . $adj_w_lng . "
                AND last_updated >= DATE_SUB(NOW(),INTERVAL ".$duration." HOUR)
                AND ios_background = ".$ios."
            ");
    return $result;
}


function select_rawStudentsToGuardians_with_guardianID($guardianID){
    global $emmadb;
    $result = $emmadb->query("
            SELECT r.*
            FROM emma_raw_students_to_guardians r 
            WHERE r.guardian_id = '". $guardianID ."'
        ");
    return $result;
}

function select_emmaNotificationDeviceGroups_with_keyName($keyName){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.notification_key_name AS nkeyName, g.notification_key AS nkey, g.last_updated
        FROM emma_notification_device_groups AS g
        WHERE g.notification_key_name = '". $keyName ."'
    ");
    return $result;
}

function select_notificationClients_with_planID_groupString_and_createdDate($planID, $groupString, $createdDate){
    global $emmadb;
    $result = $emmadb->query("
            Select DISTINCT c.client_id
            FROM emma_notification_clients AS c
            JOIN users AS u ON c.user_id = u.id AND u.emma_plan_id = '" . $planID . "'
            JOIN emma_user_groups AS g ON u.id = g.user_id
            WHERE g.emma_group_id IN ('" . $groupString . "')
            AND c.created_date > '".$createdDate."'
        ");
    return $result;
}
function select_notificationClients_with_planID_groupString($planID, $groupString){
    global $emmadb;
    $result = $emmadb->query("
            Select DISTINCT c.client_id
            FROM emma_notification_clients AS c
            JOIN users AS u ON c.user_id = u.id AND u.emma_plan_id = '" . $planID . "'
            JOIN emma_user_groups AS g ON u.id = g.user_id
            WHERE g.emma_group_id IN ('" . $groupString . "')
        ");
    return $result;
}

function select_securitiy_with_securityID($securityID){
    global $emmadb;
    $result = $emmadb->query("
      SELECT *
      FROM emma_securities
      WHERE emma_security_id = '" . $securityID . "'
    ");
    return $result;
}
function select_securitiy_with_planID($planID){
    global $emmadb;
    $result = $emmadb->query("
    SELECT e.*
    FROM emma_securities e 
    WHERE e.emma_plan_id = '". $planID ."'
    AND e.active = '1'
");
    return $result;
}
function select_securitiyType_with_typeID($typeID){
    global $emmadb;
    $result = $emmadb->query("
    SELECT *
    FROM emma_security_types
    WHERE emma_security_type_id = '" . $typeID . "'
  ");
    return $result;
}
function select_AllsecuritiyType(){
    global $emmadb;
    $result = $emmadb->query("
      select *
      from emma_security_types
    ");
    return $result;
}
function select_emmaAssetIcons_with_image($image){
    global $emmadb;
    $result = $emmadb->query("
        SELECT eac.asset_icon_id
        FROM emma_asset_icons as eac
        WHERE eac.image = '".$image."'
    ");
    return $result;
}

function select_callReportsBySite_with_reportID($reportID){
    global $emmadb;
    $result = $emmadb->query("
      select ecr.*, es.emma_site_name, es.emma_site_street_address, es.emma_site_city, es.emma_site_state, es.emma_site_zip
      from emma_call_reports ecr
      join emma_sites es on ecr.site_id = es.emma_site_id
      where id = '" . $reportID . "'
    ");
    return $result;
}
function select_witnessReport_with_reportNumber($reportNum){
    global $emmadb;
    $result = $emmadb->query("
        select @pos=@pos+1 as pos, ecrw.*
        from emma_call_report_witness ecrw
        join (select @pos:=0) p
        where call_report_number = '" . $reportNum . "'
    ");
    return $result;
}
function select_sosCallReports_with_reportID($reportID){
    global $emmadb;
    $result = $emmadb->query("
      select scr.*, s.closed_date, CONCAT(u.firstname, ' ', u.lastname) as closed_name
      from emma_sos_call_reports scr
      join emma_sos s on scr.emma_sos_id = s.emma_sos_id
      join users u on s.closed_by_id = u.id
      where emma_sos_call_report_id = '" . $reportID . "'
    ");
    return $result;
}
function select_emmaResources_with_fileString($fileString){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_resources
        WHERE file_string = " . $fileString . "
      ");
    return $result;
}
function select_emmaResources_with_resourceID_and_planID($resourceID, $planID){
    global $emmadb;
    $result = $emmadb->query("
      SELECT r.*
      FROM emma_resources AS r
      WHERE r.emma_resource_id = " . $resourceID . "
      AND r.emma_plan_id = " . $planID . "
    ");
    return $result;
}
function select_siteCount_with_planID($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT COUNT(1) as total
        FROM emma_sites
        WHERE emma_sites.emma_plan_id = '".$planID."'
    ");
    return $result;
}
function select_assetTypeDisplay_with_assetTypeDisplayID($assetTypeDisplayID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT eatd.*
        FROM emma_asset_type_display as eatd
        WHERE eatd.id = '".$assetTypeDisplayID."'
    ");
    return $result;
}
function select_guestCodes_with_guestCodeID($guestCodeID){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_guest_codes
        where guest_code_id = '".$guestCodeID."'
    ");
    return $result;
}
function select_securityMessages_with_securityID($securityID){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_security_messages
        where security_id = '" . $securityID . "'
    ");
    return $result;
}
function select_sosCallReports_with_sosID($sosID){
    global $emmadb;
    $result = $emmadb->query("
    select *
    from emma_sos_call_reports
    where emma_sos_id = '".$sosID."'
  ");
    return $result;
}








//--------------------------------------------------------------------------------------------------------------------------
function select_responder($responderID) {
  global $fvmdb;
  return $fvmdb->query("
    SELECT p.id, p.firstname, p.lastname, p.email, p.phone, p.address, p.city, p.state, p.zip, p.notes, p.locationid, p.display
    FROM persons p
    WHERE p.id = " . $responderID . "
  ");
}

function select_responder_classes($responderID) {
  global $fvmdb;
  return $fvmdb->query("
    SELECT tctl.name, tpwt.expiration, tpwt.completiondate, tpwt.classnumber, tpwt.display
    FROM training_personswithtypes tpwt
    JOIN training_classtypeslang tctl ON tpwt.typeid = tctl.typeid
    WHERE tpwt.personid = ".$responderID."
  ");
}

function select_aed($aedID) {
  global $fvmdb;
  return $fvmdb->query("
    SELECT
        a.aed_id,
        o.name as organization,
        l.name as org_location,
        ab.aed_brand_id,
        am.aed_model_id,
        ab.brand,
        am.model,
        a.serialnumber,
        a.lastcheck,
        a.location,
        a.notes,
        a.address,
        a.latitude,
        a.longitude,
        a.current,
        a.display,
        pad.aed_pad_id,
        pad.aed_pad_type_id,
        pad.lot as pad_lot,
        pad.expiration as pad_expiration,
        pad.type as pad_type,
        ped_pad.aed_pad_id as aed_ped_pad_id,
        ped_pad.aed_pad_type_id as aed_ped_pad_type_id,
        ped_pad.lot as ped_pad_lot,
        ped_pad.expiration as ped_pad_expiration,
        ped_pad.type as ped_pad_type,
        spare_pad.aed_pad_id as spare_aed_pad_id,
        spare_pad.aed_pad_type_id as spare_aed_pad_type_id,
        spare_pad.lot as spare_pad_lot,
        spare_pad.expiration as spare_pad_expiration,
        spare_pad.type as spare_pad_type,
        spare_ped_pad.aed_pad_id as spare_aed_ped_pad_id,
        spare_ped_pad.aed_pad_type_id as spare_aed_ped_pad_type_id,
        spare_ped_pad.lot as spare_ped_pad_lot,
        spare_ped_pad.expiration as spare_ped_pad_expiration,
        spare_ped_pad.type as spare_ped_pad_type,
        pak.aed_pak_id,
        pak.aed_pak_type_id,
        pak.lot as pak_lot,
        pak.expiration as pak_expiration,
        pak.type as pak_type,
        ped_pak.aed_pak_id as aed_ped_pak_id,
        ped_pak.aed_pak_type_id as aed_ped_pak_type_id,
        ped_pak.lot as ped_pak_lot,
        ped_pak.expiration as ped_pak_expiration,
        ped_pak.type as ped_pak_type,
        spare_pak.aed_pak_id as spare_aed_pak_id,
        spare_pak.aed_pak_type_id as spare_aed_pak_type_id,
        spare_pak.lot as spare_pak_lot,
        spare_pak.expiration as spare_pak_expiration,
        spare_pak.type as spare_pak_type,
        spare_ped_pak.aed_pak_id as spare_aed_ped_pak_id,
        spare_ped_pak.aed_pak_type_id as spare_aed_ped_pak_type_id,
        spare_ped_pak.lot as spare_ped_pak_lot,
        spare_ped_pak.expiration as spare_ped_pak_expiration,
        spare_ped_pak.type as spare_ped_pak_type,
        bat.aed_battery_id,
        bat.aed_battery_type_id,
        bat.lot as bat_lot,
        bat.expiration as bat_expiration,
        bat_t.type as bat_type,
        spare_bat.aed_battery_id as spare_aed_battery_id,
        spare_bat.aed_battery_type_id as spare_aed_battery_type_id,
        spare_bat.lot as spare_bat_lot,
        spare_bat.expiration as spare_bat_expiration,
        spare_bat_t.type as spare_bat_type,
        acc.aed_accessory_id,
        acc.aed_accessory_type_id,
        acc.lot as acc_lot,
        acc.expiration as acc_expiration,
        acc_t.type as acc_type
    FROM aeds a
             JOIN aed_models am ON a.aed_model_id = am.aed_model_id
             JOIN aed_brands ab ON am.aed_brand_id = ab.aed_brand_id
             JOIN locations l ON a.location_id = l.id
             JOIN organizations o ON l.orgid = o.id
             LEFT JOIN (
                select aed_pads.aed_pad_id, aed_pads.aed_pad_type_id, aed_pads.lot, aed_pads.expiration, aed_pad_types.type, aed_pads.aed_id
                from aed_pads
                join aed_pad_types on aed_pads.aed_pad_type_id = aed_pad_types.aed_pad_type_id AND aed_pad_types.pediatric = 0
                where aed_pads.display = 1 AND aed_pads.spare = 0) pad ON pad.aed_id = a.aed_id
             LEFT JOIN (
                select aed_pads.aed_pad_id, aed_pads.aed_pad_type_id, aed_pads.lot, aed_pads.expiration, aed_pad_types.type, aed_pads.aed_id
                from aed_pads
                join aed_pad_types on aed_pads.aed_pad_type_id = aed_pad_types.aed_pad_type_id AND aed_pad_types.pediatric = 0
                where aed_pads.display = 1 AND aed_pads.spare = 1) spare_pad ON spare_pad.aed_id = a.aed_id
             LEFT JOIN (
                select aed_pads.aed_pad_id, aed_pads.aed_pad_type_id, aed_pads.lot, aed_pads.expiration, aed_pad_types.type, aed_pads.aed_id
                from aed_pads
                join aed_pad_types on aed_pads.aed_pad_type_id = aed_pad_types.aed_pad_type_id AND aed_pad_types.pediatric = 1
                where aed_pads.display = 1 AND aed_pads.spare = 0) ped_pad ON ped_pad.aed_id = a.aed_id
             LEFT JOIN (
                select aed_pads.aed_pad_id, aed_pads.aed_pad_type_id, aed_pads.lot, aed_pads.expiration, aed_pad_types.type, aed_pads.aed_id
                from aed_pads
                join aed_pad_types on aed_pads.aed_pad_type_id = aed_pad_types.aed_pad_type_id AND aed_pad_types.pediatric = 1
                where aed_pads.display = 1 AND aed_pads.spare = 1) spare_ped_pad ON spare_ped_pad.aed_id = a.aed_id
             LEFT JOIN (
                select aed_paks.aed_pak_id, aed_paks.aed_pak_type_id, aed_paks.lot, aed_paks.expiration, aed_pak_types.type, aed_paks.aed_id
                from aed_paks
                join aed_pak_types on aed_paks.aed_pak_type_id = aed_pak_types.aed_pak_type_id AND aed_pak_types.pediatric = 0
                where aed_paks.display = 1 AND aed_paks.spare = 0) pak ON pak.aed_id = a.aed_id
             LEFT JOIN (
                select aed_paks.aed_pak_id, aed_paks.aed_pak_type_id, aed_paks.lot, aed_paks.expiration, aed_pak_types.type, aed_paks.aed_id
                from aed_paks
                join aed_pak_types on aed_paks.aed_pak_type_id = aed_pak_types.aed_pak_type_id AND aed_pak_types.pediatric = 0
                where aed_paks.display = 1 AND aed_paks.spare = 1) spare_pak ON spare_pak.aed_id = a.aed_id
             LEFT JOIN (
                select aed_paks.aed_pak_id, aed_paks.aed_pak_type_id, aed_paks.lot, aed_paks.expiration, aed_pak_types.type, aed_paks.aed_id
                from aed_paks
                join aed_pak_types on aed_paks.aed_pak_type_id = aed_pak_types.aed_pak_type_id AND aed_pak_types.pediatric = 1
                where aed_paks.display = 1 AND aed_paks.spare = 0) ped_pak ON pak.aed_id = a.aed_id
             LEFT JOIN (
                select aed_paks.aed_pak_id, aed_paks.aed_pak_type_id, aed_paks.lot, aed_paks.expiration, aed_pak_types.type, aed_paks.aed_id
                from aed_paks
                join aed_pak_types on aed_paks.aed_pak_type_id = aed_pak_types.aed_pak_type_id AND aed_pak_types.pediatric = 1
                where aed_paks.display = 1 AND aed_paks.spare = 1) spare_ped_pak ON spare_pak.aed_id = a.aed_id
             LEFT JOIN aed_batteries bat ON (bat.aed_id = a.aed_id AND bat.display = 1 AND bat.spare = 0)
             LEFT JOIN aed_battery_types bat_t ON bat.aed_battery_type_id = bat_t.aed_battery_type_id
             LEFT JOIN aed_batteries spare_bat ON (spare_bat.aed_id = a.aed_id AND spare_bat.display = 1 AND spare_bat.spare = 1)
             LEFT JOIN aed_battery_types spare_bat_t ON spare_bat.aed_battery_type_id = spare_bat_t.aed_battery_type_id
             LEFT JOIN aed_accessories acc ON acc.aed_id = a.aed_id AND acc.display = 1
             LEFT JOIN aed_accessory_types acc_t ON acc.aed_accessory_type_id = acc_t.aed_accessory_type_id
    WHERE a.aed_id = ".$aedID."
  ");
}

function select_asset_events($assetID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, CONCAT(u.firstname, ' ', u.lastname) AS creator, u.id AS user_id, u.username AS user_email, u.phone AS user_phone, t.name, t.emma_asset_type_id, i.image, i.asset_icon_id
        FROM emma_assets e
        JOIN emma_asset_types t ON e.emma_asset_type_id = t.emma_asset_type_id
        LEFT JOIN emma_asset_icons i ON t.emma_asset_icon_id = i.asset_icon_id
        JOIN users u ON e.created_by_id = u.id    
        WHERE e.emma_asset_id = '".$assetID."'
    ");
    return $result;
}
function select_asset_assetInfo($assetID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT eaci.date_time, eaci.info
        FROM emma_assets_custom_info as eaci
        WHERE eaci.id = '".$assetID."'
    ");
    return $result;
}
function select_asset_types($planID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT *
        FROM emma_asset_types
        where emma_plan_id = '" . $planID . "'
        and active = 1
        ORDER BY name
    ");
    return $result;
}
function select_assets_types($planID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT *
        FROM emma_asset_types
        where emma_plan_id = '" .$planID . "'
        ORDER BY name
    ");
    return $result;
}
function select_assetsMap_events($planID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT e.*
        FROM emma_assets e
        WHERE e.emma_plan_id = '" . $planID . "'
    ");
    return $result;
}
function select_assignGroups_allgroups($planID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT g.*
        FROM emma_groups AS g 
        WHERE g.emma_plan_id = ". $planID ."
    ");
    return $result;
}
function select_call_events($callID){
    global $emmadb;
    $result = $emmadb->query(sprintf("
    SELECT ecl.call_id, ecl.emergency_id, ecl.call_datetime, ecl.active, ecl.latitude, ecl.longitude, et.name AS type, CONCAT(u.firstname, ' ', u.lastname) AS username, u.id AS userid, u.username AS useremail, u.phone, e.drill, et.img_filename as type_filename
    FROM emma_911_call_log AS ecl
    JOIN users u on ecl.userid = u.id
    LEFT JOIN emergencies e on ecl.emergency_id = e.emergency_id
    LEFT JOIN emergency_types et on et.emergency_type_id = e.emergency_type_id
    WHERE ecl.call_id = '".$callID."'
    ", $callID));
    return $result;
}
function select_call_populateEvents($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, et.name AS type, et.img_filename
        FROM emergencies e
        JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
        WHERE e.active = '1'
        AND e.emma_plan_id = '" . $planID . "'
    ");
    return $result;
}
function select_call_assets($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT eat.name, a.latitude as lat, a.longitude as lng, a.address, eai.image
        FROM emma_assets a
        LEFT JOIN emma_asset_types as eat on eat.emma_asset_type_id = a.emma_asset_type_id
        LEFT JOIN emma_asset_icons as eai on eat.emma_asset_icon_id = eai.asset_icon_id
        WHERE a.active = '1'
        AND a.emma_plan_id = '".$planID."'
        AND a.active = 1
        AND a.deleted = 0
    ");
    return $result;
}
function select_call_broadcastTypes(){
    global $emmadb;
    $result =  $emmadb->query("
        select *
        from emma_broadcast_types
        order by name
    ");
    return $result;
}
function select_call_groups($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_groups
        WHERE emma_plan_id = '" . $planID . "'
        ORDER BY name
    ");
    return $result;
}
function select_callMap_events($callID){
    global $emmadb;
    $result = $emmadb->query(sprintf("
        SELECT ecl.call_id, ecl.emergency_id, ecl.call_datetime, ecl.active, ecl.latitude, ecl.longitude, et.name AS type, CONCAT(u.firstname, ' ', u.lastname) AS username, u.id AS userid, u.username AS useremail, u.phone, e.drill, et.img_filename as type_filename
        FROM emma_911_call_log AS ecl
        JOIN users u on ecl.userid = u.id
        LEFT JOIN emergencies e on ecl.emergency_id = e.emergency_id
        LEFT JOIN emergency_types et on et.emergency_type_id = e.emergency_type_id
        WHERE ecl.call_id = '".$callID."'
    ", $callID));
    return $result;
}
function select_callMap_populateEvents($planID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT e.*, et.name AS type, et.img_filename
        FROM emergencies e
        JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
        WHERE e.active = '1'
        AND e.emma_plan_id = '" . $planID . "'
    ");
    return $result;
}
function select_callMap_assets($planID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT eat.name, a.latitude as lat, a.longitude as lng, a.address, eai.image
        FROM emma_assets a
        LEFT JOIN emma_asset_types as eat on eat.emma_asset_type_id = a.emma_asset_type_id
        LEFT JOIN emma_asset_icons as eai on eat.emma_asset_icon_id = eai.asset_icon_id
        WHERE a.active = '1'
        AND a.emma_plan_id = '".$planID."'
        AND a.active = 1
        AND a.deleted = 0
    ");
    return $result;
}
function select_changePin_groups($planID){
    global $emmadb;
    $result =  $emmadb->query("
        select *
        from emma_groups
        where emma_plan_id = '" . $planID . "'
        and info_only = 1
        order by name
    ");
    return $result;
}
function select_createGeofence_fences($planID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT g.id, g.fence_name
        FROM emma_geofence_locations g
        WHERE g.plan_id = '". $planID ."'
        ORDER BY g.fence_name
    ");
    return $result;
}
function select_createSite_siteContacts($planID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT CONCAT(esc.first_name, ' ', esc.last_name) as name, esc.emma_site_contact_id
        FROM emma_site_contacts as esc     
        WHERE esc.emma_plan_id = '".$planID."'
    ");
    return $result;
}
function select_createUser_allgroups($planID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT g.*
        FROM emma_groups AS g 
        WHERE g.emma_plan_id = " . $planID . "
    ");
    return $result;
}
function select_displayHelp_resources($resourceID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT r.*
        FROM emma_help_resources AS r
        WHERE r.emma_resource_id = " . $resourceID . "
        AND r.active = 1
    ");
    return $result;
}
function select_displayHelp_resourceName($resourceID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT r.file_name
        FROM emma_help_resources AS r
        WHERE r.emma_resource_id = " . $resourceID . "
    AND r.active = 1
    ");
    return $result;
}
function select_displayHelp_resourceTypes(){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT *
        FROM emma_help_resource_types
        WHERE active = 1
        ORDER BY resource_name
    ");
    return $result;
}
function select_drill_drills($emergencyID){
    global $emmadb;
    $result =  $emmadb->query(sprintf("
        SELECT e.*, et.name AS type, et.img_filename AS type_filename, es.emma_site_id, es.emma_site_name, CONCAT(u.firstname, ' ', u.lastname) AS reported_by, u.id AS user_id, u.username AS user_email, u.phone AS user_phone, est.name AS sub_type_name, u.emma_pin AS user_pin, e.created_pin
        FROM emergencies e
        JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
        JOIN emergency_sub_types est ON e.emergency_sub_type_id = est.emergency_sub_type_id
        JOIN emma_sites es ON e.emma_site_id = es.emma_site_id
        JOIN users u ON e.created_by_id = u.id
        WHERE emergency_id = %s
    ", $emergencyID));
    return $result;
}
function select_drills_eventGroups($eventID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT rg.emma_group_id
        FROM emergency_received_groups rg
        WHERE rg.emergency_id = '" . $eventID . "'
    ");
    return $result;
}
function select_drills_groups($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_groups
        WHERE emma_plan_id = '" . $planID . "'
    ");
    return $result;
}
function select_drills_alertTypes($planID){
    global $emmadb;
    $result = $emmadb->query("
        select @pos:=@pos+1 as pos, et.*
        from emma_plan_event_types pet
        join emergency_types et on pet.emma_emergency_type_id = et.emergency_type_id
        join (select @pos:=0) p
        where pet.emma_plan_id = '".$planID."'
    ");
    return $result;
}
function select_drills_notReveivedUsers($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.id, CONCAT(u.firstname, ' ', u.lastname) AS user, u.username AS email, u.phone
        FROM users u
        WHERE u.emma_plan_id = '" . $planID . "'
        AND u.display = 'yes'
    ");
    return $result;
}
function select_drills_broadcastScripts($planID, $type){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_scripts
        where emma_script_type_id = '".$type."'
        and emma_plan_id = '" . $planID . "'
    ");
    return $result;
}
function select_editAssetInfo_getIcon(){
    global $emmadb;
    $result = $emmadb->query("
        SELECT eac.asset_icon_id, eac.image
        FROM emma_asset_icons as eac                
    ");
    return $result;
}
function select_editGroup_groupPrivileges($groupID, $privilegeID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT p.*
        FROM emma_group_privileges AS p
        WHERE p.emma_group_id = " . $groupID . "
        AND p.emma_privilege_id = " . $privilegeID . "
    ");
    return $result;
}
function select_editGroup_feedPrivileges($groupID, $privilegeID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT p.*
        FROM emma_feed_view_privilege AS p
        WHERE p.group_id = " . $groupID . "
        AND p.group_privilege_id = " . $privilegeID . "
    ");
    return $result;
}
function select_editGroup_allTypes($planID){
    global $emmadb;
    $result = $emmadb->query("
      SELECT e.img_filename, e.name, e.emergency_type_id
      FROM emma_plan_event_types AS g 
      JOIN emergency_types e ON g.emma_emergency_type_id = e.emergency_type_id
      WHERE g.emma_plan_id = '".$planID."'
      ORDER BY e.name
    ");
    return $result;
}
function check_active_sos($userID){
    global $emmadb;
    $result = $emmadb->query("
      SELECT *
        FROM emma_sos AS s
        WHERE s.created_by_id = '". $userID ."' 
        AND s.cancelled_date IS NULL
        AND s.help_date IS NOT NULL
        AND s.closed_date IS NULL
    ");
    return $result;
}
function select_editGroup_groupIcons($groupID, $eventID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.*
        FROM emma_group_events AS g
        WHERE g.emma_group_id = " . $groupID . "
        AND g.event_id = " . $eventID . "
        AND g.active = 1
    ");
    return $result;
}
function select_editPlan_eventTypes($planID){
    global $emmadb;
    $result = $emmadb->query("
        select et.*
        from emergency_types et
        where et.emergency_type_id not in (
            select pet.emma_emergency_type_id
            from emma_plan_event_types pet
            where emma_plan_id = '" . $planID . "'
            )
    ");
    return $result;
}
function select_editPlan_securityTypes(){
    global $emmadb;
    $result = $emmadb->query("
        select st.*
        from emma_security_types st
    ");
    return $result;
}
function select_editPlan_planSecurityTypes($planID, $securityTypeID){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_plan_security_types
        where emma_plan_id = '" . $planID . "'
        and emma_security_type_id = '" .$securityTypeID . "'
    ");
    return $result;
}
function select_editSite_contacts($siteID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT c.first_name, c.last_name, c.email, c.phone, c.emma_site_contact_id as contact_id
        FROM emma_site_contacts AS c
        WHERE c.emma_site_contact_id = " .$siteID . "
    ");
    return $result;
}
function select_editUser_usergroups($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT emma_group_id
        FROM emma_user_groups
        WHERE user_id = " . $userID . "
    ");
    return $result;
}
function select_editUser_allgroups($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.*
        FROM emma_groups AS g 
        WHERE g.emma_plan_id = " . $planID . "
    ");
    return $result;
}
function select_event_events($eventID){
    global $emmadb;
    $result = $emmadb->query(sprintf("
    SELECT e.*, et.name AS type, et.img_filename AS type_filename, es.emma_site_id, es.emma_site_name, CONCAT(u.firstname, ' ', u.lastname) AS reported_by, u.id AS user_id, u.username AS user_email, u.phone AS user_phone, est.name AS sub_type_name, u.emma_pin AS user_pin, e.created_pin, ecl.call_datetime, CONCAT(ucall.firstname, ' ', ucall.lastname) as called_by, ecl.latitude as lat, ecl.longitude as lng 
    FROM emergencies as e
    JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
    JOIN emergency_sub_types est ON e.emergency_sub_type_id = est.emergency_sub_type_id
    LEFT JOIN emma_sites es ON e.emma_site_id = es.emma_site_id
    JOIN users u ON e.created_by_id = u.id
    LEFT JOIN emma_911_call_log ecl on e.emergency_id = ecl.emergency_id
    LEFT JOIN users ucall on ecl.userid = ucall.id
    WHERE e.emergency_id = %s
", $eventID));
    return $result;
}
function select_event_policeCalls($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT ecl.latitude, ecl.longitude, ecl.call_datetime, CONCAT(u.firstname, ' ', u.lastname) as user_name
        FROM emma_911_call_log as ecl
        JOIN users u on ecl.userid = u.id
        WHERE ecl.plan_id = '".$planID."'
        AND ecl.active = 1
    ");
    return $result;
}
function select_event_assets($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT eat.name, a.latitude as lat, a.longitude as lng, a.address, eai.image
        FROM emma_assets a
        LEFT JOIN emma_asset_types as eat on eat.emma_asset_type_id = a.emma_asset_type_id
        LEFT JOIN emma_asset_icons as eai on eat.emma_asset_icon_id = eai.asset_icon_id
        WHERE a.active = '1'
        AND a.emma_plan_id = '".$planID."'
        AND a.active = 1
        AND a.deleted = 0
    ");
    return $result;
}
function select_event_openSubEvents($emergencyID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emergencies e
        WHERE parent_emergency_id = '" . $emergencyID . "'
        AND active = 1
    ");
    return $result;
}
function select_event_reveivedGroups($emergencyID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT rg.emma_group_id
        FROM emergency_received_groups rg
        WHERE rg.emergency_id = '" . $emergencyID . "'
    ");
    return $result;
}
function select_event_groups($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_groups
        WHERE emma_plan_id = '" . $planID . "'
        ORDER BY name
    ");
    return $result;
}
function select_event_alertTypes($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT @pos:=@pos+1 AS pos, et.*
        FROM emma_plan_event_types pet
        JOIN emergency_types et ON pet.emma_emergency_type_id = et.emergency_type_id
        JOIN (SELECT @pos:=0) p
        WHERE pet.emma_plan_id = '" . $planID . "'
    ");
    return $result;
}
function select_event_notReveivedGroups($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.id, CONCAT(u.firstname, ' ', u.lastname) AS user, u.username AS email, u.phone
        FROM users u
        WHERE u.emma_plan_id = '" . $planID . "'
        AND u.display = 'yes'
      ");
    return $result;
}
function select_event_broadcastTypes(){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_broadcast_types
        where close = 1
        order by name
    ");
    return $result;
}
function select_event_broadcastTypes2(){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_broadcast_types
        where broadcast = 1
        order by name
    ");
    return $result;
}
function select_geofence_geofences($geofenceID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.*, g2.fence_name as main
        FROM emma_geofence_locations g
        LEFT JOIN emma_geofence_locations g2 ON g.main_fence = g2.id
        WHERE g.id = " . $geofenceID . "
    ");
    return $result;
}
function select_geofence_geofence($geofenceID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.*
        FROM emma_geofence_locations g
        WHERE g.id = " . $geofenceID . "
    ");
    return $result;
}
function select_geofence_broadcastScripts($planID){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_scripts
        where emma_plan_id = '" . $planID . "'
    ");
    return $result;
}
function select_geofence_scriptGroups(){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_broadcast_types
        order by name
    ");
    return $result;
}
function select_geofence_geofenceScripts($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.text, g.name
        FROM emma_mass_notification_scripts g
        WHERE g.emma_plan_id = '". $planID ."'
        AND g.active = 1
        ORDER BY g.name
    ");
    return $result;
}
function select_geofence_messages($geofenceID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT gm.*, concat(u.firstname, ' ', u.lastname) AS user, u.id aS userId
        FROM emma_geofence_messages gm    
        JOIN users u ON gm.created_by_id = u.id     
        WHERE gm.geofence_id = '". $geofenceID ."'
    ");
    return $result;
}
function select_group_allevents($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*
        FROM emma_plan_event_types AS e
        WHERE e.emma_plan_id = '" . $planID . "'
        ORDER BY e.emma_emergency_type_id
    ");
    return $result;
}
function select_group_groupPrivileges($groupID, $privilegeID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT p.*
        FROM emma_group_privileges AS p
        WHERE p.emma_group_id = " . $groupID . "
        AND p.emma_privilege_id = " . $privilegeID . "
    ");
    return $result;
}
function select_group_feedPrivileges($groupID, $privilegeID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT p.*
        FROM emma_feed_view_privilege AS p
        WHERE p.group_id = " . $groupID . "
        AND p.group_privilege_id = " . $privilegeID . "
        AND p.active = 1
    ");
    return $result;
}
function select_group_allgroups($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.*
        FROM emma_groups AS g 
        WHERE g.emma_plan_id = '" . $planID . "'
        ORDER BY g.name
    ");
    return $result;
}
function select_group_allTypes($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.img_filename, e.name, e.emergency_type_id
        FROM emma_plan_event_types AS g 
        JOIN emergency_types e ON g.emma_emergency_type_id = e.emergency_type_id
        WHERE g.emma_plan_id = '".$planID."'
        ORDER BY e.name
    ");
    return $result;
}
function select_group_groupIcons($groupID, $eventID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT p.*
        FROM emma_group_events AS p
        WHERE p.emma_group_id = " . $groupID . "
        AND p.event_id = " . $eventID . "
        AND p.active = 1
    ");
    return $result;
}
function select_group_people($groupID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT u.*
        FROM users AS u
        JOIN emma_user_groups AS b ON u.id = b.user_id
        WHERE b.emma_group_id = " . $groupID . "
        AND u.display = 'yes'
    ");
    return $result;
}
function select_plan_new_user(){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT u.*, p.name, p.emma_plan_id
        FROM users AS u
        JOIN emma_plans p ON u.emma_plan_id = p.emma_plan_id
        WHERE u.display = 'yes'
        AND u.created_date > DATE_SUB(NOW(), INTERVAL 1 DAY)
    ");
    return $result;
}
function select_plan_new(){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT p.name, p.emma_plan_id
        FROM emma_plans p 
        WHERE p.date_active > DATE_SUB(NOW(), INTERVAL 1 DAY) 
    ");
    return $result;
}
function select_plan_new_drills(){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT e.*, p.name, p.emma_plan_id
        FROM emergencies e
        JOIN emma_plans p ON e.emma_plan_id = p.emma_plan_id
        WHERE e.created_date > DATE_SUB(NOW(), INTERVAL 1 DAY) 
        AND e.drill = 1 
        AND e.active = 1
    ");
    return $result;
}
function select_plans_new_login(){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT p.emma_plan_id, p.name, ul.login_datetime
        FROM(SELECT u.emma_plan_id, l.login_datetime
            FROM(SELECT MAX(login_datetime) as login_datetime, username
                FROM emma_login_log
                  GROUP BY username
                  ORDER BY login_datetime DESC
                ) AS l
            JOIN users u ON l.username = u.username
            GROUP BY u.emma_plan_id
            ) AS ul
        JOIN emma_plans p ON ul.emma_plan_id = p.emma_plan_id
        WHERE ul.login_datetime > DATE_SUB(NOW(), INTERVAL 1 DAY)
        ORDER BY p.name
    ");
    return $result;
}
function select_plans_911admin(){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT p.name, g.name as groupname, u.username
        FROM users u 
        JOIN emma_user_groups ug ON u.id = ug.user_id
        JOIN emma_groups g ON ug.emma_group_id = g.emma_group_id
        JOIN emma_plans p ON g.emma_plan_id = p.emma_plan_id
        WHERE g.`911admin` = 1
    ");
    return $result;
}
function select_plans_publicsafety(){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT COUNT(p.id) as count
        FROM emma_plans p 
        WHERE p.public_safety = 1
    ");
    return $result;
}

function select_plan_group_with_name($planId, $groupName){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT g.emma_group_id
        FROM emma_groups g
        WHERE g.emma_plan_id = '". $planId ."'
        AND g.name = '". $groupName ."'
    ");
    return $result;
}
function select_guestCode_guestCodes($guestCodeID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT g.*, group_concat(DISTINCT eg.name ORDER BY eg.name) AS groupName, ep.name AS planName
        FROM emma_guest_codes AS g
        LEFT JOIN emma_groups eg ON g.group_id = eg.emma_group_id
        LEFT JOIN emma_plans ep ON g.emma_plan_id = ep.emma_plan_id
        WHERE g.guest_code_id = '" . $guestCodeID . "'
    ");
    return $result;
}
function select_guestCode_people($guestCodeID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT u.*
        FROM users AS u 
        LEFT JOIN emma_multi_plan AS mp ON u.id = mp.user_id AND mp.active = 1
        WHERE mp.guest_code = '" . $guestCodeID . "'
        AND u.display = 'yes'
        GROUP BY u.id
        ORDER BY u.firstname
    ");
    return $result;
}
function select_help_resources(){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT *
        FROM emma_help_resource_types
        WHERE active = 1
    ");
    return $result;
}
function select_help_resourceTypes(){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT *
        FROM emma_resource_types
        WHERE active = 1
        ORDER BY resource_name
    ");
    return $result;
}
function select_helpTypes_types($typeID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT t.resource_name AS name
        FROM emma_help_resource_types AS t 
        WHERE t.emma_resource_type_id = " . $typeID . "
    ");
    return $result;
}
function select_helpTypes_resourceQueryUser($typeID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT r.*
        FROM emma_help_resources AS r
        WHERE r.emma_resource_type_id = " . $typeID . "
        AND r.active = 1       
        AND r.user = 1                 
        ORDER BY r.file_name
    ");
    return $result;
}
function select_helpTypes_resourceQuerysecurity($typeID){
    global $emmadb;
    $result =  $emmadb->query("
        SELECT r.*
        FROM emma_help_resources AS r
        WHERE r.emma_resource_type_id = " . $typeID . "
        AND r.active = 1    
        AND r.security = 1                    
        ORDER BY r.file_name
    ");
    return $result;
}
function select_helpTypes_resourceQueryadmin911($typeID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT r.*
        FROM emma_help_resources AS r
        WHERE r.emma_resource_type_id = " . $typeID . "
        AND r.active = 1    
        AND r.admin911 = 1                    
        ORDER BY r.file_name
    ");
    return $result;
}
function select_helpTypes_resourceTypes(){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_help_resource_types
        WHERE active = 1
    ");
    return $result;
}
function select_home_plans($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT p.name, p.emma_plan_id
        FROM emma_multi_plan m
        LEFT JOIN emma_plans p ON m.plan_id = p.emma_plan_id
        WHERE m.user_id = '". $userID ."'
        AND m.active = 1
        AND p.active = 1
        ORDER BY p.name
    ");
    return $result;
}
function select_home_activeLives($planArray){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.emergency_id, et.name, e.drill, p.name as planname
        FROM emergencies e
        JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
        JOIN emma_plans p ON e.emma_plan_id = p.emma_plan_id
        WHERE e.emma_plan_id IN (". implode(',',$planArray) .")
        AND e.active = '1'
        AND e.drill = '0'
        AND e.parent_emergency_id IS NULL
    ");
    return $result;
}
function select_home_activeDrills($planArray){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.emergency_id, et.name, e.drill, p.name as planname
        FROM emergencies e
        JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
        JOIN emma_plans p ON e.emma_plan_id = p.emma_plan_id
        WHERE e.emma_plan_id IN (". implode(',',$planArray) .")
        AND e.active = '1'
        AND e.drill = '1'
        AND e.parent_emergency_id IS NULL
    ");
    return $result;
}
function select_home_securityCheck($planArray){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_plans
        where emma_plan_id IN (". implode(',',$planArray) .")
        and securities = 1
    ");
    return $result;
}
function select_home_activeSecurities($planArray){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *, p.name 
        FROM emma_securities s
        JOIN emma_security_types st on s.emma_security_type_id = st.emma_security_type_id
        JOIN emma_plans p ON s.emma_plan_id = p.emma_plan_id 
        WHERE s.emma_plan_id IN (". implode(',',$planArray) .")
        AND s.active = '1'
        GROUP BY s.emma_plan_id 
    ");
    return $result;
}
function get_home_security_count($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT * 
        FROM emma_securities s
        JOIN emma_security_types st on s.emma_security_type_id = st.emma_security_type_id
        JOIN emma_plans p ON s.emma_plan_id = p.emma_plan_id 
        WHERE s.emma_plan_id = '". $planID ."'
        AND s.active = '1'
        ORDER BY p.name
    ");
    return $result;
}
function select_home_sosCheck($planArray){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_plans
        where emma_plan_id IN (". implode(',',$planArray) .")
        and panic = 1
    ");
    return $result;
}
function select_home_sosQuery($planArray){
    global $emmadb;
    $result = $emmadb->query("
      SELECT emma_sos_id, CONCAT(u.firstname ,' ', u.lastname) AS sos_name, p.name, p.emma_plan_id
FROM emma_sos AS s
  JOIN users AS u ON s.created_by_id = u.id
  JOIN emma_multi_plan mp ON u.id = mp.user_id
  JOIN emma_plans p ON s.plan_id = p.emma_plan_id
WHERE s.cancelled_date IS NULL
      AND s.help_date IS NULL
      AND s.pending_date IS NOT NULL
      AND s.closed_date IS NULL
      AND s.plan_id IN (". implode(',',$planArray) .")
      AND mp.active = 1
GROUP BY p.emma_plan_id
    ");
    return $result;
}
function select_home_sosQuery_count($plan){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_sos AS s
        JOIN users AS u ON s.created_by_id = u.id AND u.emma_plan_id = '". $plan ."'
        WHERE s.cancelled_date IS NULL
        AND s.help_date IS NULL
        AND s.pending_date IS NOT NULL
        AND s.closed_date IS NULL
    ");
    return $result;
}
function select_home_sosneeds($planArray){
    global $emmadb;
    $result = $emmadb->query("
        SELECT emma_sos_id, CONCAT(u.firstname ,' ', u.lastname) AS sos_name, p.name, p.emma_plan_id
FROM emma_sos AS s
  JOIN users AS u ON s.created_by_id = u.id
  JOIN emma_multi_plan mp ON u.id = mp.user_id
  JOIN emma_plans p ON s.plan_id = p.emma_plan_id
WHERE s.cancelled_date IS NULL
      AND s.help_date IS NOT NULL
      AND s.closed_date IS NULL
      AND s.plan_id IN (". implode(',',$planArray) .")
      AND mp.active = 1
GROUP BY p.emma_plan_id
    ");
    return $result;
}
function select_home_sosneeds_count($plan){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_sos AS s
        JOIN users AS u ON s.created_by_id = u.id AND u.emma_plan_id = '". $plan ."'
        WHERE s.cancelled_date IS NULL
        AND s.help_date IS NOT NULL
        AND s.closed_date IS NULL
    ");
    return $result;
}


function select_feed_events($planArray, $hour){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.created_date as sortdate, CONCAT(u.firstname ,' ', u.lastname) AS name, u.username, e.comments as notification, 'event' as alert_type, e.emergency_id as alert_id, et.name as type_name, e.drill
        FROM emergencies e
        JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
        JOIN emma_plans p ON e.emma_plan_id = p.emma_plan_id
        JOIN users AS u ON e.created_by_id = u.id
        WHERE e.emma_plan_id IN (". implode(',',$planArray) .")
        AND e.parent_emergency_id IS NULL
        AND e.created_date >= DATE_SUB(now(),INTERVAL ". $hour ." HOUR)
    ");
    return $result;
}
function select_feed_sos($planArray, $hour){
    global $emmadb;
    $result = $emmadb->query("
      SELECT s.created_date as sortdate, CONCAT(u.firstname ,' ', u.lastname) AS name, u.username, 'Need Help' as notification, 'sos_details' as alert_type, s.emma_sos_id as alert_id, 'EMMA SOS' as type_name
FROM emma_sos AS s
  JOIN users AS u ON s.created_by_id = u.id
  JOIN emma_multi_plan mp ON u.id = mp.user_id
  JOIN emma_plans p ON s.plan_id = p.emma_plan_id
WHERE s.cancelled_date IS NULL
      AND s.closed_date IS NULL 
      AND s.help_date IS NOT NULL
      AND s.plan_id IN (". implode(',',$planArray) .")
      AND s.created_date >= DATE_SUB(now(),INTERVAL ". $hour ." HOUR)
      AND mp.active = 1
GROUP BY p.emma_plan_id
    ");
    return $result;
}
function select_feed_mass($planArray, $hour){
    global $emmadb;
    $result = $emmadb->query("
        SELECT c.created_date as sortdate, CONCAT(u.firstname ,' ', u.lastname) AS name, u.username, c.notification, 'mass_communication' as alert_type, c.emma_mass_communication_id as alert_id, 'Mass Notification' as type_name
        FROM emma_mass_communications c 
        JOIN users AS u ON c.created_by_id = u.id
        JOIN emma_plans p ON c.emma_plan_id = p.emma_plan_id
        WHERE c.emma_plan_id IN (". implode(',',$planArray) .")
        AND c.created_date >= DATE_SUB(now(),INTERVAL ". $hour ." HOUR)
    ");
    return $result;
}
function select_feed_lockdowns($planArray, $hour){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.created_date as sortdate, CONCAT(u.firstname ,' ', u.lastname) AS name, u.username, n.notification, 'lockdown' as alert_type, e.lockdown_id as alert_id, 'Lockdown' as type_name, e.drill
        from emma_lockdowns e
        JOIN users AS u ON e.created_by_id = u.id
        JOIN emma_plans p ON e.plan_id = p.emma_plan_id 
        JOIN emma_lockdown_notifications n ON e.plan_id = n.plan_id
        WHERE e.plan_id IN (". implode(',',$planArray) .")
        AND e.created_date >= DATE_SUB(now(),INTERVAL ". $hour ." HOUR)
    ");
    return $result;
}
function select_feed_mlockdowns($planArray, $hour){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.created_date as sortdate, CONCAT(u.firstname ,' ', u.lastname) AS name, u.username, n.notification, 'mlockdown' as alert_type, e.m_lockdown_id as alert_id, 'Modified Lockdown' as type_name
        from emma_modified_lockdowns e
        JOIN users AS u ON e.created_by_id = u.id
        JOIN emma_plans p ON e.plan_id = p.emma_plan_id 
        JOIN emma_modified_lockdown_notifications n ON e.plan_id = n.plan_id
        WHERE e.plan_id IN (". implode(',',$planArray) .")
        AND e.created_date >= DATE_SUB(now(),INTERVAL ". $hour ." HOUR)
    ");
    return $result;
}
function select_feed_geofence($planArray, $hour){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.created_date as sortdate, CONCAT(u.firstname ,' ', u.lastname) AS name, u.username, e.comments as notification, 'geofence' as alert_type, l.id as alert_id, 'Geofence' as type_name, l.fence_name as geofencename
        from emma_geofence_messages e
        JOIN emma_geofence_locations l ON e.geofence_id = l.id
        JOIN users AS u ON e.created_by_id = u.id
        JOIN emma_plans p ON e.plan_id = p.emma_plan_id 
        WHERE l.plan_id IN (". implode(',',$planArray) .")
        AND e.created_date >= DATE_SUB(now(),INTERVAL ". $hour ." HOUR)
    ");
    return $result;
}
function select_feed_event_responses($id){
    global $emmadb;
    $result = $emmadb->query("
        SELECT r.*, r.user_id as userid, CONCAT(u.firstname ,' ', u.lastname) AS name, u.username, r.comments as response, ers.color, ers.name as statusname
        FROM emergency_responses r 
        JOIN users u ON r.user_id = u.id
        JOIN emergency_response_status ers ON r.status = ers.emergency_response_status_id
        WHERE r.emergency_id = '". $id ."'
        AND r.status != -1
    ");
    return $result;
}
function select_feed_mass_responses($id){
    global $emmadb;
    $result = $emmadb->query("
        SELECT r.*, r.created_by as userid, CONCAT(u.firstname ,' ', u.lastname) AS name, u.username, r.message as response
        FROM emma_mass_communication_responses r 
        JOIN users u ON r.created_by = u.id 
        WHERE r.mass_id = '". $id ."'
    ");
    return $result;
}
function select_feed_geofence_responses($id){
    global $emmadb;
    $result = $emmadb->query("
        SELECT r.*, r.created_by as userid, CONCAT(u.firstname ,' ', u.lastname) AS name, u.username, r.message as response
        FROM emma_geofence_message_responses r 
        JOIN users u ON r.created_by = u.id 
        WHERE r.geofence_message_id = '". $id ."'
    ");
    return $result;
}
function select_feed_sos_responses($id){
    global $emmadb;
    $result = $emmadb->query("
        SELECT r.*, r.created_by as userid, CONCAT(u.firstname ,' ', u.lastname) AS name, u.username, r.message as response
        FROM emma_sos_responses r 
        JOIN users u ON r.created_by = u.id 
        WHERE r.sos_id = '". $id ."'
    ");
    return $result;
}
function select_feed_groups($eventID, $type){
    global $emmadb;
    if($type == 'mass_communication') {
        $result = $emmadb->query("
            SELECT g.name, g.emma_group_id
            FROM communication_received_groups c
            JOIN emma_groups g ON c.emma_group_id = g.emma_group_id
            WHERE c.communication_id = '" . $eventID . "' 
        ");
    }elseif($type == 'event'){
        $result = $emmadb->query("
        SELECT g.name, g.emma_group_id
        FROM emergency_received_groups e 
        JOIN emma_groups g ON e.emma_group_id = g.emma_group_id
        WHERE e.emergency_id = '". $eventID ."'
    ");
    }
    return $result;
}
function select_get_alert_plans($id, $type){
    global $emmadb;
    switch ($type){
        case 'event':
            $result = $emmadb->query("
                SELECT p.name
                FROM emergencies e
                JOIN emma_plans p ON e.emma_plan_id = p.emma_plan_id
                WHERE e.emergency_id = '". $id ."'
            ");
            break;
        case 'mass_communication':
            $result = $emmadb->query("
                SELECT p.name
                FROM emma_mass_communications e
                JOIN emma_plans p ON e.emma_plan_id = p.emma_plan_id
                WHERE e.emma_mass_communication_id = '". $id ."'
            ");
            break;
        case 'sos_details':
            $result = $emmadb->query("
                SELECT p.name
                FROM emma_sos e
                JOIN emma_plans p ON e.plan_id = p.emma_plan_id
                WHERE e.emma_sos_id = '". $id ."'
            ");
            break;
        case 'geofence':
            $result = $emmadb->query("
                SELECT p.name
                FROM emma_geofence_messages e
                JOIN emma_plans p ON e.plan_id = p.emma_plan_id
                WHERE e.id = '". $id ."'
            ");
            break;
        case 'lockdown':
            $result = $emmadb->query("
                SELECT p.name
                FROM emma_lockdowns e
                JOIN emma_plans p ON e.plan_id = p.emma_plan_id
                WHERE e.lockdown_id = '". $id ."'
            ");
            break;
    }
    return $result;
}

function select_homeEvents_activeLives($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.emergency_id, et.name, e.drill
        FROM emergencies e
        JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
        WHERE e.emma_plan_id = '" . $planID . "'
        AND e.active = '1'
        AND e.drill = '0'
        AND e.parent_emergency_id IS NULL
    ");
    return $result;
}
function select_homeEvents_activeDrills($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.emergency_id, et.name, e.drill
        FROM emergencies e
        JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
        WHERE e.emma_plan_id = '" . $planID . "'
        AND e.active = '1'
        AND e.drill = '1'
        AND e.parent_emergency_id IS NULL
    ");
    return $result;
}
function select_homeEvents_securitiesCheck($planID){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_plans
        where emma_plan_id = '" . $planID . "'
        and securities = 1
    ");
    return $result;
}
function select_homeEvents_activeSecurities($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_securities s
        JOIN emma_security_types st on s.emma_security_type_id = st.emma_security_type_id
        WHERE s.emma_plan_id = '" . $planID . "'
        AND s.active = '1'
    ");
    return $result;
}
function select_map_events($eventID){
    global $emmadb;
    $result = $emmadb->query(sprintf("
        SELECT e.*, et.name AS type, et.img_filename AS type_filename, es.emma_site_id, es.emma_site_name, CONCAT(u.firstname, ' ', u.lastname) AS reported_by, u.id AS user_id, u.username AS user_email, u.phone AS user_phone, est.name AS sub_type_name, u.emma_pin AS user_pin, e.created_pin, ecl.call_datetime, CONCAT(ucall.firstname, ' ', ucall.lastname) as called_by, ecl.latitude as lat, ecl.longitude as lng 
        FROM emergencies as e
        JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
        JOIN emergency_sub_types est ON e.emergency_sub_type_id = est.emergency_sub_type_id
        LEFT JOIN emma_sites es ON e.emma_site_id = es.emma_site_id
        JOIN users u ON e.created_by_id = u.id
        LEFT JOIN emma_911_call_log ecl on e.emergency_id = ecl.emergency_id
        LEFT JOIN users ucall on ecl.userid = ucall.id
        WHERE e.emergency_id = %s
    ", $eventID));
    return $result;
}
function select_map_policeCalls($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT ecl.latitude, ecl.longitude, ecl.call_datetime, CONCAT(u.firstname, ' ', u.lastname) as user_name
        FROM emma_911_call_log as ecl
        JOIN users u on ecl.userid = u.id
        WHERE ecl.plan_id = '".$planID."'
        AND ecl.active = 1
    ");
    return $result;
}
function select_map_assets($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT eat.name, a.latitude as lat, a.longitude as lng, a.address, eai.image
        FROM emma_assets a
        LEFT JOIN emma_asset_types as eat on eat.emma_asset_type_id = a.emma_asset_type_id
        LEFT JOIN emma_asset_icons as eai on eat.emma_asset_icon_id = eai.asset_icon_id
        WHERE a.active = '1'
        AND a.emma_plan_id = '".$planID."'
        AND a.active = 1
        AND a.deleted = 0
    ");
    return $result;
}
function select_massComunication_events($eventID){
    global $emmadb;
    $result = $emmadb->query(sprintf("
        SELECT e.*, CONCAT(u.firstname, ' ', u.lastname) AS reported_by, u.id AS user_id, u.username AS user_email, u.phone AS user_phone, p.name
        FROM emma_mass_communications e
        JOIN users u ON e.created_by_id = u.id
        JOIN emma_plans p ON e.emma_plan_id = p.emma_plan_id
        WHERE emma_mass_communication_id = %s
    ", $eventID));
    return $result;
}
function select_massComunication_groups($eventID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.name, g.emma_group_id
        FROM communication_received_groups c
        JOIN emma_groups g ON c.emma_group_id = g.emma_group_id
        WHERE c.communication_id = '". $eventID ."' 
        ");
    return $result;
}
function select_massComunication_notifications($planID, $createdDate){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, CONCAT(u.firstname,' ',u.lastname) as name, u.username
        FROM emma_mass_communications e 
        JOIN users u ON e.created_by_id = u.id
        WHERE e.emma_plan_id = '". $planID ."'                           
        AND e.created_date > '". $createdDate ."' - INTERVAL 2 HOUR
        AND e.created_date < '". $createdDate ."' + INTERVAL 2 HOUR
    ");
    return $result;
}
function select_massComunication_notifications_feed($planID, $limit){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, CONCAT(u.firstname,' ',u.lastname) as name, u.username
        FROM emma_mass_communications e 
        JOIN users u ON e.created_by_id = u.id
        WHERE e.emma_plan_id = '". $planID ."'                           
        AND e.created_date BETWEEN timestamp(DATE_SUB(NOW(), INTERVAL ". $limit ." MINUTE)) AND timestamp(NOW())
    ");
    return $result;
}
function select_massnotificationScripts_scriptTypes(){
    global $emmadb;
    $result = $emmadb->query("
      select @pos:=@pos+1 as pos, emcbd.*
      from emma_mass_communication_script_types emcbd
      join (select @pos:=0) p
      ORDER BY emcbd.name
    ");
    return $result;
}
function select_massnotificationScripts_groups($planID){
    global $emmadb;
    $result = $emmadb->query("
        select @pos:=@pos+1 as pos, g.*
        from emma_groups g
        join (select @pos:=0) p
        where g.emma_plan_id = '" . $planID . "'
        order by g.name
    ");
    return $result;
}
function select_massnotificationScripts_scripts($planID, $scriptGroupID, $scriptTypeID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, b.name as typename
        FROM emma_scripts e
        JOIN emma_mass_communication_broadcast_types b ON e.emma_broadcast_type_id = b.emma_broadcast_id
        WHERE e.emma_script_group_id = '" . $scriptGroupID . "'
        AND e.emma_plan_id = '". $planID ."'
        AND e.emma_script_type_id = '". $scriptTypeID ."'
    ");
    return $result;
}
function select_massnotificationScripts_types(){
    global $emmadb;
    $result = $emmadb->query("
        SELECT t.emma_broadcast_id as emma_script_group_id, t.name
        FROM emma_mass_communication_broadcast_types as t 
        ORDER BY t.name
    ");
    return $result;
}
function select_massnotificationScripts_mcSriptTypes(){
    global $emmadb;
    $result = $emmadb->query("
        SELECT t.*
        FROM emma_mass_communication_script_types as t 
        ORDER BY t.name
    ");
    return $result;
}
function getNotificationScriptContent($planID){
    global $emmadb;
    $scripts = $emmadb->query("
        SELECT e.name, e.text, e.mass_notification_script_id
        FROM emma_mass_notification_scripts e 
        WHERE e.emma_plan_id = '". $planID ."'
        AND e.active = 1
        ORDER BY e.name
    ");
    return $scripts;
}
function getEmailScriptContent($planID){
    global $emmadb;
    $scripts = $emmadb->query("
        SELECT e.name, e.text, e.emma_email_script_id
        FROM emma_email_scripts e 
        WHERE e.plan_id = '". $planID ."'
        AND e.active = 1
        ORDER BY e.name
    ");
    return $scripts;
}
function getTextScriptContent($planID){
    global $emmadb;
    $scripts = $emmadb->query("
        SELECT e.name, e.text, e.emma_text_script_id
        FROM emma_text_scripts e 
        WHERE e.plan_id = '". $planID ."'
        AND e.active = 1
        ORDER BY e.name
    ");
    return $scripts;
}
function select_massnotificationScripts_scriptGroups($planID){
    global $emmadb;
    $result = $emmadb->query("
      select *
      from emma_groups
      where emma_plan_id = '". $planID ."'
      order by name
    ");
    return $result;
}
function select_plans_securityTypes(){
    global $emmadb;
    $result = $emmadb->query("
        select st.*
        from emma_security_types st
    ");
    return $result;
}
function select_plans_plandSecurityTypes($planID, $securityTypeID){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_plan_security_types
        where emma_plan_id = '" . $planID . "'
        and emma_security_type_id = '" . $securityTypeID . "'
    ");
    return $result;
}
function select_rawUsers_rawUsers($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT r.*, g.name
        FROM raw_users r 
        JOIN emma_groups g ON r.`group` = g.emma_group_id
        WHERE r.plan = '". $planID ."'
        AND r.active = 1
    ");
    return $result;
}
function select_rawUsers_students($guardianID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.student_name AS name, e.student_id
        FROM emma_raw_students_to_guardians e 
        WHERE e.guardian_id = '". $guardianID ."'
    ");
    return $result;
}
function select_resource_resources($planID, $resourceID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT r.*
        FROM emma_resources AS r
        WHERE r.emma_resource_id = " . $resourceID . "
        AND r.emma_plan_id = " . $planID . "
        AND r.active = 1
    ");
    return $result;
}
function select_resource_resourceTypes(){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_resource_types
        WHERE active = 1
        ORDER BY resource_name
    ");
    return $result;
}
function select_resource_sites($planID){
    global $emmadb;
    $result = $emmadb->query("
      SELECT e.*
      FROM emma_sites AS e
      WHERE e.emma_plan_id = " . $planID . "
    ");
    return $result;
}
function select_resourceType_Types($typeID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT t.resource_name AS name
        FROM emma_resource_types AS t 
        WHERE t.emma_resource_type_id = " . $typeID . "
    ");
    return $result;
}
function select_resourceType_resourceQuery($planID, $typeID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT r.*
        FROM emma_resources AS r
        WHERE r.emma_plan_id = " . $planID . "
        AND r.emma_resource_type_id = " . $typeID . "
        AND r.active = 1
        ORDER BY r.file_name
    ");
    return $result;
}
function select_resourceType_resourceTypes(){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_resource_types
        WHERE active = 1
    ");
    return $result;
}
function select_resourceType_sites($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*
        FROM emma_sites AS e
        WHERE e.emma_plan_id = " . $planID . "
    ");
    return $result;
}
function select_resources_resources(){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_resource_types
        WHERE active = 1
    ");
    return $result;
}
function select_resources_mostUsed($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT r.*
        FROM emma_resources AS r
        WHERE r.emma_plan_id = '" . $planID . "'
        AND r.active = 1
        ORDER BY view_count DESC
        LIMIT 6 
    ");
    return $result;
}
function select_resources_mostRecent($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT r.*
        FROM emma_resources AS r
        WHERE r.emma_plan_id = " . $planID . "
        AND r.active = 1
        ORDER BY r.created_date DESC
        LIMIT 6 
    ");
    return $result;
}
function select_resources_resourceTypes(){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_resource_types
        WHERE active = 1
        ORDER BY resource_name
    ");
    return $result;
}
function select_resources_sites($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*
        FROM emma_sites AS e
        WHERE e.emma_plan_id = " . $planID . "
    ");
    return $result;
}
function select_scripts_scriptTypes(){
    global $emmadb;
    $result = $emmadb->query("
      select @pos:=@pos+1 as pos, st.*
      from emma_script_types st
      join (select @pos:=0) p
      ORDER BY st.name
    ");
    return $result;
}
function select_scripts_groups($planID){
    global $emmadb;
    $result = $emmadb->query("
        select @pos:=@pos+1 as pos, g.*
        from emma_groups g
        join (select @pos:=0) p
        where g.emma_plan_id = '" . $planID . "'
        order by g.name
    ");
    return $result;
}
function select_scripts_scripts($scriptGroupID, $planID, $scriptTypeID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, b.name as typename
        FROM emma_scripts e
        JOIN emma_broadcast_types b ON e.emma_broadcast_type_id = b.emma_script_group_id
        WHERE e.emma_script_group_id = '" . $scriptGroupID . "'
        AND e.emma_plan_id = '". $planID ."'
        AND e.emma_script_type_id = '". $scriptTypeID ."'
    ");
    return $result;
}
function select_scripts_types(){
    global $emmadb;
    $result = $emmadb->query("
        SELECT t.*
        FROM emma_broadcast_types t 
        ORDER BY t.name
    ");
    return $result;
}
function select_scripts_emmaScriptTypes(){
    global $emmadb;
    $result = $emmadb->query("
        SELECT t.*
        FROM emma_script_types t 
        ORDER BY t.name
    ");
    return $result;
}
function select_scripts_scriptGroups($planID){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_groups
        where emma_plan_id = '". $planID ."'
        order by name
    ");
    return $result;
}
function select_securities_securityTypes($planID){
    global $emmadb;
    $result = $emmadb->query("
        select st.*
        from emma_security_types st
        join emma_plan_security_types pst on st.emma_security_type_id = pst.emma_security_type_id
        where pst.emma_plan_id = '" . $planID . "'
        order by st.name
    ");
    return $result;
}
function select_security_securities($securityID){
    global $emmadb;
    $result = $emmadb->query(sprintf("
        SELECT s.*, st.name AS type, es.emma_site_id, es.emma_site_name, CONCAT(u.firstname, ' ', u.lastname) AS reported_by, u.id AS user_id, u.username AS user_email, u.phone AS user_phone, st.img_filename as type_filename, p.name as emma_plan_name, es.*, u.emma_pin as user_pin, s.created_pin
        FROM emma_securities s
        JOIN emma_security_types st ON s.emma_security_type_id = st.emma_security_type_id
        JOIN emma_sites es ON s.emma_site_id = es.emma_site_id
        JOIN users u ON s.created_by_id = u.id
        JOIN emma_plans p on s.emma_plan_id = p.emma_plan_id
        WHERE emma_security_id = %s
    ", $securityID));
    return $result;
}
function select_security_callReports($securityID){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_call_reports
        where security_id = '" . $securityID . "'
    ");
    return $result;
}
function select_security_broadcastTypes(){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_broadcast_types
        where close = 1
        order by name
    ");
    return $result;
}
function select_security_broadcastTypesBroadcast(){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_broadcast_types
        where broadcast = 1
        order by name
    ");
    return $result;
}
function select_sendUserEmails_users($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.*
        FROM users u 
        JOIN emma_multi_plan m ON u.id = m.user_id
        WHERE m.plan_id = '". $planID ."'
        AND m.active = 1
    ");
    return $result;
}
function select_site_contacts($siteContactID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT c.first_name, c.last_name, c.email, c.phone
        FROM emma_site_contacts AS c
        WHERE c.emma_site_contact_id = ". $siteContactID ."
    ");
    return $result;
}
function select_sosDetails_soses($sosID){
    global $emmadb;
    $result = $emmadb->query(sprintf("
        SELECT s.*, CONCAT(u.firstname, ' ', u.lastname) as created_by_name, u.username as username, u.phone
        FROM emma_sos s
        JOIN users u on s.created_by_id = u.id
        WHERE s.emma_sos_id = %s
    ", $sosID));
    return $result;
}
function select_lockdown_details($lockdownID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT s.*, CONCAT(u.firstname, ' ', u.lastname) as created_by_name, u.username as username, u.phone, p.name
        FROM emma_lockdowns s
        JOIN users u on s.created_by_id = u.id
        JOIN emma_plans p ON s.plan_id = p.emma_plan_id
        WHERE s.lockdown_id = '". $lockdownID ."'
        ");
    return $result;
}
function select_modified_lockdown_details($lockdownID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT s.*, CONCAT(u.firstname, ' ', u.lastname) as created_by_name, u.username as username, u.phone, p.name
        FROM emma_modified_lockdowns s
        JOIN users u on s.created_by_id = u.id
        JOIN emma_plans p ON s.plan_id = p.emma_plan_id
        WHERE s.m_lockdown_id = '". $lockdownID ."'
        ");
    return $result;
}
function select_sosDetails_callReports($sosID){
    global $emmadb;
    $result = $emmadb->query("
        select *
        from emma_sos_call_reports
        where emma_sos_id = '".$sosID."'
    ");
    return $result;
}
function select_students_rawUsers($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT r.*, CONCAT(r.firstname, ' ', r.lastname) AS name
        FROM users r 
        WHERE r.emma_plan_id = '". $planID ."'
        AND r.student = 1
    ");
    return $result;
}
function select_students_parents($studentID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT CONCAT(u.firstname, ' ', u.lastname) AS name
        FROM emma_students_to_guardians e 
        LEFT JOIN users u ON e.guardian_id = u.id
        WHERE e.student_id = '". $studentID ."'
    ");
    return $result;
}
function select_user_drills($userID){
    global $emmadb;
    $result = $emmadb->query(sprintf("
        SELECT COUNT(*) AS count
        FROM emergencies AS e
        JOIN emergency_types AS t ON e.emergency_type_id = t.emergency_type_id
        WHERE created_by_id = %s
        AND e.drill = 1
    ", $userID));
    return $result;
}
function select_user_emergencies($userID){
    global $emmadb;
    $result = $emmadb->query(sprintf("
        SELECT COUNT(*) AS count
        FROM emergencies AS e
        JOIN emergency_types AS t ON e.emergency_type_id = t.emergency_type_id
        WHERE created_by_id = %s
        AND e.drill = 0
    ", $userID));
    return $result;
}
function select_user_drillResponses($userID){
    global $emmadb;
    $result = $emmadb->query(sprintf("
        SELECT count(*) AS count
        FROM emergency_responses AS r
        JOIN emergencies AS e ON r.emergency_id = e.emergency_id
        JOIN emergency_types AS t ON e.emergency_type_id = t.emergency_type_id
        WHERE created_by_id = %s
        AND e.drill = 1
    ", $userID));
    return $result;
}
function select_user_emergencyResponses($userID){
    global $emmadb;
    $result = $emmadb->query(sprintf("
        SELECT count(*) AS count
        FROM emergency_responses AS r
        JOIN emergencies AS e ON r.emergency_id = e.emergency_id
        JOIN emergency_types AS t ON e.emergency_type_id = t.emergency_type_id
        WHERE created_by_id = %s
        AND e.drill = 0
    ", $userID));
    return $result;
}
function select_userhome_selectPlans($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT mp.plan_id, p.name
        FROM emma_plans p
        LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id
        WHERE mp.user_id = '" . $userID . "'
        AND p.date_expired > NOW()
        AND mp.active = 1
    ");
    return $result;
}
function select_userhome_events($planArray, $groupArray){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, et.name AS type, et.badge, et.img_filename, es.emma_site_name, es.emma_site_notes, est.name as sub_name
        FROM emergencies e
        JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
        JOIN emergency_sub_types est on est.emergency_sub_type_id = e.emergency_sub_type_id
        JOIN emma_sites es ON es.emma_site_id = e.emma_site_id
        JOIN emergency_received_groups g ON e.emergency_id = g.emergency_id
        WHERE e.active = '1'
        AND e.parent_emergency_id IS NULL 
        AND e.emma_plan_id IN (".implode(',',$planArray).")
        AND g.emma_group_id IN (".implode(',',$groupArray).")
        GROUP BY e.emergency_id 
    ");
    return $result;
}
function select_userhome_policeCalls($planArray){
    global $emmadb;
    $result = $emmadb->query("
        SELECT ecl.latitude, ecl.longitude, ecl.call_datetime, CONCAT(u.firstname, ' ', u.lastname) as user_name
        FROM emma_911_call_log as ecl
        JOIN users u on ecl.userid = u.id
        WHERE ecl.plan_id IN (".implode(',',$planArray).")
        AND ecl.active = 1
    ");
    return $result;
}
function select_confirm_user_password($userID, $password){
    global $emmadb;
    $hashedPassword = hash('sha512', $password);
    $result = $emmadb->query("
        SELECT u.*
        FROM users u 
        WHERE u.id = '". $userID ."'
        AND u.big_password = '". $hashedPassword ."'
        AND u.password = PASSWORD('". $password ."')
    ");
    return $result;
}
function select_userhome_assets($planArray){
    global $emmadb;
    $result = $emmadb->query("
        SELECT eat.name, a.latitude as lat, a.longitude as lng, a.address, eai.image
        FROM emma_assets a
        LEFT JOIN emma_asset_types as eat on eat.emma_asset_type_id = a.emma_asset_type_id
        LEFT JOIN emma_asset_icons as eai on eat.emma_asset_icon_id = eai.asset_icon_id
        WHERE a.active = '1'
        AND a.emma_plan_id IN (".implode(',',$planArray).")
        AND a.active = 1
        AND a.deleted = 0
    ");
    return $result;
}
function select_userhome_activeLives($planArray){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.emergency_id, et.name, e.drill, ep.name as plan_name, ep.emma_plan_id
        FROM emergencies e
        JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
        LEFT JOIN emma_plans as ep on e.emma_plan_id = ep.emma_plan_id
        WHERE e.emma_plan_id IN (".implode(',',$planArray).")
        AND e.active = '1'
        AND e.drill = '0'
        AND e.parent_emergency_id IS NULL
    ");
    return $result;
}
function select_userhome_populateEvents($planArray){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, et.name AS type, et.img_filename
        FROM emergencies e
        JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
        WHERE e.active = '1'
        AND e.emma_plan_id IN (".implode(',',$planArray).")
    ");
    return $result;
}
function select_verifyEmail_confirmQuery($userID, $hash){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.id, u.username
        FROM users AS u
        WHERE u.id = " . $userID . "
        AND password = '" . $hash . "'
    ");
    return $result;
}
function select_dashboard_planEventTypes($userID, $planID){
    global $emmadb;
    $result = $emmadb->query("
        select et.*, pet.disabled
        from emma_user_groups ug
        join emma_groups g on ug.emma_group_id = g.emma_group_id
        join emma_group_events pet on g.emma_group_id = pet.emma_group_id
        join emergency_types et on pet.event_id = et.emergency_type_id
        join emma_plans AS p ON p.emma_plan_id = g.emma_plan_id
        where ug.user_id = '" . $userID . "'
        AND p.emma_plan_id = '".$planID."'
        and pet.active = 1
        GROUP BY et.emergency_type_id
    ");
    return $result;
}
function select_dashboard_planInfos($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*
        FROM emma_plans e
        WHERE e.emma_plan_id = '". $planID ."'
        ORDER BY name
    ");
    return $result;
}
function select_dashboard_plans($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT p.name, p.emma_plan_id
        FROM emma_multi_plan m
        LEFT JOIN emma_plans p ON m.plan_id = p.emma_plan_id
        WHERE m.user_id = '". $userID ."'
        AND m.active = 1
    ");
    return $result;
}
function select_dashboard_selectedPlans($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT mp.plan_id, p.name, eug.emma_group_id
        FROM emma_plans p
        LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id
        LEFT JOIN emma_user_groups as eug on eug.user_id = '" . $userID . "'
        WHERE mp.user_id = '" . $userID . "'
        AND p.date_expired > NOW()
        AND mp.active = 1
    ");
    return $result;
}
function select_dashboard_notificationScripts($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT `name`, `text`
        FROM emma_mass_notification_scripts
        WHERE emma_plan_id = '". $planID ."'
        AND active = 1
    ");
    return $result;
}
function select_dashboard_emailScripts($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT name, script
        FROM emma_email_scripts
        WHERE plan_id = '". $planID ."'
        AND active = 1
    ");
    return $result;
}




//-------------------------------------------------------------------------------------------------------------------------------------------------

function select_securites_for_report($planID){
    global $emmadb;
    $result = $emmadb->query("
        select s.*, site.emma_site_name, st.name as security_type, CONCAT(c.firstname, ' ', c.lastname) as creator_name, CONCAT(u.firstname, ' ', u.lastname) as closer_name
        from emma_securities s
        join emma_sites site on s.emma_site_id = site.emma_site_id
        join emma_security_types st on s.emma_security_type_id = st.emma_security_type_id
        join users c on s.created_by_id = c.id
        join users u on s.closed_by_id = u.id
        where s.emma_plan_id = '" . $planID . "'
    ");
    return $result;
}
function select_GetActiveSOS_odd($planID, $orderString, $limitLength, $offsetStart){
    global $emmadb;
    $result = $emmadb->query("
        SELECT SQL_CALC_FOUND_ROWS s.emma_sos_id, s.help_date, CONCAT(u.firstname, ' ', u.lastname) AS user, u.phone, u.landline_phone, s.help_lat as latitude, s.help_lng as longitude, u.id as user_id, u.username as email
        FROM emma_sos s
        JOIN users u ON s.created_by_id = u.id AND u.emma_plan_id = '".$planID."'
        WHERE s.help_date IS NOT NULL
        AND s.closed_date IS NULL
        AND s.cancelled_date is NULL
        ORDER BY ". $orderString .", created_date desc 
        LIMIT " . $limitLength . " OFFSET " . $offsetStart . "
    ");
    return $result;
}
function select_FOUND_ROWS(){
    global $emmadb;
    $result = $emmadb->query("
        SELECT FOUND_ROWS()
    ");
    return $result;
}
//-------------------------------------------------------------------------------------------------------------------------------------------------


function select_assets($asset_id,$type_id){
    global $emmadb;
    $result = $emmadb->query("
    SELECT eatd.*, eado.name as dropdown_name, eado.id as dropdown_id, eaci.info, eaci.date_time, eaci.id as custominfoid, eaci.dropdown as selecteddropdown
    FROM emma_asset_type_display as eatd
    LEFT JOIN emma_assets_dropdown_options as eado ON eatd.dropdown_id = eado.id AND eado.display = 1
    LEFT JOIN emma_assets_custom_info as eaci on eaci.emma_asset_id = '".$asset_id."'
    WHERE eatd.asset_type_id = '".$type_id."'    
    AND eatd.active = 1
    ");
    return $result;
}
function select_assetsdropdowns($asset_id){
    global $emmadb;
    $result = $emmadb->query("
    SELECT eaci.id,eaci.dropdown
    FROM emma_assets_custom_info as eaci
    WHERE eaci.emma_asset_id = '".$asset_id."'
    AND eaci.dropdown IS NOT NULL
    ");
    return $result;
}
function select_assetstypes($type_id){
    global $emmadb;
    $result = $emmadb->query("
    SELECT eai.image
    FROM emma_asset_types as eat
    LEFT JOIN emma_asset_icons as eai on eat.emma_asset_icon_id = eai.asset_icon_id
    WHERE eat.emma_asset_type_id = '".$type_id."'
    ");
    return $result;
}
function select_assetstypedisplay($type_id){
    global $emmadb;
    $result = $emmadb->query("
    SELECT eatd.*, eado.name as dropdown_name, eado.id as dropdown_id
    FROM emma_asset_type_display as eatd
    LEFT JOIN emma_assets_dropdown_options as eado ON eatd.dropdown_id = eado.id AND eado.display = 1
    WHERE eatd.asset_type_id = '".$type_id."'
    AND eatd.active = 1
    ");
    return $result;
}
function select_getAssets_assets($active, $emmaPlanId, $orderString, $length, $start, $search0, $search1, $search2, $search3, $search4, $search5){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS a.*, at.name as asset_type_name, concat(u.firstname, ' ', u.lastname) as user_name, u.id as userid, u.username as useremail
    FROM emma_assets a
    JOIN emma_asset_types at ON a.emma_asset_type_id = at.emma_asset_type_id
    JOIN users u ON a.created_by_id = u.id    
    WHERE (" . ($active != '' ? "a.active = " . $active : "1") . ")
    AND a.deleted = 0
    AND a.emma_plan_id = '".$emmaPlanId."'
    AND (" . ($search0 != "" ? "at.name like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "concat(u.firstname, ' ', u.lastname) like ('%" . $search1 . "%')" : "1") . ")
    AND (" . ($search2 != "" ? "a.created_date like ('%" . $search2 . "%')" : "1") . ")
    AND (" . ($search3 != "" ? "a.latitude like ('%" . $search3 . "%')" : "1") . ")
    AND (" . ($search4 != "" ? "a.longitude like ('%" . $search4 . "%')" : "1") . ")
    AND (" . ($search5 != "" ? "(a.active = '0' AND 'Inactive' like ('%" . $search5 . "%')) OR (a.active = '1' AND 'Current' like ('%" . $search5 . "%'))" : "1") . ")
    ORDER BY ". $orderString .", created_date desc 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getAssetsEdit_assets($emmaPlanId,$orderString,$length,$start,$search0){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS a.*, eac.image
    FROM emma_asset_types a
    LEFT JOIN emma_asset_icons eac on a.emma_asset_icon_id = eac.asset_icon_id
    WHERE a.emma_plan_id = '".$emmaPlanId."'    
    AND a.active = 1
    AND (" . ($search0 != "" ? "a.name like ('%" . $search0 . "%')" : "1") . ")
    ORDER BY ". $orderString .", name desc
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getEmergencies_emergencies($active){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.emergency_id, et.name AS type, CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') AS user, e.created_date
        FROM emergencies e
        JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
        JOIN users u ON e.created_by_id = u.id
        WHERE (" . ($active != '' ? "e.active = " . $active : "1") . ")
        ORDER BY e.created_date DESC
    ");
    return $result;
}
function select_getAssetsmap_assets($emmaPlanId){
    global $emmadb;
    $result = $emmadb->query("
    SELECT e.*, t.name, i.image
    FROM emma_assets e
    JOIN emma_asset_types t ON e.emma_asset_type_id = t.emma_asset_type_id
    LEFT JOIN emma_asset_icons i ON t.emma_asset_icon_id = i.asset_icon_id
    JOIN users u ON e.created_by_id = u.id
    WHERE e.emma_plan_id = '".$emmaPlanId."'
");
    return $result;
}
function select_getcancelledSOS_events($emmaPlanId,$orderString,$length,$start){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS s.emma_sos_id, s.cancelled_date, CONCAT(u.firstname, ' ', u.lastname) AS user, s.cancelled_lat as latitude, s.cancelled_lng as longitude, u.id as user_id, u.phone, u.username as email
    FROM emma_sos s
    JOIN users u ON s.created_by_id = u.id AND u.emma_plan_id = '".$emmaPlanId."'
    AND s.cancelled_date IS NOT NULL
    ORDER BY ". $orderString .", created_date desc 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getclosedSOS_events($emmaPlanId,$orderString,$length,$start){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS s.emma_sos_id, s.closed_date, CONCAT(u.firstname, ' ', u.lastname) AS user, s.closed_lat as latitude, s.closed_lng as longitude, u.id as user_id, s.closed_comments, '0' as closed_call_report, u.phone, u.username as email
    FROM emma_sos s
    JOIN users u ON s.created_by_id = u.id AND u.emma_plan_id = '".$emmaPlanId."'
    WHERE s.closed_date IS NOT NULL
    AND s.help_date IS NOT NULL
    AND s.pending_date IS NOT NULL
    AND s.cancelled_date IS NULL
    ORDER BY ". $orderString .", created_date desc 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getevents_events($emmaPlanId,$orderString,$length,$start, $drill, $active, $search0, $search1, $search2, $search3){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS e.emergency_id, et.name AS type, CONCAT(u.firstname, ' ', u.lastname) AS userName, e.active, e.created_date, u.id as userid, u.username as useremail
    FROM emergencies e
    JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
    JOIN users u ON e.created_by_id = u.id
    WHERE (" . ($active != '' ? "e.active = " . $active : "1") . ")
    AND e.parent_emergency_id IS NULL
    AND (" . (isset($drill) && $drill != "" ? "e.drill = '" . $drill . "'" : "1") . ")
    AND (" . ($emmaPlanId != '' ? "e.emma_plan_id = " . $emmaPlanId : "1") . ")
    AND (" . ($search0 != "" ? "et.name like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') like ('%" . $search1 . "%')" : "1") . ")
    AND (" . ($search2 != "" ? "(e.active = '0' AND 'Closed' like ('%" . $search2 . "%')) OR (e.active = '1' AND 'Active' like ('%" . $search2 . "%'))" : "1") . ")
    AND (" . ($search3 != "" ? "e.created_date like ('%" . $search3 . "%')" : "1") . ")
    ORDER BY ". $orderString .", created_date desc 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getevents_lockdowns($emmaPlanId,$orderString,$length,$start,$search0, $search1){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS e.lockdown_id, CONCAT(u.firstname, ' ', u.lastname) AS userName, e.created_date, u.id as userid, u.username as useremail
    FROM emma_lockdowns e 
    JOIN users u ON e.created_by_id = u.id
    WHERE (" . ($emmaPlanId != '' ? "e.plan_id = " . $emmaPlanId : "1") . ")
    AND (" . ($search0 != "" ? "e.created_date like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') like ('%" . $search1 . "%')" : "1") . ")
    AND e.drill = 0
    ORDER BY ". $orderString .", created_date desc 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getevents_modified_lockdowns($emmaPlanId,$orderString,$length,$start,$search0, $search1){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS e.m_lockdown_id, CONCAT(u.firstname, ' ', u.lastname) AS userName, e.created_date, u.id as userid, u.username as useremail
    FROM emma_modified_lockdowns e 
    JOIN users u ON e.created_by_id = u.id
    WHERE (" . ($emmaPlanId != '' ? "e.plan_id = " . $emmaPlanId : "1") . ")
    AND (" . ($search0 != "" ? "e.created_date like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') like ('%" . $search1 . "%')" : "1") . ")
    ORDER BY ". $orderString .", created_date desc 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getevents_lockdown_drills($emmaPlanId,$orderString,$length,$start,$search0, $search1){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS e.lockdown_id, CONCAT(u.firstname, ' ', u.lastname) AS userName, e.created_date, u.id as userid, u.username as useremail
    FROM emma_lockdowns e 
    JOIN users u ON e.created_by_id = u.id
    WHERE (" . ($emmaPlanId != '' ? "e.plan_id = " . $emmaPlanId : "1") . ")
    AND (" . ($search0 != "" ? "e.created_date like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') like ('%" . $search1 . "%')" : "1") . ")
    AND e.drill = 1
    ORDER BY ". $orderString .", created_date desc 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getevents_activear($emmaPlanId,$orderString,$length,$start,$search0, $search1){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS e.emma_ar_id, e.description, e.created_date
    FROM emma_ar e 
    WHERE (" . ($emmaPlanId != '' ? "e.plan_id = " . $emmaPlanId : "1") . ")
    AND (" . ($search0 != "" ? "e.created_date like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "e.description like ('%" . $search1 . "%')" : "1") . ")
    AND e.closed = 0
    ORDER BY ". $orderString .", created_date desc 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getevents_activearm($emmaPlanId,$orderString,$length,$start,$search0, $search1){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS e.emma_moss_id, e.description, e.created_date
    FROM emma_moss e 
    WHERE (" . ($emmaPlanId != '' ? "e.plan_id = " . $emmaPlanId : "1") . ")
    AND (" . ($search0 != "" ? "e.created_date like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "e.description like ('%" . $search1 . "%')" : "1") . ")
    AND e.closed = 0
    ORDER BY ". $orderString .", created_date desc 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getevents_closedar($emmaPlanId,$orderString,$length,$start,$search0, $search1){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS e.emma_ar_id, e.description, e.closed_date
    FROM emma_ar e 
    WHERE (" . ($emmaPlanId != '' ? "e.plan_id = " . $emmaPlanId : "1") . ")
    AND (" . ($search0 != "" ? "e.closed_date like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "e.description like ('%" . $search1 . "%')" : "1") . ")
    AND e.closed = 1
    ORDER BY ". $orderString .", closed_date desc 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getevents_closedarm($emmaPlanId,$orderString,$length,$start,$search0, $search1){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS e.emma_moss_id, e.description, e.closed_date
    FROM emma_moss e 
    WHERE (" . ($emmaPlanId != '' ? "e.plan_id = " . $emmaPlanId : "1") . ")
    AND (" . ($search0 != "" ? "e.closed_date like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "e.description like ('%" . $search1 . "%')" : "1") . ")
    AND e.closed = 1
    ORDER BY ". $orderString .", closed_date desc 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}

function select_getgeofences_geofences($emmaPlanId,$orderString,$length,$start){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS gf.*
    FROM emma_geofence_locations gf
    WHERE gf.plan_id = '" . $emmaPlanId . "'
    AND gf.main_fence = '0'
    GROUP BY gf.id
    ORDER BY ". $orderString ."
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getgeofence_children($geoId){
    global $emmadb;
    $result = $emmadb->query("
        SELECT gf.*
        FROM emma_geofence_locations gf
        WHERE gf.main_fence = '". $geoId ."'
    ");
    return $result;
}
function select_getgroupscript_broadcastscripts($emmaPlanId,$scriptType,$type,$group){
    global $emmadb;
    $result = $emmadb->query("
select e.*
from emma_scripts e
WHERE e.emma_plan_id = '" . $emmaPlanId . "'
AND e.emma_script_type_id = '". $scriptType ."'
AND e.emma_broadcast_type_id = '". $type ."'
AND e.emma_script_group_id = '". $group ."'
order by name
");
    return $result;
}
function select_getgroups_groups($emmaPlanId,$orderString,$length,$start){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS eg.name, eg.emma_group_id, eg.info_only, eg.admin, eg.security, eg.emma_sos, eg.`911admin`
    FROM emma_groups AS eg
    WHERE eg.emma_plan_id = " . $emmaPlanId . "
    GROUP BY eg.emma_group_id
    ORDER BY ". $orderString ." 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getgroup_edited($emmaPlanId){
    global $emmadb;
    $result = $emmadb->query("
    SELECT eg.*, u.firstname, u.lastname, hg.edit_date, u.username
    FROM emma_edit_history_groups hg
    JOIN emma_groups AS eg ON hg.emma_group_id = eg.emma_group_id
    JOIN users u ON hg.edit_by_id = u.id
    WHERE eg.emma_plan_id = " . $emmaPlanId . " 
    AND hg.edit_date > DATE_SUB(NOW(), INTERVAL 1 DAY)
");
    return $result;
}
function select_getsite_edited($emmaPlanId){
    global $emmadb;
    $result = $emmadb->query("
    SELECT u.firstname, u.lastname, hg.edit_date, u.username, s.*
    FROM emma_edit_history_sites hg
    JOIN emma_sites s ON hg.emma_site_id = s.emma_site_id
    JOIN users u ON hg.edit_by_id = u.id
    WHERE s.emma_plan_id = " . $emmaPlanId . " 
    AND hg.edit_date > DATE_SUB(NOW(), INTERVAL 1 DAY)
");
    return $result;
}
function select_getgroups_inactiveusers($groupID){
    global $emmadb;
    $result = $emmadb->query("
    select count(*) as number
    from emma_user_groups ug
    join users u on ug.user_id = u.id
    where ug.emma_group_id = '".$groupID."'
    and u.active = 0
  ");
    return $result;
}
function select_getgroups_activeusers($groupID){
    global $emmadb;
    $result = $emmadb->query("
    select count(*) as number
    from emma_user_groups ug
    join users u on ug.user_id = u.id
    where ug.emma_group_id = '".$groupID."'
    and u.active = 1
  ");
    return $result;
}
function select_getguestcodes_guestcodes($emmaPlanId,$length,$start, $search0, $search1, $search2, $search3){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS gc.guest_code_id, gc.start_date, gc.end_date , gc.duration_minutes, group_concat(DISTINCT eg.name ORDER BY eg.name) AS `group`
    FROM emma_guest_codes gc
    LEFT JOIN emma_groups eg ON gc.group_id = eg.emma_group_id
    WHERE gc.emma_plan_id = '" . $emmaPlanId . "'
    AND (" . ($search0 != "" ? "gc.guest_code_id like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "gc.start_date like ('%" . $search1 . "%')" : "1") . ")
    AND (" . ($search2 != "" ? "gc.duration like ('%" . $search2 . "%')" : "1") . ")
    AND (" . ($search3 != "" ? "eg.name like ('" . $search3 . "%')" : "1") . ")
    GROUP BY gc.guest_code_id
    ORDER BY gc.start_date, gc.guest_code_id
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getguestusers_users($emmaPlanId,$length,$start, $search0, $search1, $search2, $search3){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS u.username, u.id, u.firstname, u.lastname, group_concat(DISTINCT eg.name ORDER BY eg.name) AS `group`, emp.active as emp_active, emp.guest_code,emp.expires
    FROM users u
    LEFT JOIN emma_user_groups AS gb ON u.id = gb.user_id
    LEFT JOIN emma_groups eg ON gb.emma_group_id = eg.emma_group_id
    LEFT JOIN emma_multi_plan emp on u.id = emp.user_id AND emp.active = 1
    WHERE u.display = 'yes'
    AND u.temporary_code IS NOT NULL
    AND u.active = 1
    AND emp.plan_id = " . $emmaPlanId . "
    AND (" . ($search0 != "" ? "u.username like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "u.firstname like ('%" . $search1 . "%')" : "1") . ")
    AND (" . ($search2 != "" ? "u.lastname like ('%" . $search2 . "%')" : "1") . ")
    AND (" . ($search3 != "" ? "eg.name like ('" . $search3 . "%')" : "1") . ")
    GROUP BY u.id
    ORDER BY u.username, u.firstname, u.lastname
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getsetupusers_users($emmaPlanId,$length,$start){
    global $emmadb;
    $result = $emmadb->query("
        select SQL_CALC_FOUND_ROWS u.id, u.username, u.firstname, u.lastname, p.name
        from users u
        JOIN emma_multi_plan mp ON u.emma_plan_id = mp.plan_id
        JOIN emma_plans AS p ON mp.plan_id = p.emma_plan_id
        WHERE u.id NOT IN(
          SELECT ug.user_id
          FROM emma_groups g
          JOIN emma_user_groups ug ON g.emma_group_id = ug.emma_group_id
          JOIN emma_plans AS ep on g.emma_plan_id = ep.emma_plan_id
          WHERE ep.emma_plan_id = '".$emmaPlanId."'
        )        
        AND mp.plan_id = '". $emmaPlanId ."' 
        AND mp.active = 1
        ORDER BY u.username 
        LIMIT " . $length . " OFFSET " . $start . "
    ");
    return $result;
}
function select_getUnassignedUsers($emmaPlanId,$orderString,$length,$start, $search0, $search1, $search2){



}

function select_getallsetupusers_users(){
    global $emmadb;
    $result = $emmadb->query("
        select SQL_CALC_FOUND_ROWS u.id, u.username, u.firstname, u.lastname, p.name
        from users u
        JOIN emma_plans p ON u.emma_plan_id = p.emma_plan_id
        WHERE u.id NOT IN(
          SELECT ug.user_id
          FROM emma_groups g
          JOIN emma_user_groups ug ON g.emma_group_id = ug.emma_group_id
        )
        ORDER BY u.username
    ");
    return $result;
}

function select_getsetupusersnegative_users($emmaPlanId,$orderString,$length,$start, $search0, $search1, $search2){
    global $emmadb;
    $result = $emmadb->query("
        SELECT SQL_CALC_FOUND_ROWS u.username, u.id, u.firstname, u.lastname
        FROM users u
          JOIN emma_multi_plan mp ON u.id = mp.user_id
        WHERE u.display = 'yes'
        AND mp.plan_id = '". $emmaPlanId ."'
        AND mp.active = 1
    ");
    return $result;
}
function select_getreceived($id,$type){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.username, u.id, u.firstname, u.lastname, CONCAT(u.firstname, ' ', u.lastname) AS fullname, a.date_received
        FROM emma_alert_received a
          JOIN users u ON a.user_id = u.id
        WHERE a.alert_id = '". $id ."'
        AND a.alert_type = '". $type ."'
        ORDER BY a.date_received
    ");
    return $result;
}
function select_gethelpsos_events($emmaPlanId,$length,$start, $orderString){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS s.emma_sos_id, s.created_date, CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') AS user, s.help_lat, s.help_lng, u.username as email
    FROM emma_sos s
    JOIN users u ON s.created_by_id = u.id AND u.emma_plan_id = '".$emmaPlanId."'
    WHERE s.sos_date IS NOT NULL
    AND s.closed_date IS NULL
    ORDER BY ". $orderString .", created_date desc 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_inactiveguestusers_users($emmaPlanId,$length,$start, $search0, $search1, $search2){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS u.username, u.id, u.firstname, u.lastname, group_concat(DISTINCT eg.name ORDER BY eg.name) AS `group`
    FROM users u
    LEFT JOIN emma_user_groups AS gb ON u.id = gb.user_id
    LEFT JOIN emma_groups eg ON gb.emma_group_id = eg.emma_group_id
    WHERE u.display = 'no'
    AND u.temporary_code IS NOT NULL
    AND u.active = 1
    AND u.emma_plan_id = " . $emmaPlanId . "
    AND (" . ($search0 != "" ? "u.username like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "u.firstname like ('%" . $search1 . "%')" : "1") . ")
    AND (" . ($search2 != "" ? "u.lastname like ('%" . $search2 . "%')" : "1") . ")
    GROUP BY u.id
    ORDER BY u.username, u.firstname, u.lastname
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_inactiveusers_users($emmaPlanId,$length,$start, $search0, $search1, $search2){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS DISTINCT u.username, u.id, u.firstname, u.lastname, group_concat(DISTINCT eg.name ORDER BY eg.name) AS `group`
    FROM users u
             LEFT JOIN emma_user_groups AS gb ON u.id = gb.user_id
             LEFT JOIN emma_groups eg ON gb.emma_group_id = eg.emma_group_id
             JOIN emma_multi_plan as mp ON u.id = mp.user_id
    WHERE mp.active = 0
        AND (mp.guest_code IS NULL OR mp.guest_code = '')
      AND mp.plan_id NOT IN (
            SELECT amp.plan_id
            FROM emma_multi_plan as amp
            WHERE amp.active = 1
            AND u.id = amp.user_id
            AND (amp.guest_code IS NULL OR amp.guest_code = '')
            AND amp.plan_id = ".$emmaPlanId."
        )
      AND u.temporary_code IS NULL
      AND u.active = 1
      AND mp.plan_id = ".$emmaPlanId."
    AND (" . ($search0 != "" ? "u.username like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "u.firstname like ('%" . $search1 . "%')" : "1") . ")
    AND (" . ($search2 != "" ? "u.lastname like ('%" . $search2 . "%')" : "1") . ")
    GROUP BY u.id
    ORDER BY u.username, u.firstname, u.lastname
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getmasscommunications_events($emmaPlanId,$length,$start, $orderString, $search0, $search1, $search2){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS mc.created_date, CONCAT(u.firstname, ' ', u.lastname) AS creator, mc.notification, mc.created_by_id, u.username as useremail, mc.emma_mass_communication_id
    FROM emma_mass_communications mc
    JOIN users u ON mc.created_by_id = u.id
    WHERE (" . ($emmaPlanId != '' ? "mc.emma_plan_id = " . $emmaPlanId : "1") . ")
    AND (" . ($search0 != "" ? "mc.created_date like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') like ('%" . $search1 . "%')" : "1") . ")
    AND (" . ($search2 != "" ? "mc.notification like ('%" . $search2 . "%')" : "1") . ")
    ORDER BY ". $orderString .", created_date desc 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getpdffile_resources($userID ,$resource_id){
    global $emmadb;
    $result = $emmadb->query("
    SELECT r.*
    FROM emma_resources AS r
    WHERE r.emma_resource_id = ". $resource_id ."
    AND r.emma_plan_id = ". $userID ."
    AND r.active = 1
");
    return $result;
}
function select_pendingsos_events($emmaPlanId ,$orderString, $length, $start){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS s.emma_sos_id, s.pending_date, CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') AS user, u.phone, u.landline_phone, s.pending_lat as latitude, s.pending_lng as longitude, u.id as user_id, u.username as email
    FROM emma_sos s
    JOIN users u ON s.created_by_id = u.id AND u.emma_plan_id = '".$emmaPlanId."'
    WHERE s.pending_date IS NOT NULL
    AND s.help_date IS NULL
    AND s.cancelled_date IS NULL
    AND s.closed_date IS NULL
    ORDER BY ". $orderString .", created_date desc 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_allsos_events($emmaPlanId ,$orderString, $length, $start){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS s.emma_sos_id, s.pending_date, CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') AS user, u.phone, u.landline_phone, s.pending_lat as latitude, s.pending_lng as longitude, u.id as user_id, u.username as email, s.help_date, s.cancelled_date, s.closed_date, s.created_date
    FROM emma_sos s
    JOIN users u ON s.created_by_id = u.id AND u.emma_plan_id = '".$emmaPlanId."'
    WHERE s.pending_date IS NOT NULL
    ORDER BY ". $orderString .", created_date desc 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_planstatistics_eventMonths($emmaPlanId){
    global $emmadb;
    $result = $emmadb->query("
    SELECT year(e.created_date) AS year, month(e.created_date) AS month, count(*) AS count, sum(CASE WHEN e.drill = 1 THEN 1 ELSE 0 END) AS drill_count, sum(CASE WHEN e.drill = 0 THEN 1 ELSE 0 END) AS event_count
    FROM emergencies e
    JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
    WHERE e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
    GROUP BY year(e.created_date), month(e.created_date)
");
    return $result;
}
function select_planstatistics_countsPcVsApp($emmaPlanId){
    global $emmadb;
    $result = $emmadb->query("
    SELECT (CASE er.device_type WHEN 0 THEN 'App' ELSE 'PC' END) AS type, count(*) AS count
    FROM emergency_responses er
    JOIN users u ON er.user_id = u.id WHERE u.emma_plan_id IN (". implode(',',$emmaPlanId) .")
    GROUP BY er.device_type;
");
    return $result;
}
function select_planstatistics_events($emmaPlanId){
    global $emmadb;
    $result = $emmadb->query("
    select e.emergency_id, e.drill, et.name, e.created_date, u.username, e.description
    from emergencies e
    join users u on e.created_by_id = u.id
    join emergency_types et on e.emergency_type_id = et.emergency_type_id
    where e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
");
    return $result;
}
function select_planstatistics_eventTypeCounts($emmaPlanId){
    global $emmadb;
    $result = $emmadb->query("
    SELECT et.name, count(*) AS count
    FROM emergencies e
    JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id AND e.drill = 0
    WHERE e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
    GROUP BY e.emergency_type_id
");
    return $result;
}
function select_planstatistics_drillTypeCounts($emmaPlanId){
    global $emmadb;
    $result = $emmadb->query("
    SELECT et.name, count(*) AS count
    FROM emergencies e
    JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id AND e.drill = 1
    WHERE e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
    GROUP BY e.emergency_type_id
");
    return $result;
}
function select_planstatistics_brodcastEventResponseTimes($emmaPlanId){
    global $emmadb;
    $result = $emmadb->query("
    SELECT sm.emergency_id, min(sm.created_date) AS first_broadcast, e.created_date AS event_date, TIMESTAMPDIFF(SECOND, e.created_date, min(sm.created_date)) AS time_diff
    FROM emergencies e
      JOIN emma_system_messages sm ON e.emergency_id = sm.emergency_id AND sm.type = '1'
      JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
    WHERE e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
    AND e.drill = '0'
    GROUP BY e.emergency_id
");
    return $result;
}
function select_planstatistics_userEventResponseTimes($emmaPlanId){
    global $emmadb;
    $result = $emmadb->query("
    SELECT
      e.emergency_id,
      er.user_id,
      er.emergency_response_id,
      min(er.created_date)                                        AS first_response,
      e.created_date                                              AS event_date,
      TIMESTAMPDIFF(SECOND, e.created_date, min(er.created_date)) AS time_diff
    FROM emergencies e
      JOIN emergency_responses er ON e.emergency_id = er.emergency_id AND er.status != '-1'
      JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
    WHERE e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
    AND e.drill = '0'
    GROUP BY e.emergency_id, er.user_id
    HAVING (TIMESTAMPDIFF(SECOND, e.created_date, min(er.created_date)) < (30*60))
");
    return $result;
}
function select_planstatistics_broadcastDrillResponseTimes($emmaPlanId){
    global $emmadb;
    $result = $emmadb->query("
    SELECT sm.emergency_id, min(sm.created_date) AS first_broadcast, e.created_date AS event_date, TIMESTAMPDIFF(SECOND, e.created_date, min(sm.created_date)) AS time_diff
    FROM emergencies e
      JOIN emma_system_messages sm ON e.emergency_id = sm.emergency_id AND sm.type = '1'
      JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
    WHERE e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
    AND e.drill = '1'
    GROUP BY e.emergency_id
");
    return $result;
}
function select_planstatistics_usertDrillResponseTimes($emmaPlanId){
    global $emmadb;
    $result = $emmadb->query("
    SELECT
      e.emergency_id,
      er.user_id,
      er.emergency_response_id,
      min(er.created_date)                                        AS first_response,
      e.created_date                                              AS event_date,
      TIMESTAMPDIFF(SECOND, e.created_date, min(er.created_date)) AS time_diff
    FROM emergencies e
      JOIN emergency_responses er ON e.emergency_id = er.emergency_id AND er.status != '-1'
      JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
    WHERE e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
    AND e.drill = '1'
    GROUP BY e.emergency_id, er.user_id
    HAVING (TIMESTAMPDIFF(SECOND, e.created_date, min(er.created_date)) < (30*60))
");
    return $result;
}
function select_planstatistics_emmaResources($emmaPlanId){
    global $emmadb;
    $result = $emmadb->query("
    SELECT er.emma_resource_type_id, rt.resource_name, count(er.emma_resource_id) AS count
    FROM emma_resources er
    JOIN emma_resource_types rt ON er.emma_resource_type_id = rt.emma_resource_type_id
    WHERE er.emma_plan_id IN (". implode(',',$emmaPlanId) .")
    AND er.active = '1'
    GROUP BY er.emma_resource_type_id
");
    return $result;
}
function select_getplans_guestCodes($length, $start, $search0, $search1, $search2, $search3){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS p.*
    FROM emma_plans p
    WHERE (" . ($search0 != "" ? "p.name like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "p.date_active like ('%" . $search1 . "%')" : "1") . ")
    AND (" . ($search2 != "" ? "p.date_expired like ('%" . $search2 . "%')" : "1") . ")
    AND (" . ($search3 != "" ? "p.max_sites like ('" . $search3 . "%')" : "1") . ")
    ORDER BY p.name
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getplans_nonuse(){
    global $emmadb;
    $result = $emmadb->query("
    SELECT p.emma_plan_id, p.name, ul.login_datetime
    FROM(SELECT u.emma_plan_id, l.login_datetime
        FROM(SELECT MAX(login_datetime) as login_datetime, username
            FROM emma_login_log
              GROUP BY username
              ORDER BY login_datetime DESC
            ) AS l
        JOIN users u ON l.username = u.username
        GROUP BY u.emma_plan_id
        ) AS ul
    JOIN emma_plans p ON ul.emma_plan_id = p.emma_plan_id
    WHERE ul.login_datetime < DATE_SUB(NOW(), INTERVAL 10 DAY)
    ORDER BY p.name
");
    return $result;
}
function select_getplans_openevents(){
    global $emmadb;
    $result = $emmadb->query("
    SELECT p.name, p.emma_plan_id, e.emergency_id, e.description, e.created_date
    FROM emma_plans p
      JOIN emergencies e ON p.emma_plan_id = e.emma_plan_id
    WHERE e.active = 1
    ORDER BY p.name
");
    return $result;
}
function select_getplans_noresources(){
    global $emmadb;
    $result = $emmadb->query("
    SELECT p.name, p.emma_plan_id
    FROM emma_plans p
    WHERE p.emma_plan_id NOT IN (
      SELECT emma_plan_id
      FROM emma_resources
      WHERE active = 1
    )
    ORDER BY p.name
");
    return $result;
}
function select_getplans_noassets(){
    global $emmadb;
    $result = $emmadb->query("
    SELECT p.name, p.emma_plan_id
    FROM emma_plans p
    WHERE p.emma_plan_id NOT IN (
      SELECT emma_plan_id
      FROM emma_assets
      WHERE active = 1
    )
    ORDER BY p.name
");
    return $result;
}
function select_getplans_nonedit_users(){
    global $emmadb;
    $result = $emmadb->query("
    SELECT p.name, p.emma_plan_id, up.edit_date
      FROM (
        SELECT uc.edit_date, u.emma_plan_id
          FROM (
            SELECT MAX(edit_date) AS edit_date, user_id
              FROM emma_edit_history_users
            GROUP BY user_id
            ORDER BY edit_date DESC
          )AS uc
        JOIN users u ON uc.user_id = u.id
        GROUP BY u.emma_plan_id
      )AS up
    JOIN emma_plans p ON up.emma_plan_id = p.emma_plan_id
      WHERE up.edit_date < DATE_SUB(NOW(), INTERVAL 30 DAY)
ORDER BY p.name
");
    return $result;
}
function select_getplans_nonuse_events(){
    global $emmadb;
    $result = $emmadb->query("
    SELECT p.emma_plan_id, p.name, e.created_date
    FROM (
      SELECT emma_plan_id, MAX(created_date)AS created_date
        FROM emergencies
      GROUP BY emma_plan_id
      ORDER BY created_date ASC
    )AS e
    JOIN emma_plans p ON e.emma_plan_id = p.emma_plan_id
      WHERE e.created_date < DATE_SUB(NOW(), INTERVAL 90 DAY)
    ORDER BY p.name
");
    return $result;
}
function select_getplans_nonuse_massnotifications(){
    global $emmadb;
    $result = $emmadb->query("
    SELECT p.emma_plan_id, p.name, e.created_date
    FROM (
      SELECT emma_plan_id, MAX(created_date)AS created_date
        FROM emma_mass_communications
      GROUP BY emma_plan_id
      ORDER BY created_date ASC
    )AS e
    JOIN emma_plans p ON e.emma_plan_id = p.emma_plan_id
      WHERE e.created_date < DATE_SUB(NOW(), INTERVAL 30 DAY)
    ORDER BY p.name
");
    return $result;
}
function select_getplans_nonuse_geofence(){
    global $emmadb;
    $result = $emmadb->query("
    SELECT p.emma_plan_id, p.name, e.created_date
    FROM(
      SELECT g.created_date, gl.plan_id
        FROM (
             SELECT geofence_id, MAX(created_date)AS created_date
             FROM emma_geofence_messages
             GROUP BY geofence_id
             ORDER BY created_date DESC
           )AS g
        JOIN emma_geofence_locations gl ON g.geofence_id = gl.id
      GROUP BY gl.plan_id
    )AS e
    JOIN emma_plans p ON e.plan_id = p.emma_plan_id
    WHERE e.created_date < DATE_SUB(NOW(), INTERVAL 30 DAY)
    ORDER BY p.name
");
    return $result;
}
function select_getplans_nogroupusers(){
    global $emmadb;
    $result = $emmadb->query("
    SELECT p.emma_plan_id, p.name, g.name AS group_name
      FROM emma_plans p
      JOIN emma_groups g ON p.emma_plan_id = g.emma_plan_id
        WHERE g.emma_group_id NOT IN (
          SELECT ug.emma_group_id 
            FROM emma_user_groups ug
        )
    ORDER BY p.name
");
    return $result;
}
function select_getplans_nogroups(){
    global $emmadb;
    $result = $emmadb->query("
    SELECT p.name, p.emma_plan_id
      FROM emma_plans p
    WHERE p.emma_plan_id NOT IN (
      SELECT emma_plan_id
        FROM emma_groups
    )
    ORDER BY p.name
");
    return $result;
}
function select_getplans_nousers(){
    global $emmadb;
    $result = $emmadb->query("
    SELECT p.name, p.emma_plan_id
      FROM emma_plans p
    WHERE p.emma_plan_id NOT IN (
      SELECT plan_id
        FROM emma_multi_plan AS emp
        WHERE emp.active = 1
    )
    ORDER BY p.name
");
    return $result;
}
function select_getplans_noscripts(){
    global $emmadb;
    $result = $emmadb->query("
    SELECT p.name, p.emma_plan_id
      FROM emma_plans p
    WHERE p.emma_plan_id NOT IN (
      SELECT emma_plan_id
        FROM emma_scripts
    )
    ORDER BY p.name
");
    return $result;
}
function select_getplans_nogeofences(){
    global $emmadb;
    $result = $emmadb->query("
    SELECT p.name, p.emma_plan_id
      FROM emma_plans p
    WHERE p.emma_plan_id NOT IN (
      SELECT plan_id
        FROM emma_geofence_locations
    )
    AND p.geofencing = 1
    ORDER BY p.name
");
    return $result;
}
function select_getplans_wrongpin(){
    global $emmadb;
    $result = $emmadb->query("
    SELECT p.name, p.emma_plan_id
      FROM emma_plans p
    WHERE p.emma_plan_id NOT IN (
      SELECT plan_id
        FROM emma_geofence_locations
    )
    AND p.geofencing = 1
    ORDER BY p.name
");
    return $result;
}
function select_getplans_masshistory(){
    global $emmadb;
    $result = $emmadb->query("
    SELECT p.name, p.emma_plan_id, u.firstname, u.lastname, c.created_date, c.notification, c.emma_mass_communication_id
    FROM emma_plans p
      JOIN emma_mass_communications c ON p.emma_plan_id = c.emma_plan_id
      JOIN users u ON c.created_by_id = u.id
    WHERE p.mass_communication = 1
    ORDER BY p.name, c.created_date
");
    return $result;
}
function select_getplans_geohistory(){
    global $emmadb;
    $result = $emmadb->query("
    SELECT p.name, p.emma_plan_id, u.firstname, u.lastname, c.created_date, c.description, l.fence_name, c.sent_to_qnt
    FROM emma_plans p
      JOIN emma_geofence_locations l ON p.emma_plan_id = l.plan_id
      JOIN emma_geofence_messages c ON l.id = c.geofence_id
      JOIN users u ON c.created_by_id = u.id
    WHERE p.geofencing = 1
    ORDER BY p.name, c.created_date
");
    return $result;
}
function select_getplans_masshistory_count($notID){
    global $emmadb;
    $result = $emmadb->query("
    SELECT COUNT(*) AS numusers
      FROM communication_received_groups rg
        JOIN emma_user_groups ug ON rg.emma_group_id = ug.emma_group_id
    WHERE rg.communication_id = '". $notID ."'
");
    return $result;
}
function select_getplans_events_cumulative($planID){
    global $emmadb;
    $result = $emmadb->query("
    SELECT COUNT(*) AS count
    FROM emergencies
    WHERE emma_plan_id = '". $planID ."'
    AND drill = 0
");
    return $result;
}
function select_getplans_drills_cumulative($planID){
    global $emmadb;
    $result = $emmadb->query("
    SELECT COUNT(*) AS count
    FROM emergencies
    WHERE emma_plan_id = '". $planID ."'
    AND drill = 1
");
    return $result;
}
function select_getreports_listing($admin){
    global $emmadb;
    $privilege = '';
    if($admin == 0){
        $privilege = 'WHERE l.user_privilege = 1';
    }
    $result = $emmadb->query("
    SELECT l.title, l.report
    FROM emma_report_listing l 
    ".$privilege."
    ORDER BY l.title
");
    return $result;
}

function select_policcalls_events($emmaPlanId, $active, $orderString, $length, $start, $search0, $search1, $search2, $search3, $search4, $search5){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS ecl.call_id, ecl.emergency_id, ecl.call_datetime, ecl.active, ecl.latitude, ecl.longitude, et.name AS type, CONCAT(u.firstname, ' ', u.lastname) AS username, u.id AS userid, u.username AS useremail
    FROM emma_911_call_log AS ecl
    JOIN users u on ecl.userid = u.id
    LEFT JOIN emergencies e on ecl.emergency_id = e.emergency_id
    LEFT JOIN emergency_types et on et.emergency_type_id = e.emergency_type_id
    WHERE (" . ($active != '' ? "ecl.active = " . $active : "1") . ")
    AND (" . ($emmaPlanId != '' ? "ecl.plan_id = " . $emmaPlanId : "1") . ")
    AND (" . ($search0 != "" ? "et.name like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') like ('%" . $search1 . "%')" : "1") . ")
    AND (" . ($search2 != "" ? "ecl.call_datetime like ('%" . $search2 . "%')" : "1") . ")
    AND (" . ($search3 != "" ? "ecl.latitude like ('%" . $search3 . "%')" : "1").")
    AND (" . ($search4 != "" ? "ecl.longitude like ('%" . $search4 . "%')" : "1").")
    AND (" . ($search5 != "" ? "(ecl.active = '0' AND 'Closed' like ('%" . $search5 . "%')) OR (ecl.active = '1' AND 'Active' like ('%" . $search5 . "%'))" : "1") . ")
    ORDER BY ". $orderString .", ecl.emergency_id desc 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getresponces_responses($emergencyId){
    global $emmadb;
    $result = $emmadb->query("
        SELECT er.*, CONCAT(u.firstname, ' ', u.lastname) AS user, u.phone, CONCAT(u.firstname, ' ', u.lastname) AS first_last_name, u.username, u.id as user_id, u.emma_pin, er.pin as response_pin, ers.*, CONCAT(v.firstname, ' ', v.lastname) as validation_user
        FROM emergency_responses er
        JOIN users u ON er.user_id = u.id
        JOIN emergency_response_status ers ON er.status = ers.emergency_response_status_id
        LEFT JOIN users v ON er.validated_id = v.id
        INNER JOIN (
           SELECT
             user_id,
             MAX(timestamp(created_date)) created_date
           FROM emergency_responses
           WHERE emergency_id = '" . $emergencyId . "'
           GROUP BY user_id
        ) b ON er.user_id = b.user_id AND er.created_date = b.created_date
        WHERE u.display = 'yes'
    ");
    return $result;
}
function select_getresponces_subevents($emergencyId){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, et.img_filename
        FROM emergencies as e 
        LEFT JOIN emergency_types as et on e.emergency_type_id = et.emergency_type_id
        WHERE parent_emergency_id = '". $emergencyId ."'
        AND active = '1'
    ");
    return $result;
}
function select_getresponces_events($emmaPlanId, $active, $drill, $orderString, $length, $start, $search0, $search1, $search2, $search3){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS e.emma_security_id, et.name AS type, CONCAT(u.firstname, ' ', u.lastname) AS userName, e.active, e.created_date, '0' as call_log, u.id as userid, u.username as useremail
    FROM emma_securities e
    JOIN emma_security_types et ON e.emma_security_type_id = et.emma_security_type_id
    JOIN users u ON e.created_by_id = u.id
    WHERE (" . ($active != '' ? "e.active = " . $active : "1") . ")
    AND (" . (isset($drill) && $drill != "" ? "e.drill = '" . $drill . "'" : "1") . ")
    AND (" . ($emmaPlanId != '' ? "e.emma_plan_id = " . $emmaPlanId : "1") . ")
    AND (" . ($search0 != "" ? "et.name like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') like ('%" . $search1 . "%')" : "1") . ")
    AND (" . ($search2 != "" ? "(e.active = '0' AND 'Closed' like ('%" . $search2 . "%')) OR (e.active = '1' AND 'Active' like ('%" . $search2 . "%'))" : "1") . ")
    AND (" . ($search3 != "" ? "e.created_date like ('%" . $search3 . "%')" : "1") . ")
    ORDER BY ". $orderString .", created_date desc 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getresponces_callLogs($emmaSecurityId){
    global $emmadb;
    $result = $emmadb->query("
    select *
    from emma_call_reports
    where security_id = '".$emmaSecurityId."'
  ");
    return $result;
}
function select_getsecurityTimeline_responses($securityId, $received){
    global $emmadb;
    $result = $emmadb->query("
    SELECT *
    FROM 
    (
      (
        SELECT
          CONCAT('usr-', sr.emma_security_response_id)                AS id,
          sr.comments                                                 AS message,
          CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') AS user,
          u.id as user_id,
          ''                                                          AS status, 
          ''                                                          AS status_name,
          '#3adb76'                                                   AS status_color,
          'success'                                                    AS status_class,
          CONCAT(u.firstname, ' ', u.lastname)                        AS first_last_name,
          u.username,
          sr.updated_date,
          '' as description,
          '' as comments,
          '' as emma_security_message_id
        FROM emma_security_responses sr
        JOIN users u ON sr.user_id = u.id
        WHERE sr.security_id = '" . $securityId . "'
          AND CONCAT('usr-', sr.emma_security_response_id) NOT IN ('" .
        $received . "') 
      ) UNION ALL (
        SELECT
          CONCAT('sys-', sm.emma_security_message_id) AS id,
          sm.message,
          CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') AS user,
          u.id as user_id, 
          ''                                        AS status, 
          'Alert'                                   AS status_name,
          '#1779ba'                                 AS status_color,
          'system'                                  AS status_class,
          'System'                                  AS first_last_name,
          ''                                        AS username,
          sm.created_date AS updated_date,
          sm.description,
          sm.comments,
          sm.emma_security_message_id
        FROM emma_security_messages sm
        JOIN users u on sm.created_by_id = u.id
        WHERE sm.security_id = '" . $securityId . "'
          AND CONCAT('sys-', sm.emma_security_message_id) NOT IN ('" . $received . "')
      )
    ) AS results
    ORDER BY updated_date
  ");
    return $result;
}
function select_getsitestatistics_eventMonths($emmaPlanId, $site_id){
    global $emmadb;
    $result = $emmadb->query("
    SELECT year(e.created_date) AS year, month(e.created_date) AS month, count(*) AS count, sum(CASE WHEN e.drill = 1 THEN 1 ELSE 0 END) AS drill_count, sum(CASE WHEN e.drill = 0 THEN 1 ELSE 0 END) AS event_count
    FROM emergencies e
    JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
    WHERE e.emma_plan_id = '" . $emmaPlanId . "'
    AND e.emma_site_id = ". $site_id ."
    GROUP BY year(e.created_date), month(e.created_date)
");
    return $result;
}
function select_getsites_sites($emmaPlanId, $orderString, $length, $start, $search0, $search1, $search2, $search3){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS s.emma_site_name, s.emma_site_id, e.emergency_id, MAX(e.active) AS emergency_status, count(DISTINCT e.emergency_id) AS emergencies, count(DISTINCT d.emergency_id) AS drills, null AS admins, null AS students, null AS staff
    FROM emma_sites AS s
      LEFT JOIN emergencies AS e ON e.emma_site_id = s.emma_site_id AND e.drill = 0
      LEFT JOIN emergencies AS d ON d.emma_site_id = s.emma_site_id AND d.drill = 1
    WHERE (" . ($emmaPlanId != '' ? "s.emma_plan_id = " . $emmaPlanId : "1") . ")
    AND (" . ($search0 != "" ? "s.emma_site_name like ('%" . $search0 . "%')" : "1") . ")
    GROUP BY s.emma_site_id
    HAVING (
      (" . ($search1 != "" ? "(max(e.active) = '0' AND 'Inactive' like ('" . $search1 . "%')) OR (max(e.active) = '1' AND 'Active' like ('" . $search1 . "%'))" : "1") . ")
    AND (" . ($search2 != "" ? "count(DISTINCT e.emergency_id) like ('" . $search2 . "%')" : "1") . ")
    AND (" . ($search3 != "" ? "count(DISTINCT d.emergency_id) like ('" . $search3 . "%')" : "1") . ")
    )
    ORDER BY ". $orderString ." 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getsosmap_soses($planId){
    global $emmadb;
    $result = $emmadb->query("
    SELECT s.*, CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') AS user, u.phone, u.landline_phone, u.id as user_id
    FROM emma_sos s
    JOIN users u ON s.created_by_id = u.id AND u.emma_plan_id = '" . $planId . "'
    WHERE s.closed_date IS NULL
    AND s.cancelled_date IS NULL
  ");
    return $result;
}
function select_gettimeline_responses($received, $emergencyId){
    global $emmadb;
    $result = $emmadb->query("
    SELECT *
    FROM 
    (
      (
        SELECT
          CONCAT('usr-', er.emergency_response_id)                    AS id,
          er.comments                                                 AS message,
          CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') AS user,
          u.id as user_id,
          er.status, 
          ers.name                                                    AS status_name,
          ers.color                                                   AS status_color,
          ers.class                                                   AS status_class,
          CONCAT(u.firstname, ' ', u.lastname)                        AS first_last_name,
          u.username,
          er.updated_date,
          '' as description,
          '' as comments,
          '' as emma_system_message_id,
          er.pin                                                      AS response_pin,
          u.emma_pin,
          CONCAT(v.firstname, ' ', v.lastname)                        AS validation_user,
          validated_time                                              AS validation_time
        FROM emergency_responses er
        JOIN users u ON er.user_id = u.id
        JOIN emergency_response_status ers ON er.status = ers.emergency_response_status_id AND CONCAT('usr-', er.emergency_response_id) NOT IN ('" .
        $received . "') 
        LEFT JOIN users v ON er.validated_id = v.id
        WHERE er.emergency_id = '" . $emergencyId . "'
          AND er.status != '-1'
      ) UNION ALL (
        SELECT
          CONCAT('sys-', sm.emma_system_message_id) AS id,
          sm.message,
          CONCAT(u.firstname, ' ', u.lastname, ' (', u.username, ')') AS user,
          u.id as user_id, 
          '-2' AS status, 
          'Alert'                                   AS status_name,
          '#ff0000'                                 AS status_color,
          'system'                                  AS status_class,
          'System'                                  AS first_last_name,
          ''                                        AS username,
          sm.created_date AS updated_date,
          sm.description,
          sm.comments,
          sm.emma_system_message_id,
          '0000'                                    AS response_pin,
          '0000'                                    AS emma_pin,
          ''                                        AS validation_user,
          ''                                        AS validation_time
        FROM emma_system_messages sm
        JOIN users u on sm.created_by_id = u.id
        WHERE sm.emergency_id = '" . $emergencyId . "'
          AND CONCAT('sys-', sm.emma_system_message_id) NOT IN ('" . $received . "')
      )
    ) AS results
    ORDER BY updated_date
  ");
    return $result;
}
function select_gettimeline_messageGroups($messageID){
    global $emmadb;
    $result = $emmadb->query("
        select g.emma_group_id, g.name
        from emma_system_message_groups mg
        join emma_groups g on mg.emma_group_id = g.emma_group_id
        where mg.emma_system_message_id = '" . $messageID . "'
      ");
    return $result;
}
function select_gettimeline_subEvents($emergencyId, $received){
    global $emmadb;
    $result = $emmadb->query("
    select 
      e.emergency_id as id,
      et.emergency_type_id as type_id,
      et.name as type_name,
      et.badge,
      es.emma_site_name, 
      e.created_date,
      e.active,
      CONCAT(u.firstname, ' ', u.lastname) as user
    from emergencies e
    join emergency_types et on e.emergency_type_id = et.emergency_type_id
    join emma_sites es on e.emma_site_id = es.emma_site_id
    join users u on e.created_by_id = u.id
    where e.parent_emergency_id = '" . $emergencyId . "'
      AND CONCAT('evt-', e.emergency_id) NOT IN ('" . $received . "')
  ");
    return $result;
}
function select_getusers_users($emmaPlanId, $orderString, $length, $start, $search0, $search1, $search2, $search3){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS u.username, u.id, u.firstname, u.lastname, group_concat(DISTINCT eg.name ORDER BY eg.name) AS `group`
    FROM users u
    LEFT JOIN emma_user_groups AS gb ON u.id = gb.user_id
    LEFT JOIN emma_groups eg ON gb.emma_group_id = eg.emma_group_id
    LEFT JOIN emma_multi_plan mp ON u.id = mp.user_id AND eg.emma_plan_id = mp.plan_id
    WHERE u.display = 'yes'
    AND u.temporary_code IS NULL
    AND u.active = 1
    AND mp.plan_id = ". $emmaPlanId ."
    AND mp.active = 1
    AND (" . ($search0 != "" ? "u.username like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "u.firstname like ('%" . $search1 . "%')" : "1") . ")
    AND (" . ($search2 != "" ? "u.lastname like ('%" . $search2 . "%')" : "1") . ")
    AND (" . ($search3 != "" ? "eg.name like ('" . $search3 . "%')" : "1") . ")
    GROUP BY u.id
    ORDER BY ". $orderString ." 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}function select_getusersdeactivate_users($emmaPlanId, $length, $start, $search0, $search1, $search2, $search3){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS u.username, u.id, u.firstname, u.lastname, group_concat(DISTINCT eg.name ORDER BY eg.name) AS `group`
    FROM users u
    LEFT JOIN emma_user_groups AS gb ON u.id = gb.user_id
    LEFT JOIN emma_groups eg ON gb.emma_group_id = eg.emma_group_id
    LEFT JOIN emma_multi_plan mp ON u.id = mp.user_id AND eg.emma_plan_id = mp.plan_id
    WHERE u.display = 'yes'
    AND u.temporary_code IS NULL
    AND u.active = 1
    AND mp.plan_id = ". $emmaPlanId ."
    AND (" . ($search0 != "" ? "u.username like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "u.firstname like ('%" . $search1 . "%')" : "1") . ")
    AND (" . ($search2 != "" ? "u.lastname like ('%" . $search2 . "%')" : "1") . ")
    AND (" . ($search3 != "" ? "eg.name like ('" . $search3 . "%')" : "1") . ")
    GROUP BY u.id
    ORDER BY u.firstname, u.lastname 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getusers_users_admin($emmaPlanId, $orderString, $length, $start, $search0, $search1, $search2, $search3, $search4){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS u.username, u.id, u.firstname, u.lastname, group_concat(DISTINCT eg.name ORDER BY eg.name) AS `group`, group_concat(DISTINCT p.name ORDER BY p.name) AS plan, u.password
    FROM users u
    LEFT JOIN emma_user_groups AS gb ON u.id = gb.user_id
    LEFT JOIN emma_groups eg ON gb.emma_group_id = eg.emma_group_id
    LEFT JOIN emma_multi_plan mp ON u.id = mp.user_id AND eg.emma_plan_id = mp.plan_id
    LEFT JOIN emma_plans p ON mp.plan_id = p.emma_plan_id
    WHERE u.active = 1
    AND mp.active = 1
    AND p.active = 1
    AND mp.plan_id IN ('". implode('\',\'',$emmaPlanId) ."')
    AND (" . ($search0 != "" ? "u.username like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "u.firstname like ('%" . $search1 . "%')" : "1") . ")
    AND (" . ($search2 != "" ? "u.lastname like ('%" . $search2 . "%')" : "1") . ")
    AND (" . ($search3 != "" ? "eg.name like ('" . $search3 . "%')" : "1") . ")
    AND (" . ($search4 != "" ? "p.name like ('" . $search4 . "%')" : "1") . ")
    GROUP BY u.id
    ORDER BY ". $orderString ." 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_getusersinactive_users($emmaPlanId, $orderString, $length, $start, $search0, $search1, $search2, $search3){
    global $emmadb;
    $result = $emmadb->query("
    SELECT SQL_CALC_FOUND_ROWS u.username, u.id, u.firstname, u.lastname, group_concat(DISTINCT eg.name ORDER BY eg.name) AS `group`
    FROM users u
    LEFT JOIN emma_user_groups AS gb ON u.id = gb.user_id
    LEFT JOIN emma_groups eg ON gb.emma_group_id = eg.emma_group_id
    LEFT JOIN emma_guest_codes egc on u.temporary_code = egc.guest_code_id
    LEFT JOIN emma_multi_plan mp on u.id = mp.user_id
    WHERE mp.active = 0
    AND u.active = 1
    AND mp.plan_id = " . $emmaPlanId . "
    AND (" . ($search0 != "" ? "u.username like ('%" . $search0 . "%')" : "1") . ")
    AND (" . ($search1 != "" ? "u.firstname like ('%" . $search1 . "%')" : "1") . ")
    AND (" . ($search2 != "" ? "u.lastname like ('%" . $search2 . "%')" : "1") . ")
    AND (" . ($search3 != "" ? "eg.name like ('" . $search3 . "%')" : "1") . ")
    GROUP BY u.id
    ORDER BY ". $orderString ." 
    LIMIT " . $length . " OFFSET " . $start . "
");
    return $result;
}
function select_feed_privileges_withPlan($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT p.feed_toggle, p.feed_timer_minutes
        FROM emma_plans p
        WHERE p.emma_plan_id = '" . $planID . "'
    ");
    return $result;
}
function select_notificationcallback_events($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, et.name as type_name
        FROM emergencies e
        JOIN emergency_types et on e.emergency_type_id = et.emergency_type_id
        WHERE e.emma_plan_id = '" . $planID . "'
      ");
    return $result;
}
function select_notifycreator_iosCheckQuery($securityId){
    global $emmadb;
    $result = $emmadb->query("
        SELECT iosFireBaseToken
        FROM emma_securities
        WHERE emma_security_id = '". $securityId ."'
    ");
    return $result;
}
function select_notifycreator_creators($creatorId){
    global $emmadb;
    $result = $emmadb->query("
      select *
      from users
      where id = '".$creatorId."'
    ");
    return $result;
}
function select_respondMassCommunication_IOSCheckQuery($securityId){
    global $emmadb;
    $result = $emmadb->query("
        SELECT iosFireBaseToken
        FROM emma_mass_communications
        WHERE emma_mass_communication_id = '". $securityId ."'
    ");
    return $result;
}
function select_searchCurrentEvents_soss($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT s.*
        FROM emma_multi_plan mp
        JOIN emma_sos s ON mp.plan_id = s.plan_id
        WHERE mp.user_id = '". $userID ."' 
        AND s.cancelled_date IS NULL
        AND s.help_date IS NOT NULL
        AND s.closed_date IS NULL
        AND mp.active = 1
    ");
    return $result;
}
function select_searchCurrentEvents_notifications($userID){
    global $emmadb;
        $result = $emmadb->query("
        SELECT e.*
        FROM users u
        JOIN emma_multi_plan mp ON u.id = mp.user_id
        JOIN emma_mass_communications e ON mp.plan_id = e.emma_plan_id
        WHERE u.id = '". $userID ."'
        AND e.created_date > DATE_SUB(CURDATE(), INTERVAL 1 WEEK)
        AND mp.active = 1
        GROUP BY e.notification
    ");
    return $result;
}
function select_searchCurrentEvents_lockdowns($userID){
    global $emmadb;
        $result = $emmadb->query("
        SELECT e.*
        FROM users u
        JOIN emma_multi_plan mp ON u.id = mp.user_id
        JOIN emma_lockdowns e ON mp.plan_id = e.plan_id
        WHERE u.id = '". $userID ."'
        AND mp.active = 1
    ");
    return $result;
}
function select_searchCurrentEvents_modified_lockdowns($userID){
    global $emmadb;
        $result = $emmadb->query("
        SELECT e.*
        FROM users u
        JOIN emma_multi_plan mp ON u.id = mp.user_id
        JOIN emma_modified_lockdowns e ON mp.plan_id = e.plan_id
        WHERE u.id = '". $userID ."'
        AND mp.active = 1
    ");
    return $result;
}
function select_feed_notifications_with_user($userID, $limit){
    global $emmadb;
        $result = $emmadb->query("
        SELECT e.*
        FROM users u
        JOIN emma_multi_plan mp ON u.id = mp.user_id
        JOIN emma_mass_communications e ON mp.plan_id = e.emma_plan_id
        WHERE u.id = '". $userID ."'
        AND e.created_date BETWEEN timestamp(DATE_SUB(NOW(), INTERVAL ". $limit ." MINUTE)) AND timestamp(NOW())
        AND mp.active = 1
    ");
    return $result;
}
function select_searchNewEvents_groups($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.*
        FROM emma_groups g 
        JOIN emma_user_groups u ON g.emma_group_id = u.emma_group_id
        WHERE u.user_id = '". $userID ."'
    ");
    return $result;
}
function select_searchNewEvents_events($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, t.name as type_name, t.img_filename, u.username, u.phone
        FROM users ur 
        JOIN emma_multi_plan mp ON ur.id = mp.user_id
        JOIN emergencies e ON  mp.plan_id = e.emma_plan_id
        JOIN emergency_types t ON e.emergency_type_id = t.emergency_type_id
        LEFT JOIN users u ON e.created_by_id = u.id
        WHERE ur.id = '". $userID ."'
        AND e.active = '1'
        AND mp.active = 1
    ");
    return $result;
}
function select_searchNewEvents_eventGroups($eventID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*
        FROM emergency_received_groups e 
        WHERE e.emergency_id = '". $eventID ."'
    ");
    return $result;
}
function select_searchNewEvents_events_securities($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, t.name as type_name, t.img_filename, u.username, u.phone
        FROM emma_securities e 
        JOIN emma_security_types t ON e.emma_security_type_id = t.emma_security_type_id
        LEFT JOIN users u ON e.created_by_id = u.id
        WHERE e.emma_plan_id = '". $planID ."'
        AND e.active = '1'
    ");
    return $result;
}
function select_searchNewEvents_events_sos($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT s.*, u.username
        FROM emma_multi_plan mp
        JOIN emma_sos s ON mp.plan_id = s.plan_id
        JOIN users u ON s.created_by_id = u.id
        WHERE mp.user_id = '". $userID ."' 
        AND s.cancelled_date IS NULL
        AND s.help_date IS NOT NULL
        AND s.closed_date IS NULL
        AND mp.active = 1
    ");
    return $result;
}
function select_searchNewEvents_events_mc($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, ur.username, ur.phone
        FROM users u
        JOIN emma_multi_plan mp ON u.id = mp.user_id
        JOIN emma_mass_communications e ON mp.plan_id = e.emma_plan_id
        LEFT JOIN users ur ON e.created_by_id = ur.id
        WHERE u.id = '". $userID ."'
        AND e.created_date > DATE_SUB(CURDATE(), INTERVAL 1 WEEK)
        AND mp.active = 1
        GROUP BY e.notification
    ");
    return $result;
}
function select_searchNewEvents_events_l($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, ur.username, ur.phone, n.notification
        FROM users u
        JOIN emma_multi_plan mp ON u.id = mp.user_id
        JOIN emma_lockdowns e ON mp.plan_id = e.plan_id
        LEFT JOIN emma_lockdown_notifications n ON e.plan_id = n.plan_id 
        LEFT JOIN users ur ON e.created_by_id = ur.id
        WHERE u.id = '". $userID ."'
        AND mp.active = 1
    ");
    return $result;
}
function select_searchNewEvents_events_ml($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, ur.username, ur.phone, n.notification
        FROM users u
        JOIN emma_multi_plan mp ON u.id = mp.user_id
        JOIN emma_modified_lockdowns e ON mp.plan_id = e.plan_id
        LEFT JOIN emma_modified_lockdown_notifications n ON e.plan_id = n.plan_id 
        LEFT JOIN users ur ON e.created_by_id = ur.id
        WHERE u.id = '". $userID ."'
        AND mp.active = 1
    ");
    return $result;
}
function select_lockdown_drill_message($lockdownID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT n.*
        FROM emma_lockdowns e 
        JOIN emma_lockdown_drill_notifications n ON e.plan_id = n.plan_id 
        WHERE e.lockdown_id = '". $lockdownID ."'
    ");
    return $result;
}
function select_modified_lockdown_message($lockdownID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT n.*
        FROM emma_modified_lockdowns e 
        JOIN emma_modified_lockdown_notifications n ON e.plan_id = n.plan_id 
        WHERE e.m_lockdown_id = '". $lockdownID ."'
    ");
    return $result;
}
function select_setHomePlans_plans($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT p.name, p.emma_plan_id
        FROM emma_multi_plan mp
        LEFT JOIN emma_plans p ON mp.plan_id = p.emma_plan_id
        WHERE mp.user_id = '". $userID ."'
        AND mp.active = 1
    ");
    return $result;
}
function select_userSignup($firstname, $lastname){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.*
        FROM users u 
        WHERE u.firstname = '". $firstname ."'
        AND u.lastname = '". $lastname ."'
    ");
    return $result;
}
function select_validateEmergencyResponse_messages($messageId){
    global $emmadb;
    $result = $emmadb->query("
        select er.*, u.emma_pin as user_pin, ers.color, ers.class, ers.table, ers.icon, CONCAT(u.firstname, ' ', u.lastname) AS user, u.phone, CONCAT(u.firstname, ' ', u.lastname) AS first_last_name, u.username, u.id as user_id
        from emergency_responses er
        join users u on er.user_id = u.id
        join emergency_response_status ers on er.status = ers.emergency_response_status_id
        where er.emergency_response_id = '" . $messageId . "'
    ");
    return $result;
}
function select_validateEmergencyResponse_validationUsers($validatedId){
    global $emmadb;
    $result = $emmadb->query("
      select u.*, CONCAT(u.firstname, ' ', u.lastname) AS full_name
      from users u
      where u.id = '" . $validatedId . "'
    ");
    return $result;
}
function select_editGeofences_geofences($geofenceID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.*
        FROM emma_geofence_locations g
        WHERE g.id = '" . $geofenceID . "'
    ");
    return $result;
}
function select_editGeofences_fences($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.id, g.fence_name
        FROM emma_geofence_locations g
        WHERE g.plan_id = '". $planID ."'
        ORDER BY g.fence_name
    ");
    return $result;
}
function select_reportsos_a($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT es.*, concat(u.firstname , ' ' , u.lastname) as user_name, concat(uc.firstname , ' ' , uc.lastname) as closed_user_name, ep.name as plan_name
        FROM emma_sos es
        LEFT JOIN users u on u.id = es.created_by_id
        left JOIN users uc on uc.id = es.closed_by_id
        JOIN emma_plans ep on ep.emma_plan_id = es.plan_id
        WHERE es.plan_id = '".$planID."'
    ");
    return $result;
}
function select_reportsos_h($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT es.*, concat(u.firstname , ' ' , u.lastname) as user_name, concat(uc.firstname , ' ' , uc.lastname) as closed_user_name, ep.name as plan_name
        FROM emma_sos es
        LEFT JOIN users u on u.id = es.created_by_id
        left JOIN users uc on uc.id = es.closed_by_id
        JOIN emma_plans ep on ep.emma_plan_id = es.plan_id
        WHERE es.plan_id = '".$planID."'
        AND es.pending_date IS NOT NULL
        AND es.help_date IS NOT NULL
        AND es.closed_date IS NULL
    ");
    return $result;
}
function select_reportsos_p($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT es.*, concat(u.firstname , ' ' , u.lastname) as user_name, concat(uc.firstname , ' ' , uc.lastname) as closed_user_name, ep.name as plan_name
        FROM emma_sos es
        LEFT JOIN users u on u.id = es.created_by_id
        left JOIN users uc on uc.id = es.closed_by_id
        JOIN emma_plans ep on ep.emma_plan_id = es.plan_id
        WHERE es.plan_id = '".$planID."'
        AND es.pending_date IS NOT NULL
        AND es.help_date IS NULL
        AND es.cancelled_date IS NULL
        AND es.closed_date IS NULL
    ");
    return $result;
}
function select_reportsos_c($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT es.*, concat(u.firstname , ' ' , u.lastname) as user_name, concat(uc.firstname , ' ' , uc.lastname) as closed_user_name, ep.name as plan_name
        FROM emma_sos es
        LEFT JOIN users u on u.id = es.created_by_id
        left JOIN users uc on uc.id = es.closed_by_id
        JOIN emma_plans ep on ep.emma_plan_id = es.plan_id
        WHERE es.plan_id = '".$planID."'
        AND es.cancelled_date IS NOT NULL
    ");
    return $result;
}
function select_reportsos_l($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT es.*, concat(u.firstname , ' ' , u.lastname) as user_name, concat(uc.firstname , ' ' , uc.lastname) as closed_user_name, ep.name as plan_name
        FROM emma_sos es
        LEFT JOIN users u on u.id = es.created_by_id
        left JOIN users uc on uc.id = es.closed_by_id
        JOIN emma_plans ep on ep.emma_plan_id = es.plan_id
        WHERE es.plan_id = '".$planID."'
        AND es.closed_date IS NOT NULL
    ");
    return $result;
}
function select_reportDrills_eventsAll($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*,es.emma_site_street_address,es.emma_site_city,concat(esc.first_name , ' ' , esc.last_name) as site_contact_name, esc.phone,esc.email, et.name as emergency_type_name, et.badge, et.sound, et.vibrate, u.username as user_created_username,concat(u.firstname , ' ' , u.lastname) as user_created_name, uc.username as user_closed_username, concat(uc.firstname , ' ' , uc.lastname) as user_closed_name, ep.name as plan_name 
        FROM emergencies e
        LEFT JOIN emma_sites es on es.emma_site_id = e.emma_site_id
        LEFT JOIN emma_site_contacts esc on esc.emma_site_contact_id = es.emma_site_contact_id
        JOIN emergency_types et on et.emergency_type_id = e.emergency_type_id
        JOIN users u on u.id = e.created_by_id
        JOIN emma_plans ep on ep.emma_plan_id = '".$planID."'
        LEFT JOIN users uc on uc.id = e.closed_by_id
        WHERE e.emma_plan_id = '".$planID."'
        AND e.drill = 1
    ");
    return $result;
}
function select_reportDrills_responses($eventID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT concat(u.firstname , ' ' , u.lastname) as user_name, ep.name as emma_plan_name, ers.name as response_status, es.comments
        FROM emergency_responses es
        join emergency_response_status ers on es.status = ers.emergency_response_status_id
        JOIN users u on u.id = es.user_id
        JOIN emma_plans ep on u.emma_plan_id = ep.emma_plan_id
        WHERE es.emergency_id = '".$eventID."'
    ");
    return $result;
}
function select_reportAssetType_assetTypes($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT eat.name, eat.emma_asset_type_id, ep.name as plan_name, ea.latitude, ea.longitude, ea.address
        FROM emma_asset_types as eat
        JOIN emma_assets ea on ea.emma_asset_type_id = eat.emma_asset_type_id AND ea.active = 1
        JOIN emma_plans ep on eat.emma_plan_id = ep.emma_plan_id    
        WHERE eat.emma_plan_id = '".$planID."'
    ");
    return $result;
}
function select_reportAsset_assets($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT s.*, t.name AS type_name, p.name AS plan_name, CONCAT(u.firstname , ' ' , u.lastname) AS created_name
        FROM emma_assets s
        JOIN emma_plans p ON s.emma_plan_id = p.emma_plan_id
        JOIN emma_asset_types t ON s.emma_asset_type_id = t.emma_asset_type_id
        JOIN users u ON s.created_by_id = u.id
        WHERE s.emma_plan_id = '". $planID ."'
        and s.active = '1'
    ");
    return $result;
}
function select_reportSOS_sos($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT es.*, concat(u.firstname , ' ' , u.lastname) as user_name, concat(uc.firstname , ' ' , uc.lastname) as closed_user_name, ep.name as plan_name
        FROM emma_sos es
        LEFT JOIN users u on u.id = es.created_by_id
        left JOIN users uc on uc.id = es.closed_by_id
        JOIN emma_plans ep on ep.emma_plan_id = es.plan_id
        WHERE es.plan_id = '".$planID."'
    ");
    return $result;
}
function select_reportLOCKDOWN_lockdown($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT el.*, concat(u.firstname , ' ' , u.lastname) as user_name, ep.name as plan_name
        FROM emma_lockdowns el
        LEFT JOIN users u on u.id = el.created_by_id
        JOIN emma_plans ep on ep.emma_plan_id = el.plan_id
        WHERE el.plan_id = '".$planID."'
    ");
    return $result;
}
function select_reportEvents_eventsAll($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*,es.emma_site_street_address,es.emma_site_city,concat(esc.first_name , ' ' , esc.last_name) as site_contact_name, esc.phone,esc.email, et.name as emergency_type_name, et.badge, et.sound, et.vibrate, u.username as user_created_username,concat(u.firstname , ' ' , u.lastname) as user_created_name, uc.username as user_closed_username, concat(uc.firstname , ' ' , uc.lastname) as user_closed_name, ep.name as plan_name 
        FROM emergencies e
        LEFT JOIN emma_sites es on es.emma_site_id = e.emma_site_id
        LEFT JOIN emma_site_contacts esc on esc.emma_site_contact_id = es.emma_site_contact_id
        JOIN emergency_types et on et.emergency_type_id = e.emergency_type_id
        JOIN users u on u.id = e.created_by_id
        JOIN emma_plans ep on ep.emma_plan_id = '".$planID."'
        LEFT JOIN users uc on uc.id = e.closed_by_id
        WHERE e.emma_plan_id = '".$planID."'
    ");
    return $result;
}
function select_reportEvents_responses($eventID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT concat(u.firstname , ' ' , u.lastname) as user_name, ep.name as emma_plan_name, ers.name as response_status, es.comments
        FROM emergency_responses es
        join emergency_response_status ers on es.status = ers.emergency_response_status_id
        JOIN users u on u.id = es.user_id
        JOIN emma_plans ep on u.emma_plan_id = ep.emma_plan_id
        WHERE es.emergency_id = '".$eventID."'
    ");
    return $result;
}
function select_reportGeofences_fences($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT s.*, sp.fence_name AS main_fence_name, CONCAT(u.firstname,' ',u.lastname) AS created_name, p.name AS plan_name
        FROM emma_geofence_locations s 
        JOIN emma_plans p ON s.plan_id = p.emma_plan_id
        LEFT JOIN emma_geofence_locations sp ON s.main_fence = sp.id
        JOIN users u ON s.created_by = u.id
        WHERE s.plan_id = '". $planID ."'
    ");
    return $result;
}
function select_reportMassComunication_communications($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT emc.emma_mass_communication_id, emc.created_date, emc.notification, CONCAT(u.firstname , ' ' , u.lastname) as user_name, ep.name as plan_name
        FROM emma_mass_communications as emc
        JOIN users u on u.id = emc.created_by_id
        JOIN emma_plans ep on ep.emma_plan_id = emc.emma_plan_id
        WHERE emc.emma_plan_id = '".$planID."'
    ");
    return $result;
}
function select_reportSecurities_securities($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT s.*, t.name AS type_name, i.emma_site_name AS site_name, p.name AS plan_name, u.firstname AS created_first, u.lastname AS created_last, uc.firstname AS closed_first, uc.lastname AS closed_last
        FROM emma_securities s 
        JOIN emma_plans p ON s.emma_plan_id = p.emma_plan_id
        JOIN emma_security_types t ON s.emma_security_type_id = t.emma_security_type_id
        LEFT JOIN emma_sites i ON s.emma_site_id = i.emma_site_id
        Left JOIN users u ON s.created_by_id = u.id 
        LEFT JOIN users uc ON s.closed_by_id = uc.id
        WHERE s.emma_plan_id = '". $planID ."'
    ");
    return $result;
}
function select_reportUsers_users($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.*,concat(u.firstname , ' ' , u.lastname) as user_name, u.title, u.type, u.mobile_type, u.phone,u.company,u.emma_code_generator,u.address,u.city,u.country,u.emma_login_count,u.landline_phone,u.zip,u.title,u.ems_type, u.creation,u.emma_code_generator, concat(uc.firstname , ' ' , uc.lastname) as user_created_name, ep.name as emma_plan_name 
        FROM users u 
        JOIN emma_plans ep on ep.emma_plan_id = u.emma_plan_id
        LEFT JOIN users uc on uc.id = u.creator  
        WHERE u.active = 1
        AND u.emma_plan_id = '".$planID."'
        GROUP BY u.id
    ");
    return $result;
}
function select_closeSOS_soses($sosID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_sos
        WHERE emma_sos_id = '" . $sosID . "'
    ");
    return $result;
}
function select_addGeofenceMessage_users($username){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.id
        FROM users u 
        WHERE u.username = '". $username ."'
    ");
    return $result;
}
function select_createMassCommunication_getGroups($plans, $groupString){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.*
        FROM emma_groups g 
        WHERE g.emma_plan_id = '".$plans ."'
        AND g.name IN ('".implode("','",$groupString)."')
    ");
    return $result;
}
function select_topBar_multiples($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT p.*
        FROM emma_plans p
        LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id AND mp.active = 1
        WHERE mp.user_id = '" . $userID . "'
        AND p.date_expired > NOW()
    ");
    return $result;
}
function select_topBar_plansPlanGod(){
    global $emmadb;
    $result = $emmadb->query("
        SELECT p.*
        FROM emma_plans p
        WHERE p.date_expired > NOW()
        ORDER BY p.name
    ");
    return $result;
}
function select_topBar_plans($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT p.*
        FROM emma_plans p
        LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id AND mp.active = 1
        WHERE mp.user_id = '" . $userID . "'
        AND p.date_expired > NOW()
        ORDER BY p.name
    ");
    return $result;
}
//function select_db_users($username){
//    global $fvmdb;
//    $result = $fvmdb->query("
//        SELECT u.id, CONCAT(u.firstname, ' ', u.lastname) AS full_name, u.username, u.emma_plan_id, u.emma_pin, u.auth, g.admin
//        FROM users u
//        JOIN emma_user_groups ug on u.id = ug.user_id
//        JOIN emma_groups g on ug.emma_group_id = g.emma_group_id
//        WHERE u.auth = '" . $username . "'
//        ORDER BY g.admin DESC
//    ");
//    return $result;
//}
function select_resetDefaultPassword($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.password, u.username, u.id
        FROM users u 
        WHERE u.emma_plan_id = '". $planID ."' 
        AND u.password = 'password'
    ");
    return $result;
}
function select_loginAttempts($username){
    global $emmadb;
    $result = $emmadb->query("
        SELECT l.*
        FROM emma_login_log AS l
        INNER JOIN (
            SELECT username, MAX(login_datetime) AS newest FROM emma_login_log GROUP BY username
            ) AS t ON l.username = t.username AND t.newest = l.login_datetime
        WHERE l.username = '".$username."'
    ");
    return $result;
}
function select_event_eventID($eventID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.emma_plan_id
        FROM emergencies AS e
        WHERE e.emergency_id = '".$eventID."' 
    ");
    return $result;
}
function select_sosDetails_sosID($eventID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT s.plan_id AS emma_plan_id
        FROM emma_sos AS s
        WHERE s.emma_sos_id = '".$eventID."' 
    ");
    return $result;
}

function select_userGroupsWithAdmin_userID($userID, $planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_user_groups AS ug
        JOIN emma_groups AS g ON ug.emma_group_id = g.emma_group_id
        WHERE (g.admin = 1 OR g.`911admin` = 1)
        AND ug.user_id = '".$userID."'
        AND g.emma_plan_id = '".$planID."'
    ");
    return $result;
}

function select_userIsInAdminGroup($userID, $planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_user_groups AS ug
        JOIN emma_groups AS g ON ug.emma_group_id = g.emma_group_id
        WHERE g.admin = 1
        AND ug.user_id = '".$userID."'
        AND g.emma_plan_id = '".$planID."'
    ");
    return $result;
}

function select_userGroupsWithAdminonly_userID($userID, $planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_user_groups AS ug
        JOIN emma_groups AS g ON ug.emma_group_id = g.emma_group_id
        WHERE (g.admin = 1 OR g.security = 1)
        AND ug.user_id = '".$userID."'
        AND g.emma_plan_id = '".$planID."'
    ");
    return $result;
}

function select_distict_user_csv_files_in_last_24_hours (){
    global $emmadb;
    $result = $emmadb->query("
        SELECT csv.upload_id, csv.folder_name, csv.file_name, csv.user_id, csv.upload_datetime, u.username
        FROM user_csv_uploads AS csv
        JOIN users AS u ON csv.user_id = u.id
        WHERE csv.upload_datetime IN (
            SELECT MAX(upload_datetime)
            FROM user_csv_uploads
            GROUP BY folder_name)
        AND csv.upload_datetime > (NOW() - INTERVAL 24 HOUR)
        AND csv.has_run = 0
    ");
    return $result;
}
function select_distict_user_csv_exavault_files_in_last_10_hours (){
    global $emmadb;
    $result = $emmadb->query("
        SELECT csv.upload_id, csv.folder_name, csv.file_name, csv.user_id, csv.upload_datetime
        FROM user_csv_uploads AS csv
        WHERE csv.upload_datetime IN (
            SELECT MAX(upload_datetime)
            FROM user_csv_uploads
            GROUP BY folder_name)
        AND csv.upload_datetime > (NOW() - INTERVAL 24 HOUR)
        AND csv.has_run = 0
        AND csv.upload_ipaddress = 'smtp_mover'
    ");
    return $result;
}
function select_plans_with_user_folder($foldername){
    global $emmadb;
    $result = $emmadb->query("
        SELECT p.emma_plan_id, p.name
        FROM emma_plans AS p
        WHERE p.user_upload_folder = '".$foldername."'
        ORDER BY p.emma_plan_id
    ");
    return $result;
}
function select_users_in_plan($planid){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.*
        FROM users AS u
        JOIN emma_multi_plan AS p ON u.id = p.user_id
        WHERE p.plan_id = '".$planid."'
        ORDER BY u.id
    ");
    return $result;
}

function select_users_by_email($email){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.*
        FROM users AS u
        WHERE u.username = '".$email."'
    ");
    return $result;
}

function select_group_with_name_and_planid($group, $planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.emma_group_id
        FROM emma_groups AS g
        WHERE g.name = '".$group."'
        AND g.emma_plan_id = '".$planID."'
    ");
    return $result;
}
function select_user_with_multiplan($userID, $planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT p.*
        FROM emma_multi_plan AS p
        WHERE p.user_id = '".$userID."'
        AND p.plan_id = '".$planID."'
        AND p.active = 1
    ");
    return $result;
}
function select_user_groupList($userID, $planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.emma_group_id, g.name
        FROM emma_user_groups AS ug
        JOIN emma_groups AS g ON ug.emma_group_id = g.emma_group_id
        WHERE ug.user_id = '".$userID."'
        AND g.emma_plan_id = '".$planID."'
    ");
    return $result;
}
function select_user_plans_to_remove_from($userID, $planToKeep, $listOfPlans){
    global $emmadb;
    $result = $emmadb->query("
        SELECT mp.*
        FROM emma_multi_plan AS mp
        WHERE mp.user_id = '".$userID."'
        AND mp.plan_id NOT IN ('".implode(',', $planToKeep)."')
        AND mp.plan_id IN ('".implode(',', $listOfPlans)."')
        AND mp.active = 1
    ");
    return $result;
}
function select_user_plans_that_need_to_add($userID, $plansToAdd, $listOfPlans){
    global $emmadb;
    $result = $emmadb->query("
        SELECT mp.*
        FROM emma_multi_plan AS mp
        WHERE mp.user_id = '".$userID."'
        AND mp.plan_id NOT IN ('".implode(',', $plansToAdd)."')
        AND mp.plan_id IN ('".implode(',', $listOfPlans)."')
        AND mp.active = 1
    ");
    return $result;
}
function select_all_user_multiplan($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT mp.*
        FROM emma_multi_plan AS mp
        WHERE mp.user_id = '".$userID."'
        AND (mp.guest_code = '' OR mp.guest_code IS NULL)
        AND mp.active = 1
    ");
    return $result;
}
function select_csv_all_user_groups($foldername, $groupNameArray, $planNameArray){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.emma_group_id, g.name AS group_name, p.emma_plan_id, p.name AS plan_name
        FROM emma_groups AS g
        JOIN emma_plans AS p ON g.emma_plan_id = p.emma_plan_id
        WHERE p.user_upload_folder = '".$foldername."'
        AND g.name IN ('".implode("','", $groupNameArray)."')
        AND p.name IN ('".implode("','", $planNameArray)."')
    ");
    return $result;
}

function select_all_current_user_groups_in_plangroup($foldername, $userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.emma_group_id, g.name AS group_name, p.emma_plan_id, p.name AS plan_name
        FROM emma_user_groups AS ug
        JOIN emma_groups AS g ON ug.emma_group_id = g.emma_group_id
        JOIN emma_plans AS p ON g.emma_plan_id = p.emma_plan_id
        WHERE p.user_upload_folder = '".$foldername."'
        AND ug.user_id = '".$userID."'
    ");
    return $result;
}
function select_all_plan_users($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.username, u.id
        FROM emma_multi_plan AS mp
        JOIN  users AS u ON mp.user_id = u.id
        WHERE mp.plan_id = '".$planID."'
        AND (mp.guest_code = '' OR mp.guest_code IS NULL)
        AND u.active = 1 
        AND u.display = 'yes'
        AND mp.active = 1
    ");
    return $result;
}
function select_csv_upload($foldername, $fileName, $userID, $ip){
    global $emmadb;
    $result = $emmadb->query("
        SELECT upload_id
        FROM user_csv_uploads
        WHERE folder_name = '".$foldername."'
        AND file_name = '".$fileName."'
        AND user_id = '".$userID."'
        AND upload_ipaddress = '".$ip."'
        AND has_run = 1
        AND upload_datetime > NOW() - INTERVAL 30 SECOND
    ");
    return $result;
}

function select_csv_upload_not_run($foldername, $fileName, $userID, $ip){
    global $emmadb;
    $result = $emmadb->query("
        SELECT upload_id
        FROM user_csv_uploads
        WHERE folder_name = '".$foldername."'
        AND file_name = '".$fileName."'
        AND user_id = '".$userID."'
        AND upload_ipaddress = '".$ip."'
        AND has_run = 0
        AND upload_datetime > NOW() - INTERVAL 30 SECOND
    ");
    return $result;
}
function select_upload_options($foldername){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM user_csv_uploads_folder_options
        WHERE folder_name = '".$foldername."'
    ");
    return $result;
}

function select_all_users_in_plan_groups($planId){
    global $emmadb;
    $result = $emmadb->query("
        SELECT DISTINCT u.username, u.id
        FROM users AS u
        JOIN emma_user_groups AS ug ON u.id = ug.user_id
        JOIN emma_groups AS g ON ug.emma_group_id = g.emma_group_id
        WHERE g.emma_plan_id = '".$planId."'
    ");
    return $result;
}
function select_all_plan_users_distinct($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT DISTINCT u.username, u.id
        FROM emma_multi_plan AS mp
        JOIN  users AS u ON mp.user_id = u.id
        WHERE mp.plan_id = '".$planID."'
        AND (mp.guest_code = '' OR mp.guest_code IS NULL)
        AND u.active = 1 
        AND u.display = 'yes'
        AND mp.active = 1
    ");
    return $result;
}
function select_all_plan_names_and_ids(){
    global $emmadb;
    $result = $emmadb->query("
        SELECT emma_plan_id AS ID, name
        FROM emma_plans
    ");
    return $result;
}

function select_group_user_count($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT eg.name, eg.emma_group_id, COUNT(eug.user_id) AS userCount
        FROM emma_plans AS p
        JOIN emma_groups AS eg ON eg.emma_plan_id = p.emma_plan_id
        LEFT JOIN emma_user_groups eug on eg.emma_group_id = eug.emma_group_id
        WHERE p.emma_plan_id = ".$planID."
        GROUP BY eg.emma_group_id DESC;
    ");
    return $result;
}
function select_all_users_in_group($groupID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT u.*
        FROM users as u
        JOIN emma_user_groups eug on u.id = eug.user_id
        WHERE eug.emma_group_id = ".$groupID.";
    ");
    return $result;
}
function select_all_users_group_info($userID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT g.emma_group_id, g.name, p.emma_plan_id, p.name
        FROM emma_user_groups AS eug
        JOIN emma_groups AS g ON eug.emma_group_id = g.emma_group_id
        JOIN emma_plans AS p ON g.emma_plan_id = p.emma_plan_id
        WHERE eug.user_id = ".$userID.";
    ");
    return $result;
}




//-------------------------------------------------------------------------------------------------------------------------------------------------fvm
function select_closestAEDs($latitude, $longitude){
    global $fvmdb;
    $result = $fvmdb->query("
        SELECT
          a.aed_id,
          a.location_id,
          a.serialnumber as serial,
          c.name as contact,
          c.phone as contact_phone,
          m.model,
          m.mobile_image,
          b.brand,
          a.location as placement,
          l.name AS location,
          o.name AS organization,
          a.latitude,
          a.longitude,
          'public' as type, 
          ACOS(SIN(RADIANS(a.latitude)) * SIN(RADIANS('" . $latitude . "')) +
               COS(RADIANS(a.latitude)) * COS(RADIANS('" . $longitude .
            "')) * COS(RADIANS(a.longitude) - RADIANS('" . $longitude . "'))) *
          6380   AS 'distance'
        from aeds a
        join locations l on a.location_id = l.id
        JOIN organizations o ON l.orgid = o.id
        JOIN aed_models m on a.aed_model_id = m.aed_model_id
        JOIN aed_brands b on m.aed_brand_id = b.aed_brand_id
        LEFT JOIN contacts c on a.site_coordinator_contact_id = c.id
        WHERE l.display = 'yes'
        AND a.display = '1'
        AND a.approved = '1'
        order by distance
        limit 1
    ");
    return $result;
}
function select_assetsMap_aeds(){
    global $fvmdb;
    $result = $fvmdb->query("
        SELECT a.latitude,a.longitude,a.location,a.sitecoordinator,c.name as contact_name, c.company, loc.address
        FROM aeds a
        JOIN location_contacts lc on lc.location_id = a.location_id
        JOIN contacts c on c.id = lc.contact_id
        JOIN locations as loc on loc.id = a.location_id
        where a.current = 1
    ");
    return $result;
}
function select_call_aeds(){
    global $fvmdb;
    $result = $fvmdb->query("
        SELECT a.latitude,a.longitude,a.location,a.sitecoordinator,c.name as contact_name, c.company, loc.address
        FROM aeds a
        JOIN location_contacts lc on lc.location_id = a.location_id
        JOIN contacts c on c.id = lc.contact_id
        JOIN locations as loc on loc.id = a.location_id
        where a.current = 1
    ");
    return $result;
}
function select_callMap_aeds(){
    global $fvmdb;
    $result = $fvmdb->query("
        SELECT a.latitude,a.longitude,a.location,a.sitecoordinator,c.name as contact_name, c.company, loc.address
        FROM aeds a
        JOIN location_contacts lc on lc.location_id = a.location_id
        JOIN contacts c on c.id = lc.contact_id
        JOIN locations as loc on loc.id = a.location_id
        where a.current = 1
    ");
    return $result;
}
function select_events_aeds(){
    global $fvmdb;
    $result = $fvmdb->query("
        SELECT a.latitude,a.longitude,a.location,a.sitecoordinator,c.name as contact_name, c.company, loc.address
        FROM aeds a
        JOIN location_contacts lc on lc.location_id = a.location_id
        JOIN contacts c on c.id = lc.contact_id
        JOIN locations as loc on loc.id = a.location_id
        where a.current = 1
    ");
    return $result;
}
function select_map_aeds(){
    global $fvmdb;
    $result = $fvmdb->query("
        SELECT a.latitude,a.longitude,a.location,a.sitecoordinator,c.name as contact_name, c.company, loc.address
        FROM aeds a
        JOIN location_contacts lc on lc.location_id = a.location_id
        JOIN contacts c on c.id = lc.contact_id
        JOIN locations as loc on loc.id = a.location_id
        where a.current = 1
    ");
    return $result;
}
function select_userHome_aeds(){
    global $fvmdb;
    $result = $fvmdb->query("
        SELECT a.latitude,a.longitude,a.location,a.sitecoordinator,c.name as contact_name, c.company, loc.address
        FROM aeds a
        JOIN location_contacts lc on lc.location_id = a.location_id
        JOIN contacts c on c.id = lc.contact_id
        JOIN locations as loc on loc.id = a.location_id
        where a.current = 1
    ");
    return $result;
}

/* ---- Widget Chart Select Functions ---- */

/*
count number of mass communications- display them by year and by plan
*/

function select_massCommunications_widget($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT COUNT(*) as count, year(emc.created_date) AS year, month(emc.created_date) AS month
        FROM emma_mass_communications emc
        WHERE emma_plan_id = '". $planID ."'
        GROUP BY year(emc.created_date), month(emc.created_date)  
    ");
    return $result;
}

/*
 for each emma_mass_communication_id display the response_time_minutes.
*/


function select_massCommunicationsResponseTime_widget($planID){
    global $emmadb;
    $result = $emmadb->query("
    SELECT COUNT (response_time_minutes) as count, ep.name
    FROM emma_mass_communications emc
    JOIN emma_plans ep on emc.emma_plan_id = ep.name
    WHERE emma_plan_id = '" . $planID . "'
        
    ");
    return $result;
}



/*
    mass communication timeline widget
 */

function select_massCommunicationsTimeline_widget($emmaPlanId){
    global $emmadb;
    $result = $emmadb->query("
        SELECT emc.emma_mass_communication_id, emc.created_date, u.username, emc.emma_plan_id, emc.notification
        FROM emma_mass_communications emc
        JOIN users u on emc.created_by_id = u.id
        JOIN emma_plans ep on emc.emma_plan_id = ep.emma_plan_id
        WHERE emc.emma_plan_id IN (". implode(',',$emmaPlanId) .")
          
    ");
    return $result;
}

/*display number of SOS per plan */
/*
function select_sosPlan_widget($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT COUNT(*) as count, year(created_date) AS year, month(created_date) AS month
        FROM emma_sos
        WHERE plan_id = '". $planID ."'
        GROUP BY year(created_date), month(created_date)
    ");
   return $result;
}
*/

/* bar chart of totals for events, drills, mass notification, sos. */

function select_planstatistics_totals_widget($emmaPlanId) {
    global $emmadb;
    $result = $emmadb->query("
        SELECT et.name, count(*) AS count
        FROM emergencies e
        JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id 
        WHERE e.emma_plan_id IN (". implode(',',$emmaPlanId) .")
        GROUP BY e.emergency_type_id
    
    ");
    return $result;
}

function select_lockdown_notification($planId){
    global $emmadb;
    $scripts = $emmadb->query("
        SELECT e.notification
        FROM emma_lockdown_notifications e 
        WHERE e.plan_id = '". $planId ."'
    ");
    return $scripts;
}
function select_modified_lockdown_notification($planId){
    global $emmadb;
    $scripts = $emmadb->query("
        SELECT e.notification
        FROM emma_modified_lockdown_notifications e 
        WHERE e.plan_id = '". $planId ."'
    ");
    return $scripts;
}

function select_lockdown_drill_notification($planId){
    global $emmadb;
    $scripts = $emmadb->query("
        SELECT e.notification
        FROM emma_lockdown_drill_notifications e 
        WHERE e.plan_id = '". $planId ."'
    ");
    return $scripts;
}

function select_group_lockdown_permission($groupId){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM emma_lockdown_privileges
        WHERE receiving_group_id = '". $groupId ."'
    ");
    return $result;
}


function select_massCommunication_with_id($ID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT emc.*, p.name
        FROM emma_mass_communications emc
        JOIN emma_plans p ON emc.emma_plan_id = p.emma_plan_id
        WHERE emma_mass_communication_id = '". $ID ."'
    ");
    return $result;
}

function select_ar_with_id($id){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, u.username
        FROM emma_ar e
        LEFT JOIN users u ON e.closed_by_id = u.id
        WHERE e.emma_ar_id = '". $id ."'
    ");
    return $result;
}
function select_mar_with_id($id){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, u.username
        FROM emma_moss e
        LEFT JOIN users u ON e.closed_by_id = u.id
        WHERE e.emma_moss_id = '". $id ."'
    ");
    return $result;
}

function select_ar_with_multiplan($plans){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, p.name
        FROM emma_ar e
        JOIN emma_plans p ON e.plan_id = p.emma_plan_id
        WHERE e.plan_id IN (". implode(',', $plans) .")
        AND e.closed = 0
    ");
    return $result;
}
function select_mar_with_multiplan($plans){
    global $emmadb;
    $result = $emmadb->query("
        SELECT e.*, p.name
        FROM emma_moss e
        JOIN emma_plans p ON e.plan_id = p.emma_plan_id
        WHERE e.plan_id IN (". implode(',', $plans) .")
        AND e.closed = 0
    ");
    return $result;
}


function select_smpt_folders(){
    global $emmadb;
    $result = $emmadb->query("
        SELECT folder_name
        FROM user_csv_uploads_folder_options
        WHERE exavault_user = 1
    ");
    return $result;

}

function select_nsw_locations(){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM nws_zone_lookup
    ");
    return $result;
}
function select_nsw_county_severe_status($county){
    global $emmadb;
    $result = $emmadb->query("
        SELECT *
        FROM nws_current_weather_status
        WHERE zone_id = '".$county."'
        AND severe_weather = 1
    ");
    return $result;
}

function select_default_notification_types_with_plan($planID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT notificationType_appNotification, notificationType_email, notificationType_SMStext
        FROM emma_plan_settings
        WHERE plan_id = '".$planID."'
    ");
    return $result;
}

function select_get_qrcodes($userID){
    global $emmadb;
    $emmaplans = select_multiPlans_with_userID($userID);
    while($emmaplan = $emmaplans->fetch_assoc()){
        $emmaplanarr[] = $emmaplan['plan_id'];
    }
    $result = $emmadb->query("
        SELECT q.*, p.name as planname, g.name as groupname
        FROM emma_guest_qr_codes q
        JOIN emma_plans p ON q.plan_id = p.emma_plan_id
        join emma_groups g ON q.group_id = g.emma_group_id
        WHERE q.active = 1
        and q.plan_id IN ('" . implode(',', $emmaplanarr) . "')
        order by p.name, g.name
    ");
    return $result;
}



function select_get_qrcodes_plans_groups($plan,$group){
    global $emmadb;
    $result = $emmadb->query("
        SELECT q.*
        FROM emma_guest_qr_codes q
        WHERE q.active = 1
        AND q.plan_id = '". $plan ."'
        AND q.group_id = '". $group ."'
    ");
    return $result;
}


function select_users_in_account_plan($accountName){
    global $emmadb;
    $result = $emmadb->query("
        SELECT Distinct emp.user_id
        FROM emma_multi_plan AS emp
        JOIN emma_plans AS p ON emp.plan_id = p.emma_plan_id
        JOIN users AS u ON emp.user_id = u.id
        WHERE p.user_upload_folder = '".$accountName."'
        AND emp.active = 1
        AND u.active = 1
    ");
    return $result;
}
function select_user_plans_within_account($userid, $accountName){
    global $emmadb;
    $result = $emmadb->query("
        SELECT Distinct p.name
        FROM emma_multi_plan AS emp
        JOIN emma_plans AS p ON emp.plan_id = p.emma_plan_id
        WHERE emp.user_id = '".$userid."'
        AND p.user_upload_folder = '".$accountName."'
        AND emp.active = 1
    ");
    return $result;
}
function select_user_groups_within_account($userid, $accountName){
    global $emmadb;
    $result = $emmadb->query("
        SELECT Distinct eg.name
        FROM emma_user_groups AS eug
        JOIN emma_groups eg on eug.emma_group_id = eg.emma_group_id
        JOIN emma_plans AS p ON eg.emma_plan_id = p.emma_plan_id
        WHERE eug.user_id = '".$userid."'
        AND p.user_upload_folder = '".$accountName."'
    ");
    return $result;
}
function select_users_in_account_plan_with_no_actives($accountName){
    global $emmadb;
    $result = $emmadb->query("
        SELECT Distinct emp.user_id
        FROM emma_multi_plan AS emp
        JOIN emma_plans AS p ON emp.plan_id = p.emma_plan_id
        JOIN users AS u ON emp.user_id = u.id
        WHERE p.user_upload_folder = '".$accountName."'
        AND emp.active = 0
        AND u.active = 1
        AND emp.plan_id NOT IN (
            SELECT plan_id
            FROM emma_multi_plan AS amp
            JOIN emma_plans AS ap ON amp.plan_id = ap.emma_plan_id
            WHERE p.user_upload_folder = '".$accountName."'
            AND amp.active = 1
            AND amp.user_id = u.id
        )
    ");
    return $result;
}

function select_multiplan_with_user_and_not_active($userID, $planNeededID){
    global $emmadb;
    $result = $emmadb->query("
        SELECT Distinct emp.user_id
        FROM emma_multi_plan AS emp
        WHERE emp.plan_id = '".$planNeededID."'
        AND emp.active = 0
        AND emp.user_id = '".$userID."'
        AND emp.plan_id NOT IN (
            SELECT plan_id
            FROM emma_multi_plan AS amp
            WHERE amp.plan_id = '".$planNeededID."'
            AND amp.active = 1
            AND amp.user_id = emp.user_id
        )
    ");
    return $result;
}

?>