<?php
/**
 * Created by PhpStorm.
 * User: COnrad Ullrich
 * Date: 08/15/2019
 * Time: 11:31 AM
 */

use Firebase\JWT\JWT;

session_start();
include('include/db.php');
require('vendor/php-jwt-master/src/JWT.php');
require('vendor/php-jwt-master/src/BeforeValidException.php');
require('vendor/php-jwt-master/src/ExpiredException.php');
require('vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('config/config.json'));

if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != "on") {
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    exit();
}

$errorMessage = '';
if (isset($_COOKIE['jwt'])) {
    try {
        $token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key,
            array('HS512'));
        header('Location: dashboard.php');
    } catch (\Firebase\JWT\BeforeValidException $bve) {
        $errorMessage = $bve->getMessage();
    } catch (\Firebase\JWT\ExpiredException $ee) {
        $errorMessage = $ee->getMessage();
    } catch (\Firebase\JWT\SignatureInvalidException $sie) {
        $errorMessage = $sie->getMessage();
    } catch (Exception $e) {
        $errorMessage = $e->getMessage();
    }
}
//login with token from emma admin
if (isset($_GET['token'])) {

    $loginToken = $fvmdb->real_escape_string($_GET['token']);

    $users = login_with_token($loginToken);
    if ($users->num_rows > 0) {
        $user = $users->fetch_assoc();

        $admin = false;
        $security = false;
        $plansecurity = false;
        $groups = select_groups_with_userID_and_planID($user['id'],$user['token_plan']);
        while ($group = $groups->fetch_assoc()) {
            if ($group['admin']) $admin = true;
            if ($group['security']) $security = true;
            if ($group['geofences']) $geofences = true;
            if ($group['911admin']) $admin911 = true;
            if ($group['securities']) $plansecurity = true;
            if ($group['panic']) $plansos = true;
        }

        //      if ($admin || $security) {
        $tokenId = base64_encode(randomToken(32));
        $issuedAt = time();
        $notBefore = $issuedAt;
        $expire = $notBefore + (10 * 365 * 24 * 60 * 60);
        $serverName = 'tsdemos.com';

        $data = [
            'iat' => $issuedAt,
            'jti' => $tokenId,
            'iss' => $serverName,
            'nbf' => $notBefore,
            'exp' => $expire,
            'data' => [
                'id' => $user['id'],
                'full_name' => $user['full_name'],
                'username' => $user['username'],
                'emma_plan_id' => $user['emma_plan_id'],
                'emma_pin' => $user['emma_pin'],
                'auth' => $user['auth'],
                'privilege' => [
                    'admin' => $admin,
                    'security' => $security,
                    'plansecurity' => $plansecurity,
                    'plansos' => $plansos,
                    'geofences' => $geofences,
                    'admin911' => $admin911,
                    'code_generator' => $user['emma_code_generator'] == 1,
                    'plans' => $user['emma_plans'] == 1,
                    'plan_god' => $user['plan_god'] == 1,
                    'plan_emails' => $user['plan_emails'] == 1,
                    'plan_texts' => $user['plan_texts'] == 1
                ]
            ]
        ];

        $jwt = Firebase\JWT\JWT::encode($data, $CONFIG->key, 'HS512');
        setcookie('jwt', $jwt, time() + (9 * 60 * 60), '/');

        if (isset($_GET['content'])) {
            header('Location: dashboard.php?content=' . $_GET['content']);
        } else {
            header('Location: dashboard.php');
        }


    }
}
if (isset($_POST['login']) && !empty($_POST['username'])) {
    try {
        $username = $fvmdb->real_escape_string($_POST['username']);
        $password = $fvmdb->real_escape_string($_POST['password']);
        $users = login_with_username_and_password($username, $password);
        if ($users->num_rows > 0) {
            $user = $users->fetch_assoc();

            $admin = false;
            $security = false;
            $plansecurity = false;
            $groups = select_groups_with_userID($user['id']);
            while ($group = $groups->fetch_assoc()) {
                if ($group['admin']) $admin = true;
                if ($group['security']) $security = true;
                if ($group['geofences']) $geofences = true;
                if ($group['911admin']) $admin911 = true;
                if ($group['securities']) $plansecurity = true;
                if ($group['panic']) $plansos = true;
            }

//      if ($admin || $security) {
            $tokenId = base64_encode(randomToken(32));
            $issuedAt = time();
            $notBefore = $issuedAt;
            $expire = $notBefore + (10 * 365 * 24 * 60 * 60);
            $serverName = 'tsdemos.com';

            $data = [
                'iat' => $issuedAt,
                'jti' => $tokenId,
                'iss' => $serverName,
                'nbf' => $notBefore,
                'exp' => $expire,
                'data' => [
                    'id' => $user['id'],
                    'full_name' => $user['full_name'],
                    'username' => $user['username'],
                    'emma_plan_id' => $user['emma_plan_id'],
                    'emma_pin' => $user['emma_pin'],
                    'auth' => $user['auth'],
                    'privilege' => [
                        'admin' => $admin,
                        'security' => $security,
                        'plansecurity' => $plansecurity,
                        'plansos' => $plansos,
                        'geofences' => $geofences,
                        'admin911' => $admin911,
                        'code_generator' => $user['emma_code_generator'] == 1,
                        'plans' => $user['emma_plans'] == 1,
                        'plan_god' => $user['plan_god'] == 1,
                        'plan_emails' => $user['plan_emails'] == 1,
                        'plan_texts' => $user['plan_texts'] == 1
                    ]
                ]
            ];

            $jwt = Firebase\JWT\JWT::encode($data, $CONFIG->key, 'HS512');
            setcookie('jwt', $jwt, time() + (9 * 60 * 60), '/');
//      } else {
//        $errorMessage = 'User is not in an Admin or Security group';
//      }
            $events = select_emergencies_with_planID($user['emma_plan_id']);
            if ($event = $events->fetch_assoc()) {
                header('Location: dashboard.php?content=event&id=' . $event['emergency_id']);
            } else {
                header('Location: dashboard.php');
            }
        } else {
            $errorMessage = 'Invalid Username/Email or Password';
        }
    } catch (Exception $e) {
        $e->getMessage();
    }
} elseif (isset($_POST['login']) && empty($_POST['username'])) {
    $errorMessage = 'Invalid Username/Email or Password';
}
?>
<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Login</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #FFFFFF;">


<?php include('include/index_top_bar.php'); ?>

<div class="row expanded the-clock" style="height: 100%">
    <div class="large-12 medium-12 small-12 columns" style="margin: 0 auto;">

        <br />
        <br />
        <br />
        <p class="MMM-header" style="text-align: center;">COMING SOON</p>
        <br />
        <br />
        <br />

   <br />
    </div>
<?php include('include/footer.php'); ?>

</div>
<?php include('include/pre_login_modals.php'); ?>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>


