<?php include('include/pre_login_header.php'); ?>
<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Login</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #FFFFFF;">


<?php include('include/index_top_bar.php'); ?>

<div class="row expanded the-clock" style="height: 100%">
    <div class="large-12 medium-12 small-12 columns" style="padding-top: 2em;">
        <div class="row expanded" style="padding-bottom: 1em">

            <div class="large-6 medium-12 small-12 columns">
                <div class="row expanded" style="padding-bottom: 1em">
                    <div class="large-offset-2 large-4 medium-4 small-4 column">
                        <div class="clear_card">
                            <a id="management-button"><i class="fas fa-tasks fa-3x"></i>
                            <div class="card_section">
                                <p>Management<br/>Features</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="large-4 medium-4 small-4 column">
                        <div class="clear_card">
                            <a id="notification-button"><i class="fas fa-comments fa-3x"></i>
                            <div class="card_section">
                                <p>Notification<br/>Features</p>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row expanded" style="padding-bottom: 1em">
                    <div class="large-offset-2 large-4 medium-4 small-4 column">
                        <div class="clear_card">
                            <a id="personal-button"><i class="fas fa-user-plus fa-3x"></i>
                            <div class="card_section">
                                <p>Personal<br/>Safety<br/>Features</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="large-4 medium-4 small-4 column">
                        <div class="clear_card">
                            <a id="public-button"><i class="fas fa-users fa-3x"></i>
                            <div class="card_section">
                                <p>Public<br/>Safety<br/>Features</p>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="large-6 medium-12 small-12 columns">
                <div class="row expanded" style="padding-bottom: 1em">
                    <div id="management-feature-content" hidden
                         class="large-12 medium-12 small-12 column button-content">
                        <div class="description_card">
                            <div class="row expanded">
                                <P class="feature_header" style="text-align: center">Management Features</P>
                            </div>
                            <div class="row expanded">
                                <div class="large-6 medium-12 small-12 column">
                                    <ul class="feature_text">
                                        <li>Multi-Channel Platform Notifications</li>
                                        <li>Users Assigned Rights & Privileges</li>
                                        <li>Groups Assigned Rights & Privileges</li>
                                        <li>Specific Group Notification Friendly</li>
                                        <li>Instant Push Notification Capable (off server)</li>
                                        <li>Information Only Notification Alerts</li>
                                        <li>Two-Way PC & Mobile Notification Alerts</li>
                                        <li>See Something Say Something Admin Friendly</li>
                                        <li>E-Security Software Applications</li>
                                        <li>Emergency Management Incident Command Use</li>
                                        <li>Automated Security System Call Logs</li>
                                        <li>GIS Asset Mapping Integration</li>
                                        <li>Hot Spot Quick Identification</li>
                                        <li>Automated User Directory Updates</li>
                                        <li>Data Archiving 24/7/365</li>
                                    </ul>
                                </div>
                                <div class="large-6 medium-12 small-12 column">
                                    <ul class="feature_text">
                                        <li>Full Reporting Accessibility</li>
                                        <li>Resource Management Portal 24/7/365</li>
                                        <li>Blue Print /Floor Plan Evac Map Quick Access</li>
                                        <li>911 Incident Command Compatibility</li>
                                        <li>911 CAD System Compatibility</li>
                                        <li>Private Dispatch / Security System Compatibility</li>
                                        <li>Emergency Drill Execution System</li>
                                        <li>Event/Drill Timeline Mapping and Charting</li>
                                        <li>Geofencing Capable</li>
                                        <li>Unlimited User Options</li>
                                        <li>Quick User / Guest User Options</li>
                                        <li>Anti-Bullying or Threat Prevention Features</li>
                                        <li>Open API / Software Development Kit (SDK) Friendly</li>
                                        <li>Automatic Wireless Emergency Alert Feed Capable</li>
                                        <li>Customizable</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="notification-feature-content" hidden
                         class="large-12 medium-12 small-12 column button-content">
                        <div class="description_card">
                            <div class="row expanded">
                                <P class="feature_header" style="text-align: center">Notification Features</P>
                            </div>
                            <div class="row expanded">
                                <div class="large-6 medium-12 small-12 column">
                                    <ul class="feature_text">
                                        <li>Multi-Channel Platform Notifications</li>
                                        <li>Specific Group Notifications</li>
                                        <li>Rights & Privilege Based Features</li>
                                        <li>Specific Group Two-Way Communication</li>
                                        <li>1-to-1 or 1-to-Many Capable</li>
                                        <li>Information Only Notification Alerts</li>

                                    </ul>
                                </div>
                                <div class="large-6 medium-12 small-12 column">
                                    <ul class="feature_text">
                                        <li>Two-Way PC & Mobile Notification Alerts</li>
                                        <li>Immediate EMMA SOS! Alerting</li>
                                        <li>Notification to Text 911 Integration</li>
                                        <li>Automatic Wireless Emergency Alert Capable</li>
                                        <li>Customizable</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="personal-safety-feature-content" hidden
                         class="large-12 medium-12 small-12 column button-content">
                        <div class="description_card">
                            <div class="row expanded">
                                <P class="feature_header" style="text-align: center">Personal Safety Features</P>
                            </div>
                            <div class="row expanded">
                                <div class="large-6 medium-12 small-12 column">
                                    <ul class="feature_text">
                                        <li>911 Quick Location Reference</li>
                                        <li>911 Quick Call Enabled</li>
                                        <li>911 Call Time/Date Reporting</li>
                                        <li>In-App GPS</li>
                                        <li>EMMA SOS!</li>

                                    </ul>
                                </div>
                                <div class="large-6 medium-12 small-12 column">
                                    <ul class="feature_text">
                                        <li>Real-time Notifications</li>
                                        <li>Real-time Notification Responses</li>
                                        <li>See Something Say Something Quick Reporting</li>
                                        <li>Automatic Wireless Emergency Alert Capable</li>
                                        <li>Customizable</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="public-safety-feature-content" hidden
                         class="large-12 medium-12 small-12 column button-content">
                        <div class="description_card">
                            <div class="row expanded">
                                <P class="feature_header" style="text-align: center">Public Safety
                                    Features</P>
                            </div>
                            <div class="row expanded">
                                <div class="large-6 medium-12 small-12 column">
                                    <ul class="feature_text">
                                        <li>Geofencing Capable</li>
                                        <li>Automatic Wireless Emergency Alert Capable</li>
                                        <li>911 Quick Location Reference</li>
                                        <li>911 Quick Call Enabled</li>
                                        <li>911 Call Time/Date Reporting</li>
                                        <li>In-App GPS</li>
                                        <li>EMMA SOS!</li>
                                        <li>Real-time Notifications</li>
                                        <li>Real-time Notification Responses</li>
                                        <li>See Something Say Something Quick Reporting</li>

                                    </ul>
                                </div>
                                <div class="large-6 medium-12 small-12 column">
                                    <ul class="feature_text">
                                        <li>Quick User / Guest User Options</li>
                                        <li>Mobile Device Friendly & Compatible</li>
                                        <li>1-to-1 or 1-to-Many Capable</li>
                                        <li>Unlimited User Options</li>
                                        <li>Quick User / Guest User Options</li>
                                        <li>Anti-Bullying or Threat Prevention Features</li>
                                        <li>Open API / Software Development Kit (SDK) Friendly</li>
                                        <li>911 Incident Command Compatibility</li>
                                        <li>911 CAD System Compatibility</li>
                                        <li>Customizable</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div><p>&nbsp;</p><br/><br/><br/><br/><br/><br/><br/></div>
            </div>

            <?php include('include/footer.php'); ?>

            <?php include('include/pre_login_modals.php'); ?>

</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>



