<?php



?>
<html>
    <head>
        <style>
            html, body {
                height: 100%;
                width: 100%;
            }
            body {
                display:flex;
                flex-direction: column;
                align-items: center;
                font-family: "Lato";
            }
            main {
                display:flex;
                justify-content: space-between;
                width:80%;
                margin-left: auto;
                margin-right:auto;
                margin-top: 2em;
            }
            main > div {
                display:flex;
                flex-direction: column;
                align-items: center;
            }
            span {
                font-size: 110%;
                letter-spacing: 0.01em;
                margin-bottom: 0.5em;
            }
            h2 {
                opacity: 0.7;
                text-transform: uppercase;
                font-size: 150%;
                font-family: "Roboto";
            }
            textarea {
                height: 30em;
                width: 40em;
                border: 1px solid rgba(0, 0, 0, 0.2);
            }
            #send {
                padding: 0.9em;
                width: 9em;
                color: white;
                margin-top: 1em;
                background: #1f7dc4;
                text-align: center;
                box-shadow: 0px 0px 1px rgba(0, 0, 0, 0.9);
                text-shadow: 0px 0px 3px rgba(0, 0, 0, 0.9);
                cursor: pointer;
            }
            #log {
                display:flex;
                flex-direction: column;
                margin-top: 1em;
                width: 80%;
            }
            #log b {
                margin-bottom: 1em;
            }
            .numberinput {
                width: 18em;
                border: 1px solid rgba(0, 0, 0, 0.3);
                margin-bottom: 0.6em;
                box-shadow: 0px 0px 1px rgba(0, 0, 0, 0.6);
                height: 1.8em;
            }
        </style>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    </head>
    <body>
        <h2> Bulk sms send </h2>
        <main>
            <div> <span> Enter Numbers (one per line, format 1676353728) </span>
                <textarea id="numbers"></textarea>
            </div>
            <div>
                <span>Request rate (ms)</span>
                <input type="text" id="rate" value=1000>
            </div>
            <div>
                <span> Message </span>
                <textarea id="message"></textarea>
            </div>
        </main>
        <span id="send">Send message</span>
        <div id="log">
            <b> Log </b>
        </div>
    </body>
    <script>
        var rate = 1000;
        var numbers;
        var message;
        var loop;
        var c = 0;
        $(function() {
            $("#rate").keyup(function() {
                rate = $(this).val();
            })
            $("#send").click(function() {
                c = 0;
                numbers = $("#numbers").val().trim().split("\n");
                message = $("#message").val();
                send();
                console.log(numbers);
            })
        })
        function send() {
            var n = numbers[c].trim();
            $.ajax({
                type: "POST",
                url: "https://igotcut.com",
                data: { number: n, message:  message },
                success: function(res) {
                    console.log(res);
                    log("Success : message '" + message + "'' delivered to " + n);
                    c++;
                    if(c < numbers.length){
                        setTimeout(send, rate);
                    }
                },
                error: function (res) {
                    log("Failure : " + res.message);
                }
            });
        }
        function log(msg) {
            $("#log").append($("<span></span>").html(msg));
        }
    </script>
</html>