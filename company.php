<?php include('include/pre_login_header.php'); ?>
<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Login</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #FFFFFF;">


<?php include('include/index_top_bar.php'); ?>

<div class="row expanded the-clock">
    <div class="large-12 medium-12 small-12 columns" style="padding-top: 2em;">
        <div class="row expanded" style="padding-bottom: 1em">
            <div class="large-6 medium-12 small-12 columns" style="padding-top: 2em">
                <p class="company_description">Think Safe, Inc is a manufacturer, developer and leading
                    integrator in the first aid industry that remains committed to setting new standards for
                    comprehensive
                    emergency readiness and response. At Think Safe we have been innovating with the end user in mind
                    every step
                    of the way. Our goal remains of simplifying emergency programs to a point where accuracy is
                    ensured, equipment is easily maintained, lives are saved and costs are reduced.</p><br/>

                <p class="company_description">Think Safe technology includes innovative patented products such as the
                    Emergency
                    Instruction Device (EID)
                    and Self-contained Emergency Treatment (SET) Systems. Think Safe is also a software developer for
                    first aid
                    technology, with over 20 software programs developed to improve access to training or treatment
                    instructions
                    and ensure proper first aid or emergency equipment upkeep.</p><br/>

                <p class="company_description">Think Safe also recently developed the patented
                    EMMA (Emergency Management Mobile App) application to solve critical communication problems that
                    existed in
                    the continuum of care before the arrival of 911 dispatched professional rescuers. Think Safe
                    provides bulk
                    pricing synergy advantages over traditional AED or emergency equipment dealers due to our wholesaler
                    arrangement on all brands/makes/models of Automated External Defibrillators (AEDs), evacuation
                    systems or
                    other critical emergency equipment.</p><br/>

                <p class="company_description">Think Safe is also an established training center and operates the First
                    Voice Training Network infrastructure that has over 500 instructors in the United States plus other
                    countries. Think Safe has AED program management solutions, medical oversight services, CPR/First
                    Aid/Safety-related training (online, blended or in-person) services and total emergency prevention,
                    treatment or servicing solutions that are available a la carte or integrated to meet the client's
                    goals and
                    needs.
                </p>

                <div class="row expanded ind_buttons" style="padding-top: 2em">

                    <div class="large-4 medium-12 small-12 column">
                        <div class="clear_card">
                            <a target="_blank" href="documents/Company_Bio_Think_Safe_9.2019.pdf"><img
                                        src="img/Company_Bio_Think_Safe_9-9-19_Page_1.jpg" width="50%" height="50%"/>
                            </a>
                            <div class="card_section">
                            <p>Company<br />Bio</p>
                            </div>
                        </div>
                    </div>
                    <div class="large-4 medium-12 small-12 column" style="text-align: center; margin-top: 5em">
                        <a href="company_learn_more.php" class="button2">Learn
                            More</a>
                    </div>
                    <div class="large-4 medium-12 small-12 column">
                        <div class="clear_card">
                            <a target="_blank" href="documents/DELUXE_DEALER_PRODUCTS_92018_FINAL_v1.pdf"><img
                                        src="img/DELUXE_DEALER_PRODUCTS_92018_FINAL_v1_Page_01.jpg" width="50%" height="50%"/>
                            </a>
                                <div class="card_section">
                                    <p>Deluxe Product<br/>Catalog</p>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div><p>&nbsp;</p><br/><br/><br/><br/><br/><br/><br/></div>
</div>
<?php include('include/footer.php'); ?>
<?php include('include/pre_login_modals.php'); ?>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>


