<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 10/23/2017
 * Time: 12:16 PM
 */

require('../include/db.php');
include('../include/processing.php');
include('swiftmailer/swift_required.php');

$data = array();
$errors = array();

$userId = $fvmdb->real_escape_string($_POST['user-id']);
$planId = $fvmdb->real_escape_string($_POST['plan-id']);
$notification = $fvmdb->real_escape_string($_POST['notification']);
$allowresponse = $fvmdb->real_escape_string($_POST['mass-notification-responses']);
$responsetime = $fvmdb->real_escape_string($_POST['mass-notification-responses-count']);
$groups = $_POST['groups'];

if($allowresponse == 'on'){
    $allowresponse = true;
}else{
    $allowresponse = false;
}

    $users = select_user_from_userID($userId);
//    $fvmdb->query("
//    SELECT *
//    FROM users
//    WHERE id = '" . $userId . "'
//");
    if (!$user = $users->fetch_assoc()) {
        $errors['user'] = 'Can not find user';
    } else {
        $FcmServerKey = 'AAAArNckjhA:APA91bFI8gqsspQJjUkzmmPdv1vUWzAcmbXu6CwgkL5yHLOYdkN9zm9R-uqglQxzq0Yi7Fx1aBmd3ra48sjRx13t2Wo1ZCD_ViyOpJGi_52mac_2uhDWSTgQP1TnlXi7aiNJTblLq6Db';
        $topics = '/topics/emma-' . $user['emma_plan_id'];
    }

if (empty($errors)) {
    $insertMassCommunication = insert_emma_mass_communications($planId, $userId, $notification, $allowresponse, $responsetime);
//    $fvmdb->query("
//    INSERT INTO emma_mass_communications (
//      emma_plan_id,
//      created_by_id,
//      created_date,
//      notification
//    ) VALUES (
//      '" . $planId . "',
//      '" . $userId . "',
//      '" . date('Y-m-d H:i:s') . "',
//      '" . $notification . "'
//    )
//  ");
    $messageId = $emmadb->insert_id;
    $postData = array(
        'to' => $topics,
        'time_to_live' => 900,
        'content_available' => true,
        'data' => array(
            'GROUP_ALERT' => 'true',
            'MASS_COMMUNICATION' => true,
            'mass_communication_id' => $messageId,
            'event' => 'Mass Communication',
            'description' => $notification,
            'sound' => true,
            'vibrate' => true,
            'groups' => array(),
        )
    );
    foreach ($groups as $group) {
        $postData['data']['groups'][] = $group;
        $IOSTopics[] = "'emma-plan" . $user['emma_plan_id'] . "-" . $group ."' in topics";
    }

    $ch = curl_init('https://fcm.googleapis.com/fcm/send');
    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Authorization: key=' . $FcmServerKey,
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postData)
    ));
    $response = curl_exec($ch);
    $responseData = json_decode($response);
    $data['response'] = $responseData;

    /////////////////////////////
    //    IOS NOTIFICATIONS    //
    /////////////////////////////

    $IOSTopicsString = $IOSTopics[0];
    if(count($IOSTopics) > 0){
        for($i = 1; ($i < 5 && $i < count($IOSTopics)); $i++){
            $IOSTopicsString .= ' || ' . $IOSTopics[$i];
        }
    }

    $data['iosTopicString'] = $IOSTopicsString;
    $data['iosTopics'] = $IOSTopics;

    $postIOSData = array(
        'condition' => $IOSTopicsString,
        'time_to_live' => 600,
        'priority'=>'high',
        'notification' => array(
            'title' => $notification,
            'sound' => 'notification.aiff',
            'badge' => '1'
        ),
        'data' => array(
            'GROUP_ALERT' => 'true',
            'event' => 'Mass Communication',
            'mass_communication_id' => $messageId,
            'description' => $notification,
            'MASS_COMMUNICATION' => true,
        )
    );

    $ch = curl_init('https://fcm.googleapis.com/fcm/send');
    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Authorization: key=' . $FcmServerKey,
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postIOSData)
    ));
    $IOSresponse = curl_exec($ch);

    if(count($IOSTopics) > 5){
        $m = 5;
        while($m < count($IOSTopics)){
            for($i = $m; ($i < ($m + 5) && $i < count($IOSTopics)); $i++){
                if($i == $m){
                    $IOSTopicsString = $IOSTopics[$i];
                }
                else {
                    $IOSTopicsString .= ' || ' . $IOSTopics[$i];
                }
            }

            $postIOSData2 = array(
//        'to' => $IOSTopicsString,
                'condition' => $IOSTopicsString,
                'time_to_live' => 600,
                'priority'=>'high',
                'notification' => array(
                    'title' => $notification,
                    'sound' => 'notification.aiff',
                    'badge' => '1'
                ),
                'data' => $postIOSData['data']
            );

            $ch = curl_init('https://fcm.googleapis.com/fcm/send');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: key=' . $FcmServerKey,
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postIOSData2)
            ));
            $IOSresponse2 = curl_exec($ch);

            $m += 5;
        }
    }



}
$data['post'] = $_POST;
$data['errors'] = $errors;
$data['success'] = empty($errors);
echo json_encode($data);