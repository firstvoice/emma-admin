<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 9/19/2019
 * Time: 9:14 AM
 */

include('include/pre_login_header.php');
$token = $fvmdb->real_escape_string($_GET['token']);
$parent = false;

$planTokens = select_plan_from_token($token);
$t = $planTokens->fetch_assoc();

if($t['students'] == 1){
    $parent = true;
}
?>
<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Login</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #FFFFFF;">


<div class="top-bar" style="border-bottom: 2px solid #0079c1;">
    <div class="row expanded">
        <div class="large-1 medium-2 small-12 columns">
            <a href="index.php"><img src="img/emma_logo.png"></a>
        </div>
        <div class="large-2 medium-6 small-6 columns">
            <div class="row">
                <a href="emma@think-safe.com"><span><b>emma@think-safe.com</b></span></a>
            </div>
            <div class="row" style="padding-top:1em">
                <a href="tel:888-473-1777"><b>888-473-1777</b></a>
            </div>
        </div>
    </div>
</div>

<div class="row expanded the-clock" style="height: 100%">
    <div class="large-12 medium-12 small-12 columns">
        <div class="row expanded">
            <div class="large-12 medium-12 small-12 columns text-center">
                <h2 style="color: #0078C1"><?php echo $t['title'];?></h2>
            </div>
        </div>
        <div class="row" style="margin-top: 3%; margin-bottom: 1%;">
            <div class="large-7 medium-10 small-12 columns" style="border: 1px solid grey; border-radius: 1em; background-color: white; margin-left: 2%">
                <div class="row expanded">
                    <div class="large-12 medium-12 small-12 columns">
                        <p style="color: grey; font-size:22px"><?php echo $t['description'];?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="large-7 medium-10 small-12 columns" style="border: 1px solid grey; border-radius: 1em; background-color: white; margin-left: 2%">
                <form id="user_signup_form" method="post">
                    <div class="row expanded">
                        <div class="large-12 medium-12 small-12 columns text-center">
                            <h4 style="color: #0078C1">Register</h4>
                        </div>
                    </div>
                    <div class="row expanded">
                        <input type="hidden" name="plan" value="<?php echo $t['plan_id']?>">
                        <input type="hidden" name="group" value="<?php echo $t['group_id']?>">
                        <div class="large-6 medium-12 small-12 columns">
                            <label style="color: grey;">First Name</label>
                            <input type="text" name="first" placeholder="First Name">
                        </div>
                        <div class="large-6 medium-12 small-12 columns">
                            <label style="color: grey;">Last Name</label>
                            <input type="text" name="last" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="row expanded">
                        <div class="large-4 medium-12 small-12 columns">
                            <label style="color: grey;">Email</label>
                            <input type="text" name="email" placeholder="Email">
                        </div>
                        <div class="large-4 medium-12 small-12 columns">
                            <label style="color: grey;">Mobile</label>
                            <input type="text" name="mobile" placeholder="Email">
                        </div>
                        <div class="large-4 medium-12 small-12 columns">
                            <label style="color: grey;">Landline</label>
                            <input type="text" name="landline" placeholder="Phone">
                        </div>
                    </div>
                    <?php if($parent){?>
                        <div class="row expanded children">
                            <div class="large-12 medium-12 small-12 columns text-center">
                                <h4 style="color: #0078C1">Students</h4>
                            </div>
                        </div>
                        <div class="row expanded children" id="child-container">
                            <div class="large-11 medium-11 small-11 columns">
                                <label style="color: grey">Name</label>
                                <input type="text" class="childname" id="childname1" name="childname[]" placeholder="Name">
                            </div>
                            <div class="large-1 medium-1 small-1 columns">
                                <a id="add-child-button"><span class="fa fa-plus"></span></a>
                            </div>
                        </div>
                    <?php }?>
                    <div class="row expanded">
                        <div class="large-4 medium-6 small-12 columns">
                            <input type="submit" class="button expanded radius" value="Submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include('include/footer.php'); ?>
<?php include('include/pre_login_modals.php'); ?>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>
<script src="js/user_signup.js"></script>


