<?php

$folderlocation = "../resources/emma/uploads/user_csv_files";

require('include/db.php');
require('vendor/php-jwt-master/src/JWT.php');
require('vendor/php-jwt-master/src/BeforeValidException.php');
require('vendor/php-jwt-master/src/ExpiredException.php');
require('vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('config/config.json'));

$USER = null;
try {
    $token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key,
        array('HS512'));
}
catch (\Firebase\JWT\BeforeValidException $bve) {
    //echo $bve->getMessage();
    header('Location: index.php');
} catch (\Firebase\JWT\ExpiredException $ee) {
    //echo $ee->getMessage();
    header('Location: index.php');
} catch (\Firebase\JWT\SignatureInvalidException $sie) {
    //echo $sie->getMessage();
    header('Location: index.php');
} catch (Exception $e) {
    //echo 'UNKNOWN EXCEPTION: ';
    //echo $e->getMessage();
    header('Location: index.php');
}
$USER = $token->data;
//check $USER->id is admin of a plan that should be able to download this
$postFolderName = $emmadb->real_escape_string($_POST["account_name"]);

$userID = $USER->id;
$allowed = false;
$folderName = null;
$userQuery = select_user_from_userID($userID);
if($userQuery->num_rows > 0){
    $userResult = $userQuery->fetch_assoc();
    if($userResult['admin_user'] == 1){
        $userFolderQuery = select_multiPlanCVS_with_userID($userID);
        if($userFolderQuery->num_rows > 0){
            while($userFolderResult = $userFolderQuery->fetch_assoc()){
                if($userFolderResult['user_upload_folder'] == $postFolderName){
                    $allowed = true;
                    $folderName = $userFolderResult['user_upload_folder'];
                }
            }
        }
    }
}

if($allowed && $folderName != null) {
    //download file
    if (isset($folderName)) {
        $file = $folderlocation. '/' . $folderName . '/csv_template.csv';
        if (file_exists($file) && is_readable($file)) {
            header('Content-Type: application/csv; charset=utf-8');
            header("Content-Disposition: attachment; filename=template.csv");
            readfile($file);
        }
    }
}
