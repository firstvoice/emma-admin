<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 3/6/2018
 * Time: 2:53 PM
 */

require('include/db.php');

$hash = $fvmdb->real_escape_string($_GET['id']);
if (empty($hash)) {
  $errors['no-hash'] = 'No Id provided';
}

$aeds = $fvmdb->query("
  SELECT a.*, a.location AS placement, o.name AS organization_name, l.name AS location_name, am.model, ab.brand
  FROM aeds a
  JOIN locations l ON a.location_id = l.id
  JOIN organizations o ON l.orgid = o.id
  JOIN aed_models am ON a.aed_model_id = am.aed_model_id
  JOIN aed_brands ab ON am.aed_brand_id = ab.aed_brand_id
  WHERE a.aed_id = '" . $hash . "'
");
if (!$aed = $aeds->fetch_assoc()) {
  $errors['aed'] = 'Can not find AED with Id: ' . $hash;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>AED Check</title>
  <!--  <link type="text/css" rel="stylesheet" href="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css"/>-->
  <!--  <link type="text/css" rel="stylesheet"-->
  <!--        href="https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css">-->

  <link rel="stylesheet" href="css/foundation.css"/>
  <link type="text/css" rel="stylesheet" href="css/main.css"/>
</head>
<body>

<div class="top-bar">
  <div class="top-bar-left">
    <ul class="menu">
      <li class="menu-text">AED Check</li>
    </ul>
  </div>
  <div class="top-bar-right">
  </div>
</div>
<?php if (!empty($errors)) { ?>
  <div class="callout large">
    <div class="row column text-center">
      <h1>Errors</h1>
      <div class="text-left">
        <pre><?php echo print_r($errors, true); ?></pre>
      </div>
    </div>
  </div>
<?php } else { ?>
  <div class="callout large">
    <div class="row column text-center">
      <h1>New AED Check</h1>
      <p><?php echo $aed['brand'] . ', ' . $aed['model'] . ' - ' . $aed['serialnumber']; ?></p>
      <p><?php echo $aed['organization_name'] . ', ' . $aed['location_name'] . ' - ' . $aed['placement']; ?></p>
    </div>
  </div>
  <form id="aed-check" action="#" method="post">
    <div class="row column">
      <h3>General Information</h3>
      <div class="row">
        <div class="small-10 columns">
          <label>Is the AED unit located in the designated area?</label>
        </div>
        <div class="small-2 columns response">
          <div class="switch small">
            <input class="switch-input" id="designated-area" type="checkbox" name="designated-area">
            <label class="switch-paddle" for="designated-area">
              <span class="show-for-sr">Is the AED unit located in the designated area?</span>
              <span class="switch-active" aria-hidden="true">Yes</span>
              <span class="switch-inactive" aria-hidden="true">No</span>
            </label>
          </div>
        </div>
      </div><!--/ Designated Area -->
      <div class="row">
        <div class="small-10 columns">
          <label>Is the AED unit clean and undamaged?</label>
        </div>
        <div class="small-2 columns response">
          <div class="switch small">
            <input class="switch-input" id="undamaged" type="checkbox" name="undamaged">
            <label class="switch-paddle" for="undamaged">
              <span class="show-for-sr">Is the AED unit clean and undamaged?</span>
              <span class="switch-active" aria-hidden="true">Yes</span>
              <span class="switch-inactive" aria-hidden="true">No</span>
            </label>
          </div>
        </div>
      </div><!--/ Clean and Undamaged -->
      <div class="row">
        <div class="small-10 columns">
          <label>Is the AED in an alarmed cabinet?</label>
        </div>
        <div class="small-2 columns response">
          <div class="switch small">
            <input class="switch-input parent" id="cabinet" type="checkbox" name="cabinet">
            <label class="switch-paddle" for="cabinet">
              <span class="show-for-sr">Is the AED in an alarmed cabinet?</span>
              <span class="switch-active" aria-hidden="true">Yes</span>
              <span class="switch-inactive" aria-hidden="true">No</span>
            </label>
          </div>
        </div>
      </div><!--/ Alarm Cabinet -->
      <div class="row child dependent-cabinet">
        <div class="small-10 columns">
          <label><span>&#8230</span> Does the alarm on the AED Cabinet work?</label>
        </div>
        <div class="small-2 columns response">
          <div class="switch small">
            <input class="switch-input" id="alarm-working" type="checkbox" name="alarm-working">
            <label class="switch-paddle" for="alarm-working">
              <span class="show-for-sr">Does the alarm on the AED Cabinet work?</span>
              <span class="switch-active" aria-hidden="true">Yes</span>
              <span class="switch-inactive" aria-hidden="true">No</span>
            </label>
          </div>
        </div>
      </div><!--/ Alarm Cabinet => Working -->
      <div class="row child dependent-cabinet">
        <div class="small-10 columns">
          <label><span>&#8230</span> Did you replace the alarm batteries?</label>
        </div>
        <div class="small-2 columns response">
          <div class="switch small">
            <input class="switch-input parent" id="replace-batteries" type="checkbox" name="replace-batteries">
            <label class="switch-paddle" for="replace-batteries">
              <span class="show-for-sr">Did you replace the alarm batteries?</span>
              <span class="switch-active" aria-hidden="true">Yes</span>
              <span class="switch-inactive" aria-hidden="true">No</span>
            </label>
          </div>
        </div>
      </div><!--/ Alarm Cabinet => Replace Batteries -->
      <div class="row child dependent-replace-batteries">
        <div class="small-10 columns">
          <label><span>&#8230</span> <span>&#8230</span> Expiration Date</label>
        </div>
        <div class="small-2 columns response">
          <input id="replace-batteries-expiration" type="date" class="date" name="replace-batteries-expiration">
        </div>
      </div><!--/ Alarm Cabinet => Replace Batteries => Expiration -->
      <div class="row">
        <div class="small-10 columns">
          <label>Is there an AED rescue kit (razors/scissors) present?</label>
        </div>
        <div class="small-2 columns response">
          <div class="switch small">
            <input class="switch-input parent" id="rescue-kit" type="checkbox" name="rescue-kit">
            <label class="switch-paddle" for="rescue-kit">
              <span class="show-for-sr">Is there an AED rescue kit (razors/scissors) present?</span>
              <span class="switch-active" aria-hidden="true">Yes</span>
              <span class="switch-inactive" aria-hidden="true">No</span>
            </label>
          </div>
        </div>
      </div><!--/ Rescue Kit -->
      <div class="row child dependent-rescue-kit">
        <div class="small-10 columns">
          <label><span>&#8230</span> Is the rescue kit opened or damaged?</label>
        </div>
        <div class="small-2 columns response">
          <div class="switch small">
            <input class="switch-input parent" id="rescue-kit-damaged" type="checkbox" name="rescue-kit-damaged">
            <label class="switch-paddle" for="rescue-kit-damaged">
              <span class="show-for-sr">Is the rescue kit opened or damaged?</span>
              <span class="switch-active" aria-hidden="true">Yes</span>
              <span class="switch-inactive" aria-hidden="true">No</span>
            </label>
          </div>
        </div>
      </div><!--/ Rescue Kit => Opened -->
      <div class="row child dependent-rescue-kit-damaged">
        <div class="small-10 columns">
          <label><span>&#8230</span> <span>&#8230</span> Does the rescue kit contain Gloves?</label>
        </div>
        <div class="small-2 columns response">
          <div class="switch small">
            <input class="switch-input" id="rescue-kit-damaged-gloves" type="checkbox" name="rescue-kit-damaged-gloves">
            <label class="switch-paddle" for="rescue-kit-damaged-gloves">
              <span class="show-for-sr">Does the rescue kit contain Gloves?</span>
              <span class="switch-active" aria-hidden="true">Yes</span>
              <span class="switch-inactive" aria-hidden="true">No</span>
            </label>
          </div>
        </div>
      </div><!--/ Rescue Kit => Opened => Gloves -->
      <div class="row child dependent-rescue-kit-damaged">
        <div class="small-10 columns">
          <label><span>&#8230</span> <span>&#8230</span> Does the rescue kit contain a Prep Razor?</label>
        </div>
        <div class="small-2 columns response">
          <div class="switch small">
            <input class="switch-input" id="rescue-kit-damaged-razor" type="checkbox" name="rescue-kit-damaged-razor">
            <label class="switch-paddle" for="rescue-kit-damaged-razor">
              <span class="show-for-sr">Does the rescue kit contain a Prep Razor?</span>
              <span class="switch-active" aria-hidden="true">Yes</span>
              <span class="switch-inactive" aria-hidden="true">No</span>
            </label>
          </div>
        </div>
      </div><!--/ Rescue Kit => Opened => Razor -->
      <div class="row child dependent-rescue-kit-damaged">
        <div class="small-10 columns">
          <label><span>&#8230</span> <span>&#8230</span> Does the rescue kit contain Scissors?</label>
        </div>
        <div class="small-2 columns response">
          <div class="switch small">
            <input class="switch-input" id="rescue-kit-damaged-scissors" type="checkbox"
                   name="rescue-kit-damaged-scissors">
            <label class="switch-paddle" for="rescue-kit-damaged-scissors">
              <span class="show-for-sr">Does the rescue kit contain Scissors?</span>
              <span class="switch-active" aria-hidden="true">Yes</span>
              <span class="switch-inactive" aria-hidden="true">No</span>
            </label>
          </div>
        </div>
      </div><!--/ Rescue Kit => Opened => Scissors -->
      <div class="row child dependent-rescue-kit-damaged">
        <div class="small-10 columns">
          <label><span>&#8230</span> <span>&#8230</span> Does the rescue kit contain Towel & Moist Towlette?</label>
        </div>
        <div class="small-2 columns response">
          <div class="switch small">
            <input class="switch-input" id="rescue-kit-damaged-towlette" type="checkbox"
                   name="rescue-kit-damaged-towlette">
            <label class="switch-paddle" for="rescue-kit-damaged-towlette">
              <span class="show-for-sr">Does the rescue kit contain Towel & Moist Towlette?</span>
              <span class="switch-active" aria-hidden="true">Yes</span>
              <span class="switch-inactive" aria-hidden="true">No</span>
            </label>
          </div>
        </div>
      </div><!--/ Rescue Kit => Opened => Towelette -->
      <div class="row child dependent-rescue-kit-damaged">
        <div class="small-10 columns">
          <label><span>&#8230</span> <span>&#8230</span> Does the rescue kit contain a CPR Mask?</label>
        </div>
        <div class="small-2 columns response">
          <div class="switch small">
            <input class="switch-input" id="rescue-kit-damaged-mask" type="checkbox" name="rescue-kit-damaged-mask">
            <label class="switch-paddle" for="rescue-kit-damaged-mask">
              <span class="show-for-sr">Does the rescue kit contain a CPR Mask?</span>
              <span class="switch-active" aria-hidden="true">Yes</span>
              <span class="switch-inactive" aria-hidden="true">No</span>
            </label>
          </div>
        </div>
      </div><!--/ Rescue Kit => Opened => CPR Mask -->
    </div> <!--/ General Information -->
    <?php
    $pads = $fvmdb->query("
      SELECT ap.*, apt.type
      FROM aed_pads ap
      JOIN aed_pad_types apt ON ap.aed_pad_type_id = apt.aed_pad_type_id
      WHERE ap.aed_id = '" . $aed['aed_id'] . "'
      AND ap.display = 1
    ");
    $selectPadTypes = '';
    $modelPads = $fvmdb->query("
      SELECT tom.*, t.type
      FROM aed_pad_types_on_models tom
      JOIN aed_pad_types t ON tom.aed_pad_type_id = t.aed_pad_type_id
      WHERE tom.aed_model_id = '" . $aed['aed_model_id'] . "'
    ");
    while ($modelPad = $modelPads->fetch_assoc()) {
      $selectPadTypes .= '<option value="' . $modelPad['aed_pad_type_id'] . '">' . $modelPad['type'] . '</option>';
    }
    if ($pads->num_rows > 0) { ?>
      <hr>
      <div class="row column">
        <h3>Pads</h3>
        <p>Are the expiration dates expired or incorrect?</p>
        <?php
        while ($pad = $pads->fetch_assoc()) {
          echo '
          <div class="row">
            <div class="small-10 columns">
              <div class="attachment-info" style="height:4rem;">
                <div><strong>Pad: </strong>' . $pad['type'] . '</div>
                <div><strong>Lot Number / UDI: </strong>' . $pad['lot'] . '</div>
                <div><strong>Expiration: </strong>' . $pad['expiration'] . '</div>
              </div>
            </div>
            <div class="small-2 columns response">
              <div class="switch small" style="height:4rem;margin-top:1.5rem;">
                <input class="switch-input parent" id="pad-' . $pad['aed_pad_id'] . '" type="checkbox" name="pad-' .
            $pad['aed_pad_id'] . '">
                <label class="switch-paddle" for="pad-' . $pad['aed_pad_id'] . '">
                  <span class="show-for-sr">Are the expiration dates expired or incorrect?</span>
                  <span class="switch-active" aria-hidden="true">Yes</span>
                  <span class="switch-inactive" aria-hidden="true">No</span>
                </label>
              </div>
            </div>
          </div>
          <div class="row child dependent-pad-' . $pad['aed_pad_id'] . '">
            <div class="small-10 columns">
              <label><span>&#8230</span> Are you installing new pads</label>
            </div>
            <div class="small-2 columns response">
              <div class="switch small">
                <input class="switch-input parent" id="new-pad-' . $pad['aed_pad_id'] .
            '" type="checkbox" name="new-pad-' . $pad['aed_pad_id'] . '">
                <label class="switch-paddle" for="new-pad-' . $pad['aed_pad_id'] . '">
                  <span class="show-for-sr">Are you installing new pads?</span>
                  <span class="switch-active" aria-hidden="true">Yes</span>
                  <span class="switch-inactive" aria-hidden="true">No</span>
                </label>
              </div>
            </div>
          </div>
          <div class="row child dependent-new-pad-' . $pad['aed_pad_id'] . '">
            <div class="small-9 columns">
              <label><span>&#8230</span> <span>&#8230</span> Pad</label>
            </div>
            <div class="small-3 columns response">
              <select id="new-pad-' . $pad['aed_pad_id'] . '-type" name="new-pad-' . $pad['aed_pad_id'] . '-type">' .
            $selectPadTypes . '</select>
            </div>
          </div>
          <div class="row child dependent-new-pad-' . $pad['aed_pad_id'] . '">
            <div class="small-9 columns">
              <label><span>&#8230</span> <span>&#8230</span> Lot Number / UDI</label>
            </div>
            <div class="small-3 columns response">
              <input id="new-pad-' . $pad['aed_pad_id'] . '-lot" type="text" name="new-pad-' . $pad['aed_pad_id'] . '-lot">
            </div>
          </div>
          <div class="row child dependent-new-pad-' . $pad['aed_pad_id'] . '">
            <div class="small-9 columns">
              <label><span>&#8230</span> <span>&#8230</span> Expiration</label>
            </div>
            <div class="small-3 columns response">
              <input id="new-pad-' . $pad['aed_pad_id'] . '-expiration" type="date" class="date" name="new-pad-' .
            $pad['aed_pad_id'] . '-expiration">
            </div>
          </div>
          ';
        }
        ?>
      </div>
    <?php }
    $paks = $fvmdb->query("
      SELECT ap.*, apt.type
      FROM aed_paks ap
      JOIN aed_pak_types apt ON ap.aed_pak_type_id = apt.aed_pak_type_id
      WHERE ap.aed_id = '" . $aed['aed_id'] . "'
      AND ap.display = 1
    ");
    $selectPakTypes = '';
    $modelPaks = $fvmdb->query("
      SELECT tom.*, t.type
      FROM aed_pak_types_on_models tom
      JOIN aed_pak_types t ON tom.aed_pak_type_id = t.aed_pak_type_id
      WHERE tom.aed_model_id = '" . $aed['aed_model_id'] . "'
    ");
    while ($modelPak = $modelPaks->fetch_assoc()) {
      $selectPakTypes .= '<option value="' . $modelPak['aed_pak_type_id'] . '">' . $modelPak['type'] . '</option>';
    }
    if ($paks->num_rows > 0) {
      ?>
      <hr>
      <div class="row column">
        <h3>Paks</h3>
        <p>Are the expiration dates expired or incorrect?</p>
        <?php
        while ($pak = $paks->fetch_assoc()) {
          echo '
          <div class="row">
            <div class="small-10 columns">
              <div class="attachment-info" style="height:4rem;">
                <div><strong>Pak: </strong>' . $pak['type'] . '</div>
                <div><strong>Lot Number / UDI: </strong>' . $pak['lot'] . '</div>
                <div><strong>Expiration: </strong>' . $pak['expiration'] . '</div>
              </div>
            </div>
            <div class="small-2 columns response">
              <div class="switch small" style="height:4rem;margin-top:1.5rem;">
                <input class="switch-input parent" id="pak-' . $pak['aed_pak_id'] . '" type="checkbox" name="pak-' .
            $pak['aed_pak_id'] . '">
                <label class="switch-paddle" for="pak-' . $pak['aed_pak_id'] . '">
                  <span class="show-for-sr">Are the expiration dates expired or incorrect?</span>
                  <span class="switch-active" aria-hidden="true">Yes</span>
                  <span class="switch-inactive" aria-hidden="true">No</span>
                </label>
              </div>
            </div>
          </div>
          <div class="row child dependent-pak-' . $pak['aed_pak_id'] . '">
            <div class="small-10 columns">
              <label><span>&#8230</span> Are you installing new paks</label>
            </div>
            <div class="small-2 columns response">
              <div class="switch small">
                <input class="switch-input parent" id="new-pak-' . $pak['aed_pak_id'] .
            '" type="checkbox" name="new-pak-' . $pak['aed_pak_id'] . '">
                <label class="switch-paddle" for="new-pak-' . $pak['aed_pak_id'] . '">
                  <span class="show-for-sr">Are you installing new paks?</span>
                  <span class="switch-active" aria-hidden="true">Yes</span>
                  <span class="switch-inactive" aria-hidden="true">No</span>
                </label>
              </div>
            </div>
          </div>
          <div class="row child dependent-new-pak-' . $pak['aed_pak_id'] . '">
            <div class="small-9 columns">
              <label><span>&#8230</span> <span>&#8230</span> Pad</label>
            </div>
            <div class="small-3 columns response">
              <select id="new-pak-' . $pak['aed_pak_id'] . '-type" name="new-pak-' . $pak['aed_pak_id'] . '-type">' .
            $selectPakTypes . '</select>
            </div>
          </div>
          <div class="row child dependent-new-pak-' . $pak['aed_pak_id'] . '">
            <div class="small-9 columns">
              <label><span>&#8230</span> <span>&#8230</span> Lot Number / UDI</label>
            </div>
            <div class="small-3 columns response">
              <input id="new-pak-' . $pak['aed_pak_id'] . '-lot" type="text" name="new-pak-' . $pak['aed_pak_id'] . '-lot">
            </div>
          </div>
          <div class="row child dependent-new-pak-' . $pak['aed_pak_id'] . '">
            <div class="small-9 columns">
              <label><span>&#8230</span> <span>&#8230</span> Expiration</label>
            </div>
            <div class="small-3 columns response">
              <input id="new-pak-' . $pak['aed_pak_id'] . '-expiration" type="date" class="date" name="new-pak-' .
            $pak['aed_pak_id'] . '-expiration">
            </div>
          </div>
          ';
        }
        ?>
      </div>
    <?php }
    $batteries = $fvmdb->query("
      SELECT ab.*, abt.type
      FROM aed_batteries ab
      JOIN aed_battery_types abt ON ab.aed_battery_type_id = abt.aed_battery_type_id
      WHERE ab.aed_id = '" . $aed['aed_id'] . "'
      AND ab.display = 1
    ");
    $selectBatteryTypes = '';
    $modelBatteries = $fvmdb->query("
      SELECT tom.*, t.type
      FROM aed_battery_types_on_models tom
      JOIN aed_battery_types t ON tom.aed_battery_type_id = t.aed_battery_type_id
      WHERE tom.aed_model_id = '" . $aed['aed_model_id'] . "'
    ");
    while ($modelBattery = $modelBatteries->fetch_assoc()) {
      $selectBatteryTypes .= '<option value="' . $modelBattery['aed_battery_type_id'] . '">' . $modelBattery['type'] .
        '</option>';
    }
    if ($batteries->num_rows > 0) { ?>
      <hr>
      <div class="row column">
        <h3>Batteries</h3>
        <p>Are the expiration dates expired or incorrect?</p>
        <?php
        while ($battery = $batteries->fetch_assoc()) {
          echo '
          <div class="row">
            <div class="small-10 columns">
              <div class="attachment-info" style="height:4rem;">
                <div><strong>Battery: </strong>' . $battery['type'] . '</div>
                <div><strong>Lot Number / UDI: </strong>' . $battery['lot'] . '</div>
                <div><strong>Expiration: </strong>' . $battery['expiration'] . '</div>
              </div>
            </div>
            <div class="small-2 columns response">
              <div class="switch small" style="height:4rem;margin-top:1.5rem;">
                <input class="switch-input parent" id="battery-' . $battery['aed_battery_id'] .
            '" type="checkbox" name="battery-' .
            $battery['aed_battery_id'] . '">
                <label class="switch-paddle" for="battery-' . $battery['aed_battery_id'] . '">
                  <span class="show-for-sr">Are the expiration dates expired or incorrect?</span>
                  <span class="switch-active" aria-hidden="true">Yes</span>
                  <span class="switch-inactive" aria-hidden="true">No</span>
                </label>
              </div>
            </div>
          </div>
          <div class="row child dependent-battery-' . $battery['aed_battery_id'] . '">
            <div class="small-10 columns">
              <label><span>&#8230</span> Are you installing new batteries</label>
            </div>
            <div class="small-2 columns response">
              <div class="switch small">
                <input class="switch-input parent" id="new-battery-' . $battery['aed_battery_id'] .
            '" type="checkbox" name="new-battery-' . $battery['aed_battery_id'] . '">
                <label class="switch-paddle" for="new-battery-' . $battery['aed_battery_id'] . '">
                  <span class="show-for-sr">Are you installing new batteries?</span>
                  <span class="switch-active" aria-hidden="true">Yes</span>
                  <span class="switch-inactive" aria-hidden="true">No</span>
                </label>
              </div>
            </div>
          </div>
          <div class="row child dependent-new-battery-' . $battery['aed_battery_id'] . '">
            <div class="small-9 columns">
              <label><span>&#8230</span> <span>&#8230</span> Pad</label>
            </div>
            <div class="small-3 columns response">
              <select id="new-battery-' . $battery['aed_battery_id'] . '-type" name="new-battery-' .
            $battery['aed_battery_id'] . '-type">' . $selectBatteryTypes . '</select>
            </div>
          </div>
          <div class="row child dependent-new-battery-' . $battery['aed_battery_id'] . '">
            <div class="small-9 columns">
              <label><span>&#8230</span> <span>&#8230</span> Lot Number / UDI</label>
            </div>
            <div class="small-3 columns response">
              <input id="new-battery-' . $battery['aed_battery_id'] . '-lot" type="text" name="new-battery-' .
            $battery['aed_battery_id'] . '-lot">
            </div>
          </div>
          <div class="row child dependent-new-battery-' . $battery['aed_battery_id'] . '">
            <div class="small-9 columns">
              <label><span>&#8230</span> <span>&#8230</span> Expiration</label>
            </div>
            <div class="small-3 columns response">
              <input id="new-battery-' . $battery['aed_battery_id'] .
            '-expiration" type="date" class="date" name="new-battery-' . $battery['aed_battery_id'] . '-expiration">
            </div>
          </div>
          ';
        }
        ?>
      </div>
    <?php }
    $accessories = $fvmdb->query("
      SELECT aa.*, aat.type
      FROM aed_accessories aa
      JOIN aed_accessory_types aat ON aa.aed_accessory_type_id = aat.aed_accessory_type_id
      WHERE aa.aed_id = '" . $aed['aed_id'] . "'
      AND aa.display = 1
    ");
    $selectAccessoryTypes = '';
    $modelAccessories = $fvmdb->query("
      SELECT tom.*, t.type
      FROM aed_accessory_types_on_models tom
      JOIN aed_accessory_types t ON tom.aed_accessory_type_id = t.aed_accessory_type_id
      WHERE tom.aed_model_id = '" . $aed['aed_model_id'] . "'
    ");
    while ($modelAccessory = $modelAccessories->fetch_assoc()) {
      $selectAccessoryTypes .= '<option value="' . $modelAccessory['aed_accessory_type_id'] . '">' .
        $modelAccessory['type'] . '</option>';
    }
    if ($accessories->num_rows > 0) { ?>
      <hr>
      <div class="row column">
        <h3>Accessories</h3>
        <p>Are the expiration dates expired or incorrect?</p>
        <?php
        while ($accessory = $accessories->fetch_assoc()) {
          echo '
          <div class="row">
            <div class="small-10 columns">
              <div class="attachment-info" style="height:4rem;">
                <div><strong>Accessory: </strong>' . $accessory['type'] . '</div>
                <div><strong>Lot Number / UDI: </strong>' . $accessory['lot'] . '</div>
                <div><strong>Expiration: </strong>' . $accessory['expiration'] . '</div>
              </div>
            </div>
            <div class="small-2 columns response">
              <div class="switch small" style="height:4rem;margin-top:1.5rem;">
                <input class="switch-input parent" id="accessory-' . $accessory['aed_accessory_id'] .
            '" type="checkbox" name="accessory-' . $accessory['aed_accessory_id'] . '">
                <label class="switch-paddle" for="accessory-' . $accessory['aed_accessory_id'] . '">
                  <span class="show-for-sr">Are the expiration dates expired or incorrect?</span>
                  <span class="switch-active" aria-hidden="true">Yes</span>
                  <span class="switch-inactive" aria-hidden="true">No</span>
                </label>
              </div>
            </div>
          </div>
          <div class="row child dependent-accessory-' . $accessory['aed_accessory_id'] . '">
            <div class="small-10 columns">
              <label><span>&#8230</span> Are you installing new batteries</label>
            </div>
            <div class="small-2 columns response">
              <div class="switch small">
                <input class="switch-input parent" id="new-accessory-' . $accessory['aed_accessory_id'] .
            '" type="checkbox" name="new-accessory-' . $accessory['aed_accessory_id'] . '">
                <label class="switch-paddle" for="new-accessory-' . $accessory['aed_accessory_id'] . '">
                  <span class="show-for-sr">Are you installing new batteries?</span>
                  <span class="switch-active" aria-hidden="true">Yes</span>
                  <span class="switch-inactive" aria-hidden="true">No</span>
                </label>
              </div>
            </div>
          </div>
          <div class="row child dependent-new-accessory-' . $accessory['aed_accessory_id'] . '">
            <div class="small-9 columns">
              <label><span>&#8230</span> <span>&#8230</span> Pad</label>
            </div>
            <div class="small-3 columns response">
              <select id="new-accessory-' . $accessory['aed_accessory_id'] . '-type" name="new-accessory-' .
            $accessory['aed_accessory_id'] . '-type">' . $selectAccessoryTypes . '</select>
            </div>
          </div>
          <div class="row child dependent-new-battery-' . $accessory['aed_accessory_id'] . '">
            <div class="small-9 columns">
              <label><span>&#8230</span> <span>&#8230</span> Lot Number / UDI</label>
            </div>
            <div class="small-3 columns response">
              <input id="new-accessory-' . $accessory['aed_accessory_id'] . '-lot" type="text" name="new-accessory-' .
            $accessory['aed_accessory_id'] . '-lot">
            </div>
          </div>
          <div class="row child dependent-new-accessory-' . $accessory['aed_accessory_id'] . '">
            <div class="small-9 columns">
              <label><span>&#8230</span> <span>&#8230</span> Expiration</label>
            </div>
            <div class="small-3 columns response">
              <input id="new-accessory-' . $accessory['aed_accessory_id'] .
            '-expiration" type="date" class="date" name="new-accessory-' . $accessory['aed_accessory_id'] . '-expiration">
            </div>
          </div>
          ';
        }
        ?>
      </div>
    <?php } ?>
    <hr>
    <div class="row column">
      <h3>AED Ready</h3>
      <div class="row">
        <div class="small-10 columns">
          <label>Is the Status Indicator showing the AED in a ready status?</label>
        </div>
        <div class="small-2 columns response">
          <div class="switch small">
            <input class="switch-input" id="status" type="checkbox" name="status">
            <label class="switch-paddle" for="status">
              <span class="show-for-sr">Is the Status Indicator showing the AED in a ready status?</span>
              <span class="switch-active" aria-hidden="true">Yes</span>
              <span class="switch-inactive" aria-hidden="true">No</span>
            </label>
          </div>
        </div>
      </div>
    </div><!--/ AED Ready -->
    <hr>
    <div class="row column">
      <h3>Responders</h3>
      <div style="padding-left:2rem;">
        <ul>
          <?php
          $responders = $fvmdb->query("
            SELECT
              p.id AS responder_id, 
              p.firstname,
              p.lastname
            FROM persons p
            WHERE p.current = 'yes'
                  AND p.display = 'yes'
                  AND p.locationid = '" . $aed['location_id'] . "'
            ORDER BY p.firstname, p.lastname
          ");
          while ($responder = $responders->fetch_assoc()) {
            echo '<li><input type="hidden" name="respoders[]" value="' . $responder['responder_id'] . '" />' .
              $responder['firstname'] . ' ' . $responder['lastname'] . '</li>';
          }
          ?>
        </ul>
      </div>
    </div>
    <hr>
    <div class="row column">
      <h3>Checked By</h3>
      <div class="row">
        <div class="small-6 columns">
          <label>Full Name
            <input name="checked-by-name" type="text"/>
          </label>
        </div>
        <div class="small-6 columns">
          <label>Email
            <input name="checked-by-email" type="text"/>
          </label>
        </div>
      </div>
    </div>
    <hr>
    <div class="row column">
      <h3>Comments</h3>
      <textarea style="height:5rem;min-width:100%;"></textarea>
    </div>
    <div class="row">
      <div class="small-2 columns">
        <input type="submit" class="button expanded" value="Submit"/>
      </div>
    </div>
  </form>
<?php } ?>

<div class="reveal callout success text-center tiny" id="success-modal" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
  <h4>Successfully Added</h4>
  <a href="index.php?id=<?php echo $aed['aed_id']; ?>" class="button success">OK</a>
</div>
<div class="reveal callout alert text-center tiny" id="fail-modal" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
  <h4 style="color:darkred">Failed...</h4>
  <div id="error-list" class="text-left"></div>
  <a class="button" data-close>OK</a>
</div>
<div id="thinking-modal" class="reveal callout text-center tiny" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
  <h4 style="color:#e5e5e5;">Working...</h4>
  <img style="width:2rem;height:2rem;" src="img/ajax_loader_blue.gif"/>
</div>
<!--<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>-->
<!--<script src="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>-->
<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/foundation.js"></script>
<script src="js/main.js"></script>
</body>
</html>
