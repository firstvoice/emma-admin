<?php
/**
 * Created by PhpStorm.
 * User: COnrad Ullrich
 * Date: 08/15/2019
 * Time: 11:31 AM
 */

use Firebase\JWT\JWT;

session_start();
include('include/db.php');
require('vendor/php-jwt-master/src/JWT.php');
require('vendor/php-jwt-master/src/BeforeValidException.php');
require('vendor/php-jwt-master/src/ExpiredException.php');
require('vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('config/config.json'));

if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != "on") {
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    exit();
}

$errorMessage = '';
if (isset($_COOKIE['jwt'])) {
    try {
        $token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key,
            array('HS512'));
        header('Location: dashboard.php');
    } catch (\Firebase\JWT\BeforeValidException $bve) {
        $errorMessage = $bve->getMessage();
    } catch (\Firebase\JWT\ExpiredException $ee) {
        $errorMessage = $ee->getMessage();
    } catch (\Firebase\JWT\SignatureInvalidException $sie) {
        $errorMessage = $sie->getMessage();
    } catch (Exception $e) {
        $errorMessage = $e->getMessage();
    }
}
//login with token from emma admin
if (isset($_GET['token'])) {

    $loginToken = $fvmdb->real_escape_string($_GET['token']);

    $users = login_with_token($loginToken);
    if ($users->num_rows > 0) {
        $user = $users->fetch_assoc();

        $admin = false;
        $security = false;
        $plansecurity = false;
        $groups = select_groups_with_userID_and_planID($user['id'], $user['token_plan']);
        while ($group = $groups->fetch_assoc()) {
            if ($group['admin']) $admin = true;
            if ($group['security']) $security = true;
            if ($group['geofences']) $geofences = true;
            if ($group['911admin']) $admin911 = true;
            if ($group['securities']) $plansecurity = true;
            if ($group['panic']) $plansos = true;
        }

        //      if ($admin || $security) {
        $tokenId = base64_encode(randomToken(32));
        $issuedAt = time();
        $notBefore = $issuedAt;
        $expire = $notBefore + (10 * 365 * 24 * 60 * 60);
        $serverName = 'tsdemos.com';

        $data = [
            'iat' => $issuedAt,
            'jti' => $tokenId,
            'iss' => $serverName,
            'nbf' => $notBefore,
            'exp' => $expire,
            'data' => [
                'id' => $user['id'],
                'full_name' => $user['full_name'],
                'username' => $user['username'],
                'emma_plan_id' => $user['emma_plan_id'],
                'emma_pin' => $user['emma_pin'],
                'auth' => $user['auth'],
                'privilege' => [
                    'admin' => $admin,
                    'security' => $security,
                    'plansecurity' => $plansecurity,
                    'plansos' => $plansos,
                    'geofences' => $geofences,
                    'admin911' => $admin911,
                    'code_generator' => $user['emma_code_generator'] == 1,
                    'plans' => $user['emma_plans'] == 1,
                    'plan_god' => $user['plan_god'] == 1,
                    'plan_emails' => $user['plan_emails'] == 1,
                    'plan_texts' => $user['plan_texts'] == 1
                ]
            ]
        ];

        $jwt = Firebase\JWT\JWT::encode($data, $CONFIG->key, 'HS512');
        setcookie('jwt', $jwt, time() + (9 * 60 * 60), '/');

        if (isset($_GET['content'])) {
            header('Location: dashboard.php?content=' . $_GET['content']);
        } else {
            header('Location: dashboard.php');
        }


    }
}
if (isset($_POST['login']) && !empty($_POST['username'])) {
    try {
        $username = $fvmdb->real_escape_string($_POST['username']);
        $password = $fvmdb->real_escape_string($_POST['password']);
        $users = login_with_username_and_password($username,$password );
        if ($users->num_rows > 0) {
            $user = $users->fetch_assoc();

            $admin = false;
            $security = false;
            $plansecurity = false;
            $groups = select_groups_with_userID($user['id']);
            while ($group = $groups->fetch_assoc()) {
                if ($group['admin']) $admin = true;
                if ($group['security']) $security = true;
                if ($group['geofences']) $geofences = true;
                if ($group['911admin']) $admin911 = true;
                if ($group['securities']) $plansecurity = true;
                if ($group['panic']) $plansos = true;
            }

//      if ($admin || $security) {
            $tokenId = base64_encode(randomToken(32));
            $issuedAt = time();
            $notBefore = $issuedAt;
            $expire = $notBefore + (10 * 365 * 24 * 60 * 60);
            $serverName = 'tsdemos.com';

            $data = [
                'iat' => $issuedAt,
                'jti' => $tokenId,
                'iss' => $serverName,
                'nbf' => $notBefore,
                'exp' => $expire,
                'data' => [
                    'id' => $user['id'],
                    'full_name' => $user['full_name'],
                    'username' => $user['username'],
                    'emma_plan_id' => $user['emma_plan_id'],
                    'emma_pin' => $user['emma_pin'],
                    'auth' => $user['auth'],
                    'privilege' => [
                        'admin' => $admin,
                        'security' => $security,
                        'plansecurity' => $plansecurity,
                        'plansos' => $plansos,
                        'geofences' => $geofences,
                        'admin911' => $admin911,
                        'code_generator' => $user['emma_code_generator'] == 1,
                        'plans' => $user['emma_plans'] == 1,
                        'plan_god' => $user['plan_god'] == 1,
                        'plan_emails' => $user['plan_emails'] == 1,
                        'plan_texts' => $user['plan_texts'] == 1
                    ]
                ]
            ];

            $jwt = Firebase\JWT\JWT::encode($data, $CONFIG->key, 'HS512');
            setcookie('jwt', $jwt, time() + (9 * 60 * 60), '/');
//      } else {
//        $errorMessage = 'User is not in an Admin or Security group';
//      }
            $events = select_emergencies_with_planID($user['emma_plan_id']);
            if ($event = $events->fetch_assoc()) {
                header('Location: dashboard.php?content=event&id=' . $event['emergency_id']);
            } else {
                header('Location: dashboard.php');
            }
        } else {
            $errorMessage = 'Invalid Username/Email or Password';
        }
    } catch (Exception $e) {
        $e->getMessage();
    }
} elseif (isset($_POST['login']) && empty($_POST['username'])) {
    $errorMessage = 'Invalid Username/Email or Password';
}
?>
<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Login</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #000000;">


<?php include('include/index_top_bar.php'); ?>

<div class="row expanded emma_verse" style="padding-bottom: 2vh;margin: 0 auto;">
    <div class="large-12 medium-12 small-12 columns" style="margin: 0 auto;">
        <h1>What others are saying in the EMMA verse!</h1>
    </div>
    <div class="large-12 medium-12 small-12 columns">
        <div class="row expanded">
            <div class="large-4 medium-12 small-12 columns">
                &nbsp;
            </div>
            <div id="rotate" class="large-8 medium-12 small-12 columns rotate_txt">
                <div>
                    <h1>Emma Rocks<h1/>
                    Tom S. Davenport, IA</h1>
                </div>
                <div>
                    <p>Emma Rules<br/>
                    Tomy  V. Cedar Rapids, IA </p>
                </div>
                <div>
                    <p>Emma is out of this world!<br />
                        Barb B. Des Moines, IA</p>
                </div>
            </div>
        </div>
    </div>

</div>
<?php include('include/footer.php'); ?>

<div id="thinking-modal" class="reveal callout text-center tiny" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4 style="color:#e5e5e5;">Working...</h4>
    <img style="width:2rem;height:2rem;" src="img/ajax_loader_blue.gif"/>
</div>
<div id="password-success-modal" class="reveal callout success text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <br>
    <p>Please check your email for your temporary password. It may take up to 5 minutes for you to receive.
        If you do not receive it, please check your junk/spam folder before contacting us.</p>
    <a data-close class="button success">OK</a>
</div>
<div id="contact-us-success-modal" class="reveal callout success text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <br>
    <p>Thank you for contacting us. We have received your message and will get back to you shortly.</p>
    <a data-close class="button success">OK</a>
</div>
<div id="fail-modal" class="reveal callout alert text-center tiny" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4 style="color:darkred">Failed...</h4>
    <div id="error-list" class="text-left"></div>
    <a class="button" style="background-color: darkred" data-close>OK</a>
</div>
<div id="forgot-password-modal" class="reveal callout text-center" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Forgot Password</h4>
    <hr>
    <form id="forgot-password-form" class="text-left" action="#" method="post">
        <label>Username/Email<span style="color: red;"> *</span>
            <input type="text" name="current-email" id="current-email" placeholder="username@domain.com" required/>
        </label>
        <div class="button-group expanded">
            <a data-close="" class="button alert">Cancel</a>
            <input type="submit" class="button" value="Submit"/>
        </div>
    </form>
</div>
<div id="login-modal" class="reveal callout text-center"
     style="background-color: #0078c1;border: 1px solid #0078C1;" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" id="login-form" class="text-left"
          method="post">
        <label style="color: white">Username/Email
            <input type="text" name="username" id="username" required/>
        </label>
        <label style="color: white">Password
            <input type="password" name="password" id="password" required/>
        </label>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true"></span>
        </button>
        <div class="button-group expanded">
            <input class="button" style="background-color: yellow;color: black;font-weight: bold" type="submit"
                   name="login" id="login" value="LOGIN"/>
        </div>
    </form>
</div>
<div id="contact-us-modal" class="reveal callout text-center" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <form id="contact-us-form" class="text-left" action="#" method="post"><input type="hidden" id="contact-us-type" name="type">
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>First Name<span style="color: red">*</span></label>
                <input type="text" name="first-name" id="firstname"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Last Name<span style="color: red">*</span></label>
                <input type="text" name="last-name" id="lastname"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Email<span style="color: red">*</span></label>
                <input type="text" name="email" id="email"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Organization</label>
                <input type="text" name="org" id="org"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Mobile</label>
                <input type="text" name="mobile" id="mobile"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Landline</label>
                <input type="text" name="landline" id="landline"/>
            </div>
            <hr>
        </div>
        <div class="row">
            <div class="large-12 medium-12 columns">
                <label id="comments">Comments<span style="color: red">*</span></label>
                <textarea name="comments" id="comments" class="text" style="color: black;"></textarea>
            </div>
        </div>
        <div class="large-12 columns">
            <div class="button-group expanded">
                <a data-close="" class="button alert">Cancel</a>
                <input type="submit" class="button" value="Submit"/>
            </div>
        </div>
    </form>
</div>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>


