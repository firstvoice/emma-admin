<?php
session_start();
session_destroy();
setcookie('jwt', '', time() + (9 * 60 * 60), '/');
header('Location: index.php');
?>
