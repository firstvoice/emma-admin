<?php

define('__ROOT__', dirname(dirname(__FILE__)));

require_once('include/db.php');
require_once('vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;

$rootPath = "../resources/emma/uploads/user_csv_files";
$folderlocation = "../resources/emma/uploads/user_csv_files";

//select upload plans
require_once('vendor/php-jwt-master/src/JWT.php');
require_once('vendor/php-jwt-master/src/BeforeValidException.php');
require_once('vendor/php-jwt-master/src/ExpiredException.php');
require_once('vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('config/config.json'));

$USER = null;
try {
    $token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key,
        array('HS512'));
}
catch (\Firebase\JWT\BeforeValidException $bve) {
    //echo $bve->getMessage();
    header('Location: index.php');
} catch (\Firebase\JWT\ExpiredException $ee) {
    //echo $ee->getMessage();
    header('Location: index.php');
} catch (\Firebase\JWT\SignatureInvalidException $sie) {
    //echo $sie->getMessage();
    header('Location: index.php');
} catch (Exception $e) {
    //echo 'UNKNOWN EXCEPTION: ';
    //echo $e->getMessage();
    header('Location: index.php');
}
$USER = $token->data;
//check $USER->id is admin of a plan that should be able to download this
$postFolderName = $emmadb->real_escape_string($_POST["account_name"]);

$userID = $USER->id;
$allowed = false;
$accountName = null;
$userQuery = select_user_from_userID($userID);
if($userQuery->num_rows > 0){
    $userResult = $userQuery->fetch_assoc();
    if($userResult['admin_user'] == 1){
        $userFolderQuery = select_multiPlanCVS_with_userID($userID);
        if($userFolderQuery->num_rows > 0){
            while($userFolderResult = $userFolderQuery->fetch_assoc()){
                if($userFolderResult['user_upload_folder'] == $postFolderName){
                    $allowed = true;
                    $accountName = $userFolderResult['user_upload_folder'];
                }
            }
        }
    }
}

//$accountPlansQuery = select_plans_in_account($accountName);
//$planString = "";
//if($accountPlansQuery->num_rows > 0){
//    for($i = 0; $i<$accountPlansQuery->num_rows;$i++){
//        $accountPlansResult = $accountPlansQuery->fetch_assoc();
//        if($i == 0){
//            $planString = $accountPlansResult['emma_plan_id'];
//        }
//        else{
//            $planString = $planString . ',' . $accountPlansResult['emma_plan_id'];
//        }
//    }
//}


//select users in all plans
$usersInAccountPlanQuery = select_users_in_account_plan($accountName);


//file setup
try {
    $spreadsheet = new Spreadsheet();
    $myWorkSheet = new PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $accountName);
    $spreadsheet->addSheet($myWorkSheet);

    if ($spreadsheet->sheetNameExists('Worksheet')) {
        $sheetIndex = $spreadsheet->getIndex(
            $spreadsheet->getSheetByName('Worksheet')
        );
        $spreadsheet->removeSheetByIndex($sheetIndex);
    }

    //add headers
    //email,firstname,middlename,lastname,title,mobilephone,landline,company,streetaddress,city,state/province,country,zip,activationEmails,noAutoLogout,adminUser,plans,groups,edit,delete
    $spreadsheet->getSheet(0)->setCellValue('A1', 'email');
    $spreadsheet->getSheet(0)->setCellValue('B1', 'firstname');
    $spreadsheet->getSheet(0)->setCellValue('C1', 'middlename');
    $spreadsheet->getSheet(0)->setCellValue('D1', 'lastname');
    $spreadsheet->getSheet(0)->setCellValue('E1', 'title');
    $spreadsheet->getSheet(0)->setCellValue('F1', 'mobilephone');
    $spreadsheet->getSheet(0)->setCellValue('G1', 'landline');
    $spreadsheet->getSheet(0)->setCellValue('H1', 'company');
    $spreadsheet->getSheet(0)->setCellValue('I1', 'streetaddress');
    $spreadsheet->getSheet(0)->setCellValue('J1', 'city');
    $spreadsheet->getSheet(0)->setCellValue('K1', 'state/province');
    $spreadsheet->getSheet(0)->setCellValue('L1', 'country');
    $spreadsheet->getSheet(0)->setCellValue('M1', 'zip');
    $spreadsheet->getSheet(0)->setCellValue('N1', 'activationEmails');
    $spreadsheet->getSheet(0)->setCellValue('O1', 'noAutoLogout');
    $spreadsheet->getSheet(0)->setCellValue('P1', 'adminUser');
    $spreadsheet->getSheet(0)->setCellValue('Q1', 'plans');
    $spreadsheet->getSheet(0)->setCellValue('R1', 'groups');
    $spreadsheet->getSheet(0)->setCellValue('S1', 'edit');
    $spreadsheet->getSheet(0)->setCellValue('T1', 'delete');


}
catch (PhpOffice\PhpSpreadsheet\Exception $e){
    global $errors;
    $errors[] = 'Failed to create spreadsheet export for'. $accountName;
    error_log('Failed to create spreadsheet export for: ' . $accountName);
    error_log($e->getMessage());
}
catch (Exception $e){
    error_log($e->getMessage());
}
$row = 2;
//loop through users for plans and groups
if($usersInAccountPlanQuery->num_rows > 0){

    for($j=0;$j<$usersInAccountPlanQuery->num_rows;$j++){
        $useridResult = $usersInAccountPlanQuery->fetch_assoc();
        //select user data
        $userinfoQ = select_user_from_userID($useridResult['user_id']);
        $userInfoR = $userinfoQ->fetch_assoc();
        //select user plans
        $userPlansQ = select_user_plans_within_account($useridResult['user_id'], $accountName);
        $planString = '';
        for($p=0;$p<$userPlansQ->num_rows;$p++){
            $userPlansR = $userPlansQ->fetch_assoc();
            if($p==0){
                $planString = $userPlansR['name'];
            }
            else{
                $planString = $planString . ';' . $userPlansR['name'];
            }
        }
        //select user groups
        $userGroupsQ = select_user_groups_within_account($useridResult['user_id'], $accountName);
        $groupString = '';
        for($g=0;$g<$userGroupsQ->num_rows;$g++){
            $userGroupsR = $userGroupsQ->fetch_assoc();
            if($g==0){
                $groupString = $userGroupsR['name'];
            }
            else{
                $groupString = $groupString . ';' . $userGroupsR['name'];
            }
        }

        //add user row to spreadsheet

        $spreadsheet->getSheet(0)->setCellValue('A' . $row, $userInfoR['username']);
        $spreadsheet->getSheet(0)->setCellValue('B' . $row, $userInfoR['firstname']);
        $spreadsheet->getSheet(0)->setCellValue('C' . $row, $userInfoR['middlename']);
        $spreadsheet->getSheet(0)->setCellValue('D' . $row, $userInfoR['lastname']);
        $spreadsheet->getSheet(0)->setCellValue('E' . $row, $userInfoR['title']);
        $spreadsheet->getSheet(0)->setCellValue('F' . $row, $userInfoR['phone']);
        $spreadsheet->getSheet(0)->setCellValue('G' . $row, $userInfoR['landline_phone']);
        $spreadsheet->getSheet(0)->setCellValue('H' . $row, $userInfoR['company']);
        $spreadsheet->getSheet(0)->setCellValue('I' . $row, $userInfoR['address']);
        $spreadsheet->getSheet(0)->setCellValue('J' . $row, $userInfoR['city']);
        $spreadsheet->getSheet(0)->setCellValue('K' . $row, $userInfoR['state']);
        $spreadsheet->getSheet(0)->setCellValue('L' . $row, $userInfoR['country']);
        $spreadsheet->getSheet(0)->setCellValue('M' . $row, $userInfoR['zip']);
        $spreadsheet->getSheet(0)->setCellValue('N' . $row, $userInfoR['activate_email']);
        $spreadsheet->getSheet(0)->setCellValue('O' . $row, $userInfoR['no_logout']);
        $spreadsheet->getSheet(0)->setCellValue('P' . $row, $userInfoR['admin_user']);
        $spreadsheet->getSheet(0)->setCellValue('Q' . $row, $planString);
        $spreadsheet->getSheet(0)->setCellValue('R' . $row, $groupString);
        $spreadsheet->getSheet(0)->setCellValue('S' . $row, '0');
        $spreadsheet->getSheet(0)->setCellValue('T' . $row, '0');

        $row++;
    }

}

//select deactivated users
$deactivatedUsersQuery = select_users_in_account_plan_with_no_actives($accountName);
if($deactivatedUsersQuery->num_rows > 0){

    for($j=0;$j<$deactivatedUsersQuery->num_rows;$j++){
        $deactivatedUsers = $deactivatedUsersQuery->fetch_assoc();
        //select user data
        $userinfoQ = select_user_from_userID($deactivatedUsers['user_id']);
        $userInfoR = $userinfoQ->fetch_assoc();
        //select user plans
//        $userPlansQ = select_user_plans_within_account($deactivatedUsers['user_id'], $accountName);
        $planString = '';

        //select user groups
//        $userGroupsQ = select_user_groups_within_account($deactivatedUsers['user_id'], $accountName);
        $groupString = '';


        //add user row to spreadsheet

        $spreadsheet->getSheet(0)->setCellValue('A' . $row, $userInfoR['username']);
        $spreadsheet->getSheet(0)->setCellValue('B' . $row, $userInfoR['firstname']);
        $spreadsheet->getSheet(0)->setCellValue('C' . $row, $userInfoR['middlename']);
        $spreadsheet->getSheet(0)->setCellValue('D' . $row, $userInfoR['lastname']);
        $spreadsheet->getSheet(0)->setCellValue('E' . $row, $userInfoR['title']);
        $spreadsheet->getSheet(0)->setCellValue('F' . $row, $userInfoR['phone']);
        $spreadsheet->getSheet(0)->setCellValue('G' . $row, $userInfoR['landline_phone']);
        $spreadsheet->getSheet(0)->setCellValue('H' . $row, $userInfoR['company']);
        $spreadsheet->getSheet(0)->setCellValue('I' . $row, $userInfoR['address']);
        $spreadsheet->getSheet(0)->setCellValue('J' . $row, $userInfoR['city']);
        $spreadsheet->getSheet(0)->setCellValue('K' . $row, $userInfoR['state']);
        $spreadsheet->getSheet(0)->setCellValue('L' . $row, $userInfoR['country']);
        $spreadsheet->getSheet(0)->setCellValue('M' . $row, $userInfoR['zip']);
        $spreadsheet->getSheet(0)->setCellValue('N' . $row, $userInfoR['activate_email']);
        $spreadsheet->getSheet(0)->setCellValue('O' . $row, $userInfoR['no_logout']);
        $spreadsheet->getSheet(0)->setCellValue('P' . $row, $userInfoR['admin_user']);
        $spreadsheet->getSheet(0)->setCellValue('Q' . $row, $planString);
        $spreadsheet->getSheet(0)->setCellValue('R' . $row, $groupString);
        $spreadsheet->getSheet(0)->setCellValue('S' . $row, '0');
        $spreadsheet->getSheet(0)->setCellValue('T' . $row, '0');

        $row++;
    }

}




//save file to server
$writer = new PhpOffice\PhpSpreadsheet\Writer\Csv($spreadsheet);
$writer->setEnclosureRequired(false);
$fileSaveName = "/export_".date("Y_m_d_h_i");
$FullFileName = $rootPath.'/'.$accountName.$fileSaveName.".csv";
$writer->save($FullFileName);

//submit file to user

if($allowed && $accountName != null) {
    //download file
    if (isset($accountName)) {
        $file = $FullFileName;
        if (file_exists($file) && is_readable($file)) {
            header('Content-Type: application/csv; charset=utf-8');
            header("Content-Disposition: attachment; filename=".$fileSaveName.".csv");
            readfile($file);
        }
    }
}
