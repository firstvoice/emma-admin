<?php include('include/pre_login_header.php'); ?>

<!DOCTYPE html>

<html class="no-js" lang="en-US" dir="ltr">

<head>
    <title>EMMA Videos</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #000000;">


<?php include('include/index_top_bar.php'); ?>

<div class="row expanded tss-theater">
    <div class="large-12 medium-12 small-12 columns">
        <h1>EMMA Demonstration Videos</h1>
    </div>
    <div class="large-12 medium-12 small-12 columns">
        <div class="row expanded">
            <div class="large-6 medium-12 small-12 column">
                &nbsp;
            </div>

            <div class="large-2 medium-12 small-12 column">
                <div class="flex-video">
                <iframe width="420" height="315"
                        src="https://www.youtube.com/embed/gfmvFLBrMvI?controls=1&showinfo=0&rel=0&autoplay=0&loop=1&mute=0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen style="border:0"></iframe>
                </div>
                <h5>Education on Personal Safety Apps - We Demonstrate 911 With EMMA</h5>
            </div>

            <div class="large-2 medium-12 small-12 column">
                <div class="flex-video">
                <iframe width="420" height="315"
                        src="https://www.youtube.com/embed/LB9giulp13o?controls=1&showinfo=0&rel=0&autoplay=0&loop=1&mute=0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen style="border:0"></iframe>
                </div>
                <h5>Education on a Safer Workplace - We Demonstrate EMMA SOS!</h5>
            </div>

            <div class="large-2 medium-12 small-12 column">
                <div class="flex-video">
                <iframe width="420" height="315"
                        src="https://www.youtube.com/embed/hcFH4WA17mE?controls=1&showinfo=0&rel=0&autoplay=0&loop=1&mute=0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen style="border:0"></iframe>
                </div>
                <h5>Communication Organization-Wide - We Demonstrate EMMA's See Something Say Something</h5>
            </div>
        </div>
    </div>

    <div class="large-12 medium-12 small-12 columns">
        <h1>EMMA Information Webinar Videos</h1>
    </div>
    <div class="large-12 medium-12 small-12 columns">
        <div class="row expanded">

            <div class="large-2 medium-12 small-12 column">

            </div>

            <div class="large-2 medium-12 small-12 column">
                <div class="flex-video">
                    <iframe width="420" height="315"
                            src="https://www.youtube.com/embed/EKCIVW7eZyw?controls=1&showinfo=0&rel=0&autoplay=0&loop=1&mute=0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen style="border:0"></iframe>
                </div>
                <h5>EMMA Mass Notification 2020</h5>

            <div class="large-2 medium-12 small-12 column">
                <div class="flex-video">
                <iframe width="420" height="315"
                        src="https://www.youtube.com/embed/8oADnGsSGtQ?controls=1&showinfo=0&rel=0&autoplay=0&loop=1&mute=0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen style="border:0"></iframe>
                </div>
                <h5>EMMA Webinar - EMMA Mass Notification</h5>
            </div>
            <div class="large-2 medium-12 small-12 column">
                <div class="flex-video">
                <iframe width="420" height="315"
                        src="https://www.youtube.com/embed/78e6-kMZjLQ?controls=1&showinfo=0&rel=0&autoplay=0&loop=1&mute=0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen style="border:0"></iframe>
                </div>
                <h5>EMMA Webinar - EMMA SOS!</h5>
            </div>

            <div class="large-2 medium-12 small-12 column">
                <div class="flex-video">
                <iframe width="420" height="315"
                        src="https://www.youtube.com/embed/o2TC8fJx9gM?controls=1&showinfo=0&rel=0&autoplay=0&loop=1&mute=0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen style="border:0"></iframe>
                </div>
                <h5>EMMA Webinar - EMMA See Something Say Something</h5>
            </div>
            <div class="large-2 medium-12 small-12 column">
                <div class="flex-video">
                <iframe width="420" height="315"
                        src="https://www.youtube.com/embed/D645rZ4_BQM?controls=1&showinfo=0&rel=0&autoplay=0&loop=1&mute=0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen style="border:0">
                </iframe>
                </div>
                <h5>EMMA Webinar EMMA Response</h5>
            </div>
        </div>
    </div>
   <div><p>&nbsp;</p><br /><br /><br /><br /><br /><br /><br /></div>
</div>
<?php include('include/footer.php'); ?>

<?php include('include/pre_login_modals.php'); ?>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>


