<?php include('include/pre_login_header.php'); ?>
<!DOCTYPE html>

<html class="no-js" lang="en-US" dir="ltr">

<head>
    <title>EMMA Login</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" type="text/css" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #C2E0F6;">



<?php include('include/index_top_bar.php'); ?>
<div class="alert_message" style="padding: 10px">
    <?php include('website_alert_message.php');?>
</div>
<!--<div class="row expanded the-clock">-->
<!--    <div class="large-12 medium-12 small-12 columns" id="MMM_column_row_index">-->
<!--        <p class="MMM-header">Making Minutes Matter!</p>-->
<!--        <p class="hp-verbiage-lg">NOTIFY. RESPOND. COMMUNICATE.</p>-->
<!--    </div>-->
<!--    <div class="large-12 medium-12 small-12 columns innerclock">-->
<!--        <div class="row expanded" id="emma_devices_index">-->
<!--            <div class="large-8 medium-12 small-12 column">-->
<!---->
<!--                <img src="img/emma-devices.png" width="75%" height="75%"/>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<div class="row expanded">
    <div class="large-12 medium-12 small-12 columns">
        <div class="row expanded">
            <div class="large-3 medium-6 small-12 columns" style="margin: 0 auto; padding-top: 3%">
                <div class="row" style="padding-bottom: 10px">
                    <div class="large-12 medium-6 small-6 columns">
                        <img src="img/emma_logo.png" height="90%" width="90%">
                    </div>
                </div>
                <div class="row" style="background-color: white; padding-top: 5px">
                    <div class="large-12 columns">
                        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" id="login-form" class="text-left" method="post">
                            <div class="row expanded">
                                <div class="large-12 columns">
                                    <label>Username or Email Address
                                        <input type="text" name="username" id="username" required/>
                                    </label>
                                </div>
                            </div>
                            <div class="row expanded">
                                <div class="large-12 columns">
                                    <label>Password
                                        <input type="password" name="password" id="password" required/>
                                    </label>
                                </div>
                            </div>
                            <div class="row expanded">
                                <div class="large-6 medium-12 small-12 columns">
                                    <input type="checkbox" id="remember-me" name="remember-me" checked>
                                    <label>Remember me</label>
                                </div>
                                <div class="large-6 medium-12 small-12 columns">
                                    <div class="button-group radius" style="float: right">
                                        <input class="button radius" type="submit" name="login" id="login" value="Log In"/>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row expanded" style="padding-top: 20px">
                    <div class="large-12 columns text-center">
                        <a data-open="forgot-password-modal">Lost Your Password?</a>
                    </div>
                </div>
                <div class="row expanded" style="padding-top: 20px">
                    <div class="large-12 columns text-center">
                        <a href="https://emmanotify.com" target="_blank" class="button">Learn More</a>
                        <a href="faq.php" target="_blank" class="button">FAQ</a>
                        <a data-open="contact-us-modal" class="button">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('include/footer.php'); ?>
<?php include('include/pre_login_modals.php'); ?>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>


