<?php

if (!($USER->privilege->admin)) {
    redirect();
}

?>

<div class="title row expanded align-middle">
    <div class="columns medium-4">
        <h2 class="text-left"><a href="./dashboard.php?content=admin_users"><i class="page-icon fa fa-id-card"></i> All Plan Users</a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
        <?php
        //only enable if plan has a csv folder
        $csvFolderQuery = select_plan_from_planID($USER->emma_plan_id);
        if($csvFolderQuery->num_rows > 0) {
            $csvFolderResult = $csvFolderQuery->fetch_assoc();
            if(!empty($csvFolderResult['user_upload_folder'])) {
        ?>
                <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
                    data-options="disableHover:true;clickOpen:true;">
                    <li>
                        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
                        <ul class="menu">
                            <li><a href="./dashboard.php?content=user_csv_upload" id="user_create">Upload CSV File</a>
                            </li>
                        </ul>
                    </li>
                </ul>
        <?php
            }
        }

        ?>
    </div>
</div>
<div class="large-12 medium-12 column text-right" style="padding-top: 5px">
    <a data-open="delete-user-confirm" class="button" type="button" style="margin: 0">Deactivate</a>
</div>

<div>
    <div class="tabs-content" data-tabs-content="script-type-tabs">
        <div id="users-active" class="tabs-panel is-active">
            <form id="users_form">
                <table id="users-table" class="data-table" style="width:100%">
                    <thead>
                    <tr>
                        <th class="text-search">Username</th>
                        <th class="text-search">First Name</th>
                        <th class="text-search">Last Name</th>
                        <th class="text-search">Group</th>
                        <th class="text-search">Plan</th>
                    </tr>
                    <tr>
                        <th>Username</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Group</th>
                        <th>Plan</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </form>
        </div>
    </div>
</div>


<div id="no-selected-users" class="reveal callout small" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>No users selected.</h4>
    <div class="text-center small-12">
        <a href="./dashboard.php?content=admin_users" data-close class="button">Ok</a>
    </div>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div id="success_modal" class="reveal callout success text-center tiny" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <a href="./dashboard.php?content=admin_users" data-close class="button success">Ok</a>
    <!--  <button class="close-button" data-close aria-label="Close reveal" type="button">-->
    <!--    <span aria-hidden="true">&times;</span>-->
    <!--  </button>-->
</div>

<div id="delete-user-confirm" class="reveal callout tiny text-center" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Are You Sure?</h4>
    <div class="text-center small-12">
        <a class="button alert" data-close>Cancel</a>
        <a id="delete-users" class="button">Ok</a>
    </div>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
