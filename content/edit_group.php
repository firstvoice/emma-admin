<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/7/2017
 * Time: 12:42 PM
 */

$group_id = $fvmdb->real_escape_string($_GET['id']);
$privilege_list = array();
$icons_list = array();

$groups = select_group_with_groupID($group_id);
//    $fvmdb->query("
//    SELECT g.*
//    FROM emma_groups AS g
//    WHERE g.emma_group_id = " . $group_id . "
//");
$group = $groups->fetch_assoc();


$lockdownperm = select_group_lockdown_permission($group_id);

?>
<form id="edit-group-form">
  <input type="hidden" name="id" value="<?php echo $group_id; ?>">
  <input type="hidden" name="edit-id" value="<?php echo $USER->id; ?>">
  <input type="hidden" name="plan-id" value="<?php echo $USER->emma_plan_id; ?>">
  <div class="title row expanded align-middle">
    <div class="columns medium-6">
      <h2 class="text-left"><a href="./dashboard.php?content=groups"><i class="page-icon fa fa-group"></i> Edit Group - <?php echo $group['name']; ?></a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
      <input type="submit" class="button" value="Save" style="margin:0;">
    </div>
  </div>

  <div class="row expanded">
    <div class="small-12 columns">
      <div class="card-info primary">
        <div class="card-info-content">
          <h3 class="lead">Permissions</h3>
          <div class="row expanded">
            <div class="large-2 medium-6 small-12 columns">
              <div class="row">
                <div class="large-4 columns">
                  <div class="switch tiny">
                    <input class="switch-input " id="administrator"
                      type="checkbox"
                      name="administrator" <?php if ($group['admin']) echo 'checked'; ?>>
                    <label class="switch-paddle " for="administrator">
                      <span class="show-for-sr">Administrator?</span>
                      <span class="switch-active" aria-hidden="true">Yes</span>
                      <span class="switch-inactive" aria-hidden="true">No</span>
                    </label>
                  </div>
                </div>
                <div class="large-8 columns">
                  <label class="text-left">Administrator</label>
                </div>
              </div>
            </div>
            <div class="large-2 medium-6 small-12 columns">
              <div class="row">
                <div class="large-4 columns">
                  <div class="switch tiny">
                    <input class="switch-input " id="event-info-only"
                      type="checkbox"
                      name="event-info-only" <?php if ($group['info_only']) echo 'checked'; ?>>
                    <label class="switch-paddle " for="event-info-only">
                      <span class="show-for-sr">Event Information Only?</span>
                      <span class="switch-active" aria-hidden="true">Yes</span>
                      <span class="switch-inactive" aria-hidden="true">No</span>
                    </label>
                  </div>
                </div>
                <div class="large-8 columns">
                  <label class="text-left">Information Only</label>
                </div>
              </div>
            </div>
              <?php
              if($USER->privilege->plansecurity) {
                  ?>
                  <div class="large-2 medium-6 small-12 columns">
                      <div class="row">
                          <div class="large-4 columns">
                              <div class="switch tiny">
                                  <input class="switch-input " id="security"
                                         type="checkbox"
                                         name="security" <?php if ($group['security']) echo 'checked'; ?>>
                                  <label class="switch-paddle " for="security">
                                      <span class="show-for-sr">Security?</span>
                                      <span class="switch-active" aria-hidden="true">Yes</span>
                                      <span class="switch-inactive" aria-hidden="true">No</span>
                                  </label>
                              </div>
                          </div>
                          <div class="large-8 columns">
                              <label class="text-left">Security</label>
                          </div>
                      </div>
                  </div>
                  <?php
              }
              if($USER->privilege->plansos) {
                  ?>
                  <div class="large-2 medium-6 small-12 columns">
                      <div class="row">
                          <div class="large-4 columns">
                              <div class="switch tiny">
                                  <input class="switch-input " id="sos"
                                         type="checkbox"
                                         name="sos" <?php if ($group['emma_sos']) echo 'checked'; ?>>
                                  <label class="switch-paddle " for="sos">
                                      <span class="show-for-sr">SOS?</span>
                                      <span class="switch-active" aria-hidden="true">Yes</span>
                                      <span class="switch-inactive" aria-hidden="true">No</span>
                                  </label>
                              </div>
                          </div>
                          <div class="large-8 columns">
                              <label class="text-left">EMMA SOS</label>
                          </div>
                      </div>
                  </div>
                  <?php
              }
              ?>
              <div class="large-2 medium-6 small-12 columns">
                  <div class="row">
                      <div class="large-4 columns">
                          <div class="switch tiny">
                              <input class="switch-input " id="911admin"
                                     type="checkbox"
                                     name="911admin" <?php if ($group['911admin']) echo 'checked'; ?>>
                              <label class="switch-paddle " for="911admin">
                                  <span class="show-for-sr">SOS?</span>
                                  <span class="switch-active" aria-hidden="true">Yes</span>
                                  <span class="switch-inactive" aria-hidden="true">No</span>
                              </label>
                          </div>
                      </div>
                      <div class="large-8 columns">
                          <label class="text-left">911admin</label>
                      </div>
                  </div>
              </div>
              <?php
              if($USER->privilege->planlockdown) {
                  ?>
                  <div class="large-2 medium-4 small-12 columns">
                      <div class="row">
                          <div class="large-4 columns">
                              <div class="switch tiny">
                                  <input class="switch-input" id="receivelockdown"
                                         type="checkbox"
                                         name="receivelockdown" <?php if ($lockdownperm->num_rows > 0) echo 'checked'; ?>
                                         >
                                  <label class="switch-paddle" for="receivelockdown">
                                      <span class="show-for-sr">Receive Lockdown</span>
                                      <span class="switch-active" aria-hidden="true">Yes</span>
                                      <span class="switch-inactive" aria-hidden="true">No</span>
                                  </label>
                              </div>
                          </div>
                          <div class="large-8 columns">
                              <label class="text-left">Receive Lockdown</label>
                          </div>
                      </div>
                  </div>
                  <?php
              }
              ?>
          </div>
        </div>
      </div>
    </div>
    <div class="small-12 columns">
      <div class="card-info success">
        <div class="card-info-content">
          <h3 class="lead">Messaging Authority</h3>
          <div class="row">
            <?php
            $allgroups = select_groups_with_planID($USER->emma_plan_id);
//                $fvmdb->query("
//              SELECT g.*
//              FROM emma_groups AS g
//              WHERE g.emma_plan_id = '".$USER->emma_plan_id."'
//              ORDER BY g.name
//            ");
            while ($allgroup = $allgroups->fetch_assoc()) {
              echo '<div class="large-3 medium-6 small-12 columns"><div class="row"> ';

              $group_privileges = select_editGroup_groupPrivileges($group_id, $allgroup['emma_group_id']);
//                  $fvmdb->query("
//                SELECT p.*
//                FROM emma_group_privileges AS p
//                WHERE p.emma_group_id = " . $group_id . "
//                AND p.emma_privilege_id = " . $allgroup['emma_group_id'] . "
//              ");
              if ($group_privileges->num_rows > 0) {
                $privilege_list[] = $allgroup['emma_group_id'];
                echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="group-id-' . $allgroup['emma_group_id'] . '" name="groups[' . $allgroup['emma_group_id'] . ']" type="checkbox" value="' . $allgroup['emma_group_id'] . '" checked>
                          <label class="switch-paddle" for="group-id-' . $allgroup['emma_group_id'] . '">
                              <span class="show-for-sr">' . $allgroup['name'] . '</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">' . $allgroup['name'] . '</label>
                  </div>';
              } else {
                echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="group-id-' . $allgroup['emma_group_id'] . '" name="groups[' . $allgroup['emma_group_id'] . ']" type="checkbox" value="' . $allgroup['emma_group_id'] . '">
                          <label class="switch-paddle" for="group-id-' . $allgroup['emma_group_id'] . '">
                              <span class="show-for-sr">' . $allgroup['name'] . '</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">' . $allgroup['name'] . '</label>
                  </div>';
              }
              echo '</div></div>';
            }
            ?>

          </div>
        </div>
      </div>
    </div>
      <?php
      $feedprivileges = select_feed_privileges_withPlan($USER->emma_plan_id);
      $feedprivilege = $feedprivileges->fetch_assoc();
      if($feedprivilege['feed_toggle'] == '1' && $feedprivilege['feed_timer_minutes'] > 0) {
          ?>
          <div class="small-12 columns">
              <div class="card-info success">
                  <div class="card-info-content">
                      <h3 class="lead">Feed Authority</h3>
                      <div class="row">
                          <?php
                          $allgroups = select_groups_with_planID($USER->emma_plan_id);
                          //                $fvmdb->query("
                          //              SELECT g.*
                          //              FROM emma_groups AS g
                          //              WHERE g.emma_plan_id = '".$USER->emma_plan_id."'
                          //              ORDER BY g.name
                          //            ");
                          while ($allgroup = $allgroups->fetch_assoc()) {
                              echo '<div class="large-3 medium-6 small-12 columns"><div class="row"> ';

                              $group_privileges = select_editGroup_feedPrivileges($group_id, $allgroup['emma_group_id']);
//                  $fvmdb->query("
//                SELECT p.*
//                FROM emma_group_privileges AS p
//                WHERE p.emma_group_id = " . $group_id . "
//                AND p.emma_privilege_id = " . $allgroup['emma_group_id'] . "
//              ");
                              if ($group_privileges->num_rows > 0) {
                                  $privilege_list[] = $allgroup['emma_group_id'];
                                  echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="group-feed-id-' . $allgroup['emma_group_id'] . '" name="groupfeeds[' . $allgroup['emma_group_id'] . ']" type="checkbox" value="' . $allgroup['emma_group_id'] . '" checked>
                          <label class="switch-paddle" for="group-feed-id-' . $allgroup['emma_group_id'] . '">
                              <span class="show-for-sr">' . $allgroup['name'] . '</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">' . $allgroup['name'] . '</label>
                  </div>';
                              } else {
                                  echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="group-feed-id-' . $allgroup['emma_group_id'] . '" name="groupfeeds[' . $allgroup['emma_group_id'] . ']" type="checkbox" value="' . $allgroup['emma_group_id'] . '">
                          <label class="switch-paddle" for="group-feed-id-' . $allgroup['emma_group_id'] . '">
                              <span class="show-for-sr">' . $allgroup['name'] . '</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">' . $allgroup['name'] . '</label>
                  </div>';
                              }
                              echo '</div></div>';
                          }
                          ?>

                      </div>
                  </div>
              </div>
          </div>
          <?php
      }
      ?>
    <div class="small-12 columns">
      <div class="card-info alert">
        <div class="card-info-content">
          <h3 class="lead">Emergencies</h3>
          <div class="row">
            <?php
            $alltypes = select_editGroup_allTypes($USER->emma_plan_id);
//                $fvmdb->query("
//              SELECT e.img_filename, e.name, e.emergency_type_id
//              FROM emma_plan_event_types AS g
//              JOIN emergency_types e ON g.emma_emergency_type_id = e.emergency_type_id
//              WHERE g.emma_plan_id = '".$USER->emma_plan_id."'
//              ORDER BY e.name
//            ");
            while ($alltype = $alltypes->fetch_assoc()) {
              echo '<div class="large-3 medium-6 small-12 columns"><div class="row"> ';

              $group_icons = select_editGroup_groupIcons($group_id, $alltype['emergency_type_id']);
//                  $fvmdb->query("
//                SELECT p.*
//                FROM emma_group_events AS p
//                WHERE p.emma_group_id = " . $group_id . "
//                AND p.event_id = " . $alltype['emergency_type_id'] . "
//                AND p.active = 1
//              ");
              if ($group_icons->num_rows > 0) {
                $icons_list[] = $alltype['emergency_type_id'];
                echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="icon-id-' . $alltype['emergency_type_id'] . '" name="icons[' . $alltype['emergency_type_id'] . ']" type="checkbox" value="' . $alltype['emergency_type_id'] . '" checked>
                          <label class="switch-paddle" for="icon-id-' . $alltype['emergency_type_id'] . '">
                              <span class="show-for-sr">' . $alltype['name'] . '</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">' . $alltype['name'] . '</label>
                  </div>';
              } else {
                echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="icon-id-' . $alltype['emergency_type_id'] . '" name="icons[' . $alltype['emergency_type_id'] . ']" type="checkbox" value="' . $alltype['emergency_type_id'] . '">
                          <label class="switch-paddle" for="icon-id-' . $alltype['emergency_type_id'] . '">
                              <span class="show-for-sr">' . $alltype['name'] . '</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">' . $alltype['name'] . '</label>
                  </div>';
              }
              echo '</div></div>';
            }

            //specialty events
            $planDetails = select_plan_from_planID($USER->emma_plan_id);
           if($planDetail = $planDetails->fetch_assoc()){

               if($planDetail['mass_communication']){
                   echo '<div class="large-3 medium-6 small-12 columns"><div class="row"> ';

                   $group_icons = select_editGroup_groupIcons($group_id, -2);
//                  $fvmdb->query("
//                SELECT p.*
//                FROM emma_group_events AS p
//                WHERE p.emma_group_id = " . $group_id . "
//                AND p.event_id = " . $alltype['emergency_type_id'] . "
//                AND p.active = 1
//              ");
                   if ($group_icons->num_rows > 0) {
                       $icons_list[] = -2;
                       echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="icon-id-' . -2 . '" name="icons[' . -2 . ']" type="checkbox" value="' . -2 . '" checked>
                          <label class="switch-paddle" for="icon-id-' . -2 . '">
                              <span class="show-for-sr">Mass Notification</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">Mass Notification</label>
                  </div>';
                   } else {
                       echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="icon-id-' . -2 . '" name="icons[' . -2 . ']" type="checkbox" value="' . -2 . '">
                          <label class="switch-paddle" for="icon-id-' . -2 . '">
                              <span class="show-for-sr">Mass Notification</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">Mass Notification</label>
                  </div>';
                   }
                   echo '</div></div>';
               }
               if($planDetail['securities']){
                   echo '<div class="large-3 medium-6 small-12 columns"><div class="row"> ';

                   $group_icons = select_editGroup_groupIcons($group_id, 0);
//                  $fvmdb->query("
//                SELECT p.*
//                FROM emma_group_events AS p
//                WHERE p.emma_group_id = " . $group_id . "
//                AND p.event_id = " . $alltype['emergency_type_id'] . "
//                AND p.active = 1
//              ");
                   if ($group_icons->num_rows > 0) {
                       $icons_list[] = 0;
                       echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="icon-id-' . 0 . '" name="icons[' . 0 . ']" type="checkbox" value="' . 0 . '" checked>
                          <label class="switch-paddle" for="icon-id-' . 0 . '">
                              <span class="show-for-sr">Securities</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">Securities</label>
                  </div>';
                   } else {
                       echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="icon-id-' . 0 . '" name="icons[' . 0 . ']" type="checkbox" value="' . 0 . '">
                          <label class="switch-paddle" for="icon-id-' . 0 . '">
                              <span class="show-for-sr">Securities</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">Securities</label>
                  </div>';
                   }
                   echo '</div></div>';
               }
               if($planDetail['panic']){
                   echo '<div class="large-3 medium-6 small-12 columns"><div class="row"> ';

                   $group_icons = select_editGroup_groupIcons($group_id, -1);
//                  $fvmdb->query("
//                SELECT p.*
//                FROM emma_group_events AS p
//                WHERE p.emma_group_id = " . $group_id . "
//                AND p.event_id = " . $alltype['emergency_type_id'] . "
//                AND p.active = 1
//              ");
                   if ($group_icons->num_rows > 0) {
                       $icons_list[] = -1;
                       echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="icon-id-' . -1 . '" name="icons[' . -1 . ']" type="checkbox" value="' . -1 . '" checked>
                          <label class="switch-paddle" for="icon-id-' . -1 . '">
                              <span class="show-for-sr">SOS</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">SOS</label>
                  </div>';
                   } else {
                       echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="icon-id-' . -1 . '" name="icons[' . -1 . ']" type="checkbox" value="' . -1 . '">
                          <label class="switch-paddle" for="icon-id-' . -1 . '">
                              <span class="show-for-sr">SOS</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">SOS</label>
                  </div>';
                   }
                   echo '</div></div>';
               }
               if($planDetail['geofencing']){
                   echo '<div class="large-3 medium-6 small-12 columns"><div class="row"> ';

                   $group_icons = select_editGroup_groupIcons($group_id, -4);
//                  $fvmdb->query("
//                SELECT p.*
//                FROM emma_group_events AS p
//                WHERE p.emma_group_id = " . $group_id . "
//                AND p.event_id = " . $alltype['emergency_type_id'] . "
//                AND p.active = 1
//              ");
                   if ($group_icons->num_rows > 0) {
                       $icons_list[] = -4;
                       echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="icon-id-' . -4 . '" name="icons[' . -4 . ']" type="checkbox" value="' . -4 . '" checked>
                          <label class="switch-paddle" for="icon-id-' . -4 . '">
                              <span class="show-for-sr">Geofence</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">Geofence</label>
                  </div>';
                   } else {
                       echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="icon-id-' . -4 . '" name="icons[' . -4 . ']" type="checkbox" value="' . -4 . '">
                          <label class="switch-paddle" for="icon-id-' . -4 . '">
                              <span class="show-for-sr">Geofence</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">Geofence</label>
                  </div>';
                   }
                   echo '</div></div>';
               }
               if($planDetail['lockdown']){
                   echo '<div class="large-3 medium-6 small-12 columns"><div class="row"> ';

                   $group_icons = select_editGroup_groupIcons($group_id, -5);
//                  $fvmdb->query("
//                SELECT p.*
//                FROM emma_group_events AS p
//                WHERE p.emma_group_id = " . $group_id . "
//                AND p.event_id = " . $alltype['emergency_type_id'] . "
//                AND p.active = 1
//              ");
                   if ($group_icons->num_rows > 0) {
                       $icons_list[] = -5;
                       echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="icon-id-' . -5 . '" name="icons[' . -5 . ']" type="checkbox" value="' . -5 . '" checked>
                          <label class="switch-paddle" for="icon-id-' . -5 . '">
                              <span class="show-for-sr">LOCKDOWN!</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">LOCKDOWN!</label>
                  </div>';
                   } else {
                       echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="icon-id-' . -5 . '" name="icons[' . -5 . ']" type="checkbox" value="' . -5 . '">
                          <label class="switch-paddle" for="icon-id-' . -5 . '">
                              <span class="show-for-sr">LOCKDOWN!</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">LOCKDOWN!</label>
                  </div>';
                   }
                   echo '</div></div>';
                   echo '<div class="large-3 medium-6 small-12 columns"><div class="row"> ';

                   $group_icons = select_editGroup_groupIcons($group_id, -6);
//                  $fvmdb->query("
//                SELECT p.*
//                FROM emma_group_events AS p
//                WHERE p.emma_group_id = " . $group_id . "
//                AND p.event_id = " . $alltype['emergency_type_id'] . "
//                AND p.active = 1
//              ");
                   if ($group_icons->num_rows > 0) {
                       $icons_list[] = -6;
                       echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="icon-id-' . -6 . '" name="icons[' . -6 . ']" type="checkbox" value="' . -6 . '" checked>
                          <label class="switch-paddle" for="icon-id-' . -6 . '">
                              <span class="show-for-sr">LOCKDOWN!(DRILL)</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">LOCKDOWN!(DRILL)</label>
                  </div>';
                   } else {
                       echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="icon-id-' . -6 . '" name="icons[' . -6 . ']" type="checkbox" value="' . -6 . '">
                          <label class="switch-paddle" for="icon-id-' . -6 . '">
                              <span class="show-for-sr">LOCKDOWN!(DRILL)</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">LOCKDOWN!(DRILL)</label>
                  </div>';
                   }
                   echo '</div></div>';
               }
               if($planDetail['ar']){

                   echo '<div class="large-3 medium-6 small-12 columns"><div class="row"> ';

                   $group_icons = select_editGroup_groupIcons($group_id, -6);
//                  $fvmdb->query("
//                SELECT p.*
//                FROM emma_group_events AS p
//                WHERE p.emma_group_id = " . $group_id . "
//                AND p.event_id = " . $alltype['emergency_type_id'] . "
//                AND p.active = 1
//              ");
                   if ($group_icons->num_rows > 0) {
                       $icons_list[] = -7;
                       echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="icon-id-' . -7 . '" name="icons[' . -7 . ']" type="checkbox" value="' . -7 . '" checked>
                          <label class="switch-paddle" for="icon-id-' . -7 . '">
                              <span class="show-for-sr">Anonymous Report</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">Anonymous Report</label>
                  </div>';
                   } else {
                       echo '
                  <div class="small-4 column">
                      <div class="switch tiny">
                          <input class="switch-input" id="icon-id-' . -7 . '" name="icons[' . -7 . ']" type="checkbox" value="' . -7 . '">
                          <label class="switch-paddle" for="icon-id-' . -7 . '">
                              <span class="show-for-sr">Anonymous Report</span>
                          </label>
                      </div>
                  </div>
                  <div class="small-8 columns">
                    <label class="text-left">Anonymous Report</label>
                  </div>';
                   }
                   echo '</div></div>';
               }
            }


            ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</form>

<div id="success-modal" class="reveal success callout tiny text-center"
  data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Success</h4>
  <div class="row columns">
    <a
      href="./dashboard.php?content=group&id=<?php echo $group['emma_group_id']; ?>"
      data-close class="button success"
      style="margin-left:auto;margin-right:auto">Ok</a>
  </div>
<!--  <button class="close-button" data-close aria-label="Close reveal"-->
<!--    type="button">-->
<!--    <span aria-hidden="true">&times;</span>-->
<!--  </button>-->
</div>