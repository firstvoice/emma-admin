<?php

if (!($USER->privilege->admin && $USER->privilege->code_generator)) {
    redirect();
}

?>
<div class="title row expanded align-middle">
    <div class="columns medium-3">
        <h2 class="text-left"><a href="./dashboard.php?content=guest_codes"><i class="page-icon fa fa-ticket"></i> <?php echo ucwords(str_replace('_',' ',$content)); ?></a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
        <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
            data-options="disableHover:true;clickOpen:true;">
            <li>
                <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
                <ul class="menu">
                    <li><a href="./dashboard.php?content=code_generator" id="guest_codes_create">Create New Code</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<div>
    <form id="guest_codes_form">
        <table id="guest_codes-table" class="data-table" style="width:100%">
            <thead>
            <tr>
                <th class="text-search">Guest Code</th>
                <th class="text-search">Date Span</th>
                <th></th>
                <th class="text-search">Group</th>
                <th></th>
            </tr>
            <tr>
                <th>Guest Code</th>
                <th>Date Span</th>
                <th>Duration</th>
                <th>Group</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>

            </tfoot>
        </table>
    </form>
</div>


<div id="success_modal" class="reveal callout success text-center tiny" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <a href="./dashboard.php?content=guest_codes" data-close class="button success">Ok</a>
<!--    <button class="close-button" data-close aria-label="Close reveal" type="button">-->
<!--        <span aria-hidden="true">&times;</span>-->
<!--    </button>-->
</div>
