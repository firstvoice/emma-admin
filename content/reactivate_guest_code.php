<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/14/2019
 * Time: 3:06 PM
 */
?>

<div class="title row expanded align-middle">
  <div class="columns medium-8">
    <h2 class="text-left"><a href="./dashboard.php?content=users"><i class="page-icon fa fa-user-circle"></i> <?php echo 'Reactivate Guest Code'; ?></a></h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
      data-options="disableHover:true;clickOpen:true;">
      <li>
        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <ul class="menu">
          <li><a href="./dashboard.php?content=create_user" id="user_create"  >Create User</a></li>
          <li><a href="./dashboard.php?content=assign_groups" id="user_create"  >Assign Users</a></li>
          <li><a href="./dashboard.php?content=remove_groups" id="user_create"  >Remove Users</a></li>
          <li><a href="./dashboard.php?content=deactivate_user" id="user_create"  >Deactivate User</a></li>
          <li><a href="./dashboard.php?content=reactivate_user" id="user_create"  >Reactivate User</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>

<div>
  <ul class="tabs" data-tabs id="script-type-tabs">
    <li class="tabs-title is-active">
      <a href="#users-guest" aria-selected="true">Guest</a>
    </li>
  </ul>
  <div class="tabs-content" data-tabs-content="script-type-tabs">
    <div id="users-guest" class="tabs-panel is-active">
      <form id="guest_users_form">
        <table id="guest-users-table" class="data-table" style="width:100%;">
          <thead>
          <tr>
              <th>Group</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Duration</th>
              <th>Status</th>
          </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
        <div class="text-center">
          <input type="submit" value="Reactivate" class="button">
        </div>
      </form>
    </div>
  </div>
</div>

<div id="no-selected-users" class="reveal callout small" data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>No users selected.</h4>
  <div class="text-center small-12">
    <a href="./dashboard.php?content=reactivate_users" data-close class="button" >Ok</a>
  </div>
  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<div id="success_modal" class="reveal success callout tiny text-center" data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Success</h4>
  <a href="./dashboard.php?content=users" data-close class="button success" style="margin-left:auto;margin-right:auto">Ok</a>
<!--  <button class="close-button" data-close aria-label="Close reveal" type="button">-->
<!--    <span aria-hidden="true">&times;</span>-->
<!--  </button>-->
</div>