<?php
?>









<div class="title row expanded align-middle">
    <div class="columns medium-5">
        <h2 class="text-left"><a href="./dashboard.php?content=resources"><i class="page-icon fa fa-map"></i> Resource
                Folder - Text Scripts</a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
        <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
            data-options="disableHover:true;clickOpen:true;">
            <li>
                <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
                <ul class="menu">
                    <li><a href="#" data-open="create-script-modal">New Script</a></li>
                    <!--                    <li><a href="#" data-open="import-script-modal">Import Script</a></li>-->
                </ul>
            </li>
        </ul>
    </div>
</div>

<div>
    <div class="large-8 columns">
        <table>
            <col width="20%">
            <col width="70%">
            <col width="10%">
            <thead>
            <tr>
                <th>Script Name</th>
                <th>Text</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $scripts = getTextScriptContent($USER->emma_plan_id);
            while($script = $scripts->fetch_assoc()){
                echo'
                <tr>
                    <td>'. $script['name'] .'</td>
                    <td>'. $script['text'] .'</td>
                    <td><div class="button-group" style="margin-top: 20px"><a class="button edit-script" data-emma-script-id="'. $script['emma_text_script_id'] .'" data-emma-script-name="'. $script['name'] .'" data-emma-script-text="'. $script['text'] .'">Edit</a><a class="button alert delete-script" data-emma-script-id="'. $script['emma_text_script_id'] .'">Delete</a></div></td>
                </tr>
                ';
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<div id="create-script-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Create Script</h4>
    <hr>
    <form id="create-script-form" class="text-left" action="#" method="post">
        <input name="plan-id" type="hidden" value="<?php echo $USER->emma_plan_id; ?>">
        <label>Name <span style="color: red">*</span>
            <input name="name" type="text"/>
        </label>
        <label>Text <span style="color: red">*</span>
            <textarea name="text" style="min-width:100%;height:5rem;"></textarea>
        </label>
        <div class="row">
            <div class="large-4 columns"></div>
            <div class="large-4 columns">
                <div class="button-group text-center">
                    <a class="button alert" data-close>Cancel</a>
                    <input type="submit" class="button" value="Submit" />
                </div>
            </div>
            <div class="large-4 columns"></div>
        </div>
    </form>
    <button class="close-button" onclick="location.reload();" aria-label="Close reveal" type="button">
        <span aria-hidden="true">×</span>
    </button>
</div>


<div style="width: 50%;" id="import-script-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Import Script</h4>
    <hr>
    <div class="large-10 columns row">
        <form style="display: none;" id="confirm-import-script-form" class="expanded row">

            <table style="display: none;" id="append-table-data-here">
                <tbody>
                <tr>
                    <th>Script Type</th>
                    <th>Broadcast Type</th>
                    <th>Group</th>
                    <th>Name</th>
                    <th>Text</th>
                </tr>
                </tbody>
            </table>
        </form>

    </div>
    <form id="upload-script-form" method="POST">
        <div class="row">
            <div class="large-4 large-offset-4 columns">
                <a class="button expanded success" href="documents/import-scripts-template.csv" download>Download Template</a>
            </div>
        </div>
        <div class="row">
            <div class="large-4 large-offset-4 columns">
                <label for="import-scripts-file" class="button expanded">Choose Upload File</label>
                <input type="file" id="import-scripts-file" name="import-scripts-file" class="show-for-sr"/>
            </div>
        </div>

        <div class="large-6 large-offset-3 columns">
            <div class="expanded button-group">
                <a data-close class="button alert hollow">Cancel</a>
                <input type="submit" class="button" value="Submit" />
            </div>
        </div>
        <div class="large-4 columns"></div>
    </form>
    <button class="close-button" onclick="location.reload();" aria-label="Close reveal" type="button">
        <span aria-hidden="true">×</span>
    </button>
</div>

<div id="success_modal" class="reveal success callout text-center tiny" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <a href="./dashboard.php?content=mass_notification_scripts" data-close class="button success">Ok</a>
    <!--  <button class="close-button" data-close aria-label="Close reveal" type="button">-->
    <!--    <span aria-hidden="true">&times;</span>-->
    <!--  </button>-->
</div>

<div id="delete-confirmation" class="reveal warning callout text-center tiny" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Confirm Delete</h4>
    <form id="delete-script-form" method="post" action="#">
        <input id="delete-script-id" name="delete-script-id" type="hidden">
        <p>Are you sure you want to delete this script?</p>
        <div class="button-group expanded">
            <a data-close="" class="button">Cancel</a>
            <input type="submit" class="button alert" value="OK"/>
        </div>
    </form>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div id="edit-script-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Edit Script</h4>
    <hr>
    <form class="edit-script-form" action="#" method="post">
        <input name="emma-script-id" id="edit-script-id" type="hidden" value="" />
        <label class="text-left">Name
            <input name="name" id="edit-script-name" type="text" value="" />
        </label>
        <label class="text-left">Text
            <textarea name="text" id="edit-script-text" style="min-width:100%;height:5rem;"></textarea>
        </label>
        <div class="row">
            <div class="large-4 columns"></div>
            <div class="large-4 columns">
                <div class="button-group text-center">
                    <a class="button alert" data-close>Cancel</a>
                    <input type="submit" class="button" value="Submit" />
                </div>
            </div>
            <div class="large-4 columns"></div>
        </div>
    </form>
    <button class="close-button" onclick="location.reload();" aria-label="Close reveal" type="button">
        <span aria-hidden="true">×</span>
    </button>
</div>


<div id="clone-script-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Clone Script</h4>
    <hr>
    <form class="clone-script-form" action="#" method="post">
        <input name="emma-script-id" id="clone-script-id" type="hidden" value="" />
        <label class="text-left">Name
            <input name="name" id="clone-script-name" type="text" value="" />
        </label>
        <label class="text-left">Script Type
            <select id="clone-script-type" name="type">
                <option value="">-Select-</option>
                <?php
                $types = select_massnotificationScripts_mcSriptTypes();
                //                $fvmdb->query("
                //                        SELECT t.*
                //                        FROM emma_mass_communication_script_types as t
                //                        ORDER BY t.name
                //                    ");
                while($type = $types->fetch_assoc()){
                    echo '<option value="'. $type['emma_script_type_id'] .'">'. $type['name'] .'</option>';
                }
                ?>
            </select>
        </label>
        <label class="text-left">Broadcast Type
            <select id="clone-broadcast-type" name="broadcast">
                <option value="">-Select-</option>
                <?php
                $types = select_massnotificationScripts_types();
                //                $fvmdb->query("
                //                        SELECT t.emma_broadcast_id as emma_script_group_id, t.name
                //                        FROM emma_mass_communication_broadcast_types as t
                //                        ORDER BY t.name
                //                    ");
                while($type = $types->fetch_assoc()){
                    echo '<option value="'. $type['emma_script_group_id'] .'">'. $type['name'] .'</option>';
                }
                ?>
            </select>
        </label>
        <label class="text-left">Group
            <select id="clone-script-group" name="group">
                <option value="">-Select-</option>
                <?php
                $groups = select_groups_with_planID($USER->emma_plan_id);
                //                $fvmdb->query("
                //                        SELECT g.*
                //                        FROM emma_groups g
                //                        WHERE g.emma_plan_id = '". $USER->emma_plan_id ."'
                //                        Order by g.name
                //                    ");
                while($group = $groups->fetch_assoc()){
                    echo '<option value="'. $group['emma_group_id'] .'">'. $group['name'] .'</option>';
                }
                ?>
            </select>
        </label>
        <label class="text-left">Text
            <textarea name="text" id="clone-script-text" style="min-width:100%;height:5rem;"></textarea>
        </label>
        <div class="row">
            <div class="large-4 columns"></div>
            <div class="large-4 columns">
                <div class="button-group text-center">
                    <a class="button alert" data-close>Cancel</a>
                    <input type="submit" class="button" value="Submit" />
                </div>
            </div>
            <div class="large-4 columns"></div>
        </div>
    </form>
    <button class="close-button" onclick="location.reload();" aria-label="Close reveal" type="button">
        <span aria-hidden="true">×</span>
    </button>
</div>
