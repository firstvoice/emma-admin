<?php
?>









<div class="title row expanded align-middle">
  <div class="columns medium-5">
    <h2 class="text-left"><a href="./dashboard.php?content=resources"><i class="page-icon fa fa-map"></i> Resource
        Folder - Scripts</a></h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
        data-options="disableHover:true;clickOpen:true;">
      <li>
        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <ul class="menu">
          <li><a href="#" data-open="create-script-modal">New Script</a></li>
<!--          <li><a href="#" data-open="import-script-modal">Import Script</a></li>-->
        </ul>
      </li>
    </ul>
  </div>
</div>

<div>
  <ul class="tabs" data-tabs id="script-type-tabs">
    <?php
    $scriptTypes = select_scripts_scriptTypes();
//        $fvmdb->query("
//      select @pos:=@pos+1 as pos, st.*
//      from emma_script_types st
//      join (select @pos:=0) p
//      ORDER BY st.name
//    ");
    while ($st = $scriptTypes->fetch_assoc()) {
      echo '
      <li class="tabs-title ' . ($st['pos'] == '1' ? 'is-active' : '') . '" >
        <a href="#panel' . $st['emma_script_type_id'] . '" ' . ($st['pos'] == '1' ? 'aria-selected="true"' : '') . ' >' . $st['name'] . '</a>
      </li>
      ';
    }


    ?>
  </ul>
  <div class="tabs-content" data-tabs-content="script-type-tabs">
    <?php
    mysqli_data_seek($scriptTypes, 0);
    while ($st = $scriptTypes->fetch_assoc()) {
      echo '
      <div class="tabs-panel ' . ($st['pos'] == '1' ? 'is-active' : '') . '" id="panel' . $st['emma_script_type_id'] . '">
        <div class="row collapse expanded">
          <div class="medium-2 columns">
            <ul class="vertical tabs" data-tabs id="script' . $st['emma_script_type_id'] . '-tabs">
      ';
      $groups = select_scripts_groups($USER->emma_plan_id);
//          $fvmdb->query("
//        select @pos:=@pos+1 as pos, g.*
//        from emma_groups g
//        join (select @pos:=0) p
//        where g.emma_plan_id = '" . $USER->emma_plan_id . "'
//        order by g.name
//      ");
      while ($group = $groups->fetch_assoc()) {
        echo '
              <li class="tabs-title ' . ($group['pos'] == '1' ? 'is-active' : '') . '">
                <a id="script-name-' . $group['emma_group_id'] . '" href="#panelv' . $group['emma_group_id'] . 'column' . $st['emma_script_type_id'] . '" ' .
          ($group['pos'] == '1' ? 'aria-selected="true"' : '') . '>' .
          $group['name'] . '</a>
              </li>
        ';
      }
      echo '
            </ul>
          </div>
          <div class="medium-10 columns">
            <div class="tabs-content" data-tabs-content="script' . $st['emma_script_type_id'] . '-tabs">
      ';
      mysqli_data_seek($groups, 0);
      while ($group = $groups->fetch_assoc()) {
        echo '
              <div class="tabs-panel ' . ($group['pos'] == '1' ? 'is-active' : '') . '" id="panelv' . $group['emma_group_id'] . 'column' . $st['emma_script_type_id'] . '">
                <table>
                    <thead>
                    <tr>
                        <th>Script</th>                 
                        <th>Broadcast Type</th>                                
                        <th></th>                                
                    </tr>
                    </thead>
                    <tbody>';
                    $scripts = select_scripts_scripts($group['emma_group_id'], $USER->emma_plan_id, $st['emma_script_type_id']);
//                        $fvmdb->query("
//                        SELECT e.*, b.name as typename
//                        FROM emma_scripts e
//                        JOIN emma_broadcast_types b ON e.emma_broadcast_type_id = b.emma_script_group_id
//                        WHERE e.emma_script_group_id = '" . $group['emma_group_id'] . "'
//                        AND e.emma_plan_id = '". $USER->emma_plan_id ."'
//                        AND e.emma_script_type_id = '". $st['emma_script_type_id'] ."'
//                    ");
                    while($script = $scripts->fetch_assoc()){
                        echo '<tr>';
                        echo '<td>' . $script['name'] . '</td >';
                        echo '<td>' . $script['typename'] . '</td >';
                        echo '<td> <a class="clone-script button" data-emma-script-id="'. $script['emma_script_id'] .'" data-emma-script-name="'. $script['name'] .'" data-emma-script-text="'. $script['text'] .'" data-emma-script-type-id="'. $script['emma_script_type_id'] .'" data-broadcast-type-id="'.$script['emma_broadcast_type_id'].'" data-group-type-id="'. $script['emma_script_group_id'] .'" style="float: right; margin-left: 2px; margin-top: 10px">Clone</a> <a class="delete-script button alert" data-emma-script-id="'. $script['emma_script_id'] .'" style="float: right; margin-left: 2px; margin-top: 10px">Delete</a> <a class="edit-script button" data-emma-script-id="'. $script['emma_script_id'] .'" data-emma-script-name="'. $script['name'] .'" data-emma-script-text="'. $script['text'] .'" data-emma-script-type-id="'. $script['emma_script_type_id'] .'" data-broadcast-type-id="'.$script['emma_broadcast_type_id'].'" data-group-type-id="'. $script['emma_script_group_id'] .'" style="float: right; margin-left: 2px; margin-top: 10px">Edit</a> </td>';
                        echo '</tr>';
                    }
                echo'
                    </tbody>
                </table>
              </div>
        ';
      }
      echo '
            </div>
          </div>
        </div>
      </div>
      ';
    }
    ?>
  </div>
</div>

<div id="create-script-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
  <h4>Create Script</h4>
  <hr>
  <form id="create-script-form" class="text-left" action="#" method="post">
    <input name="plan-id" type="hidden" value="<?php echo $USER->emma_plan_id; ?>">
    <label>Script Type <span style="color: red">*</span>
      <select required id="script-type-prefab" name="script-type">
          <option value="">-Select-</option>
        <?php
        mysqli_data_seek($scriptTypes, 0);
        while ($scriptType = $scriptTypes->fetch_assoc()) {
          echo '<option value="' . $scriptType['emma_script_type_id'] . '">' . $scriptType['name'] . '</option>';
        }
        ?>
      </select>
    </label>
      <label class="text-left">Broadcast Type <span style="color: red">*</span>
          <select required id="broadcast-type-prefab" name="broadcast">
              <option value="">-Select-</option>
              <?php
              $types = select_scripts_types();
//                  $fvmdb->query("
//                        SELECT t.*
//                        FROM emma_broadcast_types t
//                        ORDER BY t.name
//                    ");
              while($type = $types->fetch_assoc()){
                  echo '<option value="'. $type['emma_script_group_id'] .'">'. $type['name'] .'</option>';
              }
              ?>
          </select>
      </label>
      <label>Group <span style="color: red">*</span>
          <select required id="group-type-prefab" name="group">
              <option value="">-Select-</option>
              <?php
              $scriptGroups = select_scripts_scriptGroups($USER->emma_plan_id);
//                  $fvmdb->query("
//                  select *
//                  from emma_groups
//                  where emma_plan_id = '". $USER->emma_plan_id ."'
//                  order by name
//              ");
              while ($scriptGroup = $scriptGroups->fetch_assoc()) {
                echo '<option value="' . $scriptGroup['emma_group_id'] . '">' . $scriptGroup['name'] . '</option>';
              }
              ?>
          </select>
      </label>
    <label>Name <span style="color: red">*</span>
      <input name="name" type="text"/>
    </label>
    <label>Text <span style="color: red">*</span>
      <textarea name="text" style="min-width:100%;height:5rem;"></textarea>
    </label>
      <div class="row">
          <div class="large-4 columns"></div>
          <div class="large-4 columns">
              <div class="button-group text-center">
                  <a class="button alert" data-close>Cancel</a>
                  <input type="submit" class="button" value="Submit" />
              </div>
          </div>
          <div class="large-4 columns"></div>
      </div>
  </form>
    <button class="close-button" onclick="location.reload();" aria-label="Close reveal" type="button">
        <span aria-hidden="true">×</span>
    </button>
</div>


<div style="width: 50%;" id="import-script-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
  <h4>Import Script</h4>
  <hr>
    <div class="large-10 columns row">
        <form style="display: none;" id="confirm-import-script-form" class="expanded row">

        <table style="display: none;" id="append-table-data-here">
            <tbody>
            <tr>
                <th>Script Type</th>
                <th>Broadcast Type</th>
                <th>Group</th>
                <th>Name</th>
                <th>Text</th>
            </tr>
            </tbody>
        </table>
         </form>

    </div>
  <form id="upload-script-form" method="POST">
      <div class="row">
          <div class="large-4 large-offset-4 columns">
            <a class="button expanded success" href="documents/import-scripts-template.csv" download>Download Template</a>
          </div>
      </div>
      <div class="row">
          <div class="large-4 large-offset-4 columns">
              <label for="import-scripts-file" class="button expanded">Choose Upload File</label>
              <input type="file" id="import-scripts-file" name="import-scripts-file" class="show-for-sr"/>
          </div>
      </div>

          <div class="large-6 large-offset-3 columns">
              <div class="expanded button-group">
                  <a data-close class="button alert hollow">Cancel</a>
                  <input type="submit" class="button" value="Submit" />
              </div>
          </div>
          <div class="large-4 columns"></div>

  </form>
    <button class="close-button" onclick="location.reload();" aria-label="Close reveal" type="button">
        <span aria-hidden="true">×</span>
    </button>
</div>

<div id="success_modal" class="reveal success callout text-center tiny" data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Success</h4>
  <a href="./dashboard.php?content=scripts" data-close class="button success">Ok</a>
<!--  <button class="close-button" data-close aria-label="Close reveal" type="button">-->
<!--    <span aria-hidden="true">&times;</span>-->
<!--  </button>-->
</div>

<div id="delete-confirmation" class="reveal warning callout text-center tiny" data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Confirm Delete</h4>
  <form id="delete-script-form" method="post" action="#">
    <input id="delete-script-id" name="delete-script-id" type="hidden">
    <p>Are you sure you want to delete this script?</p>
    <div class="button-group expanded">
      <a data-close="" class="button">Cancel</a>
      <input type="submit" class="button alert" value="OK"/>
    </div>
  </form>
  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<div id="edit-script-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Edit Script</h4>
    <hr>
    <form class="edit-script-form" action="#" method="post">
        <input name="emma-script-id" id="edit-script-id" type="hidden" value="" />
        <label class="text-left">Name
            <input name="name" id="edit-script-name" type="text" value="" />
        </label>
        <label class="text-left">Script Type
            <select id="edit-script-type" name="type">
                <option value="">-Select-</option>
                <?php
                $types = select_scripts_emmaScriptTypes();
//                    $fvmdb->query("
//                        SELECT t.*
//                        FROM emma_script_types t
//                        ORDER BY t.name
//                    ");
                while($type = $types->fetch_assoc()){
                    echo '<option value="'. $type['emma_script_type_id'] .'">'. $type['name'] .'</option>';
                }
                ?>
            </select>
        </label>
        <label class="text-left">Broadcast Type
            <select id="edit-broadcast-type" name="broadcast">
                <option value="">-Select-</option>
                <?php
                $types = select_scripts_types();
//                $fvmdb->query("
//                        SELECT t.*
//                        FROM emma_broadcast_types t
//                        ORDER BY t.name
//                    ");
                while($type = $types->fetch_assoc()){
                    echo '<option value="'. $type['emma_script_group_id'] .'">'. $type['name'] .'</option>';
                }
                ?>
            </select>
        </label>
        <label class="text-left">Group
            <select id="edit-script-group" name="group">
                <option value="">-Select-</option>
                <?php
                    $groups = select_groups_with_planID($USER->emma_plan_id);
//                    $fvmdb->query("
//                        SELECT g.*
//                        FROM emma_groups g
//                        WHERE g.emma_plan_id = '". $USER->emma_plan_id ."'
//                        Order by g.name
//                    ");
                    while($group = $groups->fetch_assoc()){
                        echo '<option value="'. $group['emma_group_id'] .'">'. $group['name'] .'</option>';
                    }
                ?>
            </select>
        </label>
        <label class="text-left">Text
            <textarea name="text" id="edit-script-text" style="min-width:100%;height:5rem;"></textarea>
        </label>
        <div class="row">
            <div class="large-4 columns"></div>
            <div class="large-4 columns">
                <div class="button-group text-center">
                    <a class="button alert" data-close>Cancel</a>
                    <input type="submit" class="button" value="Submit" />
                </div>
            </div>
            <div class="large-4 columns"></div>
        </div>
    </form>
    <button class="close-button" onclick="location.reload();" aria-label="Close reveal" type="button">
        <span aria-hidden="true">×</span>
    </button>
</div>


<div id="clone-script-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Clone Script</h4>
    <hr>
    <form class="clone-script-form" action="#" method="post">
        <input name="emma-script-id" id="clone-script-id" type="hidden" value="" />
        <label class="text-left">Name
            <input name="name" id="clone-script-name" type="text" value="" />
        </label>
        <label class="text-left">Script Type
            <select id="clone-script-type" name="type">
                <option value="">-Select-</option>
                <?php
                $types = select_scripts_emmaScriptTypes();
//                $fvmdb->query("
//                        SELECT t.*
//                        FROM emma_script_types t
//                        ORDER BY t.name
//                    ");
                while($type = $types->fetch_assoc()){
                    echo '<option value="'. $type['emma_script_type_id'] .'">'. $type['name'] .'</option>';
                }
                ?>
            </select>
        </label>
        <label class="text-left">Broadcast Type
            <select id="clone-broadcast-type" name="broadcast">
                <option value="">-Select-</option>
                <?php
                $types = select_scripts_types();
//                $fvmdb->query("
//                        SELECT t.*
//                        FROM emma_broadcast_types t
//                        ORDER BY t.name
//                    ");
                while($type = $types->fetch_assoc()){
                    echo '<option value="'. $type['emma_script_group_id'] .'">'. $type['name'] .'</option>';
                }
                ?>
            </select>
        </label>
        <label class="text-left">Group
            <select id="clone-script-group" name="group">
                <option value="">-Select-</option>
                <?php
                $groups = select_groups_with_planID($USER->emma_plan_id);
//                $fvmdb->query("
//                        SELECT g.*
//                        FROM emma_groups g
//                        WHERE g.emma_plan_id = '". $USER->emma_plan_id ."'
//                        Order by g.name
//                    ");
                while($group = $groups->fetch_assoc()){
                    echo '<option value="'. $group['emma_group_id'] .'">'. $group['name'] .'</option>';
                }
                ?>
            </select>
        </label>
        <label class="text-left">Text
            <textarea name="text" id="clone-script-text" style="min-width:100%;height:5rem;"></textarea>
        </label>
        <div class="row">
            <div class="large-4 columns"></div>
            <div class="large-4 columns">
                <div class="button-group text-center">
                    <a class="button alert" data-close>Cancel</a>
                    <input type="submit" class="button" value="Submit" />
                </div>
            </div>
            <div class="large-4 columns"></div>
        </div>
    </form>
    <button class="close-button" onclick="location.reload();" aria-label="Close reveal" type="button">
        <span aria-hidden="true">×</span>
    </button>
</div>
