<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 12/6/2017
 * Time: 3:53 PM
 */

$plans = select_plan_from_planID($USER->emma_plan_id);
//$fvmdb->query("
//  select *
//  from emma_plans
//  where emma_plan_id = '" . $USER->emma_plan_id . "'
//");
$plan = $plans->fetch_assoc();

$sites = select_sites_with_planID($USER->emma_plan_id);
//$fvmdb->query("
//  select *
//  from emma_sites
//  where emma_plan_id = '" . $USER->emma_plan_id . "'
//");
$siteCount = $sites->num_rows;


if (!($USER->privilege->admin)) {
    redirect();
}

?>


<div class="title row expanded align-middle">
  <div class="columns medium-4">
    <h2 class="text-left"><a href="./dashboard.php?content=sites"><i class="page-icon fa fa-institution"></i> <?php echo ucwords($content); ?></a></h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
      data-options="disableHover:true;clickOpen:true;">
      <li>
        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <ul class="menu">
          <?php
          if ($siteCount < $plan['max_sites']) {
            echo '<li><a href="./dashboard.php?content=create_site">Create Site</a></li>';
          } else {
            echo '<li><a href="#" class="disabled" disabled>Create Site (MAX)</a></li>';
          }
          ?>

        </ul>
      </li>
    </ul>
  </div>
</div>

<table id="sites-table" class="data-table" style="width:100%">
  <colgroup>

  </colgroup>
  <thead>
  <tr>
      <th class="text-search">Site</th>
      <th class="text-search">Event Status</th>
      <th class="text-search"># Events</th>
      <th class="text-search"># Drills</th>
  </tr>
  <tr>
    <th>Site</th>
    <th>Event Status</th>
    <th># Events</th>
    <th># Drills</th>
  </tr>
  </thead>
  <tbody>
  </tbody>
  <tfoot>

  </tfoot>
</table>