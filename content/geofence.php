<?php
/**
 * Created by PhpStorm.
 * User: John
 * Date: 12/6/2017
 * Time: 20:48
 */


$geofence_id = $fvmdb->real_escape_string($_GET['id']);

if (!empty($geofence_id)) {
    $geofences = select_geofence_geofences($geofence_id);
//        $fvmdb->query("
//    SELECT g.*, g2.fence_name as main
//    FROM emma_geofence_locations g
//    LEFT JOIN emma_geofence_locations g2 ON g.main_fence = g2.id
//    WHERE g.id = " . $geofence_id . "
//");
    $fence = $geofences->fetch_assoc();
    
} else {
    echo '<div class="title">
            <h2 class="text-left"><a href="./dashboard.php?content=geofences">' . ucwords($content) . '</a></h2>
        </div>';
    echo '<div>
            <p class="text-center">Whoops no geofence found.</p>
        </div>';
}

$broadcastScriptsString = '';
$broadcastScripts = select_geofence_broadcastScripts($USER->emma_plan_id);
//    $fvmdb->query("
//    select *
//    from emma_scripts
//    where emma_plan_id = '" . $USER->emma_plan_id . "'
//  ");
while ($broadcastScript = $broadcastScripts->fetch_assoc()) {
    $broadcastScriptsString .= "<option value='" .
        $broadcastScript['emma_script_id'] .
        "' data-text='" . $broadcastScript['text'] . "'>" .
        $broadcastScript['name'] . "</option>";
}

$scriptGroupsString = '';
$scriptGroups = select_geofence_scriptGroups();
//    $fvmdb->query("
//    select *
//    from emma_broadcast_types
//    order by name
//  ");
while ($scriptGroup = $scriptGroups->fetch_assoc()) {
    $scriptGroupsString .= "<option value='" . $scriptGroup['emma_script_group_id'] . "'>" . $scriptGroup['name'] . "</option>";
}

if (!empty($fence)) { ?>
    <div class="title row expanded align-middle">
        <div class="columns medium-2">
            <h2 class="text-left"><a href="./dashboard.php?content=geofences"><i class="page-icon fa fa-globe"></i> <?php echo ucwords($content); ?></a></h2>
        </div>
        <div class="columns show-for-medium"></div>
        <div class="columns shrink">
            <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
                data-options="disableHover:true;clickOpen:true;">
                <li>
                    <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
                    <ul class="menu">
<!--                        <li><a href="./dashboard.php?content=edit_geofence_new&id=--><?php //echo $fence['id']; ?><!--" id="edit_create">Edit-->
<!--                                Geofence</a></li>-->
                        <?php
                        if($fence['active'] == 1) {
                            echo '<li><a data-open="deactivate-geofence-modal">Deactivate Geofence</a></li>';
                        }else{
                            echo '<li><a data-open="reactivate-geofence-modal">Reactivate Geofence</a></li>';
                        }
                        ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="row expanded">

        <div class="large-6 medium-12 column" style="height: 100%">
            <div class="card-info warning">
                <div class="card-info-content">
                    <form id="new-message-form">
                        <input type="hidden" name="id" value="<?php echo $geofence_id;?>">
                        <input type="hidden" name="plan" value="<?php echo $USER->emma_plan_id;?>">
                        <input type="hidden" name="user" value="<?php echo $USER->id;?>">
                        <h3 class="lead">New Message</h3>
                        <div class="row expanded">
                            <div class="large-12 medium-12 small-12 columns">
                                <label>Emergency Details
                                    <textarea name="description" style="min-width:100%;height:5rem;"></textarea>
                                </label>
                            </div>
                        </div>
                        <div class="row expanded">
                            <div class="large-12 medium-12 small-12 columns">
                                <label>Comments</label>
                                <select class="select-create-event-script" id="select-geocode-script" style="margin-bottom:0;border-bottom:none;">
                                    <option value="" data-text="">-- Select Script --</option>
                                    <?php
                                    $geofenceScripts = select_geofence_geofenceScripts($USER->emma_plan_id);
                                    //                                                $fvmdb->query("
                                    //                                                SELECT g.text, g.name
                                    //                                                FROM emma_geofence_scripts g
                                    //                                                WHERE g.emma_plan_id = '". $USER->emma_plan_id ."'
                                    //                                                AND g.active = 1
                                    //                                                ORDER BY g.name
                                    //                                            ");
                                    while($geofenceScript = $geofenceScripts->fetch_assoc()){
                                        echo'<option value="'. $geofenceScript['text'] .'">'. $geofenceScript['name'] .'</option>';
                                    }
                                    ?>
                                </select>
                                <br>
                                <textarea name="comments" id="create-geocode-description" class="create-event-description" style="min-width:100%;height:5rem;" maxlength="100"></textarea>
                            </div>
                        </div>
                        <div class="row expanded">
                            <div class="large-12 medium-12 small-12 columns">
                                <button type="submit" class="button expanded">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="large-6 medium-12 column" style="height: 100%">
            <div class="card-info primary">
                <div class="card-info-content">
                    <h3 class="lead">Details</h3>
                    <div class="row">
                        <div class="columns large-12 medium-12">
                            <p>Geofence Name: <?php if (!empty($fence['fence_name'])) {
                                    echo $fence['fence_name'];
                                } else {
                                    echo 'None';
                                } ?></p>
                        </div>
                    </div>
                    <?php
                    if($fence['type'] == 'Rectangle')
                        { echo '
                    <div class="row">
                        <div class="columns large-12 medium-12">
                            ';if (!empty($fence['nw_corner_lat']) && !empty($fence['nw_corner_lng'])) {
                                echo '<p>NW Corner Coordinates: '.$fence['nw_corner_lat'] . ', ' . $fence['nw_corner_lng'].'</p>';
                            }
                           echo '
                        </div>
                    </div>
                    <div class="row">
                        <div class="columns large-12 medium-12">
                            '; if (!empty($fence['se_corner_lat']) && !empty($fence['se_corner_lng'])) {
                                echo '<p>SE Corner Coordinates: '.$fence['se_corner_lat'] . ', ' . $fence['se_corner_lng'].'</p>';
                            }
                            echo '
                        </div>
                    </div>';
                    }?>
                    <!--                    <div class="row">-->
                    <!--                        <div class="columns large-12 medium-12">-->
                    <!--                            <p>NE Corner Coordinates: --><?php //if (!empty($fence['ne_corner_lat']) && !empty($fence['ne_corner_lng'])) {
                    //                                    echo $fence['ne_corner_lat'] . ', ' . $fence['ne_corner_lng'];
                    //                                } else {
                    //                                    echo 'N/A';
                    //                                } ?><!--</p>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="row">-->
                    <!--                        <div class="columns large-12 medium-12">-->
                    <!--                            <p>SW Corner Coordinates: --><?php //if (!empty($fence['sw_corner_lat']) && !empty($fence['sw_corner_lng'])) {
                    //                                    echo $fence['sw_corner_lat'] . ', ' . $fence['sw_corner_lng'];
                    //                                } else {
                    //                                    echo 'N/A';
                    //                                } ?><!--</p>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <?php
                    if($fence['type'] == 'Circle')
                        { echo '
                    <div class="row">
                        <div class="columns large-12 medium-12">
                            <p>Center Coordinates:'; if (!empty($fence['center_lat']) && !empty($fence['center_lng'])) {
                                    echo $fence['center_lat'] . ', ' . $fence['center_lng'];
                                } else {
                                    echo 'N/A';
                                } echo '</p>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="columns large-12 medium-12" >
                            <p> Radius:';if (!empty($fence['radius'])) {
                             echo $fence['radius'] . ' Miles';
                         } else {
                             echo 'N/A';
                         } echo '</p>
                        </div >
                    </div >';
                    }
                    ?>
                    <div class="row">
                        <div class="columns large-12 medium-12">
                            <p>Type: <?php if (!empty($fence['type'])) {
                                    echo $fence['type'];
                                } else {
                                    echo 'N/A';
                                } ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="columns large-12 medium-12">
                            <p>Main Fence: <?php if (!empty($fence['main'])) {
                                    echo $fence['main'];
                                } else {
                                    echo 'N/A';
                                } ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="columns large-12 medium-12">
                            <p>Status: <?php if($fence['active'] == 1){
                                    echo'<span style="color: green">Active</span>';
                                } else {
                                    echo'<span style="color: red">Inactive</span>';
                                }?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="row expanded">

        <div class="large-6 medium-12 column">
            <div class="card-info alert">
                <div class="card-info-content">
                    <h3 class="lead">Message Center</h3>
                    <div id="message-center" class="max-height">
                    <?php
                    $messages = select_geofence_messages($geofence_id);
//                        $fvmdb->query("
//                    SELECT gm.*, concat(u.firstname, ' ', u.lastname) AS user, u.id aS userId
//                    FROM emma_geofence_messages gm
//                    JOIN users u ON gm.created_by_id = u.id
//                    WHERE gm.geofence_id = '". $geofence_id ."'
//                    ");
                    while($message = $messages->fetch_assoc()) {
                        echo '
                        <div data-closable class="callout alert-callout-subtle radius inset-shadow" style="overflow:hidden;word-wrap:break-word;">
                            <strong>
                                <a href="dashboard.php?content=user&id=' . $message['userId'] . '">' . $message['user'] . '</a> -
                                ' . date('m/d/Y H:i:s',strtotime($message['created_date'])) . ' - messages sent: '.$message['sent_to_qnt'].'
                            </strong>
                            <div><b>Emergency Details: </b>' . $message['description'] . '</div>
                            <div><b>Comments: </b>' . $message['comments'] . '</div>
                        </div>';
                    }
                    ?></div>
                </div>
            </div>
        </div>
        <?php
        if(empty($fence['center_lng']) || empty($fence['center_lat'])){
            $fence['center_lat'] = ($fence['nw_corner_lat'] + $fence['se_corner_lat'])/2;
            $fence['center_lng'] = ($fence['nw_corner_lng'] + $fence['se_corner_lng'])/2;


            $fence['ne_corner_lat'] = $fence['nw_corner_lat'];
            $fence['ne_corner_lng'] = $fence['se_corner_lng'];
            $fence['sw_corner_lat'] = $fence['se_corner_lat'];
            $fence['sw_corner_lng'] = $fence['nw_corner_lng'];
        }
        $children = select_getgeofence_children($fence['id']);
        while($child = $children->fetch_assoc()){
            $childarr[] = [
                'lat' => $child['center_lat'],
                'lng' => $child['center_lng'],
                'rad' => $child['radius'],
            ];
        }
        ?>
        <script type="text/javascript">
            let mainEvent = <?php echo json_encode($fence); ?>;
            let fenceChildren = <?php echo json_encode($childarr); ?>;
        </script>
        <div class="large-6 medium-12 column">
            <div class="card-info success">
                <div class="card-info-content" style="height: 21rem">
                    <div id="event-map" style="width:100%; height:100%;"></div>
                    <!----Need Map---->
                </div>
            </div>
        </div>
    </div>
    <div id="deactivate-geofence-modal" class="reveal callout alert text-center tiny" data-reveal
         data-animation-in="fade-in"
         data-animation-out="fade-out">
        <form id="deactivate-geofence-form" action="#" method="post" onsubmit="return false;">
            <input type="hidden" name="geofence-id" value="<?php echo $geofence_id; ?>" />
            <h4 style="color:darkred">Deactivate Geofence?</h4>
            <div class="expanded button-group">
                <a class="button" data-close>Cancel</a>
                <input class="button" type="submit" value="Deactivate" />
            </div>
        </form>
    </div>
    <div id="reactivate-geofence-modal" class="reveal callout text-center tiny" data-reveal
         data-animation-in="fade-in"
         data-animation-out="fade-out">
        <form id="reactivate-geofence-form" action="#" method="post" onsubmit="return false;">
            <input type="hidden" name="geofence-id" value="<?php echo $geofence_id; ?>" />
            <h4>Reactivate Geofence?</h4>
            <div class="expanded button-group">
                <a class="button" data-close>Cancel</a>
                <input class="button" type="submit" value="Reactivate" />
            </div>
        </form>
    </div>
    <div id="success_modal" class="reveal success callout tiny text-center"
         data-reveal data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4>Success</h4>
        <a href="./dashboard.php?content=geofence&id=<?php echo $geofence_id;?>" data-close
           class="button success" style="margin-left:auto;margin-right:auto">Ok</a>
<!--        <button class="close-button" data-close aria-label="Close reveal"-->
<!--                type="button">-->
<!--            <span aria-hidden="true">&times;</span>-->
<!--        </button>-->
    </div>


<?php } ?>


