<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 12/6/2017
 * Time: 3:53 PM
 */

$plans = select_plan_from_planID($USER->emma_plan_id);
//$fvmdb->query("
//  select *
//  from emma_plans
//  where emma_plan_id = '" . $USER->emma_plan_id . "'
//");
$plan = $plans->fetch_assoc();

$sites = select_sites_with_planID($USER->emma_plan_id);
//$fvmdb->query("
//  select *
//  from emma_sites
//  where emma_plan_id = '" . $USER->emma_plan_id . "'
//");
$siteCount = $sites->num_rows;


if (!($USER->privilege->admin)) {
    redirect();
}

?>


<div class="title row expanded align-middle">
    <div class="columns medium-4">
        <h2 class="text-left"><a href="./dashboard.php?content=qr_codes"><i class="page-icon fa fa-qrcode"></i> QR Codes</a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
        <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
            data-options="disableHover:true;clickOpen:true;">
            <li>
                <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
                <ul class="menu">
                    <li><a data-open="create-qr-modal">Create QR Code</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<div class="content-body">
    <div class="row expanded" id="qr-container" style="text-align: center">

    </div>
</div>
<div id="create-qr-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Create QR Code</h4>
    <hr>
    <form id="create-qr-form" class="text-left" action="#" method="post">
        <input name="user-id" type="hidden" value="<?php echo $USER->id; ?>"/>
        <label>Plan <span style="color: red">*</span>
            <select required id="qr-plan" name="qr-plan">
                <option value="">-Select-</option>
                <?php
                $user_plans = select_multiPlans_with_userID($USER->id);
                while ($user_plan = $user_plans->fetch_assoc()) {
                    echo '<option value="' . $user_plan['plan_id'] . '">' . $user_plan['name'] . '</option>';
                }
                ?>
            </select>
        </label>
        <label>Group <span style="color: red">*</span>
            <select required id="qr-group" name="qr-group">
                <option value="">-Select-</option>
            </select>
        </label>
        <div class="row">
            <div class="large-4 columns"></div>
            <div class="large-4 columns">
                <div class="button-group text-center">
                    <a class="button alert" data-close>Cancel</a>
                    <input type="submit" class="button" value="Submit" />
                </div>
            </div>
            <div class="large-4 columns"></div>
        </div>
    </form>
    <button class="close-button" onclick="location.reload();" aria-label="Close reveal" type="button">
        <span aria-hidden="true">×</span>
    </button>
</div>
<div id="download-qr-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Download QR Code</h4>
    <hr>
    <form action="process/print_qrcode.php" method="post" target="_blank">
        <input type="hidden" class="download-plan" name="plan" value="">
        <input type="hidden" class="download-group" name="group" value="">
        <input type="hidden" class="download-type" name="type" value="png">
        <input type="submit" class="button radius" value="Download PNG">
    </form>
    <form action="process/print_qrcode.php" method="post" target="_blank">
        <input type="hidden" class="download-plan" name="plan" value="">
        <input type="hidden" class="download-group" name="group" value="">
        <input type="hidden" class="download-type" name="type" value="pdf">
        <input type="submit" class="button radius" value="Download PDF">
    </form>
    <button class="close-button" onclick="location.reload();" aria-label="Close reveal" type="button">
        <span aria-hidden="true">×</span>
    </button>
</div>

