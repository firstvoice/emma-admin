<?php

if (!($USER->privilege->security || $USER->privilege->admin)) {
    redirect();
}


?>

<div class="title row expanded align-middle">
    <div class="columns medium-6">
        <h2 class="text-left"><a href="./dashboard.php?content=modified_lockdowns"><i class="page-icon fa fa-lock"></i> Modified LOCKDOWN!</a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
    </div>
</div>
<table id="modified-lockdowns-table" class="data-table">
    <thead>
    <tr>
        <th class="text-search">Date</th>
        <th class="text-search">Username/Email</th>
    </tr>
    <tr>
        <th>Date</th>
        <th>Username/Email</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
    </tfoot>
</table>
