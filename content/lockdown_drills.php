<?php

if (!($USER->privilege->security || $USER->privilege->admin)) {
    redirect();
}
$message = '';
$lockdownmessages = select_lockdown_drill_notification($USER->emma_plan_id);
while($lockdownmessage = $lockdownmessages->fetch_assoc()){
    $drillmessage = $lockdownmessage['notification'];
}

?>

<div class="title row expanded align-middle">
    <div class="columns medium-6">
        <h2 class="text-left"><a href="./dashboard.php?content=lockdown_drills"><i class="page-icon fa fa-lock"></i> LOCKDOWN! (DRILL)</a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
        <?php
        if($USER->privilege->admin) {
            echo '
            <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
              data-options="disableHover:true;clickOpen:true;">
              <li>
                <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
                <ul class="menu">
                  <li><a href="#" data-open="edit-message-modal">Edit Message</a></li>
                </ul>
              </li>
            </ul>
            ';
        }
        ?>
    </div>
</div>
<div class="row expanded">
    <div class="large-12 columns">
        <label><b>Drill Message:</b> <?php echo $drillmessage;?></label>
    </div>
</div>
<table id="lockdowns-table" class="data-table">
    <thead>
    <tr>
        <th class="text-search">Date</th>
        <th class="text-search">Username/Email</th>
    </tr>
    <tr>
        <th>Date</th>
        <th>Username/Email</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
    </tfoot>
</table>
<div id="edit-message-modal" class="reveal callout text-center small"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Edit Message</h4>
    <form type="post" id="edit-massage-form">
        <div class="row" style="margin-top: 5%">
            <div class="large-12 columns">
                <input type="hidden" name="plan-id" value="<?php echo $USER->emma_plan_id;?>">
                <input type="hidden" name="drill" value="1">
                <textarea id="lockdown-message" name="message"><?php echo $drillmessage;?></textarea>
            </div>
        </div>
        <div class="row">
            <div class="large-12 columns button-group expanded">
                <a data-close class="button alert">Cancel</a>
                <input type="submit" class="button" value="Save">
            </div>
        </div>
    </form>
</div>