<?php

if (!($USER->privilege->security || $USER->privilege->admin)) {
    redirect();
}

?>

<div class="title row expanded align-middle">
  <div class="columns medium-2">
    <h2 class="text-left"><a href="./dashboard.php?content=drills"><i class="page-icon fa fa-clock-o"></i> <?php echo ucwords($content); ?></a></h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
<!--    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu-->
<!--      data-options="disableHover:true;clickOpen:true;">-->
<!--      <li>-->
<!--        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>-->
<!--        <ul class="menu">-->
<!--          <li><a href="#" data-open="create-drill-modal">Create Drill</a></li>-->
<!--        </ul>-->
<!--      </li>-->
<!--    </ul>-->
  </div>
</div>

<table id="drills-table" class="data-table">
  <thead>
  <tr>
      <th class="text-search">Drill</th>
      <th class="text-search">Username/Email</th>
      <th class="text-search">Status</th>
      <th class="text-search">Created</th>
  </tr>
  <tr>
    <th>Drill</th>
    <th>Username/Email</th>
    <th>Status</th>
    <th>Created</th>
  </tr>
  </thead>
  <tbody>
  </tbody>
  <tfoot>

  </tfoot>
</table>
