<div class="title row expanded align-middle">
    <div class="columns medium-4">
        <h2 class="text-left"><a href="./dashboard.php?content=geofences"><i class="page-icon fa fa-globe"></i> Create Geofence</a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
    </div>
</div>
<div class="row expanded">
    <div class="large-12 medium-12 small-12 columns">
        <div class="card-info primary">
            <div class="card-info-content">
                <form id="create-geofence-form">
                    <input type="hidden" name="user"
                           value="<?php echo $USER->id; ?>"/>
                    <input type="hidden" name="plan-id"
                           value="<?php echo $USER->emma_plan_id; ?>"/>
                    <div class="row expanded">
                        <div class="large-6 medium-12 small-12 column">
                            <label>Name
                                <input type="Text" name="name" value=""/>
                            </label>
                        </div>
                        <div class="large-3 medium-6 small-6 column">
                            <label>Type
                                <select id="dropdown_type" name="type">
                                    <option selected disabled>-Select-</option>
                                    <option value="Circle">Circle</option>
                                    <option value="Rectangle">Rectangle</option>
                                </select>
                            </label>
                        </div>
                        <div class="large-3 medium-6 small-6 column">
                            <label>Main Fence
                                <select name="main">
                                    <option value="">-None-</option>
                                    <?php
                                    $fences = select_createGeofence_fences($USER->emma_plan_id);
//                                        $fvmdb->query("
//                                            SELECT g.id, g.fence_name
//                                            FROM emma_geofence_locations g
//                                            WHERE g.plan_id = '". $USER->emma_plan_id ."'
//                                            ORDER BY g.fence_name
//                                        ");
                                    while($fence = $fences->fetch_assoc()){
                                        echo'<option value="'. $fence['id'] .'">'. $fence['fence_name'] .'</option>';
                                    }
                                    ?>
                                </select>
                            </label>
                        </div>
                    </div>
                    <div id="circle_append_here" class="row expanded">
                    </div>

                    <div id="rectangle_first_append_here" class="row expanded">

                    </div>
                    <!--                    <div class="row expanded">-->
                    <!--                        <div class="large-12 medium-12 small-12 column">-->
                    <!--                            <label class="text-center"><b>NE Corner Coordinates</b></label>-->
                    <!--                            <div class="row expanded">-->
                    <!--                                <div class="large-6 medium-6 small-6 column">-->
                    <!--                                    <label>Latitude-->
                    <!--                                        <input type="text" name="ne-lat" value="">-->
                    <!--                                    </label>-->
                    <!--                                </div>-->
                    <!--                                <div class="large-6 medium-6 small-6 column">-->
                    <!--                                    <label>Longitude-->
                    <!--                                        <input type="text" name="ne-lng" value="">-->
                    <!--                                    </label>-->
                    <!--                                </div>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="row expanded">-->
                    <!--                        <div class="large-12 medium-12 small-12 column">-->
                    <!--                            <label class="text-center"><b>SW Corner Coordinates</b></label>-->
                    <!--                            <div class="row expanded">-->
                    <!--                                <div class="large-6 medium-6 small-6 column">-->
                    <!--                                    <label>Latitude-->
                    <!--                                        <input type="text" name="sw-lat" value="">-->
                    <!--                                    </label>-->
                    <!--                                </div>-->
                    <!--                                <div class="large-6 medium-6 small-6 column">-->
                    <!--                                    <label>Longitude-->
                    <!--                                        <input type="text" name="sw-lng" value="">-->
                    <!--                                    </label>-->
                    <!--                                </div>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <div id="rectangle_second_append_here" class="row expanded">

                    </div>
                    <div class="row large-12 columns">
                        <div class="large-2 columns large-offset-5">
                            <button type="submit" class="button expanded">Create</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!--/ Message Center -->
    </div>
</div>


<div id="success_modal" class="reveal callout tiny success text-center"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <div class="row columns">
        <a data-close class="button success"
           style="margin-left:auto;margin-right:auto;" href="./dashboard.php?content=geofences">Ok</a>
    </div>
</div>