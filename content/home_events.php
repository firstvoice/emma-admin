<?php




?>


<div class="large-12 medium-12 small-12">
    <div class="card-info warning">
        <div class="card-info-content">
            <h3 class="lead">Active Events/Drills</h3>
            <div id="active-events-drills" class="chart">
                <ul style="margin-bottom:0;">
                    <li>Events
                        <ul><?php
                            $activeLives = select_homeEvents_activeLives($USER->emma_plan_id);
//                                $fvmdb->query("
//                            SELECT e.emergency_id, et.name, e.drill
//                            FROM emergencies e
//                            JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
//                            WHERE e.emma_plan_id = '" . $USER->emma_plan_id . "'
//                            AND e.active = '1'
//                            AND e.drill = '0'
//                            AND e.parent_emergency_id IS NULL
//                            ");
                            while ($activeLive = $activeLives->fetch_assoc()) {
                            echo '<li><a href="./dashboard.php?content=event&id=' .
                                        $activeLive['emergency_id'] . '">' . $activeLive['name'] .
                                    '</a></li>';
                            }
                            ?>
                        </ul>
                    </li>
                    <li>Drills
                        <ul>
                            <?php
                            $activeDrills = select_homeEvents_activeDrills($USER->emma_plan_id);
//                                $fvmdb->query("
//                              SELECT e.emergency_id, et.name, e.drill
//                              FROM emergencies e
//                              JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
//                              WHERE e.emma_plan_id = '" . $USER->emma_plan_id . "'
//                              AND e.active = '1'
//                              AND e.drill = '1'
//                              AND e.parent_emergency_id IS NULL
//                            ");
                            while ($activeDrill = $activeDrills->fetch_assoc()) {
                                echo '<li><a href="./dashboard.php?content=event&id=' .
                                    $activeDrill['emergency_id'] . '">' . $activeDrill['name'] .
                                    '</a></li>';
                            }
                            ?>
                        </ul>
                    </li>
                    <?php
                    $plans = select_homeEvents_securitiesCheck($USER->emma_plan_id);
//                        $fvmdb->query("
//                          select *
//                          from emma_plans
//                          where emma_plan_id = '" . $USER->emma_plan_id . "'
//                          and securities = 1
//                        ");
                    if ($plans->num_rows > 0) { ?>
                        <li>Security
                            <ul>
                                <?php
                                $activeSecurities = select_homeEvents_activeSecurities($USER->emma_plan_id);
//                                    $fvmdb->query("
//                                SELECT *
//                                FROM emma_securities s
//                                JOIN emma_security_types st on s.emma_security_type_id = st.emma_security_type_id
//                                WHERE s.emma_plan_id = '" . $USER->emma_plan_id . "'
//                                AND s.active = '1'
//                              ");
                                while ($activeSecurity = $activeSecurities->fetch_assoc()) {
                                    echo '<li><a href="./dashboard.php?content=security&id=' .
                                        $activeSecurity['emma_security_id'] . '">' .
                                        $activeSecurity['name'] . '</a></li>';
                                }
                                ?>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div><!--/ Active Events/Drills -->






<?php

if($USER->emma_plan_id != 20 ) {
    $show = true;
}
else{
    $show = false;
}

?>
<input hidden id="plan_id" value="<?php echo $show ?>";



