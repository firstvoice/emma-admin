<?php
/**
 * Created by PhpStorm.
 * User: John
 * Date: 12/6/2017
 * Time: 20:48
 */

$user_id = $fvmdb->real_escape_string($_GET['id']);

if (!empty($user_id)) {
    $users = select_user_from_userID($user_id);
//    $fvmdb->query("
//    SELECT u.*
//    FROM users AS u
//    WHERE u.id = " . $user_id . "
//");
    $user = $users->fetch_assoc();

    $activation_status = 'Yes';

    if (strlen($user['password']) == 32 && strpos($user['password'], '*') === false) {
        $activation_status = 'No';
    }
} else {
    echo '<div class="title">
            <h2 class="text-left"><a href="./dashboard.php?content=users">' . ucwords($content) . '</a></h2>
        </div>';
    echo '<div>
            <p class="text-center">Whoops no user found.</p>
        </div>';
}

if (!empty($user)) { ?>
    <div class="title row expanded align-middle">
        <div class="columns medium-2">
            <h2 class="text-left"><a href="./dashboard.php?content=users"><i class="page-icon fa fa-user-circle"></i> <?php echo ucwords($content); ?></a></h2>
        </div>
        <div class="columns show-for-medium"></div>
        <div class="columns shrink">
            <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
                data-options="disableHover:true;clickOpen:true;">
                <li>
                    <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
                    <ul class="menu">
                        <li><a href="./dashboard.php?content=edit_user&id=<?php echo $user['id']; ?>" id="edit_create">Edit
                                User</a></li>
<!--                        <li><a data-open="delete-user-modal">Delete User</a></li>-->
                        <?php
                        if($user['display'] == 'no' && $user['active'] == 1){
                            echo'<li><a data-open="reactivate-user-modal">Reactivate User</a></li>';
                        }
                        ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <div class="row expanded">
        <div class="large-8 medium-12 column">
            <div class="card-info primary">
                <div class="card-info-content">
                    <h3 class="lead"><?php echo $user['firstname'] . ' ' . $user['lastname']; ?></h3>
                    <div class="row">
                        <div class="columns large-4 medium-12">
                            <p>Title: <?php if (!empty($user['title'])) {
                                    echo $user['title'];
                                } else {
                                    echo 'None';
                                } ?></p>
                        </div>
                        <div class="columns large-4 medium-12">
                            <p>Landline: <?php if (!empty($user['landline_phone'])) {
                                    echo $user['landline_phone'];
                                } else {
                                    echo 'None';
                                } ?></p>
                        </div>
                        <div class="columns large-4 medium-12">
                            <p>Cell: <?php if (!empty($user['phone'])) {
                                    echo $user['phone'];
                                } else {
                                    echo 'None';
                                } ?></p>
                        </div>
                        <div class="columns large-8 small-12">
                            <p>Email: <?php if (!empty($user['username'])) {
                                    echo "<a href='mailto:" . $user['username'] . "'>" . $user['username'] . "</a>";
                                } else {
                                    echo 'None';
                                } ?></p>
                        </div>
                        <div class="columns large-4 medium-12">
                            <p>Email Confirmed: <?php echo $activation_status; ?></p>
                        </div>
                        <div class="small-12 columns">
                            <p>
                                Address: <?php echo((empty($user['address'])) ? 'No record.' :
                                    $user['address'] . ', ' . $user['city'] . ', ' . $user['province'] . ' ' . $user['zip']); ?></p>
                        </div>
                    </div>
                    <?php
                    if($USER->privilege->admin_user){
                    ?>
                    <h5>Plans</h5>
                    <div class="row">
                        <?php
                        $user_plans = select_multiPlans_with_userID($user_id);
//                        $fvmdb->query("
//                            SELECT emma_group_id
//                            FROM emma_user_groups
//                            WHERE user_id = " . $user_id . "
//                        ");
                        $user_plans_array = array();
                        while ($user_plan = $user_plans->fetch_assoc()) {
                            $user_plans_array[] = $user_plan['plan_id'];
                        }
                        $allplans = select_multiPlans_with_userID($USER->id);
//                        $fvmdb->query("
//                            SELECT g.*
//                            FROM emma_groups AS g
//                            WHERE g.emma_plan_id = " . $user['emma_plan_id'] . "
//                        ");
                        while ($allplan = $allplans->fetch_assoc()) {
                            echo '
                            <div class="large-3 medium-6 small-12 columns">
                                <div class="row"> 
                                    <div class="small-4 column">
                                        <div class="switch tiny">
                                            <input class="switch-input" name="plan-ids[]" value="' .
                                $allplan['plan_id'] . '" id="plan-id-' . $allplan['plan_id'] .
                                '" type="checkbox" disabled ' .
                                (in_array($allplan['plan_id'], $user_plans_array) ? 'checked' : '') . '>
                                            <label class="switch-paddle" for="plan-id-' . $allplan['plan_id'] . '">
                                                <span class="show-for-sr">' . $allplan['name'] . '</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="small-8">
                                      <label class="text-left">' . $allplan['name'] . '</label>
                                    </div>
                                </div>
                            </div>';
                        }
                        ?>
                    </div>
                    <?php
                    }
                    ?>
                    <h5>Groups</h5>
                    <div class="row">
                        <?php
                        $user_groups = select_userGroups_with_userID($user_id);
//                        $fvmdb->query("
//                            SELECT emma_group_id
//                            FROM emma_user_groups
//                            WHERE user_id = " . $user_id . "
//                        ");
                        $user_groups_array = array();
                        while ($user_group = $user_groups->fetch_assoc()) {
                            $user_groups_array[] = $user_group['emma_group_id'];
                        }
                        $allgroups = select_groups_with_planID($user['emma_plan_id']);
//                        $fvmdb->query("
//                            SELECT g.*
//                            FROM emma_groups AS g
//                            WHERE g.emma_plan_id = " . $user['emma_plan_id'] . "
//                        ");
                        while ($allgroup = $allgroups->fetch_assoc()) {
                            echo '
                            <div class="large-3 medium-6 small-12 columns">
                                <div class="row"> 
                                    <div class="small-4 column">
                                        <div class="switch tiny">
                                            <input class="switch-input" name="group-ids[]" value="' .
                                $allgroup['emma_group_id'] . '" id="group-id-' . $allgroup['emma_group_id'] .
                                '" type="checkbox" disabled ' .
                                (in_array($allgroup['emma_group_id'], $user_groups_array) ? 'checked' : '') . '>
                                            <label class="switch-paddle" for="group-id-' . $allgroup['emma_group_id'] . '">
                                                <span class="show-for-sr">' . $allgroup['name'] . '</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="small-8">
                                      <label class="text-left">' . $allgroup['name'] . '</label>
                                    </div>
                                </div>
                            </div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="large-4 medium-12 column">
            <div class="card-info success">
                <div class="card-info-content">
                    <h3 class="lead">Usage</h3>
                    <div class="row">
                        <div class="large-12 columns">
                            <?php
                            $drills = select_user_drills($user_id);
//                                $fvmdb->query(sprintf("
//                  SELECT COUNT(*) AS count
//                  FROM emergencies AS e
//                  JOIN emergency_types AS t ON e.emergency_type_id = t.emergency_type_id
//                  WHERE created_by_id = %s
//                  AND e.drill = 1
//              ", $user_id));
                            $drill = $drills->fetch_assoc();

                            $emergencies = select_user_emergencies($user_id);
//                                $fvmdb->query(sprintf("
//                  SELECT COUNT(*) AS count
//                  FROM emergencies AS e
//                  JOIN emergency_types AS t ON e.emergency_type_id = t.emergency_type_id
//                  WHERE created_by_id = %s
//                  AND e.drill = 0
//              ", $user_id));
                            $emergency = $emergencies->fetch_assoc();

                            $drill_responses = select_user_drillResponses($user_id);
//                                $fvmdb->query(sprintf("
//                  SELECT count(*) AS count
//                  FROM emergency_responses AS r
//                  JOIN emergencies AS e ON r.emergency_id = e.emergency_id
//                  JOIN emergency_types AS t ON e.emergency_type_id = t.emergency_type_id
//                  WHERE created_by_id = %s
//                  AND e.drill = 1
//              ", $user_id));
                            $drill_response = $drill_responses->fetch_assoc();

                            $emergency_responses = select_user_emergencyResponses($user_id);
//                                $fvmdb->query(sprintf("
//                  SELECT count(*) AS count
//                  FROM emergency_responses AS r
//                  JOIN emergencies AS e ON r.emergency_id = e.emergency_id
//                  JOIN emergency_types AS t ON e.emergency_type_id = t.emergency_type_id
//                  WHERE created_by_id = %s
//                  AND e.drill = 0
//              ", $user_id));
                            $emergency_response = $emergency_responses->fetch_assoc();
                            ?>
                            <p>Drills Created: <?php echo $drill['count']; ?></p>
                            <p>Emergencies Created: <?php echo $emergency['count']; ?></p>
                            <p>Drills Responses: <?php echo $drill_response['count']; ?></p>
                            <p>Emergency Responses: <?php echo $emergency_response['count']; ?></p>
                            <p>App Usage: <?php echo '100%'; ?></p>
                            <p>PC Usage: <?php echo '0%'; ?></p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="delete-user-modal" class="reveal callout alert text-center tiny" data-reveal
         data-animation-in="fade-in"
         data-animation-out="fade-out">
        <form id="delete-user-form" action="#" method="post" onsubmit="return false;">
            <input type="hidden" name="user-id" value="<?php echo $user_id; ?>" />
            <h4 style="color:darkred">Are you sure?</h4>
            <div class="large-4 columns" style="margin: 0 auto;">
                <div class="button-group expanded">
                    <a class="button alert" data-close>Cancel</a>
                    <input class="button alert" type="submit" value="OK" />
                </div>
            </div>
        </form>
    </div>
    <div id="reactivate-user-modal" class="reveal callout text-center tiny" data-reveal
         data-animation-in="fade-in"
         data-animation-out="fade-out">
        <form id="reactivate-user-form" action="#" method="post" onsubmit="return false;">
            <input type="hidden" name="user-id" value="<?php echo $user_id; ?>" />
            <h4 style="color:darkred">Are you sure?</h4>
            <div class="large-4 columns" style="margin: 0 auto;">
                <div class="button-group expanded">
                    <a class="button alert" data-close>Cancel</a>
                    <input class="button" type="submit" value="OK" />
                </div>
            </div>
        </form>
    </div>
    <div id="success_modal" class="reveal success callout tiny text-center"
         data-reveal data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4>Success</h4>
        <a href="./dashboard.php?content=users" data-close
           class="button success" style="margin-left:auto;margin-right:auto">Ok</a>
        <!--    <button class="close-button" data-close aria-label="Close reveal"-->
        <!--      type="button">-->
        <!--      <span aria-hidden="true">&times;</span>-->
        <!--    </button>-->
    </div>

<?php } ?>


