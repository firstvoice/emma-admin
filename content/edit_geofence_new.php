<?php
    $id = $fvmdb->real_escape_string($_GET['id']);
    $geos = array();
    $sites = select_sites_with_planID($USER->emma_plan_id);
    $site = $sites->fetch_assoc();
?>
<div class="title row expanded align-middle">
    <div class="columns medium-4">
        <h2 class="text-left"><a href="./dashboard.php?content=geofences"><i class="page-icon fa fa-globe"></i> Edit Geofence</a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
    </div>
</div>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<meta charset="utf-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<div class="row expanded" style="height: 70%">
    <div class="large-8 medium-12 small-12 columns">
        <div id="map" style="border: solid 1px #0078C1; margin-top: 1%"></div>
        <!--                <div class="over_map">-->
        <!--                    <button class="button" id="geoFence">Create Geofence</button>-->
        <!--                    <div id="gfInfo" class="gfInfoHidden row">-->
        <!--                        <div class="large-8 columns">-->
        <!--                            <input type="text" id="gfName" name="Geofenced Name" placeholder="Geofence Area Name">-->
        <!--                        </div>-->
        <!--                        <div class="large-4 columns">-->
        <!--                            <button class="button" id="save">Submit</button>-->
        <!--                        </div>-->
        <!--                    </div>-->
        <!--                    <div id="tools" class="toolsHidden">-->
        <!--                        <!-- <button id="rec">Draw Rec</button> -->
        <!--                        <button class="button" id="rec2">Draw Rectangle</button>-->
        <!--                        <button class="button" id="cir">Draw Circle</button>-->
        <!--                    </div>-->
        <!--                    <p id="nameGFCheck" class="errorMsg">Please enter a name for the geofence area</p>-->
        <!--                    <p id="shapeGFCheck" class="errorMsg">Please draw at least 1 shape</p>-->
        <!--                </div>-->
    </div>
    <div class="large-4 medium-12 small-12 columns">
        <div class="card-info primary">
            <div class="card-info-content">
                <div class="over_map">
                    <div class="row">
                        <table>
                            <thead>
                                <tr>
                                    <th>Latitude</th>
                                    <th>Longitude</th>
                                    <th>Radius(Miles)</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                $parent = select_geofence_geofence($id);
                                $geos[] = $p = $parent->fetch_assoc();
                                $childs = select_getgeofence_children($id);

                                while($children = $childs->fetch_assoc()){
                                    $geos[] = $children;
                                    $childarr[] = [
                                        'id' => $children['id'],
                                        'lat' => $children['center_lat'],
                                        'lng' => $children['center_lng'],
                                        'rad' => $children['radius'],
                                    ];
                                }
                                foreach ($geos as $g){
                                    echo'
                                    <tr id="circle-'. $g['id'] .'" class="circle-edit" style="cursor: pointer;" data-id="'. $g['id'] .'" data-lat="'. $g['center_lat'] .'" data-lng="'. $g['center_lng'] .'" data-rad="'. $g['radius'] .'">
                                        <td>'. $g['center_lat'] .'</td>
                                        <td>'. $g['center_lng'] .'</td>
                                        <td>'. $g['radius'] .'</td>
                                    </tr>
                                    ';
                                }
                            ?>
                            <script type="text/javascript">
                                let fenceParent = <?php echo json_encode($p); ?>;
                                let fenceChildren = <?php echo json_encode($childarr); ?>;
                            </script>

                            </tbody>
                        </table>
                    </div>
                </div>
                <script>

                    var map;
                    let json = {
                        gfName: "",
                        circles: [],
                        // rectangles:[],
                        circleObjects:[],
                        infoWindowsCircles:[],
                        recObjects: [],
                        infoWindowsRec: []
                    };
                    var drawingManager;
                    function initMap() {
                        map = new google.maps.Map(document.getElementById('map'), {
                            center: {lat: <?php echo $site['emma_site_latitude'];?>, lng: <?php echo $site['emma_site_longitude'];?>},
                            zoom: 14
                        });
                    }

                    function add_geofence_circle(id,lat,lng,radmiles) {
                        var circles = [];
                        var fence = new google.maps.Circle({
                            center: {lat: parseFloat(lat), lng: parseFloat(lng)},
                            radius: (parseFloat(radmiles) * 1609.344),
                            id: id,
                            strokeColor: '#FF0000',
                            strokeOpacity: 0.8,
                            strokeWeight: 3,
                            fillColor: '#ffffff',
                            fillOpacity: 0.35
                        });
                        circles[fence.id] = fence;
                        fence.setMap(map);
                        google.maps.event.addDomListener(document.getElementById('circle-'+id), 'mouseover', function() {
                            var circle = circles[id];
                            circle.setOptions({fillColor: '#7faf8e'});
                        });
                        google.maps.event.addDomListener(document.getElementById('circle-'+id), 'mouseout', function() {
                            var circle = circles[id];
                            circle.setOptions({fillColor: '#ffffff'});
                        });
                    }

                    // This example requires the Drawing library. Include the libraries=drawing
                    // parameter when you first load the API. For example:
                    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=drawing">



                    // document.getElementById("save").addEventListener("click", function(){
                    //     let gfName = document.getElementById("gfName").value;
                    //     if(gfName !== null && gfName !== undefined && gfName !== "") {
                    //         json.gfName = gfName;
                    //
                    //         if (json.circleObjects.length > 0 || json.recObjects.length > 0){
                    //             console.log(json);
                    //
                    //             // Close drawing manager
                    //             if(drawingManager) {
                    //                 drawingManager.setMap(null);
                    //             }
                    //             document.getElementById("gfInfo").style.display = "none";
                    //             document.getElementById("tools").style.display = "none";
                    //             document.getElementById("geoFence").style.display = "flex";
                    //
                    //             document.getElementById("circle-container").innerHTML = `<input type="hidden" name="geoname" value="`+ gfName +`">`;
                    //             for(var i=0; i < json.circles.length; i++) {
                    //                 if (!!json.circles[i]) {
                    //                     document.getElementById("circle-container").innerHTML += `<input type="hidden" name="geolat[]" value="` + json.circles[i].centerLat.toString() + `">`;
                    //                     document.getElementById("circle-container").innerHTML += `<input type="hidden" name="geolng[]" value="` + json.circles[i].centerLng.toString() + `">`;
                    //                     document.getElementById("circle-container").innerHTML += `<input type="hidden" name="georad[]" value="` + json.circles[i].radiusMeters.toString() + `">`;
                    //                 }
                    //             }
                    //             $("#confirm_geofence_modal").foundation("open");
                    //             // json = {
                    //             //   gfName: "",
                    //             //   circles: [],
                    //             //   circleObjects:[],
                    //             //   recObjects: []
                    //             // };
                    //         } else {
                    //             document.getElementById("shapeGFCheck").style.display = "flex";
                    //             setTimeout(function(){
                    //                 document.getElementById("shapeGFCheck").style.display = "none";
                    //             }, 3000);
                    //         }
                    //
                    //     } else {
                    //         document.getElementById("nameGFCheck").style.display = "flex";
                    //         setTimeout(function(){
                    //             document.getElementById("nameGFCheck").style.display = "none";
                    //         }, 3000);
                    //     }
                    //
                    //
                    //
                    //     // for(var i = 0;i < json.circleObjects.length; i++) {
                    //     //   json.circleObjects[i].setOptions({editable:false});
                    //     //   json.circleObjects[i].setOptions({draggable:false});
                    //     // }
                    //
                    //     // for(var i = 0;i < json.recObjects.length; i++) {
                    //     //   json.recObjects[i].setOptions({editable:false});
                    //     //   json.recObjects[i].setOptions({draggable:false});
                    //     // }
                    //
                    //
                    // });

                    // document.getElementById("geoFence").addEventListener("click", function(){
                    //     // drawCir();
                    //     document.getElementById("tools").style.display = "flex";
                    //     document.getElementById("gfInfo").style.display = "flex";
                    //     document.getElementById("geoFence").style.display = "none";
                    // });

                    // document.getElementById("rec").addEventListener("click", function(){
                    //   drawRec();
                    // });
                    // document.getElementById("rec2").addEventListener("click", function(){
                    //     drawRecWithCircles();
                    // });
                    // document.getElementById("cir").addEventListener("click", function(){
                    //     drawCir();
                    // });
                </script>
            </div>
        </div>
    </div>
</div>


<script src="js/circlesRectangles.js"></script>
<script src="js/rectangle.js"></script>
<script src="js/circles.js"></script>
<!--<script src="js/create_geofence_new.js"></script>-->



<div id="success_modal" class="reveal callout tiny success text-center"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <div class="row columns">
        <a data-close class="button success"
           style="margin-left:auto;margin-right:auto;" href="./dashboard.php?content=geofences">Ok</a>
    </div>
</div>

<div id="edit-circle-modal" class="reveal callout tiny text-center"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Edit Geofence</h4>
    <br>
    <div class="row columns">
        <form id="edit-circle-form" class="text-center" style="margin: 0 auto;">
            <div class="large-12 columns">
                <div class="row">
                    <div class="large-4 columns">
                        <label>Latitude</label>
                        <input type="text" id="edit-circle-lat" name="center-lat">
                    </div>
                    <div class="large-4 columns">
                        <label>Longitude</label>
                        <input type="text" id="edit-circle-lng" name="center-lng">
                    </div>
                    <div class="large-4 columns">
                        <label>Radius(Miles)</label>
                        <input type="text" id="edit-circle-rad" name="radius">
                    </div>
                </div>
            </div>
            <input type="hidden" id="edit-circle-id" name="id">
            <input type="hidden" name="user-id" value="<?php echo $USER->id;?>">
            <a data-close class="button alert">Cancel</a>
            <input type="submit" class="button" value="Submit">
        </form>
    </div>
</div>