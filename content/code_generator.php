<div class="title row expanded align-middle">
  <div class="columns medium-4">
    <h2 class="text-left"><a href="./dashboard.php?content=code_generator"><i class="page-icon fa fa-ticket"></i> Code
        Generator</a></h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
  </div>
</div>
<div class="row expanded">
  <div class="large-6 medium-12 small-12 columns">
    <div class="card-info primary">
      <div class="card-info-content">
        <form id="save-code-form" action="./process/generate_code.php"
          method="post">
          <h3 class="lead">Code Generator</h3>
          <input type="hidden" name="jwt"
            value="<?php echo $_COOKIE['jwt']; ?>"/>
          <input type="hidden" name="plan-id"
            value="<?php echo $USER->emma_plan_id; ?>"/>
          <div class="row expanded">
            <div class="large-6 medium-12 small-12 columns">
              <label>Start Date <span style="color: red">*</span>
                <input id="start-date" type="date" name="start-date"/>
              </label>
            </div>
            <div class="large-6 medium-12 small-12 columns">
              <label>End Date <span style="color: red">*</span>
                <input id="end-date" type="date" name="end-date"/>
              </label>
            </div>
          </div>
          <div class="row expanded">
            <div class="large-6 medium-12 small-12 columns">
              <label>Group <span style="color: red">*</span>
                <select name="group-id">
                    <option value="">-Select-</option>
                  <?php
                  $groups = select_changePin_groups($USER->emma_plan_id);
//                      $fvmdb->query("
//                    select *
//                    from emma_groups
//                    where emma_plan_id = '" . $USER->emma_plan_id . "'
//                    and info_only = 1
//                    order by name
//                  ");
                  while ($group = $groups->fetch_assoc()) {
                    echo '<option value="' . $group['emma_group_id'] . '">' .
                      $group['name'] . '</option>';
                  }
                  ?>
                </select>
              </label>
            </div>
            <div class="large-6 medium-12 small-12 columns">
              <label>Duration (Minutes)
                <input type="number" name="duration"/>
              </label>
            </div>
          </div>
          <div class="row expanded">
              <div class="input-group large-9 medium-12 small-12 columns">
                <span class="input-group-label">Code</span>
                <input id="code" name="code" class="input-group-field"
                  type="text">
              </div>
              <div class="large-3 medium-12 small-12 columns">
                  <input type="button" id="generate-code" class="button expanded" value="Generate">
              </div>
          </div>
          <div class="row expanded">
              <div class="large-10 medium-8 small-6 columns">

              </div>
              <div class="large-2 medium-4 small-6 columns">
                  <button type="submit" class="button" style="float: right">Submit</button>
              </div>
          </div>
        </form>
      </div>
    </div><!--/ Message Center -->
  </div>
</div>


<div id="success_modal" class="reveal callout tiny success text-center"
  data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Success</h4>
  <div class="row columns">
    <a data-close class="button success"
      style="margin-left:auto;margin-right:auto;" href="./dashboard.php?content=guest_codes">Ok</a>
  </div>
</div>