<?php
global $USER;
global $fvmdb;

if (!($USER->privilege->security || $USER->privilege->admin)) {
  redirect();
}

?>

<div class="title row expanded align-middle">
  <div class="columns medium-2">
    <h2 class="text-left"><a
        href="./dashboard.php?content=aeds"><i class="page-icon fa fa-briefcase"></i> AEDs</a>
    </h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
      data-options="disableHover:true;clickOpen:true;">
      <li>
        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <ul class="menu">
          <li><a href="#" data-open="create-aed-modal">Create AED</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>

<table id="aeds-table" class="data-table" style="width:100%">
  <thead>
  <tr>
    <th class="text-search">Organization</th>
    <th class="text-search">Location</th>
    <th class="text-search">Brand</th>
    <th class="text-search">Model</th>
    <th class="text-search">Serial Number</th>
    <th class="text-search">Last Checked</th>
    <th class="text-search">Status</th>
  </tr>
  <tr>
    <th>Organization</th>
    <th>Location</th>
    <th>Brand</th>
    <th>Model</th>
    <th>Serial Number</th>
    <th>Last Checked</th>
    <th>Status</th>
  </tr>
  </thead>
  <tbody>
  </tbody>
  <tfoot>
  </tfoot>
</table>

<div id="create-aed-modal" class="reveal callout text-center small"
  data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Create AED</h4>
  <hr>
  <form id="create-aed-form" class="create-aed-form text-left" action="#"
    method="post">
    <input type="hidden" name="user-id" value="<?php echo $USER->id; ?>"/>
    <div class="row">
      <div class="large-12 columns">
        <div class="row">
          <div class="large-6 columns">
            <label>Organization
              <select required name="org-id" id="create_aed_form_org_id">
                <option value="" selected="selected" hidden="hidden">-- Select Organization --</option>
                <?php
                $orgs = $fvmdb->query("
                  SELECT o.id, o.name
                  FROM organizations o
                  JOIN emma_plan_orgs epo ON (epo.org_id = o.id AND epo.plan_id = ".$USER->emma_plan_id.")
                ");
                while($org = $orgs->fetch_assoc()){
                  echo '<option value="'.$org['id'].'">'.$org['name'].'</option>';
                }
                ?>
              </select>
            </label>
          </div>
          <div class="large-6 columns">
            <label>Location
              <select required name="loc-id" id="create_aed_form_loc_id">
              </select>
            </label>
          </div>
        </div>
        <div class="row">
          <div class="large-6 columns">
            <label>Brand
              <select required name="brand-id" id="create_aed_form_brand_id">
                <option value="" selected="selected" hidden="hidden">-- Select Brand --</option>
                <?php
                $brands = $fvmdb->query("
                  SELECT ab.aed_brand_id, ab.brand
                  FROM aed_brands ab
                ");
                while($brand = $brands->fetch_assoc()){
                  echo '<option value="'.$brand['aed_brand_id'].'">'.$brand['brand'].'</option>';
                }
                ?>
              </select>
            </label>
          </div>
          <div class="large-6 columns">
            <label>Model
              <select required name="model-id" id="create_aed_form_model_id">
              </select>
            </label>
          </div>
        </div>
        <div class="row">
          <div class="large-6 columns">
            <label>Serial Number
              <input required type="text" id="serial" name="serial" value=""/>
            </label>
          </div>
          <div class="large-6 columns">
            <label>Placement
              <input required type="text" id="serial" name="placement" value=""/>
            </label>
          </div>
        </div>
        <div class="row">
          <div class="large-6 columns">
            <label>Latitude
              <input required type="number" id="entered_lat" name="latitude" step="any" value=""/>
            </label>
          </div>
          <div class="large-6 columns">
            <label>Longitude
              <input required type="number" id="entered_lng" name="longitude" step="any" value=""/>
            </label>
          </div>
        </div>
        <div class="row">
          <div class="large-12 columns">
            <label>Address
              <input required type="text" name="address" value=""/>
            </label>
          </div>
        </div>
        <div class="row">
          <div class="large-12 columns">
            <label>Notes
              <textarea name="notes"></textarea>
            </label>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="large-12 columns">
          <div id="pad-row" class="row">
            <div class="large-6 columns">
              <label>Pad
                <select id="pad-select" name="pad">
                </select>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Lot
                <input type="text" name="pad-lot" value=""/>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Expiration
                <input type="date" name="pad-expiration" value=""/>
              </label>
            </div>
          </div>
          <div id="ped-pad-row" class="row">
            <div class="large-6 columns">
              <label>Pediatric Pad
                <select id="ped-pad-select" name="ped-pad">
                </select>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Lot
                <input type="text" name="ped-pad-lot" value=""/>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Expiration
                <input type="date" name="ped-pad-expiration" value=""/>
              </label>
            </div>
          </div>
          <div id="spare-pad-row" class="row">
            <div class="large-6 columns">
              <label>Spare Pad
                <select id="spare-pad-select" name="spare-pad">
                </select>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Lot
                <input type="text" name="spare-pad-lot" value=""/>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Expiration
                <input type="date" name="spare-pad-expiration" value=""/>
              </label>
            </div>
          </div>
          <div id="spare-ped-pad-row" class="row">
            <div class="large-6 columns">
              <label>Spare Pediatric Pad
                <select id="spare-ped-pad-select" name="spare-ped-pad">
                </select>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Lot
                <input type="text" name="spare-ped-pad-lot" value=""/>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Expiration
                <input type="date" name="spare-ped-pad-expiration" value=""/>
              </label>
            </div>
          </div>
          <div id="pak-row" class="row">
            <div class="large-6 columns">
              <label>Pak
                <select id="pak-select" name="pak">
                </select>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Lot
                <input type="text" name="pak-lot" value=""/>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Expiration
                <input type="date" name="pak-expiration" value=""/>
              </label>
            </div>
          </div>
          <div id="ped-pak-row" class="row">
            <div class="large-6 columns">
              <label>Pediatric Pak
                <select id="ped-pak-select" name="ped-pak">
                </select>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Lot
                <input type="text" name="ped-pak-lot" value=""/>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Expiration
                <input type="date" name="ped-pak-expiration" value=""/>
              </label>
            </div>
          </div>
          <div id="spare-pak-row" class="row">
            <div class="large-6 columns">
              <label>Spare Pak
                <select id="spare-pak-select" name="spare-pak">
                </select>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Lot
                <input type="text" name="spare-pak-lot" value=""/>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Expiration
                <input type="date" name="spare-pak-expiration" value=""/>
              </label>
            </div>
          </div>
          <div id="spare-ped-pak-row" class="row">
            <div class="large-6 columns">
              <label>Spare Pediatric Pak
                <select id="spare-ped-pak-select" name="spare-ped-pak">
                </select>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Lot
                <input type="text" name="spare-ped-pak-lot" value=""/>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Expiration
                <input type="date" name="spare-ped-pak-expiration" value=""/>
              </label>
            </div>
          </div>
          <div id="battery-row" class="row">
            <div class="large-6 columns">
              <label>Battery
                <select id="battery-select" name="battery">
                </select>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Lot
                <input type="text" name="battery-lot" value=""/>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Expiration
                <input type="date" name="battery-expiration" value=""/>
              </label>
            </div>
          </div>
          <div id="spare-battery-row" class="row">
            <div class="large-6 columns">
              <label>Spare Battery
                <select id="spare-battery-select" name="spare-battery">
                </select>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Lot
                <input type="text" name="spare-battery-lot" value=""/>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Expiration
                <input type="date" name="spare-battery-expiration" value=""/>
              </label>
            </div>
          </div>
          <div id="accessory-row" class="row">
            <div class="large-6 columns">
              <label>Accessory
                <select id="accessory-select" name="accessory">
                </select>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Lot
                <input type="text" name="accessory-lot" value=""/>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Expiration
                <input type="date" name="accessory-expiration" value=""/>
              </label>
            </div>
          </div>
      </div>
    </div>
    <hr>
    <div class="large-12 columns">
      <div id="edit-map" style="width: 100%;height: 400px;"></div>
    </div>
    <br>
    <div class="row">
      <div class="large-4 columns button-group expanded" style="margin: 0 auto;">
        <a href="./dashboard.php?content=aeds" class="button alert">Cancel</a>
        <input type="submit" class="button" value="Submit"/>
      </div>
    </div>
  </form>
  <button class="close-button" data-close="" aria-label="Close reveal"
    type="button">
    <span aria-hidden="true">×</span>
  </button>
</div>

<div id="aed-success-modal" class="reveal callout success text-center tiny"
  data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Success</h4>
  <a href="dashboard.php?content=aeds" class="button success">OK</a>
</div>