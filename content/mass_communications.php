
<?php

if (!($USER->privilege->security || $USER->privilege->admin)) {
    redirect();
}

?>
<div class="title row expanded align-middle">
  <div class="columns medium-6">
    <h2 class="text-left"><a href="./dashboard.php?content=mass_communications"><i class="page-icon fa fa-bell-o"></i> Mass
        Notifications</a></h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
      data-options="disableHover:true;clickOpen:true;">
      <li>
        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <ul class="menu">
          <li><a href="#" data-open="create-mass-communication-modal">Create Mass
              Notification</a>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</div>

<table id="mass-communications-table" class="data-table" style="width:100%">
  <thead>
  <tr>
      <th class="text-search">Date</th>
      <th class="text-search">Creator</th>
      <th class="text-search">Notification</th>
  </tr>
  <tr>
    <th>Date</th>
    <th>Creator</th>
    <th>Notification</th>
  </tr>
  </thead>
  <tbody>
  </tbody>
  <tfoot>

  </tfoot>
</table>
<?php
$groupsString = '';
$groups = select_groups_with_planID($USER->emma_plan_id);
//$fvmdb->query("
//  SELECT *
//  FROM emma_groups
//  WHERE emma_plan_id = '" . $USER->emma_plan_id . "'
//  ORDER BY name
//");
while ($group = $groups->fetch_assoc()) {
  $groupsString .= '
    <div class="large-4 medium-12 small-12 columns">
    <div class="row">
      <div class="small-3 column">
        <div class="switch tiny">
          <input class="switch-input group-select" 
            id="group-' . $group['emma_group_id'] . '" 
            type="checkbox" 
            name="groups[' . $group['emma_group_id'] . ']" 
            value="' . $group['emma_group_id'] . '">
          <label class="switch-paddle" for="group-' . $group['emma_group_id'] . '">
            <span class="show-for-sr">' . $group['name'] . '</span>
          </label>
        </div>
      </div>
      <div class="small-9 column">
        <label class="text-left">' . $group['name'] . '</label>
      </div>
    </div>
    </div>
  ';
}
?>

<!--<div id="create-mass-communication-modal"-->
<!--  class="reveal callout text-center small"-->
<!--  data-reveal data-animation-in="fade-in"-->
<!--  data-animation-out="fade-out">-->
<!--  <h4>Create Mass Notification</h4>-->
<!--  <hr>-->
<!--  <form id="create-mass-communication-form" class="text-left" action="#"-->
<!--    method="post">-->
<!--    <input type="hidden" name="user-id" value="--><?php //echo $USER->id; ?><!--"/>-->
<!--    <input type="hidden" name="plan-id"-->
<!--      value="--><?php //echo $USER->emma_plan_id; ?><!--"/>-->
<!--    <div class="row">-->
<!--      <div class="small-12 columns">-->
<!--        <label>Groups</label>-->
<!--        <div class="row">-->
<!--            <div class="large-4 medium-12 small-12 columns">-->
<!--                <div class="row">-->
<!--                    <div class="small-3 column">-->
<!--                        <div class="switch tiny">-->
<!--                            <input class="switch-input all-select" id="all-select" type="checkbox" >-->
<!--                            <label class="switch-paddle" for="all-select">-->
<!--                                <span class="show-for-sr">All</span>-->
<!--                            </label>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="small-9 column">-->
<!--                        <label class="text-left">All</label>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            --><?php //echo $groupsString; ?><!--</div>-->
<!--      </div>-->
<!--    </div>-->
<!--    <div class="row">-->
<!--      <div class="small-12 columns">-->
<!--        <label>Notification-->
<!--          <textarea name="notification"-->
<!--            style="min-width:100%;height:5rem;"></textarea>-->
<!--        </label>-->
<!--      </div>-->
<!--    </div>-->
<!--    <div class="row">-->
<!--      <div class="large-3 columns"></div>-->
<!--      <div class="large-6 large-centered columns">-->
<!--        <input type="submit" class="button expanded" value="Create"/>-->
<!--      </div>-->
<!--      <div class="large-3 columns"></div>-->
<!--    </div>-->
<!--  </form>-->
<!--  <button class="close-button" data-close="" aria-label="Close reveal"-->
<!--    type="button">-->
<!--    <span aria-hidden="true">×</span>-->
<!--  </button>-->
<!--</div>-->
