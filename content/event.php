<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 11/15/2017
 * Time: 11:11 AM
 */
$id = $fvmdb->real_escape_string($_GET['id']);


//verify if allowed
//get plan event is in
//check if user is in plan
//check if user is admin rights for plan
$userID = $USER->id;

$eventPlanResult = select_event_eventID($id);
$eventPlan = $eventPlanResult->fetch_assoc();
$eventPlanID = $eventPlan['emma_plan_id'];

$userGroupsWithAdminResult = select_userGroupsWithAdmin_userID($userID, $eventPlanID);
if($userGroupsWithAdminResult->num_rows > 0){

    if($eventPlanID != $USER->emma_plan_id) {

        $CONFIG = json_decode(file_get_contents('config/config.json'));
        $USER = null;

        $token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key, array('HS512'));

        $USER = $token->data;

        $plans = select_plan_from_planID($eventPlanID);
        $plan = $plans->fetch_assoc();

        if ($plan['securities']) $plansecurity = true;
        if ($plan['panic']) $plansos = true;


        $token->data->emma_plan_id = $eventPlanID;
        $token->data->privilege->plansecurity = $plansecurity;
        $token->data->privilege->plansos = $plansos;
        $jwt = Firebase\JWT\JWT::encode($token, $CONFIG->key, 'HS512');
        setcookie('jwt', $jwt, time() + (9 * 60 * 60), '/');
    }

    $events = select_event_events($id);

}
else{
   $events = null;
}



//$events = select_event_events($id);
//    $fvmdb->query(sprintf("
//    SELECT e.*, et.name AS type, et.img_filename AS type_filename, es.emma_site_id, es.emma_site_name, CONCAT(u.firstname, ' ', u.lastname) AS reported_by, u.id AS user_id, u.username AS user_email, u.phone AS user_phone, est.name AS sub_type_name, u.emma_pin AS user_pin, e.created_pin, ecl.call_datetime, CONCAT(ucall.firstname, ' ', ucall.lastname) as called_by, ecl.latitude as lat, ecl.longitude as lng
//    FROM emergencies as e
//    JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
//    JOIN emergency_sub_types est ON e.emergency_sub_type_id = est.emergency_sub_type_id
//    LEFT JOIN emma_sites es ON e.emma_site_id = es.emma_site_id
//    JOIN users u ON e.created_by_id = u.id
//    LEFT JOIN emma_911_call_log ecl on e.emergency_id = ecl.emergency_id
//    LEFT JOIN users ucall on ecl.userid = ucall.id
//    WHERE e.emergency_id = %s
//", $id));


//Select all AED locations.
$aeds = select_events_aeds();
//    $fvmdb->query("
//SELECT a.latitude,a.longitude,a.location,a.sitecoordinator,c.name as contact_name, c.company, loc.address
//FROM aeds a
//JOIN location_contacts lc on lc.location_id = a.location_id
//JOIN contacts c on c.id = lc.contact_id
//JOIN locations as loc on loc.id = a.location_id
//where a.current = 1
//");
$police_calls = select_event_policeCalls($USER->emma_plan_id);
//    $fvmdb->query("
//SELECT ecl.latitude, ecl.longitude, ecl.call_datetime, CONCAT(u.firstname, ' ', u.lastname) as user_name
//FROM emma_911_call_log as ecl
//JOIN users u on ecl.userid = u.id
//WHERE ecl.plan_id = '".$USER->emma_plan_id."'
//AND ecl.active = 1
//");

echo '<script>';
echo 'let police_calls = [];';
echo 'let positions = [];';
echo 'let assets = [];';
while($aed = $aeds->fetch_assoc())
{
    //If the name of the location contains valid characters and isn't empty.
    if(!preg_match('/[^A-Za-z0-9]/', $aed['location']) && !empty($aed['location']))
    {
        if((!empty($aed['latitude']) && !empty($aed['longitude'])) && ($aed['latitude'] != "0" && $aed['longitude'] != "0") && (!empty($aed['location'])))
        {
            echo 'var pos = {lat: '.$aed['latitude'].', lng: '.$aed['longitude'].', location: "'.$aed['location'].'", coordinator: "'.$aed['sitecoordinator'].'", contact: "'.$aed['contact_name'].'", company: "'.$aed['company'].'", address: "'.$aed['address'].'"};';
            echo 'positions.push(pos);';
        }
    }
}
while($police_call = $police_calls->fetch_assoc())
{

    if(!empty($police_call['latitude'] && !empty($police_call['longitude'])))
    {
        echo 'var call = {lat: '.$police_call['latitude'].', lng: '.$police_call['longitude'].', username: "'.$police_call['user_name'].'", calldate: "'.$police_call['call_datetime'].'"};';
        echo 'police_calls.push(call);';
    }
}
echo '</script>';

//Select all ASSET locations.
$assets = select_event_assets($USER->emma_plan_id);
//    $fvmdb->query("
//SELECT eat.name, a.latitude as lat, a.longitude as lng, a.address, eai.image
//FROM emma_assets a
//LEFT JOIN emma_asset_types as eat on eat.emma_asset_type_id = a.emma_asset_type_id
//LEFT JOIN emma_asset_icons as eai on eat.emma_asset_icon_id = eai.asset_icon_id
//WHERE a.active = '1'
//AND a.emma_plan_id = '".$USER->emma_plan_id."'
//AND a.active = 1
//AND a.deleted = 0
//");

echo '<script>';
while ($asset = $assets->fetch_assoc()) {
//If the name of the location contains valid characters and isn't empty.
    if((!empty($asset['lat'])) && ($asset['lat'] != 0) && ($asset['lat'] != 1) && (!empty($asset['lng'])) && ($asset['lng'] != 0) && ($asset['lng'] != 1) && ($asset['address'] != null) && ($asset['address'] != '')) {
        echo 'var ast = {lat: ' . $asset['lat'] . ', lng: ' . $asset['lng'] . ', name: "' . $asset['name'] . '", address: "'.$asset['address'].'", image: "'.$asset['image'].'"};';
        echo 'assets.push(ast);';
    }
}
echo '</script>';




if ($events->num_rows > 0)
{
    $event = $events->fetch_assoc();
    ?>
    <div class="title row expanded align-middle">
        <div class="columns medium-8">
            <h2 class="text-left"><?php echo $event['drill'] === '1' ? '<a href="./dashboard.php?content=drills"><i class="page-icon fa fa-exclamation-triangle"></i> ' . ucwords($content) . '<span style="margin:1rem;color:red;">(DRILL)</span></a>' : '<a href="./dashboard.php?content=events"><i class="page-icon fa fa-exclamation-triangle"></i> ' . ucwords($content) . '</a>'; ?></h2>
        </div>
        <div class="columns show-for-medium"></div>
        <div class="columns shrink">
            <!--      <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu-->
            <!--          data-options="disableHover:true;clickOpen:true;">-->
            <!--        <li>-->
            <!--          <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>-->
            <!--          <ul class="menu">-->
            <!--            <li><a href="./dashboard.php?content=map&id=-->
            <?php //echo $id; ?><!--">View Map</a></li>-->
            <!--            <li><a href="./dashboard.php?content=events">Close Event</a></li>-->
            <!--            <li><a href="*" data-open="broadcast-event-modal">Broadcast Event</a></li>-->
            <!--          </ul>-->
            <!--        </li>-->
            <!--      </ul>-->
        </div>
    </div>
    <script type="text/javascript">
        let mainEvent = <?php echo json_encode($event); ?>;
        console.log(mainEvent);
        <?php if($event['parent_emergency_id'] == ''){ ?>
        let eventId = <?php echo json_encode($event['emergency_id']); ?>;
        <?php  }else{?>
        let eventId = <?php echo json_encode($event['parent_emergency_id']); ?>;
        <?php  }?>
    </script>
    <div class="row expanded">
        <div class="large-6 medium-12 small-12 columns">
            <div class="card-info primary">
                <div class="card-info-content">
                    <div class="row">
                        <div class="large-6 medium-12 small-12 columns">
                            <h3 class="lead"><?php echo $event['parent_emergency_id'] ? 'Sub-Event Details' : 'Details'; ?></h3>
                            <p>Event
                                Status: <?php echo $event['active'] == 1 ? '<span style="color:green;">Active</span>' : '<span style="color:red;">Closed</span>'; ?></p>
                            <p>Event Pin
                                Used: <?php echo($event['created_pin'] == $event['user_pin'] ?
                                    '<span style="color:green;">Valid</span>' : '<span style="color:red;">Invalid</span>') ?></p>
                            <p>Original Alert
                                Reported: <?php echo date('n/j/Y g:i:s a', strtotime($event['created_date'])); ?></p>
                            <p>Site:
                                <a
                                        href="./dashboard.php?content=site&id=<?php echo $event['emma_site_id']; ?>"><?php echo $event['emma_site_name']; ?></a>
                            </p>
                            <p>
                                In App 911 Call Date: <?php
                               // if($event['called911'] == ''){
                               //     echo 'NOT CALLED';
                               // }else{
                               //     echo date('n/j/Y g:i:s a', strtotime($event['called911']));
                               // }

                                if($event['call_datetime'] != null)
                                {
                                    echo $event['call_datetime'];
                                    echo '<p>Called By: '.$event['called_by'].'</p>';
                                    echo '<p>Call Latitude: '.$event['latitude'].'</p>'; //RANDOMLY GENERATED FOR NOW
                                    echo '<p>Call Latitude: '.$event['longitude'].'</p>'; //RANDOMLY GENERATED FOR NOW
                                }
                                else{
                                    echo 'NOT CALLED';

                                }
                                ?>

                            </p>


                            <p>Reported By: <?php echo $event['reported_by']; ?></p>
                            <p>User Email: <a href="mailto:<?php echo $event['user_email']; ?>"><?php echo $event['user_email']; ?></a></p>
                            <p>User Phone: <?php echo $event['user_phone']; ?></p>
                            <p>Latitude: <?php echo $event['latitude'];?></p>
                            <p>Longitude: <?php echo $event['longitude'];?></p>
                            <p>Nearest Address: <?php echo $event['nearest_address'];?></p>
                            <p>Emergency Details: <?php echo $event['description']; ?></p>
                            <p>Comments: <?php echo $event['comments']; ?></p>
                        </div>
                        <div class="large-6 medium-12 small-12 columns">
                            <?php if ($event['parent_emergency_id']) {
                                echo '<p><a href="./dashboard.php?content=event&id=' . $event['parent_emergency_id'] . '" style="margin-top:2em;" class="button expanded">Main Event</a></p>';
                            } ?>
                            <div class="text-center" style="margin:2em;padding:1em;border:1px solid black">
                                <h5><?php echo $event['type']; ?></h5>
                                <h6><?php echo $event['sub_type_name']; ?></h6>
                                <img src="img/<?php echo $event['type_filename']; ?>" alt="Event Type Icon"/>
                            </div>
                            <?php
                            if ($USER->privilege->admin911 && !($USER->privilege->security || $USER->privilege->admin)) {

                            } else {
                                ?>
                                <h3 class="lead">Actions</h3>
                                <form action="/dashboard.php" method="get">
                                    <input type="hidden" name="content" value="map"/>
                                    <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                                    <a href="./dashboard.php?content=map&id=<?php echo $id; ?>"
                                       class="button expanded" <?php echo $event['parent_emergency_id'] ? 'disabled' : ''; ?>>View
                                        Map</a>
                                </form>
                                <?php
                                $hasOpenSubevents = false;
                                $openSubEvents = select_event_openSubEvents($id);
//                                    $fvmdb->query("
//                                  SELECT *
//                                  FROM emergencies e
//                                  WHERE parent_emergency_id = '" . $id . "'
//                                  AND active = 1
//                                ");
                                if ($openSubEvents->num_rows > 0) {
                                    $hasOpenSubevents = true;
                                }
                                ?>
                                <button title="<?php echo $hasOpenSubevents ? 'Close all Sub-events first' : 'Close' ?>"
                                        data-open="close-event-modal"
                                        class="button expanded" <?php if ($event['active'] == '0') echo 'disabled'; ?> <?php echo $hasOpenSubevents ? 'disabled' : '' ?>>
                                    Close Event
                                </button>
                                <button data-open="broadcast-event-modal"
                                        class="button expanded" <?php if ($event['active'] == '0') echo 'disabled'; ?> <?php echo $event['parent_emergency_id'] ? 'disabled' : ''; ?>>
                                    Broadcast
                                    Event
                                </button>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <h3 class="lead">Alert Sent To: </h3>
                            <div class="row">
                                <?php
                                $received = array();
                                $receivedGroups = select_event_reveivedGroups($event['emergency_id']);
//                                    $fvmdb->query("
//                                    SELECT rg.emma_group_id
//                                    FROM emergency_received_groups rg
//                                    WHERE rg.emergency_id = '" . $event['emergency_id'] . "'
//                                ");
                                while ($receivedGroup = $receivedGroups->fetch_assoc()) {
                                    $received[$receivedGroup['emma_group_id']] = $receivedGroup['emma_group_id'];
                                }
                                $groups = select_event_groups($USER->emma_plan_id);
//                                    $fvmdb->query("
//                                    SELECT *
//                                    FROM emma_groups
//                                    WHERE emma_plan_id = '" . $USER->emma_plan_id . "'
//                                    ORDER BY name
//                                ");
                                $continue = true;
                                while ($group = $groups->fetch_assoc()) {
                                    echo '
                                      <div class="large-4 medium-12 small-12 columns">
                                      <div class="row">
                                        <div class="small-3 column">
                                          <div class="switch tiny">
                                            <input class="switch-input" id="group-' . $group['emma_group_id'] .
                                        '" type="checkbox" ' . (in_array($group['emma_group_id'], $received) ? 'checked' : '') . ' disabled>
                                            <label class="switch-paddle" for="group-' . $group['emma_group_id'] . '">
                                              <span class="show-for-sr">' . $group['name'] . '</span>
                                            </label>
                                          </div>
                                        </div>
                                        <div class="small-9 column">
                                          <label class="text-left">' . $group['name'] . '</label>
                                        </div>
                                      </div>
                                      </div>
                                    ';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/ Details -->
            <div class="card-info warning">
                <div class="card-info-content">
                    <h3 class="lead">Current User Status</h3>
                    <div id="user-responses">
                        <div data-open="need-help-users-modal" id="total-need-help"
                             class="callout alert-callout-border alert">
                            <strong>Need Help </strong><span>0 Users</span>
                        </div>
                        <div data-open="need-info-users-modal" id="total-need-info"
                             class="callout alert-callout-border warning">
                            <strong>Information </strong><span>0 Users</span>
                        </div>
                        <div data-open="need-validation-users-modal" id="total-need-validation"
                             class="callout alert-callout-border purple">
                            <strong>Need Validation </strong><span>0 Users</span>
                        </div>
                        <div data-open="pending-users-modal" id="total-pending"
                             class="callout alert-callout-border secondary">
                            <strong>Pending </strong><span>0 Users</span>
                        </div>
                        <div data-open="not-received-users-modal" id="total-not-received"
                             class="callout alert-callout-border primary">
                            <strong>Not Received </strong><span>0 Users</span>
                        </div>
                        <div data-open="all-clear-users-modal" id="total-all-clear"
                             class="callout alert-callout-border success">
                            <strong>All Clear </strong><span>0 Users</span>
                        </div>
                    </div>
                </div>
            </div><!--/ User Status -->
        </div>
        <div class="large-6 medium-12 small-12 columns">
            <?php
            if ($USER->privilege->admin911 && !($USER->privilege->security || $USER->privilege->admin)) {
                $alertTypes = select_event_alertTypes($USER->emma_plan_id);
//                    $fvmdb->query("
//                SELECT @pos:=@pos+1 AS pos, et.*
//                FROM emma_plan_event_types pet
//                JOIN emergency_types et ON pet.emma_emergency_type_id = et.emergency_type_id
//                JOIN (SELECT @pos:=0) p
//                WHERE pet.emma_plan_id = '" . $USER->emma_plan_id . "'
//              ");
            } else {
                ?>
                <div class="card-info primary">
                    <div class="card-info-content">
                        <h3 class="lead">Sub-Events</h3>
                        <div id="sub-events" class="text-center">
                            <div class="row">
                                <?php
                                $alertTypes = select_event_alertTypes($USER->emma_plan_id);
//                                    $fvmdb->query("
//                                SELECT @pos:=@pos+1 AS pos, et.*
//                                FROM emma_plan_event_types pet
//                                JOIN emergency_types et ON pet.emma_emergency_type_id = et.emergency_type_id
//                                JOIN (SELECT @pos:=0) p
//                                WHERE pet.emma_plan_id = '" . $USER->emma_plan_id . "'
//                              ");
                                while ($alertType = $alertTypes->fetch_assoc()) {
                                    echo '
                                <div class="columns small-4">
                                  <a href="#" class="button-badge" data-open="sub-events-' . $alertType['badge'] . '-modal">
                                    <img src="img/' . $alertType['img_filename'] . '" style="width:80px;height:80px;">
                                    <span id="badge-' . $alertType['badge'] . '" class="badge alert is-hidden">0</span>
                                    <div class="card-section">
                                        <span>' . $alertType['name'] . '</span>
                                    </div>
                                  </a>
                                </div>
                                ';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div><!--/ Sub-Events -->
                <?php
            }
            ?>
            <div class="card-info secondary">
                <div class="card-info-content">
                    <h3 class="lead">Message Center</h3>
                    <div id="message-center" class="max-height">

                    </div>
                </div>
            </div><!--/ Message Center -->
            <div class="card-info secondary">
                <div id="event-map" style="width:100%; height:400px;"></div>
            </div><!--/ Map -->
            <?php if($USER->privilege->admin){ ?>
            <div class="card-info secondary">
                <div style="width:100%;">
                    <div class="card-info-content">
                        <div class="row">
                            <div class="large-12 medium-12 small-12 columns" id="event-received">
                                <h5 class="received">Received</h5>
                                <div class="max-height">
                                    <?php
                                    $type = 'event';
                                    $received = select_getreceived($id, $type);
                                    while($r = $received->fetch_assoc()){
                                        echo'
                                            <div>
                                                <div>
                                                    <span style="margin-left: 5px;"><a href="dashboard.php?content=user&id='. $r['id'] .'">' . $r['fullname'] . ' (' . $r['username'] . ')</a> - '. date('H:i:s m/d/Y', strtotime($r['date_received'])) .'</span>                         
                                                </div>
                                            </div>
                                            ';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <div id="timeline" style="position:relative;min-height:200px;">
        <h4 style="position:absolute; top:10px; left:30px; z-index: 100;">Timeline</h4></div>

    <!-- MODALS -->
    <div id="not-received-users-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4>Not Received</h4>
        <table>
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Mobile</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $notReceivedUsers = select_event_notReveivedGroups($USER->emma_plan_id);
//                $fvmdb->query("
//        SELECT u.id, CONCAT(u.firstname, ' ', u.lastname) AS user, u.username AS email, u.phone
//        FROM users u
//        WHERE u.emma_plan_id = '" . $USER->emma_plan_id . "'
//        AND u.display = 'yes'
//      ");
            while ($notReceivedUser = $notReceivedUsers->fetch_assoc()) {
                echo '
        <tr id="nr-' . $notReceivedUser['id'] . '">
            <td>
              <a href="./dashboard.php?content=user&id=' . $notReceivedUser['id'] . '">' . $notReceivedUser['user'] . '</a>
            </td>
            <td>' . $notReceivedUser['email'] . '</td>
            <td>' . $notReceivedUser['phone'] . '</td>
            <td>' . $notReceivedUser['phone'] . '</td>
        </tr>
        ';
            }
            ?>
            </tbody>
        </table>
        <button class="close-button" data-close="" aria-label="Close reveal" type="button">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div id="pending-users-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4>Pending</h4>
        <table>
            <thead>
            <tr>
                <th>Username</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Mobile</th>
                <th>Updated</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <button class="close-button" data-close="" aria-label="Close reveal" type="button">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div id="all-clear-users-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4>All Clear</h4>
        <table>
            <thead>
            <tr>
                <th>Username</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Mobile</th>
                <th>Updated</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <button class="close-button" data-close="" aria-label="Close reveal" type="button">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div id="need-help-users-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4>Need Help</h4>
        <table>
            <thead>
            <tr>
                <th>Username</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Mobile</th>
                <th>Updated</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <button class="close-button" data-close="" aria-label="Close reveal" type="button">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div id="need-info-users-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4>Need Information</h4>
        <table>
            <thead>
            <tr>
                <th>Username</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Mobile</th>
                <th>Updated</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <button class="close-button" data-close="" aria-label="Close reveal" type="button">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div id="need-validation-users-modal" class="reveal callout text-center small" data-reveal
         data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4>Need Validation</h4>
        <table>
            <thead>
            <tr>
                <th>Username</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Mobile</th>
                <th>Updated</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <button class="close-button" data-close="" aria-label="Close reveal" type="button">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div id="close-event-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4>Close Event</h4>
        <hr>
        <form id="close-event" action="#" method="post">
            <input name="user-id" type="hidden" value="<?php echo $USER->id; ?>"/>
            <input name="event-id" type="hidden" value="<?php echo $id; ?>"/>

            <div class="row columns">
                <fieldset class="fieldset text-left" style="width:100%;">
                    <legend>Alert Types <span style="color: red;">*</span></legend>
                    <input id="alert-notification" name="alert-notification" type="checkbox" checked><label
                            for="alert-notification">Notification</label>
                    <input id="alert-email" name="alert-email" type="checkbox"><label
                            for="alert-email">Email</label>
                    <input id="alert-text" name="alert-text" type="checkbox"><label
                            for="alert-text">Text</label>
                </fieldset>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <label class="text-left">Emergency Details <span style="color: red;">*</span>
                        <textarea class="expanded" name="description"
                                  style="height:5rem;width:100%;min-width:100%;"><?php echo $event['description']; ?></textarea>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <label class="text-left">Broadcast Type
                        <select id="select-close-broadcast-type">
                            <option value="" data-text="">-Select-</option>
                            <?php
                            $broadcastTypes = select_event_broadcastTypes();
//                                $fvmdb->query("
//                                select *
//                                from emma_broadcast_types
//                                where close = 1
//                                order by name
//                              ");
                            while ($broadcastType = $broadcastTypes->fetch_assoc()) {
                                echo "<option value='" . $broadcastType['emma_script_group_id'] . "'>" . $broadcastType['name'] . "</option>";
                            }
                            ?>
                        </select>
                    </label>
                    <label class="text-left select-close-script-group-container">Group Type
                        <select id="select-close-script-group">
                            <option value="" data-text="">-Select-</option>
                            <?php
                            $groups = select_groups_with_planID($USER->emma_plan_id);
//                            $fvmdb->query("
//                              SELECT *
//                              FROM emma_groups
//                              WHERE emma_plan_id = '" . $USER->emma_plan_id . "'
//                              ORDER BY name
//                            ");
                            while($group = $groups->fetch_assoc()){
                                echo "<option value='" . $group['emma_group_id'] . "'>" . $group['name'] . "</option>";
                            }
                            ?>
                        </select>
                    </label>
                    <label class="text-left select-close-script-container">Script
                        <select id="select-close-script">
                            <option value="" data-text="">-Select-</option>

                        </select>
                    </label>
                    <label class="text-left">Comments <span style="color: red;">*</span>
                        <textarea id="close-description" class="expanded" name="comments"
                                  style="height:5rem;width:100%;min-width:100%;"><?php echo $event['comments']; ?></textarea>
                    </label>
                </div>
            </div>
            <h5>Groups <span style="color: red">*</span></h5>
            <div class="row">
                <div class="large-4 medium-12 small-12 columns">
                    <div class="row">
                        <div class="small-3 column">
                            <div class="switch tiny">
                                <input class="switch-input cl-all-select" id="close-all-select" type="checkbox" >
                                <label class="switch-paddle" for="close-all-select">
                                    <span class="show-for-sr">All</span>
                                </label>
                            </div>
                        </div>
                        <div class="small-9 column">
                            <label class="text-left">All</label>
                        </div>
                    </div>
                </div>
                <?php
                $groups = select_groups_with_planID($USER->emma_plan_id);
//                $fvmdb->query("
//            SELECT *
//            FROM emma_groups
//            WHERE emma_plan_id = '" . $USER->emma_plan_id . "'
//            ORDER BY name
//        ");
                $continue = true;
                while ($group = $groups->fetch_assoc()) {
                    echo '
                      <div class="large-4 medium-12 small-12 columns">
                      <div class="row">
                        <div class="small-3 column">
                          <div class="switch tiny">
                            <input class="switch-input cl-group-select" id="cl-group-' . $group['emma_group_id'] .
                        '" type="checkbox" name="group[' . $group['emma_group_id'] . ']" value="' . $group['emma_group_id'] . '">
                            <label class="switch-paddle" for="cl-group-' . $group['emma_group_id'] . '">
                              <span class="show-for-sr">' . $group['name'] . '</span>
                            </label>
                          </div>
                        </div>
                        <div class="small-9 column">
                          <label class="text-left">' . $group['name'] . '</label>g
                        </div>
                      </div>
                      </div>
                    ';
                }
                ?>
            </div>
            <div class="row">
                <div class="large-4 columns"></div>
                <div class="large-4 columns">
                    <div class="button-group expanded" style="margin: 0 auto;">
                        <button class="button alert" data-close="" aria-label="Close reveal" type="button">Cancel</button>
                        <input type="submit" class="button" value="Submit"/>
                    </div>
                </div>
                <div class="large-4 columns"></div>
            </div>
        </form>
        <button class="close-button" data-close="" aria-label="Close reveal" type="button">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div id="broadcast-event-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4>Broadcast Event</h4>
        <hr>
        <form id="broadcast-event" action="#" method="post">
            <input name="user-id" type="hidden" value="<?php echo $USER->id; ?>"/>
            <input name="event-id" type="hidden" value="<?php echo $id; ?>"/>
            <div class="row columns">
                <fieldset class="fieldset text-left" style="width:100%;">
                    <legend>Alert Types <span style="color: red;">*</span></legend>
                    <input id="alert-notification" name="alert-notification" type="checkbox" checked><label
                            for="alert-notification">Notification</label>
                    <input id="alert-email" name="alert-email" type="checkbox" <?php if(!$USER->privilege->plan_emails){ echo 'disabled';} ?>><label
                            for="alert-email">Email</label>
                    <input id="alert-text" name="alert-text" type="checkbox" <?php if(!$USER->privilege->plan_texts){ echo 'disabled';} ?>><label
                            for="alert-text">Text</label>
                </fieldset>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <label class="text-left">Emergency Details <span style="color: red;">*</span>
                        <textarea class="expanded" name="description"
                                  style="height:5rem;width:100%;min-width:100%;"><?php echo $event['description']; ?></textarea>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <label class="text-left">Broadcast Type
                        <select id="select-broadcast-broadcast-type">
                            <option value="" data-text="">-Select-</option>
                            <?php
                            $broadcastTypes = select_event_broadcastTypes2();
//                                $fvmdb->query("
//                                select *
//                                from emma_broadcast_types
//                                  where broadcast = 1
//                                order by name
//                              ");
                            while ($broadcastType = $broadcastTypes->fetch_assoc()) {
                                echo "<option value='" . $broadcastType['emma_script_group_id'] . "'>" . $broadcastType['name'] . "</option>";
                            }
                            ?>
                        </select>
                    </label>
                    <label class="text-left select-broadcast-script-group-container">Group Type
                        <select id="select-broadcast-script-group">
                            <option value="" data-text="">-Select-</option>
                            <?php
                            $groups = select_groups_with_planID($USER->emma_plan_id);
//                            $fvmdb->query("
//                              SELECT *
//                              FROM emma_groups
//                              WHERE emma_plan_id = '" . $USER->emma_plan_id . "'
//                              ORDER BY name
//                            ");
                            while($group = $groups->fetch_assoc()){
                                echo "<option value='" . $group['emma_group_id'] . "'>" . $group['name'] . "</option>";
                            }
                            ?>
                        </select>
                    </label>
                    <label class="text-left select-broadcast-script-container">Script
                        <select id="select-broadcast-script">
                            <option value="" data-text="">-Select-</option>

                        </select>
                    </label>
                    <label class="text-left">Comments <span style="color: red;">*</span>
                        <textarea id="broadcast-description" class="expanded" name="comments"
                                  style="height:5rem;width:100%;min-width:100%;"><?php echo $event['comments']; ?></textarea>
                    </label>
                </div>
            </div>
            <h5>Groups <span style="color: red">*</span></h5>
            <div class="row">
                <div class="large-4 medium-12 small-12 columns">
                    <div class="row">
                        <div class="small-3 column">
                            <div class="switch tiny">
                                <input class="switch-input b-all-select" id="broadcast-all-select" type="checkbox" >
                                <label class="switch-paddle" for="broadcast-all-select">
                                    <span class="show-for-sr">All</span>
                                </label>
                            </div>
                        </div>
                        <div class="small-9 column">
                            <label class="text-left">All</label>
                        </div>
                    </div>
                </div>
                <?php
                $groups = select_groups_with_planID($USER->emma_plan_id);
//                $fvmdb->query("
//                    SELECT *
//                    FROM emma_groups
//                    WHERE emma_plan_id = '" . $USER->emma_plan_id . "'
//                    ORDER BY name
//                ");
                $continue = true;
                while ($group = $groups->fetch_assoc()) {
                    echo '
                      <div class="large-4 medium-12 small-12 columns">
                      <div class="row">
                        <div class="small-3 column">
                          <div class="switch tiny">
                            <input class="switch-input broadcast-group b-group-select" id="b-group-' . $group['emma_group_id'] .
                        '" type="checkbox" name="group[' . $group['emma_group_id'] . ']" value="' . $group['emma_group_id'] . '">
                            <label class="switch-paddle" for="b-group-' . $group['emma_group_id'] . '">
                              <span class="show-for-sr">' . $group['name'] . '</span>
                            </label>
                          </div>
                        </div>
                        <div class="small-9 column">
                          <label class="text-left">' . $group['name'] . '</label>
                        </div>
                      </div>
                      </div>
                    ';
                }
                ?>
            </div>
            <div class="row">
                <div class="large-4 columns"></div>
                <div class="large-4 columns">
                    <div class="button-group expanded" style="margin: 0 auto;">
                        <button class="button alert" data-close>Cancel</button>
                        <input type="submit" class="button" value="Submit"/>
                    </div>
                </div>
                <div class="large-4 columns"></div>
            </div>
        </form>
        <button class="close-button" data-close="" aria-label="Close reveal" type="button">
            <span aria-hidden="true">×</span>
        </button>
        <div id="broadcast-error"></div>
    </div>
    <?php
    $alertTypes->data_seek(0);
    while ($alertType = $alertTypes->fetch_assoc()) {
        echo '
    <div id="sub-events-' . $alertType['badge'] . '-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
         data-animation-out="fade-out">
      <h4>' . $alertType['name'] . ' Sub-Events</h4>
      <hr>
      <table>
        <thead>
        <tr>
          <th>Site</th>
          <th>Creator</th>
          <th>Date/Time</th>
          <th>Status</th>
        </tr>
        </thead>
        <tbody id="' . $alertType['badge'] . '-sub-events">
        </tbody>
      </table>
      <button class="close-button" data-close="" aria-label="Close reveal" type="button">
        <span aria-hidden="true">×</span>
      </button>
    </div>
    ';
    }
    ?>
    <!--/ MODALS -->

<?php } else {?>
    <div class="row columns">

    </div>
    <div class="callout alert">
        Could not find Event
    </div>
<?php } ?>
