<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 11/15/2017
 * Time: 11:11 AM
 */
$id = $fvmdb->real_escape_string($_GET['id']);
$securities = select_security_securities($id);
//    $fvmdb->query(sprintf("
//    SELECT s.*, st.name AS type, es.emma_site_id, es.emma_site_name, CONCAT(u.firstname, ' ', u.lastname) AS reported_by, u.id AS user_id, u.username AS user_email, u.phone AS user_phone, st.img_filename as type_filename, p.name as emma_plan_name, es.*, u.emma_pin as user_pin, s.created_pin
//    FROM emma_securities s
//    JOIN emma_security_types st ON s.emma_security_type_id = st.emma_security_type_id
//    JOIN emma_sites es ON s.emma_site_id = es.emma_site_id
//    JOIN users u ON s.created_by_id = u.id
//    JOIN emma_plans p on s.emma_plan_id = p.emma_plan_id
//    WHERE emma_security_id = %s
//", $id));
if ($security = $securities->fetch_assoc()) { ?>
  <div class="title row expanded align-middle">
    <div class="columns medium-2">
      <h2 class="text-left"><a
          href="./dashboard.php?content=securities"><i class="page-icon fa fa-shield"></i> <?php echo ucwords($content); ?></a>
      </h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
    </div>
  </div>
  <script type="text/javascript">
    let mainSecurity = <?php echo json_encode($security); ?>;
  </script>
  <div class="row expanded">
    <div class="large-6 medium-12 small-12 columns">
      <div class="card-info primary">
        <div class="card-info-content">
          <div class="row">
            <div class="large-6 medium-12 small-12 columns">
              <h3 class="lead">Details</h3>
              <p>Event Status: <?php echo $security['active'] == 1 ? 'Active'
                  : 'Closed'; ?></p>
              <p>Event Pin
                Used: <?php echo($security['created_pin'] == $security['user_pin'] ?
                  '<span style="color:green;">Valid</span>' : '<span style="color:red;">Invalid</span>') ?></p>
              <p>Original Alert
                Reported: <?php echo date('n/j/Y g:i:s a',
                  strtotime($security['created_date'])); ?></p>
              <p>Site:
                <a
                  href="./dashboard.php?content=site&id=<?php echo $security['emma_site_id']; ?>"><?php echo $security['emma_site_name']; ?></a>
              </p>
              <p>Reported By: <a
                  href="./dashboard.php?content=user&id=<?php echo $security['user_id']; ?>"><?php echo $security['reported_by']; ?></a>
              </p>
              <p>User Email: <a
                  href="mailto:<?php echo $security['user_email']; ?>"><?php echo $security['user_email']; ?></a>
              </p>
              <p>User Phone: <a
                  href="tel:<?php echo $security['user_phone']; ?>"><?php echo $security['user_phone']; ?></a>
              </p>
              <p>Emergency Details: <?php echo $security['description']; ?></p>
              <p>Comments: <?php echo $security['comments']; ?></p>
            </div>
            <div class="large-6 medium-12 small-12 columns">
              <div class="text-center"
                style="margin:2em;padding:1em;border:1px solid black">
                <h5><?php echo $security['type']; ?></h5>
                <img src="img/<?php echo $security['type_filename']; ?>"
                  alt="Event Type Icon"/>
              </div>
              <h3 class="lead">Actions</h3>
              <?php
              $callReports = select_security_callReports($security['emma_security_id']);
//                  $fvmdb->query("
//                select *
//                from emma_call_reports
//                where security_id = '" . $security['emma_security_id'] . "'
//              ");
              if($callReport = $callReports->fetch_assoc()) {
                echo "<a target='_blank' class='button expanded' href='process/create_call_report.php?id=".$callReport['id']."'>Print Call Report</a>";
              }
              ?>
              <form action="/dashboard.php" method="get">
                <input type="hidden" name="content" value="map"/>
                <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                <a href="./dashboard.php?content=map&id=<?php echo $id; ?>"
                  class="button expanded">Map</a>
              </form>
              <button data-open="close-security-modal"
                class="button expanded" <?php if ($security['active'] ==
                '0') echo 'disabled'; ?>>Close
                Security
              </button>
              <button data-open="notify-creator-modal"
                class="button expanded" <?php if ($security['active'] ==
                '0') echo 'disabled'; ?>>
                Notify Creator
              </button>
            </div>
          </div>
        </div>
      </div><!--/ Details -->
      <div class="card-info secondary">
        <div class="card-info-content">
          <h3 class="lead">Message Center</h3>
          <div id="message-center" class="max-height">

          </div>
        </div>
      </div><!--/ Message Center -->
    </div>
    <div class="large-6 medium-12 small-12 columns">
      <div class="card-info secondary">
        <div id="security-map" style="width:100%; height:400px;"></div>
      </div><!--/ Map -->
    </div>
  </div>
  <div id="timeline" style="position:relative;min-height:200px;">
    <h4 style="position:absolute; top:10px; left:30px; z-index: 100;">
      Timeline</h4></div>

  <!-- MODALS -->
  <div id="close-security-modal" class="reveal callout text-center large"
    data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4>Close Event</h4>
    <hr>
    <form id="close-security" action="#" method="post">
      <input name="user-id" type="hidden" value="<?php echo $USER->id; ?>"/>
      <input name="security-id" type="hidden" value="<?php echo $id; ?>"/>
      <div class="row">
        <div class="large-12 columns">
          <label class="text-left">Description
            <textarea class="expanded" name="description"
              style="height:5rem;width:100%;min-width:100%;"><?php echo $security['description']; ?></textarea>
          </label>
        </div>
      </div>
      <div class="row">
          <div class="large-12 columns">
              <label class="text-left">Broadcast Type
                  <select id="select-close-broadcast-type">
                      <option value="" data-text="">-Select-</option>
                      <?php
                      $broadcastTypes = select_security_broadcastTypes();
//                          $fvmdb->query("
//                                select *
//                                from emma_broadcast_types
//                                where close = 1
//                                order by name
//                              ");
                      while ($broadcastType = $broadcastTypes->fetch_assoc()) {
                          echo "<option value='" . $broadcastType['emma_script_group_id'] . "'>" . $broadcastType['name'] . "</option>";
                      }
                      ?>
                  </select>
              </label>
              <label class="text-left select-close-script-group-container">Group Type
                  <select id="select-close-script-group">
                      <option value="" data-text="">-Select-</option>
                      <?php
                      $groups = select_groups_with_planID($USER->emma_plan_id);
//                      $fvmdb->query("
//                              SELECT *
//                              FROM emma_groups
//                              WHERE emma_plan_id = '" . $USER->emma_plan_id . "'
//                              ORDER BY name
//                            ");
                      while($group = $groups->fetch_assoc()){
                          echo "<option value='" . $group['emma_group_id'] . "'>" . $group['name'] . "</option>";
                      }
                      ?>
                  </select>
              </label>
              <label class="text-left select-close-script-container">Script
                  <select id="select-close-script">
                      <option value="" data-text="">-Select-</option>

                  </select>
              </label>
              <label class="text-left">Comments <span style="color: red;">*</span>
                  <textarea id="close-description" class="expanded" name="comments"
                            style="height:5rem;width:100%;min-width:100%;"><?php echo $security['comments']; ?></textarea>
              </label>
          </div>
      </div>
      <div class="row">
        <div class="large-12 columns text-left">
          <span>Call Log Info?</span>
          <div class="switch small" style="display: inline-block; float: right">
            <input class="switch-input" id="call-log" type="checkbox"
              name="call-log">
            <label class="switch-paddle" for="call-log">
              <span class="show-for-sr">Call Log Info?</span>
              <span class="switch-active" aria-hidden="true">Yes</span>
              <span class="switch-inactive" aria-hidden="true">No</span>
            </label>
          </div>
          <div id="call-log-info"
            style="display: none; clear: both;">
            <input name="plan-id" type="hidden"
              value="<?php echo $security['emma_plan_id']; ?>"/>
            <input name="site-id" type="hidden"
              value="<?php echo $security['emma_site_id']; ?>"/>
            <h4 class="text-center">Call Report</h4>
            <hr>
            <div class="row columns">
              <label>Post</label>
              <input name="report-post" type="text"/>
            </div>
            <div class="row columns">
              <label>Notified Services</label>
              <table border="2"
                style="border-collapse: collapse; border-spacing: 0px;">
                <thead>
                <col width="20%">
                <col width="10%">
                <col width="15%">
                <tr>
                  <th>Service</th>
                  <th>Y/N</th>
                  <th>Date/Time</th>
                  <th>Name</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>Police/Fire</td>
                  <td><select name="police-yn">
                      <option value="no" selected>No</option>
                      <option value="yes">Yes</option>
                    </select></td>
                  <td><input name="police-time" type="time"></td>
                  <td><input name="police-name" type="text"></td>
                </tr>
                <tr>
                  <td>Res-Life</td>
                  <td><select name="reslife-yn">
                      <option value="no" selected>No</option>
                      <option value="yes">Yes</option>
                    </select></td>
                  <td><input name="reslife-time" type="time"></td>
                  <td><input name="reslife-name" type="text"></td>
                </tr>
                <tr>
                  <td><input name="other-notified" type="text"
                      placeholder="Other"></td>
                  <td><select name="other-yn">
                      <option value="no" selected>No</option>
                      <option value="yes">Yes</option>
                    </select></td>
                  <td><input name="other-time" type="time"></td>
                  <td><input name="other-name" type="text"></td>
                </tr>
                </tbody>
              </table>
            </div>
            <br>
            <div class="row columns">
              <label>Persons Involved/Witnesses</label>
              <table border="2" style=" border-spacing: 2px;">
                <thead>
                <col width="20%">
                <col width="25%">
                <col width="20%">
                <tr>
                  <th>Type</th>
                  <th>Name/Position</th>
                  <th>Phone Number</th>
                  <th>Organization Name & Address</th>
                </tr>
                </thead>
                <tbody id="emma-call-body">
                <tr>
                  <td><select name="person-type[]">
                      <option value="Staff">Staff</option>
                      <option value="Faculty">Faculty</option>
                      <option value="Student">Student</option>
                      <option value="Other">Other</option>
                    </select></td>
                  <td><input name="person-name[]" type="text"></td>
                  <td><input name="person-phone[]" type="text"></td>
                  <td><input name="person-org[]" type="text"></td>
                </tr>
                </tbody>
              </table>
              <div style="width:100%;">
                <a id="emma-call-add-button" class="button"
                  style="float:right;">+</a>
              </div>
            </div>
            <div class="row columns">
              <label>Officer Name</label>
              <input type="text" name="officer-name">
            </div>
            <div class="row columns">
              <label>Description of Incident</label>
              <textarea name="incident-description"></textarea>
            </div>
            <div class="row columns">
              <label>Results of Investigation & Action Taken</label>
              <textarea name="resulting-action"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="large-5 columns"></div>
          <div class="large-2 columns">
              <div class="button-group expanded" style="margin: 0 auto;">
                  <button class="button alert" data-close>Cancel</button>
                  <input type="submit" class="button" value="Submit"/>
              </div>
          </div>
        <div class="large-5 columns"></div>
      </div>
    </form>
    <button class="close-button" data-close="" aria-label="Close reveal"
      type="button">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div id="notify-creator-modal" class="reveal callout text-center small"
    data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4>Notify Creator</h4>
    <hr>
    <form id="notify-creator-form" action="#" method="post">
      <input name="user-id" type="hidden" value="<?php echo $USER->id; ?>"/>
      <input name="security-id" type="hidden" value="<?php echo $id; ?>"/>
      <input name="creator-id" type="hidden"
        value="<?php echo $security['created_by_id']; ?>"/>
      <div class="row columns">
        <fieldset class="fieldset text-left" style="width:100%;">
          <legend>Alert Types</legend>
          <input id="alert-notification" name="alert-notification"
            type="checkbox" checked><label
            for="alert-notification">Notification</label>
          <input id="alert-email" name="alert-email" type="checkbox"><label
            for="alert-email">Email</label>
        </fieldset>
      </div>
      <div class="row">
          <div class="large-12 columns">
              <label class="text-left">Broadcast Type
                  <select id="select-notify-creator-broadcast-type">
                      <option value="" data-text="">-Select-</option>
                      <?php
                      $broadcastTypes = select_security_broadcastTypesBroadcast();
//                          $fvmdb->query("
//                                select *
//                                from emma_broadcast_types
//                                where broadcast = 1
//                                order by name
//                              ");
                      while ($broadcastType = $broadcastTypes->fetch_assoc()) {
                          echo "<option value='" . $broadcastType['emma_script_group_id'] . "'>" . $broadcastType['name'] . "</option>";
                      }
                      ?>
                  </select>
              </label>
              <label class="text-left select-notify-creator-script-group-container">Group Type
                  <select id="select-notify-creator-script-group">
                      <option value="" data-text="">-Select-</option>
                      <?php
                      $groups = select_groups_with_planID($USER->emma_plan_id);
//                      $fvmdb->query("
//                              SELECT *
//                              FROM emma_groups
//                              WHERE emma_plan_id = '" . $USER->emma_plan_id . "'
//                              ORDER BY name
//                            ");
                      while($group = $groups->fetch_assoc()){
                          echo "<option value='" . $group['emma_group_id'] . "'>" . $group['name'] . "</option>";
                      }
                      ?>
                  </select>
              </label>
              <label class="text-left select-notify-creator-script-container">Script
                  <select id="select-notify-creator-script">
                      <option value="" data-text="">-Select-</option>

                  </select>
              </label>
              <label class="text-left">Comments <span style="color: red;">*</span>
                  <textarea id="notify-creator-description" class="expanded" name="comments"
                            style="height:5rem;width:100%;min-width:100%;"><?php echo $security['comments']; ?></textarea>
              </label>
          </div>
      </div>
      <div class="row">
          <div class="large-4 columns"></div>
          <div class="large-4 columns">
              <div class="button-group expanded" style="margin: 0 auto;">
                  <button class="button alert" data-close>Cancel</button>
                  <input type="submit" class="button" value="Submit"/>
              </div>
          </div>
          <div class="large-4 columns"></div>
      </div>
    </form>
    <button class="close-button" data-close="" aria-label="Close reveal"
      type="button">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <!--/ MODALS -->

<?php } else { ?>
  <div class="row columns">

  </div>
  <div class="callout alert">
    Could not find Event
  </div>
<?php } ?>
