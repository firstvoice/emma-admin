<?php

$guest_code_id = $fvmdb->real_escape_string($_GET['id']);

if (!empty($guest_code_id)) {
    $guest_codes = select_guestCode_guestCodes($guest_code_id);
//        $fvmdb->query("
//    SELECT g.*, group_concat(DISTINCT eg.name ORDER BY eg.name) AS groupName, ep.name AS planName
//    FROM emma_guest_codes AS g
//    LEFT JOIN emma_groups eg ON g.group_id = eg.emma_group_id
//    LEFT JOIN emma_plans ep ON g.emma_plan_id = ep.emma_plan_id
//    WHERE g.guest_code_id = '" . $guest_code_id . "'
//");
    $gc = $guest_codes->fetch_assoc();

} else {
    echo '<div class="title">
            <h2 class="text-left"><a href="./dashboard.php?content=guest_codes">' . ucwords(str_replace('_', ' ', $content)) . '</a></h2>
        </div>';
    echo '<div>
            <p class="text-center">Whoops no guest code found.</p>
        </div>';
}

if (!empty($gc)) { ?>
    <div class="title row expanded align-middle">
        <div class="columns medium-3">
            <h2 class="text-left"><a href="./dashboard.php?content=guest_codes"><i class="page-icon fa fa-ticket"></i> <?php echo ucwords(str_replace('_', ' ', $content)); ?></a></h2>
        </div>
        <div class="columns show-for-medium"></div>
        <div class="columns shrink">
            <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
                data-options="disableHover:true;clickOpen:true;">
                <li>
                    <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
                    <ul class="menu">
                        <li><a href="./dashboard.php?content=edit_guest_code&id=<?php echo $gc['guest_code_id']; ?>" id="edit_create">Edit
                                Guest Code</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <div class="row expanded">
        <div class="large-12 medium-12 column">
            <div class="card-info primary">
                <div class="card-info-content">
                    <h3 class="lead">Guest Code Information:</h3>
                    <div class="large-12 columns">
                        <div class="row">
                            <div class="columns large-4 medium-6">
                                <p>Guest Code: <?php if (!empty($gc['guest_code_id'])) {
                                        echo $gc['guest_code_id'];
                                    } else {
                                        echo 'None';
                                    } ?></p>
                            </div>
                            <div class="columns large-4 medium-6">
                                <p>Plan: <?php if (!empty($gc['planName'])) {
                                        echo $gc['planName'];
                                    } else {
                                        echo 'None';
                                    } ?></p>
                            </div>
                            <div class="columns large-4 medium-6">
                                <p>Group: <?php if (!empty($gc['groupName'])) {
                                        echo $gc['groupName'];
                                    } else {
                                        echo 'None';
                                    } ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="columns large-4 medium-6">
                                <p>Date Span: <?php if (!empty($gc['start_date']) || !empty($gc['end_date'])) {
                                        echo date('m/d/Y',strtotime($gc['start_date'])) . ' - ' . date('m/d/Y',strtotime($gc['end_date']));
                                    } else {
                                        echo 'None';
                                    } ?></p>
                            </div>
                            <div class="columns large-4 medium-6">
                                <p>Duration: <?php if (!empty($gc['duration_minutes'])) {
                                        if($gc['duration_minutes'] < 60){
                                            echo $gc['duration_minutes'] . ' Minutes';
                                        }else {
                                            echo $gc['duration_minutes'] / 60 . ' Hours';
                                        }
                                    } else {
                                        echo 'None';
                                    } ?></p>
                            </div>
                            <div class="columns large-4 medium-6">
                                <p>Status: <?php if ((!empty($gc['start_date']) && !empty($gc['end_date']))) {
                                        if((strtotime($gc['end_date']) < strtotime('now')) || (strtotime($gc['start_date']) > strtotime('now'))){
                                            echo '<span style="color: red">Inactive</span>';
                                        }else{
                                            echo '<span style="color: green">Active</span>';
                                        }
                                    } else {
                                        echo 'None';
                                    } ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row expanded">
        <div class="large-12 medium-12 column">
            <div class="card-info secondary">
                <div class="card-info-content">
                    <h3 class="lead">Users:</h3>
                    <div class="row large-12 columns">
                        <table>
                            <colgroup>

                            </colgroup>
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Landline</th>
                                <th>Mobile Phone</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            $people = select_guestCode_people($guest_code_id);
//                                $fvmdb->query("
//                                SELECT u.*
//                                FROM users AS u
//                                LEFT JOIN emma_multi_plan AS p ON u.id = p.user_id
//                                WHERE p.guest_code = '" . $guest_code_id . "'
//                                AND u.display = 'yes'
//                                GROUP BY u.id
//                                ORDER BY u.firstname
//                            ");
                            while ($person = $people->fetch_assoc()) {
                                echo '
                                    <tr>
                                        <td>' . $person['firstname'] . ' ' . $person['lastname'] . '</td>
                                        <td>' . $person['username'] . '</td>
                                        <td>' . $person['landline_phone'] . '</td>
                                        <td>' . $person['phone'] . '</td>
                                    </tr>
                                ';
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>


