<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/17/2019
 * Time: 11:47 AM
 */

$id = $fvmdb->real_escape_string($_GET['id']);
$events = select_call_events($id);
//    $fvmdb->query(sprintf("
//    SELECT ecl.call_id, ecl.emergency_id, ecl.call_datetime, ecl.active, ecl.latitude, ecl.longitude, et.name AS type, CONCAT(u.firstname, ' ', u.lastname) AS username, u.id AS userid, u.username AS useremail, u.phone, e.drill, et.img_filename as type_filename
//    FROM emma_911_call_log AS ecl
//    JOIN users u on ecl.userid = u.id
//    LEFT JOIN emergencies e on ecl.emergency_id = e.emergency_id
//    LEFT JOIN emergency_types et on et.emergency_type_id = e.emergency_type_id
//    WHERE ecl.call_id = '".$id."'
//", $id));

$populate_events = select_call_populateEvents($USER->emma_plan_id);
//    $fvmdb->query("
//    SELECT e.*, et.name AS type, et.img_filename
//    FROM emergencies e
//    JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
//    WHERE e.active = '1'
//    AND e.emma_plan_id = '" . $USER->emma_plan_id . "'
//    ");

echo '<script>';
echo 'let events_arr = [];';
while ($populate_event = $populate_events->fetch_assoc()) {
//If the name of the location contains valid characters and isn't empty.
    if((!empty($populate_event['latitude'])) && ($populate_event['latitude'] != 0) && ($populate_event['latitude'] != 1) && (!empty($populate_event['longitude'])) && ($populate_event['longitude'] != 0) & ($populate_event['longitude'] != 1)) {
        echo 'var evnt = {lat: ' . $populate_event['latitude'] . ', lng: ' . $populate_event['longitude'] . ', type: "' . $populate_event['type'] . '", img: "'.$populate_event['img_filename'].'"};';
        echo 'events_arr.push(evnt);';
    }

}
echo '</script>';
//Select all AED locations.
$aeds = select_call_aeds();
//    $fvmdb->query("
//SELECT a.latitude,a.longitude,a.location,a.sitecoordinator,c.name as contact_name, c.company, loc.address
//FROM aeds a
//JOIN location_contacts lc on lc.location_id = a.location_id
//JOIN contacts c on c.id = lc.contact_id
//JOIN locations as loc on loc.id = a.location_id
//where a.current = 1
//");

echo '<script>';
echo 'let positions = [];';
echo 'let assets = [];';
while($aed = $aeds->fetch_assoc())
{
    //If the name of the location contains valid characters and isn't empty.
    if(!preg_match('/[^A-Za-z0-9]/', $aed['location']) && !empty($aed['location']))
    {
        if((!empty($aed['latitude']) && !empty($aed['longitude'])) && ($aed['latitude'] != "0" && $aed['longitude'] != "0") && (!empty($aed['location'])))
        {
            echo 'var pos = {lat: '.$aed['latitude'].', lng: '.$aed['longitude'].', location: "'.$aed['location'].'", coordinator: "'.$aed['sitecoordinator'].'", contact: "'.$aed['contact_name'].'", company: "'.$aed['company'].'", address: "'.$aed['address'].'"};';
            echo 'positions.push(pos);';
        }
    }
}
echo '</script>';

//Select all ASSET locations.
$assets = select_call_assets($USER->emma_plan_id);
//    $fvmdb->query("
//SELECT eat.name, a.latitude as lat, a.longitude as lng, a.address, eai.image
//FROM emma_assets a
//LEFT JOIN emma_asset_types as eat on eat.emma_asset_type_id = a.emma_asset_type_id
//LEFT JOIN emma_asset_icons as eai on eat.emma_asset_icon_id = eai.asset_icon_id
//WHERE a.active = '1'
//AND a.emma_plan_id = '".$USER->emma_plan_id."'
//AND a.active = 1
//AND a.deleted = 0
//");

echo '<script>';
while ($asset = $assets->fetch_assoc()) {
//If the name of the location contains valid characters and isn't empty.
    if((!empty($asset['lat'])) && ($asset['lat'] != 0) && ($asset['lat'] != 1) && (!empty($asset['lng'])) && ($asset['lng'] != 0) && ($asset['lng'] != 1) && ($asset['address'] != null) && ($asset['address'] != '')) {
        echo 'var ast = {lat: ' . $asset['lat'] . ', lng: ' . $asset['lng'] . ', name: "' . $asset['name'] . '", address: "'.$asset['address'].'", image: "'.$asset['image'].'"};';
        echo 'assets.push(ast);';
    }
}
echo '</script>';




if ($event = $events->fetch_assoc()) { ?>
    <script type="text/javascript">
            let mainEvent = <?php echo json_encode($event); ?>;
    </script>
    <div class="title row expanded align-middle">
        <div class="columns medium-8">
            <h2 class="text-left"><a href="./dashboard.php?content=police_calls"><i class="page-icon fa fa-phone"></i> <?php echo ucwords($content);?></a></h2>
        </div>
        <div class="columns show-for-medium"></div>
        <div class="columns shrink">
            <!--      <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu-->
            <!--          data-options="disableHover:true;clickOpen:true;">-->
            <!--        <li>-->
            <!--          <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>-->
            <!--          <ul class="menu">-->
            <!--            <li><a href="./dashboard.php?content=map&id=-->
            <?php //echo $id; ?><!--">View Map</a></li>-->
            <!--            <li><a href="./dashboard.php?content=events">Close Event</a></li>-->
            <!--            <li><a href="*" data-open="broadcast-event-modal">Broadcast Event</a></li>-->
            <!--          </ul>-->
            <!--        </li>-->
            <!--      </ul>-->
        </div>
    </div>
    <div class="row expanded">
        <div class="large-6 medium-12 small-12 columns">
            <div class="card-info primary">
                <div class="card-info-content">
                    <div class="row">
                        <div class="large-6 medium-12 small-12 columns">
                            <p>Call
                                Status: <?php echo $event['active'] == 1 ? '<span style="color:green;">Active</span>' : '<span style="color:red;">Closed</span>'; ?></p>
                            <p>Call Date: <?php echo date('n/j/Y g:i:s a', strtotime($event['call_datetime'])); ?></p>
                            <p>Called by: <?php echo $event['username']; ?></p>
                            <p>User Email: <a href="mailto:<?php echo $event['useremail']; ?>"><?php echo $event['useremail']; ?></a></p>
                            <p>User Phone: <?php echo $event['phone']; ?></p>
                            <p>Latitude: <?php echo $event['latitude']; ?></p>
                            <p>Longitude: <?php echo $event['longitude']; ?></p>
                            <p id="closest-address">Closest Address:</p>
                        </div>
                        <div class="large-6 medium-12 small-12 columns">
                            <?php if ($event['parent_emergency_id']) {
                                echo '<p><a href="./dashboard.php?content=event&id=' . $event['parent_emergency_id'] . '" style="margin-top:2em;" class="button expanded">Main Event</a></p>';
                            } ?>
                            <div class="text-center" style="margin:2em;padding:1em;border:1px solid black">
                                <h5><?php echo $event['type']; ?></h5>
                                <h6><?php echo $event['sub_type_name']; ?></h6>
                                <img src="img/<?php echo $event['type_filename']; ?>" alt="Event Type Icon"/>
                            </div>
                            <?php
                            if ($USER->privilege->admin911 && !($USER->privilege->security || $USER->privilege->admin)) {

                            } else {
                                ?>
                                <h3 class="lead">Actions</h3>
                                <form action="/dashboard.php" method="get">
                                    <input type="hidden" name="content" value="map"/>
                                    <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                                    <a href="./dashboard.php?content=call_map&id=<?php echo $id; ?>"
                                       class="button expanded" <?php echo $event['parent_emergency_id'] ? 'disabled' : ''; ?>>View
                                        Map</a>
                                </form>
                                <?php if($event['active'] === '1')
                                    {
                                        echo '<button class="button expanded" data-open="close-call-modal">Close Event</button>';
                                    }
                                    ?>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div><!--/ Details -->
        </div>
        <div class="large-6 medium-12 small-12 columns">
            <div class="card-info secondary">
                <div id="event-map" style="width:100%; height:400px;"></div>
            </div><!--/ Map -->
        </div>
    </div>

    <!-- MODALS -->
    <div id="broadcast-event-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4>Broadcast Event</h4>
        <hr>
        <form id="broadcast-event" action="#" method="post">
            <input name="user-id" type="hidden" value="<?php echo $USER->id; ?>"/>
            <input name="event-id" type="hidden" value="<?php echo $id; ?>"/>
            <div class="row columns">
                <fieldset class="fieldset text-left" style="width:100%;">
                    <legend>Alert Types <span style="color: red;">*</span></legend>
                    <input id="alert-notification" name="alert-notification" type="checkbox" checked><label
                            for="alert-notification">Notification</label>
                    <input id="alert-email" name="alert-email" type="checkbox" disabled><label
                            for="alert-email">Email</label>
                    <input id="alert-text" name="alert-text" type="checkbox" disabled><label
                            for="alert-text">Text</label>
                </fieldset>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <label class="text-left">Emergency Details <span style="color: red;">*</span>
                        <textarea class="expanded" name="description"
                                  style="height:5rem;width:100%;min-width:100%;"><?php echo $event['description']; ?></textarea>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <label class="text-left">Broadcast Type
                        <select id="select-broadcast-broadcast-type">
                            <option value="" data-text="">-Select-</option>
                            <?php
                            $broadcastTypes = select_call_broadcastTypes();
//                                $fvmdb->query("
//                                select *
//                                from emma_broadcast_types
//                                order by name
//                              ");
                            while ($broadcastType = $broadcastTypes->fetch_assoc()) {
                                echo "<option value='" . $broadcastType['emma_script_group_id'] . "'>" . $broadcastType['name'] . "</option>";
                            }
                            ?>
                        </select>
                    </label>
                    <label class="text-left select-broadcast-script-group-container">Group Type
                        <select id="select-broadcast-script-group">
                            <option value="" data-text="">-Select-</option>
                            <?php
                            $groups = select_call_groups($USER->emma_plan_id);
//                                $fvmdb->query("
//                              SELECT *
//                              FROM emma_groups
//                              WHERE emma_plan_id = '" . $USER->emma_plan_id . "'
//                              ORDER BY name
//                            ");
                            while($group = $groups->fetch_assoc()){
                                echo "<option value='" . $group['emma_group_id'] . "'>" . $group['name'] . "</option>";
                            }
                            ?>
                        </select>
                    </label>
                    <label class="text-left select-broadcast-script-container">Script
                        <select id="select-broadcast-script">
                            <option value="" data-text="">-Select-</option>

                        </select>
                    </label>
                    <label class="text-left">Comments <span style="color: red;">*</span>
                        <textarea id="broadcast-description" class="expanded" name="comments"
                                  style="height:5rem;width:100%;min-width:100%;"><?php echo $event['comments']; ?></textarea>
                    </label>
                </div>
            </div>
            <h5>Groups</h5>
            <div class="row">
                <div class="large-4 medium-12 small-12 columns">
                    <div class="row">
                        <div class="small-3 column">
                            <div class="switch tiny">
                                <input class="switch-input all-select" id="broadcast-all-select" type="checkbox" >
                                <label class="switch-paddle" for="broadcast-all-select">
                                    <span class="show-for-sr">All</span>
                                </label>
                            </div>
                        </div>
                        <div class="small-9 column">
                            <label class="text-left">All</label>
                        </div>
                    </div>
                </div>
                <?php
                $groups = select_call_groups($USER->emma_plan_id);
//                    $fvmdb->query("
//                    SELECT *
//                    FROM emma_groups
//                    WHERE emma_plan_id = '" . $USER->emma_plan_id . "'
//                    ORDER BY name
//                ");
                $continue = true;
                while ($group = $groups->fetch_assoc()) {
                    echo '
                      <div class="large-4 medium-12 small-12 columns">
                      <div class="row">
                        <div class="small-3 column">
                          <div class="switch tiny">
                            <input class="switch-input broadcast-group group-select" id="b-group-' . $group['emma_group_id'] .
                        '" type="checkbox" name="group[' . $group['emma_group_id'] . ']" value="' . $group['emma_group_id'] . '">
                            <label class="switch-paddle" for="b-group-' . $group['emma_group_id'] . '">
                              <span class="show-for-sr">' . $group['name'] . '</span>
                            </label>
                          </div>
                        </div>
                        <div class="small-9 column">
                          <label class="text-left">' . $group['name'] . '</label>
                        </div>
                      </div>
                      </div>
                    ';
                }
                ?>
            </div>
            <div class="row">
                <div class="large-4 columns"></div>
                <div class="large-4 columns">
                    <div class="button-group expanded" style="margin: 0 auto;">
                        <button class="button alert" data-close>Cancel</button>
                        <input type="submit" class="button" value="Submit"/>
                    </div>
                </div>
                <div class="large-4 columns"></div>
            </div>
        </form>
        <button class="close-button" data-close="" aria-label="Close reveal" type="button">
            <span aria-hidden="true">×</span>
        </button>
        <div id="broadcast-error"></div>
    </div>
    <div id="close-call-modal" class="reveal callout alert text-center tiny" data-reveal
         data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4 style="color:darkred">Are You Sure?</h4>
        <div id="error-list" class="text-left"></div>
        <form id="close-call-form" type="post" action="close_call.php">
            <input name="call_id" hidden value="<?php echo $id ?>"/>
        <div class="button-group expanded">
            <a data-close="" class="button hollow alert">Cancel</a>
        <button type="submit" class="button alert" data-close>OK</button>
        </form>
        </div>
    </div>
    <!--/ MODALS -->

<?php } else { ?>
    <div class="row columns">

    </div>
    <div class="callout alert">
        Could not find Event
    </div>
<?php } ?>
