<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/18/2017
 * Time: 5:44 PM
 */

$user_id = $fvmdb->real_escape_string($_GET['id']);

$users = select_user_from_userID($user_id);
//    $fvmdb->query("
//    SELECT u.*
//    FROM users AS u
//    WHERE u.id = " . $user_id . "
//");
if (!$users->num_rows > 0) {
    echo '
    <div class="row expanded title">
        <div class="large-8 medium-12">
            <h2 class="text-left">Edit User - Error</h2>
        </div>
    </div>
    <div>
        <h4 class="text-center">Failed to load user, please try again.</h4>
    </div>
    ';
    exit();
}

$user = $users->fetch_assoc();

$plans = select_plan_from_planID($user['emma_plan_id']);
$plan = $plans->fetch_assoc();

?>

<form id="edit_user_form">
    <input type="hidden" name="id" value="<?php echo $user_id; ?>">
    <input type="hidden" name="edit-id" value="<?php echo $USER->id; ?>">
    <div class="title row expanded align-middle">
        <div class="columns medium-6">
            <h2 class="text-left"><a href="./dashboard.php?content=users"><i class="page-icon fa fa-user-circle"></i>
                    Edit User - <?php echo $user['firstname'] . ' ' . $user['lastname']; ?></a></h2>
        </div>
        <div class="columns show-for-medium"></div>
        <div class="columns shrink">
            <input type="submit" class="button" value="Save" style="margin:0;">
        </div>
    </div>

    <div class="row expanded">
        <div class="large-7 columns">
            <div class="card-info primary">
                <div class="card-info-content">
                    <h3 class="lead">Details</h3>
                    <div class="row">
                        <div class="large-6 column">
                            <label>Email/Username
                                <input type="Text" name="username" value="<?php echo $user['username']; ?>" <?php if(!$USER->privilege->admin_user)echo'disabled';?>/>
                                <input type="hidden" name="username-current" value="<?php echo $user['username']; ?>"/>
                            </label>
                        </div>
                        <div class="large-6 column">
                            <label>Main Plan
                                <input type="Text" id="main-plan-name" value="<?php echo $plan['name']; ?>" disabled/>
                            </label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="large-4 medium-12 small-12 column">
                            <label>First Name
                                <input type="Text" name="firstname" required value="<?php echo $user['firstname']; ?>"/>
                            </label>
                        </div>

                        <div class="large-4 medium-12 small-12 column">
                            <label>Middle Name
                                <input type="Text" name="middlename" value="<?php echo $user['middlename']; ?>"/>
                            </label>
                        </div>

                        <div class="large-4 medium-12 small-12 column">
                            <label>Last Name
                                <input type="Text" name="lastname" required value="<?php echo $user['lastname']; ?>"/>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-4 medium-12 column">
                            <label>Landline
                                <input type="Text" name="landline"
                                       value="<?php echo $user['landline_phone']; ?>"/>
                            </label>
                        </div>
                        <div class="large-4 medium-12 column">
                            <label>Cell
                                <input type="Text" name="phone" value="<?php echo $user['phone']; ?>"/>
                            </label>
                        </div>

                        <div class="large-4 medium-12 column">
                            <label>Title
                                <input type="Text" name="title" value="<?php echo $user['title']; ?>"/>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <label>Address
                                <input type="Text" name="address" value="<?php echo $user['address']; ?>"/>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-5 medium-12 small-12 column">
                            <label>City
                                <input type="Text" name="city" value="<?php echo $user['city']; ?>"/>
                            </label>
                        </div>

                        <div class="large-5 medium-12 small-12 column">
                            <label>State
                                <input type="Text" name="state" value="<?php echo $user['province']; ?>"/>
                            </label>
                        </div>
                        <div class="large-2 medium-12 small-12 column">
                            <label>Zip
                                <input type="Text" name="zip" value="<?php echo $user['zip']; ?>"/>
                            </label>
                        </div>
                    </div>
                    <?php if($user['id'] == $USER->id){?>
                        <div class="row">
                            <div class="large-3 medium-12 small-12 column">
                                <label>PIN
                                    <input type="Text" name="pin" required value="<?php echo $user['emma_pin'];?>"/>
                                </label>
                            </div>
                        </div>
                    <?php }?>

                    <h3 class="lead">Password <span style="color: red">*If left empty password will not be edited </span></h3>
                    <p style="color: red">Password requires a length of 8 characters including an uppercase letter, lowercase letter, special character and a number</p>
                    <div class="row">
                        <div class="large-6 medium-12 small-12 column">
                            <label>New Password
                                <input type="password" name="new-password"/>
                            </label>
                        </div>

                        <div class="large-6 medium-12 small-12 column">
                            <label>Confirm Password
                                <input type="password" name="con-password"/>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if($USER->privilege->admin && $USER->id != $user['id']) {
            echo '
        <div class="large-5 columns">
            <div class="card-info primary">
                <div class="card-info-content">
                    <h3 class="lead">Groups</h3>
                    <div class="row">';

                        $user_groups = select_editUser_usergroups($user_id);
//                            $fvmdb->query("
//                          SELECT emma_group_id
//                          FROM emma_user_groups
//                          WHERE user_id = " . $user_id . "
//                        ");
                        $user_groups_array = array();
                        while ($user_group = $user_groups->fetch_assoc()) {
                            $user_groups_array[] = $user_group['emma_group_id'];
                        }
                        $allgroups = select_editUser_allgroups($user['emma_plan_id']);
//                            $fvmdb->query("
//                          SELECT g.*
//                          FROM emma_groups AS g
//                          WHERE g.emma_plan_id = " . $user['emma_plan_id'] . "
//                        ");
                        while ($allgroup = $allgroups->fetch_assoc()) {
                            echo '
                     <div class="large-6 medium-6 small-6 columns">
                                <div class="row"> 
                                    <div class="small-4 column">
                                        <div class="switch medium">
                                            <input class="switch-input" name = "group-ids[]" value = "' .
                                $allgroup['emma_group_id'] . '" id = "group-id-' . $allgroup['emma_group_id'] .
                                '" type = "checkbox" ' .
                                (in_array($allgroup['emma_group_id'], $user_groups_array) ? 'checked' : '') . '>
                                            <label class="switch-paddle" for="group-id-' . $allgroup['emma_group_id'] . '" >
                                                <span class="show-for-sr" > ' . $allgroup['name'] . ' </span >
                                            </label >
                                        </div >
                                    </div >
                                    <div class="small-8" >
                                      <label class="text-left" > ' . $allgroup['name'] . ' </label >
                                    </div >
                                </div >
                            </div > ';
                        }
                echo'
                    </div>
                </div>
            </div>
        </div>';
        if($USER->privilege->admin_user) {
            echo '
        <div class="large-5 columns">
            <div class="card-info primary">
                <div class="card-info-content">
                    <input type="hidden" id="main-plan" name="main-plan" value="">
                    <h3 class="lead">Plans</h3>
                    <div class="row">';

            $user_plans = select_multiPlans_with_userID($user_id);
            //                        $fvmdb->query("
            //                            SELECT emma_group_id
            //                            FROM emma_user_groups
            //                            WHERE user_id = " . $user_id . "
            //                        ");
            $user_plans_array = array();
            while ($user_plan = $user_plans->fetch_assoc()) {
                $user_plans_array[] = $user_plan['plan_id'];
            }
            $allplans = select_multiPlans_with_userID($USER->id);
            //                        $fvmdb->query("
            //                            SELECT g.*
            //                            FROM emma_groups AS g
            //                            WHERE g.emma_plan_id = " . $user['emma_plan_id'] . "
            //                        ");
            while ($allplan = $allplans->fetch_assoc()) {
                echo '
                     <div class="large-6 medium-6 small-6 columns">                      
                        <div class="row"> 
                            <div class="small-4 column">
                                <div class="switch medium">
                                    <input class="switch-input plan-selects" name="plan-ids[]" data-name="'. $allplan['name'] .'" value="' .
                    $allplan['plan_id'] . '" id="plan-id-' . $allplan['plan_id'] .
                    '" type="checkbox" ' .
                    (in_array($allplan['plan_id'], $user_plans_array) ? 'checked' : '') . '>
                                    <label class="switch-paddle" for="plan-id-' . $allplan['plan_id'] . '">
                                        <span class="show-for-sr">' . $allplan['name'] . '</span>
                                    </label>
                                </div>
                            </div>
                            <div class="small-8 ">
                              <label class="text-left">' . $allplan['name'] . '</label>
                            </div>
                        </div>                           
                    </div > ';
            }
            echo '
                    </div>
                </div>
            </div>
        </div>';
        }else{
            echo'<input type="hidden" id="main-plan" name="main-plan" value="'. $user['emma_plan_id'] .'">';
        }
    }else{
        echo'<input type="hidden" id="main-plan" name="main-plan" value="'. $user['emma_plan_id'] .'">';
    }

    ?>


    </div>
</form>

<div id="success_modal" class="reveal success callout tiny text-center" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <a href="./dashboard.php?content=user&id=<?php echo $user_id;?>" data-close class="button success"
       style="margin-left:auto;margin-right:auto">Ok</a>
    <!--  <button class="close-button" data-close aria-label="Close reveal" type="button">-->
    <!--    <span aria-hidden="true">&times;</span>-->
    <!--  </button>-->
</div>
<div id="select_main_plan_modal" class="reveal callout small text-center" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Select User's Main Plan</h4>
    <div id="main-plan-container" class="row">

    </div>
    <div class="text-center small-12">
        <a class="button alert" data-close>Cancel</a>
        <a id="submit-main-plan" class="button">Ok</a>
    </div>
</div>