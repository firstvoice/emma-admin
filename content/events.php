<?php

if (!($USER->privilege->security || $USER->privilege->admin)) {
    redirect();
}

?>

<div class="title row expanded align-middle">
  <div class="columns medium-2">
    <h2 class="text-left"><a
        href="./dashboard.php?content=events"><i class="page-icon fa fa-exclamation-triangle"></i> <?php echo ucwords($content); ?></a>
    </h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
    <!--    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu-->
    <!--      data-options="disableHover:true;clickOpen:true;">-->
    <!--      <li>-->
    <!--        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>-->
    <!--        <ul class="menu">-->
    <!--          <li><a href="#" data-open="create-event-modal">Create Event</a></li>-->
    <!--        </ul>-->
    <!--      </li>-->
    <!--    </ul>-->
  </div>
</div>

<table id="events-table" class="data-table" style="width:100%">
  <thead>
  <tr>
      <th class="text-search">Event</th>
      <th class="text-search">User</th>
      <th class="text-search">Status</th>
      <th class="text-search">Created</th>
  </tr>
  <tr>
    <th>Event</th>
    <th>User</th>
    <th>Status</th>
    <th>Created</th>
  </tr>
  </thead>
  <tbody>
  </tbody>
  <tfoot>
  </tfoot>
</table>
