<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 11/15/2017
 * Time: 11:11 AM
 */
$id = $fvmdb->real_escape_string($_GET['id']);

//    $fvmdb->query(sprintf("
//    SELECT s.*, CONCAT(u.firstname, ' ', u.lastname) as created_by_name, u.username as username, u.phone
//    FROM emma_sos s
//    JOIN users u on s.created_by_id = u.id
//    WHERE s.emma_sos_id = %s
//", $id));





//verify if allowed
//get plan event is in
//check if user is in plan
//check if user is admin rights for plan
$userID = $USER->id;

$lockdowns = select_lockdown_details($id);
$lockdown = $lockdowns->fetch_assoc()
    ?>
    <div class="title row expanded align-middle">
        <div class="columns medium-4">
            <h2 class="text-left"><a href="./dashboard.php?content=<?php echo($lockdown['drill'] ? 'lockdown_drills' : 'lockdowns');?>"><i class="page-icon fa fa-lock"></i> LOCKDOWN! Details</a></h2>
        </div>
        <div class="columns show-for-medium"></div>
        <div class="columns shrink">
        </div>
    </div>
    <script type="text/javascript">
        let mainSos = <?php echo json_encode($lockdown); ?>;
    </script>
    <div class="row expanded">
        <div class="large-6 medium-12 small-12 columns">
            <div class="card-info primary">
                <div class="card-info-content">
                    <div class="row">
                        <div class="large-12 medium-12 small-12 columns">
                            <h3 class="lead">Details</h3>
                            <p>Plan: <?php echo $lockdown['name']; ?></p>
                            <p>Drill: <?php echo ($lockdown['drill'] ? '<span style="color: green">Yes</span>' : '<span style="color: red">No</span>'); ?></p>
                            <p>Original Alert Date <?php echo date('m/d/Y H:i:s',strtotime($lockdown['created_date'])); ?></p>
                            <p>Created Coordinates: <?php echo $lockdown['created_lat'].', '.$lockdown['created_lng']; ?></p>
                            <p>User: <?php echo $lockdown['created_by_name']; ?></p>
                            <p>Username: <?php echo $lockdown['username']; ?></p>
                            <p>User Phone: <?php echo $lockdown['phone']; ?></p>
                        </div>
                    </div>
                </div>
            </div><!--/ Details -->
        </div>
        <div class="large-6 medium-12 small-12 columns">
            <div class="card-info secondary">
                <div id="lockdown-map" style="width:100%; height:400px;"></div>
            </div><!--/ Map -->
        </div>
    </div>

    <!-- MODALS -->
    <div id="close-sos-modal" class="reveal callout text-center"
         data-reveal data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4>Close SOS?</h4>
        <hr>
        <form id="close-sos" action="#" method="post">
            <input name="user-id" type="hidden" value="<?php echo $USER->id; ?>"/>
            <input name="sos-id" type="hidden" value="<?php echo $id; ?>"/>
            <div class="row">
                <div class="large-12 columns">
                    <label>Comments
                        <textarea name="comments" class="expanded" style="height:5rem;width:100%;min-width:100%;"></textarea>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns text-left">
                    <span>Call Log Info?</span>
                    <div class="switch small" style="display: inline-block; float: right">
                        <input class="switch-input" id="call-log" type="checkbox"
                               name="call-log">
                        <label class="switch-paddle" for="call-log">
                            <span class="show-for-sr">Call Log Info?</span>
                            <span class="switch-active" aria-hidden="true">Yes</span>
                            <span class="switch-inactive" aria-hidden="true">No</span>
                        </label>
                    </div>
                    <div id="call-log-info"
                         style="display: none; clear: both;">
                        <input name="sos-id" type="hidden"
                               value="<?php echo $lockdown['lockdown_id']; ?>"/>
                        <h4 class="text-center">Call Report</h4>
                        <hr>
                        <div class="row columns">
                            <label>Officer Name</label>
                            <input type="text" name="officer-name">
                        </div>
                        <div class="row columns">
                            <label>Description of Incident</label>
                            <textarea name="incident-description"></textarea>
                        </div>
                        <div class="row columns">
                            <label>Results of Investigation & Action Taken</label>
                            <textarea name="resulting-action"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="large-3 columns"></div>
                <div class="large-6 large-centered columns">
                    <div class="expanded button-group">
                        <a data-close class="button">Cancel</a>
                        <input type="submit" class="button" value="Submit"/>
                    </div>
                </div>
                <div class="large-3 columns"></div>
            </div>
        </form>
        <button class="close-button" data-close="" aria-label="Close reveal"
                type="button">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div id="success_modal" class="reveal callout success text-center tiny"
         data-reveal
         data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4>Success</h4>
        <a data-close="" href="./dashboard.php?content=lockdowns" class="button success">OK</a>
    </div>
    <!--/ MODALS -->

