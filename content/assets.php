<?php

if (!($USER->privilege->admin)) {
    redirect();
}


?>



<div class="title row expanded align-middle">
  <div class="columns medium-2">
    <h2 class="text-left"></i><a href="./dashboard.php?content=assets"><i class="page-icon fa fa-cube"></i> <?php echo ucwords($content); ?></a></h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
      data-options="disableHover:true;clickOpen:true;">
      <li>
        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <ul class="menu">
            <li><a href="#" data-open="create-asset-modal">Create Asset</a></li>
            <?php if($USER->privilege->admin){echo '<li><a href="./dashboard.php?content=edit_asset_info">Asset Types</a></li>';}?>
            <li><a href="./dashboard.php?content=assets_map">Map</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>

<table id="assets-table" class="data-table" style="width:100%">
  <thead>
  <tr>
      <th class="text-search">Asset</th>
      <th class="text-search">User</th>
      <th class="text-search">Date</th>
      <th class="text-search">Latitude</th>
      <th class="text-search">Longitude</th>
      <th class="text-search">Status</th>
  </tr>
  <tr>
    <th>Asset</th>
    <th>User</th>
    <th>Date</th>
    <th>Latitude</th>
    <th>Longitude</th>
    <th>Status</th>
  </tr>
  </thead>
  <tbody>
  </tbody>
  <tfoot>

  </tfoot>
</table>

<div id="create-asset-modal" class="reveal callout text-center small"
  data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>New Asset</h4>
  <hr>
  <form id="create-asset-form" class="create-asset-form text-left" action="#"
    method="post">
    <input type="hidden" name="user-id" value="<?php echo $USER->id; ?>"/>
    <input type="hidden" name="plan-id"
      value="<?php echo $USER->emma_plan_id; ?>"/>
    <div class="row">
      <div class="large-12 columns">
        <label>Asset
          <select required name="type-id" id="create_asset_form_type_id">
              <option value="" disabled selected>-Select-</option>
            <?php
            $types = select_asset_types($USER->emma_plan_id);
//                $fvmdb->query("
//              SELECT *
//              FROM emma_asset_types
//              where emma_plan_id = '" . $USER->emma_plan_id . "'
//              AND active = 1
//              ORDER BY name
//            ");
            while ($type = $types->fetch_assoc()) {
              echo '<option value="' . $type['emma_asset_type_id'] . '">' .
                $type['name'] . '</option>';
            }
            ?>
          </select>
        </label>
          <div class="row">
          <div class="large-6 columns">
        <label>Latitude
          <input required type="number" id="entered_lat" name="latitude" step="any"/>
        </label>
          </div>
          <div class="large-6 columns">
        <label>Longitude
          <input required type="number" id="entered_lng" name="longitude" step="any"/>
        </label>
          </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
                <label>Address
                <input required type="text" name="address"/>
                  </label>
            </div>
          </div>
      </div>
        <div id="insert-area">

        </div>
    </div>
      <div class="large-12 columns">
          <div id="new-map" style="width: 100%;height: 400px;"></div>
      </div>
      <br>
    <div class="row">
        <br>
        <div class="large-4 columns button-group expanded" style="margin: 0 auto;">
        <button data-close aria-label="close reveal" type="button" class="button alert">Cancel</button>
        <input type="submit" class="button" value="Submit"/>
      </div>
    </div>
  </form>
  <button class="close-button" data-close="" aria-label="Close reveal"
    type="button">
    <span aria-hidden="true">×</span>
  </button>
</div>








<div id="success-create-modal" class="reveal callout success text-center tiny"
  data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Success</h4>
  <form action="./dashboard.php?content=assets" method="get">
    <input type="hidden" name="content" value="assets"/>
    <input class="button success" type="submit" value="OK"/>
  </form>
</div>
<div id="edit-asset-modal" class="reveal callout text-center small"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Edit Asset</h4>
    <hr>
    <form id="edit-asset-form" class="edit-asset-form text-left" action="#"
          method="post">
        <div class="row">
            <div class="large-12 columns">
                <label>Asset
                    <select name="type-id" id="form_id">
                        <?php
                        $types = select_assets_types($USER->emma_plan_id);
//                            $fvmdb->query("
//                        SELECT *
//                        FROM emma_asset_types
//                        where emma_plan_id = '" . $USER->emma_plan_id . "'
//                        ORDER BY name
//                        ");
                        while ($type = $types->fetch_assoc()) {
                            echo '<option value="' . $type['emma_asset_type_id'] . '">' .
                                $type['name'] . '</option>';
                        }
                        ?>
                    </select>
                </label>
                <label>Latitude
                    <input required type="number" id="form_latitude" name="latitude" step="any" />
                </label>
                <label>Longitude
                    <input required type="number" name="longitude" id="form_longitude" step="any"/>
                </label>
                <label>Created Date
                    <input required type="datetime" id="form_createdDate" name="created_date"/>
                </label>
                <label>Status
                    <select name="status" id="form_status"/>
                    <option style="color: green;" value="Current">Current</option>
                    <option style="color: red;" value="Inactive">Inactive</option>
                </label>


                <input type="hidden" name="id" id="form_raw_id"/>
                <input type="hidden" name="type_raw_name" id="form_type_raw_name"/>

            </div>
        </div>
        <div class="row">
            <div class="large-3 columns"></div>
            <div class="large-6 large-centered columns">
                <input type="submit" class="button expanded" value="Submit Edit"/>
            </div>
            <div class="large-3 columns"></div>
        </div>
    </form>
</div>