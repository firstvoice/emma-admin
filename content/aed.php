<?php
global $fvmdb;
global $USER;
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 11/15/2017
 * Time: 11:11 AM
 */
$id = $fvmdb->real_escape_string($_GET['id']);

?>



<?php

if ($aed = select_aed($id)->fetch_assoc()) { ?>
  <input readonly hidden id="aed-id" value="<?php echo $id?>" class="aed_id"/>
  <div class="title row expanded align-middle">
    <div class="columns medium-8">
      <h2 class="text-left"><a href="./dashboard.php?content=aeds"><i class="page-icon fa fa-cube"></i> AED</a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
      <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
        data-options="disableHover:true;clickOpen:true;">
        <li>
          <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
          <ul class="menu">
            <?php
            echo '<li><a data-status="'.$aed['active'].'" data-id="'.$aed['emma_aed_id'].'" data-type="'.$aed['name'].'" data-createdDate="'.$aed['created_date'].'" data-latitude="'.$aed['latitude'].'" data-longitude="'.$aed['longitude'].'" data-username="'.$aed['username'].'" onclick="edit_aed(this);">Edit AED</a></li>';
            echo '<li><a data-open="deactivate-aed-modal">Deactivate AED</a></li>';
            echo '<li><a data-open="delete-aed-modal">Delete AED</a></li>'
            ?>
          </ul>
        </li>
      </ul>
    </div>
  </div>
  <div class="row expanded">
    <div class="large-4 medium-6 small-12 columns">
      <div class="card-info primary">
        <div class="card-info-content">
          <div class="row">
            <div class="large-12 medium-12 small-12 columns">
              <h3 class="lead">Details</h3>
              <hr>
              <p><b>Organization:</b> <?php echo $aed['organization']; ?></p>
              <p><b>Location:</b> <?php echo $aed['org_location']; ?></p>
              <p><b>Brand:</b> <?php echo $aed['brand']; ?></p>
              <p><b>Model:</b> <?php echo $aed['model']; ?></p>
              <p><b>Serial:</b> <?php echo $aed['serialnumber']; ?></p>
              <p><b>Placement:</b> <?php echo $aed['location']; ?></p>
              <p><b>Last Check:</b> <?php echo $aed['lastcheck']; ?></p>
              <p><b>Address:</b> <?php echo $aed['address']; ?></p>
              <p><b>Latitude:</b> <?php echo $aed['latitude'];?>&nbsp;&nbsp;&nbsp;&nbsp; <b>Longitude:</b> <?php echo $aed['longitude'];?></p>
            </div>
          </div>
        </div>
      </div><!--/ Details -->
    </div>
    <div class="large-4 medium-6 small-12 columns">
      <div class="card-info primary">
        <div class="card-info-content">
          <div class="row">
            <div class="large-12 medium-12 small-12 columns">
              <h3 class="lead">Accessories</h3>
              <hr>
              <div class="row">
                <div class="large-6 columns">
                  <?php
                  if (isset($aed['aed_pad_id'])) {
                    echo '<h5>PAD</h5> <p>Type: '.$aed['pad_type'].'<br>Lot: '.$aed['pad_lot'].'<br>Expiration: '.$aed['pad_expiration'].'</p>';
                  }
                  ?>
                </div>
                <div class="large-6 columns">
                  <?php
                  if (isset($aed['spare_aed_pad_id'])) {
                    echo '<h5>Spare PAD</h5> <p>Type: '.$aed['spare_pad_type'].'<br>Lot: '.$aed['spare_pad_lot'].'<br>Expiration: '.$aed['spare_pad_expiration'].'</p>';
                  }
                  ?>
                </div>
              </div>
              <div class="row">
                <div class="large-6 columns">
                  <?php
                  if (isset($aed['aed_ped_pad_id'])) {
                    echo '<h5>Pediatric PAD</h5> <p>Type: '.$aed['ped_pad_type'].'<br>Lot: '.$aed['ped_pad_lot'].'<br>Expiration: '.$aed['ped_pad_expiration'].'</p>';
                  }
                  ?>
                </div>
                <div class="large-6 columns">
                  <?php
                  if (isset($aed['spare_aed_ped_pad_id'])) {
                    echo '<h5>Spare Pediatric PAD</h5> <p>Type: '.$aed['spare_ped_pad_type'].'<br>Lot: '.$aed['spare_ped_pad_lot'].'<br>Expiration: '.$aed['spare_ped_pad_expiration'].'</p>';
                  }
                  ?>
                </div>
              </div>
              <div class="row">
                <div class="large-6 columns">
                  <?php
                  if (isset($aed['aed_pak_id'])) {
                    echo '<h5>PAK</h5> <p>Type: '.$aed['pak_type'].'<br>Lot: '.$aed['pak_lot'].'<br>Expiration: '.$aed['pak_expiration'].'</p>';
                  }
                  ?>
                </div>
                <div class="large-6 columns">
                  <?php
                  if (isset($aed['spare_aed_pak_id'])) {
                    echo '<h5>Spare PAK</h5> <p>Type: '.$aed['spare_pak_type'].'<br>Lot: '.$aed['spare_pak_lot'].'<br>Expiration: '.$aed['spare_pak_expiration'].'</p>';
                  }
                  ?>
                </div>
              </div>
              <div class="row">
                <div class="large-6 columns">
                  <?php
                  if (isset($aed['aed_ped_pak_id'])) {
                    echo '<h5>Pediatric PAK</h5> <p>Type: '.$aed['ped_pak_type'].'<br>Lot: '.$aed['ped_pak_lot'].'<br>Expiration: '.$aed['ped_pak_expiration'].'</p>';
                  }
                  ?>
                </div>
                <div class="large-6 columns">
                  <?php
                  if (isset($aed['spare_aed_ped_pak_id'])) {
                    echo '<h5>Spare Pediatric PAK</h5> <p>Type: '.$aed['spare_ped_pak_type'].'<br>Lot: '.$aed['spare_ped_pak_lot'].'<br>Expiration: '.$aed['spare_ped_pak_expiration'].'</p>';
                  }
                  ?>
                </div>
              </div>
              <div class="row">
                <div class="large-6 columns">
                  <?php
                  if (isset($aed['aed_battery_id'])) {
                    echo '<h5>Battery</h5> <p>Type: '.$aed['bat_type'].'<br>Lot: '.$aed['bat_lot'].'<br>Expiration: '.$aed['bat_expiration'].'</p>';
                  }
                  ?>
                </div>
                <div class="large-6 columns">
                  <?php
                  if (isset($aed['aed_battery_id'])) {
                    echo '<h5>Spare Battery</h5> <p>Type: '.$aed['spare_bat_type'].'<br>Lot: '.$aed['spare_bat_lot'].'<br>Expiration: '.$aed['spare_bat_expiration'].'</p>';
                  }
                  ?>
                </div>
              </div>
              <div class="row">
                <div class="large-6 columns">
                  <?php
                  if (isset($aed['aed_accessory_id'])) {
                    echo '<h5>Accessory</h5> <p>Type: '.$aed['acc_type'].'<br>Lot: '.$aed['acc_lot'].'<br>Expiration: '.$aed['acc_expiration'].'</p>';
                  }
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><!--/ Details -->
    </div>
    <div class="large-4 medium-6 small-12 columns">
      <div class="card-info primary">
        <div class="card-info-content">
          <div class="row">
            <div class="large-12 medium-12 small-12 columns">
              <h3 class="lead">Notes</h3>
              <hr>
              <p><?php echo $aed['notes']; ?></p>
            </div>
          </div>
        </div>
      </div><!--/ Details -->
    </div>
  </div>
  <div class="row expanded">
    <div class="large-12 medium-12 small-12 columns">
      <div class="card-info primary">
        <div class="card-info-content">
          <div id="aed-map" style="width:100%; height:400px;"></div>
        </div>
      </div><!--/ Details -->
    </div>
  </div>
  <script type="text/javascript">
    var aed = <?php echo json_encode($aed); ?>;
  </script>


<?php } else { ?>
  <div class="row columns">

  </div>
  <div class="callout alert">
    Could not find AED
  </div>
<?php } ?>

<div id="edit-aed-modal" class="reveal callout text-center small"
  data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Edit AED</h4>
  <hr>
  <form id="edit-aed-form" class="edit-aed-form text-left" action="#"
    method="post">
    <input type="hidden" name="user-id" value="<?php echo $USER->id; ?>"/>
    <input type="hidden" name="id" value="<?php echo $id?>"/>
    <div class="row">
      <div class="large-12 columns">
        <div class="row">
          <div class="large-6 columns">
            <label>Brand
              <h4><?php echo $aed['brand']; ?></h4>
            </label>
          </div>
          <div class="large-6 columns">
            <label>Model
              <h4><?php echo $aed['model']; ?></h4>
            </label>
          </div>
        </div>
        <div class="row">
          <div class="large-6 columns">
            <label>Serial
              <input required type="text" name="serial" value="<?php echo $aed['serialnumber'] ?>"/>
            </label>
          </div>
          <div class="large-6 columns">
            <label>Placement
              <input required type="text" name="placement" value="<?php echo $aed['location'] ?>"/>
            </label>
          </div>
        </div>
        <div class="row">
          <div class="large-6 columns">
            <label>Latitude
              <input required type="number" id="entered_lat" name="latitude" step="any" value="<?php echo $aed['latitude'] ?>"/>
            </label>
          </div>
          <div class="large-6 columns">
            <label>Longitude
              <input required type="number" id="entered_lng" name="longitude" step="any" value="<?php echo $aed['longitude'] ?>"/>
            </label>
          </div>
        </div>
        <div class="row">
          <div class="large-12 columns">
            <label>Address
              <input required type="text" name="address" value="<?php echo $aed['address'] ?>"/>
            </label>
          </div>
        </div>
        <div class="row">
          <div class="large-12 columns">
            <label>Notes
              <textarea name="notes"><?php echo $aed['notes'] ?></textarea>
            </label>
          </div>
        </div>
        <label>Status
          <select <?php if($aed['current'] == 1){echo 'style="color: green;"';}else{echo 'style="color: red;"';} ?> id="status_bar" name="status">
            <option <?php if($aed['current'] == 1){echo 'selected';} ?> value="1" style="color: green;">Active</option>
            <option <?php if($aed['current'] == 0){echo 'selected';} ?> value="0" style="color: red;">Inactive</option>
          </select>
        </label>
      </div>
    </div>
    <hr>
    <?php
    $pads = $fvmdb->query("
      SELECT pad_t.aed_pad_type_id, pad_t.type AS pad_type, pad_t.pediatric
      FROM aed_pad_types_on_models pad_tom
      JOIN aed_pad_types pad_t ON pad_t.aed_pad_type_id = pad_tom.aed_pad_type_id
      WHERE pad_tom.aed_model_id = ".$aed['aed_model_id']."
    ");
    $paks = $fvmdb->query("
      SELECT pak_t.aed_pak_type_id, pak_t.type AS pak_type, pak_t.pediatric
      FROM aed_pak_types_on_models pak_tom
      JOIN aed_pak_types pak_t ON pak_t.aed_pak_type_id = pak_tom.aed_pak_type_id
      WHERE pak_tom.aed_model_id = ".$aed['aed_model_id']."
    ");
    $batteries = $fvmdb->query("
      SELECT bat_t.aed_battery_type_id, bat_t.type AS battery_type
      FROM aed_battery_types_on_models bat_tom
      JOIN aed_battery_types bat_t ON bat_t.aed_battery_type_id = bat_tom.aed_battery_type_id
      WHERE bat_tom.aed_model_id = ".$aed['aed_model_id']."
    ");
    $accessories = $fvmdb->query("
      SELECT acc_t.aed_accessory_type_id, acc_t.type AS accessory_type
      FROM aed_accessory_types_on_models acc_tom
      JOIN aed_accessory_types acc_t ON acc_t.aed_accessory_type_id = acc_tom.aed_accessory_type_id
      WHERE acc_tom.aed_model_id = ".$aed['aed_model_id']."
    ");
    ?>
    <div class="row">
      <div class="large-12 columns">
        <div id="pad-row" class="row" <?php echo mysqli_num_rows($pads) == 0 ? 'style="display: none;"' : ''?>>
          <div class="large-4 columns">
            <label>Pad
              <select id="pad-select" name="pad">
                <option value="" selected="selected" hidden="hidden">-- Select Pad --</option>
                <?php
                while($pad = $pads->fetch_assoc()) {
                  if($pad['pediatric'] == '0') {
                    echo ($aed['aed_pad_type_id'] == $pad['aed_pad_type_id'])
                      ?
                      '<option value="' . $pad['aed_pad_type_id'] .
                      '" selected>' . $pad['pad_type'] . '</option>'
                      :
                      '<option value="' . $pad['aed_pad_type_id'] . '">' .
                      $pad['pad_type'] . '</option>';
                  }
                }
                ?>
              </select>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Lot
              <input type="text" name="pad-lot" value="<?php echo $aed['pad_lot']; ?>"/>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Expiration
              <input type="date" name="pad-expiration" value="<?php echo $aed['pad_expiration']; ?>"/>
            </label>
          </div>
          <div class="large-2 columns">
            <label>Attached
              <input type="radio" name="pad-attached" value="main" checked="checked"/>
            </label>
          </div>
        </div>
        <div id="ped-pad-row" class="row" <?php echo mysqli_num_rows($pads) == 0 ? 'style="display: none;"' : ''?>>
          <div class="large-4 columns">
            <label>Pediatric Pad
              <select id="ped-pad-select" name="ped-pad">
                <option value="" selected="selected" hidden="hidden">-- Select Pad --</option>
                <?php
                mysqli_data_seek($pads, 0);
                while($pad = $pads->fetch_assoc()) {
                  if($pad['pediatric'] == '1') {
                    echo ($aed['aed_ped_pad_type_id'] ==
                      $pad['aed_pad_type_id'])
                      ?
                      '<option value="' . $pad['aed_pad_type_id'] .
                      '" selected>' . $pad['pad_type'] . '</option>'
                      :
                      '<option value="' . $pad['aed_pad_type_id'] . '">' .
                      $pad['pad_type'] . '</option>';
                  }
                }
                ?>
              </select>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Lot
              <input type="text" name="ped-pad-lot" value="<?php echo $aed['ped_pad_lot']; ?>"/>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Expiration
              <input type="date" name="ped-pad-expiration" value="<?php echo $aed['ped_pad_expiration']; ?>"/>
            </label>
          </div>
          <div class="large-2 columns">
            <label>Attached
              <input type="radio" name="pad-attached" value="main" checked="checked"/>
            </label>
          </div>
        </div>
        <div id="spare-pad-row" class="row" <?php echo mysqli_num_rows($pads) == 0 ? 'style="display: none;"' : ''?>>
          <div class="large-4 columns">
            <label>Spare Pad
              <select id="spare-pad-select" name="spare-pad">
                <option value="" selected="selected" hidden="hidden">-- Select Pad --</option>
                <?php
                mysqli_data_seek($pads, 0);
                while($pad = $pads->fetch_assoc()) {
                  if($pad['pediatric'] == '0') {
                    echo ($aed['spare_aed_pad_type_id'] ==
                      $pad['aed_pad_type_id'])
                      ?
                      '<option value="' . $pad['aed_pad_type_id'] .
                      '" selected>' . $pad['pad_type'] . '</option>'
                      :
                      '<option value="' . $pad['aed_pad_type_id'] . '">' .
                      $pad['pad_type'] . '</option>';
                  }
                }
                ?>
              </select>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Lot
              <input type="text" name="spare-pad-lot" value="<?php echo $aed['spare_pad_lot']; ?>"/>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Expiration
              <input type="date" name="spare-pad-expiration" value="<?php echo $aed['spare_pad_expiration']; ?>"/>
            </label>
          </div>
          <div class="large-2 columns">
            <label>Attached
              <input type="radio" name="pad-attached" value="spare" />
            </label>
          </div>
        </div>
        <div id="spare-ped-pad-row" class="row" <?php echo mysqli_num_rows($pads) == 0 ? 'style="display: none;"' : ''?>>
          <div class="large-4 columns">
            <label>Spare Pediatric Pad
              <select id="spare-ped-pad-select" name="spare-ped-pad">
                <option value="" selected="selected" hidden="hidden">-- Select Pad --</option>
                <?php
                mysqli_data_seek($pads, 0);
                while($pad = $pads->fetch_assoc()) {
                  if($pad['pediatric'] == '1') {
                    echo ($aed['spare_aed_ped_pad_type_id'] ==
                      $pad['aed_pad_type_id'])
                      ?
                      '<option value="' . $pad['aed_pad_type_id'] .
                      '" selected>' . $pad['pad_type'] . '</option>'
                      :
                      '<option value="' . $pad['aed_pad_type_id'] . '">' .
                      $pad['pad_type'] . '</option>';
                  }
                }
                ?>
              </select>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Lot
              <input type="text" name="spare-ped-pad-lot" value="<?php echo $aed['spare_ped_pad_lot']; ?>"/>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Expiration
              <input type="date" name="spare-ped-pad-expiration" value="<?php echo $aed['spare_ped_pad_expiration']; ?>"/>
            </label>
          </div>
          <div class="large-2 columns">
            <label>Attached
              <input type="radio" name="pad-attached" value="spare" />
            </label>
          </div>
        </div>
        <div id="pak-row" class="row" <?php echo mysqli_num_rows($paks) == 0 ? 'style="display: none;"' : ''?>>
          <div class="large-4 columns">
            <label>Pak
              <select id="pak-select" name="pak">
                <option value="" selected="selected" hidden="hidden">-- Select Pak --</option>
                <?php
                while($pak = $paks->fetch_assoc()) {
                  if($pak['pediatric'] == '0') {
                    echo ($aed['aed_pak_type_id'] == $pak['aed_pak_type_id'])
                      ?
                      '<option value="' . $pak['aed_pak_type_id'] .
                      '" selected>' . $pak['pak_type'] . '</option>'
                      :
                      '<option value="' . $pak['aed_pak_type_id'] . '">' .
                      $pak['pak_type'] . '</option>';
                  }
                }
                ?>
              </select>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Lot
              <input type="text" name="pak-lot" value="<?php echo $aed['pak_lot']; ?>"/>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Expiration
              <input type="date" name="pak-expiration" value="<?php echo $aed['pak_expiration']; ?>"/>
            </label>
          </div>
          <div class="large-2 columns">
            <label>Attached
              <input type="radio" name="pak-attached" value="main" checked="checked"/>
            </label>
          </div>
        </div>
        <div id="ped-pak-row" class="row" <?php echo mysqli_num_rows($paks) == 0 ? 'style="display: none;"' : ''?>>
          <div class="large-4 columns">
            <label>Pediatric Pak
              <select id="ped-pak-select" name="ped-pak">
                <option value="" selected="selected" hidden="hidden">-- Select Pak --</option>
                <?php
                while($pak = $paks->fetch_assoc()) {
                  if($pak['pediatric'] == '1') {
                    echo ($aed['aed_ped_pak_type_id'] ==
                      $pak['aed_pak_type_id'])
                      ?
                      '<option value="' . $pak['aed_pak_type_id'] .
                      '" selected>' . $pak['pak_type'] . '</option>'
                      :
                      '<option value="' . $pak['aed_pak_type_id'] . '">' .
                      $pak['pak_type'] . '</option>';
                  }
                }
                ?>
              </select>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Lot
              <input type="text" name="ped-pak-lot" value="<?php echo $aed['ped_pak_lot']; ?>"/>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Expiration
              <input type="date" name="ped-pak-expiration" value="<?php echo $aed['ped_pak_expiration']; ?>"/>
            </label>
          </div>
          <div class="large-2 columns">
            <label>Attached
              <input type="radio" name="pak-attached" value="main" checked="checked"/>
            </label>
          </div>
        </div>
        <div id="spare-pak-row" class="row" <?php echo mysqli_num_rows($paks) == 0 ? 'style="display: none;"' : ''?>>
          <div class="large-4 columns">
            <label>Spare Pak
              <select id="spare-pak-select" name="spare-pak">
                <option value="" selected="selected" hidden="hidden">-- Select Pak --</option>
                <?php
                mysqli_data_seek($paks, 0);
                while($pak = $paks->fetch_assoc()) {
                  if($pak['pediatric'] == '0') {
                    echo ($aed['spare_aed_pak_type_id'] ==
                      $pak['aed_pak_type_id'])
                      ?
                      '<option value="' . $pak['aed_pak_type_id'] .
                      '" selected>' . $pak['pak_type'] . '</option>'
                      :
                      '<option value="' . $pak['aed_pak_type_id'] . '">' .
                      $pak['pak_type'] . '</option>';
                  }
                }
                ?>
              </select>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Lot
              <input type="text" name="spare-pak-lot" value="<?php echo $aed['spare_pak_lot']; ?>"/>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Expiration
              <input type="date" name="spare-pak-expiration" value="<?php echo $aed['spare_pak_expiration']; ?>"/>
            </label>
          </div>
          <div class="large-2 columns">
            <label>Attached
              <input type="radio" name="pak-attached" value="spare" />
            </label>
          </div>
        </div>
        <div id="spare-ped-pak-row" class="row" <?php echo mysqli_num_rows($paks) == 0 ? 'style="display: none;"' : ''?>>
          <div class="large-4 columns">
            <label>Spare Pediatric Pak
              <select id="spare-ped-pak-select" name="spare-ped-pak">
                <option value="" selected="selected" hidden="hidden">-- Select Pak --</option>
                <?php
                mysqli_data_seek($paks, 0);
                while($pak = $paks->fetch_assoc()) {
                  if($pak['pediatric'] == '1') {
                    echo ($aed['spare_aed_ped_pak_type_id'] ==
                      $pak['aed_pak_type_id'])
                      ?
                      '<option value="' . $pak['aed_pak_type_id'] .
                      '" selected>' . $pak['pak_type'] . '</option>'
                      :
                      '<option value="' . $pak['aed_pak_type_id'] . '">' .
                      $pak['pak_type'] . '</option>';
                  }
                }
                ?>
              </select>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Lot
              <input type="text" name="spare-ped-pak-lot" value="<?php echo $aed['spare_ped_pak_lot']; ?>"/>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Expiration
              <input type="date" name="spare-ped-pak-expiration" value="<?php echo $aed['spare_ped_pak_expiration']; ?>"/>
            </label>
          </div>
          <div class="large-2 columns">
            <label>Attached
              <input type="radio" name="pak-attached" value="spare" />
            </label>
          </div>
        </div>
        <div id="battery-row" class="row" <?php echo mysqli_num_rows($batteries) == 0 ? 'style="display: none;"' : ''?>>
          <div class="large-4 columns">
            <label>Battery
              <select id="battery-select" name="battery">
                <option value="" selected="selected" hidden="hidden">-- Select Battery --</option>
                <?php
                while($battery = $batteries->fetch_assoc()) {
                  echo ($aed['aed_battery_type_id'] == $battery['aed_battery_type_id']) ?
                    '<option value="'.$battery['aed_battery_type_id'].'" selected>'.$battery['battery_type'].'</option>' :
                    '<option value="'.$battery['aed_battery_type_id'].'">'.$battery['battery_type'].'</option>';
                }
                ?>
              </select>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Lot
              <input type="text" name="battery-lot" value="<?php echo $aed['bat_lot']; ?>"/>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Expiration
              <input type="date" name="battery-expiration" value="<?php echo $aed['bat_expiration']; ?>"/>
            </label>
          </div>
          <div class="large-2 columns">
            <label>Attached
              <input type="radio" name="battery-attached" value="main" checked="checked"/>
            </label>
          </div>
        </div>
        <div id="spare-battery-row" class="row" <?php echo mysqli_num_rows($batteries) == 0 ? 'style="display: none;"' : ''?>>
          <div class="large-4 columns">
            <label>Spare Battery
              <select id="spare-battery-select" name="spare-battery">
                <option value="" selected="selected" hidden="hidden">-- Select Battery --</option>
                <?php
                mysqli_data_seek($batteries, 0);
                while($battery = $batteries->fetch_assoc()) {
                  echo ($aed['spare_aed_battery_type_id'] == $battery['aed_battery_type_id']) ?
                    '<option value="'.$battery['aed_battery_type_id'].'" selected>'.$battery['battery_type'].'</option>' :
                    '<option value="'.$battery['aed_battery_type_id'].'">'.$battery['battery_type'].'</option>';
                }
                ?>
              </select>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Lot
              <input type="text" name="spare-battery-lot" value="<?php echo $aed['spare_bat_lot']; ?>"/>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Expiration
              <input type="date" name="spare-battery-expiration" value="<?php echo $aed['spare_bat_expiration']; ?>"/>
            </label>
          </div>
          <div class="large-2 columns">
            <label>Attached
              <input type="radio" name="battery-attached" value="spare" />
            </label>
          </div>
        </div>
        <div id="accessory-row" class="row" <?php echo mysqli_num_rows($accessories) == 0 ? 'style="display: none;"' : ''?>>
          <div class="large-4 columns">
            <label>Accessory
              <select id="accessory-select" name="accessory">
                <option value="" selected="selected" hidden="hidden">-- Select Accessory --</option>
                <?php
                while($accessory = $accessories->fetch_assoc()) {
                  echo ($aed['aed_accessory_type_id'] == $accessory['aed_accessory_type_id']) ?
                    '<option value="'.$accessory['aed_accessory_type_id'].'" selected>'.$accessory['accessory_type'].'</option>' :
                    '<option value="'.$accessory['aed_accessory_type_id'].'">'.$accessory['accessory_type'].'</option>';
                }
                ?>
              </select>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Lot
              <input type="text" name="accessory-lot" value="<?php echo $aed['acc_lot']; ?>"/>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Expiration
              <input type="date" name="accessory-expiration" value="<?php echo $aed['acc_expiration']; ?>"/>
            </label>
          </div>
          <div class="large-2 columns"></div>
        </div>
      </div>
    </div>
    <hr>
    <div class="large-12 columns">
      <div id="edit-map" style="width: 100%;height: 400px;"></div>
    </div>
    <br>
    <div class="row">
      <div class="large-4 columns button-group expanded" style="margin: 0 auto;">
        <a href="./dashboard.php?content=aeds" class="button alert">Cancel</a>
        <input type="submit" class="button" value="Submit"/>
      </div>
    </div>
  </form>
  <button class="close-button" data-close="" aria-label="Close reveal"
    type="button">
    <span aria-hidden="true">×</span>
  </button>
</div>
<div id="deactivate-aed-modal" class="reveal warning callout text-center tiny" data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Deactivate aed</h4>
  <form id="deactivate-aed-form" method="post" action="#">
    <input name="aed-id" type="hidden" value="<?php echo $id ;?>">
    <p>Are you sure?</p>
    <div class="large-4 columns button-group expanded" style="margin: 0 auto;">
      <a data-close="" class="button">Cancel</a>
      <input type="submit" class="button alert" value="Submit"/>
    </div>
  </form>
  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div id="delete-aed-modal" class="reveal warning callout text-center tiny" data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Delete aed</h4>
  <form id="delete-aed-form" method="post" action="#">
    <input name="aed-id" type="hidden" value="<?php echo $id ;?>">
    <p>Are you sure?</p>
    <div class="large-4 columns button-group expanded" style="margin: 0 auto;">
      <a data-close="" class="button">Cancel</a>
      <input type="submit" class="button alert" value="Submit"/>
    </div>
  </form>
  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div id="aed-success-modal" class="reveal callout success text-center tiny"
  data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Success</h4>
  <a href="dashboard.php?content=aeds" class="button success">OK</a>
</div>
