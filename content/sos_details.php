<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 11/15/2017
 * Time: 11:11 AM
 */
$id = $fvmdb->real_escape_string($_GET['id']);

//    $fvmdb->query(sprintf("
//    SELECT s.*, CONCAT(u.firstname, ' ', u.lastname) as created_by_name, u.username as username, u.phone
//    FROM emma_sos s
//    JOIN users u on s.created_by_id = u.id
//    WHERE s.emma_sos_id = %s
//", $id));





//verify if allowed
//get plan event is in
//check if user is in plan
//check if user is admin rights for plan
$userID = $USER->id;

$eventPlanResult = select_sosDetails_sosID($id);
$eventPlan = $eventPlanResult->fetch_assoc();
$eventPlanID = $eventPlan['emma_plan_id'];

$userGroupsWithAdminResult = select_userGroupsWithAdminonly_userID($userID, $eventPlanID);
//echo 'UserID: '.$userID." eventPlan: ". $eventPlanID." activePlan: ". $USER->emma_plan_id;
if($userGroupsWithAdminResult->num_rows > 0){

    if($eventPlanID != $USER->emma_plan_id) {

        $CONFIG = json_decode(file_get_contents('config/config.json'));
        $USER = null;

        $token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key, array('HS512'));

        $USER = $token->data;

        $plans = select_plan_from_planID($eventPlanID);
        $plan = $plans->fetch_assoc();

        if ($plan['securities']) $plansecurity = true;
        if ($plan['panic']) $plansos = true;


        $token->data->emma_plan_id = $eventPlanID;
        $token->data->privilege->plansecurity = $plansecurity;
        $token->data->privilege->plansos = $plansos;
        $jwt = Firebase\JWT\JWT::encode($token, $CONFIG->key, 'HS512');
        setcookie('jwt', $jwt, time() + (9 * 60 * 60), '/');
    }

    $soses = select_sosDetails_soses($id);

}
else{
    $soses = null;
}













if ($soses->num_rows > 0) {
    $sos = $soses->fetch_assoc()
    ?>
  <div class="title row expanded align-middle">
    <div class="columns medium-4">
      <h2 class="text-left"><a href="./dashboard.php?content=sos"><i class="page-icon fa fa-life-ring"></i> EMMA SOS Details</a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
    </div>
  </div>
  <script type="text/javascript">
    let mainSos = <?php echo json_encode($sos); ?>;
  </script>
  <div class="row expanded">
    <div class="large-6 medium-12 small-12 columns">
      <div class="card-info primary">
        <div class="card-info-content">
          <div class="row">
            <div class="large-12 medium-12 small-12 columns">
              <h3 class="lead">Details</h3>
              <p>Original Alert Date <?php echo date('m/d/Y H:i:s',strtotime($sos['pending_date'])); ?></p>
              <p>Created Coordinates: <?php echo $sos['pending_lat'].', '.$sos['pending_lng']; ?></p>
              <p>User: <?php echo $sos['created_by_name']; ?></p>
              <p>Username: <?php echo $sos['username']; ?></p>
              <p>User Phone: <?php echo $sos['phone']; ?></p>
              <?php echo $sos['help_date'] != null ? '<p>SOS Created: '.date('m/d/Y H:i:s',strtotime($sos['help_date'])).'</p>' : ''; ?>
              <?php echo $sos['help_date'] != null ? '<p>SOS Coordinates: '.$sos['help_lat'].', '.$sos['help_lng'].'</p>' : ''; ?>
              <?php echo $sos['cancelled_date'] != null ? '<p>Cancelled: '.date('m/d/Y H:i:s',strtotime($sos['cancelled_date'])).'</p>' : ''; ?>
              <?php echo $sos['cancelled_date'] != null ? '<p>Cancelled Coordinates: '.$sos['cancelled_lat'].', '.$sos['cancelled_lng'].'</p>' : ''; ?>
              <?php echo $sos['closed_date'] != null ? '<p>Closed Date: '.date('m/d/Y H:i:s',strtotime($sos['closed_date'])).'</p>' : ''; ?>
              <?php echo $sos['closed_date'] == null && $sos['cancelled_date'] == null ? '<a data-open="close-sos-modal" class="button">Close SOS</a>' : ''; ?>
              <?php echo $sos['closed_date'] == null && $sos['cancelled_date'] == null ? '' : '<p>Closed Comments: '.$sos['closed_comments'].'</p>'; ?>
              <?php
              $callReports = select_sosDetails_callReports($sos['emma_sos_id']);
//                  $fvmdb->query("
//                select *
//                from emma_sos_call_reports
//                where emma_sos_id = '".$sos['emma_sos_id']."'
//              ");
              if($callReport = $callReports->fetch_assoc()) {
                echo 'Closed PDF: <a target="_blank" href="process/create_closed_sos_report.php?id='.$callReport['emma_sos_call_report_id'].'"><i class="large fa fa-file-text"></i></a>';
              }
              ?>
            </div>
          </div>
        </div>
      </div><!--/ Details -->
    </div>
    <div class="large-6 medium-12 small-12 columns">
      <div class="card-info secondary">
        <div id="sos-map" style="width:100%; height:400px;"></div>
      </div><!--/ Map -->
    </div>
  </div>
    <?php if($sos['allow_responses'] == 1 && (strtotime($sos['pending_date'] . ' +' . $sos['response_time_minutes'] . ' minutes') > strtotime('now'))){  ?>
    <div class="row expanded">
        <div class="large-6 medium-12 small-12 columns">
            <div class="card-info alert">
                <form id="sos-response-form">
                    <div class="row card-info-content">
                        <div class="large-12 columns">
                            <h3 class="lead">Respond</h3>
                            <textarea name="comments"></textarea>
                            <input type="submit" class="button" value="Submit" style="float: right">
                            <input name="sos-id" type="hidden"
                                   value="<?php echo $sos['emma_sos_id']; ?>"/>
                            <input name="user-id" type="hidden"
                                   value="<?php echo $USER->id; ?>"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="large-6 medium-12 small-12 columns">
            <div class="card-info alert">
                <div class="row card-info-content">
                    <div class="large-12 columns">
                        <h3 class="lead">Response Feed</h3>
                        <div id="live-feed">
                            <div class="large-12 columns">
                                <div class="row">
                                    <div id="feed-center" style="width: 100%">
                                        <?php
                                        $responses = select_feed_sos_responses($id);
                                        while ($response = $responses->fetch_assoc()) {
                                            echo '
                                                <div class="callout alert-callout-subtle radius inset-shadow success" style="overflow:hidden;word-wrap:break-word;">
                                                    <div>
                                                        <a href="dashboard.php?content=user&id=' . $response['userid'] . '" style="margin-top: 10px;">' . $response['name'] . ' (' . $response['username'] . ')</a><span> - ' . date('H:i:s m/d/Y', strtotime($response['created_date'])) . '</span>
                                                    </div>
                                   
                                                    <div>
                                                        <b>Response: </b><span style="margin-left: 5px;">' . $response['response'] . '</span>
                                                    </div>
                                                </div>
                                            ';
                                        }

                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php  }?>

  <!-- MODALS -->
  <div id="close-sos-modal" class="reveal callout text-center"
    data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4>Close SOS?</h4>
    <hr>
    <form id="close-sos" action="#" method="post">
      <input name="user-id" type="hidden" value="<?php echo $USER->id; ?>"/>
      <input name="sos-id" type="hidden" value="<?php echo $id; ?>"/>
      <div class="row">
        <div class="large-12 columns">
          <label>Comments
            <textarea name="comments" class="expanded" style="height:5rem;width:100%;min-width:100%;"></textarea>
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-12 columns text-left">
          <span>Call Log Info?</span>
          <div class="switch small" style="display: inline-block; float: right">
            <input class="switch-input" id="call-log" type="checkbox"
              name="call-log">
            <label class="switch-paddle" for="call-log">
              <span class="show-for-sr">Call Log Info?</span>
              <span class="switch-active" aria-hidden="true">Yes</span>
              <span class="switch-inactive" aria-hidden="true">No</span>
            </label>
          </div>
          <div id="call-log-info"
            style="display: none; clear: both;">
            <input name="sos-id" type="hidden"
              value="<?php echo $sos['emma_sos_id']; ?>"/>
            <h4 class="text-center">Call Report</h4>
            <hr>
            <div class="row columns">
              <label>Officer Name</label>
              <input type="text" name="officer-name">
            </div>
            <div class="row columns">
              <label>Description of Incident</label>
              <textarea name="incident-description"></textarea>
            </div>
            <div class="row columns">
              <label>Results of Investigation & Action Taken</label>
              <textarea name="resulting-action"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="large-6 columns">
            <a data-close class="button" style="float: right">Cancel</a>
        </div>
        <div class="large-6 columns">
            <input type="submit" class="button" style="float: left" value="Submit"/>
        </div>
      </div>
    </form>
    <button class="close-button" data-close="" aria-label="Close reveal"
      type="button">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div id="success_modal" class="reveal callout success text-center tiny"
    data-reveal
    data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4>Success</h4>
    <a data-close="" href="./dashboard.php?content=sos" class="button success">OK</a>
  </div>
  <!--/ MODALS -->

<?php } else { ?>
  <div class="row columns">

  </div>
  <div class="callout alert">
    Could not find Event
  </div>
<?php } ?>
