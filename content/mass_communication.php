<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 11/15/2017
 * Time: 11:11 AM
 */
$id = $fvmdb->real_escape_string($_GET['id']);
$events = select_massComunication_events($id);
//    $fvmdb->query(sprintf("
//    SELECT e.*, CONCAT(u.firstname, ' ', u.lastname) AS reported_by, u.id AS user_id, u.username AS user_email, u.phone AS user_phone
//    FROM emma_mass_communications e
//    JOIN users u ON e.created_by_id = u.id
//    WHERE emma_mass_communication_id = %s
//", $id));
if ($event = $events->fetch_assoc()) { ?>
    <div class="title row expanded align-middle">
        <div class="columns medium-8">
            <h2 class="text-left"><a href="./dashboard.php?content=mass_communications"><i class="page-icon fa fa-exclamation-triangle"></i> Mass Notifications</a></h2>
        </div>
        <div class="columns show-for-medium"></div>
        <div class="columns shrink">
        </div>
    </div>
    <script>let massId = <?php echo $id;?></script>
    <div class="row expanded">
        <div class="large-4 medium-12 small-12 columns">
            <div class="card-info primary">
                <div class="card-info-content">
                    <div class="row">
                        <div class="large-12 medium-12 small-12 columns">
                            <h3 class="lead">Details</h3>
                            <p>Location: <?php echo $event['name']; ?></p>
                            <p>Original Alert
                                Reported: <?php echo date('n/j/Y g:i:s a', strtotime($event['created_date'] . ' +' . $USER->timezone . ' Hours')); ?></p>
                            <p>Reported By: <?php echo $event['reported_by']; ?></p>
                            <p>User Email: <a href="mailto:<?php echo $event['user_email']; ?>"><?php echo $event['user_email']; ?></a></p>
                            <p>User Phone: <?php echo $event['user_phone']; ?></p>
                            <p>Sent To: <?php $groups = select_massComunication_groups($id); $totalcount = $groups->num_rows; $i = 1; while($group = $groups->fetch_assoc()){echo '<a href="dashboard.php?content=group&id='. $group['emma_group_id'] .'">' . $group['name'] . '</a>';if($i == $totalcount){echo ' ';}else{echo ', ';} $i++;} ?></p>
                            <p>Notification: <?php echo $event['notification']; ?></p>
                            <button data-open="broadcast-notification-modal" class="button expanded">Broadcast Notification</button>
<!--                            <button data-open="respond-notification-modal" class="button expanded">Respond</button>-->
                        </div>
                    </div>
                </div>
            </div><!--/ Details -->
        </div>
        <div class="large-8 medium-12 small-12 columns">
            <div class="card-info primary">
                <div class="card-info-content">
                    <div class="row">
                        <div class="large-12 medium-12 small-12 columns" id="mass-history">
                            <h5 class="history">History</h5>
                            <div class="max-height">
                                <?php
                                    $notifications = select_massComunication_notifications($USER->emma_plan_id, $event['created_date']);
//                                        $fvmdb->query("
//                                        SELECT e.*, CONCAT(u.firstname,' ',u.lastname) as name, u.username
//                                        FROM emma_mass_communications e
//                                        JOIN users u ON e.created_by_id = u.id
//                                        WHERE e.emma_plan_id = '". $USER->emma_plan_id ."'
//                                        AND e.created_date > '". $event['created_date'] ."' - INTERVAL 2 HOUR
//                                        AND e.created_date < '". $event['created_date'] ."' + INTERVAL 2 HOUR
//                                    ");
                                    while($not = $notifications->fetch_assoc()){
                                        if($not['emma_mass_communication_id'] == $event['emma_mass_communication_id']){
                                            $color = 'color: red;';
                                        }else{
                                            $color = '';
                                        }
                                        echo'
                                            <div class="callout alert-callout-subtle radius inset-shadow system" style="overflow:hidden;word-wrap:break-word;">
                                                <div>
                                                    <a href="dashboard.php?content=mass_communication&id='. $not['emma_mass_communication_id'] .'" style="margin-top: 10px;'. $color .'">'. $not['name'] .' ('. $not['username'] .')</a><span> - '. date('H:i:s m/d/Y',strtotime($not['created_date'])) .'</span>
                                                </div>
                                                <div>
                                                    <b>Sent To: </b><span style="margin-left: 5px;">'; $groups = select_massComunication_groups($not['emma_mass_communication_id']); $totalcount = $groups->num_rows; $i = 1; while($group = $groups->fetch_assoc()){echo '<a href="dashboard.php?content=group&id='. $group['emma_group_id'] .'">' . $group['name'] . '</a>';if($i == $totalcount){echo ' ';}else{echo ', ';} $i++;} echo'</span>
                                                </div>
                                                <div>
                                                    <b>Message: </b><span style="margin-left: 5px;">'. $not['notification'] .'</span>
                                                </div>
                                            </div>
                                            ';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/ Details -->
        </div>
        <?php if($USER->privilege->admin){ ?>
        <div class="large-4 medium-12 small-12 columns">
            <div class="card-info primary">
                <div class="card-info-content">
                    <div class="row">
                        <div class="large-12 medium-12 small-12 columns" id="mass-received">
                            <h5 class="received">Received</h5>
                            <div class="max-height">
                                <?php
                                    $type = 'notification';
                                    $received = select_getreceived($id, $type);
                                    while($r = $received->fetch_assoc()){
                                        echo'
                                            <div>
                                                <div>
                                                    <span style="margin-left: 5px;"><a href="dashboard.php?content=user&id='. $r['id'] .'">' . $r['fullname'] . ' (' . $r['username'] . ')</a> - '. date('H:i:s m/d/Y', strtotime($r['date_received'])) .'</span>                         
                                                </div>
                                            </div>
                                            ';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/ Details -->
        </div>
<!--            --><?php
//            $texts = select_massnotification_texts($id);
//            ?>
<!--        <div class="large-4 medium-12 small-12 columns">-->
<!--            <div class="card-info primary">-->
<!--                <div class="card-info-content">-->
<!--                    <div class="row">-->
<!--                        <div class="large-12 medium-12 small-12 columns" id="mass-received">-->
<!--                            <h5 class="received">Text Sent</h5>-->
<!--                            <div class="max-height">-->
<!--                                --><?php
//                                    while($text = $texts->fetch_assoc()){
//                                        echo'
//                                            <div>
//                                                <div>
//                                                    <span style="margin-left: 5px;"><a href="dashboard.php?content=user&id='. $r['id'] .'">' . $r['fullname'] . ' (' . $r['username'] . ')</a> - '. date('H:i:s m/d/Y', strtotime($r['date_received'])) .'</span>
//                                                </div>
//                                            </div>
//                                            ';
//                                    }
//                                ?>
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        <?php }?>
        <div class="large-8 medium-12 small-12 columns" id="response-container">
            <?php if($event['allow_response'] == '1' && (strtotime($event['created_date'] . ' +' . $event['response_time_minutes'] . ' minutes') >= strtotime('now'))){?>
            <div class="card-info primary">
                <div class="card-info-content">
                    <div class="row">
                        <div class="large-12 medium-12 small-12 columns" id="mass-response">
                            <form id="mass-response-form">
                                <input type="hidden" name="user-id" value="<?php echo $USER->id;?>">
                                <input type="hidden" name="event-id" value="<?php echo $event['emma_mass_communication_id'];?>">
                                <h5>Response</h5>
                                <div>
                                    <div class="row">
                                        <div class="large-12 columns">
                                            <textarea name="response"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-10 columns">

                                        </div>
                                        <div class="large-2 columns">
                                            <input type="submit" class="button expanded" value="Submit">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!--/ Details -->
        </div>
        <?php  }?>
    </div>


<?php
} else { ?>
    <div class="row columns">

    </div>
    <div class="callout alert">
        Could not find Notification
    </div>
<?php } ?>

<div id="broadcast-notification-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Broadcast Notification</h4>
    <hr>
    <form id="rebroadcast-event" action="#" method="post">
        <input name="user-id" type="hidden" value="<?php echo $USER->id; ?>"/>
        <input name="communication-id" type="hidden" value="<?php echo $event['emma_mass_communication_id']; ?>"/>
        <input name="plan-id" type="hidden" value="<?php echo $event['emma_plan_id']; ?>"/>
        <?php
        echo'
        <div class="row">
            <div class="large-12 columns">
                <label class="text-left">Script
                    <select class="select-broadcast-notification-script" id="select-broadcast-notification-script">
                        <option value="" data-text="">-Select-</option>';
                        $notificationScripts = select_dashboard_notificationScripts($USER->emma_plan_id);
                        //                            $fvmdb->query("
                        //                            SELECT `name`, `text`
                        //                            FROM emma_mass_notification_scripts
                        //                            WHERE emma_plan_id = '". $USER->emma_plan_id ."'
                        //                            AND active = 1
                        //                        ");
                        while($notificationScript = $notificationScripts->fetch_assoc()){
                        echo'<option value="'. $notificationScript['text'] .'">'. $notificationScript['name'] .'</option>';
                        }
                        echo'</select>
                </label>
            </div>
        </div>';
        ?>
        <div class="row">
            <div class="large-12 columns">
                <label class="text-left">Notification <span style="color: red;">*</span>
                    <textarea class="expanded" name="notification" id="broadcast-notification-text"
                              style="height:5rem;width:100%;min-width:100%;"><?php echo $event['notification']; ?></textarea>
                </label>
            </div>
        </div>
        <h5>Groups <span style="color: red">*</span></h5>
        <div class="row">
            <div class="large-4 medium-12 small-12 columns">
                <div class="row">
                    <div class="small-3 column">
                        <div class="switch tiny">
                            <input class="switch-input all-select" id="all-select" type="checkbox" >
                            <label class="switch-paddle" for="all-select">
                                <span class="show-for-sr">All</span>
                            </label>
                        </div>
                    </div>
                    <div class="small-9 column">
                        <label class="text-left">All</label>
                    </div>
                </div>
            </div>
            <?php
            $groups = select_groups_with_planID($USER->emma_plan_id);
//            $fvmdb->query("
//                    SELECT *
//                    FROM emma_groups
//                    WHERE emma_plan_id = '" . $USER->emma_plan_id . "'
//                    ORDER BY name
//                ");
            $continue = true;
            while ($group = $groups->fetch_assoc()) {
                echo '
                      <div class="large-4 medium-12 small-12 columns">
                      <div class="row">
                        <div class="small-3 column">
                          <div class="switch tiny">
                            <input class="switch-input broadcast-group group-select" id="b-group-' . $group['emma_group_id'] .
                    '" type="checkbox" name="groups['. $group['emma_group_id'] .']" value="' . $group['emma_group_id'] . '">
                            <label class="switch-paddle" for="b-group-' . $group['emma_group_id'] . '">
                              <span class="show-for-sr">' . $group['name'] . '</span>
                            </label>
                          </div>
                        </div>
                        <div class="small-9 column">
                          <label class="text-left">' . $group['name'] . '</label>
                        </div>
                      </div>
                      </div>
                    ';
            }
            ?>
        </div>
        <div class="row">
            <div class="large-4 columns"></div>
            <div class="large-4 columns">
                <div class="button-group expanded" style="margin: 0 auto;">
                    <button class="button alert" data-close>Cancel</button>
                    <input type="submit" class="button" value="Submit"/>
                </div>
            </div>
            <div class="large-4 columns"></div>
        </div>
    </form>
    <button class="close-button" data-close="" aria-label="Close reveal" type="button">
        <span aria-hidden="true">×</span>
    </button>
    <div id="broadcast-error"></div>
</div>


<div id="respond-notification-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Respond</h4>
    <hr>
    <form id="respond-form" action="#" method="post">
        <input name="user-id" type="hidden" value="<?php echo $USER->id; ?>"/>
        <input name="plan-id" type="hidden" value="<?php echo $event['emma_plan_id']; ?>"/>
        <div class="row">
            <div class="large-12 columns">
                <label class="text-left">Message <span style="color: red;">*</span>
                    <textarea class="expanded" name="message" style="height:5rem;width:100%;min-width:100%;"></textarea>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="large-4 columns"></div>
            <div class="large-4 columns">
                <div class="button-group expanded" style="margin: 0 auto;">
                    <button class="button alert" data-close>Cancel</button>
                    <input type="submit" class="button" value="Submit"/>
                </div>
            </div>
            <div class="large-4 columns"></div>
        </div>
    </form>
    <button class="close-button" data-close="" aria-label="Close reveal" type="button">
        <span aria-hidden="true">×</span>
    </button>
    <div id="broadcast-error"></div>
</div>
