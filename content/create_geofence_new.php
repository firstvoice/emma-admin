<div class="title row expanded align-middle">
    <div class="columns medium-4">
        <h2 class="text-left"><a href="./dashboard.php?content=geofences"><i class="page-icon fa fa-globe"></i> Create Geofence</a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
    </div>
</div>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<meta charset="utf-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<div class="row expanded" style="height: 70%">
    <div class="large-8 medium-12 small-12 columns">
        <div id="map" style="border: solid 1px #0078C1; margin-top: 1%"></div>
        <!--                <div class="over_map">-->
        <!--                    <button class="button" id="geoFence">Create Geofence</button>-->
        <!--                    <div id="gfInfo" class="gfInfoHidden row">-->
        <!--                        <div class="large-8 columns">-->
        <!--                            <input type="text" id="gfName" name="Geofenced Name" placeholder="Geofence Area Name">-->
        <!--                        </div>-->
        <!--                        <div class="large-4 columns">-->
        <!--                            <button class="button" id="save">Submit</button>-->
        <!--                        </div>-->
        <!--                    </div>-->
        <!--                    <div id="tools" class="toolsHidden">-->
        <!--                        <button id="rec">Draw Rec</button> -->
        <!--                        <button class="button" id="rec2">Draw Rectangle</button>-->
        <!--                        <button class="button" id="cir">Draw Circle</button>-->
        <!--                    </div>-->
        <!--                    <p id="nameGFCheck" class="errorMsg">Please enter a name for the geofence area</p>-->
        <!--                    <p id="shapeGFCheck" class="errorMsg">Please draw at least 1 shape</p>-->
        <!--                </div>-->
    </div>
    <div class="large-4 medium-12 small-12 columns">
        <div class="card-info primary">
            <div class="card-info-content">
                <div class="over_map">
                    <div class="row">
                        <div class="large-12 columns">
                            <button class="button expanded" id="geoFence">Create Geofence</button>
                        </div>
                    </div>
                    <div id="gfInfo" class="gfInfoHidden row">
                        <div class="large-9 columns">
                            <input type="text" id="gfName" name="Geofenced Name" placeholder="Geofence Area Name">
                        </div>
                        <div class="large-3 columns">
                            <button class="button" id="save">Submit</button>
                        </div>
                    </div>
                    <div id="tools" class="toolsHidden row">
                        <!-- <button id="rec">Draw Rec</button> -->
                        <div class="large-4 columns">
                            <button class="button expanded" id="dRec" style="float: left;">Draw Rectangle</button>
                        </div>
                        <div class="large-4 columns">
                            <button class="button expanded" id="cir" style="float: left;">Draw Circle</button>
                        </div>
                    </div>
                    <div id="rotateRecTool" class="rotateRecTool row">
                        <div class="large-4 columns">
                            <button id="rotateBtn" class="button expanded">Clockwise</button>
                        </div>
                        <div class="large-4 columns">
                            <button id="rotateBtnAnt" class="button expanded">Anti-Clockwise</button>
                        </div>
                        <div class="large-4 columns">
                            <button id="dRecDone" class="button expanded">Done</button>
                        </div>
                    </div>
                    <p id="nameGFCheck" class="errorMsg">Please enter a name for the geofence area</p>
                    <p id="shapeGFCheck" class="errorMsg">Please draw at least 1 shape</p>
                </div>
                <?php
                $sites = select_sites_with_planID($USER->emma_plan_id);
                $site = $sites->fetch_assoc();
                ?>
                <script>
                    // This example requires the Drawing library. Include the libraries=drawing
                    // parameter when you first load the API. For example:
                    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=drawing">
                    var map;
                    let json = {
                        gfName: "",
                        circles: [],
                        // rectangles:[],
                        circleObjects:[],
                        infoWindowsCircles:[],
                        recObjects: [],
                        infoWindowsRec: []
                    };
                    var drawingManager;
                    function initMap() {
                        map = new google.maps.Map(document.getElementById('map'), {
                            center: {lat: <?php echo $site['emma_site_latitude'];?>, lng: <?php echo $site['emma_site_longitude'];?>},
                            zoom: 14
                        });
                    }

                    document.getElementById("save").addEventListener("click", function(){
                        let gfName = document.getElementById("gfName").value;
                        if(gfName !== null && gfName !== undefined && gfName !== "") {
                            json.gfName = gfName;

                            if (json.circleObjects.length > 0 || json.recObjects.length > 0){
                                console.log(json);

                                // Close drawing manager
                                if(drawingManager) {
                                    drawingManager.setMap(null);
                                }
                                document.getElementById("gfInfo").style.display = "none";
                                document.getElementById("tools").style.display = "none";
                                document.getElementById("geoFence").style.display = "flex";

                                document.getElementById("circle-container").innerHTML = `<input type="hidden" name="geoname" value="`+ gfName +`">`;
                                for(var i=0; i < json.circles.length; i++) {
                                    if (!!json.circles[i]) {
                                        document.getElementById("circle-container").innerHTML += `<input type="hidden" name="geolat[]" value="` + json.circles[i].centerLat.toString() + `">`;
                                        document.getElementById("circle-container").innerHTML += `<input type="hidden" name="geolng[]" value="` + json.circles[i].centerLng.toString() + `">`;
                                        document.getElementById("circle-container").innerHTML += `<input type="hidden" name="georad[]" value="` + json.circles[i].radiusMeters.toString() + `">`;
                                    }
                                }
                                $("#confirm_geofence_modal").foundation("open");
                                // json = {
                                //   gfName: "",
                                //   circles: [],
                                //   circleObjects:[],
                                //   recObjects: []
                                // };
                            } else {
                                document.getElementById("shapeGFCheck").style.display = "flex";
                                setTimeout(function(){
                                    document.getElementById("shapeGFCheck").style.display = "none";
                                }, 3000);
                            }

                        } else {
                            document.getElementById("nameGFCheck").style.display = "flex";
                            setTimeout(function(){
                                document.getElementById("nameGFCheck").style.display = "none";
                            }, 3000);
                        }



                        // for(var i = 0;i < json.circleObjects.length; i++) {
                        //   json.circleObjects[i].setOptions({editable:false});
                        //   json.circleObjects[i].setOptions({draggable:false});
                        // }

                        // for(var i = 0;i < json.recObjects.length; i++) {
                        //   json.recObjects[i].setOptions({editable:false});
                        //   json.recObjects[i].setOptions({draggable:false});
                        // }


                    });

                    document.getElementById("geoFence").addEventListener("click", function(){
                        // drawCir();
                        document.getElementById("tools").style.display = "flex";
                        document.getElementById("gfInfo").style.display = "flex";
                        document.getElementById("geoFence").style.display = "none";
                    });

                    // document.getElementById("rec").addEventListener("click", function(){
                    //   drawRec();
                    // });
                    document.getElementById("dRec").addEventListener("click", function(){
                        document.getElementById("gfInfo").style.display = "none";
                        document.getElementById("tools").style.display = "none";
                        document.getElementById("geoFence").style.display = "none";
                        document.getElementById("rotateRecTool").style.display = "flex";
                        drawDiagonalRec();
                    });
                    document.getElementById("cir").addEventListener("click", function(){
                        drawCir();
                    });
                </script>
            </div>
        </div>
    </div>
</div>


<script src="js/circlesRectangles.js"></script>
<script src="js/rotateRec.js"></script>
<script src="js/rectangle.js"></script>
<script src="js/circles.js"></script>
<script src="js/create_geofence_new.js"></script>



<div id="success_modal" class="reveal callout tiny success text-center"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <div class="row columns">
        <a data-close class="button success"
           style="margin-left:auto;margin-right:auto;" href="./dashboard.php?content=geofences">Ok</a>
    </div>
</div>

<div id="confirm_geofence_modal" class="reveal callout tiny text-center"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Create Geofence?</h4>
    <br>
    <div class="row columns">
        <form id="create_geofence_form" class="text-center" style="margin: 0 auto;">
            <div id="circle-container"></div>
            <input type="hidden" name="plan-id" value="<?php echo $USER->emma_plan_id;?>">
            <input type="hidden" name="user-id" value="<?php echo $USER->id;?>">
            <a data-close class="button alert">Cancel</a>
            <input type="submit" class="button" value="Submit">
        </form>
    </div>
</div>