<?php
/**
 * Created by PhpStorm.
 * User: John
 * Date: 12/6/2017
 * Time: 20:48
 */


?>
<div class="title row expanded align-middle">
    <div class="columns medium-4">
        <h2 class="text-left"><a href="./dashboard.php?content=text_console"><i class="page-icon fa fa-phone"></i> <?php echo ucwords(str_replace('_', ' ', $content)); ?></a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink"></div>
</div>
<div class="row expanded" style="margin-top: 10px">
    <div class="large-12 columns">
        <div class="row expanded">
            <div class="large-5 columns">
                <span> Enter Numbers (one per line, format 1676353728) </span>
                <textarea id="numbers" style="height: 150px"></textarea>
            </div>
            <div class="large-2 columns">
                <span>Request rate (ms)</span>
                <input type="text" id="rate" value=10>
            </div>
            <div class="large-5 columns">
                <span> Message </span>
                <textarea id="message" style="height: 150px"></textarea>
            </div>
        </div>
        <div class="row expanded">
            <div class="large-10 columns" id="log">
                <b> Log </b>
            </div>
            <div class="large-2 columns">
                <button class="button expanded" id="send">Send message</button>
            </div>
        </div>
    </div>
</div>

<!--<script>-->
<!--    var rate = 1000;-->
<!--    var numbers;-->
<!--    var message;-->
<!--    var loop;-->
<!--    var c = 0;-->
<!--    $(function() {-->
<!--        $("#rate").keyup(function() {-->
<!--            rate = $(this).val();-->
<!--        })-->
<!--        $("#send").click(function() {-->
<!--            c = 0;-->
<!--            numbers = $("#numbers").val().trim().split("\n");-->
<!--            message = $("#message").val();-->
<!--            send();-->
<!--            console.log(numbers);-->
<!--        })-->
<!--    })-->
<!--    function send() {-->
<!--        var n = numbers[c].trim();-->
<!--        $.ajax({-->
<!--            type: "POST",-->
<!--            url: "https://igotcut.com",-->
<!--            data: { number: n, message:  message },-->
<!--            success: function(res) {-->
<!--                console.log(res);-->
<!--                log("Success : message '" + message + "'' delivered to " + n);-->
<!--                c++;-->
<!--                if(c < numbers.length){-->
<!--                    setTimeout(send, rate);-->
<!--                }-->
<!--            },-->
<!--            error: function (res) {-->
<!--                log("Failure : " + res.message);-->
<!--            }-->
<!--        });-->
<!--    }-->
<!--    function log(msg) {-->
<!--        $("#log").append($("<span></span>").html(msg));-->
<!--    }-->
<!--</script>-->

