<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/12/2017
 * Time: 4:13 PM
 */

$type_id = $fvmdb->real_escape_string($_GET['resource_type']);
$types = select_resourceType_Types($type_id);
//    $fvmdb->query("
//    SELECT t.resource_name AS name
//    FROM emma_resource_types AS t
//    WHERE t.emma_resource_type_id = " . $type_id . "
//");
$type_name = $types->fetch_assoc();
?>
<div class="title row expanded align-middle">
  <div class="columns medium-8">
    <h2 class="text-left"><a href="./dashboard.php?content=resources"><i class="page-icon fa fa-map"></i> Resource
        Folder - <?php echo $type_name['name']; ?></a></h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
      data-options="disableHover:true;clickOpen:true;">
      <li>
        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <ul class="menu">
          <li><a href="#" data-open="add-resource-modal">New Resource</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>

<div class="large-12">
  <div class="row expanded margin-top-1em image-row">
    <?php
    $resource_query = select_resourceType_resourceQuery($USER->emma_plan_id, $type_id);
//        $fvmdb->query("
//            SELECT r.*
//            FROM emma_resources AS r
//            WHERE r.emma_plan_id = " . $USER->emma_plan_id . "
//            AND r.emma_resource_type_id = " . $type_id . "
//            AND r.active = 1
//            ORDER BY r.file_name
//        ");
    if ($resource_query->num_rows < 1) {
      echo '<p>There are no resources of that type.</p>';
    } else {
      $count = 0;

      while ($resource = $resource_query->fetch_assoc()) {
        if (file_exists($resource['file_string'])) {
          if ($count % 6 == 0 && $count != 0) {
            echo '</div><div class="row expanded margin-top-1em image-row">';
          }

          if ($resource['file_extension'] == 'png') {
            echo '<div class="large-2 column">
                        <a href="./dashboard.php?content=resource&id=' .
              $resource['emma_resource_id'] . '"><img class="thumbnail" src="' .
              $resource['file_string'] . '" /></a>
                        <div class="small-12 text-center">' .
              $resource['file_name'] . '</div>
                        </div>
                        ';
          } else if ($resource['file_extension'] == 'pdf') {
            echo '
                        <div class="large-2 column">
                            <a href="./dashboard.php?content=resource&id=' .
              $resource['emma_resource_id'] . '">
                                <object data="' . $resource['file_string'] . '" type="application/pdf" width="100%" height="90%">
                                   alt : <a href="./dashboard.php?content=resource&id=' .
              $resource['emma_resource_id'] . '">' . $resource['file_name'] . '</a>
                                </object>
                            </a>
                            <a href="./dashboard.php?content=resource&id=' .
              $resource['emma_resource_id'] . '">
                                <div class="small-12 text-center">' .
              $resource['file_name'] . '</div>
                            </a>
                        </div>
                    ';

          } else if ($resource['file_extension'] == 'docx') {
            echo '<div class="large-2 column">
                    <a href="./dashboard.php?content=resource&id=' .
              $resource['emma_resource_id'] . '">
                        <img class="thumbnail placeholder" src="resource_folder/placeholder_icons/word.png" width="100%" height="90%" />
                    </a>
                    <a href="./dashboard.php?content=resource&id=' .
              $resource['emma_resource_id'] . '">
                        <div class="small-12 text-center">' .
              $resource['file_name'] . '</div>
                    </a>
                    </div>
                    ';
          } else if ($resource['file_extension'] == 'xlsx') {
            echo '<div class="large-2 column">
                    <a href="./dashboard.php?content=resource&id=' .
              $resource['emma_resource_id'] . '">
                        <img class="thumbnail placeholder" src="resource_folder/placeholder_icons/excel.png" width="100%" height="90%" />
                    </a>
                    <a href="./dashboard.php?content=resource&id=' .
              $resource['emma_resource_id'] . '">
                        <div class="small-12 text-center">' .
              $resource['file_name'] . '</div>
                    </a>
                    </div>
                    ';
          }
          $count++;
        }
      }
    }

    ?>
  </div>
</div>

<div id="add-resource-modal" class="reveal callout large" data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <form id="new-resource-form" enctype="multipart/form-data" action=""
    method="post">
    <input type="hidden" name="plan_id"
      value="<?php echo $USER->emma_plan_id; ?>"/>
    <input type="hidden" name="user_id" value="<?php echo $USER->id; ?>"/>
    <div class="text-center">
      <h4 class="lead">New Resource</h4>
    </div>
    <label>File:
      <input type="file" name="file"
        accept=".jpeg,.png,.gif,.doc,.docx,.xls,.xlsx,.csv,image/gif,image/png,image/jpeg,application/pdf"
        required/>
    </label>
    <label>File Name:
      <input type="Text" name="file_name" required/>
    </label>
    <label>Resource Category:
      <select name="file_type">
        <?php
        $resource_types = select_resourceType_resourceTypes();
//            $fvmdb->query("
//          SELECT *
//          FROM emma_resource_types
//          WHERE active = 1
//        ");
        while ($resource_type = $resource_types->fetch_assoc()) {
          echo '<option value="' . $resource_type['emma_resource_type_id'] .
            '">' . $resource_type['resource_name'] . '</option>';
        }
        ?>
      </select>
    </label>

    <label>Site:
      <select name="site_id">
        <?php
        $sites = select_resourceType_sites($USER->emma_plan_id);
//            $fvmdb->query("
//          SELECT e.*
//          FROM emma_sites AS e
//          WHERE e.emma_plan_id = " . $USER->emma_plan_id . "
//        ");
        while ($site = $sites->fetch_assoc()) {
          echo '<option value="' . $site['emma_site_id'] . '">' .
            $site['emma_site_name'] . '</option>';
        }
        ?>
      </select>
    </label>


    <div class="text-center small-12">
      <input type="submit" value="submit" class="button"/>
    </div>
    <button class="close-button" data-close aria-label="Close reveal"
      type="button">
      <span aria-hidden="true">&times;</span>
    </button>
  </form>
</div>
<div id="success_modal" class="reveal callout success text-center tiny"
  data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Success</h4>
  <a href="./dashboard.php?content=resources" data-close class="button success">Ok</a>
<!--  <button class="close-button" data-close aria-label="Close reveal"-->
<!--    type="button">-->
<!--    <span aria-hidden="true">&times;</span>-->
<!--  </button>-->
</div>
<div id="invalid_modal" class="reveal callout small" data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>File type is not allowed</h4>
  <div class="text-center small-12">
    <a data-close class="button">Ok</a>
  </div>
  <button class="close-button" data-close aria-label="Close reveal"
    type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
