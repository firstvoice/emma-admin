<?php
$id = $fvmdb->real_escape_string($_GET['id']);
$events = select_map_events($id);
$aeds = select_map_aeds();
?>

<div id="map" style="width: 100%; height: 90vh;"></div>
