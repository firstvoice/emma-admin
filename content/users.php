<?php

if (!($USER->privilege->admin)) {
    redirect();
}

?>

<div class="title row expanded align-middle">
  <div class="columns medium-2">
    <h2 class="text-left"><a href="./dashboard.php?content=users"><i class="page-icon fa fa-user-circle"></i> <?php echo ucwords($content); ?></a></h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
        data-options="disableHover:true;clickOpen:true;">
      <li>
        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <ul class="menu">
          <li><a href="./dashboard.php?content=create_user" id="user_create">Create User</a></li>
          <li><a href="./dashboard.php?content=assign_groups" id="user_create">Assign Users</a></li>
          <li><a href="./dashboard.php?content=remove_groups" id="user_create">Remove Users</a></li>
          <li><a href="./dashboard.php?content=deactivate_user" id="user_create">Deactivate User</a></li>
          <li><a href="./dashboard.php?content=reactivate_user" id="user_create"  >Reactivate User</a></li>
            <!---<li><a href="./dashboard.php?content=reactivate_guest_code" id="user_create">Reactivate Guest Code</a></li>--->
        </ul>
      </li>
    </ul>
  </div>
</div>
<!--    <div class="large-4 medium-12 column text-right">-->
<!--        <a href="./dashboard.php?content=create_user" id="user_create" class="button" >Create User</a>-->
<!--        <input id="user_assign_group" class="button " type="button" value="Assign Users" />-->
<!--        <input id="user_remove_group" class="button" type="button" value="Remove Users" />-->
<!--        <input id="user_deactivate" class="button " type="button" value="Deactivate User" />-->
<!--    </div>-->

<div>
  <ul class="tabs" data-tabs id="script-type-tabs">
    <li class="tabs-title is-active">
      <a href="#users-active" aria-selected="true">Active</a>
    </li>
    <li class="tabs-title">
      <a href="#users-inactive">Inactive</a>
    </li>
    <li class="tabs-title">
      <a href="#users-guest" aria-selected="true">Guest</a>
    </li>
    <li class="tabs-title">
      <a href="#users-setup">Unassigned</a>
    </li>
  </ul>
  <div class="tabs-content" data-tabs-content="script-type-tabs">
    <div id="users-active" class="tabs-panel is-active">
      <form id="users_form">
        <table id="users-table" class="data-table" style="width:100%">
          <thead>
          <tr>
              <th class="text-search">Username</th>
              <th class="text-search">First Name</th>
              <th class="text-search">Last Name</th>
              <th class="text-search">Group</th>
          </tr>
          <tr>
            <th>Username</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Group</th>
          </tr>
          </thead>
          <tbody>
          </tbody>
          <tfoot>

          </tfoot>
        </table>
      </form>
    </div>
      <div id="users-inactive" class="tabs-panel">
          <form id="users_form">
              <table id="users-inactive-table" class="data-table" style="width:100%">
                  <thead>
                  <tr>
                      <th class="text-search">Username</th>
                      <th class="text-search">First Name</th>
                      <th class="text-search">Last Name</th>
                      <th class="text-search">Group</th>
                  </tr>
                  <tr>
                      <th>Username</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Group</th>
                  </tr>
                  </thead>
                  <tbody>
                  </tbody>
                  <tfoot>

                  </tfoot>
              </table>
          </form>
      </div>
        <div id="users-guest" class="tabs-panel">
          <table id="guest-users-table" class="data-table" style="width:100%">
            <thead>
            <tr>
                <th class="text-search">Username</th>
                <th class="text-search">First Name</th>
                <th class="text-search">Last Name</th>
                <th class="text-search">Group</th>
                <th> </th>
            </tr>
            <tr>
              <th>Username</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Group</th>
                <th>Code Status</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>

            </tfoot>
          </table>
        </div>
      <div id="users-setup" class="tabs-panel">
      <table id="setup-users-table" class="data-table" style="width:100%">
        <thead>
        <tr>
          <th>Username</th>
          <th>First Name</th>
          <th>Last Name</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>

        </tfoot>
      </table>
    </div>
  </div>
</div>


<div id="update_group_modal" class="reveal callout small" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
  <h4>Assign group</h4>
  <label> Groups
    <form id="assign_group_form">
      <div class="row large-12 columns">

        <?php
        $allgroups = select_groups_with_planID($USER->emma_plan_id);
//        $fvmdb->query("
//                                SELECT g.*
//                                FROM emma_groups AS g
//                                WHERE g.emma_plan_id = " . $USER->emma_plan_id . "
//                              ");
        while ($allgroup = $allgroups->fetch_assoc()) {

          echo '
                    <div class="large-3 medium-6 small-12 columns">
                        <div class="row"> 
                            <div class="small-4 column">
                                <div class="switch tiny">
                                    <input class="switch-input" name="group-ids[]" value="' .
            $allgroup['emma_group_id'] . '" id="group-id-' . $allgroup['emma_group_id'] . '" type="checkbox" >
                                    <label class="switch-paddle" for="group-id-' . $allgroup['emma_group_id'] . '">
                                        <span class="show-for-sr">' . $allgroup['name'] . '</span>
                                    </label>
                                </div>
                            </div>
                            <div class="small-8">
                              <label class="text-left">' . $allgroup['name'] . '</label>
                            </div>
                        </div>
                    </div>';

        }
        ?>

      </div>
    </form>
  </label>
  <div class="text-center small-12">
    <input type="button" class="button" id="assign_group_submit" value="Submit"/>
  </div>
  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<div id="remove_group_modal" class="reveal callout small" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
  <h4>Remove from groups</h4>
  <label> Groups
    <form id="remove_group_form">
      <div class="row large-12 columns">

        <?php
        $allgroups = select_groups_with_planID($USER->emma_plan_id);
//        $fvmdb->query("
//                                SELECT g.*
//                                FROM emma_groups AS g
//                                WHERE g.emma_plan_id = " . $USER->emma_plan_id . "
//                              ");
        while ($allgroup = $allgroups->fetch_assoc()) {

          echo '
                    <div class="large-3 medium-6 small-12 columns">
                        <div class="row"> 
                            <div class="small-4 column">
                                <div class="switch tiny">
                                    <input class="switch-input" name="group-ids[]" value="' .
            $allgroup['emma_group_id'] . '" id="remove-group-id-' . $allgroup['emma_group_id'] . '" type="checkbox" >
                                    <label class="switch-paddle" for="remove-group-id-' . $allgroup['emma_group_id'] . '">
                                        <span class="show-for-sr">' . $allgroup['name'] . '</span>
                                    </label>
                                </div>
                            </div>
                            <div class="small-8">
                              <label class="text-left">' . $allgroup['name'] . '</label>
                            </div>
                        </div>
                    </div>';
        }
        ?>

      </div>
    </form>
  </label>
  <div class="text-center small-12">
    <input type="button" class="button" id="remove_group_submit" value="Submit"/>
  </div>
  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<div id="no-selected-users" class="reveal callout small" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
  <h4>No users selected.</h4>
  <div class="text-center small-12">
    <a href="./dashboard.php?content=users" data-close class="button">Ok</a>
  </div>
  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<div id="success_modal" class="reveal callout success text-center tiny" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
  <h4>Success</h4>
  <a href="./dashboard.php?content=users" data-close class="button success">Ok</a>
<!--  <button class="close-button" data-close aria-label="Close reveal" type="button">-->
<!--    <span aria-hidden="true">&times;</span>-->
<!--  </button>-->
</div>
