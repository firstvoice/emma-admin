<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 11/15/2017
 * Time: 11:11 AM
 */


$id = $fvmdb->real_escape_string($_GET['id']);

$ars = select_ar_with_id($id);

if ($ars->num_rows > 0) {
    $ar = $ars->fetch_assoc();
    ?>
    <div class="title row expanded align-middle">
        <div class="columns medium-8">
            <h2 class="text-left"><a href="./dashboard.php?content=anonymous_reports"><i class="page-icon fa fa-eye-slash"></i> Anonymous Report</a></h2>
        </div>
        <div class="columns show-for-medium"></div>
        <div class="columns shrink">
        </div>
    </div>
    <div class="row expanded">
        <div class="large-6 medium-12 small-12 columns">
            <div class="card-info primary">
                <div class="card-info-content">
                    <div class="row">
                        <div class="large-12 medium-12 small-12 columns">
                            <h3 class="lead">Details</h3>
                            <p><b>Created Date:</b> <?php echo date('m/d/Y H:i:s',strtotime($ar['created_date'])); ?></p>
                            <p><b>Description:</b> <?php echo $ar['description']; ?></p>
                            <?php echo $ar['closed'] != 0 ? '<p><b>Closed Date:</b> '.date('m/d/Y H:i:s',strtotime($ar['closed_date'])).'</p>' : ''; ?>
                            <?php echo $ar['closed'] == 0 ? '<a data-open="close-ar-modal" class="button">Close Anonymous Report</a>' : ''; ?>
                            <?php echo $ar['closed'] == 0 ? '' : '<p><b>Closed Comments:</b> '.$ar['closed_comments'].'</p>'; ?>
                            <?php echo $ar['closed'] == 0 ? '' : '<p><b>Closed By:</b> '.$ar['username'].'</p>'; ?>
                        </div>
                    </div>
                </div>
            </div><!--/ Details -->
        </div>
    </div>

    <!-- MODALS -->
    <div id="close-ar-modal" class="reveal callout text-center"
         data-reveal data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4>Close Anonymous Report?</h4>
        <hr>
        <form id="close-ar" action="#" method="post">
            <input name="user-id" type="hidden" value="<?php echo $USER->id; ?>"/>
            <input name="ar-id" type="hidden" value="<?php echo $id; ?>"/>
            <div class="row">
                <div class="large-12 columns">
                    <label>Comments
                        <textarea name="comments" class="expanded" style="height:5rem;width:100%;min-width:100%;"></textarea>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="large-3 columns"></div>
                <div class="large-6 large-centered columns">
                    <div class="expanded button-group">
                        <a data-close class="button alert">Cancel</a>
                        <input type="submit" class="button" value="Submit"/>
                    </div>
                </div>
                <div class="large-3 columns"></div>
            </div>
        </form>
        <button class="close-button" data-close="" aria-label="Close reveal"
                type="button">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div id="success_modal" class="reveal callout success text-center tiny"
         data-reveal
         data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4>Success</h4>
        <a data-close="" href="./dashboard.php?content=anonymous_reports" class="button success">OK</a>
    </div>
    <!--/ MODALS -->

<?php } else { ?>
    <div class="row columns">

    </div>
    <div class="callout alert">
        Could not find Event
    </div>
<?php } ?>
