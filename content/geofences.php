
<?php


if(!$USER->privilege->geofences){
    header('Location: dashboard.php?content=geofence_demo');
}


?>
<div class="title row expanded align-middle">
    <div class="columns medium-3">
        <h2 class="text-left"><a href="./dashboard.php?content=geofences"><i class="page-icon fa fa-globe"></i> <?php echo ucwords($content); ?></a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
        <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
            data-options="disableHover:true;clickOpen:true;">
            <li>
                <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
                <ul class="menu">
                    <li><a href="./dashboard.php?content=create_geofence_new" id="geofence_create">Create New Geofence</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<div>
    <form id="geofences_form">
        <table id="geofences-table" class="data-table" style="width:100%">
            <thead>
            <tr>
                <th>Geofence Name</th>
                <th>NW Corner Coordinates</th>
                <th>SE Corner Coordinates</th>
                <th>Center Coordinates</th>
                <th>Radius</th>
                <th>Type</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            </tfoot>
        </table>
    </form>
</div>


<div id="success_modal" class="reveal callout success text-center tiny" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <a href="./dashboard.php?content=geofences" data-close class="button success">Ok</a>
<!--    <button class="close-button" data-close aria-label="Close reveal" type="button">-->
<!--        <span aria-hidden="true">&times;</span>-->
<!--    </button>-->
</div>
