<?php
$plans = select_plan_from_planID($USER->emma_plan_id);
//$fvmdb->query("
//  select *
//  from emma_plans
//  where emma_plan_id = '" . $USER->emma_plan_id . "'
//");
if ($plan = $plans->fetch_assoc()) { ?>
  <form id="edit-plan-form" action="#" method="post">
    <input type="hidden" name="plan-id" value="<?php echo $plan['emma_plan_id']; ?>" />
    <div class="title row expanded align-middle">
      <div class="columns medium-4">
          <h2 class="text-left"><a href="./dashboard.php?content=plan"><i class="page-icon fa fa-book"></i> Edit Plan</a></h2>
      </div>
      <div class="columns show-for-medium"></div>
      <div class="columns shrink">
        <button class="button">Save</button>
      </div>
    </div>

    <div class="row expanded">
      <div class="large-12 small-12 columns">
        <div class="card-info primary">
          <div class="card-info-content">
            <div class="row">
              <div class="large-12 medium-12 small-12 columns">
                <h3 class="lead">Details</h3>
                <div class="row">
                  <div class="small-6 columns">
                    <p>Name: <input type="text"
                        value="<?php echo $plan['name']; ?>"/></p>
                    <p>Max Sites: <input type="number"
                        value="<?php echo $plan['max_sites']; ?>"/></p>
                    <p>Security Upgrade: <input
                        type="checkbox" <?php echo $plan['securities']
                        ? 'checked'
                        : ''; ?>/></p>
                  </div>
                  <div class="small-6 columns">
                    <p>Date Active: <?php echo $plan['date_active']; ?></p>
                    <p>Date Expired: <?php echo $plan['date_expired']; ?></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div><!--/ Details -->
      </div>
      <div class="large-12 small-12 columns">
        <div class="card-info primary">
          <div class="card-info-content">
            <div class="row">
              <div class="large-12 medium-12 small-12 columns">
                <div class="row">
                  <div class="large-6 small-12 columns">
                    <h3 class="lead">Active Event Types</h3>
                    <ul class="vertical menu">
                      <?php
                      $eventTypes = select_eventTypes_with_planID($plan['emma_plan_id']);
//                      $fvmdb->query("
//                      select et.*
//                      from emergency_types et
//                      join emma_plan_event_types pet on et.emergency_type_id = pet.emma_emergency_type_id
//                      where pet.emma_plan_id = '" . $plan['emma_plan_id'] . "'
//                    ");
                      while ($eventType = $eventTypes->fetch_assoc()) {
                        echo '<li><a href="#" style="justify-content:start;" class="event-type-link" data-img-filename="' .
                          $eventType['img_filename'] .
                          '" data-open="event-type-details-modal" data-type-name="' .
                          $eventType['name'] . '" data-type-id="' .
                          $eventType['emergency_type_id'] . '">' .
                          $eventType['name'] . '</a></li>';
                      }
                      if ($plan['securities']) {
                        echo '<li><a href="#" style="justify-content:start;" class="event-type-link" data-img-filename="icon_security.png" data-open="event-type-details-modal" data-type-name="Securities Upgrade" data-type-id="0">Securities Upgrade</a></li>';
                      }
                      ?>
                    </ul>
                  </div>
                  <div class="large-6 small-12 columns">
                    <h3 class="lead">Upgradable Event Types</h3>
                    <ul class="vertical menu">
                      <?php
                      $eventTypes = select_editPlan_eventTypes($plan['emma_plan_id']);
//                          $fvmdb->query("
//                      select et.*
//                      from emergency_types et
//                      where et.emergency_type_id not in (
//                        select pet.emma_emergency_type_id
//                        from emma_plan_event_types pet
//                        where emma_plan_id = '" . $plan['emma_plan_id'] . "'
//                      )
//                    ");
                      while ($eventType = $eventTypes->fetch_assoc()) {
                        echo '<li><a href="#" style="justify-content:start;" class="event-type-link" data-img-filename="' .
                          $eventType['img_filename'] .
                          '" data-open="event-type-details-modal" data-type-name="' .
                          $eventType['name'] . '" data-type-id="' .
                          $eventType['emergency_type_id'] . '">' .
                          $eventType['name'] . '</a></li>';
                      }
                      if (!$plan['securities']) {
                        echo '<li><a href="#" style="justify-content:start;" class="event-type-link" data-img-filename="icon_security.png" data-open="event-type-details-modal" data-type-name="Securities Upgrade" data-type-id="0">Securities Upgrade</a></li>';
                      }
                      ?>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div><!--/ Security Types -->
      </div>
    </div>
  </form>
  <div id="event-type-details-modal" class="reveal callout text-center small"
    data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4 id="event-type-name">Type Details</h4>
    <hr>
    <div class="row text-left" style="padding-bottom:1rem;">
      <div class="large-4 columns">
        <img id="event-type-image"
          style="border: 1px solid black; width: 200px;"/>
      </div>
      <div class="large-8 columns">
        <h5>Sub-Types</h5>
        <ul id="sub-type-list">
        </ul>
      </div>
    </div>
    <button class="close-button" data-close="" aria-label="Close reveal"
      type="button">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div id="security-type-details-modal" class="reveal callout text-center small"
    data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <input type="hidden" name="plan-id"
      value="<?php echo $USER->emma_plan_id; ?>">
    <h4>Security Type Details</h4>
    <hr>
    <div class="row text-left">
      <div class="large-4 columns">
        <img style="border: 1px solid black; width: 200px; height: 200px;"/>
      </div>
      <div class="large-8 columns">
        <h5>Sub-Types</h5>
      </div>
    </div>
    <button class="close-button" data-close="" aria-label="Close reveal"
      type="button">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div id="edit-security-types-modal" class="reveal callout text-center small"
    data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <form id="edit-security-types-form" action="#" method="post">
      <input type="hidden" name="plan-id"
        value="<?php echo $USER->emma_plan_id; ?>">
      <h4>Edit Security Types</h4>
      <hr>
      <div class="row text-left">
        <div class="small-12 columns">
          <ul>
            <?php
            $securityTypes = select_editPlan_securityTypes();
//                $fvmdb->query("
//            select st.*
//            from emma_security_types st
//          ");
            while ($securityType = $securityTypes->fetch_assoc()) {
              $planSecurityTypes = select_editPlan_planSecurityTypes($plan['emma_plan_id'], $securityType['emma_security_type_id']);
//                  $fvmdb->query("
//              select *
//              from emma_plan_security_types
//              where emma_plan_id = '" . $plan['emma_plan_id'] . "'
//              and emma_security_type_id = '" .
//                $securityType['emma_security_type_id'] . "'
//            ");
              if ($planSecurityTypes->num_rows > 0) {
                echo '<li><input type="checkbox" name="security-types[' .
                  $securityType['emma_security_type_id'] . ']" checked/>' .
                  $securityType['name'] . '</li>';
              } else {
                echo '<li><input type="checkbox" name="security-types[' .
                  $securityType['emma_security_type_id'] . ']" />' .
                  $securityType['name'] . '</li>';
              }
            }
            ?>
          </ul>
        </div>
      </div>
      <div class="row columns">
        <input type="submit" class="button" value="Save"/>
      </div>
    </form>
    <button class="close-button" data-close="" aria-label="Close reveal"
      type="button">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div id="plan-success-modal" class="reveal callout success text-center tiny"
    data-reveal
    data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4>Success</h4>
    <a href="./dashboard.php?content=plan" class="button success">OK</a>
  </div>
<?php } ?>