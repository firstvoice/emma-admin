<script type="text/javascript"
  src="https://www.gstatic.com/charts/loader.js"></script>
<!--<script type="text/javascript" src="https://www.google.com/jsapi"></script>-->

<!--large gray menu bar at top -->
<div class="title row expanded align-middle">

  <div class="columns medium-3">
    <h2 class="text-left"><a href="./dashboard.php?content=home"><i class="page-icon fa fa-home"></i> <?php echo ucwords($content) ; ?></a>
    </h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
      data-options="disableHover:true;clickOpen:true;">
      <li>
        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <ul class="menu">
          <li><a href="dashboard.php?content=plan">Plan Profile</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>
<?php
$arpriv = false;
$events = false;
$hide = 'hidden';
$types = select_eventTypes_with_multiplans($USER->emma_home_plans);
if($types->num_rows > 0){
    $events = true;
    $hide = '';
}
$moss = false;
if($USER->privilege->admin){
//    if($USER->id == 8412) {
//    var_dump($USER);
//    }
    $checked = '';

    if($USER->privilege->ar){
        $ars = select_ar_with_multiplan($USER->emma_home_plans);
        if($ars->num_rows > 0){
            $arpriv = true;
        }
    }else if($USER->privilege->mar){
        $moss = true;
        $ars = select_mar_with_multiplan($USER->emma_home_plans);
        if($ars->num_rows > 0){
            $arpriv = true;
        }
    }

    $plans = select_home_plans($USER->id);
//        $fvmdb->query("
//        SELECT p.name, p.emma_plan_id
//        FROM emma_multi_plan m
//        LEFT JOIN emma_plans p ON m.plan_id = p.emma_plan_id
//        WHERE m.user_id = '". $USER->id ."'
//    ");
    $plancount = $plans->num_rows;

//    echo($plancount . ' ' . count($USER->emma_home_plans));

    if(count($USER->emma_home_plans) == $plancount){
        $checked = ' checked';
    }
    ?>

<div class="row expanded">
    <div class="large-12 columns">
        <div class="row">
            <!--<div class="large-2 columns button small" id="show-plans" style="margin-top: 10px; margin-left: 15px; margin-bottom: 5px">-->
            <div class="large-2 columns button small" id="show-plans">
                Dashboard View
                <i id="show-plans-arrow" class="fa fa-angle-double-down"></i>
            </div>
        </div>
        <div class="row expanded" id="plans-container" style="margin-top: 10px">
            <?php

            echo'<div class="large-2 medium-4 small-6 columns text-center">
                        <div class="row">
                            <div class="large-3 small-3 column">
                                <div class="switch tiny">
                                    <input class="switch-input" data-id="all" id="plan-all-select" type="checkbox"'. $checked .'>
                                    <label class="switch-paddle" for="plan-all-select">
                                        <span class="show-for-sr">All</span>
                                    </label>
                                </div>
                            </div>
                            <!--contains text for the first toggle-->
                            <div class="large-9 small-9 column" style="text-align: left">
                                 <span class="text-left">All</span>
                            </div>
                        </div>
                    </div>';

            $plans = select_home_plans($USER->id);
//            $fvmdb->query("
//                SELECT p.name, p.emma_plan_id
//                FROM emma_multi_plan m
//                LEFT JOIN emma_plans p ON m.plan_id = p.emma_plan_id
//                WHERE m.user_id = '". $USER->id ."'
//            ");
            while($plan = $plans->fetch_assoc()){
                if(in_array($plan['emma_plan_id'],$USER->emma_home_plans)){
                    echo'
                    <div class="large-2 medium-4 small-6 columns text-center">
                        <div class="row">
                            <div class="large-3 small-3 column">
                                <div class="switch tiny">
                                    <input class="switch-input plan-select" data-id="'. $plan['emma_plan_id'] .'" id="'. $plan['name'] .'-select" name="'. $plan['emma_plan_id'] .'" type="checkbox" checked>
                                    <label class="switch-paddle" for="'. $plan['name'] .'-select">
                                        <span class="show-for-sr">'. $plan['name'] .'</span>
                                    </label>
                                </div>
                            </div>
                            <!--contains text for toggle 2-->
                            <div class="large-9 small-9 column" style="text-align: left">
                                <span class="text-left">'. $plan['name'] .'</span>
                            </div>
                        </div>
                    </div>';
                }else{
                    echo'
                    <div class="large-2 medium-4 small-6 columns text-center">
                        <div class="row">
                            <div class="large-3 small-3 column">
                                <div class="switch tiny">
                                    <input class="switch-input plan-select" data-id="'. $plan['emma_plan_id'] .'" id="'. $plan['name'] .'-select" name="'. $plan['emma_plan_id'] .'" type="checkbox">
                                    <label class="switch-paddle" for="'. $plan['name'] .'-select">
                                        <span class="show-for-sr">'. $plan['name'] .'</span>
                                    </label>
                                </div>
                            </div>
                            <div class="large-9 small-9 column" style="text-align: left">
                                <span class="text-left">'. $plan['name'] .'</span>
                            </div>
                        </div>
                    </div>';
                }
            }

            ?>
            <div class="large-2 medium-4 small-6 columns text-center" style="float: right">
                <a class="button" id="default-plan-select">Set As Default</a>
            </div>
        </div>
    </div>
</div>

<?php }?>

<div class="row expanded">
  <div class="large-4 medium-12 small-12 columns">
    <div class="card-info warning">
      <div class="card-info-content">
        <h3 class="lead">All Active - <span style="color: red;">Click Red For More Details</span></h3>
        <div id="active-events-drills" class="chart">
            <?php if($events == true) { ?>
          <ul style="margin-bottom:0;">

                  <li>Events
                      <ul>
                          <?php
                          $activeLives = select_home_activeLives($USER->emma_home_plans);
                          //                    $fvmdb->query("
                          //                  SELECT e.emergency_id, et.name, e.drill
                          //                  FROM emergencies e
                          //                  JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
                          //                  WHERE e.emma_plan_id IN (". implode(',',$USER->emma_home_plans) .")
                          //                  AND e.active = '1'
                          //                  AND e.drill = '0'
                          //                  AND e.parent_emergency_id IS NULL
                          //                ");
                          while ($activeLive = $activeLives->fetch_assoc()) {
                              echo '<li><a href="./dashboard.php?content=event&id=' .
                                  $activeLive['emergency_id'] . '" style="color: red">'. $activeLive['planname'] . ' - ' . $activeLive['name'] .
                                  '</a></li>';
                          }
                          ?>
                      </ul>
                  </li>
                  <li>Drills
                      <ul>
                          <?php
                          $activeDrills = select_home_activeDrills($USER->emma_home_plans);
                          //                    $fvmdb->query("
                          //                  SELECT e.emergency_id, et.name, e.drill
                          //                  FROM emergencies e
                          //                  JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
                          //                  WHERE e.emma_plan_id IN (". implode(',',$USER->emma_home_plans) .")
                          //                  AND e.active = '1'
                          //                  AND e.drill = '1'
                          //                  AND e.parent_emergency_id IS NULL
                          //                ");
                          while ($activeDrill = $activeDrills->fetch_assoc()) {
                              echo '<li><a href="./dashboard.php?content=event&id=' .
                                  $activeDrill['emergency_id'] . '" style="color: red">' . $activeDrill['planname'] . ' - ' . $activeDrill['name'] .
                                  '</a></li>';
                          }
                          ?>
                      </ul>
                  </li>
                  <?php
              }
              ?>
            <?php
            $plans = select_home_securityCheck($USER->emma_home_plans);
//                $fvmdb->query("
//              select *
//              from emma_plans
//              where emma_plan_id IN (". implode(',',$USER->emma_home_plans) .")
//              and securities = 1
//            ");
            if ($plans->num_rows > 0) {
                $activeSecurities = select_home_activeSecurities($USER->emma_home_plans);
//                    $fvmdb->query("
//                    SELECT *
//                    FROM emma_securities s
//                    JOIN emma_security_types st on s.emma_security_type_id = st.emma_security_type_id
//                    WHERE s.emma_plan_id IN (". implode(',',$USER->emma_home_plans) .")
//                    AND s.active = '1'
//                ");

                ?>
                <li><a id="open-security">Security: <span style="color: red"><?php while($security = $activeSecurities->fetch_assoc()){echo $security['name'] . ' - ';$securityCount = get_home_security_count($security['emma_plan_id']); echo $securityCount->num_rows . '   ';}?></span></a>
              </li>
            <?php }
            $plans =  select_home_sosCheck($USER->emma_home_plans);
//                $fvmdb->query("
//              select *
//              from emma_plans
//              where emma_plan_id IN (". implode(',',$USER->emma_home_plans) .")
//              and panic = 1
//            ");
            if ($plans->num_rows > 0) {
            ?>
                <li>EMMA SOS
                  <ul>
                      <?php
                      $sos_query = select_home_sosQuery($USER->emma_home_plans);
//                          $fvmdb->query("
//                            SELECT emma_sos_id, CONCAT(u.firstname ,' ', u.lastname) AS sos_name
//                            FROM emma_sos AS s
//                            JOIN users AS u ON s.created_by_id = u.id AND u.emma_plan_id IN (". implode(',',$USER->emma_home_plans) .")
//                            WHERE s.cancelled_date IS NULL
//                            AND s.help_date IS NULL
//                            AND s.pending_date IS NOT NULL
//                            AND s.closed_date IS NULL
//                        ");
                      echo '<li id="running_sos_parent" class="parent">Current Running SOS\'s: <span style="color: red;">'; while($sos = $sos_query->fetch_assoc()){echo $sos['name'] . ' - ';$sosCount = select_home_sosQuery_count($sos['emma_plan_id']); echo $sosCount->num_rows . '   ';} echo'</span><ul id="sos_running_list" class="list hidden">';
                      echo'</ul></li>';

                      $sos_needs_query = select_home_sosneeds($USER->emma_home_plans);
//                          $fvmdb->query("
//                            SELECT emma_sos_id, CONCAT(u.firstname ,' ', u.lastname) AS sos_name
//                            FROM emma_sos AS s
//                            JOIN users AS u ON s.created_by_id = u.id AND u.emma_plan_id IN (". implode(',',$USER->emma_home_plans) .")
//                            WHERE s.cancelled_date IS NULL
//                            AND s.help_date IS NOT NULL
//                            AND s.closed_date IS NULL
//                        ");

                      echo '<li id="sos_needs_help_parent" class="parent"><a id="open-help" '.($sos_needs_query->num_rows > 0? 'red' : '').'">Needs Help: <span style="color: red;">'; while($sos = $sos_needs_query->fetch_assoc()){echo $sos['name'] . ' - ';$sosCount = select_home_sosneeds_count($sos['emma_plan_id']); echo $sosCount->num_rows . '   ';} echo'</span></a><ul id="sos_need_help_list" class="list hidden">';
                      echo '</ul></li>';
                      ?>
                  </ul>
              </li>
              <?php
            }
              ?>
          </ul>
        </div>
      </div>
    </div>
  </div><!--/ Active Events/Drills -->
  <div class="large-8 medium-12 small-12 columns">
    <div class="card-info warning">
      <div class="card-info-content">
          <div class="row">
              <div class="large-10 columns">
                  <h3 class="lead">Live Feed</h3>
              </div>
              <div class="large-2 columns">
                  <select id="feed-length">
                      <option value="1" <?php if($USER->feed_length == 1)echo 'selected';?>>1 Hour</option>
                      <option value="2" <?php if($USER->feed_length == 2)echo 'selected';?>>2 Hours</option>
                      <option value="6" <?php if($USER->feed_length == 6)echo 'selected';?>>6 Hours</option>
                      <option value="12" <?php if($USER->feed_length == 12)echo 'selected';?>>12 Hours</option>
                      <option value="24" <?php if($USER->feed_length == 24)echo 'selected';?>>24 Hours</option>
                      <option value="48" <?php if($USER->feed_length == 48)echo 'selected';?>>48 Hours</option>
                  </select>
              </div>
          </div>
        <div id="live-feed">
            <div class="large-12 columns">
                <div class="row">
                    <div id="feed-center" style="width: 100%">
                        <?php

                        $alertarray = array();
                        $currentplan = get_emma_plan($USER->emma_plan_id);
                        $hours = $USER->feed_length;
                        $notifications = select_feed_mass($USER->emma_home_plans, $hours);
                        $events = select_feed_events($USER->emma_home_plans, $hours);
                        $lockdowns = select_feed_lockdowns($USER->emma_home_plans, $hours);
                        $mlockdowns = select_feed_mlockdowns($USER->emma_home_plans, $hours);
                        $soss = select_feed_sos($USER->emma_home_plans, $hours);
                        $geofences = select_feed_geofence($USER->emma_home_plans, $hours);


                        if($notifications->num_rows > 0) {
                            while ($notification = $notifications->fetch_assoc()) {
                                $alertarray[] = $notification;
                            }
                        }
                        if($events->num_rows > 0) {
                            while ($event = $events->fetch_assoc()) {
                                $alertarray[] = $event;
                            }
                        }
                        if($lockdowns->num_rows > 0) {
                            while ($lockdown = $lockdowns->fetch_assoc()) {
                                $alertarray[] = $lockdown;
                            }
                        }
                        if($mlockdowns->num_rows > 0) {
                            while ($mlockdown = $mlockdowns->fetch_assoc()) {
                                $alertarray[] = $mlockdown;
                            }
                        }
                        if($soss->num_rows > 0) {
                            while ($sos = $soss->fetch_assoc()) {
                                $alertarray[] = $sos;
                            }
                        }
                        if($geofences->num_rows > 0) {
                            while ($geofence = $geofences->fetch_assoc()) {
                                $alertarray[] = $geofence;
                            }
                        }
                        if(count($alertarray) > 0) {
//                        echo'<br> unsorted <br>';
//                        var_dump($alertarray);
                            function sortFunction($a, $b)
                            {
                                $v1 = strtotime($a["sortdate"]);
                                $v2 = strtotime($b["sortdate"]);
                                return $v2 - $v1;
                            }

                            usort($alertarray, "sortFunction");
//                        echo'<br> sorted <br>';
//                        var_dump($alertarray);
                            foreach ($alertarray as $alert) {
                                echo '
                            <div class="callout alert-callout-subtle radius inset-shadow system" style="overflow:hidden;word-wrap:break-word;">
                                <div>
                                    <a href="dashboard.php?content=' . $alert['alert_type'] . '&id=' . $alert['alert_id'] . '" style="margin-top: 10px;"><h5>' . $alert['type_name'] . ' ';
                                if ($alert['drill'] == 1) {
                                    echo '(DRILL)';
                                }
                                echo '</h5></a>
                                </div>';
                                if ($alert['alert_type'] == 'geofence') {
                                    echo '<div>
                                        <b>Fence: </b><span>' . $alert['geofencename'] . '</span>
                                    </div>';
                                }
                                echo '
                                <div>
                                    <a href="dashboard.php?content=' . $alert['alert_type'] . '&id=' . $alert['alert_id'] . '" style="margin-top: 10px;">' . $alert['name'] . ' (' . $alert['username'] . ')</a><span> - ' . date('H:i:s m/d/Y', strtotime($alert['sortdate'], $USER->timezone . ' Hours')) . '</span>
                                </div>
                                <div>
                                    <b>Plan: </b>
                                    <span>';
                                        $plans = select_get_alert_plans($alert['alert_id'], $alert['alert_type']);
                                        while($plan = $plans->fetch_assoc()){
                                            echo $plan['name'] . ' ';
                                        }
                                    echo'</span>
                                </div>';
                                if ($alert['alert_type'] == 'mass_communication' || $alert['alert_type'] == 'event') {
                                    echo '
                                <div>
                                    <b>Sent To: </b><span style="margin-left: 5px;">';
                                    $groups = select_feed_groups($alert['alert_id'], $alert['alert_type']);
                                    $totalcount = $groups->num_rows;
                                    $i = 1;
                                    while ($group = $groups->fetch_assoc()) {
                                        echo '<a href="dashboard.php?content=group&id=' . $group['emma_group_id'] . '">' . $group['name'] . '</a>';
                                        if ($i == $totalcount) {
                                            echo ' ';
                                        } else {
                                            echo ', ';
                                        }
                                        $i++;
                                    }
                                    echo '</span>
                                </div>';
                                }
                                echo '
                                <div>
                                    <b>Message: </b><span style="margin-left: 5px;">' . $alert['notification'] . '</span>
                                </div>
                            </div>
                            ';
                                if ($alert['alert_type'] == 'event') {
                                    $responses = select_feed_event_responses($alert['alert_id']);
                                    while ($response = $responses->fetch_assoc()) {
                                        echo '
                                    <div class="callout alert-callout-subtle radius inset-shadow success" style="overflow:hidden;word-wrap:break-word;">          
                                        <div>
                                            <a href="dashboard.php?content=user&id=' . $response['userid'] . '" style="margin-top: 10px;">' . $response['name'] . ' (' . $response['username'] . ')</a><span> - ' . date('H:i:s m/d/Y', strtotime($response['created_date'], $USER->timezone . ' Hours')) . '</span>
                                        </div>
                                        <div>
                                            <b>Status: </b><span style="margin-left: 5px; color: ' . $response['color'] . '">' . $response['statusname'] . '</span>
                                        </div>
                                        <div>
                                            <b>Response: </b><span style="margin-left: 5px;">' . $response['response'] . '</span>
                                        </div>
                                    </div>
                                ';
                                    }
                                } elseif ($alert['alert_type'] == 'mass_communication') {
                                    $responses = select_feed_mass_responses($alert['alert_id']);
                                    while ($response = $responses->fetch_assoc()) {
                                        echo '
                                    <div class="callout alert-callout-subtle radius inset-shadow success" style="overflow:hidden;word-wrap:break-word;">
                                        <div>
                                            <a href="dashboard.php?content=user&id=' . $response['userid'] . '" style="margin-top: 10px;">' . $response['name'] . ' (' . $response['username'] . ')</a><span> - ' . date('H:i:s m/d/Y', strtotime($response['created_date'], $USER->timezone . ' Hours')) . '</span>
                                        </div>
                       
                                        <div>
                                            <b>Response: </b><span style="margin-left: 5px;">' . $response['response'] . '</span>
                                        </div>
                                    </div>
                                ';
                                    }
                                } elseif ($alert['alert_type'] == 'geofence') {
                                    $responses = select_feed_geofence_responses($alert['alert_id']);
                                    while ($response = $responses->fetch_assoc()) {
                                        echo '
                                    <div class="callout alert-callout-subtle radius inset-shadow success" style="overflow:hidden;word-wrap:break-word;">
                                        <div>
                                            <a href="dashboard.php?content=user&id=' . $response['userid'] . '" style="margin-top: 10px;">' . $response['name'] . ' (' . $response['username'] . ')</a><span> - ' . date('H:i:s m/d/Y', strtotime($response['created_date'], $USER->timezone . ' Hours')) . '</span>
                                        </div>
                       
                                        <div>
                                            <b>Response: </b><span style="margin-left: 5px;">' . $response['response'] . '</span>
                                        </div>
                                    </div>
                                ';
                                    }
                                } elseif ($alert['alert_type'] == 'sos_details') {
                                    $responses = select_feed_sos_responses($alert['alert_id']);
                                    while ($response = $responses->fetch_assoc()) {
                                        echo '
                                    <div class="callout alert-callout-subtle radius inset-shadow success" style="overflow:hidden;word-wrap:break-word;">
                                        <div>
                                            <a href="dashboard.php?content=user&id=' . $response['userid'] . '" style="margin-top: 10px;">' . $response['name'] . ' (' . $response['username'] . ')</a><span> - ' . date('H:i:s m/d/Y', strtotime($response['created_date'], $USER->timezone . ' Hours')) . '</span>
                                        </div>
                       
                                        <div>
                                            <b>Response: </b><span style="margin-left: 5px;">' . $response['response'] . '</span>
                                        </div>
                                    </div>
                                ';
                                    }
                                }
                            }
                        }else{
                            echo'No Live Events Within '. $hours .' Hours';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php if($arpriv){ ?>
<div class="row expanded">
    <div class="large-4 medium-12 small-12 columns">
        <div class="card-info warning">
            <div class="card-info-content">
                <h3 class="lead">Anonymous Reports</h3>
                <div class="ar-feed">
                    <ul>
                    <?php
                    while($ar = $ars->fetch_assoc()){
                        if($moss) {
                            echo '<li><a href="dashboard.php?content=anonymous_report_moss&id=' . $ar['emma_moss_id'] . '">' . $ar['name'] . ' - ' . date('m/d/Y H:i:s', strtotime($ar['created_date'], $USER->timezone . ' Hours')) . '</a></li>';
                        }else{
                            echo '<li><a href="dashboard.php?content=anonymous_report&id=' . $ar['emma_ar_id'] . '">' . $ar['name'] . ' - ' . date('m/d/Y H:i:s', strtotime($ar['created_date'], $USER->timezone . ' Hours')) . '</a></li>';
                        }
                    }
                    ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>