<?php
if($USER->privilege->plans){
    $link = '<a href="./dashboard.php?content=plans">';
}else{
    $link = '<a href="./dashboard.php?content=home">';
}

if($_GET['id'] != '') {
    $plan = get_emma_plan($_GET['id']);
}else{
    $plan = get_emma_plan($USER->emma_plan_id);
}
$plansettingsQuery = select_default_notification_types_with_plan($plan['emma_plan_id']);
if($plansettingsQuery->num_rows > 0) {
    $plansettings = $plansettingsQuery->fetch_assoc();
}


//if ($plan) { ?>

  <div class="title row expanded align-middle">
    <div class="columns medium-2">
        <h2 class="text-left"><?php echo $link;?><i class="page-icon fa fa-book"></i> <?php echo ucwords($content); ?></a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
        <?php if($USER->privilege->plans){?>
        <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu data-options="disableHover:true;clickOpen:true;">
            <li>
                <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
                <ul class="menu">
                    <li><a href="./dashboard.php?content=edit_plan" >Edit Plan</a></li>
                </ul>
            </li>
        </ul>
    <?php }?>
    </div>
  </div>

  <div class="row expanded">
    <div class="large-12 small-12 columns">
      <div class="card-info primary">
        <div class="card-info-content">
          <div class="row">
            <div class="large-12 medium-12 small-12 columns">
              <h3 class="lead">Details</h3>
              <div class="row">
                <div class="small-6 columns">
                  <p>Name: <?php echo $plan['name']; ?></p>
                  <p>Max Sites: <?php echo $plan['max_sites']; ?></p>
                  <p>Security Upgrade: <?php echo $plan['securities'] ? 'Yes'
                      : 'No'; ?></p>
                </div>
                <div class="small-6 columns">
                  <p>Date Active: <?php echo $plan['date_active']; ?></p>
                  <p>Date Expired: <?php echo $plan['date_expired']; ?></p>
                  <p>Package: <?php echo $plan['package_name'];?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><!--/ Details -->
        <? if($USER->privilege->admin){ ?>
        <div class="card-info primary">
            <div class="card-info-content">
                <div class="row expanded">
                    <div class="large-12 medium-12 small-12 columns">
                        <h3 class="lead">Notification Defaults</h3>
                        <form id="default-noti-form">
                            <input type="hidden" name="pid" value="<?php echo $plan["emma_plan_id"]; ?>">
                            <input type="hidden" name="uid" value="<?php echo $USER->id; ?>">
                        <div class="row expanded">
                            <div class="small-12 large-4 column">

                                <label style="margin-bottom:1rem;font-size:1rem;">
                                    <div class="switch small" style="display:inline-block;float:left;margin-right:1rem;margin-bottom:0;">
                                        <input class="switch-input" id="default-notification" type="checkbox" name="default-notification" <?php if($plansettings['notificationType_appNotification']== 1) echo 'checked'; ?> >
                                        <label class="switch-paddle" for="default-notification">
                                            <span class="show-for-sr">Push Notifications</span>
                                            <span class="switch-active" aria-hidden="true">Yes</span>
                                            <span class="switch-inactive" aria-hidden="true">No</span>
                                        </label>
                                    </div>
                                    Push Notifications
                                </label>

                            </div>
                            <div class="small-12 large-4 column">

                                <label style="margin-bottom:1rem;font-size:1rem;">
                                    <div class="switch small" style="display:inline-block;float:left;margin-right:1rem;margin-bottom:0;">
                                        <input class="switch-input" id="default-email" type="checkbox" name="default-email" <?php if($plansettings['notificationType_email']== 1) echo 'checked'; ?> >
                                        <label class="switch-paddle" for="default-email">
                                            <span class="show-for-sr">Email</span>
                                            <span class="switch-active" aria-hidden="true">Yes</span>
                                            <span class="switch-inactive" aria-hidden="true">No</span>
                                        </label>
                                    </div>
                                    Email Notifications
                                </label>


                            </div>
                            <div class="small-12 large-4 column">

                                <label style="margin-bottom:1rem;font-size:1rem;">
                                    <div class="switch small" style="display:inline-block;float:left;margin-right:1rem;margin-bottom:0;">
                                        <input class="switch-input" id="default-text" type="checkbox" name="default-text" <?php if($plansettings['notificationType_SMStext']== 1) echo 'checked'; ?> >
                                        <label class="switch-paddle" for="default-text">
                                            <span class="show-for-sr">Text</span>
                                            <span class="switch-active" aria-hidden="true">Yes</span>
                                            <span class="switch-inactive" aria-hidden="true">No</span>
                                        </label>
                                    </div>
                                    Text Notifications
                                </label>

                            </div>
                        </div>
                            <div class="row expanded">
                                <div class="small-12 large-2 large-offset-10 column">
                                    <input type="submit" class="button" value="Save" style="margin:0;">
                                </div>
                            </div>
                        </form>
<!--                        testing div -->
<!--                        <div class="row">-->
<!--                            <div class= "small-12 large-4">-->
<!--                                --><?php //echo $plansettings['notificationType_appNotification'] ?>
<!--                            </div>-->
<!--                            <div class= "small-12 large-4">-->
<!--                                --><?php //echo $plansettings['notificationType_email'] ?>
<!--                            </div>-->
<!--                            <div class= "small-12 large-4">-->
<!--                                --><?php //echo $plansettings['notificationType_SMStext'] ?>
<!--                            </div>-->
<!--                        </div>-->
                    </div>
                </div>
            </div>
        </div>
        <?php } ?> <!-- end default notifications -->
    </div>
<!--    <div class="large-12 small-12 columns">-->
<!--      <div class="card-info primary">-->
<!--        <div class="card-info-content">-->
<!--          <div class="row">-->
<!--            <div class="large-12 medium-12 small-12 columns">-->
<!--              <div class="row">-->
<!--                <div class="large-6 small-12 columns">-->
<!--                  <h3 class="lead">Active Event Types</h3>-->
<!--                  <ul class="vertical menu">-->
<!--                    --><?php
//                    get_active_event_types($plan['emma_plan_id']);
//                    if ($plan['securities']) {
//                      echo '<li><a href="#" style="justify-content:start;" class="event-type-link" data-img-filename="icon_security.png" data-type-name="Security Upgrade" data-type-id="0">Security Upgrade</a></li>';
//                    }
//                    ?>
<!--                  </ul>-->
<!--                </div>-->
<!--                <div class="large-6 small-12 columns">-->
<!--                  <h3 class="lead">Upgradable Event Types</h3>-->
<!--                  <ul class="vertical menu">-->
<!--                    --><?php
//                    get_upgrade_event_types($plan['emma_plan_id']);
//                    if (!$plan['securities']) {
//                      echo '<li><a href="#" style="justify-content:start;" class="event-type-link" data-img-filename="icon_security.png" data-type-name="Security Upgrade" data-type-id="0">Security Upgrade</a></li>';
//                    }
//                    ?>
<!--                  </ul>-->
<!--                </div>-->
<!--              </div>-->
<!--            </div>-->
<!--          </div>-->
<!--        </div>-->
<!--      </div> Security Types -->
<!--    </div>-->
  </div>
    <div class="row expanded text-center">
        <div class="large-6 columns" style="margin: 0 auto;">
            <p style="color: red">To upgrade or update your plan call: (319)377-5125</p>
        </div>
    </div>
  <div id="event-type-details-modal" class="reveal callout text-center small"
    data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4 id="event-type-name">Type Details</h4>
    <hr>
    <div class="row text-left" style="padding-bottom:1rem;">
      <div class="large-4 columns">
        <img id="event-type-image"
          style="border: 1px solid black; width: 200px;"/>
      </div>
      <div class="large-8 columns">
        <h5>Sub-Types</h5>
        <ul id="sub-type-list">
        </ul>
      </div>
    </div>
    <button class="close-button" data-close="" aria-label="Close reveal"
      type="button">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div id="security-type-details-modal" class="reveal callout text-center small"
    data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <input type="hidden" name="plan-id"
      value="<?php echo $USER->emma_plan_id; ?>">
    <h4>Security Type Details</h4>
    <hr>
    <div class="row text-left">
      <div class="large-4 columns">
        <img style="border: 1px solid black; width: 200px; height: 200px;"/>
      </div>
      <div class="large-8 columns">
        <h5>Sub-Types</h5>
      </div>
    </div>
    <button class="close-button" data-close="" aria-label="Close reveal"
      type="button">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div id="edit-security-types-modal" class="reveal callout text-center small"
    data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <form id="edit-security-types-form" action="#" method="post">
      <input type="hidden" name="plan-id"
        value="<?php echo $USER->emma_plan_id; ?>">
      <h4>Edit Security Types</h4>
      <hr>
      <div class="row text-left">
        <div class="small-12 columns">
          <ul>
            <?php
            $securityTypes = select_plans_securityTypes();
//                $fvmdb->query("
//            select st.*
//            from emma_security_types st
//          ");
            while ($securityType = $securityTypes->fetch_assoc()) {
              $planSecurityTypes = select_plans_plandSecurityTypes($plan['emma_plan_id'], $securityType['emma_security_type_id']);
//                  $fvmdb->query("
//              select *
//              from emma_plan_security_types
//              where emma_plan_id = '" . $plan['emma_plan_id'] . "'
//              and emma_security_type_id = '" .
//                $securityType['emma_security_type_id'] . "'
//            ");
              if ($planSecurityTypes->num_rows > 0) {
                echo '<li><input type="checkbox" name="security-types[' .
                  $securityType['emma_security_type_id'] . ']" checked/>' .
                  $securityType['name'] . '</li>';
              } else {
                echo '<li><input type="checkbox" name="security-types[' .
                  $securityType['emma_security_type_id'] . ']" />' .
                  $securityType['name'] . '</li>';
              }
            }
            ?>
          </ul>
        </div>
      </div>
      <div class="row columns">
        <input type="submit" class="button" value="Save"/>
      </div>
    </form>
    <button class="close-button" data-close="" aria-label="Close reveal"
      type="button">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div id="plan-success-modal" class="reveal callout success text-center tiny"
    data-reveal
    data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4>Success</h4>
    <a href="./dashboard.php?content=plan" class="button success">OK</a>
  </div>
<?php //} ?>