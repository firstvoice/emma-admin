<?php

?>

<!---Hidden Div of pre-existing contacts for this group/plan that we can clone.--->
<div id="existing-contacts-prefab" style="display: none;">
    <div class="large-6 columns">
        <h3 class="lead"><button id="delete-existing-contact-button" type="button" class="button-expanded" style="background-color: red;"><i style="background-color: white;" class=" fa fa-times"></i></button>Contact
    <?php
    $site_Contacts = select_createSite_siteContacts($USER->emma_plan_id);
//        $fvmdb->query("
//        SELECT CONCAT(esc.first_name, ' ', esc.last_name) as name, esc.emma_site_contact_id
//        FROM emma_site_contacts as esc
//        WHERE esc.emma_plan_id = '".$USER->emma_plan_id."'
//    ");
    echo '<select name=contact>';
    while($site_Contact = $site_Contacts->fetch_assoc())
    {
        echo '<option value="'.$site_Contact['emma_site_contact_id'].'">'.$site_Contact['name'].'</option>';
    }
    echo '</select>';

    ?>
        </h3>
    </div>
</div>


<form id="create-site-form">
  <input type="hidden" name="plan-id"
    value="<?php echo $USER->emma_plan_id; ?>">
  <input id="ignore-address" type="hidden" name="ignore-address" value="0" />
  <div class="title row expanded align-middle">
    <div class="columns medium-6">
      <h2 class="text-left"><a href="./dashboard.php?content=sites"><i class="page-icon fa fa-institution"></i> Create Site</a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
      <input id="save-site-button" class="button" value="Save" style="margin:0;">
    </div>
  </div>
    <input hidden id="lat" name="lat"/>
    <input hidden id="lng" name="lng"/>
  <div class="row expanded">
    <div class="large-12 columns">
      <div class="card-info warning">
        <div class="card-info-content">
          <div>
            <h3 class="lead">Site Information</h3>
          </div>
          <div class="row expanded">
            <div class="large-12 medium-12 column">
              <label>Site Name
                <input required class="needs-filled" type="text" name="name"/>
              </label>
            </div>
          </div>
          <div class="row expanded">
            <div class="large-4 medium-12 small-12 column">
              <label>Address
                <input required class="needs-filled" id="address" type="Text" name="address"/>
              </label>
            </div>
            <div class="large-4 medium-12 small-12 column">
              <label>City
                <input required class="needs-filled" type="Text" id="city" name="city"/>
              </label>
            </div>

            <div class="large-3 medium-12 small-12 column">
              <label>State
                <input required class="needs-filled" type="Text" id="state" name="state"/>
              </label>
            </div>
            <div class="large-1 medium-12 small-12 column">
              <label>Zip
                <input required class="needs-filled" type="Text" id="zip" name="zip"/>
              </label>
            </div>
          </div>
          <div class="row expanded">
            <div class="large-12 columns">
              <label>Notes
                <textarea name="notes" style="width:100%;min-width:100%;height:5rem;"></textarea>
              </label>
            </div>
          </div>


        <div class="row">
        <div class="large-2 columns">
            <label style="text-align: center;">New Contact
                <button id="new-contact-button" type="button" class="button expanded"><i class="fa fa-plus"></i></button>
            </label>
        </div>
            <div class="large-2 columns">
                <label style="text-align: center;">Existing Contact
                    <button id="existing-contact-button" type="button" class="button expanded"><i class="fa fa-plus"></i></button>
                </label>
            </div>
        </div>
            <div id="new-contact-append-here">
            </div>
            <div id="existing-contact-append-here">

            </div>
        </div>
      </div>
    </div>
  </div>
</form>

<div id="success-modal" class="reveal success callout tiny text-center"
  data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Success</h4>
  <div class="row columns">
    <a
      href="./dashboard.php?content=sites"
      data-close class="button"
      style="margin-left:auto;margin-right:auto">Ok</a>
  </div>
<!--  <button class="close-button" data-close aria-label="Close reveal"-->
<!--    type="button">-->
<!--    <span aria-hidden="true">&times;</span>-->
<!--  </button>-->
</div>
<div id="warning-modal" class="reveal callout warning text-center tiny" data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4 style="color:darkred">Warning</h4>
  <div id="warning-list" class="text-left">
    <p>Address not found...</p>
  </div>
  <a id="warning-save" class="button" data-close>Save</a>
  <button class="close-button" data-close aria-label="Close reveal"
    type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
