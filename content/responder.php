<?php
include('include/static.php');
global $fvmdb;
global $USER;
global $US_STATES;


/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 11/15/2017
 * Time: 11:11 AM
 */
$id = $fvmdb->real_escape_string($_GET['id']);
?>


<?php

if ($responder = select_responder($id)->fetch_assoc()) { ?>
  <input readonly hidden id="responder-id" value="<?php echo $id?>" class="responder_id"/>
  <div class="title row expanded align-middle">
    <div class="columns medium-8">
      <h2 class="text-left"><a href="./dashboard.php?content=responders"><i class="page-icon fa fa-cube"></i> Responder</a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
      <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
        data-options="disableHover:true;clickOpen:true;">
        <li>
          <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
          <ul class="menu">
            <?php
            echo '<li><a data-id="'.$responder['id'].'" onclick="edit_responder(this);">Edit Responder</a></li>';
            echo '<li><a data-open="deactivate-responder-modal">Deactivate Responder</a></li>';
            echo '<li><a data-open="delete-responder-modal">Delete Responder</a></li>'
            ?>
          </ul>
        </li>
      </ul>
    </div>
  </div>
  <div class="row expanded">
    <div class="large-4 medium-6 small-12 columns">
      <div class="card-info primary">
        <div class="card-info-content">
          <div class="row">
            <div class="large-12 medium-12 small-12 columns">
              <h3 class="lead">Details</h3>
              <hr>
              <p><b>Name:</b> <?php echo $responder['firstname'] . ' ' . $responder['lastname']; ?></p>
              <p><b>Email:</b> <?php echo $responder['email']; ?></p>
              <p><b>Phone:</b> <?php echo $responder['phone']; ?></p>
              <p><b>Address:</b> <?php echo $responder['address']; ?></p>
              <p><b>City:</b> <?php echo $responder['city']; ?></p>
              <p><b>State:</b> <?php echo $responder['state']; ?></p>
              <p><b>Zip:</b> <?php echo $responder['zip']; ?></p>
            </div>
          </div>
        </div>
      </div><!--/ Details -->
    </div>
    <div class="large-4 medium-6 small-12 columns">
      <div class="card-info primary">
        <div class="card-info-content">
          <div class="row">
            <div class="large-12 medium-12 small-12 columns">
              <h3 class="lead">Classes</h3>
              <hr>
              <table>
                <thead>
                  <tr>
                    <th>Class</th>
                    <th>Expiration</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                $classes = select_responder_classes($responder['id']);
                while($class = $classes->fetch_assoc()) {
                  echo '<tr><td>'.$class['name'].'</td><td>'.$class['expiration'].'</td></tr>';
                }
                ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div><!--/ Details -->
    </div>
    <div class="large-4 medium-6 small-12 columns">
      <div class="card-info primary">
        <div class="card-info-content">
          <div class="row">
            <div class="large-12 medium-12 small-12 columns">
              <h3 class="lead">Notes</h3>
              <hr>
              <p><?php echo $responder['notes']; ?></p>
            </div>
          </div>
        </div>
      </div><!--/ Details -->
    </div>
  </div>
  <script type="text/javascript">
    var responder = <?php echo json_encode($responder); ?>;
  </script>


<?php } else { ?>
  <div class="row columns">

  </div>
  <div class="callout alert">
    Could not find Responder
  </div>
<?php } ?>

<div id="edit-responder-modal" class="reveal callout text-center small"
  data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Edit Responder</h4>
  <hr>
  <form id="edit-responder-form" class="edit-responder-form text-left" action="#"
    method="post">
    <input type="hidden" name="user-id" value="<?php echo $USER->id; ?>"/>
    <input type="hidden" name="id" value="<?php echo $id?>"/>
    <div class="row">
      <div class="large-12 columns">
        <div class="row">
          <div class="large-6 columns">
            <label>First Name
              <input required type="text" name="first-name" value="<?php echo $responder['firstname'] ?>"/>
            </label>
          </div>
          <div class="large-6 columns">
            <label>Last Name
              <input required type="text" name="last-name" value="<?php echo $responder['lastname'] ?>"/>
            </label>
          </div>
        </div>
        <div class="row">
          <div class="large-6 columns">
            <label>Email
              <input required type="text" name="email" value="<?php echo $responder['email']; ?>"/>
            </label>
          </div>
          <div class="large-6 columns">
            <label>Phone
              <input required type="text" name="phone" value="<?php echo $responder['phone']; ?>"/>
            </label>
          </div>
        </div>
        <div class="row">
          <div class="large-12 columns">
            <label>Address
              <input required type="text" name="address" value="<?php echo $responder['address']; ?>"/>
            </label>
          </div>
        </div>
        <div class="row">
          <div class="large-6 columns">
            <label>City
              <input required type="text" name="city" value="<?php echo $responder['city']; ?>"/>
            </label>
          </div>
          <div class="large-2 columns">
            <label>State
              <select name="state">
                <option value="" hidden="hidden" selected="selected"></option>
                <?php
                foreach ($US_STATES as $abbr => $state) {
                  echo strtolower($responder['state']) == strtolower($abbr) || strtolower($responder['state']) == strtolower($state) ?
                  '<option value="'.$abbr.'" selected="selected">'.ucwords(strtolower($state)).'</option>':
                  '<option value="'.$abbr.'">'.ucwords(strtolower($state)).'</option>';
                }
                ?>
              </select>
            </label>
          </div>
          <div class="large-4 columns">
            <label>Zip
              <input required type="text" name="zip" value="<?php echo $responder['zip']; ?>"/>
            </label>
          </div>
        </div>
        <div class="row">
          <div class="large-12 columns">
            <label>Notes
              <textarea name="notes"><?php echo $responder['notes']; ?></textarea>
            </label>
          </div>
        </div>
        <label>Status
          <select <?php if($responder['display'] == 'yes'){echo 'style="color: green;"';}else{echo 'style="color: red;"';} ?> id="status_bar" name="status">
            <option <?php if($responder['display'] == 'yes'){echo 'selected';} ?> value="yes" style="color: green;">Active</option>
            <option <?php if($responder['display'] == 'no'){echo 'selected';} ?> value="no" style="color: red;">Inactive</option>
          </select>
        </label>
      </div>
      <div id="insert-area">

      </div>
    </div>
    <br>
    <div class="row">
      <div class="large-4 columns button-group expanded" style="margin: 0 auto;">
        <a href="./dashboard.php?content=responder&id=<?php echo $id; ?>" class="button alert">Cancel</a>
        <input type="submit" class="button" value="Submit"/>
      </div>
    </div>
  </form>
  <button class="close-button" data-close="" aria-label="Close reveal"
    type="button">
    <span aria-hidden="true">×</span>
  </button>
</div>

<div id="deactivate-responder-modal" class="reveal warning callout text-center tiny" data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Deactivate Responder</h4>
  <form id="deactivate-responder-form" method="post" action="#">
    <input name="responder-id" type="hidden" value="<?php echo $id ;?>">
    <p>Are you sure?</p>
    <div class="large-4 columns button-group expanded" style="margin: 0 auto;">
      <a data-close="" class="button">Cancel</a>
      <input type="submit" class="button alert" value="Submit"/>
    </div>
  </form>
  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div id="delete-responder-modal" class="reveal warning callout text-center tiny" data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Delete Responder</h4>
  <form id="delete-responder-form" method="post" action="#">
    <input name="responder-id" type="hidden" value="<?php echo $id ;?>">
    <p>Are you sure?</p>
    <div class="large-4 columns button-group expanded" style="margin: 0 auto;">
      <a data-close="" class="button">Cancel</a>
      <input type="submit" class="button alert" value="Submit"/>
    </div>
  </form>
  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<div id="responder-success-modal" class="reveal callout success text-center tiny"
  data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Success</h4>
  <a href="dashboard.php?content=responders" class="button success">OK</a>
</div>
