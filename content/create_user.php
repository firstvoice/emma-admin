<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/11/2017
 * Time: 10:13 AM
 */

?>
<form id="create_user_form">
  <div class="title row expanded align-middle">
    <div class="columns medium-6">
      <h2 class="text-left"><a href="./dashboard.php?content=users"><i class="page-icon fa fa-user-circle"></i> Create User</a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
      <input type="submit" class="button" value="Save" style="margin:0;">
    </div>
  </div>
  <input type="hidden" id="user_id" name="user_id" value="<?php echo $USER->id; ?>"/>
  <input type="hidden" id="plan_id" name="plan-id" value="<?php echo $USER->emma_plan_id; ?>"/>
  <div class="row columns expanded">
    <div class="large-12 ">
      <div class="row expanded">
        <div class="large-12 medium-12 column">
          <div class="card-info warning">
            <div class="card-info-content">
              <div class="text-center">
                <h3 class="lead">Create New User</h3>
              </div>
              <div class="row expanded align-Left">
                <div class="large-6 medium-12 column">
                  <label>Username/Email
                    <input id="username" type="Text" name="username" required/>
                  </label>
                </div>
                <div class="large-6 columns" id="exists-container">

                </div>
              </div>
              <div class="row expanded">
                <div class="large-4 medium-12 small-12 column">
                  <label>First Name
                    <input type="Text" name="firstname" required/>
                  </label>
                </div>

                <div class="large-4 medium-12 small-12 column">
                  <label>Middle Name
                    <input type="Text" name="middlename"/>
                  </label>
                </div>

                <div class="large-4 medium-12 small-12 column">
                  <label>Last Name
                    <input type="Text" name="lastname" required/>
                  </label>
                </div>
              </div>
              <div class="row expanded">
                <div class="large-4 medium-12 column">
                  <label>Landline
                    <input type="Text" name="landline" required/>
                  </label>
                </div>
                <div class="large-4 medium-12 column">
                  <label>Mobile
                    <input type="Text" name="phone" required/>
                  </label>
                </div>

                <div class="large-4 medium-12 column">
                  <label>Title
                    <input type="Text" name="title"/>
                  </label>
                </div>
              </div>
              <div class="row expanded">
                <div class="large-4 medium-12 small-12 column">
                  <label>Address
                    <input type="Text" name="address"/>
                  </label>
                </div>

                <div class="large-4 medium-12 small-12 column">
                  <label>City
                    <input type="Text" name="city"/>
                  </label>
                </div>

                <div class="large-3 medium-12 small-12 column">
                  <label>State
                    <input type="Text" name="state"/>
                  </label>
                </div>
                <div class="large-1 medium-12 small-12 column">
                  <label>Zip
                    <input type="Text" name="zip"/>
                  </label>
                </div>
              </div>
              <div class="text-center">
                    <h3 class="lead">Assign to Groups</h3>
              </div>
              <div class="row large-12 columns">
                  <div class="large-3 medium-6 small-12 columns">
                      <div class="row">
                          <div class="small-4 column">
                              <div class="switch tiny">
                                  <input class="switch-input all-select" id="all-select" type="checkbox" >
                                  <label class="switch-paddle" for="all-select">
                                      <span class="show-for-sr">All</span>
                                  </label>
                              </div>
                          </div>
                          <div class="small-8 column">
                              <label class="text-left">All</label>
                          </div>
                      </div>
                  </div>
                <?php
                $allgroups = select_createUser_allgroups($USER->emma_plan_id);
//                    $fvmdb->query("
//                                SELECT g.*
//                                FROM emma_groups AS g
//                                WHERE g.emma_plan_id = " . $USER->emma_plan_id . "
//                              ");
                while ($allgroup = $allgroups->fetch_assoc()) {
                  echo '
                                <div class="large-3 medium-6 small-12 columns">
                                    <div class="row"> 
                                        <div class="small-4 column">
                                            <div class="switch tiny">
                                                <input class="switch-input group-toggle" name="group-ids[]" value="' .
                    $allgroup['emma_group_id'] . '" id="group-id-' .
                    $allgroup['emma_group_id'] . '" type="checkbox" >
                                                <label class="switch-paddle" for="group-id-' .
                    $allgroup['emma_group_id'] . '">
                                                    <span class="show-for-sr">' .
                    $allgroup['name'] . '</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="small-8">
                                          <label class="text-left">' .
                    $allgroup['name'] . '</label>
                                        </div>
                                    </div>
                                </div>';

                }
                ?>

              </div>
            </div>
          </div>
        </div>
      </div><!-- card row end -->
    </div>
  </div>
</form>
<div id="success_modal" class="reveal callout success text-center tiny"
  data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Success</h4>
  <a href="./dashboard.php?content=users" data-close class="button success">Ok</a>
<!--  <button class="close-button" data-close aria-label="Close reveal"-->
<!--    type="button">-->
<!--    <span aria-hidden="true">&times;</span>-->
<!--  </button>-->
</div>

<div id="add-user-modal" class="reveal callout text-center small"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Add User</h4>
    <form id="add-user-form">
        <div class="row">
            <div class="large-6 columns">
                <label>Username</label>
                <input type="text" id="add-user-username" value="" disabled>
                <input type="hidden" id="add-user-id" name="user-id" value="">
                <input type="hidden" id="add-plan-id" name="plan-id" value="<?php echo $USER->emma_plan_id;?>">
            </div>
        </div>
        <label>Groups</label>
        <div class="row">
            <?php
            $allgroups = select_createUser_allgroups($USER->emma_plan_id);
            //                    $fvmdb->query("
            //                                SELECT g.*
            //                                FROM emma_groups AS g
            //                                WHERE g.emma_plan_id = " . $USER->emma_plan_id . "
            //                              ");
            while ($allgroup = $allgroups->fetch_assoc()) {
                echo '
                                <div class="large-3 medium-6 small-12 columns">
                                    <div class="row"> 
                                        <div class="small-4 column">
                                            <div class="switch tiny">
                                                <input class="switch-input modal-group-toggle" name="group-ids[]" value="' .
                    $allgroup['emma_group_id'] . '" id="modal-group-id-' .
                    $allgroup['emma_group_id'] . '" type="checkbox" >
                                                <label class="switch-paddle" for="modal-group-id-' .
                    $allgroup['emma_group_id'] . '">
                                                    <span class="show-for-sr">' .
                    $allgroup['name'] . '</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="small-8">
                                          <label class="text-left">' .
                    $allgroup['name'] . '</label>
                                        </div>
                                    </div>
                                </div>';

            }
            ?>
        </div>
        <a class="button alert" data-close>Cancel</a>
        <input type="submit" class="button" value="Submit">
    </form>
</div>

<div id="reactivate-user-modal" class="reveal callout text-center small"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Reactivate User</h4>
    <form id="reactivate-user-form">
        <span style="padding: 10px">Are you sure?</span>
        <div class="text-center small-12">
            <input type="hidden" id="reactivate-user-id" name="selected_users" value="">
            <a class="button alert" data-close>Cancel</a>
            <input type="submit" class="button" value="Submit">
        </div>
    </form>
</div>
