
<form action="#" method="post" id="forgot-password">
    <div class="row align-center">
        <div class="large-6 medium-12 small-12 align-center column">
            <div class="row text-center align-center" style="margin-top: 15%">
                <div class="large-8 medium-10 small-12 columns">
                    <h3>Forgot Password</h3>
                </div>
            </div>
            <div class="row align-center">
                <div class="large-8 medium-10 small-12 columns">
                    <label>Username/Email<span style="color: red;"> *</span></label>
                    <input type="text" name="current-email" id="current-email" placeholder="username@domain.com"/>
                </div>
            </div>
            <div class="row text-center align-center">
                <div class="large-8 medium-10 small-12 columns">
                    <input class="button expanded" type="submit" name="submit" id="submit" value="Send Email"/>
                </div>
            </div>
        </div>
    </div>
</form>


