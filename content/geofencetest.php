<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 2/26/2020
 * Time: 9:12 AM
 */

?>


<!DOCTYPE html>
<html>
<head>
    <title>Drawing Tools</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        .over_map {
            position: absolute;
            top: 2%;
            left: 35%;
            z-index: 1;
            max-height: 325px;
            overflow: auto;
            /*background-color: #ccc;*/
        }
        .toolsHidden {
            display: none;
        }
        .gfInfoHidden {
            display: none;
        }

        .gfInfoHidden {
            display: none;
        }
        .errorMsg {
            display: none;
            color: red;
            font-size: smaller;
        }

        /*.recCirc {
          width: 37px;
          height: 28px;
          background: url(iconsss/circle-regular.svg) no-repeat top left;
        }*/
    </style>
</head>
<body>
<div id="map"></div>
<div class="over_map">
    <button id="geoFence">Create Geofenced Area</button>

    <div id="gfInfo" class="gfInfoHidden">
        <input type="text" id="gfName" name="Geofenced Name" placeholder="Geofence Area Name">
        <button id="save">Save</button>
    </div>

    <br>
    <div id="tools" class="toolsHidden">
        <!-- <button id="rec">Draw Rec</button> -->
        <button id="rec2">Draw RecCircle</button>
        <button id="cir">Draw Circle</button>
    </div>
    <p id="nameGFCheck" class="errorMsg">Please enter a name for the geofence area</p>
    <p id="shapeGFCheck" class="errorMsg">Please draw at least 1 shape</p>
</div>
<script>
    // This example requires the Drawing library. Include the libraries=drawing
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=drawing">
    var map;
    let json = {
        gfName: "",
        circles: [],
        // rectangles:[],
        circleObjects:[],
        infoWindowsCircles:[],
        recObjects: [],
        infoWindowsRec: []
    };
    var drawingManager;
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -34.397, lng: 150.644},
            zoom: 14
        });
    }

    document.getElementById("save").addEventListener("click", function(){
        let gfName = document.getElementById("gfName").value;
        if(gfName !== null && gfName !== undefined && gfName !== "") {
            json.gfName = gfName;

            if (json.circleObjects.length > 0 || json.recObjects.length > 0){
                console.log(json);

                // Close drawing manager
                if(drawingManager) {
                    drawingManager.setMap(null);
                }
                document.getElementById("gfInfo").style.display = "none";
                document.getElementById("tools").style.display = "none";
                document.getElementById("geoFence").style.display = "block";
                // json = {
                //   gfName: "",
                //   circles: [],
                //   circleObjects:[],
                //   recObjects: []
                // };
            } else {
                document.getElementById("shapeGFCheck").style.display = "block";
                setTimeout(function(){
                    document.getElementById("shapeGFCheck").style.display = "none";
                }, 3000);
            }

        } else {
            document.getElementById("nameGFCheck").style.display = "block";
            setTimeout(function(){
                document.getElementById("nameGFCheck").style.display = "none";
            }, 3000);
        }



        // for(var i = 0;i < json.circleObjects.length; i++) {
        //   json.circleObjects[i].setOptions({editable:false});
        //   json.circleObjects[i].setOptions({draggable:false});
        // }

        // for(var i = 0;i < json.recObjects.length; i++) {
        //   json.recObjects[i].setOptions({editable:false});
        //   json.recObjects[i].setOptions({draggable:false});
        // }


    });

    document.getElementById("geoFence").addEventListener("click", function(){
        // drawCir();
        document.getElementById("tools").style.display = "block";
        document.getElementById("gfInfo").style.display = "block";
        document.getElementById("geoFence").style.display = "none";
    });

    // document.getElementById("rec").addEventListener("click", function(){
    //   drawRec();
    // });
    document.getElementById("rec2").addEventListener("click", function(){
        drawRecWithCircles();
    });
    document.getElementById("cir").addEventListener("click", function(){
        drawCir();
    });
</script>

<script src="../js/circlesRectangles.js"></script>
<script src="../js/rectangle.js"></script>
<script src="../js/circles.js"></script>
<script src="../js/create_geofence_new.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDS_cKQ5EnelY2rUO6JOIQKOeyil5b7enw&libraries=drawing&callback=initMap"
        async defer></script>
</body>
</html>

<!--Old Version-->

<!--<!DOCTYPE html>-->
<!--<html>-->
<!--<head>-->
<!--    <title>Drawing Tools</title>-->
<!--    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">-->
<!--    <meta charset="utf-8">-->
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">-->
<!--    <style>-->
<!--        /* Always set the map height explicitly to define the size of the div-->
<!--         * element that contains the map. */-->
<!--        #map {-->
<!--            height: 100%;-->
<!--        }-->
<!--        /* Optional: Makes the sample page fill the window. */-->
<!--        html, body {-->
<!--            height: 100%;-->
<!--            margin: 0;-->
<!--            padding: 0;-->
<!--        }-->
<!--        .over_map {-->
<!--            position: absolute;-->
<!--            top: 2%;-->
<!--            left: 35%;-->
<!--            z-index: 1;-->
<!--            max-height: 325px;-->
<!--            overflow: auto;-->
<!--            /*background-color: #ccc;*/-->
<!--        }-->
<!--        .toolsHidden {-->
<!--            display: none;-->
<!--        }-->
<!--        .gfInfoHidden {-->
<!--            display: none;-->
<!--        }-->
<!---->
<!--        .gfInfoHidden {-->
<!--            display: none;-->
<!--        }-->
<!--        .errorMsg {-->
<!--            display: none;-->
<!--            color: red;-->
<!--            font-size: smaller;-->
<!--        }-->
<!---->
<!--        /*.recCirc {-->
<!--          width: 37px;-->
<!--          height: 28px;-->
<!--          background: url(iconsss/circle-regular.svg) no-repeat top left;-->
<!--        }*/-->
<!--    </style>-->
<!--</head>-->
<!--<body>-->
<!--<div id="map" style="width: 75%; height: 60%;"></div>-->
<!--<div id="output" style="width: 75%; height: 60%;"></div>-->
<!--<div class="over_map">-->
<!--    <button id="geoFence">Create Geofenced Area</button>-->
<!---->
<!--    <div id="gfInfo" class="gfInfoHidden">-->
<!--        <input type="text" id="gfName" name="Geofenced Name" placeholder="Geofence Area Name">-->
<!--        <button id="save">Save</button>-->
<!--    </div>-->
<!---->
<!--    <br>-->
<!--    <div id="tools" class="toolsHidden">-->
<!--<!--        <button id="rec">Draw Rec</button>-->-->
<!--        <button id="rec2">Draw RecCircle</button>-->
<!--        <button id="cir">Draw Circle</button>-->
<!--    </div>-->
<!--    <p id="nameGFCheck" class="errorMsg">Please enter a name for the geofence area</p>-->
<!--    <p id="shapeGFCheck" class="errorMsg">Please draw at least 1 shape</p>-->
<!--</div>-->
<!--<script>-->
<!--    // This example requires the Drawing library. Include the libraries=drawing-->
<!--    // parameter when you first load the API. For example:-->
<!--    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=drawing">-->
<!--    var map;-->
<!--    let json = {-->
<!--        gfName: "",-->
<!--        circles: [],-->
<!--        // rectangles:[],-->
<!--        circleObjects:[],-->
<!--        recObjects: []-->
<!--    };-->
<!--    var drawingManager;-->
<!--    function initMap() {-->
<!--        map = new google.maps.Map(document.getElementById('map'), {-->
<!--            center: {lat: 41.9779, lng: -91.6656},-->
<!--            zoom: 14-->
<!--        });-->
<!--    }-->
<!---->
<!--    document.getElementById("save").addEventListener("click", function(){-->
<!--        let gfName = document.getElementById("gfName").value;-->
<!--        if(gfName !== null && gfName !== undefined && gfName !== "") {-->
<!--            json.gfName = gfName;-->
<!---->
<!--            if (json.circleObjects.length > 0 || json.recObjects.length > 0){-->
<!--                console.log(json);-->
<!--                for(var i=0; i < json.circles.length; i++) {-->
<!--                    document.getElementById("output").append(i+1 + ": " + json.circles[i].centerLat + " " + json.circles[i].centerLng + " " + json.circles[i].radiusMeters + "  ////  ");-->
<!--                }-->
<!---->
<!--                // Close drawing manager-->
<!--                if(drawingManager) {-->
<!--                    drawingManager.setMap(null);-->
<!--                }-->
<!--                document.getElementById("gfInfo").style.display = "none";-->
<!--                document.getElementById("tools").style.display = "none";-->
<!--                document.getElementById("geoFence").style.display = "block";-->
<!--                // json = {-->
<!--                //   gfName: "",-->
<!--                //   circles: [],-->
<!--                //   circleObjects:[],-->
<!--                //   recObjects: []-->
<!--                // };-->
<!--            } else {-->
<!--                document.getElementById("shapeGFCheck").style.display = "block";-->
<!--                setTimeout(function(){-->
<!--                    document.getElementById("shapeGFCheck").style.display = "none";-->
<!--                }, 3000);-->
<!--            }-->
<!---->
<!--        } else {-->
<!--            document.getElementById("nameGFCheck").style.display = "block";-->
<!--            setTimeout(function(){-->
<!--                document.getElementById("nameGFCheck").style.display = "none";-->
<!--            }, 3000);-->
<!--        }-->
<!---->
<!---->
<!---->
<!--        // for(var i = 0;i < json.circleObjects.length; i++) {-->
<!--        //   json.circleObjects[i].setOptions({editable:false});-->
<!--        //   json.circleObjects[i].setOptions({draggable:false});-->
<!--        // }-->
<!---->
<!--        // for(var i = 0;i < json.recObjects.length; i++) {-->
<!--        //   json.recObjects[i].setOptions({editable:false});-->
<!--        //   json.recObjects[i].setOptions({draggable:false});-->
<!--        // }-->
<!---->
<!---->
<!--    });-->
<!---->
<!--    document.getElementById("geoFence").addEventListener("click", function(){-->
<!--        // drawCir();-->
<!--        document.getElementById("tools").style.display = "block";-->
<!--        document.getElementById("gfInfo").style.display = "block";-->
<!--        document.getElementById("geoFence").style.display = "none";-->
<!--    });-->
<!---->
<!--    // document.getElementById("rec").addEventListener("click", function(){-->
<!--    //     drawRec();-->
<!--    // });-->
<!--    document.getElementById("rec2").addEventListener("click", function(){-->
<!--        drawRecWithCircles();-->
<!--    });-->
<!--    document.getElementById("cir").addEventListener("click", function(){-->
<!--        drawCir();-->
<!--    });-->
<!--</script>-->
<!---->
<!--<script src="circles.js"></script>-->
<!--<script src="circlesRectangles.js"></script>-->
<!--<!--<script src="rectangle.js"></script>-->-->
<!--<script src="script.js"></script>-->
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDS_cKQ5EnelY2rUO6JOIQKOeyil5b7enw&libraries=drawing&callback=initMap"-->
<!--        async defer></script>-->
<!--</body>-->
<!--</html>-->
