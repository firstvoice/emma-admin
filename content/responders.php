<?php

if (!($USER->privilege->security || $USER->privilege->admin)) {
  redirect();
}

?>

<div class="title row expanded align-middle">
  <div class="columns medium-2">
    <h2 class="text-left"><a
        href="./dashboard.php?content=responders"><i class="page-icon fa fa-briefcase"></i> <?php echo ucwords($content); ?></a>
    </h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
    <?php if(!empty($USER->fvm_id)) { ?>
    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
      data-options="disableHover:true;clickOpen:true;">
      <li>
        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <ul class="menu">
          <li><a href="#" data-open="create-responder-modal">Create Responder</a></li>
        </ul>
      </li>
    </ul>
    <?php } ?>
  </div>
</div>
<table id="responders-table" class="data-table" style="width:100%">
  <thead>
  <tr>
    <th class="text-search">Organization</th>
    <th class="text-search">Location</th>
    <th class="text-search">Name</th>
    <th class="text-search">Class</th>
    <th class="text-search">Status</th>
  </tr>
  <tr>
    <th>Organization</th>
    <th>Location</th>
    <th>Name</th>
    <th>Class</th>
    <th>Status</th>
  </tr>
  </thead>
  <tbody>
  </tbody>
  <tfoot>
  </tfoot>
</table>

<?php if(!empty($USER->fvm_id)) { ?>
<div id="create-responder-modal" class="reveal callout text-center small"
  data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Create Responder</h4>
  <hr>
  <form id="create-aed-form" class="create-aed-form text-left" action="#"
    method="post">
    <input type="hidden" name="user-id" value="<?php echo $USER->id; ?>"/>
    <div class="row">
      <div class="large-12 columns">
      </div>
    </div>
  </form>
  <button class="close-button" data-close="" aria-label="Close reveal"
    type="button">
    <span aria-hidden="true">×</span>
  </button>
</div>
<?php } ?>