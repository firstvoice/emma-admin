<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 11/15/2017
 * Time: 11:11 AM
 */
$id = $fvmdb->real_escape_string($_GET['id']);
$drills = select_drill_drills($id);
//    $fvmdb->query(sprintf("
//    SELECT e.*, et.name AS type, et.img_filename AS type_filename, es.emma_site_id, es.emma_site_name, CONCAT(u.firstname, ' ', u.lastname) AS reported_by, u.id AS user_id, u.username AS user_email, u.phone AS user_phone, est.name AS sub_type_name, u.emma_pin AS user_pin, e.created_pin
//    FROM emergencies e
//    JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
//    JOIN emergency_sub_types est ON e.emergency_sub_type_id = est.emergency_sub_type_id
//    JOIN emma_sites es ON e.emma_site_id = es.emma_site_id
//    JOIN users u ON e.created_by_id = u.id
//    WHERE emergency_id = %s
//", $id));
if ($drill = $drills->fetch_assoc()) { ?>
  <div class="title row expanded align-middle">
    <div class="columns medium-2">
      <h2 class="text-left"><i class="page-icon fa fa-clock-o"></i> <?php echo ucwords($content); ?></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
      <!--      <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu-->
      <!--          data-options="disableHover:true;clickOpen:true;">-->
      <!--        <li>-->
      <!--          <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>-->
      <!--          <ul class="menu">-->
      <!--            <li><a href="./dashboard.php?content=map&id=--><?php //echo $id; ?><!--">View Map</a></li>-->
      <!--            <li><a href="./dashboard.php?content=drills">Close Event</a></li>-->
      <!--            <li><a href="*" data-open="broadcast-drill-modal">Broadcast Event</a></li>-->
      <!--          </ul>-->
      <!--        </li>-->
      <!--      </ul>-->
    </div>
  </div>
  <script type="text/javascript">
    let mainEvent = <?php echo json_encode($drill); ?>;
  </script>
  <div class="row expanded">
    <div class="large-6 medium-12 small-12 columns">
      <div class="card-info primary">
        <div class="card-info-content">
          <div class="row">
            <div class="large-6 medium-12 small-12 columns">
              <h3 class="lead">Details</h3>
              <p>Event Status: <?php echo $drill['active'] == 1 ? 'Active' : 'Closed'; ?></p>
              <p>Event PIN
                Used: <?php echo($drill['created_pin'] == $drill['user_pin'] ?
                  '<span style="color:green;">Valid</span>' : '<span style="color:red;">Invalid</span>') ?></p>
              <p>Original Alert
                Reported: <?php echo date('n/j/Y g:i:s a', strtotime($drill['created_date'])); ?></p>
              <p>Site:
                <a
                  href="./dashboard.php?content=site&id=<?php echo $drill['emma_site_id']; ?>"><?php echo $drill['emma_site_name']; ?></a>
              </p>
              <p>Reported By: <?php echo $drill['reported_by']; ?></p>
              <p>User Email: <?php echo $drill['user_email']; ?></p>
              <p>User Phone: <?php echo $drill['user_phone']; ?></p>
              <p>Description: <?php echo $drill['description']; ?></p>
              <p>Comments: <?php echo $drill['comments']; ?></p>
            </div>
            <div class="large-6 medium-12 small-12 columns">
              <div class="text-center" style="margin:2em;padding:1em;border:1px solid black">
                <h5><?php echo $drill['type']; ?></h5>
                <h6><?php echo $drill['sub_type_name']; ?></h6>
                <img src="img/<?php echo $drill['type_filename']; ?>" alt="Event Type Icon"/>
              </div>
              <h3 class="lead">Actions</h3>
              <form action="/dashboard.php" method="get">
                <input type="hidden" name="content" value="map"/>
                <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                <a href="./dashboard.php?content=map&id=<?php echo $id; ?>" class="button expanded">View
                  Map</a>
              </form>
              <button data-open="close-drill-modal" class="button expanded" <?php if ($drill['active'] == '0') echo 'disabled'; ?>>Close
                Event
              </button>
              <button data-open="broadcast-drill-modal"
                class="button expanded" <?php if ($drill['active'] == '0') echo 'disabled'; ?>>
                Broadcast
                Event
              </button>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <h3 class="lead">Alert Sent To: </h3>
              <div class="row">
                <?php
                $received = array();
                $receivedGroups = select_drills_eventGroups($drill['emergency_id']);
//                    $fvmdb->query("
//                                    SELECT rg.emma_group_id
//                                    FROM emergency_received_groups rg
//                                    WHERE rg.emergency_id = '" . $drill['emergency_id'] . "'
//                                ");
                while ($receivedGroup = $receivedGroups->fetch_assoc()) {
                  $received[$receivedGroup['emma_group_id']] = $receivedGroup['emma_group_id'];
                }
                $groups = select_drills_groups($USER->emma_plan_id);
//                    $fvmdb->query("
//                                    SELECT *
//                                    FROM emma_groups
//                                    WHERE emma_plan_id = '" . $USER->emma_plan_id . "'
//                                ");
                $continue = true;
                while ($group = $groups->fetch_assoc()) {
                  echo '
                                      <div class="large-4 medium-12 small-12 columns">
                                      <div class="row">
                                        <div class="small-3 column">
                                          <div class="switch tiny">
                                            <input class="switch-input" id="group-' . $group['emma_group_id'] .
                    '" type="checkbox" ' . (in_array($group['emma_group_id'], $received) ? 'checked' : '') . ' disabled>
                                            <label class="switch-paddle" for="group-' . $group['emma_group_id'] . '">
                                              <span class="show-for-sr">' . $group['name'] . '</span>
                                            </label>
                                          </div>
                                        </div>
                                        <div class="small-9 column">
                                          <label class="text-left">' . $group['name'] . '</label>
                                        </div>
                                      </div>
                                      </div>
                                    ';
                }
                ?>
              </div>
            </div>
          </div>
        </div>
      </div><!--/ Details -->
      <div class="card-info warning">
        <div class="card-info-content">
          <h3 class="lead">Current User Status</h3>
          <div id="user-responses">
            <div data-open="need-help-users-modal" id="total-need-help"
              class="callout alert-callout-border alert">
              <strong>Need Help </strong><span>0 Users</span>
            </div>
            <div data-open="need-info-users-modal" id="total-need-info"
              class="callout alert-callout-border warning">
              <strong>Information </strong><span>0 Users</span>
            </div>
            <div data-open="need-validation-users-modal" id="total-need-validation"
              class="callout alert-callout-border purple">
              <strong>Need Validation </strong><span>0 Users</span>
            </div>
            <div data-open="pending-users-modal" id="total-pending"
              class="callout alert-callout-border secondary">
              <strong>Pending </strong><span>0 Users</span>
            </div>
            <div data-open="not-received-users-modal" id="total-not-received"
              class="callout alert-callout-border primary">
              <strong>Not Received </strong><span>0 Users</span>
            </div>
            <div data-open="all-clear-users-modal" id="total-all-clear"
              class="callout alert-callout-border success">
              <strong>All Clear </strong><span>0 Users</span>
            </div>
          </div>
        </div>
      </div><!--/ User Status -->
    </div>
    <div class="large-6 medium-12 small-12 columns">
      <div class="card-info primary">
        <div class="card-info-content">
          <h3 class="lead">Sub-Events</h3>
          <div id="sub-drills" class="text-center">
            <div class="row">
              <?php
              $alertTypes = select_drills_alertTypes($USER->emma_plan_id);
//                  $fvmdb->query("
//                select @pos:=@pos+1 as pos, et.*
//                from emma_plan_event_types pet
//                join emergency_types et on pet.emma_emergency_type_id = et.emergency_type_id
//                join (select @pos:=0) p
//                where pet.emma_plan_id = '".$USER->emma_plan_id."'
//              ");
              while($alertType = $alertTypes->fetch_assoc()) {
                echo '
                <div class="columns small-4">
                  <a href="#" class="button-badge" data-open="sub-drills-'.$alertType['badge'].'-modal">
                    <img src="img/'.$alertType['img_filename'].'" style="width:80px;height:80px;">
                    <span id="badge-'.$alertType['badge'].'" class="badge alert is-hidden">0</span>
                  </a>
                </div>
                ';
              }
              ?>
            </div>
          </div>
        </div>
      </div><!--/ Sub-Events -->
      <div class="card-info secondary">
        <div class="card-info-content">
          <h3 class="lead">Message Center</h3>
          <div id="message-center" class="max-height">

          </div>
        </div>
      </div><!--/ Message Center -->
      <div class="card-info secondary">
        <div id="drill-map" style="width:100%; height:400px;"></div>
      </div><!--/ Map -->
    </div>
  </div>
  <div id="timeline" style="position:relative;min-height:200px;">
    <h4 style="position:absolute; top:10px; left:30px; z-index: 100;">Timeline</h4></div>

  <!-- MODALS -->
  <div id="not-received-users-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4>Not Received</h4>
    <table>
      <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Mobile</th>
      </tr>
      </thead>
      <tbody>
      <?php
      $notReceivedUsers = select_drills_notReveivedUsers($USER->emma_plan_id);
//          $fvmdb->query("
//        SELECT u.id, CONCAT(u.firstname, ' ', u.lastname) AS user, u.username AS email, u.phone
//        FROM users u
//        WHERE u.emma_plan_id = '" . $USER->emma_plan_id . "'
//        AND u.display = 'yes'
//      ");
      while ($notReceivedUser = $notReceivedUsers->fetch_assoc()) {
        echo '
        <tr id="nr-' . $notReceivedUser['id'] . '">
            <td>
              <a href="./dashboard.php?content=user&id=' . $notReceivedUser['id'] . '">' . $notReceivedUser['user'] . '</a>
            </td>
            <td>' . $notReceivedUser['email'] . '</td>
            <td>' . $notReceivedUser['phone'] . '</td>
            <td>' . $notReceivedUser['phone'] . '</td>
        </tr>
        ';
      }
      ?>
      </tbody>
    </table>
    <button class="close-button" data-close="" aria-label="Close reveal" type="button">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div id="pending-users-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4>Pending</h4>
    <table>
      <thead>
      <tr>
        <th>Username</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Mobile</th>
        <th>Updated</th>
      </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
    <button class="close-button" data-close="" aria-label="Close reveal" type="button">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div id="all-clear-users-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4>All Clear</h4>
    <table>
      <thead>
      <tr>
        <th>Username</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Mobile</th>
        <th>Updated</th>
      </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
    <button class="close-button" data-close="" aria-label="Close reveal" type="button">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div id="need-help-users-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4>Need Help</h4>
    <table>
      <thead>
      <tr>
        <th>Username</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Mobile</th>
        <th>Updated</th>
      </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
    <button class="close-button" data-close="" aria-label="Close reveal" type="button">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div id="need-info-users-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4>Need Information</h4>
    <table>
      <thead>
      <tr>
        <th>Username</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Mobile</th>
        <th>Updated</th>
      </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
    <button class="close-button" data-close="" aria-label="Close reveal" type="button">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div id="need-validation-users-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4>Need Validation</h4>
    <table>
      <thead>
      <tr>
        <th>Username</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Mobile</th>
        <th>Updated</th>
        <th></th>
      </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
    <button class="close-button" data-close="" aria-label="Close reveal" type="button">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div id="close-drill-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4>Broadcast Event</h4>
    <hr>
    <form id="close-drill" action="#" method="post">
      <input name="user-id" type="hidden" value="<?php echo $USER->id; ?>"/>
      <input name="event-id" type="hidden" value="<?php echo $id; ?>"/>
      <div class="row">
        <?php
        $groups = select_drills_groups($USER->emma_plan_id);
//            $fvmdb->query("
//                    SELECT *
//                    FROM emma_groups
//                    WHERE emma_plan_id = '" . $USER->emma_plan_id . "'
//                ");
        $continue = true;
        while ($group = $groups->fetch_assoc()) {
          echo '
                      <div class="large-4 medium-12 small-12 columns">
                      <div class="row">
                        <div class="small-3 column">
                          <div class="switch tiny">
                            <input class="switch-input" id="c-group-' . $group['emma_group_id'] .
            '" type="checkbox" name="group[' . $group['emma_group_id'] . ']" value="' . $group['emma_group_id'] . '">
                            <label class="switch-paddle" for="c-group-' . $group['emma_group_id'] . '">
                              <span class="show-for-sr">' . $group['name'] . '</span>
                            </label>
                          </div>
                        </div>
                        <div class="small-9 column">
                          <label class="text-left">' . $group['name'] . '</label>
                        </div>
                      </div>
                      </div>
                    ';
        }
        ?>
      </div>
      <div class="row columns">
        <fieldset class="fieldset text-left" style="width:100%;">
          <legend>Alert Types</legend>
          <input id="alert-notification" name="alert-notification" type="checkbox" checked><label
            for="alert-notification">Notification</label>
          <input id="alert-email" name="alert-email" type="checkbox"><label
            for="alert-email">Email</label>
          <input id="alert-text" name="alert-text" type="checkbox"><label
            for="alert-text">Text</label>
        </fieldset>
      </div>
      <div class="row">
        <div class="large-12 columns">
          <label class="text-left">Description
            <textarea class="expanded" name="description"
              style="height:5rem;width:100%;min-width:100%;"><?php echo $drill['description']; ?></textarea>
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-12 columns">
          <label class="text-left">Comments
            <select id="select-close-script" style="margin-bottom:0;border-bottom:none;">
              <option value="" data-text="">-- Select Script --</option>
              <?php
              $broadcastScripts = select_drills_broadcastScripts($USER->emma_plan_id, 3);
//                  $fvmdb->query("
//                select *
//                from emma_scripts
//                where emma_script_type_id = '3'
//                and emma_plan_id = '" . $USER->emma_plan_id . "'
//              ");
              while ($broadcastScript = $broadcastScripts->fetch_assoc()) {
                echo "<option value='" . $broadcastScript['emma_script_id'] .
                  "' data-text='" . $broadcastScript['text'] . "'>" .
                  $broadcastScript['name'] . "</option>";
              }
              ?>
            </select>
            <textarea id="close-description" class="expanded" name="comments"
              style="height:5rem;width:100%;min-width:100%;"><?php echo $drill['comments']; ?></textarea>
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-3 columns"></div>
        <div class="large-6 large-centered columns">
          <input type="submit" class="button expanded" value="Close"/>
        </div>
        <div class="large-3 columns"></div>
      </div>
    </form>
    <button class="close-button" data-close="" aria-label="Close reveal" type="button">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div id="broadcast-drill-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4>Broadcast Event</h4>
    <hr>
    <form id="broadcast-drill" action="#" method="post">
      <input name="user-id" type="hidden" value="<?php echo $USER->id; ?>"/>
      <input name="event-id" type="hidden" value="<?php echo $id; ?>"/>
      <div class="row">
        <?php
        $groups = select_drills_groups($USER->emma_plan_id);
//        $fvmdb->query("
//                    SELECT *
//                    FROM emma_groups
//                    WHERE emma_plan_id = '" . $USER->emma_plan_id . "'
//                ");
        $continue = true;
        while ($group = $groups->fetch_assoc()) {
          echo '
                      <div class="large-4 medium-12 small-12 columns">
                      <div class="row">
                        <div class="small-3 column">
                          <div class="switch tiny">
                            <input class="switch-input" id="b-group-' . $group['emma_group_id'] .
            '" type="checkbox" name="group[' . $group['emma_group_id'] . ']" value="' . $group['emma_group_id'] . '">
                            <label class="switch-paddle" for="b-group-' . $group['emma_group_id'] . '">
                              <span class="show-for-sr">' . $group['name'] . '</span>
                            </label>
                          </div>
                        </div>
                        <div class="small-9 column">
                          <label class="text-left">' . $group['name'] . '</label>
                        </div>
                      </div>
                      </div>
                    ';
        }
        ?>
      </div>
      <div class="row columns">
        <fieldset class="fieldset text-left" style="width:100%;">
          <legend>Alert Types</legend>
          <input id="alert-notification" name="alert-notification" type="checkbox" checked><label
            for="alert-notification">Notification</label>
          <input id="alert-email" name="alert-email" type="checkbox" disabled><label
            for="alert-email">Email</label>
          <input id="alert-text" name="alert-text" type="checkbox" disabled><label
            for="alert-text">Text</label>
        </fieldset>
      </div>
      <div class="row">
        <div class="large-12 columns">
          <label class="text-left">Description
            <textarea class="expanded" name="description"
              style="height:5rem;width:100%;min-width:100%;"><?php echo $drill['description']; ?></textarea>
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-12 columns">
          <label class="text-left">Comments
            <select id="select-broadcast-script" style="margin-bottom:0;border-bottom:none;">
              <option value="" data-text="">-- Select Script --</option>
              <?php
              $broadcastScripts = select_drills_broadcastScripts($USER->emma_plan_id, 2);
//                  $fvmdb->query("
//                select *
//                from emma_scripts
//                where emma_script_type_id = '2'
//                and emma_plan_id = '" . $USER->emma_plan_id . "'
//              ");
              while ($broadcastScript = $broadcastScripts->fetch_assoc()) {
                echo "<option value='" . $broadcastScript['emma_script_id'] .
                  "' data-text='" . $broadcastScript['text'] . "'>" .
                  $broadcastScript['name'] . "</option>";
              }
              ?>
            </select>
            <textarea id="broadcast-description" class="expanded" name="comments"
              style="height:5rem;width:100%;min-width:100%;"><?php echo $drill['comments']; ?></textarea>
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-3 columns"></div>
        <div class="large-6 large-centered columns">
          <input type="submit" class="button expanded" value="Broadcast"/>
        </div>
        <div class="large-3 columns"></div>
      </div>
    </form>
    <button class="close-button" data-close="" aria-label="Close reveal" type="button">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <?php
  $alertTypes->data_seek(0);
  while($alertType = $alertTypes->fetch_assoc()) {
    echo '
    <div id="sub-drills-'.$alertType['badge'].'-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in"
         data-animation-out="fade-out">
      <h4>'.$alertType['name'].' Sub-Events</h4>
      <hr>
      <table>
        <thead>
        <tr>
          <th>Site</th>
          <th>Creator</th>
          <th>Time</th>
        </tr>
        </thead>
        <tbody id="'.$alertType['badge'].'-sub-drills">
        </tbody>
      </table>
      <button class="close-button" data-close="" aria-label="Close reveal" type="button">
        <span aria-hidden="true">×</span>
      </button>
    </div>
    ';
  }
  ?>
  <!--/ MODALS -->

<?php } else { ?>
  <div class="row columns">

  </div>
  <div class="callout alert">
    Could not find Event
  </div>
<?php } ?>
