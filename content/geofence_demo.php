<?php
/**
 * Created by PhpStorm.
 * User: John
 * Date: 12/6/2017
 * Time: 20:48
 */
?>

    <div class="title row expanded align-middle">
        <div class="columns medium-6">
            <h2 class="text-left"><i class="page-icon fa fa-globe"></i> Geofences</h2>
        </div>
        <div class="columns show-for-medium"></div>
        <div class="columns shrink">
        </div>
    </div>
    <div class="row expanded text-left" style="margin-top: 20px">
        <div class="large-6 medium-6 column">
            <ul>
                <li>
                    <p style="font-size: 18pt">Geofencing allows you, in a particular coordinate area, to alert and send messages to individuals that are in that specific footprint and no others. An individual out of the building, on a break, or on vacation will not receive the alert and cause any unnecessary panic or be de-sensitized to these mass notifications. Geofencing can be mapped for larger areas, such as entire districts, and then broken up into specific fields or buildings or "micro-areas" of concern. Relevant alerts are sent to the relevant areas. Relevant individuals in these areas can then respond and continue accordingly.</p>
                </li>
            </ul>
        </div>
        <div class="large-6 medium-6 column">
            <img src="img/demo_geofence.png">
        </div>
    </div>





