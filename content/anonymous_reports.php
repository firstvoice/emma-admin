
<?php

if (!($USER->privilege->security || $USER->privilege->admin)) {
    redirect();
}

?>
<div class="title row expanded align-middle">
    <div class="columns medium-4">
        <h2 class="text-left"><a href="./dashboard.php?content=anonymous_reports"><i class="page-icon fa fa-eye-slash"></i> Anonymous Reports</a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">

    </div>
</div>

<div>
    <ul class="tabs" data-tabs id="script-type-tabs">
        <li class="tabs-title is-active">
            <a href="#panel-active" aria-selected="true">Active</a>
        </li>
        <li class="tabs-title">
            <a href="#panel-closed" aria-selected="true">Closed</a>
        </li>
    </ul>
    <div class="tabs-content" data-tabs-content="script-type-tabs">
        <div id="panel-active" class="tabs-panel is-active">
            <table id="ar-active-table" class="data-table" style="width:100%">
                <thead>
                <tr>
                    <th class="text-search">Date</th>
                    <th class="text-search">Description</th>
                </tr>
                <tr>
                    <th>Date</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>
        <div id="panel-closed" class="tabs-panel">
            <table id="ar-closed-table" class="data-table" style="width:100%">
                <thead>
                <tr>
                    <th class="text-search">Date</th>
                    <th class="text-search">Description</th>
                </tr>
                <tr>
                    <th>Date</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>
    </div>
</div>