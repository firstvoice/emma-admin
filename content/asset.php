<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 11/15/2017
 * Time: 11:11 AM
 */
$id = $fvmdb->real_escape_string($_GET['id']);
$events = select_asset_events($id);
//    $fvmdb->query("
//    SELECT e.*, CONCAT(u.firstname, ' ', u.lastname) AS creator, u.id AS user_id, u.username AS user_email, u.phone AS user_phone, t.name, t.emma_asset_type_id, i.image, i.asset_icon_id
//    FROM emma_assets e
//    JOIN emma_asset_types t ON e.emma_asset_type_id = t.emma_asset_type_id
//    LEFT JOIN emma_asset_icons i ON t.emma_asset_icon_id = i.asset_icon_id
//    JOIN users u ON e.created_by_id = u.id
//    WHERE e.emma_asset_id = '".$id."'
//    ");
$asset_information = select_asset_assetInfo($id);
//    $fvmdb->query("
//SELECT eaci.date_time, eaci.info
//FROM emma_assets_custom_info as eaci
//WHERE eaci.id = '".$id."'
//");


//$assetInformation = $fvmdb->query("
//SELECT ea.longitude,ea.latitude,ea.
//FROM emma_assets as ea
//WHERE
//");

?>



<?php

if ($event = $events->fetch_assoc()) { $asset_id = $event['emma_asset_id']; $asset_type_id = $event['emma_asset_type_id'];?>
    <input readonly hidden value="<?php echo $asset_type_id?>" class="asset_type_id"/>
    <input readonly hidden value="<?php echo $asset_id?>" class="asset_id"/>
    <div class="title row expanded align-middle">
        <div class="columns medium-8">
            <h2 class="text-left"><a href="./dashboard.php?content=assets"><i class="page-icon fa fa-cube"></i> Asset</a></h2>
        </div>
        <div class="columns show-for-medium"></div>
        <div class="columns shrink">
            <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
                data-options="disableHover:true;clickOpen:true;">
                <li>
                    <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
                    <ul class="menu">
                        <?php
                        echo '<li><a data-status="'.$event['active'].'" data-typeid="'.$event['emma_asset_type_id'].'" data-id="'.$event['emma_asset_id'].'" data-type="'.$event['name'].'" data-createdDate="'.$event['created_date'].'" data-latitude="'.$event['latitude'].'" data-longitude="'.$event['longitude'].'" data-username="'.$event['username'].'" onclick="edit_asset(this);">Edit Asset</a></li>';
                        echo '<li><a data-open="delete-asset-modal">Deactivate Asset</a></li>';
                        echo '<li><a data-open="remove-asset-modal">Delete Asset</a></li>'
                        ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="row expanded">
        <div class="large-4 medium-6 small-12 columns">
            <div class="card-info primary">
                <div class="card-info-content">
                    <div class="row">
                        <div class="large-12 medium-12 small-12 columns">
                            <h3 class="lead">Details</h3>
                            <p>Asset Type: <?php echo $event['name']; ?></p>
                            <p>Created By: <a href="./dashboard.php?content=user&id=<?php echo $event['user_id']; ?>"><?php echo $event['creator']; ?></a></p>
                            <p>Creator Email: <a href="mailto:<?php echo $event['user_email']; ?>"><?php echo $event['user_email']; ?></a></p>
                            <p>Creator Phone: <?php echo format_telephone($event['user_phone']); ?></p>
                            <P>Latitude: <?php echo $event['latitude'];?>&nbsp;&nbsp;&nbsp;&nbsp; Longitude: <?php echo $event['longitude'];?></P>
                            <p>Address: <?php echo $event['address'];?></p>
                        </div>
                    </div>
                </div>
            </div><!--/ Details -->
        </div>
        <div class="large-8 medium-6 small-12 columns">
            <div class="card-info primary">
                <div class="card-info-content">
                    <div id="asset-map" style="width:100%; height:400px;"></div>
                </div>
            </div><!--/ Details -->
        </div>
    </div>
    <script type="text/javascript">
        var mainEvent = <?php echo json_encode($event); ?>;
    </script>


<?php } else { ?>
    <div class="row columns">

    </div>
    <div class="callout alert">
        Could not find Asset
    </div>
<?php } ?>

<div id="edit-asset-modal" class="reveal callout text-center small"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Edit Asset</h4>
    <hr>
    <form id="edit-asset-form" class="edit-asset-form text-left" action="#"
          method="post">
        <input type="hidden" name="user-id" value="<?php echo $USER->id; ?>"/>
        <input type="hidden" name="plan-id"
               value="<?php echo $USER->emma_plan_id; ?>"/>
        <input type="hidden" name="id" value="<?php echo $asset_id?>"/>
        <div class="row">
            <div class="large-12 columns">
                <div class="row">
                <div class="large-12 columns">
                <label>Asset
                    <select required name="type-id" id="edit_asset_form_type_id">
                        <?php
                        $types = select_asset_types($USER->emma_plan_id);
//                            $fvmdb->query("
//              SELECT *
//              FROM emma_asset_types
//              where emma_plan_id = '" . $USER->emma_plan_id . "'
//              and active = 1
//              ORDER BY name
//            ");
                        while ($type = $types->fetch_assoc()) {
                            if($type['emma_asset_type_id'] == $asset_type_id)
                            {
                                echo '<option selected value="' . $type['emma_asset_type_id'].'">'.$type['name'].'</option>';
                            }
                            else{
                            echo '<option value="' . $type['emma_asset_type_id'] . '">' .
                                $type['name'] . '</option>';
                            }
                        }
                        ?>
                    </select>
                </label>
                </div>
                </div>

                <div class="row">
                    <div class="large-6 columns">
                        <label>Latitude
                            <input required type="number" id="entered_lat" name="latitude" step="any" value="<?php echo $event['latitude'] ?>"/>
                        </label>
                    </div>
                    <div class="large-6 columns">
                        <label>Longitude
                            <input required type="number" id="entered_lng" name="longitude" step="any" value="<?php echo $event['longitude'] ?>"/>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <label>Address
                            <input required type="text" name="address" value="<?php echo $event['address'] ?>"/>
                        </label>
                    </div>
                </div>
                <label>Status
                    <select <?php if($event['active'] == 1){echo 'style="color: green;"';}else{echo 'style="color: red;"';} ?> id="status_bar" name="status">
                        <option <?php if($event['active'] == 1){echo 'Selected';} ?> value="Current" style="color: green;">Active</option>
                        <option <?php if($event['active']==0){echo 'Selected';} ?> value="Inactive" style="color: red;">Inactive</option>
                    </select>
                </label>
            </div>
            <div id="insert-area">

            </div>
        </div>
        <div class="large-12 columns">
        <div id="edit-map" style="width: 100%;height: 400px;"></div>
        </div>
        <br>
        <div class="row">
            <div class="large-4 columns button-group expanded" style="margin: 0 auto;">
                <a href="./dashboard.php?content=assets" class="button alert">Cancel</a>
                <input type="submit" class="button" value="Submit"/>
            </div>
        </div>
    </form>
    <button class="close-button" data-close="" aria-label="Close reveal"
            type="button">
        <span aria-hidden="true">×</span>
    </button>
</div>

<div id="delete-asset-modal" class="reveal warning callout text-center tiny" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Deactivate Asset</h4>
    <form id="delete-asset-form" method="post" action="#">
        <input name="asset-id" type="hidden" value="<?php echo $id ;?>">
        <p>Are you sure?</p>
        <div class="large-4 columns button-group expanded" style="margin: 0 auto;">
            <a data-close="" class="button">Cancel</a>
            <input type="submit" class="button alert" value="Submit"/>
        </div>
    </form>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div id="remove-asset-modal" class="reveal warning callout text-center tiny" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Delete Asset</h4>
    <form id="remove-asset-form" method="post" action="#">
        <input name="asset-id" type="hidden" value="<?php echo $id ;?>">
        <p>Are you sure?</p>
        <div class="large-4 columns button-group expanded" style="margin: 0 auto;">
            <a data-close="" class="button">Cancel</a>
            <input type="submit" class="button alert" value="Submit"/>
        </div>
    </form>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div id="asset-success-delete-modal" class="reveal callout success text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <a href="dashboard.php?content=assets" class="button success">OK</a>
</div>
