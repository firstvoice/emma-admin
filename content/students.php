<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 9/23/2019
 * Time: 2:25 PM
 */

$plans = select_plan_from_planID($USER->emma_plan_id);
//$fvmdb->query("
//  select *
//  from emma_plans
//  where emma_plan_id = '" . $USER->emma_plan_id . "'
//");
$plan = $plans->fetch_assoc();


if (!($USER->privilege->admin)) {
    redirect();
}

?>


<div class="title row expanded align-middle">
  <div class="columns medium-4">
    <h2 class="text-left"><a href="./dashboard.php?content=students"><i class="page-icon fa fa-user"></i> <?php echo ucwords(str_replace('_',' ',$content)); ?></a></h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">

  </div>
</div>
<form id="approve-user-form" method="post">
    <table class="data-table" style="width:100%">
      <colgroup>

      </colgroup>
      <thead>
      <tr>
        <th>Student Name</th>
        <th>Parent/Guardian 1</th>
        <th>Parent/Guardian 2</th>
      </tr>
      </thead>
      <tbody>
      <?php
        $rawusers = select_students_rawUsers($USER->emma_plan_id);
//            $fvmdb->query("
//        SELECT r.*, CONCAT(r.firstname, ' ', r.lastname) AS name
//        FROM users r
//        WHERE r.emma_plan_id = '". $USER->emma_plan_id ."'
//        AND r.student = 1
//        ");
        while($rawu = $rawusers->fetch_assoc()){
            $parentnames = array();
            $parents = select_students_parents($rawu['id']);
//                $fvmdb->query("
//                SELECT CONCAT(u.firstname, ' ', u.lastname) AS name
//                FROM emma_students_to_guardians e
//                LEFT JOIN users u ON e.guardian_id = u.id
//                WHERE e.student_id = '". $rawu['id'] ."'
//            ");
            while($parent = $parents->fetch_assoc()){
                $parentnames[] = $parent['name'];
            }
            if($parentnames[0] !== NULL) {
                if($parentnames[1] !== NULL) {
                    echo '
                    <tr>
                        <td>' . $rawu['name'] . '</td>
                        <td>' . $parentnames[0] . '</td>
                        <td>' . $parentnames[1] . '</td>           
                    </tr>
                    ';
                }else{
                    echo '
                    <tr>
                        <td>' . $rawu['name'] . '</td>
                        <td>' . $parentnames[0] . '</td>
                        <td><a class="select-parent" data-open="select-parent-modal" data-student="'. $rawu['id'] .'">None</a></td>           
                    </tr>
                    ';
                }
            }else{
                echo '
                <tr>
                    <td>' . $rawu['name'] . '</td>
                    <td><a class="select-parent" data-student="'. $rawu['id'] .'">None</a></td>
                    <td><a class="select-parent" data-student="'. $rawu['id'] .'">None</a></td>           
                </tr>
                ';
            }
        }
      ?>
      </tbody>
      <tfoot>

      </tfoot>
    </table>
</form>