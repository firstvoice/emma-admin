<?php
//include('../js/user_home.js');


$multi_plan = [];
$multi_group = [];
$selectPlans = select_userhome_selectPlans($USER->id);
//    $fvmdb->query("
//        SELECT mp.plan_id, p.name
//        FROM emma_plans p
//        LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id
//        WHERE mp.user_id = '" . $USER->id . "'
//        AND p.date_expired > NOW()
//                                ");

while($plan = $selectPlans->fetch_assoc())
{
    array_push($multi_plan,$plan['plan_id']);
}

$groups = select_groups_with_userID($USER->id);
while($group = $groups->fetch_assoc()){
    array_push($multi_group,$group['emma_group_id']);
}

$events = select_userhome_events($multi_plan, $multi_group);
//    $fvmdb->query("
//    SELECT e.*, et.name AS type, et.badge, et.img_filename, es.emma_site_name, es.emma_site_notes, est.name as sub_name
//    FROM emergencies e
//    JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
//    JOIN emergency_sub_types est on est.emergency_sub_type_id = e.emergency_sub_type_id
//    JOIN emma_sites es ON es.emma_site_id = e.emma_site_id
//    WHERE e.active = '1'
//    AND e.parent_emergency_id IS NULL
//    AND e.emma_plan_id IN (".implode(',',$multi_plan).")
//    ");

//Select all AED locations.
$aeds = select_userHome_aeds();
//    $fvmdb->query("
//SELECT a.latitude,a.longitude,a.location,a.sitecoordinator,c.name as contact_name, c.company, loc.address
//FROM aeds a
//JOIN location_contacts lc on lc.location_id = a.location_id
//JOIN contacts c on c.id = lc.contact_id
//JOIN locations as loc on loc.id = a.location_id
//where a.current = 1
//");
$police_calls = select_userhome_policeCalls($multi_plan);
//    $fvmdb->query("
//SELECT ecl.latitude, ecl.longitude, ecl.call_datetime, CONCAT(u.firstname, ' ', u.lastname) as user_name
//FROM emma_911_call_log as ecl
//JOIN users u on ecl.userid = u.id
//WHERE ecl.plan_id IN (".implode(',',$multi_plan).")
//AND ecl.active = 1
//");

$currentplan = get_emma_plan($USER->emma_plan_id);
//$currentplan = $currentplans->fetch_assoc();

echo '<script>';
echo 'let police_calls = [];';
echo 'let positions = [];';
echo 'let assets = [];';
while($aed = $aeds->fetch_assoc())
{
    //If the name of the location contains valid characters and isn't empty.
    if(!preg_match('/[^A-Za-z0-9]/', $aed['location']) && !empty($aed['location']))
    {
        if((!empty($aed['latitude']) && !empty($aed['longitude'])) && ($aed['latitude'] != "0" && $aed['longitude'] != "0") && (!empty($aed['location'])))
        {
            echo 'var pos = {lat: '.$aed['latitude'].', lng: '.$aed['longitude'].', location: "'.$aed['location'].'", coordinator: "'.$aed['sitecoordinator'].'", contact: "'.$aed['contact_name'].'", company: "'.$aed['company'].'", address: "'.$aed['address'].'"};';
            echo 'positions.push(pos);';
        }
    }
}
while($police_call = $police_calls->fetch_assoc())
{

    if(!empty($police_call['latitude'] && !empty($police_call['longitude'])))
    {
        echo 'var call = {lat: '.$police_call['latitude'].', lng: '.$police_call['longitude'].', username: "'.$police_call['user_name'].'", calldate: "'.$police_call['call_datetime'].'"};';
        echo 'police_calls.push(call);';
    }
}
echo '</script>';

//Select all ASSET locations.
$assets = select_userhome_assets($multi_plan);
//    $fvmdb->query("
//SELECT eat.name, a.latitude as lat, a.longitude as lng, a.address, eai.image
//FROM emma_assets a
//LEFT JOIN emma_asset_types as eat on eat.emma_asset_type_id = a.emma_asset_type_id
//LEFT JOIN emma_asset_icons as eai on eat.emma_asset_icon_id = eai.asset_icon_id
//WHERE a.active = '1'
//AND a.emma_plan_id IN (".implode(',',$multi_plan).")
//AND a.active = 1
//AND a.deleted = 0
//");

echo '<script>';
while ($asset = $assets->fetch_assoc()) {
//If the name of the location contains valid characters and isn't empty.
    if((!empty($asset['lat'])) && ($asset['lat'] != 0) && ($asset['lat'] != 1) && (!empty($asset['lng'])) && ($asset['lng'] != 0) && ($asset['lng'] != 1) && ($asset['address'] != null) && ($asset['address'] != '')) {
        echo 'var ast = {lat: ' . $asset['lat'] . ', lng: ' . $asset['lng'] . ', name: "' . $asset['name'] . '", address: "'.$asset['address'].'", image: "'.$asset['image'].'"};';
        echo 'assets.push(ast);';
    }
}
echo '
var user = true;
</script>';





echo '
<div class="title row expanded align-middle">
    <div class="columns medium-2">
        <h2 class="text-left">Home</h2>
    </div>    
</div>';

if ($event = $events->fetch_assoc()) {
    ?>
    <input readonly hidden id="event_is_active" value="1">
    <script type="text/javascript">
        var mainEvent = <?php echo json_decode($event); ?>;
    </script>
    <div class="row expanded">
        <?php
        if($USER->privilege->admin911){
        ?>
            <div class="large-4 medium-4 small-4 columns">
                <div class="card-info warning">
                    <div class="card-info-content">
                        <h3 class="lead">Active Events/Drills</h3>
                        <div id="active-events-drills" class="chart">
                            <ul style="margin-bottom:0;">
                                <li>Events
                                    <ul>
                                        <?php

                                        $activeLives = select_userhome_activeLives($multi_plan);
//                                            $fvmdb->query("
//                  SELECT e.emergency_id, et.name, e.drill, ep.name as plan_name, ep.emma_plan_id
//                  FROM emergencies e
//                  JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
//                  LEFT JOIN emma_plans as ep on e.emma_plan_id = ep.emma_plan_id
//                  WHERE e.emma_plan_id IN (".implode(',',$multi_plan).")
//                  AND e.active = '1'
//                  AND e.drill = '0'
//                  AND e.parent_emergency_id IS NULL
//                ");
                                        while ($activeLive = $activeLives->fetch_assoc()) {
                                            echo '<li><button style="color: blue;" type="button" data-id = '.$activeLive['emma_plan_id'].' onclick="go_event(this);" data-href="./dashboard.php?content=event&id=' .
                                                $activeLive['emergency_id'] . '">' . $activeLive['name'] .' ('.$activeLive['plan_name'].')'.
                                                '</button></li>';
                                        }
                                        ?>
                                    </ul>
                                </li>


                            </ul>
                        </div>
                    </div>
                </div>
            </div><!--/ Active Events/Drills -->
            <?php
        }


            ?>
        <?php if($USER->privilege->admin911)
        {
            echo '<div class="large-8 medium-8 small-8 columns">';
        }
        else{
            echo '<div class="large-6 medium-12 small-12 columns">';
        }?>
            <div class="card-info secondary">
                <div id="event-map" style="width:100%; height:75vh;"></div>
            </div><!--/ Map -->
        </div>
    <div class="large-6 medium-12 small-12 columns">
        <div class="card-info warning">
            <div class="card-info-content">
                <div class="row">
                    <div class="large-10 columns">
                        <h3 class="lead">Live Feed</h3>
                    </div>
                    <div class="large-2 columns">
                        <select id="feed-length">
                            <option value="1" <?php if($USER->feed_length == 1)echo 'selected';?>>1 Hour</option>
                            <option value="2" <?php if($USER->feed_length == 2)echo 'selected';?>>2 Hours</option>
                            <option value="6" <?php if($USER->feed_length == 6)echo 'selected';?>>6 Hours</option>
                            <option value="12" <?php if($USER->feed_length == 12)echo 'selected';?>>12 Hours</option>
                            <option value="24" <?php if($USER->feed_length == 24)echo 'selected';?>>24 Hours</option>
                            <option value="48" <?php if($USER->feed_length == 48)echo 'selected';?>>48 Hours</option>
                        </select>
                    </div>
                </div>
                <div id="live-feed">
                    <div class="large-12 columns">
                        <div class="row">
                            <div id="feed-center" style="width: 100%">
                                <?php

                                $alertarray = array();
                                $currentplan = get_emma_plan($USER->emma_plan_id);
                                $hours = $USER->feed_length;
                                $notifications = select_feed_mass($USER->emma_home_plans, $hours);
                                $events = select_feed_events($USER->emma_home_plans, $hours);
                                $lockdowns = select_feed_lockdowns($USER->emma_home_plans, $hours);
                                $soss = select_feed_sos($USER->emma_home_plans, $hours);
                                $geofences = select_feed_geofence($USER->emma_home_plans, $hours);


                                if($notifications->num_rows > 0) {
                                    while ($notification = $notifications->fetch_assoc()) {
                                        $alertarray[] = $notification;
                                    }
                                }
                                if($events->num_rows > 0) {
                                    while ($event = $events->fetch_assoc()) {
                                        $alertarray[] = $event;
                                    }
                                }
                                if($lockdowns->num_rows > 0) {
                                    while ($lockdown = $lockdowns->fetch_assoc()) {
                                        $alertarray[] = $lockdown;
                                    }
                                }
                                if($soss->num_rows > 0) {
                                    while ($sos = $soss->fetch_assoc()) {
                                        $alertarray[] = $sos;
                                    }
                                }
                                if($geofences->num_rows > 0) {
                                    while ($geofence = $geofences->fetch_assoc()) {
                                        $alertarray[] = $geofence;
                                    }
                                }
                                if(count($alertarray) > 0) {
//                        echo'<br> unsorted <br>';
//                        var_dump($alertarray);
                                    function sortFunction($a, $b)
                                    {
                                        $v1 = strtotime($a["sortdate"]);
                                        $v2 = strtotime($b["sortdate"]);
                                        return $v2 - $v1;
                                    }

                                    usort($alertarray, "sortFunction");
//                        echo'<br> sorted <br>';
//                        var_dump($alertarray);
                                    foreach ($alertarray as $alert) {
                                        echo '
                            <div class="callout alert-callout-subtle radius inset-shadow system" style="overflow:hidden;word-wrap:break-word;">
                                <div>
                                    <a href="dashboard.php?content=' . $alert['alert_type'] . '&id=' . $alert['alert_id'] . '" style="margin-top: 10px;"><h5>' . $alert['type_name'] . ' ';
                                        if ($alert['drill'] == 1) {
                                            echo '(DRILL)';
                                        }
                                        echo '</h5></a>
                                </div>';
                                        if ($alert['alert_type'] == 'geofence') {
                                            echo '<div>
                                        <b>Fence: </b><span>' . $alert['geofencename'] . '</span>
                                    </div>';
                                        }
                                        echo '
                                <div>
                                    <a href="dashboard.php?content=' . $alert['alert_type'] . '&id=' . $alert['alert_id'] . '" style="margin-top: 10px;">' . $alert['name'] . ' (' . $alert['username'] . ')</a><span> - ' . date('H:i:s m/d/Y', strtotime($alert['sortdate'])) . '</span>
                                </div>
                                <div>
                                    <b>Plan: </b>
                                    <span>';
                                        $plans = select_get_alert_plans($alert['alert_id'], $alert['alert_type']);
                                        while($plan = $plans->fetch_assoc()){
                                            echo $plan['name'] . ' ';
                                        }
                                        echo'</span>
                                </div>';
                                        if ($alert['alert_type'] == 'mass_communication' || $alert['alert_type'] == 'event') {
                                            echo '
                                <div>
                                    <b>Sent To: </b><span style="margin-left: 5px;">';
                                            $groups = select_feed_groups($alert['alert_id'], $alert['alert_type']);
                                            $totalcount = $groups->num_rows;
                                            $i = 1;
                                            while ($group = $groups->fetch_assoc()) {
                                                echo '<a href="dashboard.php?content=group&id=' . $group['emma_group_id'] . '">' . $group['name'] . '</a>';
                                                if ($i == $totalcount) {
                                                    echo ' ';
                                                } else {
                                                    echo ', ';
                                                }
                                                $i++;
                                            }
                                            echo '</span>
                                </div>';
                                        }
                                        echo '
                                <div>
                                    <b>Message: </b><span style="margin-left: 5px;">' . $alert['notification'] . '</span>
                                </div>
                            </div>
                            ';
                                        if ($alert['alert_type'] == 'event') {
                                            $responses = select_feed_event_responses($alert['alert_id']);
                                            while ($response = $responses->fetch_assoc()) {
                                                echo '
                                    <div class="callout alert-callout-subtle radius inset-shadow success" style="overflow:hidden;word-wrap:break-word;">          
                                        <div>
                                            <a href="dashboard.php?content=user&id=' . $response['userid'] . '" style="margin-top: 10px;">' . $response['name'] . ' (' . $response['username'] . ')</a><span> - ' . date('H:i:s m/d/Y', strtotime($response['created_date'])) . '</span>
                                        </div>
                                        <div>
                                            <b>Status: </b><span style="margin-left: 5px; color: ' . $response['color'] . '">' . $response['statusname'] . '</span>
                                        </div>
                                        <div>
                                            <b>Response: </b><span style="margin-left: 5px;">' . $response['response'] . '</span>
                                        </div>
                                    </div>
                                ';
                                            }
                                        } elseif ($alert['alert_type'] == 'mass_communication') {
                                            $responses = select_feed_mass_responses($alert['alert_id']);
                                            while ($response = $responses->fetch_assoc()) {
                                                echo '
                                    <div class="callout alert-callout-subtle radius inset-shadow success" style="overflow:hidden;word-wrap:break-word;">
                                        <div>
                                            <a href="dashboard.php?content=user&id=' . $response['userid'] . '" style="margin-top: 10px;">' . $response['name'] . ' (' . $response['username'] . ')</a><span> - ' . date('H:i:s m/d/Y', strtotime($response['created_date'])) . '</span>
                                        </div>
                       
                                        <div>
                                            <b>Response: </b><span style="margin-left: 5px;">' . $response['response'] . '</span>
                                        </div>
                                    </div>
                                ';
                                            }
                                        } elseif ($alert['alert_type'] == 'geofence') {
                                            $responses = select_feed_geofence_responses($alert['alert_id']);
                                            while ($response = $responses->fetch_assoc()) {
                                                echo '
                                    <div class="callout alert-callout-subtle radius inset-shadow success" style="overflow:hidden;word-wrap:break-word;">
                                        <div>
                                            <a href="dashboard.php?content=user&id=' . $response['userid'] . '" style="margin-top: 10px;">' . $response['name'] . ' (' . $response['username'] . ')</a><span> - ' . date('H:i:s m/d/Y', strtotime($response['created_date'])) . '</span>
                                        </div>
                       
                                        <div>
                                            <b>Response: </b><span style="margin-left: 5px;">' . $response['response'] . '</span>
                                        </div>
                                    </div>
                                ';
                                            }
                                        }
                                    }
                                }else{
                                    echo'No Live Events Within '. $hours .' Hours';
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!--    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDS_cKQ5EnelY2rUO6JOIQKOeyil5b7enw&callback=initMap"></script>-->
    <div id="response-card-modal" data-options="closeOnClick:false;closeOnEsc:false;" class="reveal callout text-center small"
         data-reveal data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4>Response Card</h4>
        <?php if($event['drill'] == 1){echo '<h4 style="color: red;">(DRILL)</h4>';}?>
        <hr>
        <div class="large-12 columns clear-all">
            <div style="text-align: left; margin-left: 1%;">
                <img style="float: right; margin:3% 12% 0 0;" src="img/<?php echo $event['img_filename']?>">
                <p><strong>Type: </strong><?php echo $event['type'] ?></p>
                <p><strong>Sub Type:</strong> <?php echo $event['sub_name'] ?></p>
                <p><strong>Site: </strong> <?php echo $event['emma_site_name'] ?></p>
                <p><strong>Comments: </strong> <?php echo $event['comments'] ?></p>
            </div>
            <?php
            $response_statuses = select_emergencyResponseStatusIDs_with_emergencyTypeID($event['emergency_type_id']);
            ?>

            <div class="row">
                <strong>Emergency Details: </strong><textarea readonly><?php echo $event['description'] ?></textarea>
                <div style="text-align: left;" class="small-12 large-expand columns"><strong>Alert Response: </strong>
                    <?php echo $USER->admin ? '<form id="response-card-form">': '<div id="response-card-form">'; ?>
                    <?php $count = 0; $num=0; while($response_status = $response_statuses->fetch_assoc())
                    {
                        $specific_responses = select_emergencyResponseStatuses_with_emergencyResponseID($response_status['emergency_response_status_id']);
                        while($specific_response = $specific_responses->fetch_assoc()) {

                            echo '<input required type="radio" name="response" id="responseChoice' . $num . '" value="'.$response_status['emergency_response_status_id'].'"><label style="color: ' . $specific_response['color'] . '" for="responseChoice' . $num . '" >' . $specific_response['name'] . '</label><br>';
                            $count++;
                            $num++;
                        }
                    }; ?>
                    <input type="text" name="comments" placeholder="Comments">
                    <input type="hidden" name="user_id" value="<?php echo $USER->id ?>">
                    <input type="hidden" name="emergency_id" value="<?php echo $event['emergency_id'];?>"/>
                    <input type="hidden" name="selected_id"/>
                    <?php if($USER->admin){echo '<input type="hidden" name="pin" value="'.$USER->emma_pin.'">';}?>
                    <?php echo $USER->admin ? '</form>': '</div>'; ?>
                </div>
            </div>
        </div>
        <div style="margin-left:auto;margin-right:auto;">
            <button type="button" id="close_response_card" class="button alert">Cancel</button>
            <button type="button" id="creating-response" onclick="pull_up_pin(this);" class="button">Send Response</button>
        </div>
    </div>

<?php
} else {
?>
    <input readonly hidden id="event_is_active" value="0">
    <div class="row expanded">
        <?php if($USER->privilege->admin911)
    {
        echo '<div class="large-7 medium-7 small-7 columns large-offset-5">';
    }
    else{
        echo '<div class="large-6 medium-12 small-12 columns">';
    }
    ?>
        <div id="event-map" style="width:100%; height:600px;"></div>

    </div>
    <div class="large-6 medium-12 small-12 columns">
    <div class="card-info warning">
        <div class="card-info-content">
            <div class="row">
                <div class="large-10 columns">
                    <h3 class="lead">Live Feed</h3>
                </div>
                <div class="large-2 columns">
                    <select id="feed-length">
                        <option value="1" <?php if($USER->feed_length == 1)echo 'selected';?>>1 Hour</option>
                        <option value="2" <?php if($USER->feed_length == 2)echo 'selected';?>>2 Hours</option>
                        <option value="6" <?php if($USER->feed_length == 6)echo 'selected';?>>6 Hours</option>
                        <option value="12" <?php if($USER->feed_length == 12)echo 'selected';?>>12 Hours</option>
                        <option value="24" <?php if($USER->feed_length == 24)echo 'selected';?>>24 Hours</option>
                        <option value="48" <?php if($USER->feed_length == 48)echo 'selected';?>>48 Hours</option>
                    </select>
                </div>
            </div>
            <div id="live-feed">
                <div class="large-12 columns">
                    <div class="row">
                        <div id="feed-center" style="width: 100%">
                            <?php

                            $alertarray = array();
                            $currentplan = get_emma_plan($USER->emma_plan_id);
                            $hours = $USER->feed_length;
                            $notifications = select_feed_mass($USER->emma_home_plans, $hours);
                            $events = select_feed_events($USER->emma_home_plans, $hours);
                            $lockdowns = select_feed_lockdowns($USER->emma_home_plans, $hours);
                            $soss = select_feed_sos($USER->emma_home_plans, $hours);
                            $geofences = select_feed_geofence($USER->emma_home_plans, $hours);


                            if($notifications->num_rows > 0) {
                                while ($notification = $notifications->fetch_assoc()) {
                                    $alertarray[] = $notification;
                                }
                            }
                            if($events->num_rows > 0) {
                                while ($event = $events->fetch_assoc()) {
                                    $alertarray[] = $event;
                                }
                            }
                            if($lockdowns->num_rows > 0) {
                                while ($lockdown = $lockdowns->fetch_assoc()) {
                                    $alertarray[] = $lockdown;
                                }
                            }
                            if($soss->num_rows > 0) {
                                while ($sos = $soss->fetch_assoc()) {
                                    $alertarray[] = $sos;
                                }
                            }
                            if($geofences->num_rows > 0) {
                                while ($geofence = $geofences->fetch_assoc()) {
                                    $alertarray[] = $geofence;
                                }
                            }
                            if(count($alertarray) > 0) {
//                        echo'<br> unsorted <br>';
//                        var_dump($alertarray);
                                function sortFunction($a, $b)
                                {
                                    $v1 = strtotime($a["sortdate"]);
                                    $v2 = strtotime($b["sortdate"]);
                                    return $v2 - $v1;
                                }

                                usort($alertarray, "sortFunction");
//                        echo'<br> sorted <br>';
//                        var_dump($alertarray);
                                foreach ($alertarray as $alert) {
                                    echo '
                            <div class="callout alert-callout-subtle radius inset-shadow system" style="overflow:hidden;word-wrap:break-word;">
                                <div>
                                    <a href="dashboard.php?content=' . $alert['alert_type'] . '&id=' . $alert['alert_id'] . '" style="margin-top: 10px;"><h5>' . $alert['type_name'] . ' ';
                                    if ($alert['drill'] == 1) {
                                        echo '(DRILL)';
                                    }
                                    echo '</h5></a>
                                </div>';
                                    if ($alert['alert_type'] == 'geofence') {
                                        echo '<div>
                                        <b>Fence: </b><span>' . $alert['geofencename'] . '</span>
                                    </div>';
                                    }
                                    echo '
                                <div>
                                    <a href="dashboard.php?content=' . $alert['alert_type'] . '&id=' . $alert['alert_id'] . '" style="margin-top: 10px;">' . $alert['name'] . ' (' . $alert['username'] . ')</a><span> - ' . date('H:i:s m/d/Y', strtotime($alert['sortdate'])) . '</span>
                                </div>
                                <div>
                                    <b>Plan: </b>
                                    <span>';
                                    $plans = select_get_alert_plans($alert['alert_id'], $alert['alert_type']);
                                    while($plan = $plans->fetch_assoc()){
                                        echo $plan['name'] . ' ';
                                    }
                                    echo'</span>
                                </div>';
                                    if ($alert['alert_type'] == 'mass_communication' || $alert['alert_type'] == 'event') {
                                        echo '
                                <div>
                                    <b>Sent To: </b><span style="margin-left: 5px;">';
                                        $groups = select_feed_groups($alert['alert_id'], $alert['alert_type']);
                                        $totalcount = $groups->num_rows;
                                        $i = 1;
                                        while ($group = $groups->fetch_assoc()) {
                                            echo '<a href="dashboard.php?content=group&id=' . $group['emma_group_id'] . '">' . $group['name'] . '</a>';
                                            if ($i == $totalcount) {
                                                echo ' ';
                                            } else {
                                                echo ', ';
                                            }
                                            $i++;
                                        }
                                        echo '</span>
                                </div>';
                                    }
                                    echo '
                                <div>
                                    <b>Message: </b><span style="margin-left: 5px;">' . $alert['notification'] . '</span>
                                </div>
                            </div>
                            ';
                                    if ($alert['alert_type'] == 'event') {
                                        $responses = select_feed_event_responses($alert['alert_id']);
                                        while ($response = $responses->fetch_assoc()) {
                                            echo '
                                    <div class="callout alert-callout-subtle radius inset-shadow success" style="overflow:hidden;word-wrap:break-word;">          
                                        <div>
                                            <a href="dashboard.php?content=user&id=' . $response['userid'] . '" style="margin-top: 10px;">' . $response['name'] . ' (' . $response['username'] . ')</a><span> - ' . date('H:i:s m/d/Y', strtotime($response['created_date'])) . '</span>
                                        </div>
                                        <div>
                                            <b>Status: </b><span style="margin-left: 5px; color: ' . $response['color'] . '">' . $response['statusname'] . '</span>
                                        </div>
                                        <div>
                                            <b>Response: </b><span style="margin-left: 5px;">' . $response['response'] . '</span>
                                        </div>
                                    </div>
                                ';
                                        }
                                    } elseif ($alert['alert_type'] == 'mass_communication') {
                                        $responses = select_feed_mass_responses($alert['alert_id']);
                                        while ($response = $responses->fetch_assoc()) {
                                            echo '
                                    <div class="callout alert-callout-subtle radius inset-shadow success" style="overflow:hidden;word-wrap:break-word;">
                                        <div>
                                            <a href="dashboard.php?content=user&id=' . $response['userid'] . '" style="margin-top: 10px;">' . $response['name'] . ' (' . $response['username'] . ')</a><span> - ' . date('H:i:s m/d/Y', strtotime($response['created_date'])) . '</span>
                                        </div>
                       
                                        <div>
                                            <b>Response: </b><span style="margin-left: 5px;">' . $response['response'] . '</span>
                                        </div>
                                    </div>
                                ';
                                        }
                                    } elseif ($alert['alert_type'] == 'geofence') {
                                        $responses = select_feed_geofence_responses($alert['alert_id']);
                                        while ($response = $responses->fetch_assoc()) {
                                            echo '
                                    <div class="callout alert-callout-subtle radius inset-shadow success" style="overflow:hidden;word-wrap:break-word;">
                                        <div>
                                            <a href="dashboard.php?content=user&id=' . $response['userid'] . '" style="margin-top: 10px;">' . $response['name'] . ' (' . $response['username'] . ')</a><span> - ' . date('H:i:s m/d/Y', strtotime($response['created_date'])) . '</span>
                                        </div>
                       
                                        <div>
                                            <b>Response: </b><span style="margin-left: 5px;">' . $response['response'] . '</span>
                                        </div>
                                    </div>
                                ';
                                        }
                                    }
                                }
                            }else{
                                echo'No Live Events Within '. $hours .' Hours';
                            }
                            ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php } ?>

<div hidden class="large-6 medium-12 small-12 columns">
    <div class="large-12 medium-12 small-12">
        <div class="card-info secondary">
            <div class="card-info-content">
                <h3 class="lead">Message Center</h3>
                <div id="message-center" class="max-height">

                </div>
                <hr>
                <form id="alert-response-form" action="#" method="post">
                    <fieldset class="fieldset">
                        <select name="site">
                            <?php
                            $sites = select_sites_with_planID($USER->emma_plan_id);
//                            $fvmdb->query("
//                                        SELECT *
//                                        FROM emma_sites
//                                        WHERE emma_plan_id = '" . $USER->emma_plan_id . "'
//                                    ");
                            while ($site = $sites->fetch_assoc()) {
                                echo '<option value="' . $site['emma_site_id'] . '">' . $site['emma_site_name'] . '</option>';
                            }
                            ?>
                        </select>
                        <legend>Status</legend>
                        <input id="all-clear" name="status" type="radio" value="0" checked><label for="all-clear">All
                            Clear</label>
                        <input id="need-help" name="status" type="radio" value="1"><label for="need-help">Need
                            Help</label>
                        <input id="need-info" name="status" type="radio" value="2"><label for="need-info">Need
                            Information</label>
                    </fieldset>
                    <input type="hidden" name="a" value="<?php echo $USER->auth; ?>"/>
                    <input type="hidden" name="user" value="<?php echo $USER->id; ?>"/>
                    <input type="hidden" name="emergency-id" value="<?php echo $event['emergency_id']; ?>"/>
                    <input type="hidden" name="mobile" value="0" />
                    <input type="hidden" name="lat" value=""/>
                    <input type="hidden" name="lng" value=""/>
                    <textarea name="comments" style="height:5rem;"></textarea>
                    <input type="submit" class="button"/>
                </form>
            </div>
        </div><!--/ Message Center -->
    </div>
</div>
    <div id="alert-feed-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
        <h4>Alert Feed</h4>
        <hr>
        <div class="large-12 columns">
            <div class="row">
                <div id="feed-center">
                    <?php
                    $notifications = select_massComunication_notifications_feed($USER->emma_plan_id, $currentplan['feed_timer_minutes']);
                    //                                        $fvmdb->query("
                    //                                        SELECT e.*, CONCAT(u.firstname,' ',u.lastname) as name, u.username
                    //                                        FROM emma_mass_communications e
                    //                                        JOIN users u ON e.created_by_id = u.id
                    //                                        WHERE e.emma_plan_id = '". $USER->emma_plan_id ."'
                    //                                        AND e.created_date > '". $event['created_date'] ."' - INTERVAL 2 HOUR
                    //                                        AND e.created_date < '". $event['created_date'] ."' + INTERVAL 2 HOUR
                    //                                    ");
                    if($notifications->num_rows > 0){
                        echo'<div><h5><b>Mass Notifications</b></h5></div>';
                    }
                    while($not = $notifications->fetch_assoc()){
                    if($not['emma_mass_communication_id'] == $event['emma_mass_communication_id']){
                    $color = 'color: red;';
                    }else{
                    $color = '';
                    }
                    echo'
                    <div class="callout alert-callout-subtle radius inset-shadow system" style="overflow:hidden;word-wrap:break-word;">
                        <div>
                            <a href="dashboard.php?content=mass_communication&id='. $not['emma_mass_communication_id'] .'" style="margin-top: 10px;'. $color .'">'. $not['name'] .' ('. $not['username'] .')</a><span> - '. date('H:i:s m/d/Y',strtotime($not['created_date'])) .'</span>
                        </div>
                        <div>
                            <b>Sent To: </b><span style="margin-left: 5px;">'; $groups = select_massComunication_groups($not['emma_mass_communication_id']); $totalcount = $groups->num_rows; $i = 1; while($group = $groups->fetch_assoc()){echo '<a href="dashboard.php?content=group&id='. $group['emma_group_id'] .'">' . $group['name'] . '</a>';if($i == $totalcount){echo ' ';}else{echo ', ';} $i++;} echo'</span>
                        </div>
                        <div>
                            <b>Message: </b><span style="margin-left: 5px;">'. $not['notification'] .'</span>
                        </div>
                    </div>
                    ';
                    }?>
                </div>
            </div>
        </div>
        <button class="close-button" onclick="location.reload();" aria-label="Close reveal"
                type="button">
            <span aria-hidden="true">×</span>
        </button>
    </div>


<?php

if($USER->emma_plan_id != 20 ) {
    $show = true;
}
else{
    $show = false;
}

?>
<input hidden id="plan_id" value="<?php echo $show; ?>"

<?php
echo '</div>';
$populate_events = select_userhome_populateEvents($multi_plan);
//    $fvmdb->query("
//    SELECT e.*, et.name AS type, et.img_filename
//    FROM emergencies e
//    JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
//    WHERE e.active = '1'
//    AND e.emma_plan_id IN (".implode(',',$multi_plan).")
//    ");

       echo '<script>';
    echo 'let events_arr = [];';
    while ($populate_event = $populate_events->fetch_assoc()) {
        if((!empty($populate_event['latitude'])) && ($populate_event['latitude'] != 0) && (!empty($populate_event['longitude'])) && ($populate_event['longitude'] != 0)) {
            echo 'var evnt = {lat: ' . $populate_event['latitude'] . ', lng: ' . $populate_event['longitude'] . ', type: "' . $populate_event['type'] . '", img: "'.$populate_event['img_filename'].'"};';
            echo 'events_arr.push(evnt);';
        }

    }
    echo '</script>';
?>