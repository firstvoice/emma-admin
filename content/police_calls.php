<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/17/2019
 * Time: 10:09 AM
 */

if($USER->privilege->security || $USER->privilege->admin || $USER->privilege->admin911)
{
    //nothing
}
else{
    redirect();
}

?>

<div class="title row expanded align-middle">
  <div class="columns medium-2">
    <h2 class="text-left"><a
        href="./dashboard.php?content=events"><i class="page-icon fa fa-phone"></i> <?php echo '911 Calls'; ?></a>
    </h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
    <!--    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu-->
    <!--      data-options="disableHover:true;clickOpen:true;">-->
    <!--      <li>-->
    <!--        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>-->
    <!--        <ul class="menu">-->
    <!--          <li><a href="#" data-open="create-event-modal">Create Event</a></li>-->
    <!--        </ul>-->
    <!--      </li>-->
    <!--    </ul>-->
  </div>
</div>

<table id="calls-table" class="data-table" style="width:100%">
  <thead>
  <tr>
      <th class="text-search">Date/Time</th>
      <th class="text-search">Username</th>
      <th class="text-search">Latitude</th>
      <th class="text-search">Longitude</th>
      <th class="text-search">Event</th>
  </tr>
  <tr>
      <th>Date/Time</th>
      <th>Username</th>
      <th>Latitude</th>
      <th>Longitude</th>
      <th>Event</th>
  </tr>
  </thead>
  <tbody>
  </tbody>
  <tfoot>
  </tfoot>
</table>
