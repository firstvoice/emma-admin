<?php


$guest_code_id = $fvmdb->real_escape_string($_GET['id']);

$guest_codes = select_guestCodes_with_guestCodeID($guest_code_id);
//$fvmdb->query("
//    SELECT g.*
//    FROM emma_guest_codes g
//    WHERE g.guest_code_id = '" . $guest_code_id . "'
//");
if (!$guest_codes->num_rows > 0) {
    echo '
    <div class="row expanded title">
        <div class="large-8 medium-12">
            <h2 class="text-left">Edit Guest Code - Error</h2>
        </div>
    </div>
    <div>
        <h4 class="text-center">Failed to load guest code, please try again.</h4>
    </div>
    ';
    exit();
}

$gc = $guest_codes->fetch_assoc();

?>

<form id="edit_code_form">
    <input type="hidden" name="id" value="<?php echo $guest_code_id; ?>">
    <div class="title row expanded align-middle">
        <div class="columns medium-8">
            <h2 class="text-left"><a href="./dashboard.php?content=guest_codes"><i class="page-icon fa fa-ticket"></i> Edit Guest Code - <?php echo $guest_code_id; ?></a></h2>
        </div>
        <div class="columns show-for-medium"></div>
        <div class="columns shrink">
            <input type="submit" class="button" value="Save" style="margin:0;">
        </div>
    </div>

    <div class="row expanded">
        <div class="large-12 columns">
            <div class="card-info primary">
                <div class="card-info-content">
                    <h3 class="lead">Details</h3>
                    <div class="row">
                        <div class="large-12 column">
                            <label>Guest Code
                                <input type="Text" name="guest-code-id" value="<?php echo $gc['guest_code_id']; ?>" readonly/>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-6 medium-12 small-12 column">
                            <label>Group
                                <select name="group-id" required>
                                    <?php
                                    $groups = select_groups_with_planID($USER->emma_plan_id);
//                                    $fvmdb->query("
//                                        select *
//                                        from emma_groups
//                                        where emma_plan_id = '" . $USER->emma_plan_id . "'
//                                      ");
                                    while ($group = $groups->fetch_assoc()) {
                                        echo '<option value="' . $group['emma_group_id'] . '">' .
                                            $group['name'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </label>
                        </div>
                        <div class="large-6 medium-12 small-12 column">
                            <label>Duration (Minutes)
                                <input type="Text" name="duration" required value="<?php echo $gc['duration_minutes']; ?>"/>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-4 medium-12 small-12 column">
                            <label>Start Date
                                <input type="date" name="start-date" class="time-edit" id="code-start" required value="<?php echo $gc['start_date']; ?>"/>
                            </label>
                        </div>
                        <div class="large-4 medium-12 small-12 column">
                            <label>End Date
                                <input type="date" name="end-date" class="time-edit" id="code-end" required value="<?php echo $gc['end_date']; ?>"/>
                            </label>
                        </div>
                        <div class="large-4 medium-12 small-12 column">
                            <label>Status</label>
                            <div id="status-display">
                            <?php
                                if((strtotime($gc['end_date']) < strtotime('now')) || (strtotime($gc['start_date']) > strtotime('now'))){
                                    echo '<h4 style="color: red">Inactive</h4>';
                                }else{
                                    echo '<h4 style="color: green">Active</h4>';
                                }
                            ;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<div id="success_modal" class="reveal success callout tiny text-center" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <a href="./dashboard.php?content=guest_code&id=<?php echo $guest_code_id; ?>" data-close class="button success" style="margin-left:auto;margin-right:auto">Ok</a>
<!--    <button class="close-button" data-close aria-label="Close reveal" type="button">-->
<!--        <span aria-hidden="true">&times;</span>-->
<!--    </button>-->
</div>