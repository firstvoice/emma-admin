<?php

if (!($USER->privilege->security || $USER->privilege->admin)) {
    redirect();
}

?>

<div class="title row expanded align-middle">
  <div class="columns medium-2">
    <h2 class="text-left"><a href="./dashboard.php?content=securities"><i class="page-icon fa fa-shield"></i> Security</a></h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
      data-options="disableHover:true;clickOpen:true;">
      <li>
        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <ul class="menu">
          <li><a href="#" data-open="create-security-modal">Create Security</a>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</div>

<table id="securities-table" class="data-table" style="width:100%">
  <thead>
  <tr>
      <th class="text-search">Security Event</th>
      <th class="text-search">Username/Email</th>
      <th class="text-search">Status</th>
      <th class="text-search">Created</th>
      <th></th>
  </tr>
  <tr>
    <th>Security Event</th>
    <th>Username/Email</th>
    <th>Status</th>
    <th>Created</th>
    <th>Call Report</th>
  </tr>
  </thead>
  <tbody>
  </tbody>
  <tfoot>

  </tfoot>
</table>

<div id="create-security-modal" class="reveal callout text-center small"
  data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Create Security</h4>
  <hr>
  <form id="create-security-form" class="text-left" action="#" method="post">
    <input type="hidden" name="user-id" value="<?php echo $USER->id; ?>"/>
    <input type="hidden" name="plan-id" value="<?php echo $USER->emma_plan_id; ?>"/>
    <input type="hidden" name="latitude" value=""/>
    <input type="hidden" name="longitude" value=""/>
    <div class="row">
      <div class="small-12 columns">
        <label>Site <span style="color: red">*</span>
          <select name="site-id" class="expanded">
            <?php
            $sites = select_sites_with_planID($USER->emma_plan_id);
//            $fvmdb->query("
//              select *
//              from emma_sites
//              where emma_plan_id = '" . $USER->emma_plan_id . "'
//            ");
            while ($site = $sites->fetch_assoc()) {
              echo '<option value="' . $site['emma_site_id'] .
                '">' . $site['emma_site_name'] . '</option>';
            }
            ?>
          </select>
        </label>
      </div>
    </div>
    <div class="row">
      <div class="small-12 columns">
        <label>Security <span style="color: red">*</span>
          <select name="type-id" class="expanded">
            <?php
            $securityTypes = select_securities_securityTypes($USER->emma_plan_id);
//                $fvmdb->query("
//              select st.*
//              from emma_security_types st
//              join emma_plan_security_types pst on st.emma_security_type_id = pst.emma_security_type_id
//              where pst.emma_plan_id = '" . $USER->emma_plan_id. "'
//              order by st.name
//            ");
            while ($securityType = $securityTypes->fetch_assoc()) {
              echo '<option value="' . $securityType['emma_security_type_id'] . '">' . $securityType['name'] . '</option>';
            }
            ?>
          </select>
        </label>
      </div>
    </div>
    <div class="row">
      <div class="small-12 columns">
        <label>Emergency Details <span style="color: red">*</span>
          <textarea name="description"
            style="min-width:100%;height:5rem;"></textarea>
        </label>
      </div>
    </div>
    <div class="row">
      <div class="small-12 columns">
        <label>Comments
          <textarea name="comments"
            style="min-width:100%;height:5rem;"></textarea>
        </label>
      </div>
    </div>
      <div class="row">
          <div class="large-12 columns button-group">
              <div class="button-group" style="margin: 0 auto;">
                  <a href="dashboard.php?content=securities" class="button">Cancel</a>
                  <input type="submit" class="button" value="Submit"/>
              </div>
          </div>
      </div>
  </form>
  <button class="close-button" data-close="" aria-label="Close reveal"
    type="button">
    <span aria-hidden="true">×</span>
  </button>
</div>
