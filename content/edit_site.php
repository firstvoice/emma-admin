<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/20/2017
 * Time: 11:48 AM
 */

$site_id = $fvmdb->real_escape_string($_GET['id']);

if (empty($site_id)) {
  header('Location: index.php');
}

$sites = select_site_with_siteID($site_id);
//$fvmdb->query("
//    SELECT *
//    FROM emma_sites
//    WHERE emma_site_id = '" . $site_id . "'
//");
$site = $sites->fetch_assoc();

if (!empty($site['emma_site_contact_id'])) {
  $contacts = select_editSite_contacts($site['emma_site_contact_id']);
//      $fvmdb->query("
//        SELECT c.first_name, c.last_name, c.email, c.phone, c.emma_site_contact_id as contact_id
//        FROM emma_site_contacts AS c
//        WHERE c.emma_site_contact_id = " . $site['emma_site_contact_id'] . "
//    ");
  if ($contacts->num_rows > 0) {
    $contact = $contacts->fetch_assoc();
  }
}

?>
<form id="edit-site-form">
    <input hidden id="lat" name="lat"/>
    <input hidden id="lng" name="lng"/>
  <div class="title row expanded align-middle">
    <div class="columns medium-8">
      <h2 class="text-left"><a
          href="./dashboard.php?content=sites"><i class="page-icon fa fa-institution"></i> Edit Site</a>
      </h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
      <input id="save-site-button" class="button" value="Save" style="margin:0;">
    </div>
  </div>

  <input id="ignore-address" type="hidden" name="ignore-address" value="0" />
  <input type="hidden" name="id" value="<?php echo $site_id; ?>"/>
  <input type="hidden" name="cid"
    value="<?php echo $contact['contact_id']; ?>"/>
  <input type="hidden" name="user-id" value="<?php echo $USER->id; ?>"/>
  <div class="row columns expanded">
    <div class="large-12 ">
      <div class="row expanded">
        <div class="large-12 medium-12 column">
          <div class="card-info warning">
            <div class="card-info-content">
              <div class="row expanded">
                <h5>Site Information</h5>
              </div>
              <div class="row expanded align-Left">
                <div class="large-6 medium-12 column">
                  <label>Site Name
                    <input type="Text" name="sitename" required
                      value="<?php echo $site['emma_site_name'] ?>"/>
                  </label>
                </div>
              </div>
              <div class="row expanded">
                <div class="large-4 medium-12 small-12 column">
                  <label>Address
                    <input type="Text" class="needs-filled" id="address" name="address" required
                      value="<?php echo $site['emma_site_street_address'] ?>"/>
                  </label>
                </div>
                <div class="large-4 medium-12 small-12 column">
                  <label>City
                    <input type="Text" class="needs-filled" id="city" name="city" required
                      value="<?php echo $site['emma_site_city'] ?>"/>
                  </label>
                </div>

                <div class="large-3 medium-12 small-12 column">
                  <label>State
                    <input type="Text" class="needs-filled" id="state" name="state" required
                      value="<?php echo $site['emma_site_state'] ?>"/>
                  </label>
                </div>
                <div class="large-1 medium-12 small-12 column">
                  <label>Zip
                    <input type="Text" class="needs-filled" id="zip" name="zip" required
                      value="<?php echo $site['emma_site_zip'] ?>"/>
                  </label>
                </div>
              </div>
              <div class="row expanded">
                <h5>Contact Information</h5>
              </div>
              <div class="row expanded">
                <div class="large-6 medium-12 small-12 column">
                  <label>First Name
                    <input type="Text" name="firstname" required
                      value="<?php echo $contact['first_name'] ?>"/>
                  </label>
                </div>


                <div class="large-6 medium-12 small-12 column">
                  <label>Last Name
                    <input type="Text" name="lastname" required
                      value="<?php echo $contact['last_name'] ?>"/>
                  </label>
                </div>
              </div>
              <div class="row expanded">
                <div class="large-6 medium-12 column">
                  <label>Email
                    <input type="Text" name="email" required
                      value="<?php echo $contact['email'] ?>"/>
                  </label>
                </div>

                <div class="large-6 medium-12 column">
                  <label>Phone
                    <input type="Text" name="phone" required
                      value="<?php echo $contact['phone'] ?>"/>
                  </label>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div><!-- card row end -->
    </div>
  </div>
</form>


<div id="success_modal" class="reveal callout tiny success text-center"
  data-reveal data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Success</h4>
  <div class="row columns">
    <a href="./dashboard.php?content=site&id=<?php echo $site_id; ?>" data-close
      class="button success" style="margin-left:auto;margin-right:auto;">Ok</a>
  </div>
</div>
<div id="warning-modal" class="reveal callout warning text-center tiny" data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4 style="color:darkred">Warning</h4>
  <div id="warning-list" class="text-left">
    <p>Address not found...</p>
  </div>
  <a id="warning-save" class="button" data-close>Save</a>
  <button class="close-button" data-close aria-label="Close reveal"
    type="button">
    <span aria-hidden="true">&times;</span>
  </button>
    echo '<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDS_cKQ5EnelY2rUO6JOIQKOeyil5b7enw"></script>';
</div>
