<?php

if (!($USER->privilege->activate_emails)) {
    redirect();
}

?>

<div class="title row expanded align-middle">
    <div class="columns medium-6">
        <h2 class="text-left"><a href="./dashboard.php?content=users"><i class="page-icon fa fa-user-circle"></i> Activation Emails</a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
        <input type="submit" class="button" form="email-users-form">
    </div>
</div>
<!--    <div class="large-4 medium-12 column text-right">-->
<!--        <a href="./dashboard.php?content=create_user" id="user_create" class="button" >Create User</a>-->
<!--        <input id="user_assign_group" class="button " type="button" value="Assign Users" />-->
<!--        <input id="user_remove_group" class="button" type="button" value="Remove Users" />-->
<!--        <input id="user_deactivate" class="button " type="button" value="Deactivate User" />-->
<!--    </div>-->

<div>
    <div id="users-active" class="tabs-panel is-active">
        <a class="button" id="select-all">Toggle All Unregistered</a>
        <form id="email-users-form">
            <table id="users-table" class="data-table" style="width:100%">
            <thead>
            <tr>
                <th class="text-search">Username</th>
                <th class="text-search">First Name</th>
                <th class="text-search">Last Name</th>
                <th class="text-search">Group</th>
                <th class="text-search">Plan</th>
            </tr>
            <tr>
                <th>Username</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Group</th>
                <th>Plan</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>

            </tfoot>
            </table>
        </form>
    </div>
</div>


<div id="no-selected-users" class="reveal callout small" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>No users selected.</h4>
    <div class="text-center small-12">
        <a href="./dashboard.php?content=users" data-close class="button">Ok</a>
    </div>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div id="success_modal" class="reveal callout success text-center tiny" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <a href="./dashboard.php?content=send_user_emails" data-close class="button success">Ok</a>
    <!--  <button class="close-button" data-close aria-label="Close reveal" type="button">-->
    <!--    <span aria-hidden="true">&times;</span>-->
    <!--  </button>-->
</div>
