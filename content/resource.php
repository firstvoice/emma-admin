<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/8/2017
 * Time: 10:33 AM
 */

// $resource_type = $fvmdb->real_escape_string($_GET{'resource_type'});
$resource_id = $fvmdb->real_escape_string($_GET{'id'});

$resources = select_resource_resources($USER->emma_plan_id, $resource_id);
//    $fvmdb->query("
//    SELECT r.*
//    FROM emma_resources AS r
//    WHERE r.emma_resource_id = " . $resource_id . "
//    AND r.emma_plan_id = " . $USER->emma_plan_id . "
//    AND r.active = 1
//");



?>

<div class="title row expanded align-middle">
  <div class="columns medium-8">
    <h2 class="text-left"><a
        href="./dashboard.php?content=resources"><i class="page-icon fa fa-map"></i> <?php echo ucwords($content); ?></a>
    </h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
      data-options="disableHover:true;clickOpen:true;">
      <li>
        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <ul class="menu">
          <li><a data-open="edit-resource-modal" href="#">Edit Resource</a></li>
          <li><a id="delete_resource_submit" href="#">Delete Resource</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>

<?php
if ($resources->num_rows == 0) {
  echo '
    <div class="text-center">
        <h5>There are no resources of that type here, contact your admin to add some.</h5>
    </div>
    <div class="text-center">
        <a href="./dashboard.php?content=resources" ><div class="button">Back</div></a>
    </div>
    
    ';
}//end empty resources
else {
  $resource = $resources->fetch_assoc();
  $update = update_emmaResource_viewCount_with_resourceID($resource['emma_resource_id']);
//      $fvmdb->query("
//        UPDATE emma_resources
//        SET view_count = view_count + 1
//        WHERE emma_resource_id = " . $resource['emma_resource_id'] . "
//    ");

  if ($resource['file_extension'] == 'png') {
    echo '
        <div>
            <div class="row margin-top-1em">
                <img src="' . $resource['file_string'] . '" />
            </div> 
            <div class="row">
                <h5>' . $resource['file_name'] . '</h5>
            </div>   
        </div>
        ';
  } else if ($resource['file_extension'] == 'pdf') {

    echo '
        <div class="row" style="height: 80%">
            <object data="' . $resource['file_string'] . '" type="application/pdf" width="100%" height="100%">
                <div class="text-center margin-top-1em">
                    Unable to display on your browser <a href="' .
      $resource['file_string'] . '" >click to download</a>
                </div>
            </object>
        </div>
        ';

  } else if ($resource['file_extension'] == 'docx') {

    $source = $resource['file_string'];

    $phpWord = \PhpOffice\PhpWord\IOFactory::load($source, 'Word2007');

    $text = '';
    $sections = $phpWord->getSections();

    $text .= '<div class="text-center"><h3 class="column">' .
      $resource['file_name'] . '</h3></div>';

    $text .= '<div class="small-12">';
    try {
      foreach ($sections as $section) {
        $elements = $section->getElements();
        foreach ($elements as $element) {
          if (get_class($element) === 'PhpOffice\PhpWord\Element\Text') {
            $text .= '<p>' . $element->getText() . '</p>';
          } elseif (get_class($element) ===
            'PhpOffice\PhpWord\Section\TextBreak') {
            $text .= " \n";
          } elseif (get_class($element) ===
            'PhpOffice\PhpWord\Element\TextBreak') {
            $text .= " </div><div class='small-12'>";
          } else {
            throw new Exception('Unknown class type ' . get_class($element));
          }
        }
      }
    } catch (Exception $e) {

    }
    $text .= '</div>';

    echo '
        <div class="row">
            <object data="' . $resource['file_string'] . '" type="application/vnd.openxmlformats-officedocument.wordprocessingml.document" width="100%" height="100%">
                <div class="margin-top-1em">
                    <div class="text-center"><a href="' .
      $resource['file_string'] . '" >click to download</a></div>
                    ' . $text . '
                </div>
            </object>
        </div>
        ';

  } else if ($resource['file_extension'] == 'doc') {

  } else if ($resource['file_extension'] == 'csv') {
    $csvFile = fopen($resource['file_string'], "r");
    $file_lines = array();
    while (!feof($csvFile)) {
      $file_lines[] = fgets($csvFile);
    }
    fclose($csvFile);

    $headers = $file_lines[0];
    $text = '<div class="row expanded">
                 <table>
                    <thead>
                        <tr>';

    $headerarray = explode(',', $headers);
    foreach ($headerarray as $header) {
      $text .= '<th>' . $header . '</th>';
    }

    $text .= '       </tr>
                    </thead>
                    <tbody>';

    $text .= '</tbody>
                </table>     
                </div>';

  } else if ($resource['file_extension'] == 'xlsx') {

    //    echo'<div class="row"> option A</div>';

    echo '
        <div class="row">
            <object data="' . $resource['file_string'] . '" type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" width="100%" height="100%">
                <div class="text-center margin-top-1em">
                    Unable to display on your browser <a href="' .
      $resource['file_string'] . '" >click to download</a>
                </div>
            </object>
        </div>
        ';

    //    echo'<div class="row"> option B</div>';
    //    echo '<div class="row">';
    //
    //    $source = $resource['file_string'];
    //    $excelReader = PHPExcel_IOFactory::createReaderForFile($source);
    //    $excelObj = $excelReader->load($source);
    //
    //    $worksheet = $excelObj->getSheet(0);
    //    $lastRow = $worksheet->getHighestRow();
    //    $lastcolumn = $worksheet->getHighestColumn();
    //
    //
    //    echo "<table class='excel-table'>";
    //    for ($row = 1; $row <= $lastRow; $row++) {
    //        echo "<tr><td>";
    //        echo $worksheet->getCell('A'.$row)->getValue();
    //        echo "</td><td>";
    //        echo $worksheet->getCell('B'.$row)->getValue();
    //        echo "</td><tr>";
    //    }
    //    echo "</table>";
    //    echo "</div>";
  }
  ?>
  <form id="delete_resource_form">
    <input type="hidden" name="id" value="<?php echo $resource_id; ?>"/>
    <input type="hidden" name="plan-id"
      value="<?php echo $USER->emma_plan_id; ?>"/>
  </form>
  </div>

  <div id="edit-resource-modal" class="reveal callout large" data-reveal
    data-animation-in="fade-in"
    data-animation-out="fade-out">
    <form id="edit-resource-form" enctype="multipart/form-data" action=""
      method="post">
      <input type="hidden" name="plan_id"
        value="<?php echo $USER->emma_plan_id; ?>"/>
      <input type="hidden" name="user_id" value="<?php echo $USER->id; ?>"/>
      <input type="hidden" name="id" value="<?php echo $resource_id; ?>"/>
      <div class="text-center">
        <h4 class="lead">Edit Resource</h4>
      </div>
      <label>File Name:
        <input type="Text" name="file_name" value="<?php echo $resource['file_name']; ?>" required/>
      </label>
      <label>Resource Category:
        <select name="file_type">
          <?php
          $resource_types = select_resource_resourceTypes();
//              $fvmdb->query("
//            SELECT *
//            FROM emma_resource_types
//            WHERE active = 1
//            ORDER BY resource_name
//          ");
          while ($resource_type = $resource_types->fetch_assoc()) {
            if($resource_type['emma_resource_type_id'] == $resource['emma_resource_type_id']) {
              echo '<option value="' . $resource_type['emma_resource_type_id'] . '" selected>' . $resource_type['resource_name'] . '</option>';
            } else {
              echo '<option value="' . $resource_type['emma_resource_type_id'] . '">' . $resource_type['resource_name'] . '</option>';
            }
          }
          ?>
        </select>
      </label>
      <label>Site:
        <select name="site_id">
          <?php
          $sites = select_resource_sites($USER->emma_plan_id);
//              $fvmdb->query("
//          SELECT e.*
//          FROM emma_sites AS e
//          WHERE e.emma_plan_id = " . $USER->emma_plan_id . "
//        ");
          while ($site = $sites->fetch_assoc()) {
            if($site['emma_site_id'] == $resource['emma_site_id']) {
              echo '<option value="' . $site['emma_site_id'] . '" selected>' . $site['emma_site_name'] . '</option>';
            } else {
              echo '<option value="' . $site['emma_site_id'] . '">' . $site['emma_site_name'] . '</option>';
            }
          }
          ?>
        </select>
      </label>
      <div class="text-center small-12">
        <input type="submit" value="submit" class="button"/>
      </div>
      <button class="close-button" data-close aria-label="Close reveal"
        type="button">
        <span aria-hidden="true">&times;</span>
      </button>
    </form>
  </div>
  <div id="success_modal" class="reveal callout success text-center tiny"
    data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4>Success</h4>
    <a href="./dashboard.php?content=resources" data-close
      class="button success">Ok</a>
<!--    <button class="close-button" data-close aria-label="Close reveal"-->
<!--      type="button">-->
<!--      <span aria-hidden="true">&times;</span>-->
<!--    </button>-->
  </div>
  <div id="edit_success_modal" class="reveal callout success text-center tiny"
    data-reveal data-animation-in="fade-in"
    data-animation-out="fade-out">
    <h4>Success</h4>
    <a href="./dashboard.php?content=resources" data-close
      class="button success">Ok</a>
<!--    <button class="close-button" data-close aria-label="Close reveal"-->
<!--      type="button">-->
<!--      <span aria-hidden="true">&times;</span>-->
<!--    </button>-->
  </div>


  <?php
}//end valid resources
?>



