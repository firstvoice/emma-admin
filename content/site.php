<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/7/2017
 * Time: 12:43 PM
 */

$site_id = $fvmdb->real_escape_string($_GET['id']);

if(empty($site_id)){
    header('Location: index.php');
}

$sites = select_site_with_siteID($site_id);
//    $fvmdb->query("
//    SELECT *
//    FROM emma_sites
//    WHERE emma_site_id = '". $site_id ."'
//");
$site = $sites->fetch_assoc();

if(!empty($site['emma_site_contact_id'])){
    $contacts = select_site_contacts($site['emma_site_contact_id']);
//        $fvmdb->query("
//        SELECT c.first_name, c.last_name, c.email, c.phone
//        FROM emma_site_contacts AS c
//        WHERE c.emma_site_contact_id = ". $site['emma_site_contact_id'] ."
//    ");
    if($contacts->num_rows > 0){
        $contact = $contacts->fetch_assoc();
    }
}


?>
<div class="title row expanded align-middle">
    <div class="columns medium-8">
      <h2 class="text-left"><a href="./dashboard.php?content=sites"><i class="page-icon fa fa-institution"></i> <?php echo ucwords($content); ?></a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
        <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
            data-options="disableHover:true;clickOpen:true;">
            <li>
                <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
                <ul class="menu">
                    <li><a href="./dashboard.php?content=edit_site&id=<?php echo $site_id; ?>">Edit Site</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div class="row expanded">
    <div class="large-6 medium-12 small-12 columns">
        <div class="card-info primary">
            <div class="card-info-content">
                <h3 class="lead">Site Info</h3>
                <div class="row columns">
                    <h3 class="lead"><?php echo $site['emma_site_name']; ?></h3>
                </div>
                <div class="row columns margin-bottom-halfEM">
                    <div class="large-6 medium-12 column margin-bottom-halfEM">
                        Contact Name: <?php  if(!empty($contact['first_name'])){echo $contact['first_name'] . ' ' . $contact['last_name'];}else{echo 'None';}  ?>
                    </div>
                    <div class="large-6 medium=12 column margin-bottom-halfEM">
                        Contact Phone: <?php  if(!empty($contact['phone'])){echo $contact['phone'];}else{echo 'None';}  ?>
                    </div>
                    <div class="small-12 column">
                        Contact Email: <?php  if(!empty($contact['email'])){echo $contact['email'];}else{echo 'None';}  ?>
                    </div>
                </div>
                <div class="row columns">
                    <div class="small-12">
                        <h5>Site Address</h5>
                    </div>
                    <div class="large-12 columns margin-bottom-1EM">
                        <?php echo $site['emma_site_street_address'] . ', ' . $site['emma_site_city'] . ', ' . $site['emma_site_state'] . ' ' . $site['emma_site_zip']; ?>
                    </div>
                </div>
            </div>
        </div><!-- end info card -->

    </div><!-- end right column -->
    <div class="large-6 medium-12 small-12 columns">
        <div class="card-info alert">
            <div class="card-info-content">
                <input type="hidden" value="<?php echo $site_id; ?>" id="site_id" />
                <h3 class="lead">Events/Drills Total</h3>
                <div id="line-chart-events-drills" class="chart"></div>

            </div>
        </div>
    </div><!-- end left column -->
    </div>
</div>
