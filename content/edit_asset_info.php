<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 5/30/2019
 * Time: 11:26 AM
 */


if (!($USER->privilege->admin)) {
    redirect();
}

?>
<div class="title row expanded align-middle">
    <div class="columns medium-2">
        <h2 class="text-left"></i><a href="./dashboard.php?content=assets"><i class="page-icon fa fa-cube"></i>Asset Types</a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
        <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
            data-options="disableHover:true;clickOpen:true;">
            <li>
                <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
                <ul class="menu">
                    <li><a href="#" data-open="create-asset-info-modal">Create Asset Type</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>

<table id="assets-table" class="data-table" style="width:100%">
    <thead>
    <tr>
        <th class="text-search">Name</th>
    </tr>
    <tr>
        <th>Name</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>

    </tfoot>
</table>

<div id="create-asset-info-modal" class="reveal callout text-center small"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Edit Asset Type</h4>
    <hr>
    <form id="create-asset-info-form" class="create-asset-form text-left" action="#"
          method="post">
        <input type="hidden" name="user-id" value="<?php echo $USER->id; ?>"/>
        <input type="hidden" name="plan-id"
               value="<?php echo $USER->emma_plan_id; ?>"/>
        <div class="row">
            <div class="large-1 columns">
                <label>Select
                <div id="my-icon-select" class="icon-select"></div>
                </label>
            </div>
            <div class="large-11 columns">
                <label>Asset Name</label>
                <input required type="text" placeholder="Asset Name" name="asset-type" id="asset-type">
            </div>
        </div>
        <div class="row">
                <div class="large-1 columns">
                    <label>Date</label>
                    <button id="addDate" class="button"><i class="fa fa-plus"></i></button>
                </div>
                <div class="large-1 columns">
                    <label>Fillin</label>
                    <button id="addFillin" class="button"><i class="fa fa-plus"></i></button>
                </div>
                <div class="large-1 columns">
                    <label>Dropdown</label>
                    <button id="addDropdown" class="button"><i class="fa fa-plus"></i></button>
                </div>
        </div>


        <!---Things get appended here.---->
        <div id="insert-area" class="row large-12 columns">

        </div>



        <!---Cancel / Submit button.--->
        <div class="row large-12 columns">
            <div class="large-2 large-offset-4 columns">
                <button class="button alert expanded" data-close="" aria-label="close reveal" type="button">Cancel</button>
            </div>
            <div class="large-2 columns">
                <input type="submit" class="button expanded" value="Submit"/>
            </div>
        </div>

    </form>

    <div id="date-hidden">
        <div id="date-prebuilt" style="display: none;" class="row large-12 columns">
            <div class="large-1 columns">
                <button class="button" id="delete" style="background-color: red;"><i style="color: white" class="fa fa-times"></i></button>
            </div>
                <div class="large-11 columns">
               <input required type="text" name="date[]" placeholder="Enter Date Name"/>
            </div>
        </div>
    </div>
    <div id="fillin-hidden">
        <div id="fillin-prebuilt" style="display: none;" class="row large-12 columns">
            <div class="large-1 columns">
                <button class="button" id="delete" style="background-color: red;"><i style="color: white" class="fa fa-times"></i></button>
            </div>
            <div class="large-11 columns">
                <input required type="text" name="fillin[]" placeholder="Enter Name"/>
            </div>
        </div>
    </div>
    <div id="dropdown-hidden">
        <div id="dropdown-prebuilt" style="display: none;" class="row large-12 columns">
            <div class="large-1 columns">
            <button type="button" onclick="delete_dropdown_create(this);" class="button radius" style="background-color: red;">
                <i style="color: white;" class="fa fa-times"></i>
            </button>
            </div>
            <div class="large-4 columns">
                <input required class="dropdownName" value="" placeholder="Enter Dropdown Name"/>
            </div>
            <div class="large-1 columns">
                <button id="addOption" type="button" onclick="add_option(this);" class="small button">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
    </div>

    <div id="delete-button-prefab" class="row large-12 columns" style="display: none;">
        <div class="large-1 columns">
            <button id="delete" class="button radius" style="background-color: red;">
                <i style="color: white;" class="fa fa-times"></i>
            </button>
        </div>
    </div>


</div>

<div id="edit-asset-modal" class="reveal callout text-center small"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Edit Asset Type</h4>
    <hr>
    <form id="edit-asset-form" class="edit-asset-form text-left" action="#"
          method="post">
        <input style="display: none;" name="asset_type_id" readonly id="asset_type_id"/>
        <div class="row">
            <div class="large-1 columns">
                <label>Select
                    <div id="my-icon-select-edit" class="icon-select"></div>
                </label>
            </div>
            <div class="large-11 columns">
                <label>Asset Name</label>
                <input required type="text" placeholder="Asset Name" name="asset-name" id="assetName">
            </div>
        </div>







        <div class="row">
            <div class="large-1 columns">
                <label>Date</label>
                <button id="addDateEdit" class="button"><i class="fa fa-plus"></i></button>
            </div>
            <div class="large-1 columns">
                <label>Fillin</label>
                <button id="addFillinEdit" class="button"><i class="fa fa-plus"></i></button>
            </div>
            <div class="large-1 columns">
                <label>Dropdown</label>
                <button id="addDropdownEdit" class="button"><i class="fa fa-plus"></i></button>
            </div>
            <div class="large-1 columns large-offset-8">
                <label>DELETE</label>
                <button id="deleteAsset" class="button" type="button" style="background-color: red;"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <!----Stuff gets Appended here.----->
        <div class="row large-12 columns" id="insert-area-edit">
        </div>

        <div class="row">
            <div class="large-4 columns button-group expanded" style="margin: 0 auto;">
                <a id="CancelEdit" class="button alert">Cancel</a>
                <input type="submit" class="button" value="Submit"/>
            </div>
        </div>

    </form>
</div>


<link rel="stylesheet" type="text/css" href="css/iconselect.css">
<script defer type="text/javascript" src="js/iconselect.js"></script>
<script defer type="text/javascript" src="js/iscroll.js"></script>
<script defer>
    var iconSelect;

    window.onload = function(){

        iconSelect = new IconSelect("my-icon-select",
            {'selectedIconWidth':23,
                'selectedIconHeight':23,
                'selectedBoxPadding':1,
                'iconsWidth':48,
                'iconsHeight':48,
                'boxIconSpace':1,
                'vectoralIconNumber':2,
                'horizontalIconNumber':6});

        var icons = [];
        <?php
        $getIcon = select_editAssetInfo_getIcon();
//            $fvmdb->query("
//                SELECT eac.asset_icon_id, eac.image
//                FROM emma_asset_icons as eac
//                ");
        while($Icon = $getIcon->fetch_assoc())
        {
            echo 'icons.push({"iconFilePath":"'.$Icon['image'].'", "iconValue":"'.$Icon['asset_icon_id'].'"});';
        }
        ?>
        //icons.push({'iconFilePath':'img/fire.png', 'iconValue':'1'});

        iconSelect.refresh(icons);





    var iconselect;


        iconselect = new IconSelect("my-icon-select-edit",
            {'selectedIconWidth':23,
                'selectedIconHeight':23,
                'selectedBoxPadding':1,
                'iconsWidth':48,
                'iconsHeight':48,
                'boxIconSpace':1,
                'vectoralIconNumber':2,
                'horizontalIconNumber':6});

        var Icons = [];
        <?php
        $getIcons = select_editAssetInfo_getIcon();
//            $fvmdb->query("
//                SELECT eac.asset_icon_id, eac.image
//                FROM emma_asset_icons as eac
//                ");
        while($icon = $getIcons->fetch_assoc())
        {
            echo 'Icons.push({"iconFilePath":"'.$icon['image'].'", "iconValue":"'.$icon['asset_icon_id'].'"});';
        }
        ?>
        //icons.push({'iconFilePath':'img/fire.png', 'iconValue':'1'});

        iconselect.refresh(Icons);

    };





</script>



<div id="success-create-modal" class="reveal callout success text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <form action="./dashboard.php?content=edit_asset_info" method="get">
        <input type="hidden" name="content" value="edit_asset_info"/>
        <input class="button success" type="submit" value="OK"/>
    </form>
</div>

<div id="check-delete-modal" class="reveal callout alert text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Are you sure Delete?</h4>
    <div class="button-group expanded">
        <input type="hidden" name="content" value="edit_asset_info"/>
        <input class="button" type="button" value="Cancel"/>
        <input class="button alert" id="confirm_delete" type="button" value="Delete"/>
    </div>
</div>


