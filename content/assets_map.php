<?php
include('../js/assets_map.js');
$events = select_assetsMap_events($USER->emma_plan_id);
//    $fvmdb->query("
//    SELECT e.*
//    FROM emma_assets e
//    WHERE e.emma_plan_id = '" . $USER->emma_plan_id . "'
//");
//Select all AED locations.
$aeds = select_assetsMap_aeds();
//    $fvmdb->query("
//SELECT a.latitude,a.longitude,a.location,a.sitecoordinator,c.name as contact_name, c.company, loc.address
//FROM aeds a
//JOIN location_contacts lc on lc.location_id = a.location_id
//JOIN contacts c on c.id = lc.contact_id
//JOIN locations as loc on loc.id = a.location_id
//where a.current = 1
//");
echo '<script>';
echo 'let positions = [];';
while($aed = $aeds->fetch_assoc())
{
    //If the name of the location contains valid characters and isn't empty.
    if(!preg_match('/[^A-Za-z0-9]/', $aed['location']) && !empty($aed['location']))
    {
        if((!empty($aed['latitude']) && !empty($aed['longitude'])) && ($aed['latitude'] != "0" && $aed['longitude'] != "0") && (!empty($aed['location'])))
        {
            echo 'var pos = {lat: '.$aed['latitude'].', lng: '.$aed['longitude'].', location: "'.$aed['location'].'", coordinator: "'.$aed['sitecoordinator'].'", contact: "'.$aed['contact_name'].'", company: "'.$aed['company'].'", address: "'.$aed['address'].'"};';
            echo 'positions.push(pos);';
        }
    }


}
echo '</script>';

echo '<div class="title row expanded">
        <input hidden id="loc_address" value=""/>
        <div class="large-6 columns">
            <h2 class="text-left"><a href="dashboard.php?content=assets"><i class="page-icon fa fa-map-marker"></i> '. ucwords(str_replace('_', ' ', $content)) .'</a></h2>
        </div>
        <div class="large-6 columns">
            <a style="float: right" href="dashboard.php?content=assets" class="button">Back</a>  
        </div>
                
</div>';

if ($event = $events->fetch_assoc()) { ?>
    <script type="text/javascript">
        var mainEvent = <?php echo json_encode($event); ?>;
        var planid = <?php echo json_encode($USER->emma_plan_id); ?>;
    </script>
    <div id="asset-map" style="width: 100%; height: 700px"></div>
    <!--    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDS_cKQ5EnelY2rUO6JOIQKOeyil5b7enw&callback=initMap"></script>-->
<?php } ?>