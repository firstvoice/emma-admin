<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 6/17/2019
 * Time: 12:24 PM
 */
$id = $fvmdb->real_escape_string($_GET['id']);
$events = select_callMap_events($id);
//$fvmdb->query(sprintf("
//    SELECT ecl.call_id, ecl.emergency_id, ecl.call_datetime, ecl.active, ecl.latitude, ecl.longitude, et.name AS type, CONCAT(u.firstname, ' ', u.lastname) AS username, u.id AS userid, u.username AS useremail, u.phone, e.drill, et.img_filename as type_filename
//    FROM emma_911_call_log AS ecl
//    JOIN users u on ecl.userid = u.id
//    LEFT JOIN emergencies e on ecl.emergency_id = e.emergency_id
//    LEFT JOIN emergency_types et on et.emergency_type_id = e.emergency_type_id
//    WHERE ecl.call_id = '".$id."'
//", $id));

$populate_events = select_callMap_populateEvents($USER->emma_plan_id);
//    $fvmdb->query("
//    SELECT e.*, et.name AS type, et.img_filename
//    FROM emergencies e
//    JOIN emergency_types et ON e.emergency_type_id = et.emergency_type_id
//    WHERE e.active = '1'
//    AND e.emma_plan_id = '" . $USER->emma_plan_id . "'
//    ");

echo '<script>';
echo 'let events_arr = [];';
while ($populate_event = $populate_events->fetch_assoc()) {
//If the name of the location contains valid characters and isn't empty.
    if((!empty($populate_event['latitude'])) && ($populate_event['latitude'] != 0) && ($populate_event['latitude'] != 1) && (!empty($populate_event['longitude'])) && ($populate_event['longitude'] != 0) & ($populate_event['longitude'] != 1)) {
        echo 'var evnt = {lat: ' . $populate_event['latitude'] . ', lng: ' . $populate_event['longitude'] . ', type: "' . $populate_event['type'] . '", img: "'.$populate_event['img_filename'].'"};';
        echo 'events_arr.push(evnt);';
    }

}
echo '</script>';
//Select all AED locations.
$aeds = select_callMap_aeds();
//    $fvmdb->query("
//SELECT a.latitude,a.longitude,a.location,a.sitecoordinator,c.name as contact_name, c.company, loc.address
//FROM aeds a
//JOIN location_contacts lc on lc.location_id = a.location_id
//JOIN contacts c on c.id = lc.contact_id
//JOIN locations as loc on loc.id = a.location_id
//where a.current = 1
//");

echo '<script>';
echo 'let positions = [];';
echo 'let assets = [];';
while($aed = $aeds->fetch_assoc())
{
    //If the name of the location contains valid characters and isn't empty.
    if(!preg_match('/[^A-Za-z0-9]/', $aed['location']) && !empty($aed['location']))
    {
        if((!empty($aed['latitude']) && !empty($aed['longitude'])) && ($aed['latitude'] != "0" && $aed['longitude'] != "0") && (!empty($aed['location'])))
        {
            echo 'var pos = {lat: '.$aed['latitude'].', lng: '.$aed['longitude'].', location: "'.$aed['location'].'", coordinator: "'.$aed['sitecoordinator'].'", contact: "'.$aed['contact_name'].'", company: "'.$aed['company'].'", address: "'.$aed['address'].'"};';
            echo 'positions.push(pos);';
        }
    }
}
echo '</script>';

//Select all ASSET locations.
$assets = select_callMap_assets($USER->emma_plan_id);
//    $fvmdb->query("
//SELECT eat.name, a.latitude as lat, a.longitude as lng, a.address, eai.image
//FROM emma_assets a
//LEFT JOIN emma_asset_types as eat on eat.emma_asset_type_id = a.emma_asset_type_id
//LEFT JOIN emma_asset_icons as eai on eat.emma_asset_icon_id = eai.asset_icon_id
//WHERE a.active = '1'
//AND a.emma_plan_id = '".$USER->emma_plan_id."'
//AND a.active = 1
//AND a.deleted = 0
//");

echo '<script>';
while ($asset = $assets->fetch_assoc()) {
//If the name of the location contains valid characters and isn't empty.
    if((!empty($asset['lat'])) && ($asset['lat'] != 0) && ($asset['lat'] != 1) && (!empty($asset['lng'])) && ($asset['lng'] != 0) && ($asset['lng'] != 1) && ($asset['address'] != null) && ($asset['address'] != '')) {
        echo 'var ast = {lat: ' . $asset['lat'] . ', lng: ' . $asset['lng'] . ', name: "' . $asset['name'] . '", address: "'.$asset['address'].'", image: "'.$asset['image'].'"};';
        echo 'assets.push(ast);';
    }
}
echo '</script>';

if ($event = $events->fetch_assoc()) { ?>
    <script type="text/javascript">
        var mainEvent = <?php echo json_encode($event); ?>;
        var planid = <?php echo json_encode($USER->emma_plan_id); ?>;
    </script>
    <div id="event-map" style="width:100%; height: 800px;"></div>
    <!--    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDS_cKQ5EnelY2rUO6JOIQKOeyil5b7enw&callback=initMap"></script>-->
<?php } ?>