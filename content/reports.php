<?php

if (!($USER->privilege->admin)) {
    redirect();
}

?>
<div class="title row expanded align-middle">
  <div class="columns medium-2">
    <h2 class="text-left"><a href="./dashboard.php?content=reports"><i class="page-icon fa fa-pie-chart"></i> <?php echo ucwords($content); ?></a></h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
  </div>
</div>
<div class="row expanded">
  <div class="large-4 medium-6 small-12 columns">
    <div class="card-info primary">
      <div class="card-info-content">
        <h3 class="lead">Generate Report</h3>
        <form method="POST" action="download_reports.php" target="_blank" id="reports">
            <div class="row">
                <div class="large-9 medium-8 small-6 columns">
                    <label>Report Type
                      <input type="hidden" name="plan" value="<?php echo $USER->emma_plan_id;?>">
                        <select name="report" class="expanded">
                            <option value="">-Select-</option>
<!--                            <option value="asset_type">Asset Types</option>   NEEDS WORKED ON -->
                            <option value="assets">Assets</option>
                            <option value="drills">Drills</option>
                            <option value="emma_sos">EMMA SOS</option>
                            <option value="events">Events</option>
                            <option value="geofences">Geofences</option>
                            <option value="lockdown">LOCKDOWN!</option>
                            <option value="mass_communications">Mass Notifications</option>
                            <option value="securities">Securities</option>
                            <option value="users">Users</option>
                        </select>
                    </label>
                </div>
                <div class="large-3 medium-4 small-6 columns" style="margin-top: 25px">
                    <button type="submit" class="button expanded" style="float: right">Submit</button>
                </div>
            </div>
        </form>
      </div>
    </div><!--/ Message Center -->
  </div>
</div>