<?php


$geofence_id = $fvmdb->real_escape_string($_GET['id']);

$geofences = select_editGeofences_geofences($geofence_id);
//    $fvmdb->query("
//    SELECT g.*
//    FROM emma_geofence_locations g
//    WHERE g.id = '" . $geofence_id . "'
//");
if (!$geofences->num_rows > 0) {
    echo '
    <div class="row expanded title">
        <div class="large-8 medium-12">
            <h2 class="text-left">Edit Geofence - Error</h2>
        </div>
    </div>
    <div>
        <h4 class="text-center">Failed to load geofence, please try again.</h4>
    </div>
    ';
    exit();
}

$geofence = $geofences->fetch_assoc();

?>


<!----Hidden inputs that we can pull values off of when we switch out dropdown type------>
<?php
if($geofence['type'] == 'Circle')
{
    echo '<input hidden id="saved_center_lat" name="center-lat" value="';if(empty($geofence['center_lat'])){echo '';}else{echo $geofence['center_lat'];} echo' ">';
    echo '<input hidden id="saved_center_lng" name="center-lng" value="'; if(empty($geofence['center_lng'])){echo '';}else{echo $geofence['center_lng'];} echo' ">';
    echo '<input hidden id="saved_radius" name="radius" value="'; if(empty($geofence['radius'])){echo '';}else{echo $geofence['radius'];} echo' "/>';

}
else if($geofence['type'] == 'Rectangle')
{
    echo '<input id="saved_nw_lat" hidden name="nw-lat" value="'; if(empty($geofence['nw_corner_lat'])){echo '';}else{echo $geofence['nw_corner_lat'];} echo '">';
    echo '<input id="saved_nw_lng" hidden name="nw-lng" value="'; if(empty($geofence['nw_corner_lng'])){echo '';}else{echo $geofence['nw_corner_lng'];} echo '">';
    echo '<input id="saved_se_lat" hidden name="sw-lat" value="'; if(empty($geofence['se_corner_lat'])){echo '';}else{echo $geofence['se_corner_lat'];} echo '">';
    echo '<input id="saved_se_lng" hidden name="sw-lng" value="'; if(empty($geofence['se_corner_lng'])){echo '';}else{echo $geofence['se_corner_lng'];} echo '">';
}


?>


<form id="edit_geofence_form">
    <input type="hidden" name="id" value="<?php echo $geofence_id; ?>">
    <div class="title row expanded align-middle">
        <div class="columns medium-8">
            <h2 class="text-left"><a href="./dashboard.php?content=geofences"><i class="page-icon fa fa-globe"></i> Edit Geofence - <?php echo $geofence['fence_name']; ?></a></h2>
        </div>
        <div class="columns show-for-medium"></div>
        <div class="columns shrink">
            <input type="submit" class="button" value="Save" style="margin:0;">
        </div>
    </div>

    <div class="row expanded">
        <div class="large-8 columns">
            <div class="card-info primary">
                <div class="card-info-content">
                    <h3 class="lead">Details</h3>
                    <div class="row">
                        <div class="large-6 medium-12 small-12 column">
                            <label>Name
                                <input type="Text" name="name" value="<?php echo $geofence['fence_name']; ?>"/>
                            </label>
                        </div>
                        <div class="large-3 medium-6 small-6 column">
                            <label>Type
                                <select id="dropdown_type" name="type">
                                    <option <?php if($geofence['type'] == 'Circle'){echo 'Selected';} ?> value="Circle">Circle</option>
                                    <option <?php if($geofence['type'] == 'Rectangle'){echo 'Selected';}?> value="Rectangle">Rectangle</option>

                                </select>
                            </label>
                        </div>
                        <div class="large-3 medium-6 small-6 column">
                            <label>Main Fence
                                <select name="main">
                                    <option value="">-None-</option>
                                    <?php
                                    $fences = select_editGeofences_fences($USER->emma_plan_id);
//                                        $fvmdb->query("
//                                            SELECT g.id, g.fence_name
//                                            FROM emma_geofence_locations g
//                                            WHERE g.plan_id = '". $USER->emma_plan_id ."'
//                                            ORDER BY g.fence_name
//                                        ");
                                    while($fence = $fences->fetch_assoc()){
                                        if($fence['id'] == $geofence['main_fence']){
                                            echo'<option value="'. $fence['id'] .'" selected>'. $fence['fence_name'] .'</option>';
                                        }else{
                                            echo'<option value="'. $fence['id'] .'">'. $fence['fence_name'] .'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </label>
                        </div>
                    </div>
                    <div id="circle_append_here" class="row">

                    </div>
                    <div id="rectangle_first_append_here" class="row">

                    </div>
                    <!---------
                    <div class="row">
                        <div class="large-12 medium-12 small-12 column">
                            <label class="text-center"><b>NE Corner Coordinates</b></label>
                            <div class="row">
                                <div class="large-6 medium-6 small-6 column">
                                    <label>Latitude
                                        <input type="text" name="ne-lat" value="<?php echo $geofence['ne_corner_lat'];?>">
                                    </label>
                                </div>
                                <div class="large-6 medium-6 small-6 column">
                                    <label>Longitude
                                        <input type="text" name="ne-lng" value="<?php echo $geofence['ne_corner_lng'];?>">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>------->
                    <div id="rectangle_second_append_here" class="row">

                    </div>
                    <!-------
                    <div class="row">
                        <div class="large-12 medium-12 small-12 column">
                            <label class="text-center"><b>SE Corner Coordinates</b></label>
                            <div class="row">
                                <div class="large-6 medium-6 small-6 column">
                                    <label>Latitude
                                        <input type="text" name="se-lat" value="<?php echo $geofence['se_corner_lat'];?>">
                                    </label>
                                </div>
                                <div class="large-6 medium-6 small-6 column">
                                    <label>Longitude
                                        <input type="text" name="se-lng" value="<?php echo $geofence['se_corner_lng'];?>">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>---------->
                </div>
            </div>
        </div>
    </div>
</form>

<div id="success_modal" class="reveal success callout tiny text-center" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <a href="./dashboard.php?content=geofence&id=<?php echo $geofence_id; ?>" data-close class="button success" style="margin-left:auto;margin-right:auto">Ok</a>
    <!--    <button class="close-button" data-close aria-label="Close reveal" type="button">-->
    <!--        <span aria-hidden="true">&times;</span>-->
    <!--    </button>-->
</div>