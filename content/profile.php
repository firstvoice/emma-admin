<?php
/**
 * Created by PhpStorm.
 * User: John
 * Date: 12/6/2017
 * Time: 20:48
 */
$emmaUsers = $emmadb->query("
  select * from users
  where id = ".$USER->id."
");
$emmaUser = $emmaUsers->fetch_assoc();

$fvmUsers = $fvmdb->query("
  select * from users
  where id = ".$USER->fvm_id."
");
?>
    <div class="title row expanded align-middle">
        <div class="columns medium-2">
            <h2 class="text-left"><a href="./dashboard.php"><i class="page-icon fa fa-user-circle"></i> <?php echo ucwords($content); ?></a></h2>
        </div>
        <div class="columns show-for-medium"></div>
        <div class="columns shrink">
            <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
                data-options="disableHover:true;clickOpen:true;">
                <li>
                    <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
                    <ul class="menu">
                        <li><a href="./dashboard.php?content=edit_user&id=<?php echo $emmaUser['id']; ?>" id="edit_create">Edit
                                User</a></li>
                        <!--                        <li><a data-open="delete-user-modal">Delete User</a></li>-->
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <div class="row expanded">
        <div class="large-6 medium-12 column">
            <div class="card-info primary">
                <div class="card-info-content">
                    <h2 class="lead">EMMA User Profile</h2>
                    <hr>
                    <div class="row">
                        <div class="columns small-12">
                          <p>Name: <?php echo $emma_user['firstname'] . ' ' . $emma_user['lastname']; ?></p>
                        </div>
                        <div class="columns large-4 medium-12">
                            <p>Title: <?php if (!empty($emma_user['title'])) {
                                    echo $emma_user['title'];
                                } else {
                                    echo 'None';
                                } ?></p>
                        </div>
                        <div class="columns large-4 medium-12">
                            <p>Landline: <?php if (!empty($emma_user['landline_phone'])) {
                                    echo $emma_user['landline_phone'];
                                } else {
                                    echo 'None';
                                } ?></p>
                        </div>
                        <div class="columns large-4 medium-12">
                            <p>Cell: <?php if (!empty($emma_user['phone'])) {
                                    echo $emma_user['phone'];
                                } else {
                                    echo 'None';
                                } ?></p>
                        </div>
                        <div class="columns large-8 small-12">
                            <p>Email: <?php if (!empty($emma_user['username'])) {
                                    echo "<a href='mailto:" . $emma_user['username'] . "'>" . $emma_user['username'] . "</a>";
                                } else {
                                    echo 'None';
                                } ?></p>
                        </div>
                        <div class="columns large-4 medium-12">
                            <p>Email Confirmed: <?php echo $activation_status; ?></p>
                        </div>
                        <div class="large-6 small-12 columns">
                            <p>
                                Address: <?php echo((empty($emma_user['address'])) ? 'No record.' :
                                    $emma_user['address'] . ', ' . $emma_user['city'] . ', ' . $emma_user['province'] . ' ' . $emma_user['zip']); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <div class="large-6 medium-12 column">
        <div class="card-info primary">
          <?php if(mysqli_num_rows($fvmUsers) == 1) {
            $fvmUser = $fvmUsers->fetch_assoc();
            echo '
            <div class="card-info-content">
              <h3 class="lead">FVM User Profile</h3>
              <hr>
              <div class="row">
                <div class="columns small-12">
                  <p>Name: '.$fvmUser['firstname'] . ' ' . $fvmUser['lastname'].'</p>
                </div>
                <div class="columns large-4 medium-12">
                  <p>Username: '.$fvmUser['username'].'</p>
                </div>
                <div class="columns large-4 medium-12">
                  <p>Phone: '.$fvmUser['phone'].'</p>
                </div>
                <div class="large-6 small-12 columns">
                  <p>Address: '.$fvmUser['address'].'</p>
                </div>
              </div>
            </div>
            ';
          } else {
            echo '
            <div class="card-info-content">
              <h3 class="lead">Link FVM Acount</h3>
              <hr>
              <form id="link-fvm-form" action="#">
                <input type="hidden" name="user-id" value="'.$USER->id.'" />
                <label>Username
                  <input type="text" name="username" />
                </label>
                <label>Password
                  <input type="password" name="password" />
                </label>
                <input class="button" type="submit" value="Link" />
              </form>
            </div>
            ';
          }?>
        </div>
      </div>
    </div>
    <div id="delete-user-modal" class="reveal callout alert text-center tiny" data-reveal
         data-animation-in="fade-in"
         data-animation-out="fade-out">
        <form id="delete-user-form" action="#" method="post" onsubmit="return false;">
            <input type="hidden" name="user-id" value="<?php echo $USER->id; ?>" />
            <h4 style="color:darkred">Are you sure?</h4>
            <div class="large-4 columns" style="margin: 0 auto;">
                <div class="button-group expanded">
                    <a class="button alert" data-close>Cancel</a>
                    <input class="button alert" type="submit" value="OK" />
                </div>
            </div>
        </form>
    </div>
    <div id="success_modal" class="reveal success callout tiny text-center"
         data-reveal data-animation-in="fade-in"
         data-animation-out="fade-out">
        <h4>Success</h4>
        <a href="./dashboard.php?content=users" data-close
           class="button success" style="margin-left:auto;margin-right:auto">Ok</a>
        <!--    <button class="close-button" data-close aria-label="Close reveal"-->
        <!--      type="button">-->
        <!--      <span aria-hidden="true">&times;</span>-->
        <!--    </button>-->
    </div>



