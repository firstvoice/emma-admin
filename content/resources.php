<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/8/2017
 * Time: 9:18 AM
 */

$resources = select_resources_resources();
//    $fvmdb->query("
//  SELECT *
//  FROM emma_resource_types
//  WHERE active = 1
//");
$count = 0;

//if (!($USER->privilege->security || $USER->privilege->admin || $USER->privil)) {
 //   redirect();
//}

?>

<div class="title row expanded align-middle">
  <div class="columns medium-3">
    <h2 class="text-left"><a
        href="./dashboard.php?content=resources"><i class="page-icon fa fa-map"></i> <?php echo ucwords($content); ?></a>
    </h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
      data-options="disableHover:true;clickOpen:true;">
      <li>
        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <ul class="menu">
          <li><a href="#" data-open="add-resource-modal">New Resource</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>

<div class="content-body margin-top-1em">
  <div class="row expanded">
    <?php
    while ($resource = $resources->fetch_assoc()) {
      $name = $resource['resource_name'];
      $altered_name = str_replace(' ', '_', $name);
      $altered_name = strtolower($altered_name);
      ?>
      <div class="large-2 small-12 medium-6 column margin-bottom-1EM">
        <a class="dashboard-nav-card"
          href="./dashboard.php?content=resource_type&resource_type=<?php echo $resource['emma_resource_type_id']; ?>">
          <i class="dashboard-nav-card-icon fa fa-folder"
            aria-hidden="true"></i>
          <h5 class="dashboard-nav-card-title"><?php echo $name; ?></h5>
        </a>
      </div>
      <?php
    }
    if($USER->privilege->security || $USER->privilege->admin) {
    ?>
        <div class="large-2 small-12 medium-6 column margin-bottom-1EM">
            <button class="dashboard-nav-card" data-open="choose_scripts_modal">
                <i class="dashboard-nav-card-icon fa fa-folder" aria-hidden="true"></i>
                <h5 class="dashboard-nav-card-title">Scripts</h5>
            </button>
        </div>
    <?php
    }
    ?>
  </div>
  <div class="small-12 column">
    <div>
      <h4>Most Used</h4>
    </div>
  </div>
  <div class="large-12 row expanded image-row">
    <?php
    $most_used_query = select_resources_mostUsed($USER->emma_plan_id);
//        $fvmdb->query("
//        SELECT r.*
//        FROM emma_resources AS r
//        WHERE r.emma_plan_id = '" . $USER->emma_plan_id . "'
//        AND r.active = 1
//        ORDER BY view_count DESC
//        LIMIT 6
//    ");
    if ($most_used_query->num_rows < 1) {
      echo '<p>No recent activity</p>';
    } else {
      while ($most_used_resource = $most_used_query->fetch_assoc()) {
        if (file_exists($most_used_resource['file_string'])) {
          if ($most_used_resource['file_extension'] == 'png') {
            echo '<div class="large-2 column">
                    <a href="./dashboard.php?content=resource&id=' .
              $most_used_resource['emma_resource_id'] . '">
                        <img class="thumbnail" src="' .
              $most_used_resource['file_string'] . '" />
                    </a>
                    <a href="./dashboard.php?content=resource&id=' .
              $most_used_resource['emma_resource_id'] . '">
                        <div class="small-12 text-center" style="padding-top: 1em;">' .
              $most_used_resource['file_name'] . '</div>
                    </a>
                    </div>
                    ';
          } else if ($most_used_resource['file_extension'] == 'pdf') {
            echo '
                    <div class="large-2 column">
                   
                        <object data="' . $most_used_resource['file_string'] . '" type="application/pdf" width="100%" height="90%">
                           alt : <a href="./dashboard.php?content=resource&id=' .
              $most_used_resource['emma_resource_id'] . '">' .
              $most_used_resource['file_name'] . '</a>
                        </object>
                    
                        <a href="./dashboard.php?content=resource&id=' .
              $most_used_resource['emma_resource_id'] . '">
                            <div class="small-12 text-center" style="padding-top: 1em;">' .
              $most_used_resource['file_name'] . '</div>
                        </a>
                    </div>';

          } else if ($most_used_resource['file_extension'] == 'docx') {
            echo '<div class="large-2 column">
                    <a href="./dashboard.php?content=resource&id=' .
              $most_used_resource['emma_resource_id'] . '">
                        <img class="thumbnail placeholder" src="resource_folder/placeholder_icons/word.png" width="100%" height="90%" />
                    </a>
                    <a href="./dashboard.php?content=resource&id=' .
              $most_used_resource['emma_resource_id'] . '">
                        <div class="small-12 text-center" style="padding-top: 1em;">' .
              $most_used_resource['file_name'] . '</div>
                    </a>
                    </div>
                    ';
          } else if ($most_used_resource['file_extension'] == 'xlsx') {
            echo '<div class="large-2 column">
                    <a href="./dashboard.php?content=resource&id=' .
              $most_used_resource['emma_resource_id'] . '">
                        <img class="thumbnail placeholder" src="resource_folder/placeholder_icons/excel.png" width="100%" height="90%" />
                    </a>
                    <a href="./dashboard.php?content=resource&id=' .
              $most_used_resource['emma_resource_id'] . '">
                        <div class="small-12 text-center" style="padding-top: 1em;">' .
              $most_used_resource['file_name'] . '</div>
                    </a>
                    </div>
                    ';
          }
        }
      }
    }

    ?>
  </div>
  <div class="small-12 column" style = "margin-top: 3em;">
    <div>
      <h4>Recent Uploads</h4>
    </div>
  </div>
  <div class="large-12 row expanded image-row" style="padding-bottom: 4rem;">
    <?php
    $most_recent_query = select_resources_mostRecent($USER->emma_plan_id);
//        $fvmdb->query("
//        SELECT r.*
//        FROM emma_resources AS r
//        WHERE r.emma_plan_id = " . $USER->emma_plan_id . "
//        AND r.active = 1
//        ORDER BY r.created_date DESC
//        LIMIT 6
//    ");
    if ($most_recent_query->num_rows < 1) {
      echo '<p>No recent activity</p>';
    } else {
      while ($most_recent_resource = $most_recent_query->fetch_assoc()) {
        if (file_exists($most_recent_resource['file_string'])) {
          if ($most_recent_resource['file_extension'] == 'png') {
            echo '<div class="large-2 column">
                    <a href="./dashboard.php?content=resource&id=' .
              $most_recent_resource['emma_resource_id'] . '">
                        <img class="thumbnail" src="' .
              $most_recent_resource['file_string'] . '" />
                    </a>
                    <a href="./dashboard.php?content=resource&id=' .
              $most_recent_resource['emma_resource_id'] . '">
                        <div class="small-12 text-center" style="padding-top: 1em;">' .
              $most_recent_resource['file_name'] . '</div>
                    </a>
                    </div>
                    ';
          } else if ($most_recent_resource['file_extension'] == 'pdf') {
            echo '
                    <div class="large-2 column">
                        <a href="./dashboard.php?content=resource&id=' .
              $most_recent_resource['emma_resource_id'] . '">
                            <object data="' .
              $most_recent_resource['file_string'] . '" type="application/pdf" width="100%" height="90%">
                               alt : <a href="./dashboard.php?content=resource&id=' .
              $most_recent_resource['emma_resource_id'] . '">' .
              $most_recent_resource['file_name'] . '</a>
                            </object>
                        </a>
                        <a href="./dashboard.php?content=resource&id=' .
              $most_recent_resource['emma_resource_id'] . '">
                            <div class="small-12 text-center" style="padding-top: 1em;">' .
              $most_recent_resource['file_name'] . '</div>
                        </a>
                    </div>';

          } else if ($most_recent_resource['file_extension'] == 'docx') {
            echo '<div class="large-2 column">
                    <a href="./dashboard.php?content=resource&id=' .
              $most_recent_resource['emma_resource_id'] . '">
                        <img class="thumbnail placeholder" src="resource_folder/placeholder_icons/word.png" width="100%" height="90%" />
                    </a>
                    <a href="./dashboard.php?content=resource&id=' .
              $most_recent_resource['emma_resource_id'] . '">
                        <div class="small-12 text-center">' .
              $most_recent_resource['file_name'] . '</div>
                    </a>
                    </div>
                    ';
          } else if ($most_recent_resource['file_extension'] == 'xlsx') {
            echo '<div class="large-2 column">
                    <a href="./dashboard.php?content=resource&id=' .
              $most_recent_resource['emma_resource_id'] . '">
                        <img class="thumbnail placeholder" src="resource_folder/placeholder_icons/excel.png" width="100%" height="90%" />
                    </a>
                    <a href="./dashboard.php?content=resource&id=' .
              $most_recent_resource['emma_resource_id'] . '">
                        <div class="small-12 text-center" style="padding-top: 1em;">' .
              $most_recent_resource['file_name'] . '</div>
                    </a>
                    </div>
                    ';
          }
        }
      }
    }

    ?>
  </div>
</div>

<div id="add-resource-modal" class="reveal callout large" data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <form id="new-resource-form" enctype="multipart/form-data" action=""
    method="post">
    <input type="hidden" name="plan_id"
      value="<?php echo $USER->emma_plan_id; ?>"/>
    <input type="hidden" name="user_id" value="<?php echo $USER->id; ?>"/>
    <div class="text-center">
      <h4 class="lead">New Resource</h4>
    </div>
    <label>File:
      <input type="file" name="file"
        accept=".jpeg,.png,.gif,.doc,.docx,.xls,.xlsx,.csv,image/gif,image/png,image/jpeg,application/pdf"
        required/>
    </label>
    <label>File Name:
      <input type="Text" name="file_name" required/>
    </label>
    <label>Resource Category:
      <select name="file_type">
          <option value="">-Select-</option>
        <?php
        $resource_types = select_resources_resourceTypes();
//            $fvmdb->query("
//          SELECT *
//          FROM emma_resource_types
//          WHERE active = 1
//          ORDER BY resource_name
//        ");
        while ($resource_type = $resource_types->fetch_assoc()) {
          echo '<option value="' . $resource_type['emma_resource_type_id'] .
            '">' . $resource_type['resource_name'] . '</option>';
        }
        ?>
      </select>
    </label>

    <label>Site:
      <select name="site_id">
          <option value="">-Select-</option>
        <?php
        $sites = select_resources_sites($USER->emma_plan_id);
//            $fvmdb->query("
//          SELECT e.*
//          FROM emma_sites AS e
//          WHERE e.emma_plan_id = " . $USER->emma_plan_id . "
//        ");
        while ($site = $sites->fetch_assoc()) {
          echo '<option value="' . $site['emma_site_id'] . '">' .
            $site['emma_site_name'] . '</option>';
        }
        ?>
      </select>
    </label>


    <div class="text-center small-12">
      <input type="submit" value="Submit" class="button"/>
    </div>
    <button class="close-button" data-close aria-label="Close reveal"
      type="button">
      <span aria-hidden="true">&times;</span>
    </button>
  </form>
</div>
<div id="success_modal" class="reveal callout success text-center tiny"
  data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Success</h4>
  <a href="./dashboard.php?content=resources" data-close class="button success">Ok</a>
<!--  <button class="close-button" data-close aria-label="Close reveal"-->
<!--    type="button">-->
<!--    <span aria-hidden="true">&times;</span>-->
<!--  </button>-->
</div>
<div id="invalid_modal" class="reveal callout small" data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>File type is not allowed</h4>
  <div class="text-center small-12">
    <a data-close class="button">Ok</a>
  </div>
  <button class="close-button" data-close aria-label="Close reveal"
    type="button">
    <span aria-hidden="true">&times;</span>
  </button>
<!--</div> --><?php
//$query = $webdb->query("
// 			select *
// 			from national_accounts_nav_links n, national_accounts_packages p
// 			where p.national_accounts_nav_links_id = n.national_accounts_nav_links_id
//
// 		");
//
//if($row = $query->fetch_assoc()) {
//    echo '
//
//
//<div>
//	<nav class="top-bar" data-topbar>
//		<section class="top-bar-section">
//			<ul class="left">';
//    if($row['right_link1_title'] != "")
//        echo '
//				<li><a href="'.$row['left_link1_link'].'" target="'.$row['left_target_link1'].'">'.$row['left_link1_title'].'</a></li>';
//    else
//        echo '';
//    if($row['right_link1_title'] != "")
//        echo'
//				<li><a href="'.$row['left_link2_link'].'" target="'.$row['left_target_link2'].'">'.$row['left_link2_title'].'</a></li>';
//    else
//        echo '';
//    echo'
//
//			</ul>
//
//			<ul class="right">';
//    if($row['right_link1_title'] != "")
//        echo '
//				<li><a href="'.$row['right_link1_link'].'" target="'.$row['right_target_link1'].'">'.$row['right_link1_title'].'</a></li>';
//    else
//        echo '';
//
//    if($row['right_link2_title'] != "")
//        echo '
//				<li><a href="'.$row['right_link2_link'].'" target="'.$row['right_target_link2'].'">'.$row['right_link2_title'].'</a></li>';
//    else
//        echo '';
//
//    if($row['right_link3_title'] != "")
//        echo '
//				<li><a href="'.$row['right_link3_link'].'" target="'.$row['right_target_link3'].'">'.$row['right_link3_title'].'</a></li>';
//    else
//        echo '';
//
//    if($row['right_link4_title'] != "")
//        echo '
//				<li><a href="'.$row['right_link4_link'].'" target="'.$row['right_target_link4'].'">'.$row['right_link4_title'].'</a></li>';
//    else
//        echo '';
//
//    if($row['right_link5_title'] != "")
//        echo '
//				<li><a href="'.$row['right_link5_link'].'" target="'.$row['right_target_link5'].'">'.$row['right_link5_title'].'</a></li>';
//    else
//        echo '';
//
//    if($row['right_link6_title'] != "")
//        echo '
//				<li><a href="'.$row['right_link6_link'].'" target="'.$row['right_target_link6'].'">'.$row['right_link6_title'].'</a></li>';
//    else
//        echo '';
//    echo '
//			</ul>
//		</section>
//	</nav>
//</div>';
//}
//?>

<div id="choose_scripts_modal" class="reveal callout small" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4 style="text-align: center;">Choose Scripts To Visit</h4>
    <hr>
    <div class="button-group expanded">
        <div class="large-12 columns">
            <div class="row">
                <div class="large-3 columns">
                    <a href="./dashboard.php?content=scripts" class="button expanded">Emergency Scripts</a>
                </div>
                <div class="large-3 columns">
                    <a href="./dashboard.php?content=mass_notification_scripts" class="button expanded">Notification Scripts</a>
                </div>
                <div class="large-3 columns">
                    <a href="./dashboard.php?content=email_scripts" class="button expanded">Email Scripts</a>
                </div>
                <div class="large-3 columns">
                    <a href="./dashboard.php?content=text_scripts" class="button expanded">Text Scripts</a>
                </div>
            </div>
        </div>
    </div>
    <button class="close-button" data-close aria-label="Close reveal"
            type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div id="failed_modal" class="reveal callout alert text-center tiny" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4 style="color:darkred">Failed...</h4>
    <div id="error-list" class="text-left"></div>
    <a class="button" style="background-color: darkred" data-close>OK</a>
</div>
