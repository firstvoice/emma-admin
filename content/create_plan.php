<div class="title row expanded align-middle">
    <div class="columns medium-5">
        <h2 class="text-left"><a href="./dashboard.php?content=plans"><i class="page-icon fa fa-book"></i> Create
                Plan</a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
    </div>
</div>

<div>
    <ul class="tabs" data-tabs id="script-type-tabs">
        <li class="tabs-title is-active" id="tab1">
            <a href="#panel1" aria-selected="true" id="tabBut1">Plan</a>
        </li>
        <li class="tabs-title disabled" id="tab2">
            <a href="#panel2" id="tabBut2">Groups</a>
        </li>
        <li class="tabs-title disabled" id="tab3">
            <a href="#panel3" id="tabBut3">Sites</a>
        </li>
        <li class="tabs-title disabled" id="tab4">
            <a href="#panel4" id="tabBut4">Assets/Emergencies</a>
        </li>
    </ul>
    <form id="new-plan-form">
        <input type="hidden" name="user" value="<?php echo $USER->id;?>">
        <div class="tabs-content" data-tabs-content="script-type-tabs">
            <div class="tabs-panel is-active" id="panel1">
                <div class="row expanded">
                    <div class="large-12 medium-12 small-12 columns">
                        <div class="row">
                            <div class="large-4 medium-4 small-12 columns">
                                <label>Plan Name <span style="color: red">*</span>:
                                    <input type="text" id="name1" name="plan-name">
                                </label>
                            </div>
                            <div class="large-4 medium-4 small-12 columns">
                                <label>Date Active <span style="color: red">*</span>:
                                    <input type="date" id="active1" name="date-active">
                                </label>
                            </div>
                            <div class="large-4 medium-4 small-12 columns">
                                <label>Date Expired <span style="color: red">*</span>:
                                    <input type="date" id="expired1" name="date-expired">
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-3 medium-4 small-6 columns">
                                <div class="row" style="padding-top: 30px">
                                    <div class="large-3 columns">
                                        <div class="switch tiny">
                                            <input class="switch-input" id="masscommunication" type="checkbox" name="masscommunication">
                                            <label class="switch-paddle" for="masscommunication">
                                                <span class="show-for-sr">Mass Notification?</span>
                                                <span class="switch-active" aria-hidden="true">Yes</span>
                                                <span class="switch-inactive" aria-hidden="true">No</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="large-9 columns">
                                        <label class="text-left">Mass Notification</label>
                                    </div>
                                </div>
                            </div>
                            <div class="large-3 medium-4 small-6 columns">
                                <div class="row" style="padding-top: 30px">
                                    <div class="large-3 columns">
                                        <div class="switch tiny">
                                            <input class="switch-input" id="securities" type="checkbox" name="securities">
                                            <label class="switch-paddle" for="securities">
                                                <span class="show-for-sr">Securities?</span>
                                                <span class="switch-active" aria-hidden="true">Yes</span>
                                                <span class="switch-inactive" aria-hidden="true">No</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="large-9 columns">
                                        <label class="text-left">Security Upgrade</label>
                                    </div>
                                </div>
                            </div>
                            <div class="large-3 medium-4 small-6 columns">
                                <div class="row" style="padding-top: 30px">
                                    <div class="large-3 columns">
                                        <div class="switch tiny">
                                            <input class="switch-input" id="geofences" type="checkbox" name="geofences">
                                            <label class="switch-paddle" for="geofences">
                                                <span class="show-for-sr">Geofences?</span>
                                                <span class="switch-active" aria-hidden="true">Yes</span>
                                                <span class="switch-inactive" aria-hidden="true">No</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="large-9 columns">
                                        <label class="text-left">Geofences</label>
                                    </div>
                                </div>
                            </div>
                            <div class="large-3 medium-4 small-6 columns">
                                <div class="row" style="padding-top: 30px">
                                    <div class="large-3 columns">
                                        <div class="switch tiny">
                                            <input class="switch-input" id="panic" type="checkbox" name="panic">
                                            <label class="switch-paddle" for="panic">
                                                <span class="show-for-sr">Panic?</span>
                                                <span class="switch-active" aria-hidden="true">Yes</span>
                                                <span class="switch-inactive" aria-hidden="true">No</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="large-9 columns">
                                        <label class="text-left">EMMA SOS</label>
                                    </div>
                                </div>
                            </div>
                            <div class="large-3 medium-4 small-6 columns" id="duration-container" hidden>
                                <label> SOS Duration
                                    <input type="number" value="500" name="sosduration">
                                </label>
                            </div>
                            <div class="large-3 medium-4 small-6 columns" id="maxgeo-container" hidden>
                                <label> Maximum Geofences
                                    <input type="number" value="1" name="maxgeo">
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="medium-10 columns">

                            </div>
                            <div class="medium-2 columns">
                                <a class="button" id="step1complete" style="float: right">Next</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-panel" id="panel2">
                <div class="row expanded">
                    <div class="large-8 medium-8 small-6 columns">
                        <div class="card-info primary">
                            <div class="card-info-content">
                                <div class="row expanded" id="complete-group-warning">
                                    <div class="large-12 columns">
                                        <span style="color: red">Fields Marked * Are Required</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-6 medium-6 small-6 columns">
                                        <label>Group Name: <span style="color: red">*</span>
                                            <input type="text" class="temp-group" id="temp-groupname">
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <h3 class="lead">Permissions</h3>
                                </div>
                                <div class="row expanded">
                                    <div class="large-4 medium-4 small-12 columns">
                                        <div class="row">
                                            <div class="large-3 columns">
                                                <div class="switch tiny">
                                                    <input class="switch-input temp-group-switch" id="administrator" type="checkbox" name="administrator">
                                                    <label class="switch-paddle" for="administrator">
                                                        <span class="show-for-sr">Administrator?</span>
                                                        <span class="switch-active" aria-hidden="true">Yes</span>
                                                        <span class="switch-inactive" aria-hidden="true">No</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="large-9 columns">
                                                <label class="text-left">Administrator</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="large-4 medium-4 small-12 columns">
                                        <div class="row">
                                            <div class="large-3 columns">
                                                <div class="switch tiny">
                                                    <input class="switch-input temp-group-switch" id="event-info-only" type="checkbox">
                                                    <label class="switch-paddle" for="event-info-only">
                                                        <span class="show-for-sr">Event Information Only?</span>
                                                        <span class="switch-active" aria-hidden="true">Yes</span>
                                                        <span class="switch-inactive" aria-hidden="true">No</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="large-9 columns">
                                                <label class="text-left">Information Only</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="large-4 medium-4 small-12 columns" id="security-container" hidden>
                                        <div class="row">
                                            <div class="large-3 columns">
                                                <div class="switch tiny">
                                                    <input class="switch-input temp-group-switch" id="security" type="checkbox">
                                                    <label class="switch-paddle" for="security">
                                                        <span class="show-for-sr">Security?</span>
                                                        <span class="switch-active" aria-hidden="true">Yes</span>
                                                        <span class="switch-inactive" aria-hidden="true">No</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="large-9 columns">
                                                <label class="text-left">Security</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="large-4 medium-4 small-12 columns" id="geofences-container" hidden>
                                        <div class="row">
                                            <div class="large-3 columns">
                                                <div class="switch tiny">
                                                    <input class="switch-input temp-group-switch" id="geofence" type="checkbox">
                                                    <label class="switch-paddle" for="geofence">
                                                        <span class="show-for-sr">Geofences?</span>
                                                        <span class="switch-active" aria-hidden="true">Yes</span>
                                                        <span class="switch-inactive" aria-hidden="true">No</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="large-9 columns">
                                                <label class="text-left">Geofences</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="large-4 medium-4 small-12 columns" id="sos-container" hidden>
                                        <div class="row">
                                            <div class="large-3 columns">
                                                <div class="switch tiny">
                                                    <input class="switch-input temp-group-switch" id="sos" type="checkbox">
                                                    <label class="switch-paddle" for="sos">
                                                        <span class="show-for-sr">SOS?</span>
                                                        <span class="switch-active" aria-hidden="true">Yes</span>
                                                        <span class="switch-inactive" aria-hidden="true">No</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="large-9 columns">
                                                <label class="text-left">EMMA SOS</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="large-4 medium-4 small-12 columns">
                                        <div class="row">
                                            <div class="large-3 columns">
                                                <div class="switch tiny">
                                                    <input class="switch-input temp-group-switch" id="911admin" type="checkbox">
                                                    <label class="switch-paddle" for="911admin">
                                                        <span class="show-for-sr">911admin?</span>
                                                        <span class="switch-active" aria-hidden="true">Yes</span>
                                                        <span class="switch-inactive" aria-hidden="true">No</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="large-9 columns">
                                                <label class="text-left">911admin</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-10 medium-11 small-11 columns">

                                    </div>
                                    <div class="large-2 medium-1 small-1 columns text-right">
                                        <a id="temp-group-btn" class="button">+</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="large-4 medium-4 small-6 columns">
                        <div class="card-info primary">
                            <div class="card-info-content">
                                <div class="row">
                                    <h3 class="lead">Groups</h3>
                                </div>
                                <div class="row expanded">
                                    <div class="large-12 medium-12 small-12 columns" id="groups-container"
                                         style="margin-bottom: 5px">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-10 medium-10 columns">

                            </div>
                            <div class="large-2 medium-2 columns">
                                <a class="button" id="step2complete" style="float: right">Next</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-panel" id="panel3">
                <div class="row expanded">
                    <div class="large-3 medium-6 small-6 columns">
                        <label>Max Sites <span style="color: red">*</span>:
                            <input type="number" min="1" id="max" name="max" value="1">
                        </label>
                    </div>
                </div>
                <div class="row expanded" id="complete-site-warning">
                    <div class="large-12 columns">
                        <span style="color: red">Fields Marked * Are Required</span>
                    </div>
                </div>
                <div class="row expanded" id="max-site-warning">
                    <div class="large-12 columns">
                        <span style="color: red">You Have Reached Maximum Sites Allowed</span>
                    </div>
                </div>
                <div class="row expanded">
                    <div class="large-8 medium-8 small-12 columns">
                        <div class="card-info primary">
                            <div class="card-info-content">
                                <div class="row">
                                    <h5>Site Information</h5>
                                </div>
                                <div class="row">
                                    <div class="large-6 medium-12 column">
                                        <label>Site Name <span style="color: red">*</span>
                                            <input type="Text" id="temp-name" name="sitename" required/>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-4 medium-6 small-12 column">
                                        <label>Address <span style="color: red">*</span>
                                            <input type="Text" id="temp-address" name="address"/>
                                        </label>
                                    </div>
                                    <div class="large-4 medium-6 small-12 column">
                                        <label>City <span style="color: red">*</span>
                                            <input type="Text" id="temp-city" name="city"/>
                                        </label>
                                    </div>
                                    <div class="large-2 medium-6 small-12 column">
                                        <label>State <span style="color: red">*</span>
                                            <input type="Text" id="temp-state" name="state"/>
                                        </label>
                                    </div>
                                    <div class="large-2 medium-6 small-12 column">
                                        <label>Zip <span style="color: red">*</span>
                                            <input type="Text" id="temp-zip" name="zip"/>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <h5>Contact Information</h5>
                                </div>
                                <div class="row">
                                    <div class="large-6 medium-12 small-12 column">
                                        <label>First Name <span style="color: red">*</span>
                                            <input type="Text" id="temp-first" name="firstname"/>
                                        </label>
                                    </div>
                                    <div class="large-6 medium-12 small-12 column">
                                        <label>Last Name <span style="color: red">*</span>
                                            <input type="Text" id="temp-last" name="lastname"/>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-6 medium-12 column">
                                        <label>Email <span style="color: red">*</span>
                                            <input type="Text" id="temp-email" name="email"/>
                                        </label>
                                    </div>
                                    <div class="large-6 medium-12 column">
                                        <label>Phone <span style="color: red">*</span>
                                            <input type="Text" id="temp-phone" name="phone"/>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-10 medium-11 small-11 columns">

                                    </div>
                                    <div class="large-2 medium-1 small-1 columns text-right">
                                        <a id="temp-site-btn" class="button">+</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="large-4 medium-4 small-12 columns">
                        <div class="card-info primary">
                            <div class="card-info-content">
                                <div class="row">
                                    <h5>Sites</h5>
                                </div>
                                <div class="row expanded">
                                    <div class="large-12 medium-12 small-12 columns" id="sites-container"
                                         style="margin-bottom: 5px">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-10 medium-10 small-10 columns">

                            </div>
                            <div class="large-2 medium-2 small-2 columns text-right">
                                <a class="button" id="step3complete" style="float: right">Next</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-panel" id="panel4">
                <div class="row expanded">
                    <div class="large-8 medium-8 small-12 columns">
                        <div class="card-info primary">
                            <div class="card-info-content">
                                <div class="row">
                                    <h3 class="lead">Assets</h3>
                                </div>
                                <div class="row">
                                    <?php
                                    get_asset_types($USER->emma_plan_id)
                                    ;?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="large-4 medium-4 small-12 columns">
                        <div class="card-info primary">
                            <div class="card-info-content">
                                <div class="row">
                                    <h3 class="lead">Emergencies</h3>
                                </div>
                                <div class="row">
                                    <?php
                                    get_emergency_types()
                                    ;?>
                                </div>
                            </div>
                        </div>
                        <div class="row expanded">
                            <div class="large-10 medium-10 small-10 columns">

                            </div>
                            <div class="large-2 medium-2 small-2 columns">
                                <input class="button" type="submit" form="new-plan-form" style="float: right" value="Complete">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


<div id="success_modal" class="reveal success callout text-center tiny" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <a href="./dashboard.php?content=plans" data-close class="button success">Ok</a>
    <!--  <button class="close-button" data-close aria-label="Close reveal" type="button">-->
    <!--    <span aria-hidden="true">&times;</span>-->
    <!--  </button>-->
</div>


