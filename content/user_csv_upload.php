<?php

if (!$USER->privilege->admin) {
    redirect();
}
$redirect = false;

$plan_ids = array();
$plan_cvs = array();
$multiplanQuery = select_multiPlanCVS_with_userID($USER->id);
if($multiplanQuery->num_rows > 0){
    for($i=0; $i < $multiplanQuery->num_rows; $i++){
        $multiplanResult = $multiplanQuery->fetch_assoc();
        $plan_ids[] = $multiplanResult['plan_id'];
        if(array_search($multiplanResult['user_upload_folder'], $plan_cvs) === false){
            $plan_cvs[] = $multiplanResult['user_upload_folder'];
        }
    }
    if(empty($plan_cvs)){
        $redirect = true;
    }
}
else{
    $redirect = true;
}

if($redirect){
    redirect();
}

if((!empty($plan_cvs)) && count($plan_cvs) == 1) {


    ?>

    <div class="title row expanded align-middle">
        <div class="columns medium-12">
            <h2 class="text-left"><a
                        href="./dashboard.php?content=user_csv_upload"><i class="page-icon fa fa-cloud-upload"></i>
                    Upload User CSV File</a>
            </h2>
        </div>
    </div>

    <div class="row expanded">
        <div class="large-4 column">
            <?php
        //display download option of csv template file --> plan based
            ?>
            <div class="large-12">
                <div class="card-info primary">
                    <div class="card-info-content">
                        <h3 class="lead">Download template file</h3>
                        <form id="download_template_form" method="post" action="download_csv_template.php">
                            <input type="hidden" name="account_name" value="<?php echo $plan_cvs[0]; ?>">
                            <input type="submit" id="download_template_button" class="button" value="Download"/>
                        </form>
                    </div>
                </div>
            </div>


            <div class="large-12">
                <div class="card-info primary">
                    <div class="card-info-content">
                        <h3 class="lead">Export user file</h3>
                        <form id="download_export" method="post" action="download_export.php">
                            <input type="hidden" name="account_name" value="<?php echo $plan_cvs[0]; ?>">
                            <input type="submit" id="download_export_button" class="button" value="Download"/>
                        </form>
                    </div>
                </div>
            </div>

            <?php
        //display file upload
            ?>

            <div class="large-12">
                <div class="card-info primary">
                    <div class="card-info-content">
                        <h3 class="lead">Upload CSV File</h3>
                        <form id="upload_usercsv_form" method="post" enctype="multipart/form-data"
                              action="user_csv_upload_handler.php" target="_blank">

                            <input type="file" name="file" class="form-control" id="InputFile">

                            <div class="row">
                                <div class="small-3 medium-3 large-2 column">
                                    <div class="switch">
                                        <input class="switch-input" id="enable-email" name="enable-email" type="checkbox" >
                                        <label class="switch-paddle" for="enable-email">
                                            <span class="show-for-sr">Send Emails</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="small-9 medium-9 large-10 column">
                                    <label class="text-left">Send Emails</label>
                                </div>
                            </div>

                            <input type="submit" id="upload_usercsv_button" class="button" value="Upload"/>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="large-8 column">
            <div class="large-12">
                <div class="card-info primary">
                    <div class="card-info-content">
                        <h3 class="lead">Instructions</h3>
                        <ul>
                            <li>Download csv template</li>
                            <li>Fill in user data for all users in all your plans</li>
                            <li>User Activation Emails grants the ability to resend activation emails for new users</li>
                            <ul>
                                <li>1 to enable</li>
                                <li>0 to disable</li>
                            </ul>
                            <li>No Auto Logout</li>
                            <ul>
                                <li>1 to enable</li>
                                <li>0 to disable</li>
                            </ul>
                            <li>Admin User grants the ability to manage all users in their plans and also upload user csv files</li>
                            <ul>
                                <li>1 to enable</li>
                                <li>0 to disable</li>
                            </ul>
                            <li>For users to be in multiple plans or groups use ; to seperate the names, do not include extra spaces before or after<br/>example Admin;Teacher;Parent</li>
                            <li>Empty plan or group columns can prevent that user from using emma</li>
                            <li>Edit is for editing user data like last name or phone number</li>
                            <ul>
                                <li>To edit user info put a 1 in the edit column</li>
                                <li>To not edit user info put a 0 in the edit column</li>
                            </ul>
                            <li>Users will not be removed from the system unless the delete column is 1</li>
                            <ul>
                                <li>After upload has ran with a user having a delete of 1 that user can then be safely removed from the file</li>
                            </ul>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
}
else {
//to many csv plan folders
    ?>

    <div class="large-4 medium-6 small-12 columns large-offset-4 medium-offset-3">
        <div class="card-info primary">
            <div class="card-info-content">
                <h3 class="lead">Error</h3>
                <p>Your user is associated with to many unrelated accounts</p>
            </div>
        </div>
    </div>

<?php
}
//display file upload
?>


<div id="success_modal" class="reveal success callout text-center tiny" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <a href="./dashboard.php?content=admin_users" data-close class="button success">Ok</a>
    <!--  <button class="close-button" data-close aria-label="Close reveal" type="button">-->
    <!--    <span aria-hidden="true">&times;</span>-->
    <!--  </button>-->
</div>

