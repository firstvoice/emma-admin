<?php

if (!($USER->privilege->admin)) {
    redirect();
}

?>


<div class="title">
  <h2 class="text-left"><a
      href="./dashboard.php?content=groups"><i class="page-icon fa fa-group"></i> <?php echo ucwords($content); ?></a>
  </h2>
</div>
<div>
  <table id="groups-table" class="data-table">
    <colgroup>

    </colgroup>
    <thead>
    <tr>
      <th>Group</th>
      <th>Inactive Users</th>
      <th>Active Users</th>
      <th>Information Only</th>
      <th>Admin</th>
      <th>Security</th>
      <th class="text-search">EMMA SOS</th>
      <th class="text-search">911Admin</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
    <tfoot>
    <tr>
      <th class="text-search">Group</th>
      <th class="text-search">Inactive Users</th>
      <th class="text-search">Active Users</th>
      <th class="text-search">Information Only</th>
      <th class="text-search">Admin</th>
      <th class="text-search">Security</th>
      <th class="text-search">EMMA SOS</th>
      <th class="text-search">911Admin</th>
    </tr>
    </tfoot>
  </table>
</div>
