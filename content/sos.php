
<?php

if (!($USER->privilege->security || $USER->privilege->admin)) {
    redirect();
}

?>
<div class="title row expanded align-middle">
  <div class="columns medium-4">
    <h2 class="text-left"><a href="./dashboard.php?content=sos"><i class="page-icon fa fa-life-ring"></i> EMMA SOS</a></h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
        <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
          data-options="disableHover:true;clickOpen:true;">
          <li>
            <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
            <ul class="menu">
              <li><a href="./dashboard.php?content=sos_map" >EMMA SOS Map</a></li>
            </ul>
          </li>
        </ul>
  </div>
</div>

<div>
  <ul class="tabs" data-tabs id="script-type-tabs">
    <li class="tabs-title is-active">
      <a href="#panel-all" aria-selected="true">All</a>
    </li>
      <li class="tabs-title">
          <a href="#panel-pending" aria-selected="true">Pending</a>
      </li>
      <li class="tabs-title">
      <a href="#panel-help" aria-selected="true">Help</a>
    </li>
    <li class="tabs-title">
      <a href="#panel-cancelled" aria-selected="true">Cancelled</a>
    </li>
    <li class="tabs-title">
      <a href="#panel-closed" aria-selected="true">Closed</a>
    </li>
  </ul>
  <div class="tabs-content" data-tabs-content="script-type-tabs">
    <div id="panel-all" class="tabs-panel is-active">
        <a class="button" target="_blank" href="process/print_table.php?t=sosa">Print Report</a>
      <table id="sos-all-table" class="data-table" style="width:100%">
        <thead>
        <tr>
            <th class="text-search">Original Alert Reported</th>
            <th class="text-search">User</th>
            <th class="text-search">Mobile</th>
            <th class="text-search">Landline</th>
            <th></th>
        </tr>
        <tr>
          <th>Original Alert Reported</th>
          <th>User</th>
          <th>Mobile</th>
          <th>Landline</th>
          <th class="no-sort">Status</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>

        </tfoot>
      </table>
    </div>
      <div id="panel-help" class="tabs-panel">
        <a class="button" target="_blank" href="process/print_table.php?t=sosh">Print Report</a>
      <table id="sos-help-table" class="data-table" style="width:100%">
        <thead>
        <tr>
            <th class="text-search">Original Alert Reported</th>
            <th class="text-search">User</th>
            <th class="text-search">Mobile</th>
            <th class="text-search">Landline</th>
            <th class="text-search">Latitude</th>
            <th class="text-search">Longitude</th>
        </tr>
        <tr>
          <th>Original Alert Reported</th>
          <th>User</th>
          <th>Mobile</th>
          <th>Landline</th>
          <th>Latitude</th>
          <th>Longitude</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>

        </tfoot>
      </table>
    </div>
    <div id="panel-pending" class="tabs-panel">
        <a class="button" target="_blank" href="process/print_table.php?t=sosp">Print Report</a>
      <table id="sos-pending-table" class="data-table" style="width:100%">
        <thead>
        <tr>
            <th class="text-search">Original Alert Reported</th>
            <th class="text-search">User</th>
            <th class="text-search">Mobile</th>
            <th class="text-search">Landline</th>
            <th class="text-search">Latitude</th>
            <th class="text-search">Longitude</th>
        </tr>
        <tr>
          <th>Original Alert Reported</th>
          <th>User</th>
          <th>Mobile</th>
          <th>Landline</th>
          <th>Latitude</th>
          <th>Longitude</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>

        </tfoot>
      </table>
    </div>
    <div id="panel-cancelled" class="tabs-panel">
        <a class="button" target="_blank" href="process/print_table.php?t=sosc">Print Report</a>
      <table id="sos-cancelled-table" class="data-table" style="width:100%">
        <thead>
        <tr>
            <th class="text-search">Original Alert Reported</th>
            <th class="text-search">User</th>
            <th class="text-search">Mobile</th>
            <th class="text-search">Latitude</th>
            <th class="text-search">Longitude</th>
        </tr>
        <tr>
          <th>Original Alert Reported</th>
          <th>User</th>
          <th>Mobile</th>
          <th>Latitude</th>
          <th>Longitude</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>

        </tfoot>
      </table>
    </div>
    <div id="panel-closed" class="tabs-panel">
        <a class="button" target="_blank" href="process/print_table.php?t=sosl">Print Report</a>
      <table id="sos-closed-table" class="data-table" style="width:100%">
        <thead>
        <tr>
            <th class="text-search">Original Alert Reported</th>
            <th class="text-search">User</th>
            <th class="text-search">Mobile</th>
            <th class="text-search">Latitude</th>
            <th class="text-search">Longitude</th>
            <th class="text-search">Comments</th>
            <th></th>
        </tr>
        <tr>
          <th>Original Alert Reported</th>
          <th>User</th>
          <th>Mobile</th>
          <th>Latitude</th>
          <th>Longitude</th>
          <th>Comments</th>
          <th>Call Log</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>

        </tfoot>
      </table>
    </div>
  </div>
</div>