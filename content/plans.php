<?php

if (!($USER->privilege->admin && $USER->privilege->plans)) {
    redirect();
}

?>
<div class="title row expanded align-middle">
    <div class="columns medium-4">
        <h2 class="text-left"><a href="./dashboard.php?content=plans"><i class="page-icon fa fa-book"></i> <?php echo ucwords($content); ?></a></h2>
    </div>
    <div class="columns show-for-medium"></div>
    <div class="columns shrink">
        <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
            data-options="disableHover:true;clickOpen:true;">
            <li>
                <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
                <ul class="menu">
                    <?php
                    echo '<li><a href="./dashboard.php?content=create_plan">Create Plan</a></li>';
                    ?>
                </ul>
            </li>
        </ul>
    </div>
</div>

<table id="plans-table" class="data-table" style="width:100%">
    <colgroup>

    </colgroup>
    <thead>
    <tr>
        <th class="text-search">Plan</th>
        <th class="text-search">Date Active</th>
        <th class="text-search">Date Expires</th>
        <th class="text-search">Max Sites</th>
    </tr>
    <tr>
        <th>Plan</th>
        <th>Date Active</th>
        <th>Date Expires</th>
        <th>Max Sites</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>

    </tfoot>
</table>