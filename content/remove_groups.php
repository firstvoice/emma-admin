<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 12/20/2017
 * Time: 8:11 AM
 */

?>

<div class="title row expanded align-middle">
    <div class="columns medium-8">
      <h2 class="text-left"><a href="./dashboard.php?content=users"><i class="page-icon fa fa-group"></i> <?php echo 'Remove Users From Groups'; ?></a></h2>
</div>
<div class="columns show-for-medium"></div>
<div class="columns shrink">
    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
        data-options="disableHover:true;clickOpen:true;">
        <li>
            <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
            <ul class="menu">
                <li><a href="./dashboard.php?content=create_user" id="user_create"  >Create User</a></li>
                <li><a href="./dashboard.php?content=assign_groups" id="user_create"  >Assign Users</a></li>
                <li><a href="./dashboard.php?content=remove_groups" id="user_create"  >Remove Users</a></li>
                <li><a href="./dashboard.php?content=deactivate_user" id="user_create"  >Deactivate User</a></li>
              <li><a href="./dashboard.php?content=reactivate_user" id="user_create"  >Reactivate User</a></li>
            </ul>
        </li>
    </ul>
</div>
</div>
<!--    <div class="large-4 medium-12 column text-right">-->
<!--        <a href="./dashboard.php?content=create_user" id="user_create" class="button" >Create User</a>-->
<!--        <input id="user_assign_group" class="button " type="button" value="Assign Users" />-->
<!--        <input id="user_remove_group" class="button" type="button" value="Remove Users" />-->
<!--        <input id="user_deactivate" class="button " type="button" value="Deactivate User" />-->
<!--    </div>-->


<div class="row expanded">
    <div class="large-12">
        <form id="users_form">
            <table id="users-table" class="data-table">
                <thead>
                <tr>
                    <th> </th>
                    <th>Username</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Group</th>

                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="text-center">
                <input id="user_remove_group" class="button" type="button" value="Remove Groups" />
            </div>
        </form>
    </div>
</div>

<div id="remove_group_modal" class="reveal callout small" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Remove from groups</h4>
    <label> Groups
        <form id="remove_group_form">
            <div class="row large-12 columns">

                <?php
                $allgroups = select_groups_with_planID($USER->emma_plan_id);
//                $fvmdb->query("
//                                SELECT g.*
//                                FROM emma_groups AS g
//                                WHERE g.emma_plan_id = ". $USER->emma_plan_id ."
//                              ");
                while($allgroup = $allgroups->fetch_assoc()){

                    echo '
                    <div class="large-4 medium-6 small-12 columns">
                        <div class="row"> 
                            <div class="small-4 column">
                                <div class="switch tiny">
                                    <input class="switch-input" name="group-ids[]" value="'. $allgroup['emma_group_id'] .'" id="remove-group-id-' . $allgroup['emma_group_id'] . '" type="checkbox" >
                                    <label class="switch-paddle" for="remove-group-id-' . $allgroup['emma_group_id'] . '">
                                        <span class="show-for-sr">' . $allgroup['name'] . '</span>
                                    </label>
                                </div>
                            </div>
                            <div class="small-8">
                              <label class="text-left">' . $allgroup['name'] . '</label>
                            </div>
                        </div>
                    </div>';
                }
                ?>

            </div>
        </form>
    </label>
    <div class="text-center small-12">
        <input type="button" class="button" id="remove_group_submit" value="Submit" />
    </div>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>


<!--<div id="update_site_modal" class="reveal callout small" data-reveal data-animation-in="fade-in"-->
<!--     data-animation-out="fade-out">-->
<!--    <h4>Assign Site</h4>-->
<!--    <label> Site-->
<!--        <select id="site_id">-->
<!--            --><?php
//            $sites = $fvmdb->query("
//                    SElECT s.*
//                    FROM emma_sites AS s
//                    WHERE s.emma_plan_id = ". $USER->emma_plan_id ."
//                ");
//            while($site = $sites->fetch_assoc()){
//                echo '<option value="'. $site['emma_site_id'] .'">'. $site['emma_site_name'] .'</option>';
//            }
//
//            ?>
<!--        </select>-->
<!--    </label>-->
<!--    <div class="text-center small-12">-->
<!--        <input type="button" class="button" id="assign_site_submit" value="Submit" />-->
<!--    </div>-->
<!--    <button class="close-button" data-close aria-label="Close reveal" type="button">-->
<!--        <span aria-hidden="true">&times;</span>-->
<!--    </button>-->
<!--</div>-->



<div id="no-selected-users" class="reveal callout small" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>No users selected.</h4>
    <div class="text-center small-12">
        <a href="./dashboard.php?content=users" data-close class="button" >Ok</a>
    </div>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div id="success_modal" class="reveal success callout text-center tiny" data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <a href="./dashboard.php?content=users" data-close class="button success" >Ok</a>
<!--    <button class="close-button" data-close aria-label="Close reveal" type="button">-->
<!--        <span aria-hidden="true">&times;</span>-->
<!--    </button>-->
</div>
