<?php
/**
 * Created by PhpStorm.
 * User: Pug
 * Date: 9/23/2019
 * Time: 8:43 AM
 */

$plans = select_plan_from_planID($USER->emma_plan_id);
//$fvmdb->query("
//  select *
//  from emma_plans
//  where emma_plan_id = '" . $USER->emma_plan_id . "'
//");
$plan = $plans->fetch_assoc();


if (!($USER->privilege->admin)) {
    redirect();
}

?>


<div class="title row expanded align-middle">
  <div class="columns medium-4">
    <h2 class="text-left"><a href="./dashboard.php?content=raw_users"><i class="page-icon fa fa-user"></i> <?php echo ucwords(str_replace('_',' ',$content)); ?></a></h2>
  </div>
  <div class="columns show-for-medium"></div>
  <div class="columns shrink">
        <input type="submit" form="approve-user-form" class="button" value="Approve">
  </div>
</div>
<form id="approve-user-form" method="post">
    <table class="data-table" style="width:100%">
      <colgroup>

      </colgroup>
      <thead>
      <tr>
        <th></th>
        <th>Name</th>
        <th>Email</th>
        <th>Students</th>
        <th>Group</th>
      </tr>
      </thead>
      <tbody>
      <?php
        $rawusers = select_rawUsers_rawUsers($USER->emma_plan_id);
//            $fvmdb->query("
//        SELECT r.*, g.name
//        FROM raw_users r
//        JOIN emma_groups g ON r.`group` = g.emma_group_id
//        WHERE r.plan = '". $USER->emma_plan_id ."'
//        AND r.active = 1
//        ");
        while($rawu = $rawusers->fetch_assoc()){
            $studentsarr = '';
            $students = select_rawUsers_students($rawu['id']);
//                $fvmdb->query("
//                SELECT e.student_name AS name, e.student_id
//                FROM emma_raw_students_to_guardians e
//                WHERE e.guardian_id = '". $rawu['id'] ."'
//            ");
            while($student = $students->fetch_assoc()){
                if($student['student_id'] == '0') {
                    $studentsarr .= ' ' . $student['name'] . ' ';
                }else{
                    $studentsarr .= ' <span style="color: green">' . $student['name'] . '</span> ';
                }
            }


            echo'
            <tr>
                <td><input type="checkbox" value="'. $rawu['id'] .'"></td>
                <td>'. $rawu['firstname'] .' '. $rawu['lastname'] .'</td>
                <td>'. $rawu['email'] .'</td>
                <td>'. $studentsarr .'</td>
                <td>'. $rawu['name'] .'</td>
            </tr>
            ';
        }
      ?>
      </tbody>
      <tfoot>

      </tfoot>
    </table>
</form>