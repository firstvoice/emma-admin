<?php
/**
 * Created by PhpStorm.
 * User: PFuhrmeister
 * Date: 7/24/2019
 * Time: 8:37 AM
 */


$resources = select_help_resources();
//    $fvmdb->query("
//  SELECT *
//  FROM emma_help_resource_types
//  WHERE active = 1
//");
$count = 0;

//if (!($USER->privilege->security || $USER->privilege->admin || $USER->privil)) {
 //   redirect();
//}

?>

<div class="title row expanded align-middle">
  <div class="columns medium-3">
    <h2 class="text-left"><a
        href="./dashboard.php?content=resources"><i class="page-icon fa fa-question"></i> <?php echo ucwords($content); ?></a>
    </h2>
  </div>
  <div class="columns show-for-medium"></div>
    <!----
  <div class="columns shrink">
    <ul id="action-menu" class="dropdown menu align-right" data-dropdown-menu
      data-options="disableHover:true;clickOpen:true;">
      <li>
        <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <ul class="menu">
          <li><a href="#" data-open="add-resource-modal">New Resource</a></li>
        </ul>
      </li>
    </ul>
  </div>
  ---->
</div>

<div class="content-body margin-top-1em">
  <div class="row expanded">
    <?php
    while ($resource = $resources->fetch_assoc()) {
      $name = $resource['resource_name'];
      $altered_name = str_replace(' ', '_', $name);
      $altered_name = strtolower($altered_name);
      ?>
      <div class="large-2 small-12 medium-6 column margin-bottom-1EM">
        <a class="dashboard-nav-card"
          href="./dashboard.php?content=help_type&resource_type=<?php echo $resource['emma_resource_type_id']; ?>">
          <i class="dashboard-nav-card-icon fa fa-folder"
            aria-hidden="true"></i>
          <h5 class="dashboard-nav-card-title"><?php echo $name; ?></h5>
        </a>
      </div>
      <?php
    }
   // if($USER->privilege->security || $USER->privilege->admin) {
 //   ?>
   <!----     <div class="large-2 small-12 medium-6 column margin-bottom-1EM">
            <button class="dashboard-nav-card" data-open="choose_scripts_modal">
                <i class="dashboard-nav-card-icon fa fa-folder" aria-hidden="true"></i>
                <h5 class="dashboard-nav-card-title">Scripts</h5>
            </button>
        </div>
    <?php
    //}
    ?>
    --->


</div>

<div id="add-resource-modal" class="reveal callout large" data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <form id="new-resource-form" enctype="multipart/form-data" action=""
    method="post">
    <input type="hidden" name="plan_id"
      value="<?php echo $USER->emma_plan_id; ?>"/>
    <input type="hidden" name="user_id" value="<?php echo $USER->id; ?>"/>
    <div class="text-center">
      <h4 class="lead">New Resource</h4>
    </div>
    <label>File:
      <input type="file" name="file"
        accept=".jpeg,.png,.gif,.doc,.docx,.xls,.xlsx,.csv,image/gif,image/png,image/jpeg,application/pdf"
        required/>
    </label>
    <label>File Name:
      <input type="Text" name="file_name" required/>
    </label>
    <label>Resource Category:
      <select name="file_type">
          <option value="">-Select-</option>
        <?php
        $resource_types = select_help_resourceTypes();
//            $fvmdb->query("
//          SELECT *
//          FROM emma_resource_types
//          WHERE active = 1
//          ORDER BY resource_name
//        ");
        while ($resource_type = $resource_types->fetch_assoc()) {
          echo '<option value="' . $resource_type['emma_resource_type_id'] .
            '">' . $resource_type['resource_name'] . '</option>';
        }
        ?>
      </select>
    </label>

    <label>Site:
      <select name="site_id">
          <option value="">-Select-</option>
        <?php
        $sites = select_sites_with_planID($USER->emma_plan_id);
//            $fvmdb->query("
//          SELECT e.*
//          FROM emma_sites AS e
//          WHERE e.emma_plan_id = " . $USER->emma_plan_id . "
//        ");
        while ($site = $sites->fetch_assoc()) {
          echo '<option value="' . $site['emma_site_id'] . '">' .
            $site['emma_site_name'] . '</option>';
        }
        ?>
      </select>
    </label>


    <div class="text-center small-12">
      <input type="submit" value="submit" class="button"/>
    </div>
    <button class="close-button" data-close aria-label="Close reveal"
      type="button">
      <span aria-hidden="true">&times;</span>
    </button>
  </form>
</div>
<div id="success_modal" class="reveal callout success text-center tiny"
  data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>Success</h4>
  <a href="./dashboard.php?content=resources" data-close class="button success">Ok</a>
<!--  <button class="close-button" data-close aria-label="Close reveal"-->
<!--    type="button">-->
<!--    <span aria-hidden="true">&times;</span>-->
<!--  </button>-->
</div>
<div id="invalid_modal" class="reveal callout small" data-reveal
  data-animation-in="fade-in"
  data-animation-out="fade-out">
  <h4>File type is not allowed</h4>
  <div class="text-center small-12">
    <a data-close class="button">Ok</a>
  </div>
  <button class="close-button" data-close aria-label="Close reveal"
    type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<div id="choose_scripts_modal" class="reveal callout small" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4 style="text-align: center;">Choose Scripts To Visit</h4>
    <hr>
    <div class="button-group expanded">
        <div class="large-12 columns">
            <div class="row">
                <div class="large-3 columns large-offset-3">
                    <a href="./dashboard.php?content=scripts" class="button expanded">Scripts</a>
                </div>
                <div class="large-3 columns">
                    <a href="./dashboard.php?content=mass_notification_scripts" class="button expanded">Mass Notification</a>
                </div>
            </div>
        </div>
    </div>
    <button class="close-button" data-close aria-label="Close reveal"
            type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
