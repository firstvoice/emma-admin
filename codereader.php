<?php

?>
<table width="390" border="0">
    <table width="390" border="0"><tr><td><video width="390" height="292" controls><source src="mp4/BBPslide9.mp4" type="video/mp4">Your browser does not support the video tag.</video></tr><tr><td><span class="style21">&nbsp;</span></td></tr></table>

    <table width="390" height="292" controls><source src="/mp4/BBPslide9.mp4" type="video/mp4"> Your browser does not support the video tag.</table   </tr>   <tr>     <td bgcolor="#FF6666"><span class="style22"><u>SPECIAL NOTE:</u>  Warning labels must be affixed to containers of regulated waste. <br /><br />Double bag all blood and body fluid waste material as a standard "OSHA BEST PRACTICE".</span></td>   </tr>   <tr>     <td><span class="style21">&nbsp;</span></td>   </tr> </table>



<table width="390" border="0">   <tr>     <td><span class="style21">Typically, workplaces use red plastic biohazard waste bags that are leak proof to collect bloody waste which is liquid or semi-liquid. <br /><br />When used, these bags will be tied in a knot at the top then disposed of in a biomedical waste box or specially marked biohazard waste container. <br /><br />Red plastic bags containing waste should not go in the regular garbage.  </span></td>   </tr>   <tr>     <td><br /><span class="style21">Items such as tissues, or bloodied first aid gauze that contain dried blood can be placed in the regular garbage after they are double-bagged. <br /><br />Check your workplace reference materials for accessible BBP clean-up kits and supplies storage areas.</span><br /><Br /></td>   </tr>   <tr>     <td bgcolor="#FFFF00"><span class="style22"><b><u>An Important Tip:</u></b> Do not pick up broken glass directly with the hands or with gloved hands.</span></td>   </tr> </table>


<table width="390" border="0">
    <table width="390" border="0"><tr><td bgcolor="#FF6666"><span class="style22"><u>SPECIAL NOTE:</u>  Warning labels must be affixed to containers of regulated waste. <br/><br/>Double bag all blood and body fluid waste material as a standard "OSHA BEST PRACTICE".</span></td></tr><tr><td><span class="style21">&nbsp;</span></td></tr></table>