<?php include('include/pre_login_header.php'); ?>
<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Case Studies</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #FFFFFF;">


<?php include('include/index_top_bar.php');

?>

<div class="row expanded the-clock" style="height: 100%">
    <div class="large-12 medium-12 small-12 columns" style="margin: 0 auto;">
    </div>
    <div class="large-12 medium-12 small-12 columns">
        <div class="row expanded" style="padding-bottom: 5em;text-align: center;">
            <div class="large-3 medium-6 small-6 column">
            <div class="clear_card">
                <a class="tracked-link" data-name="parkland-case-study" data-user="<?php echo $_SESSION['user'];?>" data-link="documents/EMMA_Confidential_Case_Study.pdf"><img
                            src="img/parkland_case_study.jpg" width="50%" height="50%"/>
                    <div class="card_section">
                    <p>Parkland Case Study</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="large-3 medium-6 small-6 column">
            <div class="clear_card">
                <a class="tracked-link" data-name="role-of-technology" data-user="<?php echo $_SESSION['user'];?>" data-link="documents/RAND_The_Role_of_Technology_In_Improving_K-12_School_Safety.pdf"><img
                            src="img/RAND_The Role of Technology In Improving K-12 School Safety_Page_1.jpg" width="50%"
                            height="50%"/>
                    <div class="card_section">
                    <p>Role of Technology In Improving K-12 School Safety</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="large-3 medium-6 small-6 column">
            <div class="clear_card">
                <a data-name="role-of-technology" data-user="<?php echo $_SESSION['user'];?>" href="documents/usss-analysis-of-targeted-school-violence.pdf"><img
                            src="documents/usss-analysis-of-targeted-school-violence.pdf" width="50%"
                            height="50%"/>
                    <div class="card_section">
                    <p>Analysis of Targeted School Violence</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="large-3 medium-6 small-6 column">
            <div class="clear_card">
                <a data-name="role-of-technology" data-user="<?php echo $_SESSION['user'];?>" href="documents/active_shooter_booklet.pdf"><img
                            src="documents/active_shooter_booklet.pdf" width="50%"
                            height="50%"/>
                    <div class="card_section">
                    <p>Active Shooter Booklet</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<div><p>&nbsp;</p><br/><br/><br/><br/><br/><br/><br/></div>
</div>
<?php include('include/footer.php'); ?>
<?php include('include/pre_login_modals.php'); ?>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>


