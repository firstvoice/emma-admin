<?php
require('include/db.php');
require('vendor/php-jwt-master/src/JWT.php');
require('vendor/php-jwt-master/src/BeforeValidException.php');
require('vendor/php-jwt-master/src/ExpiredException.php');
require('vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('config/config.json'));

$USER = null;
try {
  $token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key, array('HS512'));
  $USER = $token->data;
} catch (Exception $e) {
  header('Location: index.php');
}