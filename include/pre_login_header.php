<?php
/**
 * Created by PhpStorm.
 * User: COnrad Ullrich
 * Date: 08/15/2019
 * Time: 11:31 AM
 */

use Firebase\JWT\JWT;

session_start();
include('include/db.php');
require('vendor/php-jwt-master/src/JWT.php');
require('vendor/php-jwt-master/src/BeforeValidException.php');
require('vendor/php-jwt-master/src/ExpiredException.php');
require('vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('config/config.json'));

//if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != "on"){
//    $redirect = str_replace('www.', '', "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
//    header('HTTP/1.1 301 Moved Permanently');
//    header('Location: ' . $redirect);
//    exit();
//}
//
//if(strpos($_SERVER['HTTP_REFERER'],'www.') !== false || strpos($_SERVER['HTTP_REFERER'],'www.') > 0){
//    $redirect = str_replace('www.', '', "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
//    header('HTTP/1.1 301 Moved Permanently');
//    header('Location: ' . $redirect);
//    exit();
//}


$errorMessage = '';
if (isset($_COOKIE['jwt'])) {
    try {
        $token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key,
            array('HS512'));
        header('Location: dashboard.php');
    } catch (\Firebase\JWT\BeforeValidException $bve) {
        $errorMessage = $bve->getMessage();
    } catch (\Firebase\JWT\ExpiredException $ee) {
        $errorMessage = $ee->getMessage();
    } catch (\Firebase\JWT\SignatureInvalidException $sie) {
        $errorMessage = $sie->getMessage();
    } catch (Exception $e) {
        $errorMessage = $e->getMessage();
    }
}
//login with token from emma admin
if (isset($_GET['token'])) {

    $loginToken = $fvmdb->real_escape_string($_GET['token']);

    $users = login_with_token($loginToken);
    if ($users->num_rows > 0) {
        $user = $users->fetch_assoc();

        $admin = false;
        $security = false;
        $plansecurity = false;
        $groups = select_groups_with_userID_and_planID($user['id'], $user['token_plan']);
        while ($group = $groups->fetch_assoc()) {
            if ($group['admin']) $admin = true;
            if ($group['security']) $security = true;
            if ($group['geofences']) $geofences = true;
            if ($group['911admin']) $admin911 = true;
            if ($group['securities']) $plansecurity = true;
            if ($group['panic']) $plansos = true;
            if ($group['lockdown']) $planlockdown = true;
            if ($group['ar']) $ar = true;
            if ($group['mar']) $mar = true;
        }

        //      if ($admin || $security) {
        $tokenId = base64_encode(randomToken(32));
        $issuedAt = time();
        $notBefore = $issuedAt;
        $expire = $notBefore + (10 * 365 * 24 * 60 * 60);
        $serverName = 'tsdemos.com';

        if($user['default_plans'] != ''){
            $home = explode(',',$user['default_plans']);
        }else{
            $home = explode(',',$user['emma_plan_id']);
        }

        $data = [
            'iat' => $issuedAt,
            'jti' => $tokenId,
            'iss' => $serverName,
            'nbf' => $notBefore,
            'exp' => $expire,
            'data' => [
                'id' => $user['id'],
                'full_name' => $user['full_name'],
                'first_name' => $user['firstname'],
                'username' => $user['username'],
                'emma_plan_id' => $user['emma_plan_id'],
                'emma_plan_name' => $user['emma_plan_name'],
                'emma_home_plans' => $home,
                'emma_pin' => $user['emma_pin'],
                'auth' => $user['auth'],
                'privilege' => [
                    'admin' => $admin,
                    'admin_user' => $user['admin_user'],
                    'security' => $security,
                    'plansecurity' => $plansecurity,
                    'plansos' => $plansos,
                    'planlockdown' => $planlockdown,
                    'geofences' => $geofences,
                    'admin911' => $admin911,
                    'code_generator' => $user['emma_code_generator'] == 1,
                    'plans' => $user['emma_plans'] == 1,
                    'plan_god' => $user['plan_god'] == 1,
                    'plan_emails' => $user['plan_emails'] == 1,
                    'plan_texts' => $user['plan_texts'] == 1,
                    'activate_emails' => $user['activate_email'] == 1,
                    'nologout' => $user['no_logout'] == 1,
                    'ar' => $ar,
                    'mar' => $mar
                ],
                'feed_length' => 24
            ]
        ];



        $jwt = Firebase\JWT\JWT::encode($data, $CONFIG->key, 'HS512');
        setcookie('jwt', $jwt, time() + (9 * 60 * 60), '/');

        if (isset($_GET['content'])) {
            header('Location: dashboard.php?content=' . $_GET['content']);
        } else {
            header('Location: dashboard.php');
        }


    }
}
if (isset($_POST['login']) && !empty($_POST['username'])) {

    $attempts = select_loginAttempts($fvmdb->real_escape_string($_POST['username']));
    $attempt = $attempts->fetch_assoc();
    if($attempt['attempt_since_success'] >= 5){
        $errorMessage = 'Account is locked. Please click \'Forgot Password\' to reset your account.';
        insert_emma_login_log($fvmdb->real_escape_string($_POST['username']), 0, $_SERVER['REMOTE_ADDR'], 'EmmaAdmin', $attempt['attempt_since_success'] + 1);
    }
    else {

        try {
            $username = $fvmdb->real_escape_string($_POST['username']);
            $password = $fvmdb->real_escape_string($_POST['password']);
            $users = login_with_username_and_password($username, $password);
            if ($users->num_rows > 0) {
                $user = $users->fetch_assoc();

                $admin = false;
                $security = false;
                $plansecurity = false;
                $groups = select_groups_with_userID($user['id']);
                while ($group = $groups->fetch_assoc()) {
                    if ($group['admin']) $admin = true;
                    if ($group['security']) $security = true;
                    if ($group['geofences']) $geofences = true;
                    if ($group['911admin']) $admin911 = true;
                    if ($group['securities']) $plansecurity = true;
                    if ($group['panic']) $plansos = true;
                    if ($group['lockdown']) $planlockdown = true;
                    if ($group['ar']) $ar = true;
                    if ($group['mar']) $mar = true;
                }


//      if ($admin || $security) {
                $tokenId = base64_encode(randomToken(32));
                $issuedAt = time();
                $notBefore = $issuedAt;
                $expire = $notBefore + (10 * 365 * 24 * 60 * 60);
                $serverName = 'tsdemos.com';

                if($user['default_plans'] != ''){
                    $home = explode(',',$user['default_plans']);
                }else{
                    $home = explode(',',$user['emma_plan_id']);
                }

                $data = [
                    'iat' => $issuedAt,
                    'jti' => $tokenId,
                    'iss' => $serverName,
                    'nbf' => $notBefore,
                    'exp' => $expire,
                    'data' => [
                        'id' => $user['id'],
                        'fvm_id' => $user['fvm_id'],
                        'full_name' => $user['full_name'],
                        'first_name' => $user['firstname'],
                        'username' => $user['username'],
                        'emma_plan_id' => $user['emma_plan_id'],
                        'emma_plan_name' => $user['emma_plan_name'],
                        'emma_home_plans' => $home,
                        'emma_pin' => $user['emma_pin'],
                        'auth' => $user['auth'],
                        'privilege' => [
                            'admin' => $admin,
                            'admin_user' => $user['admin_user'],
                            'security' => $security,
                            'plansecurity' => $plansecurity,
                            'plansos' => $plansos,
                            'planlockdown' => $planlockdown,
                            'geofences' => $geofences,
                            'admin911' => $admin911,
                            'code_generator' => $user['emma_code_generator'] == 1,
                            'plans' => $user['emma_plans'] == 1,
                            'plan_god' => $user['plan_god'] == 1,
                            'plan_emails' => $user['plan_emails'] == 1,
                            'plan_texts' => $user['plan_texts'] == 1,
                            'activate_emails' => $user['activate_email'] == 1,
                            'nologout' => $user['no_logout'] == 1,
                            'ar' => $ar,
                            'mar' => $mar
                        ],
                        'feed_length' => 24
                    ]
                ];

                $jwt = Firebase\JWT\JWT::encode($data, $CONFIG->key, 'HS512');
                setcookie('jwt', $jwt, time() + (9 * 60 * 60), '/');
//      } else {
//        $errorMessage = 'User is not in an Admin or Security group';
//      }
                $events = select_emergencies_with_planID($user['emma_plan_id']);
                if ($event = $events->fetch_assoc()) {
                    header('Location: dashboard.php?content=event&id=' . $event['emergency_id']);
                } else {
                    header('Location: dashboard.php');
                }


                insert_emma_login_log($fvmdb->real_escape_string($_POST['username']), 1, $_SERVER['REMOTE_ADDR'], 'EmmaAdmin', 0);


            } else {
                //find attempt number

                $attempts = select_loginAttempts($fvmdb->real_escape_string($_POST['username']));
                $attempt = $attempts->fetch_assoc();

                insert_emma_login_log($fvmdb->real_escape_string($_POST['username']), 0, $_SERVER['REMOTE_ADDR'], 'EmmaAdmin', $attempt['attempt_since_success'] + 1);
                $errorMessage = 'Invalid Username/Email or Password. If you do not remember your username please contact your EMMA administrator.';
            }
        } catch (Exception $e) {
            $e->getMessage();
        }
    }
} elseif (isset($_POST['login']) && empty($_POST['username'])) {
    $errorMessage = 'Invalid Username/Email or Password. If you do not remember your username please contact your EMMA administrator.';
}
?>
