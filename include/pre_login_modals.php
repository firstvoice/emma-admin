<div id="thinking-modal" class="reveal callout text-center tiny" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4 style="color:#e5e5e5;">Working...</h4>
    <img style="width:2rem;height:2rem;" src="img/ajax_loader_blue.gif"/>
</div>
<div id="password-success-modal" class="reveal callout success text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <br>
    <p>Please check your email for your temporary password. It may take up to 5 minutes for you
        to receive.
        If you do not receive it, please check your junk/spam folder before contacting us.</p>
    <a data-close class="button success">OK</a>
</div>
<div id="contact-us-success-modal" class="reveal callout success text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <br>
    <p>Thank you. An E.M.M.A. representative will reach out within 24-48 hours. Feel free to email us as well: <a href="mailto:emma@think-safe.com">emma@think-safe.com</a>. Thank you!</p>
    <a data-close class="button success">OK</a>
</div>
<div id="fail-modal" class="reveal callout alert text-center tiny" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4 style="color:darkred">Failed...</h4>
    <div id="error-list" class="text-left"></div>
    <a class="button" style="background-color: darkred" data-close>OK</a>
</div>
<div id="forgot-password-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out"
     style="background-color: #0078c1;border: 1px solid #0078C1;">
    <h4 style="color: white">Forgot Password</h4>
    <form id="forgot-password-form" class="text-left" method="post">
        <label style="color: white">Username/Email<span style="color: red;"> *</span>
            <input type="text" name="current-email" id="current-email"
                   placeholder="username@domain.com"/>
        </label>
        <div class="button-group expanded">
            <a data-close="" class="button alert">Cancel</a>
            <button type="submit" form="forgot-password-form" style="background-color: yellow; color: black; font-weight: bold" class="button">Submit</button>
        </div>
    </form>
</div>
<div id="login-modal" class="reveal callout text-center"
     style="background-color: #0078c1;border: 1px solid #0078C1;" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out"
     data-options="closeOnClick:false;closeOnEsc:false;">
    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" id="login-form"
          class="text-left"
          method="post">
        <label style="color: white">Username/Email
            <input type="text" name="username" id="username" required/>
        </label>
        <label style="color: white">Password
            <input type="password" name="password" id="password" required/>
        </label>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true"></span>
        </button>
        <div class="button-group expanded">
            <input class="button"
                   style="background-color: yellow;color: black;font-weight: bold" type="submit"
                   name="login" id="login" value="LOGIN"/>
        </div>
        <div class="large-12 columns text-center">
            <a data-open="forgot-password-modal" style="color: white">Forgot Password</a>
        </div>
        <button class="close-button" data-close aria-label="Close reveal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </form>
</div>
<div id="contact-us-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <form id="contact-us-form" class="text-left" action="#" method="post"><input type="hidden" id="contact-us-type"
                                                                                 name="type">
        <div class="row">
            <div class="large-12 medium-12 columns" style="padding-bottom: .5vh">
                <b><i>Information is confidential.</i></b>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>First Name<span style="color: red">*</span></label>
                <input type="text" name="first-name" id="firstname"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Last Name<span style="color: red">*</span></label>
                <input type="text" name="last-name" id="lastname"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Email<span style="color: red">*</span></label>
                <input type="text" name="email" id="email"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Organization</label>
                <input type="text" name="org" id="org"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Mobile</label>
                <input type="text" name="mobile" id="mobile"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Landline</label>
                <input type="text" name="landline" id="landline"/>
            </div>
            <hr>
        </div>
        <div class="row">
            <div class="large-12 medium-12 columns">
                <label id="comments">We are eager to educate you further on our E.M.M.A. technology. Please write what questions you have and an E.M.M.A. representative will reach out to you with further information. Thank You!<span style="color: red">*</span></label>
                <textarea name="comments" id="comments" class="text"
                          style="color: black;"></textarea>
            </div>
        </div>
        <div class="large-12 columns">
            <div class="button-group expanded">
                <a data-close="" class="button alert">Cancel</a>
                <input type="submit" class="button" value="Submit"/>
            </div>
        </div>

        <div class="row text-center">
            <!--            <div class="large-3 medium-3 columns text-center">-->
            <!--                <p>Think Safe, Inc.</p>-->
            <!--            </div>-->
            <!--            <div class="large-6 medium-6 columns text-center">-->
            <!--                <p>emma@think-safe.com</p>-->
            <!--            </div>-->
            <!--            <div class="large-3 medium-3 columns text-center">-->
            <!--                <p>(319)377-5125</p>-->
            <!--            </div>-->
            <p style="margin: 0 auto">Think Safe, Inc.<br>
                emma@think-safe.com<br>
                (319)377-5125</p>
        </div>
    </form>
    <script type="text/javascript">
        var vvfc_BaseURL = (("https:" == document.location.protocol) ? "https://forms.id-visitors.com/FrontEndWeb/" : "http://forms.id-visitors.com/FrontEndWeb/");
        var vvfc_ServiceURL = vvfc_BaseURL + 'ProcessFormCapture.aspx';
        var vvfc_ScriptURL = vvfc_BaseURL + 'Scripts/vvfcscript.js';
        var trackedForms = ['contact-us-form'];
        var options = { accountId: 'uOb7EgGMeK', serviceURL: vvfc_ServiceURL, pollInterval: 5000,
            forms: [{ id: 'contact-us-form', ignoreFields: ['__VIEWSTATE', '__VIEWSTATEGENERATOR', '__EVENTVALIDATION'], accountFormId: '5f9882bf68b5f70ba4a6f44b' } ]};
        (function (src) {
            var vvscript = document.createElement('script');
            vvscript.type = 'text/javascript';
            vvscript.async = true; vvscript.src = src;
            var scriptElements = document.getElementsByTagName('script');
            var lastScriptElement = scriptElements[scriptElements.length - 1];
            lastScriptElement.parentNode.insertBefore(vvscript, lastScriptElement.nextSibling);
        })(vvfc_ScriptURL);
    </script>
</div>
<div id="schedule-demo-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <form class="contact-us-form text-left" action="#" method="post"><input type="hidden" id="contact-us-type"
                                                                                 name="type">
        <div class="row">
            <div class="large-12 medium-12 columns" style="padding-bottom: .5vh">
                <b><i>Information is confidential.</i></b>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>First Name<span style="color: red">*</span></label>
                <input type="text" name="first-name" id="firstname"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Last Name<span style="color: red">*</span></label>
                <input type="text" name="last-name" id="lastname"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Email<span style="color: red">*</span></label>
                <input type="text" name="email" id="email"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Organization</label>
                <input type="text" name="org" id="org"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Mobile</label>
                <input type="text" name="mobile" id="mobile"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Landline</label>
                <input type="text" name="landline" id="landline"/>
            </div>
            <hr>
        </div>
        <div class="row">
            <div class="large-12 medium-12 columns">
                <label id="comments">Our free demo schedule is typically M, T, W, Th, and F at 10AM and 2PM Central Time. Please let us know your day and time preference and a team member will reach out to you with further details. We can arrange demos outside of those times as well. Thank you.<span style="color: red">*</span></label>
                <textarea name="comments" id="comments" class="text"
                          style="color: black;"></textarea>
            </div>
        </div>
        <div class="large-12 columns">
            <div class="button-group expanded">
                <a data-close="" class="button alert">Cancel</a>
                <input type="submit" class="button" value="Submit"/>
            </div>
        </div>

        <div class="row text-center">
            <!--            <div class="large-3 medium-3 columns text-center">-->
            <!--                <p>Think Safe, Inc.</p>-->
            <!--            </div>-->
            <!--            <div class="large-6 medium-6 columns text-center">-->
            <!--                <p>emma@think-safe.com</p>-->
            <!--            </div>-->
            <!--            <div class="large-3 medium-3 columns text-center">-->
            <!--                <p>(319)377-5125</p>-->
            <!--            </div>-->
            <p style="margin: 0 auto">Think Safe, Inc.<br>
                emma@think-safe.com<br>
                (319)377-5125</p>
        </div>
    </form>
</div>

<div id="find-partner-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <form class="contact-us-form text-left" action="#" method="post"><input type="hidden" id="contact-us-type"
                                                                                 name="type">
        <div class="row">
            <div class="large-12 medium-12 columns" style="padding-bottom: .5vh">
                <b>Our First Voice product line is sold through dealers. To sign up or find out more about becoming a
                    dealer for EMMA or First Voice apps or products, contact us todayOur First Voice product line is
                    sold through dealers. To sign up or find out more about becoming a dealer for EMMA or First Voice
                    apps or products, contact us today</b><br/><br/>
                <b><i>Your information is never shared.</i></b><br/>
                <b><i>Confidentiality guaranteed.</i></b>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>First Name<span style="color: red">*</span></label>
                <input type="text" name="first-name" id="firstname"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Last Name<span style="color: red">*</span></label>
                <input type="text" name="last-name" id="lastname"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Email<span style="color: red">*</span></label>
                <input type="text" name="email" id="email"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Organization</label>
                <input type="text" name="org" id="org"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Mobile</label>
                <input type="text" name="mobile" id="mobile"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Landline</label>
                <input type="text" name="landline" id="landline"/>
            </div>
            <hr>
        </div>
        <div class="row">
            <div class="large-12 medium-12 columns">
                <label id="comments">Comments<span style="color: red">*</span></label>
                <textarea name="comments" id="comments" class="text"
                          style="color: black;"></textarea>
            </div>
        </div>
        <div class="large-12 columns">
            <div class="button-group expanded">
                <a data-close="" class="button alert">Cancel</a>
                <input type="submit" class="button" value="Submit"/>
            </div>
        </div>

        <div class="row text-center">
            <!--            <div class="large-3 medium-3 columns text-center">-->
            <!--                <p>Think Safe, Inc.</p>-->
            <!--            </div>-->
            <!--            <div class="large-6 medium-6 columns text-center">-->
            <!--                <p>emma@think-safe.com</p>-->
            <!--            </div>-->
            <!--            <div class="large-3 medium-3 columns text-center">-->
            <!--                <p>(319)377-5125</p>-->
            <!--            </div>-->
            <p style="margin: 0 auto">Think Safe, Inc.<br>
                emma@think-safe.com<br>
                (319)377-5125</p>
        </div>
    </form>
</div>

<div id="pricing-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <form id="pricing-form" class="text-left" action="#" method="post"><input type="hidden" id="pricing-type"
                                                                              name="type">
        <div class="row">
            <div class="large-12 medium-12 columns" style="padding-bottom: .5vh">
                <b>Request a Quote</b><br/>
                <b>Request a quick follow up about how EMMA can support and revolutionize your organization’s or group’s
                    communication, notification, or emergency management needs.</b><br/><br/>
                <b><i>Your information is never shared.</i></b><br/>
                <b><i>Confidentiality guaranteed.</i></b>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>First Name<span style="color: red">*</span></label>
                <input type="text" name="first-name" id="firstname"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Last Name<span style="color: red">*</span></label>
                <input type="text" name="last-name" id="lastname"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Email<span style="color: red">*</span></label>
                <input type="text" name="email" id="email"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Organization</label>
                <input type="text" name="org" id="org"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Mobile</label>
                <input type="text" name="mobile" id="mobile"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Landline</label>
                <input type="text" name="landline" id="landline"/>
            </div>
            <hr>
        </div>
        <div class="row">
            <div class="large-12 medium-12 columns">
                <label id="comments">Comments<span style="color: red">*</span></label>
                <textarea name="comments" id="comments" class="text"
                          style="color: black;"></textarea>
            </div>
        </div>
        <div class="large-12 columns">
            <div class="button-group expanded">
                <a data-close="" class="button alert">Cancel</a>
                <input type="submit" class="button" value="Submit"/>
            </div>
        </div>

        <div class="row text-center">
            <!--            <div class="large-3 medium-3 columns text-center">-->
            <!--                <p>Think Safe, Inc.</p>-->
            <!--            </div>-->
            <!--            <div class="large-6 medium-6 columns text-center">-->
            <!--                <p>emma@think-safe.com</p>-->
            <!--            </div>-->
            <!--            <div class="large-3 medium-3 columns text-center">-->
            <!--                <p>(319)377-5125</p>-->
            <!--            </div>-->
            <p style="margin: 0 auto">Think Safe, Inc.<br>
                emma@think-safe.com<br>
                (319)377-5125</p>
        </div>
    </form>
    <script type="text/javascript">
        var vvfc_BaseURL = (("https:" == document.location.protocol) ? "https://forms.id-visitors.com/FrontEndWeb/" : "http://forms.id-visitors.com/FrontEndWeb/");
        var vvfc_ServiceURL = vvfc_BaseURL + 'ProcessFormCapture.aspx';
        var vvfc_ScriptURL = vvfc_BaseURL + 'Scripts/vvfcscript.js';
        var trackedForms = ['pricing-form'];
        var options = { accountId: 'uOb7EgGMeK', serviceURL: vvfc_ServiceURL, pollInterval: 5000,
            forms: [{ id: 'pricing-form', ignoreFields: ['__VIEWSTATE', '__VIEWSTATEGENERATOR', '__EVENTVALIDATION'], accountFormId: '5f98844c68b5f70ba4a6f467' } ]};
        (function (src) {
            var vvscript = document.createElement('script');
            vvscript.type = 'text/javascript';
            vvscript.async = true; vvscript.src = src;
            var scriptElements = document.getElementsByTagName('script');
            var lastScriptElement = scriptElements[scriptElements.length - 1];
            lastScriptElement.parentNode.insertBefore(vvscript, lastScriptElement.nextSibling);
        })(vvfc_ScriptURL);
    </script>
</div>


<div id="become-partner-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <form class="contact-us-form text-left" action="#" method="post"><input type="hidden" id="contact-us-type"
                                                                                 name="type">
        <div class="row">
            <div class="large-12 medium-12 columns" style="padding-bottom: .5vh">
                <b>Think Safe’s EMMA Partner Program (EPP) is for companies who have an existing app or synergy in the
                    industry and want to partner to bring one consolidated app or offering through EMMA’s OpenAPI.
                    Together, our companies can connect hardware and software solutions to make minutes
                    matter! </b><br/><br/>
                <b><i>Your information is never shared.</i></b><br/>
                <b><i>Confidentiality guaranteed.</i></b>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>First Name<span style="color: red">*</span></label>
                <input type="text" name="first-name" id="firstname"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Last Name<span style="color: red">*</span></label>
                <input type="text" name="last-name" id="lastname"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Email<span style="color: red">*</span></label>
                <input type="text" name="email" id="email"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Organization</label>
                <input type="text" name="org" id="org"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Mobile</label>
                <input type="text" name="mobile" id="mobile"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Landline</label>
                <input type="text" name="landline" id="landline"/>
            </div>
            <hr>
        </div>
        <div class="row">
            <div class="large-12 medium-12 columns">
                <label id="comments">Comments<span style="color: red">*</span></label>
                <textarea name="comments" id="comments" class="text"
                          style="color: black;"></textarea>
            </div>
        </div>
        <div class="large-12 columns">
            <div class="button-group expanded">
                <a data-close="" class="button alert">Cancel</a>
                <input type="submit" class="button" value="Submit"/>
            </div>
        </div>

        <div class="row text-center">
            <!--            <div class="large-3 medium-3 columns text-center">-->
            <!--                <p>Think Safe, Inc.</p>-->
            <!--            </div>-->
            <!--            <div class="large-6 medium-6 columns text-center">-->
            <!--                <p>emma@think-safe.com</p>-->
            <!--            </div>-->
            <!--            <div class="large-3 medium-3 columns text-center">-->
            <!--                <p>(319)377-5125</p>-->
            <!--            </div>-->
            <p style="margin: 0 auto">Think Safe, Inc.<br>
                emma@think-safe.com<br>
                (319)377-5125</p>
        </div>
    </form>
</div>

<div id="predownload-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <form id="predownload-form" class="text-left" action="#" method="post">
        <input type="hidden" id="download-type" name="type">
        <input type="hidden" id="download-link" name="link">
        <div class="row">
            <div class="large-12 medium-12 columns" style="padding-bottom: .5vh">
                <b><i>Your information is never shared.</i></b><br/>
                <b><i>Confidentially guaranteed.</i></b>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>First Name<span style="color: red">*</span></label>
                <input type="text" name="first-name" id="firstname"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Last Name<span style="color: red">*</span></label>
                <input type="text" name="last-name" id="lastname"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Email<span style="color: red">*</span></label>
                <input type="text" name="email" id="email"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Organization</label>
                <input type="text" name="org" id="org"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Mobile</label>
                <input type="text" name="mobile" id="mobile"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Landline</label>
                <input type="text" name="landline" id="landline"/>
            </div>
            <hr>
        </div>
        <div class="large-12 columns">
            <div class="button-group expanded">
                <a data-close="" class="button alert">Cancel</a>
                <input type="submit" class="button" value="Submit"/>
            </div>
        </div>
    </form>
</div>


<div id="become-advocate-modal" class="reveal callout text-center" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <form class="contact-us-form text-left" action="#" method="post"><input type="hidden" id="contact-us-type"
                                                                                 name="type">
        <div class="row">
            <div class="large-12 medium-12 columns" style="padding-bottom: .5vh">
                <b>Become an EMMA advocate for a safe workplace/learning environment by requesting your
                    EMMA Advocate Starter Kit below. This
                    kit includes a flyer and social media graphics that you can use to share the importance of keeping
                    our workplace/learning environment protected in active shooter and other life threatening situations. Share
                    with your workplace/learning environment, bring to your next
                    meeting, and send to other concerned individuals.</b><br/><br/>
                <b><i>Your information is never shared.</i></b><br/>
                <b><i>Confidentiality guaranteed.</i></b>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>First Name<span style="color: red">*</span></label>
                <input type="text" name="first-name" id="firstname"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Last Name<span style="color: red">*</span></label>
                <input type="text" name="last-name" id="lastname"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Email<span style="color: red">*</span></label>
                <input type="text" name="email" id="email"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Organization</label>
                <input type="text" name="org" id="org"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Mobile</label>
                <input type="text" name="mobile" id="mobile"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Landline</label>
                <input type="text" name="landline" id="landline"/>
            </div>
            <hr>
        </div>
        <div class="row">
            <div class="large-12 medium-12 columns">
                <label id="comments">Comments<span style="color: red">*</span></label>
                <textarea name="comments" id="comments" class="text"
                          style="color: black;"></textarea>
            </div>
        </div>
        <div class="large-12 columns">
            <div class="button-group expanded">
                <a data-close="" class="button alert">Cancel</a>
                <input type="submit" class="button" value="Submit"/>
            </div>
        </div>

        <div class="row text-center">
            <!--            <div class="large-3 medium-3 columns text-center">-->
            <!--                <p>Think Safe, Inc.</p>-->
            <!--            </div>-->
            <!--            <div class="large-6 medium-6 columns text-center">-->
            <!--                <p>emma@think-safe.com</p>-->
            <!--            </div>-->
            <!--            <div class="large-3 medium-3 columns text-center">-->
            <!--                <p>(319)377-5125</p>-->
            <!--            </div>-->
            <p style="margin: 0 auto">Think Safe, Inc.<br>
                emma@think-safe.com<br>
                (319)377-5125</p>
        </div>
    </form>
</div>