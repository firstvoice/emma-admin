<!--<div class="row expanded app-dashboard-top-nav-bar show-for-small-only">-->
<!--</div>-->

<?php

$r = false;
$regions = select_regions_with_userID($USER->id);
if($regions->num_rows > 0){
    $r = true;
}

?>


<div class="row expanded app-dashboard-top-nav-bar">
    <!--<div class="large-4 medium-4 small-3 columns">-->
    <?php
    if($r) {?>
        <div class="large-3 medium-3 small-4 columns">
    <?php
    }else{?>
        <div class="large-4 medium-4 small-4 columns">
    <?php
    }

    ?>
        <button data-toggle="app-dashboard-sidebar" class="menu-icon hide-for-medium"></button>
        <!--this is the code for the emma logo and caution sign in the left corner-->
        <!-- <a href="./dashboard.php?content=home" class="app-dashboard-logo hide-for-small-only"><img src="img/emma_icon.png" style="width:20px;height:20px;" /> EMMA</a>">-->

        <a href="./dashboard.php?content=home" class="app-dashboard-logo hide-for-small-only">
            <img src="img/emma_icon.png" style="width:20px;height:20px;" />
            EMMA
        </a>

    </div>
    <!--<div class="large-4 medium-4 small-9 columns">-->
    <?php
    if($r) {?>
        <div class="large-3 medium-3 small-8 columns text-center">
    <?php
    }else{?>
        <div class="large-4 medium-4 small-8 columns text-center">
    <?php
    }

    ?>
        <!---------Empty, this div prevents auto resizing.---->
        <?php if($USER->privilege->admin || $USER->privilege->security){ echo'
        <div class="text-center">
        <!--this is the button for create event in the center top bar-->
            <button id="main-create-event" data-open="create-event-modal" class="button hollow" aria-controls="create-event-modal" aria-haspopup="true" tabindex="0"><img src="img/emma_icon.png" style="margin-right: 1em;">Create Event<img src="img/emma_icon.png" style="margin-left: 1em;"></button>
        </div>';}
        ?>
    </div>


    <!--  <div style="z-index: 1;position: absolute;left:90%; top:.8%;">
        <div class="right-menu">
            <button class="menu-button"><?php //echo $USER->full_name;?></button>
            <div class="dropdown-menu">
                <a href="./dashboard.php?content=user&id=<?//php echo $USER->id; ?>">Account</a></button>
                <form type="post" action="logout.php">
                    <button type="submit" class="button hollow expanded">Logout</button>
                </form>
            </div>
        </div>
    </div> ---->
    <?php

    $m = false;
    $multiples = select_multiPlans_with_userID($USER->id);
    if($multiples->num_rows > 1){
        $m = true;
    }
    ?>
    <?php
    if($m || $USER->privilege->plan_god) {
        if($r){
    ?>
    <div class="large-1 medium-1 small-1 columns text-right hide-for-small-only" style="padding-right:0; color: white;">Region: </div>
    <div class="large-1 medium-1 small-1 columns text-right hide-for-small-only">
        <ul class="dropdown menu text-right expanded" data-dropdown-menu>
        <li>
            <select id="change_region_select">
                <option value="all">All</option>
                <?php
                    $regions = select_regions_with_userID($USER->id);
                    while($region = $regions->fetch_assoc()){
                        echo'<option value="'. $region['plan_region_id'] .'">'. $region['region_name'] .'</option>';
                    }
                ?>
            </select>
        </li>
        </ul>
    </div>
        <?php } ?>
        <!--this is the plan before the drop down on the right side -->
    <div class="large-1 medium-1 small-1 columns text-right hide-for-small-only" style="padding-right:0; color: white;">Plan: </div>
    <?php }?>
    <div class="large-3 columns medium-3 small-4 hide-for-small-only" style="padding-left: 3;">
        <ul class="dropdown menu text-right expanded" data-dropdown-menu>
            <?php
            if($m || $USER->privilege->plan_god) {
                ?>
                <li>
                    <select id="change_plan_select">
                        <?php if ($USER->privilege->plan_god) {
                            $multi = array();
                            $multiplans = select_multiPlanIDs_with_userID($USER->id);
                            while ($multiplan = $multiplans->fetch_assoc()) {
                                $multi[] = $multiplan['plan_id'];
                            }

                            $plans = select_allActivePlans();
                            while ($selectedPlan = $plans->fetch_assoc()) {
                                $regionarr = array();
                                $regions = select_regions_with_planID($selectedPlan['emma_plan_id']);
                                while ($region = $regions->fetch_assoc()){
                                    $regionarr[] = $region['plan_region_id'];
                                }
                                if (in_array($selectedPlan['emma_plan_id'], $multi)) {
                                    if ($selectedPlan['emma_plan_id'] == $USER->emma_plan_id) {
                                        echo '<option value="' . $selectedPlan['emma_plan_id'] . '" data-region="'. implode(' ', $regionarr) .'" style="color: #0078c1" selected>' . $selectedPlan['name'] . '</option>';
                                    } else {
                                        echo '<option value="' . $selectedPlan['emma_plan_id'] . '" data-region="'. implode(' ', $regionarr) .'" style="color: #0078c1">' . $selectedPlan['name'] . '</option>';
                                    }
                                } else {
                                    if ($selectedPlan['emma_plan_id'] == $USER->emma_plan_id) {
                                        echo '<option value="' . $selectedPlan['emma_plan_id'] . '" data-region="'. $selectedPlan['region_id'] .'" selected>' . $selectedPlan['name'] . '</option>';
                                    } else {
                                        echo '<option value="' . $selectedPlan['emma_plan_id'] . '" data-region="'. $selectedPlan['region_id'] .'">' . $selectedPlan['name'] . '</option>';
                                    }
                                }
                            }
                        } else {
                            $plans = select_multiPlanInfo_with_userID($USER->id);
                            while ($selectedPlan = $plans->fetch_assoc()) {
                                $regionarr = array();
                                $regions = select_regions_with_planID($selectedPlan['emma_plan_id']);
                                while ($region = $regions->fetch_assoc()){
                                    $regionarr[] = $region['plan_region_id'];
                                }
                                if ($selectedPlan['emma_plan_id'] == $USER->emma_plan_id) {
                                    echo '<option value="' . $selectedPlan['emma_plan_id'] . '" data-region="'. implode(' ', $regionarr) .'" selected>' . $selectedPlan['name'] . '</option>';
                                } else {
                                    echo '<option value="' . $selectedPlan['emma_plan_id'] . '" data-region="'. implode(' ', $regionarr) .'">' . $selectedPlan['name'] . '</option>';
                                }
                            }
                        }

                        ?>
                    </select>
                </li>
                <?php
            }else{
                echo '<li style="width: 50%"></li>';
            }
            ?>
            <li><a class=menu-item style="top: 2%; color: white"><?php echo $USER->full_name; ?>  <i id="arrow" class="fa fa-sort-down" style="padding-bottom: 8px;color: white; position: absolute; left: 93%; top: 20%"></i></a>
                <ul class="menu vertical">
                    <li><a class="menu-item" href="dashboard.php?content=profile">Profile</a></li>
                    <li><a class="menu-item" data-open="log-out-modal">Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>

<div class="row expanded app-dashboard-top-nav-bar show-for-small-only">
    <div class="large-12 medium-12 small-12 columns">
    <?php
    $m = false;
    $multiples = select_topBar_multiples($USER->id);
//        $fvmdb->query("
//        SELECT p.*
//        FROM emma_plans p
//        LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id
//        WHERE mp.user_id = '" . $USER->id . "'
//        AND p.date_expired > NOW()
//    ");
    if($multiples->num_rows > 1){
        $m = true;
    }
    ?>
    <?php
    if($m || $USER->privilege->plan_god) {
        if($r){
            ?>
            <div class="row">
                <div class="large-1 medium-1 small-2 columns text-right" style="padding-right:0; color: white;">Region: </div>
                <div class="large-1 medium-2 small-10 columns text-right">
                    <ul class="dropdown menu text-right expanded" data-dropdown-menu>
                        <li>
                            <select id="change_region_select">
                                <option value="all">All</option>
                                <?php
                                $regions = select_regions_with_userID($USER->id);
                                while($region = $regions->fetch_assoc()){
                                    echo'<option value="'. $region['plan_region_id'] .'">'. $region['region_name'] .'</option>';
                                }
                                ?>
                            </select>
                        </li>
                    </ul>
                </div>
            </div>
        <?php } ?>
        <!--this is the plan right side selection on small screens. changed small-1 and small-11-->
    <?php } ?>
        <div class="row">
        <?php
        if($m || $USER->privilege->plan_god){
        ?>
            <div class="large-1 medium-1 small-2 columns text-right" style="color: white;">Plan: </div>
        <?php }?>
            <div class="large-2 columns medium-2 small-10">
                <ul class="dropdown menu text-right expanded" data-dropdown-menu>
                    <?php
                    if($m || $USER->privilege->plan_god) {
                        ?>
                        <li>
                            <select id="change_plan_select">
                                <?php if ($USER->privilege->plan_god) {
                                    $multi = array();
                                    $multiplans = select_multiPlanIDs_with_userID($USER->id);
        //                                $fvmdb->query("
        //                            SELECT e.plan_id
        //                            FROM emma_multi_plan e
        //                            WHERE e.user_id = $USER->id
        //                        ");
                                    while ($multiplan = $multiplans->fetch_assoc()) {
                                        $multi[] = $multiplan['plan_id'];
                                    }

                                    $plans = select_topBar_plansPlanGod();
        //                                $fvmdb->query("
        //                            SELECT p.*
        //                            FROM emma_plans p
        //                            WHERE p.date_expired > NOW()
        //                            ORDER BY p.name
        //                        ");
                                    while ($selectedPlan = $plans->fetch_assoc()) {
                                        $regionarr = array();
                                        $regions = select_regions_with_planID($selectedPlan['emma_plan_id']);
                                        while ($region = $regions->fetch_assoc()){
                                            $regionarr[] = $region['plan_region_id'];
                                        }
                                        if (in_array($selectedPlan['emma_plan_id'], $multi)) {
                                            if ($selectedPlan['emma_plan_id'] == $USER->emma_plan_id) {
                                                echo '<option value="' . $selectedPlan['emma_plan_id'] . '" style="color: #0078c1" data-region="'. implode(' ', $regionarr) .'" selected>' . $selectedPlan['name'] . '</option>';
                                            } else {
                                                echo '<option value="' . $selectedPlan['emma_plan_id'] . '" style="color: #0078c1" data-region="'. implode(' ', $regionarr) .'">' . $selectedPlan['name'] . '</option>';
                                            }
                                        } else {
                                            if ($selectedPlan['emma_plan_id'] == $USER->emma_plan_id) {
                                                echo '<option value="' . $selectedPlan['emma_plan_id'] . '" data-region="'. implode(' ', $regionarr) .'" selected>' . $selectedPlan['name'] . '</option>';
                                            } else {
                                                echo '<option value="' . $selectedPlan['emma_plan_id'] . '" data-region="'. implode(' ', $regionarr) .'">' . $selectedPlan['name'] . '</option>';
                                            }
                                        }
                                    }
                                } else {
                                    $plans = select_topBar_plans($USER->id);
        //                                $fvmdb->query("
        //                            SELECT p.*
        //                            FROM emma_plans p
        //                            LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id
        //                            WHERE mp.user_id = '" . $USER->id . "'
        //                            AND p.date_expired > NOW()
        //                            ORDER BY p.name
        //                        ");
                                    while ($selectedPlan = $plans->fetch_assoc()) {
                                        $regionarr = array();
                                        $regions = select_regions_with_planID($selectedPlan['emma_plan_id']);
                                        while ($region = $regions->fetch_assoc()){
                                            $regionarr[] = $region['plan_region_id'];
                                        }
                                        if ($selectedPlan['emma_plan_id'] == $USER->emma_plan_id) {
                                            echo '<option value="' . $selectedPlan['emma_plan_id'] . '" data-region="'. implode(' ', $regionarr) .'" selected>' . $selectedPlan['name'] . '</option>';
                                        } else {
                                            echo '<option value="' . $selectedPlan['emma_plan_id'] . '" data-region="'. implode(' ', $regionarr) .'">' . $selectedPlan['name'] . '</option>';
                                        }
                                    }
                                }

                                ?>
                            </select>
                        </li>
                        <?php
                    }else{
                        echo '<li style="width: 50%"></li>';
                    }
                    ?>
                    <!--this is the farthest right drop down for plan name and profile log out dropdown-->
                    <li><a class=menu-item style="top: 2%; color: white;" data-open="confirm-password-modal"><?php echo $USER->first_name; ?>  <i id="arrow" class="fa fa-sort-down" style="padding-bottom: 8px;color: white; position: absolute; left: 93%; top: 20%;"></i></a>
                        <ul class="menu vertical">
                            <li><a class="menu-item" data-open="confirm-password-modal">Profile</a></li>
                            <li><a class="menu-item" data-open="log-out-modal">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
