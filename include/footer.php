<footer>
    <div class="line">

        <br><br>
        <br><br><br>
        <hr>

    </div>
    <div class="row expanded" style="padding-bottom: 5px">
        <div class="large-6 medium-6 small-12 columns">
            <p>&copy; 2018 - <?php echo date("Y"); ?> First Voice Apps by Think Safe, Inc. Patents Pending</p>
        </div>
        <div class="large-3 medium-6 small-12 columns" style="margin: 0 auto;">
            <?php
            if (HOMESITELINK1 != '' || HOMESITELOGO1 != '' || HOMESITELOGO1SIZE != '' || HOMESITELOGO1SIZE) {
                echo '    <a target="_blank" href="' . HOMESITELINK1 . '"><img src="img/' . HOMESITELOGO1 . '" width="' . HOMESITELOGO1SIZE . '" height="' . HOMESITELOGO1SIZE . '"></a> <img src="img/bar.png" />';
            }
            if (HOMESITELINK2 != '' || HOMESITELOGO2 != '' || HOMESITELOGO2SIZE != '' || HOMESITELOGO2SIZE) {
                echo '<a target="_blank" href="' . HOMESITELINK2 . '"><img src="img/' . HOMESITELOGO2 . '" width="' . HOMESITELOGO2SIZE . '" height="' . HOMESITELOGO2SIZE . '"></a> <br>';
            }
            ?>
        </div>
    </div>

</footer>
