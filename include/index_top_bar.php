<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-179662500-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-179662500-1');
</script>


<div class="top-bar" style="border-bottom: 2px solid #0079c1;">
    <div class="row expanded">

<!--        <div id="small-top_menu" class="large-6 medium-12 small-3 columns">-->
<!--            <div class="title-bar" data-responsive-toggle="responsive-menu_small" data-hide-for="large">-->
<!--                <button class="menu-icon" type="button" data-toggle="responsive-menu_small"></button>-->
<!--                <div class="title-bar-title">-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="top-bar" id="responsive-menu_small">-->
<!--                <ul class="dropdown vertical menu menu-links" data-dropdown-menu>-->
<!--                    <li><a href="#">Videos</a></li>-->
<!--                    <li><a href="#">EMMA in Use</a></li>-->
<!--                    <li><a href="#">Industries</a></li>-->
<!--                    <li><a href="#">Company</a></li>-->
<!--                    <li><a href="#">Resources</a></li>-->
<!--                    <li><a href="#">Features</a></li>-->
<!--                    <li><a data-open="login-modal">LOGIN</a></li>-->
<!--                </ul>-->
<!--            </div>-->
<!--        </div>-->

        <div class="large-1 medium-4 small-4 columns">
            <a href="https://emmanotify.com"><img src="img/emma_logo.png"></a>
        </div>


<!--        <div id="small-top_menu" class="large-2 medium-8 small-8 columns">-->
<!--            <div class="title-bar" data-responsive-toggle="responsive-menu_small" data-hide-for="large">-->
<!--                <button class="menu-icon" type="button" data-toggle="responsive-menu_small"></button>-->
<!--                <div class="title-bar-title">-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="top-bar" id="responsive-menu_small">-->
<!--                <ul class="dropdown vertical menu menu-links" data-dropdown-menu>-->
<!--                    <li><a href="emma_videos.php">Videos</a></li>-->
<!--                    <li><a href="emma_in_use.php">EMMA in Use</a></li>-->
<!--                    <li><a href="industries.php">Industries</a></li>-->
<!--                    <li><a href="company.php">Company</a></li>-->
<!--                    <li><a href="partners.php">Partners</a></li>-->
<!--                    <li><a href="resources.php">Resources</a></li>-->
<!--                    <li><a href="features.php">Features</a></li>-->
<!--                    <li><a href="downloads.php">Downloads</a></li>-->
<!--                    <li><a data-open="contact-us-modal">Contact Us</a></li>-->
<!--                    <li><a data-open="login-modal">LOGIN</a></li>-->
<!--                </ul>-->
<!--            </div>-->
<!--        </div>-->

        <div class="large-6 medium-8 small-8 columns">
            <div class="row" style="padding-top:1em">
                <span><a href="mailto:emma@think-safe.com"><span><b>emma@think-safe.com</b></span></a><span style="padding-right: 30px"></span><a href="tel:888-473-1777"><b>888-473-1777</b></a></span>
            </div>
<!--            <div class="row" style="padding-top:1em">-->
<!--                -->
<!--            </div>-->
        </div>


        <div class="large-2 medium-6 small-6 columns">

        </div>

<!--        <div id="large-top_menu" class="large-7 medium-6 small-12 columns">-->
<!---->
<!--            <ul class="dropdown menu menu-links" data-dropdown-menu>-->
<!--                <li><a href="emma_videos.php">Videos</a></li>-->
<!--               <li><a href="emma_in_use.php">EMMA in Use</a></li>-->
<!--                <li><a href="industries.php">Industries</a></li>-->
<!--                <li><a href="company.php">Company</a></li>-->
<!--                <li><a href="partners.php">Partners</a></li>-->
<!--                <li><a href="resources.php">Resources</a></li>-->
<!--                <li><a href="features.php">Features</a></li>-->
<!--                <li><a href="downloads.php">Downloads</a></li>-->
<!--                <li><a data-open="contact-us-modal">Contact Us</a></li>-->
<!--                <li><a data-open="login-modal">LOGIN</a></li>-->
<!--            </ul>-->
<!---->
<!--        </div>-->
    </div>
</div>

<?php if($errorMessage != ''){
echo '<div class="error-message text-center" style="background-color: red; margin-bottom: 0; padding-bottom: 0;"><h5>'.$errorMessage.'</h5></div>';
$errorMessage = '';
}
?>
