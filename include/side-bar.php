<div id="app-dashboard-sidebar"
     class="app-dashboard-sidebar position-left off-canvas off-canvas-absolute reveal-for-medium"
     data-off-canvas data-sticky>
    <div class="app-dashboard-sidebar-title-area">
        <div class="app-dashboard-close-sidebar">
            <!-- Close button -->
            <button id="close-sidebar" data-app-dashboard-toggle-shrink
                    class="toggler app-dashboard-sidebar-close-button show-for-medium"
                    aria-label="Close menu"
                    type="button">
        <span aria-hidden="true"><a href="#"><i
                        class="large fa fa-angle-double-left"></i></a></span>
            </button>
        </div>
        <div class="app-dashboard-open-sidebar">
            <!-- Open button -->
            <button id="open-sidebar" data-app-dashboard-toggle-shrink
                    class="toggler app-dashboard-open-sidebar-button show-for-medium"
                    aria-label="open menu"
                    type="button">
        <span aria-hidden="true"><a href="#"><i
                        class="large fa fa-angle-double-right"></i></a></span>
            </button>
        </div>
    </div>
    <div class="app-dashboard-sidebar-inner sticky">
        <ul class="menu vertical">
            <?php

            $types = select_eventTypes_with_multiplans($USER->emma_home_plans);
            if($types->num_rows > 0){
                $events = true;
            }

            if ($USER->privilege->admin) {
                echo '
        <li>
          <a href="./dashboard.php" class="is-active" title="Home">
            <i class="large fa fa-home"></i><span
              class="app-dashboard-sidebar-text">Home</span>
          </a>
        </li>';
            }
            if(!$USER->privilege->admin && !$USER->privilege->security){
                echo '
        <li>
          <a href="./dashboard.php?content=user_home" class="is-active" title="Home">
            <i class="large fa fa-home"></i><span
              class="app-dashboard-sidebar-text">Home</span>
          </a>
        </li>
        ';
            }
//            echo '
//        <li>
//          <a href="./dashboard.php?content=aeds" class="is-active" title="Home">
//            <i class="large fa fa-briefcase"></i><span
//              class="app-dashboard-sidebar-text">AEDs</span>
//          </a>
//        </li>
//        ';
//            echo '
//        <li>
//          <a href="./dashboard.php?content=map" class="is-active" title="Home">
//            <i class="large fa fa-map"></i><span
//              class="app-dashboard-sidebar-text">Map</span>
//          </a>
//        </li>
//        ';
            if (($USER->privilege->security || $USER->privilege->admin) && $events) {
                echo '
        <li>
          <a href="./dashboard.php?content=events" title="Events">
            <i class="large fa fa-exclamation-triangle"></i><span
              class="app-dashboard-sidebar-text">Events</span>
          </a>
        </li>';
            }
            if($USER->privilege->admin911 && !($USER->privilege->security || $USER->privilege->admin)){

                $activeLives = select_activeEmergencies_and_emergencyType_with_planID($USER->emma_plan_id);
                if ($activeLive = $activeLives->fetch_assoc()) {
                    $activeEventId = $activeLive['emergency_id'];
                    echo '
            <li>
              <a href="./dashboard.php?content=event&id='.$activeEventId.'" title="Current Event">
                <i class="large fa fa-exclamation-triangle"></i><span
                  class="app-dashboard-sidebar-text">Current Event</span>
              </a>
            </li>';
                }
            }
            if (($USER->privilege->security || $USER->privilege->admin) && $events) {
                echo '
        <li>
          <a href="./dashboard.php?content=drills" title="Drills">
            <i class="large fa fa-clock-o"></i><span
              class="app-dashboard-sidebar-text">Drills</span>
          </a>
        </li>';
            }
            if (($USER->privilege->security || $USER->privilege->admin) && $USER->privilege->plansecurity) {
                echo '
        <li>
          <a href="./dashboard.php?content=securities" title="Security">
            <i class="large fa fa-shield"></i><span
              class="app-dashboard-sidebar-text">Security</span>
          </a>
        </li>';
            }
            if ($USER->privilege->security || $USER->privilege->admin) {
                echo '
        <li>
          <a href="./dashboard.php?content=mass_communications" title="Mass Notification">
            <i class="large fa fa-bell"></i><span
              class="app-dashboard-sidebar-text">Mass Notification</span>
          </a>
        </li>';
            }
            if (($USER->privilege->security || $USER->privilege->admin) && $USER->privilege->plansos) {
                echo '
        <li>
          <a href="./dashboard.php?content=sos" title="EMMA SOS">
            <i class="large fa fa-life-ring"></i><span
              class="app-dashboard-sidebar-text">EMMA SOS</span>
          </a>
        </li>';
            }
            if (($USER->privilege->security || $USER->privilege->admin) && $USER->privilege->planlockdown) {
                echo '
        <li>
          <a href="./dashboard.php?content=lockdowns" title="LOCKDOWN!">
            <i class="large fa fa-lock"></i><span
              class="app-dashboard-sidebar-text">LOCKDOWN!</span>
          </a>
        </li>
        <li>
          <a href="./dashboard.php?content=lockdown_drills" title="LOCKDOWN! (DRILL)">
            <i class="large fa fa-lock"></i><span
              class="app-dashboard-sidebar-text">LOCKDOWN! (DRILL)</span>
          </a>
        </li>
        <li>
          <a href="./dashboard.php?content=modified_lockdowns" title="Modified LOCKDOWN!">
            <i class="large fa fa-lock"></i><span
              class="app-dashboard-sidebar-text">Modified LOCKDOWN!</span>
          </a>
        </li>';
            }
            if (($USER->privilege->security || $USER->privilege->admin) && $USER->privilege->ar) {
                echo '
        <li>
          <a href="./dashboard.php?content=anonymous_reports" title="Anonymous Report">
            <i class="large fa fa-eye-slash"></i><span
              class="app-dashboard-sidebar-text">Anonymous Reports</span>
          </a>
        </li>';
            }if (($USER->privilege->security || $USER->privilege->admin) && $USER->privilege->mar) {
                echo '
        <li>
          <a href="./dashboard.php?content=anonymous_reports_moss" title="Anonymous Report">
            <i class="large fa fa-eye-slash"></i><span
              class="app-dashboard-sidebar-text">Anonymous Reports</span>
          </a>
        </li>';
            }
            if ($USER->privilege->security || $USER->privilege->admin) {
                echo '
        <li>
          <a href="./dashboard.php?content=police_calls" title="Police Calls">
            <i class="large fa fa-phone"></i><span
              class="app-dashboard-sidebar-text">911 Calls</span>
          </a>
        </li>
        ';
            }
            if ($USER->privilege->security || $USER->privilege->admin || $USER->privilege->admin911) {
                echo '
        <li>
          <a href="./dashboard.php?content=resources" title="Resources">
            <i class="large fa fa-map-o"></i><span
              class="app-dashboard-sidebar-text">Resources</span>
          </a>
        </li>
        ';
            }
            if ($USER->privilege->admin) {
                echo '
        <li>
          <a href="./dashboard.php?content=assets" title="Assets">
            <i class="large fa fa-cube"></i><span
              class="app-dashboard-sidebar-text">Assets</span>
          </a>
        </li>';
            }
            if ($USER->privilege->admin) {
                echo '
        <li>
          <a href="./dashboard.php?content=users" title="Users">
            <i class="large fa fa-user-circle"></i><span
              class="app-dashboard-sidebar-text">Users</span>
          </a>
        </li>';
              echo '
        <li>
          <a href="./dashboard.php?content=responders" class="is-active" title="Home">
            <i class="large fa fa-group"></i><span
              class="app-dashboard-sidebar-text">Responders</span>
          </a>
        </li>
        ';
            }if ($USER->privilege->admin_user){
                echo '
        <li>
          <a href="./dashboard.php?content=admin_users" title="Admin">
            <i class="large fa fa-id-card"></i><span
              class="app-dashboard-sidebar-text">All Plan Users</span>
          </a>
        </li>';
            }
            if ($USER->privilege->admin) {
                echo '
        <li>
          <a href="./dashboard.php?content=groups" title="Groups">
            <i class="large fa fa-group"></i><span
              class="app-dashboard-sidebar-text">Groups</span>
          </a>
        </li>';
            }
            if ($USER->privilege->admin) {
                echo '
        <li>
          <a href="./dashboard.php?content=sites" title="Sites">
            <i class="large fa fa-institution"></i><span
              class="app-dashboard-sidebar-text">Sites</span>
          </a>
        </li>';
            }
            if ($USER->privilege->admin) {
                echo '
        <li>
          <a href="./dashboard.php?content=reports" title="Reports">
            <i class="large fa fa-pie-chart"></i><span
              class="app-dashboard-sidebar-text">Reports</span>
          </a>
        </li>
        ';
            }
            //      if ($USER->privilege->admin && $USER->privilege->code_generator) {
            //        echo '
            //        <li>
            //          <a href="./dashboard.php?content=code_generator" title="Code Generator">
            //            <i class="large fa fa-cogs"></i><span
            //              class="app-dashboard-sidebar-text">Code Generator</span>
            //          </a>
            //        </li>
            //        ';
            //      }

            if ($USER->privilege->admin && $USER->privilege->code_generator) {
                echo '
        <li>
          <a href="./dashboard.php?content=guest_codes" title="Guest Codes">
            <i class="large fa fa-ticket"></i><span
              class="app-dashboard-sidebar-text">Guest Codes</span>
          </a>
        </li>
        ';
            }
            if ($USER->privilege->geofences) {
                echo '
        <li>
          <a href="./dashboard.php?content=geofences" title="Geofences">
            <i class="large fa fa-globe"></i><span
              class="app-dashboard-sidebar-text">Geofences</span>
          </a>
        </li>
        ';
            }elseif($USER->privilege->admin){
                echo '
        <li>
          <a href="./dashboard.php?content=geofence_demo" title="Geofences">
            <i class="large fa fa-globe"></i><span
              class="app-dashboard-sidebar-text">Geofences</span>
          </a>
        </li>
        ';
            }
            if ($USER->privilege->admin && $USER->privilege->plans) {
                echo '
        <li>
          <a href="./dashboard.php?content=plans" title="Plans">
            <i class="large fa fa-book"></i><span
              class="app-dashboard-sidebar-text">Plans</span>
          </a>
        </li>
        ';
            }
            if ($USER->privilege->activate_emails) {
                echo '
        <li>
          <a href="./dashboard.php?content=send_user_emails" title="User Emails">
            <i class="large fa fa-envelope"></i><span
              class="app-dashboard-sidebar-text">Activation Emails</span>
          </a>
        </li>
        ';
            }

            echo '
            <li>
                <a href="./dashboard.php?content=help" title="Help">
                    <i class="large fa fa-question"></i>
                    <span class="app-dashboard-sidebar-text">Help</span>
                </a>
            </li>
            ';
            ?>
        </ul>
    </div>
</div>
