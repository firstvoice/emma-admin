<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 11/1/2017
 * Time: 9:44 AM
 */

define('__ROOT__', $_SERVER['HTTP_HOST'] . dirname(dirname($_SERVER['PHP_SELF'])));

define('SMTP', 'retrac.xyz');
define('microsoftSMTP', 'smtp.office365.com');
define('FROMEMAIL', 'donotreply@emmaadmin.com');
define('PROGRAM', 'Emma Admin');

define('HOMEPAGELOGO' , 'fvs_background_logo.png');
define('HOMESITELOGO1' , 'footer_ThinkSafe_logo.png');
define('HOMESITELINK1' , 'https://think-safe.com');
define('HOMESITELOGO1SIZE' , '42%');
define('HOMESITELOGO2' , 'footer_FirstVoice_logo.png');
define('HOMESITELINK2' , 'https://firstvoice.us');
define('HOMESITELOGO2SIZE' , '35%');
define('WEBLINK' , 'https://tsdemos.com/egor/emma-admin-local/');
define('URL', 'https://www.emmaadmin.com/');
define('EMMALOGO', 'img/emma_icon.png');

if(dirname($_SERVER['DOCUMENT_ROOT']) == "/home/admin/public_html"){
    require_once(dirname($_SERVER['DOCUMENT_ROOT']).'/../global_library/db_connection.php');
}
else{
    require_once(dirname($_SERVER['DOCUMENT_ROOT']).'/global_library/db_connection.php');
}
//$SMT = $_SwiftMailerTransport;
$SMT = $_SwiftMailerTransport;
$fvmdb = $_fvmdb;
$webdb = $_webdb;
$crmdb = $_crmdb;
$fvtdb = $_fvtdb;
$emmadb = $_emmadb;
$fvmdb->set_charset('utf8');
$crmdb->set_charset('utf8');
$fvtdb->set_charset('utf8');
$emmadb->set_charset('utf8');

if(basename(__DIR__) == 'emma-admin-local'){
    require_once('/queries/insert.php');
    require_once('/queries/update.php');
    require_once('/queries/select.php');
    require_once('/queries/deletes.php');
}
else {
    require_once(dirname(__DIR__).'/queries/insert.php');
    require_once(dirname(__DIR__).'/queries/update.php');
    require_once(dirname(__DIR__).'/queries/select.php');
    require_once(dirname(__DIR__).'/queries/deletes.php');
}

function randomToken ($length = 32) {
  if (!isset($length)) {
    $length = 32;
  }
  if (function_exists('random_bytes')) {
    return random_bytes($length);
  }
  if (function_exists('mcrypt_create_iv')) {
    return mcrypt_create_iv($length, MCRYPT_DEV_URANDOM);
  }
  if (function_exists('openssl_random_pseudo_bytes')) {
    return openssl_random_pseudo_bytes($length);
  }
  throw new Exception("No valid function available");
}

if (isset($_GET['authenticate'])) {
  try {
    $authenticate = $fvmdb->real_escape_string($_GET['authenticate']);
    $users = $fvmdb->query("
      SELECT u.id, CONCAT(u.firstname, ' ', u.lastname) AS full_name, u.username, u.emma_plan_id, u.emma_pin, u.auth, g.admin
      FROM users u
      JOIN emma_user_groups ug on u.id = ug.user_id
      JOIN emma_groups g on ug.emma_group_id = g.emma_group_id
      WHERE u.auth = '" . $username . "'
      ORDER BY g.admin DESC
    ");
    if ($users->num_rows > 0) {
      $user = $users->fetch_assoc();

      if ($user['admin']) {
        $tokenId = base64_encode(randomToken(32));
        $issuedAt = time();
        $notBefore = $issuedAt;
        $expire = $notBefore + (10 * 365 * 24 * 60 * 60);
        $serverName = 'retrac.xyz';

        $data = [
          'iat' => $issuedAt,
          'jti' => $tokenId,
          'iss' => $serverName,
          'nbf' => $notBefore,
          'exp' => $expire,
          'data' => [
            'id' => $user['id'],
            'full_name' => $user['full_name'],
            'username' => $user['username'],
            'emma_plan_id' => $user['emma_plan_id'],
            'emma_pin' => $user['emma_pin'],
            'auth' => $user['auth']
          ]
        ];

        $jwt = Firebase\JWT\JWT::encode($data, $CONFIG->key, 'HS512');
        setcookie('jwt', $jwt);
        header('Location: dashboard.php');
      } else {
        $errorMessage = 'User is not in an Admin group';
      }
    } else {
      $errorMessage = 'Invalid Username or Password';
    }
  } catch (Exception $e) {
    $e->getMessage();
  }

}