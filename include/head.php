<!-- Meta data -->
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<!-- Stylesheets -->
<link rel="stylesheet" href="css/foundation.css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
<!--<link href="https://use.fontawesome.com/releases/v5.0.2/css/all.css" rel="stylesheet">-->
<link rel="stylesheet" href="css/main.css"/>
<link rel="stylesheet" href="css/alert-callout.css"/>
<link rel="stylesheet" href="css/card-user-profile.css"/>
<link rel="stylesheet" href="css/info-card.css"/>
<link rel="stylesheet" href="css/responsive-timeline.css"/>
<link rel="stylesheet" href="css/clipped-circle-graph.css"/>
<link rel="stylesheet" href="NewDataTables/DataTables/datatables.css"/>
<link rel="stylesheet" href="css/stats.css"/>
<link rel="stylesheet" href="css/messages.css"/>
<link rel="stylesheet" type="text/css" href="css/vis-timeline-graph2d.css">

<!-- Javascripts -->
<script type="text/javascript" src="js/vendor/moment.js"></script>
<script type="text/javascript" src="js/vendor/vis.js"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110238940-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-110238940-1');
</script>

