<?php include('include/pre_login_header.php'); ?>
<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Help Center</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #FFFFFF;">


<?php include('include/index_top_bar.php'); ?>

<div class="row expanded the-clock" style="height: 100%">
    <div class="large-12 medium-12 small-12 columns" style="padding-bottom: 2em;">
        <br />
        <br />
        <br />
        <p class="MMM-header" style="text-align: center;">COMING SOON</p>
        <br />
        <br />
        <br />

        <br />
    </div>
<!--    <div class="large-12 medium-12 small-12 columns">-->
<!--        <div class="row expanded" style="padding-bottom: 1em;text-align: center;">-->
<!--            <div class="large-3 medium-6 small-6 column">-->
<!--                <div class="clear_card">-->
<!--                    <a target="_blank" href="documents/EMMA_Continum_of_Care_v3.pdf"><img-->
<!--                                src="img/EMMA_Continum_of_Care_v3.jpg" width="50%" height="50%"/>-->
<!--                        <p>EMMA’s Impact on the Continuum of Care in Pre-911 Dispatched Emergency Responder Arrival</p>-->
<!--                    </a>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="large-3 medium-6 small-6 column">-->
<!--                <div class="clear_card">-->
<!--                    <a target="_blank" href="documents/EMMA_ENS_features_2019.pdf"><img-->
<!--                                src="img/EMMA_ENS_features_2019.jpg"-->
<!--                                width="50%" height="50%"/>-->
<!--                        <p>EMMA ENS & Notifications Features Sheet</p>-->
<!--                    </a>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="large-3 medium-6 small-6 column">-->
<!--                <div class="clear_card">-->
<!--                    <a target="_blank" href="documents/EMMA_FAB_Users-Admins_2019.pdf"><img-->
<!--                                src="img/EMMA_FAB_Users-Admins_2019.jpg"-->
<!--                                width="50%" height="50%"/>-->
<!--                        <p>EMMA Features & Benefits for App Users & EMMA Administrators</p>-->
<!--                    </a>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="large-3 medium-6 small-6 column">-->
<!--                <div class="clear_card">-->
<!--                    <a target="_blank" href="documents/EMMA_Response_Graphic_Sales_Sheet_3.2018.pdf"><img-->
<!--                                src="img/EMMA_Response_Graphic_Sales_Sheet_3_2018.jpg"-->
<!--                                width="50%" height="50%"/>-->
<!--                        <p>EMMA Response Graphics Sales Sheet 2018</p>-->
<!--                    </a>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row expanded" style="padding-bottom: 1em;text-align: center;">-->
<!--            <div class="large-3 medium-6 small-6 column">-->
<!--                <div class="clear_card">-->
<!--                    <a target="_blank" href="documents/Emma_public_private_911_app_sheet_2019-02-13_sm.pdf"><img-->
<!--                                src="img/Emma_public_private_911_app_sheet_2019-02-13_sm.jpg"-->
<!--                                width="50%" height="50%"/>-->
<!--                        <p>EMMA Public & Private Use </p>-->
<!--                    </a>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div><p>&nbsp;</p><br/><br/><br/><br/><br/><br/><br/></div>-->
</div>
<?php include('include/footer.php'); ?>
<?php include('include/pre_login_modals.php'); ?>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>


