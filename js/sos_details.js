let map;
let bounds;
let updateInterval = 5000;
let infowindow;
let interval = 60;

function initMap() {
  let map = new google.maps.Map(document.getElementById('sos-map'), {
    zoom: 2,
    center: {lat: 0, lng: 0},
  });

  let infowindow = new google.maps.InfoWindow({
    content: 'SOS',
  });

  var sosIds = [];
  var markers = [];

  var bounds = new google.maps.LatLngBounds();
  let sos = mainSos;
  let marker;
  if (sos['help_date']) {
    marker = new google.maps.Marker({
      position: {
        lat: parseFloat(sos['help_lat']),
        lng: parseFloat(sos['help_lng']),
      },
      icon: 'img/help.png',
      map: map,
      user: sos['user'],
    });
  } else {
    marker = new google.maps.Marker({
      position: {
        lat: parseFloat(sos['pending_lat']),
        lng: parseFloat(sos['pending_lng']),
      },
      icon: 'img/pending.png',
      map: map,
      user: sos['user'],
    });
  }
  google.maps.event.addListener(marker, 'click', function () {
    var html = '' +
      '<h6>' + this.user + '</h6>' +
      '';
    infowindow.setContent(html);
    infowindow.open(map, this);
  });
  bounds.extend(marker.position);
  map.fitBounds(bounds);
}

$(document).ready(function () {
  console.log(mainSos);

  $('#close-sos').submit(function () {
    $.ajax({
      dataType: 'json',
      url: 'process/close_sos.php',
      type: 'POST',
      data: $('#close-sos').serialize(),
      beforeSend: function () {
        $('#close-sos').find('input[type="submit"]').attr('disabled', true);
        THINKINGMODAL.foundation('open');
      },
    }).done(function (data) {
      console.log(data);
      if (data.success) {
        $('#success_modal').foundation('open');
      } else {
        FAILMODAL.foundation('open');
      }
    }).fail(function (data, textStatus, errorThrown) {
      console.log(data);
      console.log(textStatus);
      console.log(errorThrown);
    });
    return false;
  });

  $('#sos-response-form').submit(function () {
    $.ajax({
      dataType: 'json',
      url: 'process/sos_response.php',
      type: 'POST',
      data: $(this).serialize(),
      beforeSend: function () {
        THINKINGMODAL.foundation('open');
      },
    }).done(function (data) {
      console.log(data);
      if (data.success) {
        $('#success_modal').foundation('open');
      } else {
        FAILMODAL.foundation('open');
      }
    }).fail(function (data, textStatus, errorThrown) {
      console.log(data);
      console.log(textStatus);
      console.log(errorThrown);
    });
    return false;
  });

  $('#call-log').change(function () {
    if($(this).is(':checked')) {
      $('#call-log-info').show();
    } else {
      $('#call-log-info').hide();
    }
  });
});