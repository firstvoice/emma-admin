$(document).ready(function () {
    var plansTable = $('#plans-table').DataTable({
        'dom': 'Brtip',
        'paging': true,
        'pageLength': 200,
        'scrollCollapse': true,
        'processing': true,
        'serverSide': true,
        'deferRender': true,
        'ajax': {
            'url': 'process/get_plans.php',
            'type': 'GET',
            'data': {
                'emma-plan-id': user['emma_plan_id'],
            },
        },
        'columns': [
            {
                'data': 'name',
                'render': function (data, type, row, meta) {
                    return '<a href=\'dashboard.php?content=plan&id=' + row['emma_plan_id'] + '\'>' +
                        data + '</a>';
                },
            },
            {'data': 'date_active'},
            {'data': 'date_expired'},
            {'data': 'max_sites'},
        ],
    });

    $('#plans-table thead th.text-search').each(function (i) {
        let title = $('#plans-table thead th').eq($(this).index()).text();
        $(this)
            .html(
                '<input class="text-search" type="text" placeholder="Search ' + title +
                '" data-index="' + i + '" />');
    });

    $(plansTable.table().container())
        .on('keyup', 'thead input.text-search', function () {
            plansTable
                .column($(this).data('index'))
                .search(this.value)
                .draw();
        });

});