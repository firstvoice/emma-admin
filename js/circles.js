function drawCir() {
  if(drawingManager) {
    drawingManager.setMap(null);
  }
  drawingManager = null;
  drawingManager = new google.maps.drawing.DrawingManager();
  //Setting options for the Drawing Tool. In our case, enabling Polygon shape.
  drawingManager.setOptions({
    drawingMode : google.maps.drawing.OverlayType.CIRCLE,
    drawingControl : true,
    drawingControlOptions : {
        position : google.maps.ControlPosition.LEFT_CENTER,
        drawingModes : [ google.maps.drawing.OverlayType.CIRCLE ]
    },
    circleOptions : {
        strokeColor : '#6c6c6c',
        strokeWeight : 3.5,
        fillColor : '#926239',
        fillOpacity : 0.6,
        editable: true,
        draggable: true,
        clickable: true
    }   
  });
  google.maps.event.addListener(drawingManager, 'overlaycomplete', function (e) {
    if (e.type === google.maps.drawing.OverlayType.CIRCLE) {
      var circleOverlay = e.overlay;
      let circle = {
        centerLat: circleOverlay.center.lat(),
        centerLng: circleOverlay.center.lng(),
        radiusMeters: circleOverlay.radius
      }
      json.circles.push(circle);
      json.circleObjects.push(circleOverlay);
      let lastCircleIndex = json.circleObjects.length - 1;
      circleOverlay.addListener('bounds_changed', function() {
        // console.log('Bounds changed.');
        // console.log(circleOverlay.center.lat());
        // console.log(circleOverlay.center.lng());
        // console.log(circleOverlay.radius);
        updateCircleCoordinates(lastCircleIndex,circleOverlay.center.lat(),circleOverlay.center.lng(),circleOverlay.radius);
      });
      createInfoWindowCircle(lastCircleIndex, circleOverlay, map);
      drawingManager.setDrawingMode(null);
      drawingManager.setMap(null);
    }
  });
  // Loading the drawing Tool in the Map.
  drawingManager.setMap(map);
}



function createInfoWindowCircle(lastCircleIndex, circle, map){
  //create info window
  let infoWindow = new google.maps.InfoWindow({
    content: infoWindowContentCircle(circle.getCenter(), lastCircleIndex)
  });

  // Setting info window
  google.maps.event.addListener(circle, 'click', function(ev){
    infoWindow.setPosition(circle.getCenter());
    infoWindow.open(map);
  });

  json.infoWindowsCircles.push(infoWindow);
}

function removeItemCircle(index) {
  console.log(json.circleObjects)
  console.log(index)
  if(json.circleObjects[index]) {
    json.circleObjects[index].setMap(null);
    delete json.circleObjects[index];
    delete json.circles[index];
    // json.circleObjects.splice(index, 1);
  }

  if(json.infoWindowsCircles[index]){
    json.infoWindowsCircles[index].close();
    delete json.infoWindowsCircles[index];
    // json.infoWindowsCircles.splice(index, 1);
  }
  console.log(json.circleObjects)
}

function infoWindowContentCircle(center, index) {
  return `<button onclick="removeItemCircle(${index})">Delete</button>`;
}
function updateCircleCoordinates(index, lat, lng, radius) {
  json.circles[index].centerLat = lat;
  json.circles[index].centerLng = lng;
  json.circles[index].radiusMeters = radius;json.circles[index].centerLat = lat;
  // console.log(json);
}