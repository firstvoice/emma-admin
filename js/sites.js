$(document).ready(function () {
    var eventsTable = $('#sites-table').DataTable({
        "dom": 'Brtip',
        "paging": true,
        "pageLength": 10,
        "scrollCollapse": true,
        "processing": true,
        "serverSide": true,
        "deferRender": true,
        "order": [[0,'asc']],
        "ajax": {
            "url": "process/get_sites.php",
            "type": "GET",
            "data": {
                "emma-plan-id": user['emma_plan_id']
            }
        },
        "columns": [
            {
                "data": "emma_site_name",
                "render": function (data, type, row, meta) {
                    return "<a href='dashboard.php?content=site&id="  + row['emma_site_id'] + "'>" + row['emma_site_name'] + "</a>";
                }
            },
            {"data": "emergency_status",
                "render": function (data, type, row, meta) {
                    if(row['emergency_status'] === '1'){
                        return "<span style='color: Red;'>Active</span>";
                    }
                    else{
                        return "Inactive";
                    }
                }
            },
            {
                "data": "emergencies",
                "render": function (data, type, row, meta) {
                    return row['emergencies'];
                }
            },
            {
                "data": "drills",
                "render": function (data, type, row, meta) {
                    return row['drills'];
                }
            }
        ]
    });

  $('#sites-table thead th.text-search').each(function (i) {
    let title = $('#sites-table thead th').eq($(this).index()).text();
    $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
  });

  $(eventsTable.table().container()).on('keyup', 'thead input.text-search', function () {
    eventsTable
      .column($(this).data('index'))
      .search(this.value)
      .draw();
  });

});