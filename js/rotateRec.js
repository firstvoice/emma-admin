function createPolygonFromRectangle(rectangle) {
    var map = rectangle.getMap();
  
    var coords = [
      { lat: rectangle.getBounds().getNorthEast().lat(), lng: rectangle.getBounds().getNorthEast().lng() },
      { lat: rectangle.getBounds().getNorthEast().lat(), lng: rectangle.getBounds().getSouthWest().lng() },
      { lat: rectangle.getBounds().getSouthWest().lat(), lng: rectangle.getBounds().getSouthWest().lng() },
      { lat: rectangle.getBounds().getSouthWest().lat(), lng: rectangle.getBounds().getNorthEast().lng() }
    ];

    // Construct the polygon.
    var rectPoly = new google.maps.Polygon({
        path: coords,
        draggable: true,
        clickable: true
    });
    var properties = ["strokeColor","strokeOpacity","strokeWeight","fillOpacity","fillColor"];
    
    //inherit rectangle properties 
    var options = {};
    properties.forEach(function(property) {
        if (rectangle.hasOwnProperty(property)) {
            options[property] = rectangle[property];
        }
    });
    rectPoly.setOptions(options);

    rectangle.setMap(null);
    rectPoly.setMap(map);
    return rectPoly;
}

function rotatePolygon(polygon,angle) {
    var map = polygon.getMap();
    var prj = map.getProjection();
    var origin = prj.fromLatLngToPoint(polygon.getPath().getAt(0)); //rotate around first point

    var coords = polygon.getPath().getArray().map(function(latLng){
       var point = prj.fromLatLngToPoint(latLng);
       var rotatedLatLng =  prj.fromPointToLatLng(rotatePoint(point,origin,angle));
       return {lat: rotatedLatLng.lat(), lng: rotatedLatLng.lng()};
    });
    polygon.setPath(coords);
}

function rotatePoint(point, origin, angle) {
    var angleRad = angle * Math.PI / 180.0;
    return {
      x: Math.cos(angleRad) * (point.x - origin.x) - Math.sin(angleRad) * (point.y - origin.y) + origin.x,
      y: Math.sin(angleRad) * (point.x - origin.x) + Math.cos(angleRad) * (point.y - origin.y) + origin.y
    };
}

function drawDiagCircles(polygon, bounds, map) {

  // 0 - top right
  // 1 - top left
  // 2 - bottom left
  // 3 - bottom right
  let topRight = polygon.getPath().getAt(0);
  let botRight = polygon.getPath().getAt(3);
  let topLeft = polygon.getPath().getAt(1);
  let botLeft = polygon.getPath().getAt(2);

  let point1, point2, bearing, circleRadius, pctCircleRadius, circleDia, noOfCircles;

  if(bounds.horizontalmeters > bounds.verticalmeters) {
    point1 = getNewPoint(topRight, getBearing(topRight, botRight), getDistance(topRight, botRight)/2);
    point2 = getNewPoint(topLeft, getBearing(topLeft, botLeft), getDistance(topLeft, botLeft)/2);
    bearing = getBearing(point2, point1);

    circleRadius = bounds.verticalmeters/2;
    pctCircleRadius = bounds.verticalmeters * 0.706;
    circleDia = bounds.verticalmeters;
    noOfCircles = bounds.horizontalmeters/circleDia;

  } else {
    point1 = getNewPoint(topRight, getBearing(topRight, topLeft), getDistance(topRight, topLeft)/2);
    point2 = getNewPoint(botRight, getBearing(botRight, botLeft), getDistance(botRight, botLeft)/2);
    bearing = getBearing(point2, point1);

    circleRadius = bounds.horizontalmeters/2;
    pctCircleRadius = bounds.horizontalmeters * 0.706;
    circleDia = bounds.horizontalmeters;
    noOfCircles = bounds.verticalmeters/circleDia;
  }

  let centerPointC1 = getNewPoint(point2, bearing, circleRadius);
  
  // Creating main circles
  for(var i = 0; i < Math.ceil(noOfCircles); i++){
    let cityCircle = new google.maps.Circle({

      strokeColor : '#6c6c6c',
      strokeWeight : 3.5,
      fillColor : '#926239',
      fillOpacity : 0.6,
      editable: true,
      draggable: true,
      map: map,
      clickable: true,
      center: centerPointC1,
      radius: pctCircleRadius
    });

    // Adding circle to json
    let circle = {
      centerLat: centerPointC1.lat(),
      centerLng: centerPointC1.lng(),
      radiusMeters: pctCircleRadius
    }

    json.circles.push(circle);
    json.circleObjects.push(cityCircle);

    let centerPointNext = getNewPoint(centerPointC1, bearing, circleDia);
    centerPointC1 = centerPointNext;

    let lastCircleIndex = json.circleObjects.length - 1;
    createInfoWindowCircle(lastCircleIndex, cityCircle, map);
  }
}

function drawDiagonalRec() {
  if(drawingManager) {
    drawingManager.setMap(null);
  }
  drawingManager = null;
  drawingManager = new google.maps.drawing.DrawingManager();
  //Setting options for the Drawing Tool. In our case, enabling Polygon shape.
  drawingManager.setOptions({
    drawingMode : google.maps.drawing.OverlayType.RECTANGLE,
    drawingControl : true,
    drawingControlOptions : {
      position : google.maps.ControlPosition.LEFT_CENTER,
      drawingModes : [ google.maps.drawing.OverlayType.RECTANGLE ]
    },
    rectangleOptions : {
      strokeColor : '#6c6c6c',
      strokeWeight : 3.5,
      fillColor : '#926239',
      fillOpacity : 0.6,
      editable: true,
      draggable: true,
      clickable: true
    }   
  });
  google.maps.event.addListener(drawingManager, 'overlaycomplete', function (e) {
    if (e.type === google.maps.drawing.OverlayType.RECTANGLE) {
      console.log("2nd")
      drawingManager.setDrawingMode(null);
      drawingManager.setMap(null);

      if (e.type === google.maps.drawing.OverlayType.RECTANGLE) {

        // Switch back to non-drawing mode after drawing a shape.
        drawingManager.setDrawingMode(null);

        // Add an event listener that selects the newly-drawn shape when the user
        // mouses down on it.
        var newShape = e.overlay;

        let bounds = getBoundsSize(newShape.getBounds());


        let rectPoly = createPolygonFromRectangle(newShape); //create a polygom from a rectangle

        document.getElementById("rotateBtn").addEventListener('click', function(e) {
          if(rectPoly){
            rotatePolygon(rectPoly, 5);
          }
        });
        document.getElementById("rotateBtnAnt").addEventListener('click', function(e) {
          if(rectPoly){
            rotatePolygon(rectPoly, -5); 
          }
        });

        document.getElementById("dRecDone").addEventListener("click", function(){

          if (rectPoly) {
            // Switch back to non-drawing mode after drawing a shape.
            drawingManager.setMap(null);

            document.getElementById("tools").style.display = "flex";
            document.getElementById("gfInfo").style.display = "flex";
            document.getElementById("geoFence").style.display = "none"; 
            document.getElementById("rotateRecTool").style.display = "none"; 

            json.recObjects.push(rectPoly);
            let lastRecIndex = json.recObjects.length - 1;
            let infoWindowPosition = rectPoly.getPath().getAt(0);
            createInfoWindowRec(lastRecIndex, rectPoly, map, infoWindowPosition);
            drawDiagCircles(rectPoly, bounds, map)
          }
          
          rectPoly = null;

        });
      }
    }
  });
  // Loading the drawing Tool in the Map.
  drawingManager.setMap(map);
}