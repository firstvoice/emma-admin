$(document).ready(function () {
    var drillsTable = $('#drills-table').DataTable({
        "dom": 'Brtip',
        "paging": true,
        "pageLength": 20,
        "scrollCollapse": true,
        "processing": true,
        "serverSide": true,
        "deferRender": true,
        "order": [[2,'desc']],
        "ajax": {
            "url": "process/get_events.php",
            "type": "GET",
            "data": {
                "emma-plan-id": user['emma_plan_id'],
                "drill": 1
            }
        },
        "columns": [
            {
                "data": "type",
                "render": function (data, type, row, meta) {
                    return "<a href='dashboard.php?content=event&id=" + row['emergency_id'] + "'>" + data + "</a>";
                }
            },
            {
                "data": "username",
                    "render": function (data, type, row, meta) {
                        return "<a href='dashboard.php?content=user&id=" + row['userid'] + "'>" + row['userName'] + "</a> (<a href='mailto:'"+ row['useremail'] +">"+ row['useremail'] +"</a>)"
                    }
            },
            {
                "data": "active",
                "render": function (data, type, row, meta) {
                    return row['active'] === '1' ? "<span style='color:green'>Active</span>" : "<span style='color:red'>Closed</span>";
                }
            },
            {
                "data": "created_date",
                "render": function (data, type, row, meta) {
                    var date = new Date(data);
                    date.setHours(date.getHours() + parseInt(user['timezone']));
                    const displayDate = `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
                    return  displayDate ;
                }
            }
        ]
    });

  $('#drills-table thead th.text-search').each(function (i) {
    let title = $('#drills-table thead th').eq($(this).index()).text();
    $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
  });

  $(drillsTable.table().container()).on('keyup', 'thead input.text-search', function () {
    drillsTable
      .column($(this).data('index'))
      .search(this.value)
      .draw();
  });
});