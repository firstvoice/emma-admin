// function drawRec() {
//   if(drawingManager) {
//     drawingManager.setMap(null);
//   }
//   drawingManager = null;
//   drawingManager = new google.maps.drawing.DrawingManager();
//   //Setting options for the Drawing Tool. In our case, enabling Polygon shape.
//   drawingManager.setOptions({
//     drawingMode : google.maps.drawing.OverlayType.RECTANGLE,
//     drawingControl : true,
//     drawingControlOptions : {
//         position : google.maps.ControlPosition.LEFT_CENTER,
//         drawingModes : [ google.maps.drawing.OverlayType.RECTANGLE ]
//     },
//     rectangleOptions : {
//         strokeColor : '#6c6c6c',
//         strokeWeight : 3.5,
//         fillColor : '#926239',
//         fillOpacity : 0.6,
//         editable: true,
//         draggable: true
//     }
//   });
//   google.maps.event.addListener(drawingManager, 'overlaycomplete', function (e) {
//     if (e.type === google.maps.drawing.OverlayType.RECTANGLE) {
//       var newShape = e.overlay;
//       json.recObjects.push(newShape);

//       drawingManager.setDrawingMode(null);
//       drawingManager.setMap(null);
//     }
//   });
//   // Loading the drawing Tool in the Map.
//   drawingManager.setMap(map);
// }
function createInfoWindowRec(lastCircleIndex, Rec, map, infoWindowPosition){
  //create info window
  let infoWindow = new google.maps.InfoWindow({
    content: infoWindowContentRec("Rec.getCenter()", lastCircleIndex)
  });

  // Setting info window
  google.maps.event.addListener(Rec, 'click', function(ev){
    infoWindow.setPosition(infoWindowPosition);
    infoWindow.open(map);
  });

  json.infoWindowsRec.push(infoWindow);
}

function removeItemRec(index) {
  if(json.recObjects[index]) {
    json.recObjects[index].setMap(null);
    delete json.recObjects[index];
    // json.recObjects.splice(index, 1);
  }
  if(json.infoWindowsRec[index]) {
    json.infoWindowsRec[index].close();
    // json.infoWindowsRec.splice(index, 1);
    delete json.infoWindowsRec[index];
  }
}

function infoWindowContentRec(center, index) {
  return `<button onclick="removeItemRec(${index})">Delete</button>`;
}