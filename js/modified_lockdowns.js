$(document).ready(function () {
    var lockdownsTable = $('#modified-lockdowns-table').DataTable({
        "dom": 'Brtip',
        "paging": true,
        "pageLength": 20,
        "scrollCollapse": true,
        "processing": true,
        "serverSide": true,
        "deferRender": true,
        "order": [[0,'desc']],
        "ajax": {
            "url": "process/get_modified_lockdowns.php",
            "type": "GET",
            "data": {
                "emma-plan-id": user['emma_plan_id'],
            }
        },
        "columns": [
            {
                "data": "created_date",
                "render": function (data, type, row, meta) {
                    var date = new Date(data);
                    date.setHours(date.getHours() + parseInt(user['timezone']));
                    const displayDate = `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
                    return "<a href='dashboard.php?content=modified_lockdown&id=" + row['m_lockdown_id'] + "'>" + displayDate + "</a>";
                }
            },
            {
                "data": "username",
                "render": function (data, type, row, meta) {
                    return "<a href='dashboard.php?content=user&id=" + row['userid'] + "'>" + row['useremail'] + "</a> (<a href='mailto:'"+ row['useremail'] +">"+ row['useremail'] +"</a>)"
                }
            },
        ]
    });

    $('#modified-lockdowns-table thead th.text-search').each(function (i) {
        let title = $('#modified-lockdowns-table thead th').eq($(this).index()).text();
        $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
    });

    $(lockdownsTable.table().container()).on('keyup', 'thead input.text-search', function () {
        lockdownsTable
            .column($(this).data('index'))
            .search(this.value)
            .draw();
    });

});