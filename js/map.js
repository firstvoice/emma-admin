let map;
close_aed_array = [];//Close_aed_array is going to be filled with matching AEDS based on distance, but isn't the final array.
aed_markers_array = [];//array of AEDs (markers) we are displaying on the map, we keep track so we can delete them later to update.
function populateAEDS(pos) {
  //positions is an array of all AED's, queried on user_home.php.
  for (let i = 0, len = positions.length; i < len; i++) {
    //Calculate the distance from the AED points vs our user centerPoint.
    if(arePointsNear(positions[i],pos,40074.274944)) //1.60934 km = 0.99999 miles, 4.82803 km is 3 miles.
    {
      //Create a new object (closePos) and add in the newly calculated distance.
      closePos = {lat: positions[i]['lat'], lng: positions[i]['lng'], location: positions[i]['location'], distance: last_distance, coordinator: positions[i].coordinator, contact: positions[i].contact, company: positions[i].company, address: positions[i].address};
      //push onto an array.
      close_aed_array.push(closePos);
      //last distance used.
      last_distance = 0;
    }
  }

  //This is the array of 10 AED's we will show user on map.
  aeds_being_added = [];


  //sort our AEDs by distance and get the first 10 (the closest).
  close_aed_array.sort((a,b) => (a.distance > b.distance) ? 1 : -1);
  inc_count = 0; //How many we have added to the array.
  for(let i = 0, len = close_aed_array.length; i < len; i++)
  {
    //Check for duplicates.
    const found = aeds_being_added.findIndex(aeds_being_added => aeds_being_added.distance === close_aed_array[i].distance);
    if(found == -1)
    {
      //Not a duplicate, go ahead and add.
      aeds_being_added.push(close_aed_array[i]);
      inc_count++;
      if(inc_count >= 10)
      {
        break;
      }
    }
  }



  //For each object in aeds_being_added, we add a pin.
  for(let i = 0, len = aeds_being_added.length; i < len; i++)
  {
    add_pin(aeds_being_added[i],"aed");
  }
  close_aed_array = []; //empty this array.


}//Pass in users current lat/long as an object (pos)
//Place active events that are happening.
function populateCalls()
{
  for (let i = 0, len = police_calls.length; i<len; i++)
  {
    add_pin(police_calls[i],"call");
  }
}
last_distance = 0;//Calculate if point is within x km of centerPoint.
function arePointsNear(checkPoint, centerPoint, km) {
  var ky = 40000 / 360;
  var kx = Math.cos(Math.PI * centerPoint.lat / 180.0) * ky;
  var dx = Math.abs(centerPoint.lng - checkPoint.lng) * kx;
  var dy = Math.abs(centerPoint.lat - checkPoint.lat) * ky;
  if(Math.sqrt(dx * dx + dy * dy) <= km)
  {
    //console.log(dx * dx + dy * dy);
    last_distance = dx*dx+dy*dy;
    return true;
  }
  return false;
  //return Math.sqrt(dx * dx + dy * dy) <= km;
}
function add_pin(object,type)
{
  if(type === "aed") {
    let marker = new google.maps.Marker({
      position: new google.maps.LatLng(object.lat, object.lng),
      map: map,
      title: object.name
    });
    aed_markers_array.push(marker);
    google.maps.event.addListener(marker, 'click', function() {
      let contact = '<p style="line-height: 20%;"></p>';
      let coordinator = '<p style="line-height: 20%;"></p>';
      let location = '<p style="line-height: 20%;"></p>';
      let company = '<p style="line-height: 20%;"></p>';
      let address = '<p style="line-height: 20%;"></p>';
      if(object.coordinator == null || object.coordinator === 'unknown' || object.coordinator === 'undefined' || object.coordinator === '' || object.coordinator === 'Unknown')
      {
        //nothing
      }
      else{
        coordinator = '<p style="line-height: 20%;">' + object.coordinator + '</p>';
      }
      if(object.location == null || object.location === '' || object.location === '*' || object.location === 'N/A' || object.location === 'unknown' || object.location === 'Unknown')
      {
        //nothing
      }
      else{
        location = '<p style="line-height: 20%;">' + object.location + '</p>';
      }
      //convert km to miles
      object.distance = (object.distance*0.62137).toFixed(2);
      tmp_dist = '<i style="color: red;">'+object.distance+' mi</i>';
      distance_text = '<p style="line-height: 20%;">Distance: '+tmp_dist+'</p>';
      if(object.contact == null || object.contact === 'unknown' || object.contact === 'undefined' || object.contact === '' || object.contact === 'Unknown')
      {
        //nothing
      }
      else{
        contact = '<p style="line-height: 20%;">Contact: ' + object.contact + '</p>';
      }
      if(object.company == null || object.company === 'unknown' || object.company === 'undefined' || object.company === '' || object.company === 'Unknown')
      {
        //nothing
      }
      else{
        company = '<p style="line-height: 20%;">'+object.company+'</p>'
      }
      if(object.address == null || object.address === 'unknown' || object.address === 'undefined' || object.address === '' || object.address === 'Unknown')
      {
        //nothing
      }
      else{
        address = '<p style="line-height: 20%;">'+object.address+'</p>';
      }
      let html = '<p style="line-height: 20%;"></p>' +
        company+
        coordinator +
        location +
        distance_text +
        contact +
        address +
        '';
      let infowindow = new google.maps.InfoWindow({
        content: html
      });
      last_located_address = ''; //set to empty
      infowindow.open(map, marker);
      map.setZoom(18);
      map.setCenter(marker.getPosition());
    });
    marker.setMap(map);
  }
  else if(type === "asset")
  {
    let icon = 'img/orange-dot.png';
    if(object.image !== null && object.image !== '' && object.image !== 'img/orange-dot.png')
    {
      icon = {
        url: object.image,
        scaledSize: new google.maps.Size(25, 25)
      }
    }
    let marker = new google.maps.Marker({
      position: new google.maps.LatLng(object.lat, object.lng),
      icon: icon,
      map: map,
      title: name
    });


    google.maps.event.addListener(marker, 'click', function() {
      let text = '';
      let address = '';
      if(object.name === null || object.name === '' || object.name === 'unknown' || object.name === 'N/A' || object.name === 'undefined')
      {
        text = '' + '<h6>' + 'Unknown Type' + '</h6>' + '';
      }
      else{
        text = '' + '<h6>' + object.name + '</h6>' + '';
      }
      if(object.address === null || object.address === '' || object.address === 'unknown' || object.address === 'N/A' || object.address === 'undefined')
      {
        //nothing
      }
      else{
        address = '<p>'+object.address+'</p>';
      }
      html = '' +
        text +
        address +
        '';
      let infowindow = new google.maps.InfoWindow({
        content: html
      });
      infowindow.open(map, marker);
      map.setZoom(18);
      map.setCenter(marker.getPosition());
    });
    marker.setMap(map);
  }
  else if(type === "event")
  {
    let icon = {
      url: 'img/' + object.img,
      scaledSize: new google.maps.Size(35,35),
    };
    let marker = new google.maps.Marker({
      position: new google.maps.LatLng(object.lat, object.lng),
      icon: icon,
      map: map,
      title: name
    });
    google.maps.event.addListener(marker, 'click', function() {
      let text = '';
      if(object.type != null && object.type != ' ' && object.type != 'unknown' && object.type != 'Unknown' && object.type != 'undefined')
      {
        text = '' + '<h6>' + object.type + '</h6>' + '';
      }
      else{
        text = '' + '<h6>' + 'Unknown Type' + '</h6>' + '';
      }
      if(object.notes != null && object.notes != ' ')
      {
        text+='<br><h3>'+object.notes+'</h3>';
      }

      let infowindow = new google.maps.InfoWindow({
        content: text
      });
      infowindow.open(map, marker);
      map.setZoom(18);
      map.setCenter(marker.getPosition());
    });
    marker.setMap(map);
  }
  else if(type === "call")
  {
    let icon = {
      url: 'img/help.png',
      scaledSize: new google.maps.Size(35,35),
    };
    let marker = new google.maps.Marker({
      position: new google.maps.LatLng(object.lat, object.lng),
      icon: icon,
      map: map,
      title: name
    });
    google.maps.event.addListener(marker, 'click', function() {
      let latlng = '<p style="line-height: 20%;">Lat/Lng: '+object.lat+ " " + object.lng + '</p>';
      let name = '<p style="line-height: 20%;">Called by: ' + object.username + '</p>';
      let date = '<p style="line-height: 20%;">Call date: ' + object.calldate + '</p>';
      let html = '<p style="line-height: 20%"></p>' +
        name +
        date +
        latlng +
        '';

      let infowindow = new google.maps.InfoWindow({
        content: html
      });
      infowindow.open(map, marker);
      map.setZoom(18);
      map.setCenter(marker.getPosition());
    });
    marker.setMap(map);
  }
  else{

  }

}
function placeAssets()
{
  for(let i = 0, len = assets.length; i < len; i++)
  {
    add_pin(assets[i],"asset");
  }
}

function addNearestAedMarkers(curLat, curLng) {
  $.ajax({
    dataType: 'json',
    url: 'process/get_user_aeds.php',
    type: 'POST',
    data: {
      'jwt': getCookie('jwt'),
      lat: curLat,
      lng: curLng
    }
  })
    .done(function(data){
      console.log(data);
      let bounds = new google.maps.LatLngBounds();
      let infowindow = new google.maps.InfoWindow({
        content: 'AEDs'
      });
      console.log(data['aeds']);
      for(var i in data['aeds']) {
        console.log(data['aeds'][i]);
        let marker = new google.maps.Marker({
          position: {
            lat: parseFloat(data['aeds'][i]['latitude']),
            lng: parseFloat(data['aeds'][i]['longitude'])
          },
          map: map,
          organization: data['aeds'][i]['organization_name'],
          location: data['aeds'][i]['location_name'],
          placement: data['aeds'][i]['placement'],
          address: data['aeds'][i]['address'],
          notes: data['aeds'][i]['notes'],
          serialnumber: data['aeds'][i]['serialnumber'],
          lastcheck: data['aeds'][i]['lastcheck'],
          lat: data['aeds'][i]['latitude'],
          lng: data['aeds'][i]['longitude']
        });
        google.maps.event.addListener(marker, 'click', function () {
          var html = '' +
            '<h4 style="color:#2f557a;">' + this.organization + ' / ' + this.location + '</h4>' +
            '<h5 style="color:#7597b0;">' + this.address + '</h5>' +
            '<hr>' +
            '<p><b>Coordinates: </b>(' + this.lat + ', ' + this.lng + ')</p>' +
            '<p><b>Placement: </b>' + this.placement + '</p>' +
            '<p><b>Serial: </b>' + this.serialnumber + '</p>' +
            '<p><b>Last Check: </b>' + this.lastcheck + '</p>' +
            '<p><b>Notes: </b>' + this.notes + '</p>'
            '';
          /*
          for (var k = 0; k < this.aeds.length; k++) {
            html += '<div style="margin-top:.5rem;font-size:1.2rem;" class="aed_popup_link" onclick="aedpopupinfo(' + this.aeds[k]['aed_id'] + ')">' + this.aeds[k]['brand'] + ' ' + this.aeds[k]['model'] + ' (' + this.aeds[k]['accessibility'] + ', ' + this.aeds[k]['mobility'] + ')</div>';
            html += '<b>Placement: </b>' + this.aeds[k]['placement'] + ' (' +  this.aeds[k]['latitude'] + ', ' + this.aeds[k]['longitude'] + ')';
            if(this.aeds[k]['coordinator'] != '') html += '<br /><b>Contact: </b>' + this.aeds[k]['coordinator'] + ' (555-123-4567)';

          }
           */
          infowindow.setContent(html);
          infowindow.open(map, this);
        });
        bounds.extend(marker.position);
        map.fitBounds(bounds);
      }
    });
}

function initMap() {
  //console.log(document.cookie);
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      let curLat = position.coords.latitude;
      let curLng = position.coords.longitude;
      map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: {
          lat: curLat,
          lng: curLng
        }
      })
      addNearestAedMarkers(curLat, curLng);
    });
  } else {
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 2,
      center: {
        lat: 0,
        lng: 0
      }
    })
  }
  /*
      var infowindow = new google.maps.InfoWindow({
          content: 'AEDs'
      });

      let icon = {
          url: 'img/' + mainEvent['type_filename'],
          scaledSize: new google.maps.Size(35,35),
      };

      let marker = new google.maps.Marker({
          position: event,
          icon: icon,
          map: map,
      });
      var updateInterval = 5000;
      var responseIds = [];
      var markers = [];

      var bounds = new google.maps.LatLngBounds();


      populateAEDS(event);
      populateCalls();
      placeAssets();


      function getResponses() {
          $.ajax({
              dataType: 'json',
              url: 'process/get_responses.php',
              type: 'GET',
              data: {'id': mainEvent['emergency_id'], 'order': 'emergency_response_id'}
          })
              .done(function (data) {
                  if (data.success) {
                      for (var i in data['responses']) {
                          var status = 'Unknown';
                          var iconSrc = 'img/pending.png';
                          switch (data['responses'][i]['status']) {
                              case '-1':
                                  status = 'Pending';
                                  iconSrc = 'img/pending.png';
                                  break;
                              case '0':
                                  status = 'All Clear';
                                  iconSrc = 'img/all_clear.png';
                                  break;
                              case '1':
                                  status = 'Need Help';
                                  iconSrc = 'img/help.png';
                                  break;
                              case '2':
                                  status = 'Need Information';
                                  iconSrc = 'img/need_info.png';
                                  break;
                          }
                          var idx = responseIds.findIndex(function (response) {
                              return response.user === data['responses'][i]['user_id'];
                          });
                          if (idx === -1) {
                              var marker = new google.maps.Marker({
                                  position: {
                                      lat: parseFloat(data['responses'][i]['latitude']),
                                      lng: parseFloat(data['responses'][i]['longitude'])
                                  },
                                  icon: iconSrc,
                                  map: map,
                                  user: data['responses'][i]['user'],
                                  status: data['responses'][i]['status_name'],
                                  comments: data['responses'][i]['comments']
                              });
                              google.maps.event.addListener(marker, 'click', function () {
                                  var html = '' +
                                      '<h6>' + this.user + '</h6>' +
                                      '<p>' + this.comments + '</p>' +
                                      '';
                                  infowindow.setContent(html);
                                  infowindow.open(map, this);
                              });
                              responseIds.push({
                                  'user': data['responses'][i]['user_id'],
                                  'status': data['responses'][i]['status'],
                                  'marker': marker
                              });
                              bounds.extend(marker.position);
                              map.fitBounds(bounds);
                          } else {
                              if (data['responses'][i]['status'] !== responseIds[idx]['status']) {
                                  responseIds[idx]['status'] = data['responses'][i]['status'];
                                  responseIds[idx]['marker'].setIcon(iconSrc);
                                  responseIds[idx]['marker'].setPosition(new google.maps.LatLng(
                                      data['responses'][i]['latitude'],
                                      data['responses'][i]['longitude']
                                  ));
                                  responseIds[idx]['marker'].comments = data['responses'][i]['comments'];
                                  bounds.extend(responseIds[idx]['marker'].position);
                                  map.fitBounds(bounds);
                              }
                          }
                      }
                      if (data['subevents']) {
                          var subevents = [];
                          var icon = 'img/event.png';
                          for (var i in data['subevents']) {


                              subevents[i] =
                                  {
                                      position: new google.maps.LatLng(data['subevents'][i]['latitude'], data['subevents'][i]['longitude'])
                                  }


                          }
                          for (var i = 0; i < subevents.length; i++) {
                              var icon = {
                                  url: 'img/' + data['subevents'][i]['img_filename'],
                                  scaledSize: new google.maps.Size(35,35),
                              };
                              var marker = new google.maps.Marker({
                                  position: subevents[i].position,
                                  icon: icon,
                                  map: map
                              });
                          }
                      }
                  } else {

                  }
              })
              .fail(function (data, textStatus, errorThrown) {
                  console.log(data);
                  console.log(textStatus);
                  console.log(errorThrown);
              });
      }
      getResponses();

      setInterval(getResponses, updateInterval);
   */
}