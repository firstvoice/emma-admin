$(document).ready(function () {
    var guestCodesTable = $('#guest_codes-table').DataTable({
        'dom': 'Brtip',
        'paging': true,
        'pageLength': 200,
        'scrollCollapse': true,
        'processing': true,
        'serverSide': true,
        'deferRender': true,
        'ajax': {
            'url': 'process/get_guest_codes.php',
            'type': 'GET',
            'data': {
                'emma-plan-id': user['emma_plan_id'],
            },
        },
        'columns': [
            {
                'data': 'guest_code_id',
                'render': function (data, type, row, meta) {
                    return '<a href=\'dashboard.php?content=guest_code&id=' + row['guest_code_id'] + '\'>' +
                        data + '</a>';
                },
            },
            {'data': 'dateSpan'},
            {'data': 'durationHours'},
            {'data': 'group'},
            {'data': 'status'},
        ],
    });

    $('#guest_codes-table thead th.text-search').each(function (i) {
        let title = $('#guest_codes-table thead th').eq($(this).index()).text();
        $(this)
            .html(
                '<input class="text-search" type="text" placeholder="Search ' + title +
                '" data-index="' + i + '" />');
    });

    $(guestCodesTable.table().container())
        .on('keyup', 'thead input.text-search', function () {
            guestCodesTable
                .column($(this).data('index'))
                .search(this.value)
                .draw();
        });
    
});