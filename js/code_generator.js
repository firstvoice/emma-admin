$(document).ready(function () {
  $('#generate-code').click(function(e) {
    e.preventDefault();

    $.ajax({
      dataType: 'json',
      url: 'process/generate_code.php',
      type: 'POST',
      data: {},
      beforeSend: function () {
        $('#thinking-modal').foundation('open');
      }
    })
      .done(function (data) {
        console.log(data);
        $('#error-list').empty();
        $('#thinking-modal').foundation('close');
        if (data.success) {
          $('#code').val(data.code);
        } else {
          for (var error in data['errors']) {
            $('#error-list').append(data['errors'][error] + '<br />');
          }
          $('#fail-modal').foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        $('#thinking-modal').foundation('close');
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
  });

  $('#save-code-form').submit(function(e) {
    e.preventDefault();

    $.ajax({
      dataType: 'json',
      url: 'process/save_code.php',
      type: 'POST',
      data: $(this).serialize(),
      beforeSend: function () {
        $('#thinking-modal').foundation('open');
      }
    })
      .done(function (data) {
        console.log(data);
        $('#error-list').empty();
        $('#thinking-modal').foundation('close');
        if (data.success) {
          $('#success_modal').foundation('open');
        } else {
          for (var error in data['errors']) {
            $('#error-list').append(data['errors'][error] + '<br />');
          }
          $('#fail-modal').foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        $('#thinking-modal').foundation('close');
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
    return false;
  });

    $('#start-date').change(function () {
        var date = $(this).val();
        console.log(date);
        var dateParts = date.split('-');
        var y = parseInt(dateParts[0], 10);
        var m = parseInt(dateParts[1], 10);
        var d = parseInt(dateParts[2], 10);
        var firstDate = new Date(y, m-1, d+1);
        var datePlusOne = addDays(firstDate, 0);
        var finalDate = formatDate(datePlusOne);
        console.log(finalDate);
        $('#end-date').attr('min', finalDate);
    })

});

function addDays(date, amount) {
    var tzOff = date.getTimezoneOffset() * 60 * 1000,
        t = date.getTime(),
        d = new Date(),
        tzOff2;

    t += (1000 * 60 * 60 * 24) * amount;
    d.setTime(t);

    tzOff2 = d.getTimezoneOffset() * 60 * 1000;
    if (tzOff != tzOff2) {
        var diff = tzOff2 - tzOff;
        t += diff;
        d.setTime(t);
    }

    return d;
}

function formatDate(date) {
    var day = date.getDate();
    var monthIndex = date.getMonth()+1;
    var year = date.getFullYear();

    return year + '-' + n(monthIndex) + '-' + n(day);
}

function n(n){
    return n > 9 ? "" + n: "0" + n;
}