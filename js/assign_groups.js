$(document).ready(function () {
  let usersTable = $('#users-table').DataTable({
    'dom': 'Brtip',
    'paging': true,
    'pageLength': 25,
    'scrollCollapse': true,
    'processing': true,
    'serverSide': true,
    'deferRender': true,
    'ajax': {
      'url': 'process/get_users.php',
      'type': 'GET',
      'data': {
        'emma-plan-id': user['emma_plan_id'],
      },
    },
    'columns': [
      {
        'data': ' ',
        'render': function (data, type, row, meta) {
          return '<div class=\'text-center\'><input type=\'checkbox\' name=\'selected_users[]\' value=\'' + row['id'] +
            '\' /></div>';
        },
      },
      {
        'data': 'username',
        'render': function (data, type, row, meta) {
          return '<a href=\'dashboard.php?content=user&id=' + row['id'] + '\'>' + data + '</a>';
        },
      },
      {'data': 'firstname'},
      {'data': 'lastname'},
      {'data': 'group'},
    ],
  });

  $('#user_assign_group').click(function () {
    $('#update_group_modal').foundation('open');
  });

  $('#assign_group_submit').click(function () {
    $.ajax({
      dataType: 'json',
      url: 'process/assign_user_list_to_group.php',
      type: 'POST',
      data: $('#users_form').serialize() + '&' + $('#assign_group_form').serialize(),
      beforeSend: function () {
        $('#thinking-modal').foundation('open');
      },
    }).done(function (data) {
      console.log(data);
      $('#thinking-modal').foundation('close');
      const errorList = $('#error-list');
      errorList.empty();
      if (data.success) {
        $('#success_modal').foundation('open');
      } else {
        for (let error of data['errors']) {
          errorList.append(error + '<br />');
        }
        $('#fail-modal').foundation('open');
      }
    }).fail(function (data, textStatus, errorThrown) {
      $('#thinking-modal').foundation('close');
      console.log(data);
      console.log(textStatus);
      console.log(errorThrown);
    });
  });

});