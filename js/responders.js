$(document).ready(function () {
  console.log(user);
  var respondersTable = $('#responders-table').DataTable({
    "dom": 'Brtip',
    "paging": true,
    "pageLength": 20,
    "scrollCollapse": true,
    "processing": true,
    "serverSide": true,
    "deferRender": true,
    "order": [[0,'asc']],
    "ajax": {
      "url": "process/get_responders.php",
      "type": "GET",
      "data": {
        "plan-id": user['emma_plan_id']
      }
    },
    "columns": [
      {
        "data": "org",
        "render": function (data, type, row, meta) {
          return "<a href='dashboard.php?content=responder&id=" + row['id'] + "'>" + data + "</a>";
        }
      },
      {
        "data": "location",
        "render": function (data, type, row, meta) {
          return "<a href='dashboard.php?content=responder&id=" + row['id'] + "'>" + data + "</a>";
        }
      },
      {
        "data": "name",
        "render": function (data, type, row, meta) {
          return "<a href='dashboard.php?content=responder&id=" + row['id'] + "'>" + data + "</a>";
        }
      },
      {
        "data": "class",
        "render": function (data, type, row, meta) {
          return "<a href='dashboard.php?content=responder&id=" + row['id'] + "'>" + data + "</a>";
        }
      },
      {
        "data": "display",
        "render": function (data, type, row, meta) {
          return data === 'yes' ?
            "<span style='color: green'>Active</span>" :
            "<span style='color: red'>Inactive</span>";
        }
      }
    ]
  });


  $('#responders-table thead th.text-search').each(function (i) {
    let title = $('#responders-table thead th').eq($(this).index()).text();
    $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
  });

  $(respondersTable.table().container()).on('keyup', 'thead input.text-search', function () {
    respondersTable
      .column($(this).data('index'))
      .search(this.value)
      .draw();
  });
});