$(document).ready(function () {
    var guestUsersTable = $('#guest-users-table').DataTable({
        'dom': 'Brtip',
        'paging': true,
        'pageLength': 200,
        'scrollCollapse': true,
        'processing': true,
        'serverSide': true,
        'deferRender': true,
        "order": [[0,'asc']],
        'ajax': {
            'url': 'process/get_guest_codes_multi.php',
            'type': 'GET',
            'data': {
                'emma-plan-id': user['emma_plan_id'],
            },
        },
        'columns': [
            {
                'data': 'group',
            },
            {'data': 'start_date',
                'orderable': true},
            {'data': 'end_date',
                'orderable': true},
            {
                'data': 'duration_minutes',
                'orderable': true
            },
            {
                'render': function (data, type, row, meta) {
                    if(row['emp_active'] == 1)
                    {
                        return '<p style="color: green;">Active</p>';
                    }
                    else{
                        return '<p style="color: red;">Inactive</p>';
                    }
                },
            },
        ],
    });

    $('#guest-users-table thead th.text-search').each(function (i) {
        let title = $('#guest-users-table thead th').eq($(this).index()).text();
        $(this)
            .html(
                '<input class="text-search" type="text" placeholder="Search ' + title +
                '" data-index="' + i + '" />');
    });

    $(guestUsersTable.table().container())
        .on('keyup', 'thead input.text-search', function () {
            guestUsersTable
                .column($(this).data('index'))
                .search(this.value)
                .draw();
        });

});