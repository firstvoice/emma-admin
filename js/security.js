let map;
let bounds;
let updateInterval = 5000;
let infowindow;
let mCreation = moment(mainSecurity['created_date']);
let incidents = new vis.DataSet();
let responseIds = [];
let interval = 60;
let timeline = new vis.Timeline(document.getElementById('timeline'), incidents,
  {
    minHeight: '200px',
    start: mCreation - 1000 * 60 * interval / 2,
    end: mCreation + 1000 * 60 * interval / 2,
    order: function (a, b) {
      return a.id - b.id;
    },
    format: {
      minorLabels: {
        millisecond: 'SSS',
        second: 's',
        minute: 'h:mm a',
        hour: 'h:mm a',
        weekday: 'ddd D',
        day: 'D',
        week: 'w',
        month: 'MMM',
        year: 'YYYY',
      },
      majorLabels: {
        millisecond: 'HH:mm:ss',
        second: 'D MMMM HH:mm',
        minute: 'ddd D MMMM',
        hour: 'ddd D MMMM',
        weekday: 'MMMM YYYY',
        day: 'MMMM YYYY',
        week: 'MMMM YYYY',
        month: 'YYYY',
        year: '',
      },
    },
  });

/* Sub Events */
const subEvents = [];
const badgeFire = $('#badge-fire');
const tableFire = $('#fire-sub-events');
const badgeLockDown = $('#badge-lock-down');
const tableLockDown = $('#lock-down-sub-events');
const badgeShelter = $('#badge-shelter');
const tableShelter = $('#shelter-sub-events');
const badgeActiveShooter = $('#badge-active-shooter');
const tableActiveShooter = $('#active-shooter-sub-events');
const badgeExplosion = $('#badge-explosion');
const tableExplosion = $('#explosion-sub-events');
const badgeHazmat = $('#badge-hazmat');
const tableHazmat = $('#hazmat-sub-events');
const badgeNaturalDisaster = $('#badge-natural-disaster');
const tableNaturalDisaster = $('#natural-disaster-sub-events');
const badgeSevereWeather = $('#badge-severe-weather');
const tableSevereWeather = $('#severe-weather-sub-events');
const badgeMedical = $('#badge-medical');
const tableMedical = $('#medical-sub-events');
const badgeOther = $('#badge-other');
const tableOther = $('#other-sub-events');

function getTimeline() {
  $.ajax({
    dataType: 'json',
    url: 'process/get_security_timeline.php',
    type: 'POST',
    data: {'id': mainSecurity['emma_security_id'], 'order': 'updated_date', 'received': incidents.getIds().join(',')},
  }).done(function (data) {
    console.log(data);
    if (data.success) {
      for (let incident of data['incidents']) {
        if (!incidents.get(incident['id'])) {
          incidents.add([
            {
              id: incident['id'],
              start: new Date(incident['updated_date']).getTime(),
              content: '',
              title: `
                <b>Time:</b> ` + moment(incident['updated_date']).format('M/D/YYYY h:mm:ss a') + `<br/>
                <b>User:</b> ` + incident['user'] + `<br/>
                <b>Comments</b> ` + incident['message'],
              type: 'point',
              className: incident['status_class'],
            },
          ]);
          timeline.moveTo(incident['updated_date']);

          let nextMessage = `
            <div data-closable class="callout alert-callout-subtle radius inset-shadow ` + incident['status_class'] + `">
              <strong>
                <a href="dashboard.php?content=user&id=` + incident['user_id'] + `">` + incident['user'] + `</a> - 
                ` + moment(incident['updated_date']).format('M/D/YYYY h:mm:ss a') + `
              </strong>
              ` + (incident['message'] === '' ? `` : `<div><b>System Message: </b><p>` + incident['message'] + `</p></div>`) + `
            </div>
          `;
          $('#message-center').append(nextMessage);
          let messageCenterDiv = document.getElementById('message-center');
          messageCenterDiv.scrollTop = messageCenterDiv.scrollHeight;
        }
      }
    }
  }).fail(function (data, textStatus, errorThrown) {
    console.log(data);
    console.log(textStatus);
    console.log(errorThrown);
  });
}

function initMap() {
  if(mainSecurity['latitude'] !== "" && mainSecurity['longitude'] !== "") {
    let security = {
      lat: parseFloat(mainSecurity['latitude']),
      lng: parseFloat(mainSecurity['longitude']),
    };
    map = new google.maps.Map(document.getElementById('security-map'), {
      zoom: 15,
      center: security,
    });

    infowindow = new google.maps.InfoWindow({
      content: 'AEDs',
    });

    let marker = new google.maps.Marker({
      position: security,
      icon: 'img/event.png',
      map: map,
    });
    bounds = new google.maps.LatLngBounds();
    bounds.extend(security);
  } else {
    let security = {
      lat: 0,
      lng: 0,
    };
    map = new google.maps.Map(document.getElementById('security-map'), {
      zoom: 2,
      center: security,
    });

    infowindow = new google.maps.InfoWindow({
      content: 'AEDs',
    });
  }
}

$(document).ready(function () {
  console.log(mainSecurity);
  getTimeline();
  setInterval(getTimeline, updateInterval);

    $('.select-close-script-group-container').hide();
    $('.select-close-script-container').hide();

    $('.select-notify-creator-script-group-container').hide();
    $('.select-notify-creator-script-container').hide();

  $('#notify-creator-form').submit(function (e) {
    e.preventDefault();
    $.ajax({
      dataType: 'json',
      url: 'process/notify_creator.php',
      type: 'POST',
      data: $('#notify-creator-form').serialize(),
      beforeSend: function () {
        $('#notify-creator-form').find('input[type="submit"]').attr('disabled', true);
        THINKINGMODAL.foundation('open');
      },
    }).done(function (data) {
      // console.log(data);
      if (data.success) {
        location.reload();
      } else {
        FAILMODAL.foundation('open');
      }
      $('#notify-creator-modal').foundation('close');
    }).fail(function (data, textStatus, errorThrown) {
      console.log(data);
      console.log(textStatus);
      console.log(errorThrown);
    });
    return false;
  });

  $('#close-security').submit(function () {
    $.ajax({
      dataType: 'json',
      url: 'process/close_security.php',
      type: 'POST',
      data: $('#close-security').serialize(),
      beforeSend: function () {
        $('#close-security').find('input[type="submit"]').attr('disabled', true);
        THINKINGMODAL.foundation('open');
      },
    }).done(function (data) {
      console.log(data);
      if (data.success) {
        window.location = 'dashboard.php?content=securities';
      } else {
        FAILMODAL.foundation('open');
      }
    }).fail(function (data, textStatus, errorThrown) {
      console.log(data);
      console.log(textStatus);
      console.log(errorThrown);
    });
    return false;
  });

    $('#select-close-broadcast-type').change(function () {
        if($('.select-close-script-group-container').is(":hidden")){
            $('.select-close-script-group-container').show();
        }else {
            $('#select-close-script-group').change();
        }
    });

    $('#select-close-script-group').change(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/get_group_scripts.php',
            type: 'POST',
            data: {
                'group-id': $('#select-close-script-group option:selected').val(),
                'plan-id': user['emma_plan_id'],
                'type': $('#select-close-broadcast-type option:selected').val(),
                'script-type': '9'
            },
            beforeSend: function () {
                $('.select-close-script-container').show();
                $('#select-close-script').empty();
            },
        })
            .done(function (data) {
                console.log(data);
                if (data.success) {
                    $('#select-close-script').append('<option value="" data-text="">-Select-</option>');
                    for (var i in data['groups']) {
                        $('#select-close-script').append('<option value="'+ data['groups'][i]['emma_script_id'] +'" data-text="'+ data['groups'][i]['text'] +'">'+ data['groups'][i]['name'] +'</option>');
                    }
                }
            })
            .fail(function (data, textStatus, errorThrown) {

            });
        return false;
    });



    $('#select-notify-creator-broadcast-type').change(function () {
        if($('.select-notify-creator-script-group-container').is(":hidden")){
            $('.select-notify-creator-script-group-container').show();
        }else {
            $('#select-notify-creator-script-group').change();
        }
    });

    $('#select-notify-creator-script-group').change(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/get_group_scripts.php',
            type: 'POST',
            data: {
                'group-id': $('#select-notify-creator-script-group option:selected').val(),
                'plan-id': user['emma_plan_id'],
                'type': $('#select-notify-creator-broadcast-type option:selected').val(),
                'script-type': '8'
            },
            beforeSend: function () {
                $('.select-notify-creator-script-container').show();
                $('#select-notify-creator-script').empty();
            },
        })
            .done(function (data) {
                console.log(data);
                if (data.success) {
                    $('#select-notify-creator-script').append('<option value="" data-text="">-Select-</option>');
                    for (var i in data['groups']) {
                        $('#select-notify-creator-script').append('<option value="'+ data['groups'][i]['emma_script_id'] +'" data-text="'+ data['groups'][i]['text'] +'">'+ data['groups'][i]['name'] +'</option>');
                    }
                }
            })
            .fail(function (data, textStatus, errorThrown) {

            });
        return false;
    });





  $('#select-notify-creator-script').change(function () {
    $('#notify-creator-description').text($('#select-notify-creator-script option:selected').data('text'));
  });

  $('#select-close-script').change(function () {
    $('#close-description').text($('#select-close-script option:selected').data('text'));
  });

  $('#call-log').change(function () {
    if($(this).is(':checked')) {
      $('#call-log-info').show();
    } else {
      $('#call-log-info').hide();
    }
  });

  $('#emma-call-add-button').click(function () {
    $('#emma-call-body').append(`
      <tr>
        <td>
          <select name="person-type[]">
            <option value="Staff">Staff</option>
            <option value="Faculty">Faculty</option>
            <option value="Student">Student</option>
            <option value="Other">Other</option>
          </select>
         </td>
        <td><input name="person-name[]" type="text"></td>
        <td><input name="person-id[]" type="text"></td>
        <td><input name="person-org[]" type="text"></td>
      </tr>
    `);
  });
});