
$(document).ready(function () {
    circle_html = '';
    rectangle_first_html = '';
    rectangle_second_html = '';

    console.log('Ready!');

    if($('#saved_center_lat').val() == null)
    {
        console.log('Ready!');

        circle_html = '<div class="large-12 medium-12 small-12 column"><label class="text-center"><b>Center Coordinates</b></label><div class="row"><div class="large-6 medium-6 small-6 column"><label>Latitude<input type="text" name="center-lat" value=""> </label> </div> <div class="large-6 medium-6 small-6 column"> <label>Longitude <input type="text" name="center-lng" value=""> </label> </div> </div> <div class="row"> <div class="large-3 column" style="margin: 0 auto;"> <label>Radius (Miles) <input type="Text" name="radius" value=""/></label> </div> </div> </div>';
    }
    else {
        console.log('Ready!');

        circle_html = '<div class="large-12 medium-12 small-12 column"><label class="text-center"><b>Center Coordinates</b></label><div class="row"><div class="large-6 medium-6 small-6 column"><label>Latitude<input type="text" name="center-lat" value="' + $('#saved_center_lat').val() + '"> </label> </div> <div class="large-6 medium-6 small-6 column"> <label>Longitude <input type="text" name="center-lng" value="' + $('#saved_center_lng').val() + '"> </label> </div> </div> <div class="row"> <div class="large-3 column" style="margin: 0 auto;"> <label>Radius (Miles) <input type="Text" name="radius" value="' + $('#saved_radius').val() + '"/> </label> </div> </div> </div>';
    }
    if($('#saved_nw_lat').val() == null)
    {
        console.log('Ready!');

        rectangle_first_html = '<div class="large-12 medium-12 small-12 column"><label class="text-center"><b>NW Corner Coordinates</b></label> <div class="row"> <div class="large-6 medium-6 small-6 column"> <label>Latitude <input type="text" name="nw-lat" value=""></label></div><div class="large-6 medium-6 small-6 column"> <label>Longitude <input type="text" name="nw-lng" value=""></label></div></div></div>';
        rectangle_second_html = '<div class="large-12 medium-12 small-12 column"><label class="text-center"><b>SW Corner Coordinates</b></label> <div class="row"> <div class="large-6 medium-6 small-6 column"> <label>Latitude <input type="text" name="sw-lat" value=""></label></div><div class="large-6 medium-6 small-6 column"> <label>Longitude <input type="text" name="sw-lng" value=""></label></div></div></div>';
    }
    else {
        console.log('Ready!');

        rectangle_first_html = '<div class="large-12 medium-12 small-12 column"><label class="text-center"><b>NW Corner Coordinates</b></label> <div class="row"> <div class="large-6 medium-6 small-6 column"> <label>Latitude <input type="text" name="nw-lat" value="' + $('#saved_nw_lat').val() + '"></label></div><div class="large-6 medium-6 small-6 column"> <label>Longitude <input type="text" name="nw-lng" value="' + $('#saved_nw_lng').val() + '"> </label> </div> </div> </div>';
        rectangle_second_html = '<div class="large-12 medium-12 small-12 column"><label class="text-center"><b>SE Corner Coordinates</b></label> <div class="row"> <div class="large-6 medium-6 small-6 column"> <label>Latitude <input type="text" name="se-lat" value="' + $('#saved_se_lat').val() + '"></label></div><div class="large-6 medium-6 small-6 column"> <label>Longitude <input type="text" name="se-lng" value="' + $('#saved_se_lng').val() + '"></label></div></div></div>';
    }



    console.log("ZIT");
    if($('#dropdown_type option:selected').text() == 'Circle')
    {
        //Append circle options
        console.log("HIT");
        $('#circle_append_here').append(circle_html);
    }
    else if($('#dropdown_type option:selected').text() == 'Rectangle')
    {
        //Append rectangle options
        $('#rectangle_first_append_here').append(rectangle_first_html);
        $('#rectangle_second_append_here').append(rectangle_second_html);
    }




    $('#edit_geofence_form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/edit_geofence.php',
            type: 'POST',
            data: $('#edit_geofence_form').serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            }
        })
            .done(function (data) {
                console.log(data);
                $('#thinking-modal').foundation('close');
                $('#error-list').empty();
                if (data.success) {
                    $('#success_modal').foundation('open');
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });


    });
    $('#dropdown_type').change(function(){
        if($('#dropdown_type').val() == 'Circle')
        {
//Append circle information.
            $('#rectangle_first_append_here').empty();//But first clear the append area
            $('#rectangle_second_append_here').empty();//But first clear the append area
            $('#circle_append_here').append(circle_html);
        }
        else if($('#dropdown_type').val() == 'Rectangle'){
            //Append rectangle information.
            $('#circle_append_here').empty();//But first clear the append area
            $('#rectangle_first_append_here').append(rectangle_first_html);
            $('#rectangle_second_append_here').append(rectangle_second_html);
        }
    });

});