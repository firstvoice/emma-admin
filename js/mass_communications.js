$(document).ready(function () {
  let massCommunications = $('#mass-communications-table').DataTable({
    "dom": 'Brtip',
    "paging": true,
    "pageLength": 20,
    "scrollCollapse": true,
    "processing": true,
    "serverSide": true,
    "deferRender": true,
    "order": [[0,'desc']],
    "ajax": {
      "url": "process/get_mass_communications.php",
      "type": "GET",
      "data": {
        "emma-plan-id": user['emma_plan_id'],
      }
    },
    "columns": [
      {
        "data": "created_date",
        "render": function (data, type, row, meta) {
          var date = new Date(data);
          date.setHours(date.getHours() + parseInt(user['timezone']));
          const displayDate = `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
          return "<a href='dashboard.php?content=mass_communication&id=" + row['emma_mass_communication_id'] + "'>" + displayDate + "</a>";
        }
      },
      {
          "data": "creator",
          "render": function (data, type, row, meta) {
              return "<a href='dashboard.php?content=user&id=" + row['created_by_id'] + "'>" + data + "</a> (<a href='mailto:'"+ row['useremail'] +">"+ row['useremail'] +"</a>)";
          }
      },
      {"data": "notification"},
    ]
  });

  $('#mass-communications-table thead th.text-search').each(function (i) {
    let title = $('#mass-communications-table thead th').eq($(this).index()).text();
    $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
  });

  $(massCommunications.table().container()).on('keyup', 'thead input.text-search', function () {
    massCommunications
      .column($(this).data('index'))
      .search(this.value)
      .draw();
  });


  // $('#create-mass-communication-form').submit(function (e) {
  //   e.preventDefault();
  //   let form = $(this);
  //   $.ajax({
  //     dataType: 'json',
  //     url: 'process/create_mass_communication.php',
  //     type: 'POST',
  //     data: form.serialize(),
  //     beforeSend: function () {
  //       $('#thinking-modal').foundation('open');
  //     }
  //   })
  //     .done(function (data) {
  //       console.log(data);
  //       $('#thinking-modal').foundation('close');
  //       if (data.success) {
  //         location.reload();
  //         //TODO: SUCCESS MESSAGE
  //       } else {
  //         //TODO: FAILED MESSAGE
  //       }
  //       //form.foundation('close');
  //     })
  //     .fail(function (data, textStatus, errorThrown) {
  //       console.log(data);
  //       console.log(textStatus);
  //       console.log(errorThrown);
  //     });
  //   return false;
  // });
});