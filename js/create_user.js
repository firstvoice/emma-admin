$(document).ready(function () {

    $('#create_user_form').submit(function (e) {
        e.preventDefault();

        $.ajax({
            dataType: 'json',
            url: 'process/create_new_user.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            }
        })
            .done(function (data) {
                console.log(data);
                $('#error-list').empty();
                $('#thinking-modal').foundation('close');
                if (data.success) {
                  $('#success_modal').foundation('open');
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
    });

    $('#all-select').click(function () {
        $('.group-toggle').click();
    })

    $('#username').on('input',function (e) {
        $('#exists-container').empty();
        e.preventDefault();
        var username = $(this).val()
        var adminuser = $('#user_id').val();
        if(validateEmail(username)) {
            $.ajax({
                dataType: 'json',
                url: 'process/get_existing_users.php',
                type: 'POST',
                data: {'username': username,
                        'user': adminuser
                },
                beforeSend: function () {

                }
            })
                .done(function (data) {
                    console.log(data);
                    if (data['exists'] === true) {
                        $('#exists-container').append(`<b style="color: red; margin: 0 auto">User already exists. </b>`);
                    }
                    if (data['deleted'] === true){
                        $('#reactivate-user-id').val(data['user']['id']);
                        $('#exists-container').append(`<span>Would you like to reactivate this user? </span><a class="button" data-user-id="`+ data['user']['id'] +`" data-open="reactivate-user-modal">Reactivate</a>`)
                    }
                    if (data['move'] === true){
                        $('#add-user-id').val(data['user']['id']);
                        $('#add-user-username').val(data['user']['username']);
                        $('#add-plan-id').val($('#plan_id').val());
                        $('#exists-container').append(`<span>Would you like to add this user to your plan? </span><a class="button" data-user-id="`+ data['user']['id'] +`" data-open="add-user-modal">Add</a>`)
                    }
                })
        }
    });

    $('#reactivate-user-form').submit(function (e) {
        e.preventDefault();

        $.ajax({
            dataType: 'json',
            url: 'process/reactivate_user.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            }
        })
            .done(function (data) {
                console.log(data);
                $('#error-list').empty();
                $('#thinking-modal').foundation('close');
                if (data.success) {
                    $('#success_modal').foundation('open');
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
    });

    $('#add-user-form').submit(function (e) {
        e.preventDefault();

        $.ajax({
            dataType: 'json',
            url: 'process/add_user_plan.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            }
        })
            .done(function (data) {
                console.log(data);
                $('#error-list').empty();
                $('#thinking-modal').foundation('close');
                if (data.success) {
                    $('#success_modal').foundation('open');
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
    });

});

function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}