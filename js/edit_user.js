$(document).ready(function () {

    $('#edit_user_form').submit(function (e) {
        e.preventDefault();
        var planarr = [];
        $('.plan-selects').each(function(){
            if($(this).prop('checked')){
                planarr.push($(this).val());
                planarr.push($(this).data('name'));
            }

        });
        console.log(planarr.length);
        if(planarr.length > 2 && $('#main-plan').val() === '') {
            $('#main-plan-container').empty();
            for (var plan = 0; plan < planarr.length; plan++) {
                if (plan % 2 === 0) {
                    var checked;
                    var namecount = plan + 1
                    console.log("id = " + planarr[plan]);
                    console.log("name = " + planarr[namecount]);
                    if(planarr[namecount] === $('#main-plan-name').val()){
                        checked = 'checked';
                    }else{
                        checked = '';
                    }
                    $('#main-plan-container').append(`
                    <div class="large-4 medium-6 small-6 columns align-self-middle">
                        <div class="row"> 
                            <div class="small-2 column">
                                <input type="radio" class="main-plans" name="main-plan" value="` + planarr[plan] + `" `+ checked +`>
                            </div>
                            <div class="small-10 column">
                              <label class="text-left">` + planarr[namecount] + `</label>
                            </div>
                        </div>                           
                    </div> 
                    `);
                }
            }
            $('#select_main_plan_modal').foundation('open');
        }else{
            if($('#main-plan').val() === ''){
                $('#main-plan').val(planarr[0])
            }
            e.preventDefault();
            $.ajax({
                dataType: 'json',
                url: 'process/edit_user.php',
                type: 'POST',
                data: $('#edit_user_form').serialize(),
                beforeSend: function () {
                    $('#thinking-modal').foundation('open');
                }
            })
                .done(function (data) {
                    console.log(data);
                    $('#thinking-modal').foundation('close');
                    $('#error-list').empty();
                    if (data.success) {
                        $('#success_modal').foundation('open');
                    } else {
                        for (var error in data['errors']) {
                            $('#error-list').append(data['errors'][error] + '<br />');
                        }
                        $('#fail-modal').foundation('open');
                    }
                })
                .fail(function (data, textStatus, errorThrown) {
                    $('#thinking-modal').foundation('close');
                    console.log(data);
                    console.log(textStatus);
                    console.log(errorThrown);
                });
        }
    });

    $('#submit-main-plan').click(function () {
        var radios = $('.main-plans');
        console.log(radios);
        for (var i = 0, length = radios.length; i < length; i++) {
            if (radios[i].checked) {
                $('#main-plan').val(radios[i].value);
                break;
            }
        }
        if($('#main-plan').val() !== ''){
            $('#edit_user_form').submit();
        }else{
            alert('Please select a plan.');
        }
    })
});