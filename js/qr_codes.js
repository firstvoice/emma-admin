$(document).ready(function () {

    $.ajax({
        dataType: 'json',
        url: 'process/get_qrcodes.php',
        type: 'POST',
        data: {
            'user': user['id'],
        },
        beforeSend: function () {
            $('#qr-container').empty();
        }
    })
        .done(function (data) {
            console.log(data);
            if (data.success) {
                $('#qr-contaier').empty();
                for (var i in data['qrcodes']) {
                    var code = data['qrcodes'][i]['qr_code'];
                    $('#qr-container').append('<div class="large-2 medium-4 small-6 columns qrblock"><row><a data-plan="'+data['qrcodes'][i]['plan_id']+'" data-group="'+data['qrcodes'][i]['group_id']+'" class="qrcodelink" id="qrcodelink' + i + '"><div id="qrcode' + i + '"></div></a></row><row><label>'+data['qrcodes'][i]['planname']+'</label></row><row><label>'+data['qrcodes'][i]['groupname']+'</label></row></div>')
                    jQuery('#qrcode' + i).qrcode({width: 100,height: 100,text:code});
                }
            }
            $('.qrcodelink').click(function () {
                var group = $(this).data('group');
                var plan = $(this).data('plan');
                $('.download-plan').val(plan);
                $('.download-group').val(group);
                $('#download-qr-modal').foundation('open');
            })
        })
        .fail(function (data, textStatus, errorThrown) {
            console.log(data);
            console.log(textStatus);
            console.log(errorThrown);
        });


    $('#qr-plan').change(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/get_plan_groups.php',
            type: 'POST',
            data: {
                'plan-id': $(this).val(),
            },
            beforeSend: function () {
                $('#qr-group').empty();
            }
        })
            .done(function (data) {
                console.log(data);
                $('#qr-group').append('<option value="">-Select-</option>');
                if (data.success) {
                    for (var i in data['groups']) {
                        $('#qr-group').append('<option value="' + data['groups'][i]['emma_group_id'] + '">' + data['groups'][i]['name'] + '</option>');
                    }
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
    })

    $('#create-qr-form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/create_new_qr.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            },
        })
            .done(function (data) {
                console.log(data);
                $('#thinking-modal').foundation('close');
                $('#error-list').empty();
                if (data.success) {
                    $('#success-modal').foundation('open');
                } else {
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
    });


});