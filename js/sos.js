$(document).ready(function () {


  let sosAllTable = $('#sos-all-table').DataTable({
    "dom": 'rtip',
    "paging": true,
    "pageLength": 20,
    "scrollCollapse": true,
    "processing": true,
    "serverSide": true,
    "deferRender": true,
    "order": [[0,'desc']],
    "columnDefs": [
      { targets: 'no-sort', orderable: false }
    ],
    "ajax": {
      "url": "process/get_all_sos.php",
      "type": "GET",
      "data": {
        "emma-plan-id": user['emma_plan_id'],
      }
    },
    "columns": [
      {
        "data": "created_date",
        "render": function (data, type, row, meta) {
          var date = new Date(data);
          date.setHours(date.getHours() + parseInt(user['timezone']));
          const displayDate = `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
          return "<a href='dashboard.php?content=sos_details&id=" + row['emma_sos_id'] + "'>" + displayDate + "</a>";
        }
      },
      {
        "data": "username",
        "render": function (data, type, row, meta) {
          return "<a href='dashboard.php?content=user&id=" + row['user_id'] + "'>" + row['user'] + "</a> (<a href='mailto:'"+ row['email'] +">"+ row['email'] +"</a>)";
        }
      },
      {
        "data": "phone"
      },
      {
        "data": "landline_phone"
      },
      {
        "data": "status"
      }

    ]
  });

  $('#sos-all-table thead th.text-search').each(function (i) {
    let title = $('#sos-all-table thead th').eq($(this).index()).text();
    $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
  });

  $(sosAllTable.table().container()).on('keyup', 'thead input.text-search', function () {
    sosAllTable
      .column($(this).data('index'))
      .search(this.value)
      .draw();
  });

  let sosHelpTable = $('#sos-help-table').DataTable({
    "dom": 'rtip',
    "paging": true,
    "pageLength": 20,
    "scrollCollapse": true,
    "processing": true,
    "serverSide": true,
    "deferRender": true,
      "order": [[0,'desc']],
    "ajax": {
      "url": "process/get_active_sos.php",
      "type": "GET",
      "data": {
        "emma-plan-id": user['emma_plan_id'],
      }
    },
    "columns": [
      {
        "data": "help_date",
        "render": function (data, type, row, meta) {
          var date = new Date(data);
          date.setHours(date.getHours() + parseInt(user['timezone']));
          const displayDate = `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
          return "<a href='dashboard.php?content=sos_details&id=" + row['emma_sos_id'] + "'>" + displayDate + "</a>";
        }
      },
      {
        "data": "username",
        "render": function (data, type, row, meta) {
          return "<a href='dashboard.php?content=user&id=" + row['user_id'] + "'>" + row['user'] + "</a> (<a href='mailto:'"+ row['email'] +">"+ row['email'] +"</a>)";
        }
      },
      {
        "data": "phone"
      },
      {
        "data": "landline_phone"
      },
      {
        "data": "latitude"
      },
      {
        "data": "longitude"
      }
    ]
  });

  $('#sos-help-table thead th.text-search').each(function (i) {
    let title = $('#sos-help-table thead th').eq($(this).index()).text();
    $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
  });

  $(sosHelpTable.table().container()).on('keyup', 'thead input.text-search', function () {
    sosHelpTable
      .column($(this).data('index'))
      .search(this.value)
      .draw();
  });

  let sosPendingTable = $('#sos-pending-table').DataTable({
    "dom": 'rtip',
    "paging": true,
    "pageLength": 20,
    "scrollCollapse": true,
    "processing": true,
    "serverSide": true,
    "deferRender": true,
      "order": [[0,'desc']],
    "ajax": {
      "url": "process/get_pending_sos.php",
      "type": "GET",
      "data": {
        "emma-plan-id": user['emma_plan_id'],
      }
    },
    "columns": [
      {
        "data": "pending_date",
        "render": function (data, type, row, meta) {
          var date = new Date(data);
          date.setHours(date.getHours() + parseInt(user['timezone']));
          const displayDate = `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
          return "<a href='dashboard.php?content=sos_details&id=" + row['emma_sos_id'] + "'>" + displayDate + "</a>";
        }
      },
      {
        "data": "username",
        "render": function (data, type, row, meta) {
          return "<a href='dashboard.php?content=user&id=" + row['user_id'] + "'>" + row['user'] + "</a> (<a href='mailto:'"+ row['email'] +">"+ row['email'] +"</a>)";
        }
      },
      {
        "data": "phone"
      },
      {
        "data": "landline_phone"
      },
      {
        "data": "latitude",
      },
      {
        "data": "longitude"
      },
    ]
  });

  $('#sos-pending-table thead th.text-search').each(function (i) {
    let title = $('#sos-pending-table thead th').eq($(this).index()).text();
    $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
  });

  $(sosPendingTable.table().container()).on('keyup', 'thead input.text-search', function () {
    sosPendingTable
      .column($(this).data('index'))
      .search(this.value)
      .draw();
  });

  let sosCancelledTable = $('#sos-cancelled-table').DataTable({
    "dom": 'rtip',
    "paging": true,
    "pageLength": 20,
    "scrollCollapse": true,
    "processing": true,
    "serverSide": true,
    "deferRender": true,
      "order": [[0,'desc']],
    "ajax": {
      "url": "process/get_cancelled_sos.php",
      "type": "GET",
      "data": {
        "emma-plan-id": user['emma_plan_id'],
      }
    },
    "columns": [
      {
        "data": "cancelled_date",
        "render": function (data, type, row, meta) {
          var date = new Date(data);
          date.setHours(date.getHours() + parseInt(user['timezone']));
          const displayDate = `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
          return "<a href='dashboard.php?content=sos_details&id=" + row['emma_sos_id'] + "'>" + displayDate + "</a>";
        }
      },
      {
        "data": "username",
        "render": function (data, type, row, meta) {
          return "<a href='dashboard.php?content=user&id=" + row['user_id'] + "'>" + row['user'] + "</a> (<a href='mailto:'"+ row['email'] +">"+ row['email'] +"</a>)";
        }
      },
      {
        "data": "phone"
      },
      {
        "data": "latitude",
      },
      {
        "data": "longitude"
      },
    ]
  });

  $('#sos-cancelled-table thead th.text-search').each(function (i) {
    let title = $('#sos-closed-table thead th').eq($(this).index()).text();
    $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
  });

  $(sosCancelledTable.table().container()).on('keyup', 'thead input.text-search', function () {
    sosCancelledTable
      .column($(this).data('index'))
      .search(this.value)
      .draw();
  });

  let sosClosedTable = $('#sos-closed-table').DataTable({
    "dom": 'rtip',
    "paging": true,
    "pageLength": 20,
    "scrollCollapse": true,
    "processing": true,
    "serverSide": true,
    "deferRender": true,
      "order": [[0,'desc']],
    "ajax": {
      "url": "process/get_closed_sos.php",
      "type": "GET",
      "data": {
        "emma-plan-id": user['emma_plan_id'],
      }
    },
    "columns": [
      {
        "data": "closed_date",
        "render": function (data, type, row, meta) {
          var date = new Date(data);
          date.setHours(date.getHours() + parseInt(user['timezone']));
          const displayDate = `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
          return "<a href='dashboard.php?content=sos_details&id=" + row['emma_sos_id'] + "'>" + displayDate + "</a>";
        }
      },
      {
        "data": "username",
        "render": function (data, type, row, meta) {
          return "<a href='dashboard.php?content=user&id=" + row['user_id'] + "'>" + row['user'] + "</a> (<a href='mailto:'"+ row['email'] +">"+ row['email'] +"</a>)";
        }
      },
      {
        "data": "phone"
      },
      {
        "data": "latitude",
      },
      {
        "data": "longitude"
      },
      {
        "data": "closed_comments"
      },
      {
        "data": "closed_call_report",
        "render": function (data, type, row, meta) {
          return row['closed_call_report'] !== '0'
            ? "<a target='_blank' href='process/create_closed_sos_report.php?id="+row['closed_call_report']+"'><i class='large fa fa-file-text'></i></a>"
            : "";
        }
      }
    ]
  });

  $('#sos-closed-table thead th.text-search').each(function (i) {
    let title = $('#sos-closed-table thead th').eq($(this).index()).text();
    $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
  });

  $(sosClosedTable.table().container()).on('keyup', 'thead input.text-search', function () {
    sosClosedTable
      .column($(this).data('index'))
      .search(this.value)
      .draw();
  });

});