


//original code from J.Baker git-old/emma-user/js/home.js
// ===================================================================
var map;
var bounds;
var updateInterval = 5000;
var infowindow;
var responseIds = [];
var incidents = new vis.DataSet();
var API_KEY = window.GoogleSamples.Config.gcmAPIKey;
var GCM_ENDPOINT = 'https://android.googleapis.com/gcm/send';

var curlCommandDiv = document.querySelector('.js-curl-command');
var isPushEnabled = false;


// This method handles the removal of subscriptionId
// in Chrome 44 by concatenating the subscription Id
// to the subscription endpoint
function endpointWorkaround(pushSubscription) {
    // Make sure we only mess with GCM
    if (pushSubscription.endpoint.indexOf('https://android.googleapis.com/gcm/send') !== 0) {
        return pushSubscription.endpoint;
    }

    var mergedEndpoint = pushSubscription.endpoint;
    // Chrome 42 + 43 will not have the subscriptionId attached
    // to the endpoint.
    if (pushSubscription.subscriptionId &&
        pushSubscription.endpoint.indexOf(pushSubscription.subscriptionId) === -1) {
        // Handle version 42 where you have separate subId and Endpoint
        mergedEndpoint = pushSubscription.endpoint + '/' +
            pushSubscription.subscriptionId;
    }
    return mergedEndpoint;
}

function sendSubscriptionToServer(subscription) {
    // TODO: Send the subscription.endpoint
    // to your server and save it to send a
    // push message at a later date
    //
    // For compatibly of Chrome 43, get the endpoint via
    // endpointWorkaround(subscription)
    // console.log('TODO: Implement sendSubscriptionToServer()');

    var mergedEndpoint = endpointWorkaround(subscription);

    // This is just for demo purposes / an easy to test by
    // generating the appropriate cURL command
    if (mergedEndpoint.indexOf(GCM_ENDPOINT) !== 0) {
        console.log('This browser isn\'t currently supported for this demo');
        //window.Demo.debug.log('This browser isn\'t currently supported for this demo');
        return;
    }

    var endpointSections = mergedEndpoint.split('/');
    var subscriptionId = endpointSections[endpointSections.length - 1];

    $.ajax({
        dataType: 'json',
        url: 'process/add_client_id.php',
        type: 'POST',
        data: {'subscription': subscriptionId}
    })
        .done(function (data) {
            //console.log(data);
            //console.log('user subscription updated');
            if (data.success) {
                //TODO: SUCCESS MESSAGE
            } else {
                //TODO: FAILED MESSAGE
            }
            //form.foundation('close');
        })
        .fail(function (data, textStatus, errorThrown) {
            console.log(data);
            console.log(textStatus);
            console.log(errorThrown);
        });

    // showCurlCommand(mergedEndpoint);
}
/*
// NOTE: This code is only suitable for GCM endpoints,
// When another browser has a working version, alter
// this to send a PUSH request directly to the endpoint
function showCurlCommand(mergedEndpoint) {
    // The curl command to trigger a push message straight from GCM
    if (mergedEndpoint.indexOf(GCM_ENDPOINT) !== 0) {
        console.log('This browser isn\'t currently supported for this demo');
        //window.Demo.debug.log('This browser isn\'t currently supported for this demo');
        return;
    }

    var endpointSections = mergedEndpoint.split('/');
    var subscriptionId = endpointSections[endpointSections.length - 1];

    var curlCommand = 'curl --header "Authorization: key=' + API_KEY +
        '" --header Content-Type:"application/json" ' + GCM_ENDPOINT +
        ' -d "{\\"registration_ids\\":[\\"' + subscriptionId + '\\"]}"';

    curlCommandDiv.textContent = curlCommand;
}

function unsubscribe() {
    curlCommandDiv.textContent = '';

    navigator.serviceWorker.ready.then(function (serviceWorkerRegistration) {
        serviceWorkerRegistration.pushManager.getSubscription().then(
            function (pushSubscription) {
                if (!pushSubscription) {
                    return;
                }
                pushSubscription.unsubscribe().then(function () {
                    //Finished
                }).catch(function (e) {
                    console.log('Unsubscription error: ', e);
                    //window.Demo.debug.log('Unsubscription error: ', e);
                });
            }).catch(function (e) {
                console.log('Error thrown while unsubscribing from push messaging.', e);
            //window.Demo.debug.log('Error thrown while unsubscribing from push messaging.', e);
        });
    });
}

function subscribe() {
    navigator.serviceWorker.ready.then(function (serviceWorkerRegistration) {
        serviceWorkerRegistration.pushManager.subscribe({userVisibleOnly: true})
            .then(function (subscription) {
                return sendSubscriptionToServer(subscription);
            })
            .catch(function (e) {
                console.log(e);
                if (Notification.permission === 'denied') {
                    console.log('Permission for Notifications was denied');
                    //window.Demo.debug.log('Permission for Notifications was denied');
                } else {
                    console.log('Unable to subscribe to push.', e);
                    //window.Demo.debug.log('Unable to subscribe to push.', e);
                }
            });
    }).catch(function (e) {
        console.log(e);
    });
}

// Once the service worker is registered set the initial state
function initialiseState() {
    if (!('showNotification' in ServiceWorkerRegistration.prototype)) {
        console.log('Notifications aren\'t supported.');
        //window.Demo.debug.log('Notifications aren\'t supported.');
        return;
    }
    if (Notification.permission === 'denied') {
        console.log('The user has blocked notifications.');
        //window.Demo.debug.log('The user has blocked notifications.');
        return;
    }
    if (!('PushManager' in window)) {
        console.log('Push messaging isn\'t supported.');
        //window.Demo.debug.log('Push messaging isn\'t supported.');
        return;
    }

    // We need the service worker registration to check for a subscription
    navigator.serviceWorker.ready.then(function (serviceWorkerRegistration) {
        // Do we already have a push message subscription?
        serviceWorkerRegistration.pushManager.getSubscription()
            .then(function (subscription) {
                //$('#subscribe-notifications').disable();

                if (!subscription) {
                    return;
                }

                sendSubscriptionToServer(subscription);

                //$('#unsubscribe-notifications').enable();
            })
            .catch(function (err) {
                console.log('Error during getSubscription()', err);
                //window.Demo.debug.log('Error during getSubscription()', err);
            });
    });
}
window.addEventListener('load', function() {
    var pushButton = document.querySelector('.js-push-button');
    pushButton.addEventListener('click', function() {
        if (isPushEnabled) {
            unsubscribe();
        } else {
            subscribe();
        }
    });

    // Check that service workers are supported, if so, progressively
    // enhance and add push messaging support, otherwise continue without it.
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('./service-worker.js')
            .then(initialiseState);
    } else {
        window.Demo.debug.log('Service workers aren\'t supported in this browser.');
    }
});
*/

/*
function registerServiceWorker() {
    return navigator.serviceWorker.register('service-worker.js')
        .then(function(registration){
            console.log('Service worker successfully registered.');
            return registration;
        })
        .catch(function(err) {
            console.error('Unable to register service worker.', err);
        })
}

function askPermission() {
    return new Promise(function(resolve, regect) {
        const permissionResult = Notification.requestPermission(function(result) {
            resolve(result);
        });
        if(permissionResult) {
            permissionResult.then(resolve, reject);
        }
    })
        .then(function(permissionResult) {
            if(permissionResult !== 'granted') {
                throw new Error('We were\'t granted permission.');
            }
        })
}

function subscribeUserToPush() {
    return navigator.serviceWorker.register('service-worker.js')
        .then(function(registration) {
            const subscribeOptions = {
                userVisibleOnly: true,
                applicationServerKey: urlBase64ToUint8Array('BFB8vPGxC6UFlVVFBdb6rYZjYUJgpQK4Kpb4NeccxGq-FnwZxMppPrYQy8lGDhQ7KZsTUZCw5EXPMVqdr3NyBhA')
            };
            return registration.pushManager.subscribe(subscribeOptions);
        })
        .then(function(pushSubscription) {
            console.log('Received PushSubscription: ', JSON.stringify(pushSubscription));
            return pushSubscription;
        })
}
*/

function getTimeline() {
    $.ajax({
        dataType: 'json',
        url: 'process/get_timeline.php',
        type: 'GET',
        data: {'id': mainEvent['emergency_id'], 'user': user['id']},
        beforeSend: function(request) {
            request.setRequestHeader('Authorization', 'Bearer ' + getJWT());
        },
    }).done(function(data) {
        if (data.success) {
            for (var i in data['incidents']) {
                var status = 'Unknown';
                var divClass = 'secondary';
                var border = '#000000';
                var color = '#3e739a';
                switch (data['incidents'][i]['status']) {
                    case '-2':
                        status = 'Alert';
                        divClass = 'system';
                        border = '#767676';
                        color = '#eaeaea';
                        break;
                    case '-1':
                        status = 'Pending';
                        divClass = 'secondary';
                        border = '#767676';
                        color = '#eaeaea';
                        break;
                    case '0':
                        status = 'All Clear';
                        divClass = 'success';
                        border = '#3adb76';
                        color = '#e1faea';
                        break;
                    case '1':
                        status = 'Need Help';
                        divClass = 'alert';
                        border = '#cc4b37';
                        color = '#f7e4e1';
                        break;
                    case '2':
                        status = 'Need Information';
                        divClass = 'warning';
                        border = '#ffae00';
                        color = '#fff3d9';
                        break;
                }
                if (incidents.get(data['incidents'][i]['id']) === null) {
                    incidents.add([
                        {
                            id: data['incidents'][i]['id'],
                            start: new Date(data['incidents'][i]['updated_date']).getTime(),
                            content: '',
                            title: '<b>Time:</b> ' +
                                moment(data['incidents'][i]['updated_date']).
                                format('M/D/YYYY h:mm:ss a') + '<br/><b>User:</b> ' +
                                data['incidents'][i]['user'] + '<br/><b>Status:</b> ' + status +
                                '<br/><b>Comments</b> ' + data['incidents'][i]['message'],
                            type: 'point',
                            className: divClass,
                        },
                    ]);
                    // timeline.focus(data['responses'][i]['emergency_response_id']);
                    // timeline.moveTo(data['incidents'][i]['updated_date']);
                    $('#message-center').append('' +
                        '<div data-closable class=\'callout alert-callout-subtle radius inset-shadow ' +
                        divClass + '\'>' +
                        '<strong>' + data['incidents'][i]['user'] + ' - ' +
                        moment(data['incidents'][i]['updated_date']).
                        format('M/D/YYYY h:mm:ss a') + '</strong>' +
                        '<p>' + data['incidents'][i]['message'] + '</p>' +
                        '</div>');
                    var messageCenterDiv = document.getElementById('message-center');
                    messageCenterDiv.scrollTop = messageCenterDiv.scrollHeight;
                    if (data['incidents'][i]['message'] === 'Event Closed') {
                        location.reload();
                    }
                }
            }
        } else {

        }
    }).fail(function(data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
    });
}

function getResponses() {
    $.ajax({
        dataType: 'json',
        url: 'process/get_responses.php',
        type: 'GET',
        data: {'id': mainEvent['emergency_id'], 'user': user['id']},
        beforeSend: function() {
            //$('#total-not-received span').empty().append($('#not-received-users-modal table tbody tr').length + " Users");
        },
    }).done(function(data) {
        if (data.success) {
            for (var i in data['responses']) {
                var status = 'Unknown';
                var divClass = 'secondary';
                var iconSrc = 'img/pending.png';
                var table = '';
                switch (data['responses'][i]['status']) {
                    case '-1':
                        status = 'Pending';
                        divClass = 'secondary';
                        iconSrc = data['responses'][i]['mobile'] === '1' ?
                            'img/grey_mobile.png' :
                            'img/grey_pc.png';
                        table = 'pending';
                        break;
                    case '0':
                        status = 'All Clear';
                        divClass = 'success';
                        iconSrc = data['responses'][i]['mobile'] === '1' ?
                            'img/green_mobile.png' :
                            'img/green_pc.png';
                        table = 'all-clear';
                        break;
                    case '1':
                        status = 'Need Help';
                        divClass = 'alert';
                        iconSrc = data['responses'][i]['mobile'] === '1' ?
                            'img/red_mobile.png' :
                            'img/red_pc.png';
                        table = 'need-help';
                        break;
                    case '2':
                        status = 'Need Information';
                        divClass = 'warning';
                        iconSrc = data['responses'][i]['mobile'] === '1' ?
                            'img/yellow_mobile.png' :
                            'img/yellow_pc.png';
                        table = 'need-info';
                        break;
                }
                var idx = responseIds.findIndex(function(response) {
                    return response.user === data['responses'][i]['user_id'];
                });
                if (idx === -1) {
                    var marker = new google.maps.Marker({
                        position: {
                            lat: parseFloat(data['responses'][i]['latitude']),
                            lng: parseFloat(data['responses'][i]['longitude']),
                        },
                        icon: iconSrc,
                        map: map,
                        user: data['responses'][i]['user'],
                        status: data['responses'][i]['status_name'],
                        comments: data['responses'][i]['comments'],
                    });
                    google.maps.event.addListener(marker, 'click', function() {
                        var html = '' +
                            '<h6>' + this.user + '</h6>' +
                            '<p>' + this.comments + '</p>' +
                            '';
                        infowindow.setContent(html);
                        infowindow.open(map, this);
                    });
                    responseIds.push({
                        'user': data['responses'][i]['user_id'],
                        'status': data['responses'][i]['status'],
                        'marker': marker,
                        'table': table,
                    });
                    if (!isNaN(marker.position.lat()) && !isNaN(marker.position.lng())) {
                        bounds.extend(marker.position);
                        map.fitBounds(bounds);
                    }
                } else {
                    if (data['responses'][i]['status'] !== responseIds[idx]['status']) {
                        responseIds[idx]['status'] = data['responses'][i]['status'];
                        responseIds[idx]['marker'].setIcon(iconSrc);
                        responseIds[idx]['marker'].setPosition(new google.maps.LatLng(
                            data['responses'][i]['latitude'],
                            data['responses'][i]['longitude']
                        ));
                        responseIds[idx]['marker'].comments = data['responses'][i]['comments'];
                        if (!isNaN(responseIds[idx]['marker'].position.lat()) &&
                            !isNaN(responseIds[idx]['marker'].position.lng())) {
                            bounds.extend(responseIds[idx]['marker'].position);
                            map.fitBounds(bounds);
                        }
                        /*
                        $('#' + data['responses'][i]['user_id']).removeClass('secondary success alert warning').addClass(divClass);
                        $('#' + data['responses'][i]['user_id'] + '').empty().append('<strong>' + status + '</strong> - ' + data['responses'][i]['updated_date'] + ' ' + data['responses'][i]['user'] + '<p>' + data['responses'][i]['comments'] + '</p>');
                        */
                    }
                }
            }
        } else {

        }
    }).fail(function(data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
    });
}

function getAssets() {
    $.ajax({
        dataType: 'json',
        url: 'process/get_assets.php',
        type: 'GET',
        data: {'id': mainEvent['emergency_id'], 'user': user['id']},
        beforeSend: function() {
            //$('#total-not-received span').empty().append($('#not-received-users-modal table tbody tr').length + " Users");
        },
    }).done(function(data) {
        console.log(data);
        if (data.success) {
            for (var i in data['assets']) {
                var marker = new google.maps.Marker({
                    position: {
                        lat: parseFloat(data['assets'][i]['latitude']),
                        lng: parseFloat(data['assets'][i]['longitude']),
                    },
                    icon: 'img/red_marker.png',
                    map: map,
                    contact: data['assets'][i]['contact'],
                    phone: data['assets'][i]['contact_phone'],
                    placement: data['assets'][i]['placement'],
                    address: data['assets'][i]['location_address'],
                });
                google.maps.event.addListener(marker, 'click', function() {
                    var html = '' +
                        '<h6>' + this.address + '</h6>' +
                        '<p>' + this.placement + '</p>' +
                        '';
                    infowindow.setContent(html);
                    infowindow.open(map, this);
                });
                if (!isNaN(marker.position.lat()) && !isNaN(marker.position.lng())) {
                    bounds.extend(marker.position);
                    map.fitBounds(bounds);
                }
            }
        } else {

        }
    }).fail(function(data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
    });
}

function initMap() {
    var event = {lat: 41.9568958, lng: -91.72251};
    if (typeof mainEvent !== 'undefined') {
        event = {
            lat: parseFloat(mainEvent['latitude']),
            lng: parseFloat(mainEvent['longitude']),
        };
        map = new google.maps.Map(document.getElementById('event-map'), {
            zoom: 15,
            center: event,
        });

        infowindow = new google.maps.InfoWindow({
            content: 'AEDs',
        });

        var marker = new google.maps.Marker({
            position: event,
            icon: 'img/event.png',
            map: map,
        });
        bounds = new google.maps.LatLngBounds();
        bounds.extend(event);

        getAssets();
        getResponses();
        setInterval(getResponses, updateInterval);
    } else {
        map = new google.maps.Map(document.getElementById('event-map'), {
            zoom: 5,
            center: event,
        });
    }
}

$(document).ready(function() {
    /*
        $('#subscribe-notifications').click(function () {
            subscribe();
        });

        $('#unsubscribe-notifications').click(function () {
            unsubscribe();
        });

        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.register('./service-worker.js')
                .then(initialiseState);
        } else {
            console.debug('Service workers aren\'t supported in this browser.');
            //window.Demo.debug.log('Service workers aren\'t supported in this browser.');
        }
    */
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('./service-worker.js').then(function() {
            return navigator.serviceWorker.ready;
        }).then(function(reg) {
            //console.log('Service Worker is ready', reg);
            reg.pushManager.subscribe({userVisibleOnly: true}).then(function(sub) {
                sendSubscriptionToServer(sub);
                //console.log('endpoint:', sub.endpoint);
                reg.active.postMessage(JSON.stringify({uid: user['id'], jwt: getJWT()}));
                //console.log('Posted message');

            });
        }).catch(function(error) {
            console.log('Error : ', error);
        });
    }

    if (typeof mainEvent !== 'undefined') {
        getTimeline();
        setInterval(getTimeline, updateInterval);
    }

    $('#alert-response-form').submit(function(e) {
        e.preventDefault();
        var form = $(this);
        $.ajax({
            dataType: 'json',
            url: 'https://tsdemos.com/mobileapi/alert_response.php',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function() {
                $('#thinking-modal').foundation('open');
            },
        }).done(function(data) {
            console.log(data);
            $('#thinking-modal').foundation('close');
            if (data.success) {
                location.reload();
                console.log(data);
                //TODO: SUCCESS MESSAGE
            } else {
                //TODO: FAILED MESSAGE
            }
            //form.foundation('close');
        }).fail(function(data, textStatus, errorThrown) {
            console.log(data);
            console.log(textStatus);
            console.log(errorThrown);
        });
        return false;
    });
});
