$(document).ready(function () {
    var eventsTable = $('#calls-table').DataTable({
        "dom": 'Brtip',
        "paging": true,
        "pageLength": 20,
        "scrollCollapse": true,
        "processing": true,
        "serverSide": true,
        "deferRender": true,
        "order": [[2,'desc']],
        "ajax": {
            "url": "process/get_police_calls.php",
            "type": "GET",
            "data": {
                "emma-plan-id": user['emma_plan_id'],
            }
        },
        "columns": [
            {
                "data": "type",
                "render": function (data, type, row, meta) {
                    console.log(row);
                    return "<a href='dashboard.php?content=call&id=" + row['call_id'] + "'>" + row['call_datetime'] + "</a>";
                }
            },
            {
                "data": "username",
                "render": function (data, type, row, meta) {
                    console.log(row);
                    console.log(data);
                    return "<a href='dashboard.php?content=user&id=" + row['userid'] + "'>" + row['username'] + "</a> (<a href='mailto:'"+ row['useremail'] +">"+ row['useremail'] +"</a>)"
                }
            },
            {"data": "latitude"},
            {"data": "longitude"},
            {"data": "type",
                "render": function (data, type, row, meta) {
                    console.log(row);
                    return "<a href='dashboard.php?content=event&id=" + row['emergency_id'] + "'>" + data + "</a>";
                }},
        ]
    });


    $('#calls-table thead th.text-search').each(function (i) {
        let title = $('#calls-table thead th').eq($(this).index()).text();
        $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
    });

    $(eventsTable.table().container()).on('keyup', 'thead input.text-search', function () {
        eventsTable
            .column($(this).data('index'))
            .search(this.value)
            .draw();
    });
});