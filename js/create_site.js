

function getcoords(address)
{
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': address}, function(results, status) {

        if (status == google.maps.GeocoderStatus.OK) {
            //var latitude = results[0].geometry.location.latitude;
            //var longitude = results[0].geometry.location.longitude;
            $('#lat').val(results[0].geometry.location.lat());
            $('#lng').val(results[0].geometry.location.lng());
            //console.log(results[0].geometry.location.latitude + " " + results[0].geometry.location.longitude);
            //console.log($('#lat').val() + ' ' + $('#lng').val());
            //alert(latitude);
        }
    });
}



let new_contact_html = '<div><h3 class="lead"><button id="delete-new-contact-button" type="button" class="button-expanded" style="background-color: red;"><i style="background-color: white;" class=" fa fa-times"></i></button> Contact Information</h3> </div> <div class="row expanded"> <div class="large-6 medium-12 small-12 column"> <label>First Name <input required class="needs-filled" type="Text" name="first-name"/> </label> </div> <div class="large-6 medium-12 small-12 column"><label>Last Name <input class="needs-filled" required type="Text" name="last-name"/> </label> </div> </div> <div class="row expanded"> <div class="large-6 medium-12 column"> <label>Email <input class="needs-filled" required type="Text" name="email"/> </label> </div> <div class="large-6 medium-12 column"> <label>Phone <input class="needs-filled" required type="Text" name="phone"/> </label> </div> </div>';

$(document).ready(function () {
  function saveSite() {

      //check if inputs are empty
      allarefilled = true;
      $('input[class="needs-filled"]').each(function(){
          if($(this).val().replace(/^\s+|\s+$/g, "").length === 0) //Removes whitespace
          {
              allarefilled = false;
          }
      });

      if(!allarefilled)
      {
          alert("Please fill out more Site Information");
      }
      else {
          $.ajax({
              dataType: 'json',
              url: 'process/create_site.php',
              type: 'POST',
              data: $('#create-site-form').serialize(),
              beforeSend: function () {
                  $('#thinking-modal').foundation('open');
              }
          })
              .done(function (data) {
                  console.log(data);
                  $('#thinking-modal').foundation('close');
                  $('#error-list').empty();
                  if (data.success) {
                      $('#success-modal').foundation('open');
                  } else {
                      if (data['errors']['invalid-address']) {
                          $('#warning-modal').foundation('open');
                      } else {
                          for (let error in data['errors']) {
                              $('#error-list').append(data['errors'][error] + '<br />');
                          }
                          $('#fail-modal').foundation('open');
                      }
                  }
              })
              .fail(function (data, textStatus, errorThrown) {
                  $('#thinking-modal').foundation('close');
                  console.log(data);
                  console.log(textStatus);
                  console.log(errorThrown);
              });
      }
  }

let address = '';
  input_address = $('#address');
  input_state = $('#state');
  input_city = $('#city');
  input_zip = $('#zip');


    input_address.change(function(){
      if(input_city.val() != '' && input_state.val() != '' && input_zip.val() != '' && input_address != '')
      {
        address = input_address.val() + ', ' + input_city.val() + ', ' + input_state.val() + ', ' + input_zip.val() + ', ' + 'USA';
        getcoords(address);
      }
  });
    input_city.change(function(){
        if(input_city.val() != '' && input_state.val() != '' && input_zip.val() != '' && input_address != '')
        {
            address = input_address.val() + ', ' + input_city.val() + ', ' + input_state.val() + ', ' + input_zip.val() + ', ' + 'USA';
            getcoords(address);
        }
    });
    input_state.change(function(){
        if(input_city.val() != '' && input_state.val() != '' && input_zip.val() != '' && input_address != '')
        {
            address = input_address.val() + ', ' + input_city.val() + ', ' + input_state.val() + ', ' + input_zip.val() + ', ' + 'USA';
            getcoords(address);
        }
    });
    input_zip.change(function(){
        if(input_city.val() != '' && input_state.val() != '' && input_zip.val() != '' && input_address != '')
        {
            address = input_address.val() + ', ' + input_city.val() + ', ' + input_state.val() + ', ' + input_zip.val() + ', ' + 'USA';
            getcoords(address);
        }
    });

$('#new-contact-button').click(function(){
    $('#existing-contact-append-here').empty();
    $('#new-contact-append-here').empty();
    $('#new-contact-append-here').append(new_contact_html);
});
$('#existing-contact-button').click(function(){
    $('#existing-contact-append-here').empty();
    $('#new-contact-append-here').empty();
    prefab = $('#existing-contacts-prefab').clone();
    prefab.css('display','inherit');
    $('#existing-contact-append-here').append(prefab);
});
$(document).on('click','#delete-new-contact-button',function(){
    $('#new-contact-append-here').empty();
});
$(document).on('click','#delete-existing-contact-button',function(){
    $('#existing-contact-append-here').empty();
});










  $('#save-site-button').click(function(e) {
    e.preventDefault();
    saveSite();
  });

  $('#warning-save').click(function(e) {
    $('#ignore-address').val("1");
    saveSite();
  });

});