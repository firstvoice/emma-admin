$(document).ready(function () {

  $('#edit-group-form').submit(function (e) {
    e.preventDefault();
    $.ajax({
      dataType: 'json',
      url: 'process/edit_group.php',
      type: 'POST',
      data: $('#edit-group-form').serialize(),
      beforeSend: function () {
        $('#thinking-modal').foundation('open');
      }
    })
      .done(function (data) {
        console.log(data);
        $('#thinking-modal').foundation('close');
        $('#error-list').empty();
        if (data.success) {
          $('#success-modal').foundation('open');
        } else {
          for (var error in data['errors']) {
            $('#error-list').append(data['errors'][error] + '<br />');
          }
          $('#fail-modal').foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        $('#thinking-modal').foundation('close');
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });


  });


});