$(document).ready(function () {
  $('#edit-plan-form').submit(function (e) {
    e.preventDefault();
    let form = $(this);
    $.ajax({
      dataType: 'json',
      url: 'process/edit_plan.php',
      type: 'POST',
      data: form.serialize(),
      beforeSend: function () {
        $('#thinking-modal').foundation('open');
      },
    })
      .done(function (data) {
        console.log(data);
        $('#thinking-modal').foundation('close');
        if (data.success) {
          $('#plan-success-modal').foundation('open');
          // location.reload();
          //TODO: SUCCESS MESSAGE
        } else {
          //TODO: FAILED MESSAGE
        }
        //form.foundation('close');
      })
      .fail(function (data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
    return false;
  });
});