function initMap () {
  let map = new google.maps.Map(document.getElementById('sos-map'), {
    zoom: 2,
    center: {lat: 0, lng: 0},
  });

  let infowindow = new google.maps.InfoWindow({
    content: 'SOS',
  });

  var sosIds = [];
  var markers = [];

  var bounds = new google.maps.LatLngBounds();

  function getSos () {
    $.ajax({
      dataType: 'json',
      url: 'process/get_sos_map.php',
      type: 'GET',
      data: {'plan-id': planId},
    })
      .done(function (data) {
        console.log(data);
        if (data.success) {
          for (let sos of data['sos']) {
            console.log(sos);
            let marker;
            if (sos['help_date']) {
              marker = new google.maps.Marker({
                position: {
                  lat: parseFloat(sos['help_lat']),
                  lng: parseFloat(sos['help_lng']),
                },
                icon: 'img/help.png',
                map: map,
                user: sos['user'],
              });
            } else {
              marker = new google.maps.Marker({
                position: {
                  lat: parseFloat(sos['pending_lat']),
                  lng: parseFloat(sos['pending_lng']),
                },
                icon: 'img/pending.png',
                map: map,
                user: sos['user'],
              });
            }
            google.maps.event.addListener(marker, 'click', function () {
              var html = '' +
                '<h6>' + this.user + '</h6>' +
                '';
              infowindow.setContent(html);
              infowindow.open(map, this);
            });
            bounds.extend(marker.position);
            map.fitBounds(bounds);
          }
        } else {

        }
      })
      .fail(function (data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
  }

  getSos();

}