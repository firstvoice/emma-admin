$(document).ready(function () {
  var eventsTable = $('#groups-table').DataTable({
    'dom': 'Brtip',
    'paging': true,
    'pageLength': 25,
    'scrollCollapse': true,
    'processing': true,
    'serverSide': true,
    'deferRender': true,
      "order": [[0,'asc']],
    'ajax': {
      'url': 'process/get_groups.php',
      'type': 'GET',
      'data': {
        'emma-plan-id': user['emma_plan_id'],
      },
    },
    'columns': [
      {
        'data': 'name',
        'render': function (data, type, row, meta) {
          return '<a href=\'dashboard.php?content=group&id=' +
            row['emma_group_id'] + '\' >' + row['name'] + '</a>';
        },
      },
      {
        'data': 'inactive_users',
        'render': function (data, type, row, meta) {
          return row['inactive_users'];
        },
        'sorting': false
      },
      {
        'data': 'active_users',
        'render': function (data, type, row, meta) {
          return row['active_users'];
        },
        'sorting': false
      },
      {
        'data': 'info_only',
        'render': function (data, type, row, meta) {
          return row['info_only'] === '1' ? '<span style="color:green;">True</span>' : '<span style="color:red;">False</span>';
        },
      },
      {
        'data': 'admin',
        'render': function (data, type, row, meta) {
          return row['admin'] === '1' ? '<span style="color:green;">True</span>' : '<span style="color:red;">False</span>';
        },
      },
      {
        'data': 'security',
        'render': function (data, type, row, meta) {
          return row['security'] === '1' ? '<span style="color:green;">True</span>' : '<span style="color:red;">False</span>';
        },
      },
      {
        'data': 'emma_sos',
        'render': function (data, type, row, meta) {
          return row['emma_sos'] === '1' ? '<span style="color:green;">True</span>' : '<span style="color:red;">False</span>';
        },
      },
      {
        'data': '911admin',
        'render': function (data, type, row, meta) {
            return row['911admin'] === '1' ? '<span style="color:green;">True</span>' : '<span style="color:red;">False</span>';
        },
      },
    ],
  });
  /*
  $('#groups-table tfoot th.text-search').each(function (i) {
    let title = $('#groups-table thead th').eq($(this).index()).text();
    $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
  });

  $(eventsTable.table().container()).on('keyup', 'tfoot input.text-search', function () {
    eventsTable
      .column($(this).data('index'))
      .search(this.value)
      .draw();
  });
  */

  // $(document).on('click', "a.group_link", function() {
  //     var Id = $(this).data('id');
  //
  //     $.ajax({
  //         dataType: 'json',
  //         url: 'process/get_group_details.php',
  //         type: 'POST',
  //         data: {'group-id': Id}
  //     })
  //         .done(function (data) {
  //             console.log(data);
  //             $('#error-list').empty();
  //             if (data.success) {
  //                 $('#group-id').empty().append(data['id']);
  //
  //
  //                 $('#group_info').foundation('open');
  //
  //             } else {
  //                 for (var error in data['errors']) {
  //                     $('#error-list').append(data['errors'][error] + '<br />');
  //                 }
  //                 $('#fail-modal').foundation('open');
  //             }
  //         })
  //         .fail(function (data, textStatus, errorThrown) {
  //             $('#thinking-modal').foundation('close');
  //             console.log(data);
  //             console.log(textStatus);
  //             console.log(errorThrown);
  //         });
  // });

});