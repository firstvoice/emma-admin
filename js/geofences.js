$(document).ready(function () {
    var geofencesTable = $('#geofences-table').DataTable({
        'dom': 'Brtip',
        'paging': true,
        'pageLength': 200,
        'scrollCollapse': true,
        'processing': true,
        'serverSide': true,
        'deferRender': true,
        "order": [[0,'asc']],
        'ajax': {
            'url': 'process/get_geofences.php',
            'type': 'GET',
            'data': {
                'emma-plan-id': user['emma_plan_id'],
            },
        },
        'columns': [
            {
                'data': 'fence_name',
                'render': function (data, type, row, meta) {
                    return '<a href=\'dashboard.php?content=geofence&id=' + row['id'] + '\'>' +
                        data + '</a>';
                },
            },
            {'data': 'nwc'},
            {'data': 'sec'},
            {'data': 'cc'},
            {'data': 'rad'},
            {'data': 'type'},
            {'data': 'status'},
        ],
    });



});