$(document).ready(function () {
  $('#delete-user-form').submit(function (e) {
    e.preventDefault();
    $.ajax({
      dataType: 'json',
      url: 'process/delete_user.php',
      type: 'POST',
      data: $(this).serialize(),
      beforeSend: function () {
        $('#thinking-modal').foundation('open');
      },
    })
      .done(function (data) {
        console.log(data);
        $('#thinking-modal').foundation('close');
        $('#error-list').empty();
        if (data.success) {
          $('#success_modal').foundation('open');
        } else {
          for (var error in data['errors']) {
            $('#error-list').append(data['errors'][error] + '<br />');
          }
          $('#fail-modal').foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        $('#thinking-modal').foundation('close');
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
    return false;
  });

  $('#reactivate-user-form').submit(function (e) {
    e.preventDefault();
    $.ajax({
      dataType: 'json',
      url: 'process/reactivate_user_display.php',
      type: 'POST',
      data: $(this).serialize(),
      beforeSend: function () {
        $('#thinking-modal').foundation('open');
      },
    })
      .done(function (data) {
        console.log(data);
        $('#thinking-modal').foundation('close');
        $('#error-list').empty();
        if (data.success) {
          $('#success_modal').foundation('open');
        } else {
          for (var error in data['errors']) {
            $('#error-list').append(data['errors'][error] + '<br />');
          }
          $('#fail-modal').foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        $('#thinking-modal').foundation('close');
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
    return false;
  });
});