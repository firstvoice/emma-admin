
// Initialize Firebase
var config = {
    apiKey: "AIzaSyDS_cKQ5EnelY2rUO6JOIQKOeyil5b7enw",
    authDomain: "emma-c2c94.firebaseapp.com",
    projectId: "emma-c2c94",
    messagingSenderId: "742343872016"
};
firebase.initializeApp(config);

// [START get_messaging_object]
// Retrieve Firebase Messaging object.
const messaging = firebase.messaging();
// [END get_messaging_object]
// [START set_public_vapid_key]
// Add the public key generated from the console here.
messaging.usePublicVapidKey('BEcKNbZrtHmjRWkXg3aLijfAEZN_ldR_mlQj9NhJUTKL1ZP6c0futNhyZZJxokijtnH3Qr2pZTKIcKCwSNLzSsE');
// [END set_public_vapid_key]
// IDs of divs that display Instance ID token UI or request permission UI.
// const tokenDivId = 'token_div';
// const permissionDivId = 'permission_div';
// [START refresh_token]
// Callback fired if Instance ID token is updated.
messaging.onTokenRefresh(function() {
    messaging.getToken().then(function(refreshedToken) {
        console.log('Token refreshed.');
        // Indicate that the new Instance ID token has not yet been sent to the
        // app server.
        setTokenSentToServer(false);
        // Send Instance ID token to app server.
        sendTokenToServer(refreshedToken);
        // [START_EXCLUDE]
        // Display new Instance ID token and clear UI of all previous messages.
        resetUI();
        // [END_EXCLUDE]
    }).catch(function(err) {
        console.log('Unable to retrieve refreshed token ', err);
        showToken('Unable to retrieve refreshed token ', err);
    });
});
// [END refresh_token]
// [START receive_message]
// Handle incoming messages. Called when:
// - a message is received while the app has focus
// - the user clicks on an app notification created by a service worker
//   `messaging.setBackgroundMessageHandler` handler.
messaging.onMessage(function(payload) {
    console.log('Message received. ', payload);
    // [START_EXCLUDE]
    // Update the UI to include the received message.
    // appendMessage(payload);
    // [END_EXCLUDE]
    let notificationTitle = payload.data.title;
    let notificationBody = payload.data.body;

    let popup = document.getElementById("emergency_alert");
    let alert_button = document.getElementById("emergency_popup_link");
    let icon = document.getElementById("emergency_popup_icon");
    let header = document.getElementById("emergency_popup_header");
    let details = document.getElementById("emergency_popup_details");


    icon.innerHTML = '<img src="img/'+ payload.data.emergency_icon +'"/>';
    header.innerHTML = notificationTitle;
    details.innerHTML = notificationBody;
    if(payload.data.emergency_icon === 'icon_emma_sos.png'){
        alert_button.href= 'dashboard.php?content=sos_details&id=' + payload.data.emergency_id;
    }
    else{
        alert_button.href= 'dashboard.php?content=event&id=' + payload.data.emergency_id;
    }
    popup.style.display = 'block';
    let audio = new Audio('sounds/notification.mp3');
    audio.play();
    // alert(notificationTitle+'\n'+notificationBody);
});
// [END receive_message]
function resetUI() {
    // clearMessages();
    // showToken('loading...');
    // [START get_token]
    // Get Instance ID token. Initially this makes a network call, once retrieved
    // subsequent calls to getToken will return from cache.
    messaging.getToken().then(function(currentToken) {
        if (currentToken) {
            sendTokenToServer(currentToken);
            // updateUIForPushEnabled(currentToken);
        } else {
            // Show permission request.
            console.log('No Instance ID token available. Request permission to generate one.');
            // Show permission UI.
            updateUIForPushPermissionRequired();
            setTokenSentToServer(false);
        }
    }).catch(function(err) {
        console.log('An error occurred while retrieving token. ', err);
        // showToken('Error retrieving Instance ID token. ', err);
        setTokenSentToServer(false);
    });
    // [END get_token]
}
// function showToken(currentToken) {
//     // Show token in console and UI.
//     var tokenElement = document.querySelector('#token');
//     tokenElement.textContent = currentToken;
// }
// Send the Instance ID token your application server, so that it can:
// - send messages back to this app
// - subscribe/unsubscribe the token from topics
function sendTokenToServer(currentToken) {
    if (!isTokenSentToServer()) {
        console.log('Sending token to server...');
        // TODO(developer): Send the current token to your server.
        updateServer(currentToken);

        setTokenSentToServer(true);
    } else {
        console.log('Token already sent to server so won\'t send it again ' +
            'unless it changes');
    }
}
function isTokenSentToServer() {
    return window.localStorage.getItem('sentToServer') === '1';
}
function setTokenSentToServer(sent) {
    window.localStorage.setItem('sentToServer', sent ? '1' : '0');
}
function requestPermission() {
    console.log('Requesting permission...');
    // [START request_permission]
    messaging.requestPermission().then(function() {
        console.log('Notification permission granted.');
        // TODO(developer): Retrieve an Instance ID token for use with FCM.
        // [START_EXCLUDE]
        // In many cases once an app has been granted notification permission, it
        // should update its UI reflecting this.
        resetUI();
        // [END_EXCLUDE]
    }).catch(function(err) {
        console.log('Unable to get permission to notify.', err);
    });
    // [END request_permission]
}
function updateUIForPushPermissionRequired() {

    requestPermission();
    // <div id="permission_div" style="display: none;">
    //         <h4>Needs Permission</h4>
    //     <p id="token"></p>
    //         <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored"
    //     onclick="requestPermission()">Request Permission</button>
    //     </div>
}
function updateServer(firebToken){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            console.log('subscription saved.');
            console.log(this.responseText);
        }
    };
    var poststring = "user="+self.user.id+"&subscription="+firebToken;
    xhttp.open("POST", "process/add_client_id", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(poststring);
}
resetUI();
