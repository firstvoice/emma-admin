$(document).ready(function () {
    for(var i=0; i<40; i++){
        $('#plan-asset-lat-in'+i).attr('disabled',true);
        $('#plan-asset-lon-in'+i).attr('disabled',true);
    }

    $('#step1complete').click(function () {
      if($('#name1').val() !== '' && $('#active1').val() !== '' && $('#expired1').val() !== ''){
          $('#tab2').removeClass('disabled');
          $('#tab2').click();
      }
   });

    $('#step2complete').click(function () {
      if(siteactivecount >= 1){
          $('#tab3').removeClass('disabled');
          $('#tab3').click();
      }
   });

    $('#step3complete').click(function () {
      if(groupactivecount >= 1){
          $('#tab4').removeClass('disabled');
          $('#tab4').click();
      }
   });

    $('#step4complete').click(function () {
          $('#tab5').removeClass('disabled');
          $('#tab5').click();
   });

    $('#complete-site-warning').hide();
    $('#max-site-warning').hide();

    $('#panic').click(function () {
        console.log($('#panic').prop("checked"));
        if($('#panic').prop("checked")){
            $('#duration-container').removeAttr('hidden');
            $('#duration-container').removeAttr('disabled');
            $('#sos-container').removeAttr('hidden');
            $('#sos-container').removeAttr('disabled');

        }
        else{
            $('#duration-container').attr('hidden',true);
            $('#duration-container').attr('disabled',true);
            $('#sos-container').attr('hidden',true);
            $('#sos-container').attr('disabled',true);
        }
    });

    $('#securities').click(function () {
        console.log($('#securities').prop("checked"));
        if($('#securities').prop("checked")){
            $('#security-container').removeAttr('hidden');
            $('#security-container').removeAttr('disabled');

        }
        else{
            $('#security-container').attr('hidden',true);
            $('#security-container').attr('disabled',true);
        }
    });

    $('#geofences').click(function () {
        console.log($('#geofences').prop("checked"));
        if($('#geofences').prop("checked")){
            $('#maxgeo-container').removeAttr('hidden');
            $('#maxgeo-container').removeAttr('disabled');
            $('#geofences-container').removeAttr('hidden');
            $('#geofences-container').removeAttr('disabled');

        }
        else{
            $('#maxgeo-container').attr('hidden',true);
            $('#maxgeo-container').attr('disabled',true);
            $('#geofences-container').attr('hidden',true);
            $('#geofences-container').attr('disabled',true);
        }
    });

    var sitecount = 0;
    var siteactivecount = 0;
   $('#temp-site-btn').click(function () {
       $('#complete-site-warning').hide();
       $('#max-site-warning').hide();

       if(siteactivecount < $('#max').val()) {

           var name = $('#temp-name').val();
           var address = $('#temp-address').val();
           var city = $('#temp-city').val();
           var state = $('#temp-state').val();
           var zip = $('#temp-zip').val();
           var first = $('#temp-first').val();
           var last = $('#temp-last').val();
           var email = $('#temp-email').val();
           var phone = $('#temp-phone').val();
           sitecount++;
           siteactivecount++;
           if (name !== '' && address !== '' && city !== '' && state !== ''
               && zip !== '' && first !== '' && last !== '' && email !== '' && phone !== '') {
               $('#sites-container').append(`
                <div id="site` + sitecount + `">
                    <div class="row">
                        <div class="large-8 columns">
                            <span>` + name + `</span>
                            <input type="hidden" name="site-name[]" value="` + name + `">
                            <input type="hidden" name="site-address[]" value="` + address + `">
                            <input type="hidden" name="site-city[]" value="` + city + `">
                            <input type="hidden" name="site-state[]" value="` + state + `">
                            <input type="hidden" name="site-zip[]" value="` + zip + `">
                            <input type="hidden" name="site-first[]" value="` + first + `">
                            <input type="hidden" name="site-last[]" value="` + last + `">
                            <input type="hidden" name="site-email[]" value="` + email + `">
                            <input type="hidden" name="site-phone[]" value="` + phone + `">
                        </div>
                        <div class="large-4 columns">
                            <a class="remove-site`+ sitecount +`" data-id="`+ sitecount +`" style="color: red">X</a>
                        </div>
                    </div> 
                </div>
               `);

               $('.remove-site'+sitecount).click(function () {
                   var count = $(this).data('id');
                   $('#site'+count).hide();
                   $('#site'+count).children().attr("disabled","disabled");
                   --siteactivecount;
                   // console.log(siteactivecount);
               });

               // console.log(siteactivecount);
               // console.log(sitecount);
           }
           else {
               $('#complete-site-warning').show();
           }
       }else {
           $('#max-site-warning').show();
       }
   });

    $('#complete-group-warning').hide();
    var groupcount = 0;
    var groupactivecount = 0;
    $('#temp-group-btn').click(function () {
        $('#complete-group-warning').hide();

            var name = $('#temp-groupname').val();
            var admin = $('#administrator').is(":checked");
            var info = $('#event-info-only').is(":checked");
            var security = $('#security').is(":checked");
            var geofence = $('#geofence').is(":checked");
            var esos = $('#sos').is(":checked");
            var admin911 = $('#911admin').is(":checked");
            groupcount++;
            groupactivecount++;
            if (name !== '') {
                $('#groups-container').append(`
                <div id="group` + groupcount + `">
                    <div class="row">
                        <div class="large-8 columns">
                            <span>` + name + `</span>
                            <input type="hidden" name="group-name[]" value="` + name + `">
                            <input type="hidden" name="group-admin[]" value="` + admin + `">
                            <input type="hidden" name="group-info[]" value="` + info + `">
                            <input type="hidden" name="group-security[]" value="` + security + `">
                            <input type="hidden" name="group-geofences[]" value="` + geofence + `">
                            <input type="hidden" name="group-esos[]" value="` + esos + `">
                            <input type="hidden" name="group-admin911[]" value="` + admin911 + `">
                        </div>
                        <div class="large-4 columns">
                            <a class="remove-group`+ groupcount +`" data-id="`+ groupcount +`" style="color: red">X</a>
                        </div>
                    </div> 
                </div>
               `);

                $('.remove-group'+groupcount).click(function () {
                    var count = $(this).data('id');
                    $('#group'+count).hide();
                    $('#group'+count).children().attr("disabled","disabled");
                    --groupactivecount;
                });
                $('.temp-group').empty();
                $('.temp-group-switch').click();

                // console.log(groupcount);
            }
            else {
                $('#complete-group-warning').show();
            }
    });

    $('.plan-asset').click(function () {

        var count = $(this).data('count');
        if($(this).prop("checked")){
            $('#plan-asset-lat'+count).removeAttr('hidden');
            $('#plan-asset-lat-in'+count).removeAttr('disabled');
            $('#plan-asset-lon'+count).removeAttr('hidden');
            $('#plan-asset-lon-in'+count).removeAttr('disabled');

        }
        else{
            $('#plan-asset-lat'+count).attr('hidden',true);
            $('#plan-asset-lat-in'+count).attr('disabled',true);
            $('#plan-asset-lon'+count).attr('hidden',true);
            $('#plan-asset-lon-in'+count).attr('disabled',true);
        }

    });

    $('#new-plan-form').submit(function (e) {
        e.preventDefault();

        $.ajax({
            dataType: 'json',
            url: 'process/create_plan.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            }
        })
            .done(function (data) {
                console.log(data);
                $('#error-list').empty();
                $('#thinking-modal').foundation('close');
                if (data.success) {
                    $('#success_modal').foundation('open');
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        return false;
    });

});