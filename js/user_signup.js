$(document).ready(function () {

    $('#user_signup_form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/user_signup.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                THINKINGMODAL.foundation('open');
            },
        })
            .done(function (data) {
                console.log(data);
                THINKINGMODAL.foundation('close');
                if (data.success) {
                    $('#success-modal').foundation('open');
                } else {
                    FAILMODAL.foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        return false;
    });
var count = 2;

    $('#add-child-button').click(function () {
        $('#child-container').append('<div class="large-11 medium-11 small-11 columns"> <label style="color: grey">Name</label> <input type="text" class="childname" id="childname'+count+'" name="childname[]" placeholder="Name"></div>')
        count++;
    })

});
