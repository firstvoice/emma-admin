var t;

window.onload = resetTimer();
// DOM Events
// document.onmousemove = resetTimer();
// document.onkeypress = resetTimer();
console.log('loaded');

function logout() {
    location.href = 'logout.php';

    // $(document).ready(function () {
    //
    //     $.ajax({
    //         dataType: 'json',
    //         url: 'process/logout.php',
    //         type: 'POST',
    //         data: form.serialize(),
    //         beforeSend: function () {
    //
    //         },
    //     })
    //         .done(function (data) {
    //             if (data.success) {
    //                 location.href = 'logout.php'
    //             } else {
    //
    //             }
    //         })
    //         .fail(function (data, textStatus, errorThrown) {
    //             console.log(data);
    //             console.log(textStatus);
    //             console.log(errorThrown);
    //         });
    // });

}

function logoutWarning(){
    // clearTimeout(t2);
    // t2 = setTimeout(logout, 60000)

    //create popup
    $(document).ready(function () {


        $('#autoLogoutModal').foundation('open');

    });
    //update timer on popup to match second countdown

    var countDownDate = new Date(Date.now() + 60000);
// Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        document.getElementById("logoutTime").innerHTML = "Logout in " + seconds + " seconds";

        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            logout();
        }
    }, 1000);



    $(document).ready(function () {

        //logout button instantly logs out
        $('#instantLogoutButton').click(function () {
            location.href = 'logout.php'
        });

        //cancel button goes to cancelLogout
        $('#stayLoggedIn').click(function () {
            resetTimer();
            clearInterval(x);
            $('#autoLogoutModal').foundation('close');
        });


    });
}

function resetTimer() {
    console.log('timer restarted');
    clearTimeout(t);
    t = setTimeout(logoutWarning, 900000)
}

// function cancelLogout(){
//     clearTimeout(t);
//     t = setTimeout(logoutWarning, 50000)
//
//     //close popup
// }

//900000