$(document).ready(function () {
    $('#close-ar').submit(function () {
        $.ajax({
            dataType: 'json',
            url: 'process/close_ar.php',
            type: 'POST',
            data: $('#close-ar').serialize(),
            beforeSend: function () {
                $('#close-ar').find('input[type="submit"]').attr('disabled', true);
                THINKINGMODAL.foundation('open');
            },
        }).done(function (data) {
            console.log(data);
            if (data.success) {
                $('#success_modal').foundation('open');
            } else {
                FAILMODAL.foundation('open');
            }
        }).fail(function (data, textStatus, errorThrown) {
            console.log(data);
            console.log(textStatus);
            console.log(errorThrown);
        });
        return false;
    });
});