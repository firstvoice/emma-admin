function geocodePosition(pos)
{
    //console.log(pos);
    //Sets closest address into input for each event modal..
    geocoder = new google.maps.Geocoder();
    geocoder.geocode
    ({
            latLng: new google.maps.LatLng(pos.latitude,pos.longitude)
        },
        function(results, status)
        {
            if (status == google.maps.GeocoderStatus.OK)
            {
                $('input[name="address"]').val(results[0].formatted_address);
                input_lat.val(pos.latitude);
                input_lng.val(pos.longitude);
            }
            else
            {
                $('input[name="address"]').val('N/A');
            }
        }
    );
}


marker_array = [];
map_array = [];
function initMap() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            loc = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            newmap = new google.maps.Map(
                document.getElementById('new-map'), {zoom: 12, center: loc});
            editmarker = new google.maps.Marker({position: loc, map: newmap, icon: 'img/orange-dot.png', draggable: true});
            map_array.push(newmap);
            google.maps.event.addListener(editmarker, 'dragend', function(event){
                //Update each lat/lng position on drag.
                input_lat.val(event.latLng.lat());
                input_lng.val(event.latLng.lng());
                let pos={latitude: input_lat.val(),longitude: input_lng.val()};
                //calc address, it wont fire the on change event when we change the values like this.
                geocodePosition(pos);
            });
            marker_array.push(editmarker);
        }, function() {
            handleLocationError(true, infoWindow, newmap.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        let loc = {lat: parseFloat(39.8283), lng: parseFloat(98.5795)};
        var newmap = new google.maps.Map(
            document.getElementById('new-map'), {zoom: 12, center: loc});
        var editmarker = new google.maps.Marker({position: loc, map: newmap, icon: 'img/orange-dot.png', draggable: true});
        google.maps.event.addListener(editmarker, 'dragend', function(event){
            //Update each lat/lng position on drag.
            input_lat.val(event.latLng.lat());
            input_lng.val(event.latLng.lng());
            let pos={latitude: input_lat.val(),longitude: input_lng.val()};
            //calc address, it wont fire the on change event when we change the values like this.
            geocodePosition(pos);
        });
    }



}
$('#create_asset_form_type_id').change(function (){
    $('#insert-area').empty();
    type_id = $('#create_asset_form_type_id option:selected').val();
    $.ajax({
        url: 'process/get_asset_items.php',
        type: 'GET',
        data: {'type_id': type_id},
        beforeSend: function () {
        },
    })
        .done(function (data) {
            data = JSON.parse(data);
            //Foreach array in data (associative array) we check to see if it has
            //a date, a dropdown, or a fillin. Then we append blank versions of those
            //with X buttons that can remove them.




            //update map
            //create a new marker on the map, but first clear the old one.
            let icon = 'img/orange-dot.png';
            if(data['image'][0]['image'] !== 'img/orange-dot.png' && data['image'][0]['image'] !== 'null' && data['image'][0]['image'] !== null)
            {
                icon = {
                    url: data['image'][0]['image'],
                    scaledSize: new google.maps.Size(25, 25)
                };
            }
            marker_array[0].setMap(null);
            var pos = {lat: parseFloat(input_lat.val()), lng: parseFloat(input_lng.val())};
            console.log(pos);
            editmarker = new google.maps.Marker({position: pos, map: map_array[0], icon: icon, draggable: true});
            google.maps.event.addListener(editmarker, 'dragend', function(event){
                //Update each lat/lng position on drag.
                input_lat.val(event.latLng.lat());
                input_lng.val(event.latLng.lng());
                let pos={latitude: input_lat.val(),longitude: input_lng.val()};
                //calc address, it wont fire the on change event when we change the values like this.
                geocodePosition(pos);
            });
            marker_array = []; //empty
            marker_array.push(editmarker);








            count = 0;



            if (data['item'] != null){
                var i;
                for (i = 0; i < data['item'].length; i++) {
                    if (data['item'][i]['date'] == '1') {
                        html = '<div class="row"></i><div class="large-12 columns" <label>' + data['item'][i]['name'] + '</label><input required type="date" name="date"/></div></div>';
                        $('#insert-area').append(html);
                    }
                    if (data['item'][i]['fillin'] == '1') {
                        html = '<div class = "row"><div class="large-12 columns"><label>Item Description</label><input type="text" name="fillin" placeholder="Item name" value="' + data['item'][i]['name'] + '"></div></div>';
                        $('#insert-area').append(html);
                    }
                    if (data['item'][i]['dropdown_id'] != null && data['item'][i]['dropdown_id'] != 0) {
                        //If the id="insert-area-edit" class="dropdownName" has the name value that already exists, then just add option to it.
                        //If it doesn't, then add in a dropdown.
                        if ($('.dropdownName[value="' + data['item'][i]['name'] + '"]')[0]) {
                            //Exists
                            option = '<option value="'+data['item'][i]['dropdown_id']+'">'+data['item'][i]['dropdown_name']+'</option>';
                            $('.dropdownName[data-dropdown="' + data["item"][i]["name"] + '"]').append(option);
                            count++;
                        }
                        else {
                            //Doesn't exist
                            html = '<label id="dropdownLabel' + count + '">'+data['item'][i]['name']+ " " +'Options</label><div class="row large-12 columns" id="dropdowninsert" data-dropdown="' + data["item"][i]["name"] + '" data-id="' + data["item"][i]["id"] + '"><div class="large-12 columns"><select required data-dropdown="' + data["item"][i]["name"] + '" data-id="' + data['item'][i]['id'] + '" data-originalname="' + data['item'][i]['name'] + '" value="'+data['item'][i]['name']+'" class="dropdownName"/></div></div>';
                            option_select = '<option value="" disabled selected>-Select-</option>';
                            option = '<option value="'+data['item'][i]['dropdown_id']+'">'+data['item'][i]['dropdown_name']+'</option>';
                            $('#insert-area').append(html);
                            $('.dropdownName[data-dropdown="' + data["item"][i]["name"] + '"]').append(option_select); //Append default -SELECT- option.
                            $('.dropdownName[data-dropdown="' + data["item"][i]["name"] + '"]').append(option); //still have to append here on first iteration.
                            count++;
                        }

                    }
                }
            }

        })
        .fail(function (data, textStatus, errorThrown) {

            console.log(textStatus);
            console.log(errorThrown);
        });
    return false;
});










$(document).ready(function () {



  var eventsTable = $('#assets-table').DataTable({
    "dom": 'Brtip',
    "paging": true,
    "pageLength": 20,
    "scrollCollapse": true,
    "processing": true,
    "serverSide": true,
    "deferRender": true,
      "order": [[0,'desc']],
    "ajax": {
      "url": "process/get_assets.php",
      "type": "GET",
      "data": {
        "emma-plan-id": user['emma_plan_id']
      }
    },
    "columns": [
      {
        "data" : "emma_asset_id",
        "render": function(data, type, row, meta) {
                return '<a href="dashboard.php?content=asset&id=' + data + '">' + row['asset_type_name'] + '</a>';
            }
      },
        {
            "data": "username",
            "render": function (data, type, row, meta) {
                return "<a href='dashboard.php?content=user&id=" + row['userid'] + "'>" + row['user_name'] + "</a> (<a href='mailto:'"+ row['useremail'] +">"+ row['useremail'] +"</a>)"
            }
        },
      {"data": "created_date"},
      {"data": "latitude"},
      {"data": "longitude"},
      {
        "data": "active",
        "render": function (data, type, row, meta) {
          return row['active'] === '1' ? "<span style='color:green'>Current</span>" : "<span style='color:red'>Inactive</span>";
        }
      },

    ]
  });

  $('#assets-table thead th.text-search').each(function (i) {
    let title = $('#assets-table thead th').eq($(this).index()).text();
    $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
  });

  $(eventsTable.table().container()).on('keyup', 'thead input.text-search', function () {
    eventsTable
      .column($(this).data('index'))
      .search(this.value)
      .draw();
  });

  input_lat = $('#entered_lat');
  input_lng = $('#entered_lng');


    input_lat.change(function(){
      if(input_lat.val() != null && input_lng.val() != null)
      {
          let pos={latitude: input_lat.val(),longitude: input_lng.val()};
          geocodePosition(pos);
      }
  });


  input_lng.change(function(){
      if(input_lat.val() != null && input_lng.val() != null)
      {
        let pos={latitude: input_lat.val(),longitude: input_lng.val()};
        geocodePosition(pos);
      }
    });



  $('#create-asset-form').submit(function () {















     let dropdown_array = new Array();
     let dates_array = new Array();
     let fillins_array = new Array();


    form = $('#create-asset-form').serializeArray();

      $("input[name^='fillin']").each(function(index){
          let obj = {name: 'fillin', value: $(this).val()};
          fillins_array.push(obj);
      });
      $("input[name^='date']").each(function(index){
          let obj = {name: 'date', value: $(this).val()};
          dates_array.push(obj);
      });
      $("select[class^='dropdownName']").each(function(index){
            let obj = {name: 'dropdown', value: $(this).val(), id: $(this).val()};
            dropdown_array.push(obj);
      });

      form.push(dropdown_array);
      form.push(dates_array);
      form.push(fillins_array);



    $.ajax({
      dataType: 'json',
      url: 'process/create_asset.php',
      type: 'POST',
      data: $.param(form) + '&Headers=' + JSON.stringify(dropdown_array) + '&Fillins=' + JSON.stringify(fillins_array) + '&Dates=' + JSON.stringify(dates_array) ,
      beforeSend: function () {
        THINKINGMODAL.foundation('open');
      },
    })
      .done(function (data) {
        //console.log(data);
          //clear arrays
          let dropdown_array = new Array();
          let dates_array = new Array();
          let fillins_array = new Array();
        THINKINGMODAL.foundation('close');
        if (data.success) {
          $('#success-create-modal').foundation('open');
        } else {
          FAILMODAL.foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
          //clear arrays
          let dropdown_array = new Array();
          let dates_array = new Array();
          let fillins_array = new Array();
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
    return false;
  });




});