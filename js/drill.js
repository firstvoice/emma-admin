let map;
let bounds;
let updateInterval = 5000;
let infowindow;
let mCreation = moment(mainEvent['created_date']);
let incidents = new vis.DataSet();
let responseIds = [];
let interval = 60;
let timeline = new vis.Timeline(document.getElementById('timeline'), incidents,
  {
    minHeight: '200px',
    start: mCreation - 1000 * 60 * interval / 2,
    end: mCreation + 1000 * 60 * interval / 2,
    order: function (a, b) {
      return a.id - b.id;
    },
    format: {
      minorLabels: {
        millisecond: 'SSS',
        second: 's',
        minute: 'h:mm a',
        hour: 'h:mm a',
        weekday: 'ddd D',
        day: 'D',
        week: 'w',
        month: 'MMM',
        year: 'YYYY',
      },
      majorLabels: {
        millisecond: 'HH:mm:ss',
        second: 'D MMMM HH:mm',
        minute: 'ddd D MMMM',
        hour: 'ddd D MMMM',
        weekday: 'MMMM YYYY',
        day: 'MMMM YYYY',
        week: 'MMMM YYYY',
        month: 'YYYY',
        year: '',
      },
    },
  });

/* Sub Events */
const subEvents = [];
const badgeFire = $('#badge-fire');
const tableFire = $('#fire-sub-drills');
const badgeLockDown = $('#badge-lock-down');
const tableLockDown = $('#lock-down-sub-drills');
const badgeShelter = $('#badge-shelter');
const tableShelter = $('#shelter-sub-drills');
const badgeActiveShooter = $('#badge-active-shooter');
const tableActiveShooter = $('#active-shooter-sub-drills');
const badgeExplosion = $('#badge-explosion');
const tableExplosion = $('#explosion-sub-drills');
const badgeHazmat = $('#badge-hazmat');
const tableHazmat = $('#hazmat-sub-drills');
const badgeNaturalDisaster = $('#badge-natural-disaster');
const tableNaturalDisaster = $('#natural-disaster-sub-drills');
const badgeSevereWeather = $('#badge-severe-weather');
const tableSevereWeather = $('#severe-weather-sub-drills');
const badgeMedical = $('#badge-medical');
const tableMedical = $('#medical-sub-drills');
const badgeOther = $('#badge-other');
const tableOther = $('#other-sub-drills');

function getTimeline() {
  $.ajax({
    dataType: 'json',
    url: 'process/get_timeline.php',
    type: 'POST',
    data: {'id': mainEvent['emergency_id'], 'order': 'updated_date', 'received': incidents.getIds().join(',')},
  }).done(function (data) {
    // console.log(data);
    if (data.success) {
      for (let incident of data['incidents']) {
        if (incident['emma_pin'] !== incident['response_pin']) {
          incident['status_class'] = 'validation';
        }
        if (!incidents.get(incident['id'])) {
          incidents.add([
            {
              id: incident['id'],
              start: new Date(incident['updated_date']).getTime(),
              content: incident['status_name'],
              title: `
                <b>Time:</b> ` + moment(incident['updated_date']).format('M/D/YYYY h:mm:ss a') + `<br/>
                <b>User:</b> ` + incident['user'] + `<br/>
                <b>Status:</b> ` + incident['status_name'] + `<br/>
                <b>Comments</b> ` + incident['message'],
              className: incident['status_class'],
            },
          ]);
          timeline.moveTo(incident['updated_date']);

          let groupList = ``;
          for (let group of incident['groups']) {
            groupList += `<a href="dashboard.php?content=group&id=` + group['emma_group_id'] + `">` + group['name'] + `</a> | `;
          }
          let nextMessage = `
            <div id="message-`+incident['id']+`" data-closable class="callout alert-callout-subtle radius inset-shadow ` + incident['status_class'] + `">
              <strong>
                <a href="dashboard.php?content=user&id=` + incident['user_id'] + `">` + incident['user'] + `</a> - 
                ` + moment(incident['updated_date']).format('M/D/YYYY h:mm:ss a') + `
              </strong>
              ` + (groupList === '' ? `` : `<div><b>Groups: </b>` + groupList + `</div>`) + `
              ` + (incident['description'] === '' ? `` : `<div><b>Description: </b>` + incident['description'] + `</div>`) + `
              ` + (incident['comments'] === '' ? `` : `<div><b>Comments: </b>` + incident['comments'] + `</div>`) + `
              ` + (incident['message'] === '' ? `` : `<div><b>Message: </b><p>` + incident['message'] + `</p></div>`) + `
              ` + (incident['validation_user'] === null || incident['validation_user'] === '' ? `` : `<strong style="color: green">Validated: </strong>` + incident['validation_user']) + `
            </div>
          `;
          $('#message-center').append(nextMessage);
          let messageCenterDiv = document.getElementById('message-center');
          messageCenterDiv.scrollTop = messageCenterDiv.scrollHeight;
        }
      }

      for (let subEvent of data['sub-events']) {
        let prevEvent = subEvents.filter(se => se.id === subEvent['id']);
        if (subEvents.filter(se => se.id === subEvent['id']).length === 0) {
          subEvents.push(subEvent);
          incidents.add([
            {
              id: 'evt-' + subEvent['id'],
              start: new Date(subEvent['created_date']).getTime(),
              content: '',
              title: `
                <b>Time:</b> ` + moment(subEvent['created_date']).format('M/D/YYYY h:mm:ss a') + `<br/>
                <b>User:</b> ` + subEvent['user'] + `<br/>
                <b>Status:</b> ` + subEvent['type_name'] + ` Sub-Event<br/>
                <b>Comments</b> Additional ` + subEvent['type_name'] + ` event reported`,
              type: 'point',
              className: 'system',
            },
          ]);
          timeline.moveTo(subEvent['created_date']);
          let row = `
            <tr>
              <td>` + subEvent['emma_site_name'] + `</td>
              <td>` + subEvent['user'] + `</td>
              <td>` + subEvent['created_date'] + `</td>
            </tr>
          `;
          $('#'+subEvent['badge']+'-sub-drills').append(row);
          let allSubTypes = subEvents.filter(se => se['type_id'] === subEvent['type_id']);
          $('#badge-'+subEvent['badge']).text(allSubTypes.length);
          if(allSubTypes.length > 0) $('#badge-'+subEvent['badge']).removeClass('is-hidden');
        }
      }
    }
  }).fail(function (data, textStatus, errorThrown) {
    console.log(data);
    console.log(textStatus);
    console.log(errorThrown);
  });
}

function getResponses() {
  $.ajax({
    dataType: 'json',
    url: 'process/get_responses.php',
    type: 'GET',
    data: {'id': mainEvent['emergency_id'], 'order': 'emergency_response_id'},
    beforeSend: function () {
      $('#total-not-received span').empty().append($('#not-received-users-modal table tbody tr').length +
        ' Users');
    },
  }).done(function (data) {
    // console.log(data);
    // console.log(responseIds);
    if (data.success) {
      for (let response of data['responses']) {
        if (response['emma_pin'] !== response['response_pin'] && response['status'] !== '-1') {
          response['name'] = 'Need Validation';
          response['icon'] = 'purple';
          response['table'] = 'need-validation';
        }
        let iconSrc = response['mobile'] === '1' ? 'img/'+response['icon']+'_mobile.png' : 'img/'+response['icon']+'_pc.png';
        let idx = responseIds.findIndex(function (r) {
          return r.user === response['user_id'];
        });
        if (idx === -1) {
          let marker = new google.maps.Marker({
            position: {
              lat: parseFloat(response['latitude']),
              lng: parseFloat(response['longitude']),
            },
            icon: iconSrc,
            map: map,
            user: response['user'],
            phone: response['phone'],
            status: response['name'],
            comments: response['comments'],
          });
          google.maps.event.addListener(marker, 'click', function () {
            let html = '' +
              '<h6>' + this.user + '</h6>' +
              '<p>' + this.phone + '</p>' +
              '<p>' + this.position.lat() + ', ' + this.position.lng() + '</p>' +
              '';
            infowindow.setContent(html);
            infowindow.open(map, this);
          });
          responseIds.push({
            'user': response['user_id'],
            'status': response['name'],
            'marker': marker,
            'table': response['table'],
          });
          $('#nr-' + response['user_id']).remove();
          $('#' + response['table'] + '-users-modal table tbody').append('' +
            '<tr id=\'' + response['user_id'] + '\'>' +
            '<td><a href=\'dashboard.php?content=user&id=' +
            response['user_id'] + '\' > ' +
            response['user'] + '</a></td>' +
            '<td>' + response['username'] + '</td>' +
            '<td>' + response['phone'] + '</td>' +
            '<td>' + response['phone'] + '</td>' +
            '<td>' + moment(response['updated_date']).format('M/D/YYYY h:mm:ss a') + '</td>' +
            (response['table'] === 'need-validation' ? '<td><a href="#" data-message-id="'+response['emergency_response_id']+'" class="validate-message button tiny" style="margin: 0;">validate</a></td>' : '') +
            '</tr>');
          validateButtons();
          $('#total-' + response['table'] + ' span').empty().append($('#' + response['table'] + '-users-modal table tbody tr').length +
            ' Users Last Updated: ' +
            moment(response['updated_date']).format('M/D/YYYY h:mm:ss a'));
          $('#total-not-received span').empty().append($('#not-received-users-modal table tbody tr').length +
            ' Users');
          if (!isNaN(marker.position.lat()) && !isNaN(marker.position.lng())) {
            bounds.extend(marker.position);
            map.fitBounds(bounds);
          }
        } else {
          if (response['name'] !== responseIds[idx]['status']) {
            $('#' + response['user_id']).remove();
            $('#total-' + responseIds[idx]['table'] + ' span').empty().append($('#' + responseIds[idx]['table'] +
              '-users-modal table tbody tr').length +
              ' Users Last Updated: ' +
              moment(response['updated_date']).format('M/D/YYYY h:mm:ss a'));
            $('#' + response['table'] + '-users-modal table tbody').append('' +
              '<tr id=\'' + response['user_id'] + '\'>' +
              '<td><a href=\'dashboard.php?content=user&id=' +
              response['user_id'] + '\' > ' +
              response['user'] + '</a></td>' +
              '<td>' + response['username'] + '</td>' +
              '<td>' + response['phone'] + '</td>' +
              '<td>' + response['phone'] + '</td>' +
              '<td>' + moment(response['updated_date']).format('M/D/YYYY h:mm:ss a') + '</td>' +
              (response['table'] === 'need-validation' ? '<td><a href="#" data-message-id="'+response['emergency_response_id']+'" class="validate-message button tiny" style="margin: 0;">validate</a></td>' : '') +
              '</tr>');
            validateButtons();
            $('#total-' + response['table'] + ' span').empty().append($('#' + response['table'] + '-users-modal table tbody tr').length +
              ' Users Last Updated: ' +
              moment(response['updated_date']).format('M/D/YYYY h:mm:ss a'));

            responseIds[idx]['status'] = response['name'];
            responseIds[idx]['table'] = response['table'];
            responseIds[idx]['marker'].setIcon(iconSrc);
            responseIds[idx]['marker'].setPosition(new google.maps.LatLng(
              response['latitude'],
              response['longitude'],
            ));
            responseIds[idx]['marker'].comments = response['comments'];
            if (!isNaN(responseIds[idx]['marker'].position.lat()) &&
              !isNaN(responseIds[idx]['marker'].position.lng())) {
              bounds.extend(responseIds[idx]['marker'].position);
              map.fitBounds(bounds);
            }
          }
        }
      }
    } else {

    }
  }).fail(function (data, textStatus, errorThrown) {
    console.log(data);
    console.log(textStatus);
    console.log(errorThrown);
  });
}

function initMap() {
  let drill = {
    lat: parseFloat(mainEvent['latitude']),
    lng: parseFloat(mainEvent['longitude']),
  };
  map = new google.maps.Map(document.getElementById('drill-map'), {
    zoom: 15,
    center: drill,
  });

  infowindow = new google.maps.InfoWindow({
    content: 'AEDs',
  });

  let marker = new google.maps.Marker({
    position: drill,
    icon: 'img/event.png',
    map: map,
  });
  bounds = new google.maps.LatLngBounds();
  bounds.extend(drill);

  getResponses();
  setInterval(getResponses, updateInterval);
}

function validateButtons() {
  $('.validate-message').click(function (e) {
    e.preventDefault();
    $.ajax({
      dataType: 'json',
      url: 'process/validate_emergency_response.php',
      type: 'POST',
      data: {'message-id': $(this).data('message-id'), 'validated-id': user['id']},
      beforeSend: function() {
        THINKINGMODAL.foundation('open');
      }
    }).done(function (data) {
      console.log(data);
      if (data.success) {
        $('#'+data['message']['user_id']).remove();
        $('#total-need-validation span').empty().append($('#need-validation-users-modal table tbody tr').length + ' Users Last Updated: ' + moment().format('M/D/YYYY h:mm:ss a'));
        $('#' + data['message']['table'] + '-users-modal table tbody').append('' +
          '<tr id=\'' + data['message']['user_id'] + '\'>' +
          '<td><a href=\'dashboard.php?content=user&id=' +
          data['message']['user_id'] + '\' > ' +
          data['message']['user'] + '</a></td>' +
          '<td>' + data['message']['username'] + '</td>' +
          '<td>' + data['message']['phone'] + '</td>' +
          '<td>' + data['message']['phone'] + '</td>' +
          '<td>' + moment(data['message']['updated_date']).format('M/D/YYYY h:mm:ss a') + '</td>' +
          '</tr>');
        $('#total-' + data['message']['table'] + ' span').empty().append($('#' + data['message']['table'] + '-users-modal table tbody tr').length +
          ' Users Last Updated: ' +
          moment().format('M/D/YYYY h:mm:ss a'));
        $('#message-usr-'+data['message']['emergency_response_id']).removeClass('validation');
        $('#message-usr-'+data['message']['emergency_response_id']).addClass(data['message']['class']);
        $('#message-usr-'+data['message']['emergency_response_id']).append(`<strong style="color: green">Validated: </strong>`+data['message']['validation_user']);
        // location.reload();

        let iconSrc = data['message']['mobile'] === '1' ? 'img/'+data['message']['icon']+'_mobile.png' : 'img/'+data['message']['icon']+'_pc.png';
        let idx = responseIds.findIndex(function (r) {
          return r.user === data['message']['user_id'];
        });
        if(idx !== -1) {
          responseIds[idx]['marker'].setIcon(iconSrc);
        }
        THINKINGMODAL.foundation('close');
      } else {
        THINKINGMODAL.foundation('close');
      }
    }).fail(function (data, textStatus, errorThrown) {
      console.log(data);
      console.log(textStatus);
      console.log(errorThrown);
    });
  });
}

$(document).ready(function () {

  getTimeline();
  setInterval(getTimeline, updateInterval);

  $('#alert-groups').submit(function () {
    $.ajax({
      dataType: 'json',
      url: 'process/alert_all_users.php',
      type: 'POST',
      data: $('#alert-groups').serialize(),
    }).done(function (data) {
      if (data.success) {
        $('#message').val('');
        //TODO: SUCCESS MESSAGE
      } else {
        //TODO: FAILED MESSAGE
      }
    }).fail(function (data, textStatus, errorThrown) {
      console.log(data);
      console.log(textStatus);
      console.log(errorThrown);
    });
    return false;
  });

  $('#broadcast-drill').submit(function (e) {
    e.preventDefault();
    $.ajax({
      dataType: 'json',
      url: 'process/broadcast_event.php',
      type: 'POST',
      data: $('#broadcast-drill').serialize(),
    }).done(function (data) {
      // console.log(data);
      if (data.success) {
        location.reload();
        //TODO: SUCCESS MESSAGE
      } else {
        //TODO: FAILED MESSAGE
      }
      $('#broadcast-drill-modal').foundation('close');
    }).fail(function (data, textStatus, errorThrown) {
      console.log(data);
      console.log(textStatus);
      console.log(errorThrown);
    });
    return false;
  });

  $('#close-drill').submit(function () {
    $.ajax({
      dataType: 'json',
      url: 'process/close_event.php',
      type: 'POST',
      data: $('#close-drill').serialize(),
    }).done(function (data) {
      // console.log(data);
      if (data.success) {
        window.location = 'dashboard.php?content=drills';
        //TODO: SUCCESS MESSAGE
      } else {
        //TODO: FAILED MESSAGE
      }
    }).fail(function (data, textStatus, errorThrown) {
      console.log(data);
      console.log(textStatus);
      console.log(errorThrown);
    });
    return false;
  });

  $('#select-broadcast-script').change(function () {
    $('#broadcast-description').text($('#select-broadcast-script option:selected').data('text'));
  });

  $('#select-close-script').change(function () {
    $('#close-description').text($('#select-close-script option:selected').data('text'));
  });
});