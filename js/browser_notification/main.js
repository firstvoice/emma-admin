$(document).foundation();

$(document).ready(function () {
    /*
    $('#login-form').submit(function (e) {
        e.preventDefault();
        $.post('process/login_jwt.php', $('#login-form').serialize(), function(data) {
            session.setJWT(data.JWT);
        }).fail(function () {
            alert('error');
        });
    });
    */
    $('[data-app-dashboard-toggle-shrink]').on('click', function (e) {
        e.preventDefault();
        $(this).parents('.app-dashboard').toggleClass('shrink-medium').toggleClass('shrink-large');
    });

    // $('#app-dashboard-sidebar').hover(function(e) {
    //     e.preventDefault();
    //     $(this).parents('.app-dashboard').toggleClass('shrink-medium').toggleClass('shrink-large');
    // })

    $('.create-event-form').submit(function (e) {
        e.preventDefault();
        var form = $(this);
        $.ajax({
            dataType: 'json',
            url: 'process/create_event.php',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            }
        })
            .done(function (data) {
                console.log(data);
                $('#thinking-modal').foundation('close');
                if (data.success) {
                    location.reload();
                    //TODO: SUCCESS MESSAGE
                } else {
                    //TODO: FAILED MESSAGE
                }
                //form.foundation('close');
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        return false;
    });
});