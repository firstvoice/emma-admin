'use strict';


self.addEventListener('push', function(event) {
    console.log('Received a push message', event);

    fetch('process/notification_callback.php?jwt=' + self.jwt).
    then(function(response) {
        var contentType = response.headers.get('content-type');
        if (contentType && contentType.includes('application/json')) {
            return response.json();
        }
        throw new TypeError('Oops, we haven\'t got JSON!');
    }).
    then(function(json) {
        console.log(json);
        if (json.success) {
            var title = 'Emergency Event!';
            var body = json.event['type_name'];
            var icon = 'img/emma_icon.png';
            var tag = 'simple-push-demo-notification-tag';

            //event.waitUntil(
            self.registration.showNotification(title, {
                body: body,
                icon: icon,
                tag: tag,
            });
            //);
        } else {

        }
    }).
    catch(function(error) {
        console.log(error);
    });
    /*
    $.ajax({
      dataType: 'json',
      url: 'process/notification_callback.php',
      type: 'POST',
      data: {'jwt': 'something'}
    }).done(function(data) {
      console.log(data);
      if (data.success) {
        var title = 'Emergency Event!';
        var body = JSON.stringify(data);
        var icon = 'img/emma_icon.png';
        var tag = 'simple-push-demo-notification-tag';

        event.waitUntil(
            self.registration.showNotification(title, {
              body: body,
              icon: icon,
              tag: tag
            })
        );
      } else {

      }
    }).fail(function(data, textStatus, errorThrown) {
      console.log(data);
      console.log(textStatus);
      console.log(errorThrown);
    });
  */
});

self.addEventListener('notificationclick', function(event) {
    console.log('On notification click: ', event.notification.tag);
    // Android doesn’t close the notification when you click on it
    // See: http://crbug.com/463146
    event.notification.close();

    // This looks to see if the current is already open and
    // focuses if it is
    event.waitUntil(clients.matchAll({
        type: 'window',
    }).then(function(clientList) {
        for (var i = 0; i < clientList.length; i++) {
            var client = clientList[i];
            if (client.url === '/' && 'focus' in client) {
                return client.focus();
            }
        }
        if (clients.openWindow) {
            return clients.openWindow('/');
        }
    }));
});

self.addEventListener('message', function(event) {
    var data = JSON.parse(event.data);
    //console.log('SW received message:');
    //console.log(data);
    self.uid = data.uid;
    self.jwt = data.jwt;
});
