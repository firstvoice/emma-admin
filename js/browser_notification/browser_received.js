//Configure Web credentials in your app and import the public and private key into a key pair.
// Add the public key generated from the console here.
messaging.usePublicVapidKey('BEcKNbZrtHmjRWkXg3aLijfAEZN_ldR_mlQj9NhJUTKL1ZP6c0futNhyZZJxokijtnH3Qr2pZTKIcKCwSNLzSsE');

//Request permission to receive notifications
messaging.requestPermission().then(function() {
    console.log('Notification permission granted.');
    // TODO(developer): Retrieve an Instance ID token for use with FCM.
    // ...
}).catch(function(err) {
    console.log('Unable to get permission to notify.', err);
});