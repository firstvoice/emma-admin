//check for serviceWorker on navigator (the client's current browser)
//check for PushManager on window (client browser supports PushManager)


if (!('serviceWorker' in navigator)) {
    // Service Worker isn't supported on this browser, disable or hide UI.
    return;
}

if (!('PushManager' in window)) {
    // Push isn't supported on this browser, disable or hide UI.
    return;
}

function registerServiceWorker() {
    return navigator.serviceWorker.register('service-worker.js')
        .then(function(registration) {
            console.log('Service worker successfully registered.');
            return registration;
        })
        .catch(function(err) {
            console.error('Unable to register service worker.', err);
        });
}
