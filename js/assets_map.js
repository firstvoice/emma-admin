var map;
aed_markers_array = []; //Array of added AED's (Showing the closest 10) we keep track of the markers so we can remove them when we drag the blue circle.
close_aed_array = [];//Close_aed_array is going to be filled with matching AEDS based on distance, but isn't the final array.
function geocodePosition(pos)
{
    //Set closest formatted address into input.
    geocoder = new google.maps.Geocoder();
    geocoder.geocode
    ({
            latLng: pos
        },
        function(results, status)
        {
            if (status == google.maps.GeocoderStatus.OK)
            {
                $('input[id="loc_address"]').val(results[0].formatted_address);
            }
            else
            {
                //$("#mapErrorMsg").html('Cannot determine address at this location.'+status).show(100);
            }
        }
    );
}

function populateAEDS(pos) {
    //positions is an array of all AED's, queried on user_home.php.
    for (let i = 0, len = positions.length; i < len; i++) {
        //Calculate the distance from the AED points vs our user centerPoint.
        if(arePointsNear(positions[i],pos,40074.274944)) //1.60934 km = 0.99999 miles, 4.82803 km is 3 miles.
        {
            //Create a new object (closePos) and add in the newly calculated distance.
            closePos = {lat: positions[i]['lat'], lng: positions[i]['lng'], location: positions[i]['location'], distance: last_distance, coordinator: positions[i].coordinator, contact: positions[i].contact, company: positions[i].company, address: positions[i].address};
            //push onto an array.
            close_aed_array.push(closePos);
            //last distance used.
            last_distance = 0;
        }
    }

    //This is the array of 10 AED's we will show user on map.
    aeds_being_added = [];


    //sort our AEDs by distance and get the first 10 (the closest).
    close_aed_array.sort((a,b) => (a.distance > b.distance) ? 1 : -1);
    inc_count = 0; //How many we have added to the array.
    for(let i = 0, len = close_aed_array.length; i < len; i++)
    {
        //Check for duplicates.
        const found = aeds_being_added.findIndex(aeds_being_added => aeds_being_added.distance === close_aed_array[i].distance);
        if(found == -1)
        {
            //Not a duplicate, go ahead and add.
            aeds_being_added.push(close_aed_array[i]);
            inc_count++;
            if(inc_count >= 10)
            {
                break;
            }
        }
    }



    //For each object in aeds_being_added, we add a pin.
    for(let i = 0, len = aeds_being_added.length; i < len; i++)
    {
        add_pin(aeds_being_added[i],"aed");
    }
    close_aed_array = []; //empty this array.




}
function arePointsNear(checkPoint, centerPoint, km) {
    var ky = 40000 / 360;
    var kx = Math.cos(Math.PI * centerPoint.lat / 180.0) * ky;
    var dx = Math.abs(centerPoint.lng - checkPoint.lng) * kx;
    var dy = Math.abs(centerPoint.lat - checkPoint.lat) * ky;
    if(Math.sqrt(dx * dx + dy * dy) <= km)
    {
        //console.log(dx * dx + dy * dy);
        last_distance = dx*dx+dy*dy;
        return true;
    }
    return false;
    //return Math.sqrt(dx * dx + dy * dy) <= km;
}
function add_pin(object,type)
{
    if(type === "aed") {
        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(object.lat, object.lng),
            map: map,
            title: object.name
        });
        aed_markers_array.push(marker);
        google.maps.event.addListener(marker, 'click', function() {
            let contact = '<p style="line-height: 20%;"></p>';
            let coordinator = '<p style="line-height: 20%;"></p>';
            let location = '<p style="line-height: 20%;"></p>';
            let company = '<p style="line-height: 20%;"></p>';
            let address = '<p style="line-height: 20%;"></p>';
            if(object.coordinator == null || object.coordinator === 'unknown' || object.coordinator === 'undefined' || object.coordinator === '' || object.coordinator === 'Unknown')
            {
                //nothing
            }
            else{
                coordinator = '<p style="line-height: 20%;">' + object.coordinator + '</p>';
            }
            if(object.location == null || object.location === '' || object.location === '*' || object.location === 'N/A' || object.location === 'unknown' || object.location === 'Unknown')
            {
                //nothing
            }
            else{
                location = '<p style="line-height: 20%;">' + object.location + '</p>';
            }
            //convert km to miles
            object.distance = (object.distance*0.62137).toFixed(2);
            tmp_dist = '<i style="color: red;">'+object.distance+' mi</i>';
            distance_text = '<p style="line-height: 20%;">Distance: '+tmp_dist+'</p>';
            if(object.contact == null || object.contact === 'unknown' || object.contact === 'undefined' || object.contact === '' || object.contact === 'Unknown')
            {
                //nothing
            }
            else{
                contact = '<p style="line-height: 20%;">Contact: ' + object.contact + '</p>';
            }
            if(object.company == null || object.company === 'unknown' || object.company === 'undefined' || object.company === '' || object.company === 'Unknown')
            {
                //nothing
            }
            else{
                company = '<p style="line-height: 20%;">'+object.company+'</p>'
            }
            if(object.address == null || object.address === 'unknown' || object.address === 'undefined' || object.address === '' || object.address === 'Unknown')
            {
                //nothing
            }
            else{
                address = '<p style="line-height: 20%;">'+object.address+'</p>';
            }
            let html = '<p style="line-height: 20%;"></p>' +
                company+
                coordinator +
                location +
                distance_text +
                contact +
                address +
                '';
            let infowindow = new google.maps.InfoWindow({
                content: html
            });
            infowindow.open(map, marker);
            map.setZoom(18);
            map.setCenter(marker.getPosition());
        });
        marker.setMap(map);
    }
}


function initMap() {

    var loc = {lat: parseFloat(mainEvent['latitude']), lng: parseFloat(mainEvent['longitude'])};
    map = new google.maps.Map(
        document.getElementById('asset-map'), {zoom: 12, center: loc});
    // var marker = new google.maps.Marker({position: loc, map: map, icon: 'img/red_mobile.png'});


    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            let image = new google.maps.MarkerImage(
                'img/bluedot_retina.png',
                null,
                null,
                new google.maps.Point(8, 8),
                new google.maps.Size(17, 17)
            );
            let marker = new google.maps.Marker({
                position: new google.maps.LatLng(pos.lat, pos.lng),
                icon: image,
                map: map,
                draggable: true,
                animation: google.maps.Animation.DROP,
                title: ' Your Location'
            });
            geocodePosition(marker.getPosition());
            google.maps.event.addListener(marker, 'mouseover', function(event){
                marker.setTitle(' ' + $('input[id="loc_address"]').val());
            });
            google.maps.event.addListener(marker, 'dragend', function (event) {
                //Get formatted address
                geocodePosition(marker.getPosition());
                //Update blue dot hover info marker text with formatted address.
                var pos = {
                    lat: event.latLng.lat(),
                    lng: event.latLng.lng()
                };
                //But first clear old markers.
                for (let i = 0; i < aed_markers_array.length; i++) {
                    aed_markers_array[i].setMap(null);
                }
                aed_markers_array = []; //empty here.
                //Update AED's / arrays with new positions.
                populateAEDS(pos);

            });
            map.setCenter(pos);
            map.setZoom(15);
            populateAEDS(pos);
        });

    }

    $.ajax({
        dataType: 'json',
        url: 'process/get_assets_map.php',
        type: 'GET',
        data: {'emma-plan-id': planid}
    })
        .done(function (data) {
            if (data.success) {
                var i;
                for (i = 0; i < data['assets'].length; i++)
                {
                    var icon = 'img/orange-dot.png';


                    if(data['assets'][i]['image']){
                        if(data['assets'][i]['image'] != 'img/orange-dot.png') {
                            icon = {
                                url: data['assets'][i]['image'],
                                scaledSize: new google.maps.Size(25, 25)
                            };
                        }
                    }

                        let marker = new google.maps.Marker({
                            position: {
                                lat: parseFloat(data['assets'][i]['latitude']),
                                lng: parseFloat(data['assets'][i]['longitude'])
                            },
                            icon: icon,
                            map: map,
                            title: data['assets'][i]['name'],
                        });
                        let c_name = '<p style="line-height: 20%;"></p>';
                        let address = '<p style="line-height: 20%;"></p>';
                    if(data['assets'][i]['name'] == null || data['assets'][i]['name'] === '')
                        {
                            //nothing
                        }
                        else{
                            c_name='<p style="lineheight: 20%;">'+data['assets'][i]['name']+'</p>';
                        }
                        if(data['assets'][i]['address'] == null)
                        {
                            //nothing
                        }
                        else{
                            address='<p style="lineheight: 20%;">'+data['assets'][i]['address']+'</p>';

                        }
                        let html = '' +
                        c_name +
                        address +
                        '';
                        google.maps.event.addListener(marker, 'click', function () {

                            //console.log(html);
                            let infowindow = new google.maps.InfoWindow({
                                content: html
                            });
                            infowindow.open(map, marker);
                            //map.setZoom(18);
                            map.setCenter(marker.getPosition());
                        });
                        marker.setMap(map);

                }
                
            } else {

            }
        })
        .fail(function (data, textStatus, errorThrown) {
            console.log(data);
            console.log(textStatus);
            console.log(errorThrown);
        });
    
}



$(document).ready(function() {


    initMap();


});