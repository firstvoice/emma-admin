
//Reloads Active Events/Drills every 5 seconds
setTimeout(function(){setInterval(function () {
    $('#mass-history').load('dashboard.php?content=mass_communication&id='+ massId +' #mass-history');
    // console.log('refresh');
}, 5000)}, 5000);
setTimeout(function(){setInterval(function () {
    $('#response-container').empty().load('dashboard.php?content=mass_communication&id='+ massId +' #response-container');
    // console.log('refresh');
}, 5000)}, 5000);




$(document).ready(function () {

    $('#rebroadcast-event').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        let groupCount = form.find('input.broadcast-group:checked').length;
        if(groupCount > 0) {
            $.ajax({
                dataType: 'json',
                url: 'process/broadcast_mass_communication.php',
                type: 'POST',
                data: $('#rebroadcast-event').serialize(),
                beforeSend: function () {
                    THINKINGMODAL.foundation('open');
                },
            }).done(function (data) {
                console.log(data);
                if (data.success) {
                    $('#success-modal').foundation('open');
                } else {
                    FAILMODAL.foundation('open');
                }
                $('#broadcast-notification-modal').foundation('close');
            }).fail(function (data, textStatus, errorThrown) {
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        } else {
            $('#broadcast-error').empty().append('<span style="color:red;">Groups Required</span>');
        }
        return false;
    });

    $('.select-broadcast-notification-script').change(function () {
        $('#broadcast-notification-text').text($(this).find('option:selected').val());
    });

    $('#mass-response-form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        $.ajax({
            dataType: 'json',
            url: 'process/respond_mass_communication.php',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function () {
                THINKINGMODAL.foundation('open');
            },
        }).done(function (data) {
            console.log(data);
            if (data.success) {
                $('#success-modal').foundation('open');
            } else {
                FAILMODAL.foundation('open');
            }
            $('#broadcast-notification-modal').foundation('close');
        }).fail(function (data, textStatus, errorThrown) {
            console.log(data);
            console.log(textStatus);
            console.log(errorThrown);
        });

        return false;
    });

});