$(document).ready(function () {
  let eventsTable = $('#securities-table').DataTable({
    "dom": 'Brtip',
    "paging": true,
    "pageLength": 20,
    "scrollCollapse": true,
    "processing": true,
    "serverSide": true,
    "deferRender": true,
      "order": [[2,'desc']],
      "ajax": {
      "url": "process/get_securities.php",
      "type": "GET",
      "data": {
        "emma-plan-id": user['emma_plan_id'],
      }
    },
    "columns": [
      {
        "data": "type",
        "render": function (data, type, row, meta) {
          return "<a href='dashboard.php?content=security&id=" + row['emma_security_id'] + "'>" + data + "</a>";
        }
      },
      {
          "data": "username",
          "render": function (data, type, row, meta) {
              return "<a href='dashboard.php?content=user&id=" + row['userid'] + "'>" + row['userName'] + "</a> (<a href='mailto:'"+ row['useremail'] +">"+ row['useremail'] +"</a>)"
          }
      },
      {
        "data": "active",
        "render": function (data, type, row, meta) {
          return row['active'] === '1' ? "<span style='color:green'>Active</span>" : "<span style='color:red'>Closed</span>";
        }
      },
      {"data": "created_date"},
      {
        "data": "call_log",
        "render": function (data, type, row, meta) {
          return row['call_log'] !== '0'
            ? "<a target='_blank' href='process/create_call_report.php?id="+row['call_log']+"'><i class='large fa fa-file-text'></i></a>"
            : "";
        }
      }
    ]
  });

  $('#securities-table thead th.text-search').each(function (i) {
    let title = $('#securities-table thead th').eq($(this).index()).text();
    $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
  });

  $(eventsTable.table().container()).on('keyup', 'thead input.text-search', function () {
    eventsTable
      .column($(this).data('index'))
      .search(this.value)
      .draw();
  });

});