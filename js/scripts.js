$(document).ready(function () {
    $('.edit-script-form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/edit_script.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                THINKINGMODAL.foundation('open');
            },
        })
            .done(function (data) {
                console.log(data);
                THINKINGMODAL.foundation('close');
                if (data.success) {
                    $('#success_modal').foundation('open');
                    $('#script-name-' + data['post']['emma-script-id']).text(data['post']['name']);
                } else {
                    FAILMODAL.foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        return false;
    });
    $('.clone-script-form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/clone_script.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                THINKINGMODAL.foundation('open');
            },
        })
            .done(function (data) {
                console.log(data);
                THINKINGMODAL.foundation('close');
                if (data.success) {
                    $('#success_modal').foundation('open');
                    $('#script-name-' + data['post']['emma-script-id']).text(data['post']['name']);
                } else {
                    FAILMODAL.foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        return false;
    });

    $('.delete-script').click(function (e) {
        e.preventDefault();
        $('#delete-script-id').val($(this).data('emma-script-id'));
        $('#delete-confirmation').foundation('open');
    });

    $('.edit-script').click(function (e) {
        e.preventDefault();
        $('#edit-script-id').val($(this).data('emma-script-id'));
        $('#edit-script-name').val($(this).data('emma-script-name'));
        $('#edit-script-text').val($(this).data('emma-script-text'));
        $('#edit-script-type option[value='+ $(this).data('emma-script-type-id') +']').attr('selected','selected');
        $('#edit-script-group option[value='+ $(this).data('group-type-id') +']').attr('selected','selected');
        $('#edit-broadcast-type option[value='+ $(this).data('broadcast-type-id') +']').attr('selected','selected');
        $('#edit-script-modal').foundation('open');
    });

    $('.clone-script').click(function (e) {
        e.preventDefault();
        $('#clone-script-id').val($(this).data('emma-script-id'));
        $('#clone-script-name').val($(this).data('emma-script-name'));
        $('#clone-script-text').val($(this).data('emma-script-text'));
        $('#clone-script-type option[value='+ $(this).data('emma-script-type-id') +']').attr('selected','selected');
        $('#clone-script-group option[value='+ $(this).data('group-type-id') +']').attr('selected','selected');
        $('#clone-broadcast-type option[value='+ $(this).data('broadcast-type-id') +']').attr('selected','selected');
        $('#clone-script-modal').foundation('open');
    });

    $('#delete-script-form').submit(function(e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/delete_script.php',
            type: 'POST',
            data: {'emma-script-id': $('#delete-script-id').val()},
            beforeSend: function () {
                THINKINGMODAL.foundation('open');
            },
        })
            .done(function (data) {
                console.log(data);
                THINKINGMODAL.foundation('close');
                if (data.success) {
                    $('#success_modal').foundation('open');
                    // location.reload();
                } else {
                    FAILMODAL.foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        return false;
    });

    $('#create-script-form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/create_script.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                THINKINGMODAL.foundation('open');
            },
        })
            .done(function (data) {
                console.log(data);
                THINKINGMODAL.foundation('close');
                if (data.success) {
                    // location.reload();
                    $('#success_modal').foundation('open');
                } else {
                    FAILMODAL.foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        return false;
    });

    $('#upload-script-form').submit(function(e){
        e.preventDefault();
        var fd = new FormData();
        fd.append('import-scripts-file', $('#import-scripts-file')[0].files[0]);
        $.ajax({
            dataType: 'json',
            url: 'process/read_script_csv_upload.php',
            type: 'POST',
            data: fd,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $('#confirm-import-script-form').css('display','inherit');

                //THINKINGMODAL.foundation('open');
            },
        })
            .done(function (data) {
                //console.log(data);
                //THINKINGMODAL.foundation('close');
                if (data.success) {
                    //data = JSON.parse(data);
                    //$('#success_modal').foundation('open');
                    //Append onto form
                    $('#upload-script-form').remove(); //Remove old form
                    let html_form = '<form id="confirm-import-script-form" class="expanded row"></form>';
                    $('#form-append-here').append(html_form);

                    console.log(data);

                    $('#append-table-data-here').css('display','inherit');


                        var i;
                        for (i = 0; i < data['item'].length; i++) {
                            if (data['item'] != null) {
                                $('#append-table-data-here > tbody:last-child').append('<tr>');


                                if(i===0) {
                                //Append an empty table row. OR IGNORE
                            }
                            else {
                                    console.log('started');
                                for (let z = 0; z <= 4; z++) {
                                    //Runs every loop, gets the inside
                                    //check if the text is equal to the text in the dropdown prefabs OR in the value of the dropdown prefabs.
                                    if(z===0)
                                    {
                                        found = false;
                                        //Script type
                                        $('#script-type-prefab option').each(function(){
                                            script_clone = $('#script-type-prefab').clone();
                                            if($(this).val() === data['item'][i][z] || $(this).text() === data['item'][i][z])
                                           {
                                               //match
                                               found = true;
                                               //let html = "<div class='large-2 columns'><input name=script[] style='text-align: center;' value='" + data['item'][i][z] + "'/></div>";
                                               //$('#confirm-import-script-form').append(html);
                                               script_clone.attr('name','script[]');
                                               script_clone.val($(this).val());
                                               $('#append-table-data-here tr:last-child').append('<td>');
                                               $('#append-table-data-here tr:last-child td:last-child').append(script_clone);
                                               $('#append-table-data-here tr:last-child').append('</td>');
                                           }
                                        });
                                           if(!found)
                                           {
                                                   //incorrect text, put in a dropdown value here instead
                                                   script_clone.attr('name','script[]');
                                                   //$('#confirm-import-script-form').append('<div class="large-2 columns" id="script-clone-append-here"></div>');
                                                   //(document).find('#script-clone-append-here').append(script_clone);


                                                   console.log("hit scripts");

                                                   $('#append-table-data-here tr:last-child').append('<td BGCOLOR="#FF0000;">');
                                                   $('#append-table-data-here tr:last-child td:last-child').append(script_clone);
                                                   $('#append-table-data-here tr:last-child').append('</td>');
                                               }


                                    }
                                    else if(z===1)
                                    {
                                        broadcast_clone = $('#broadcast-type-prefab').clone();
                                        //Broadcast type
                                        found = false;
                                        $('#broadcast-type-prefab option').each(function(){
                                            if($(this).val() === data['item'][i][z] || $(this).text() === data['item'][i][z])
                                            {
                                                //match
                                                broadcast_clone.val($(this).val());
                                                found = true;
                                                broadcast_clone.attr('name','broadcast[]');
                                                //$('#confirm-import-script-form').append('<div class="large-2 columns" id="broadcast-clone-append-here"></div>');
                                                $('#append-table-data-here tr:last-child').append('<td>');
                                                broadcast_clone.appendTo($('#append-table-data-here tr:last-child td:last-child'));
                                                $('#append-table-data-here tr:last-child').append('</td>');
                                            }
                                        });

                                        if(!found)
                                            {

                                                    console.log("hit broadcast");
                                                    //incorrect text, put in a dropdown value here instead
                                                    broadcast_clone.attr('name','broadcast[]');
                                                    //$('#confirm-import-script-form').append('<div class="large-2 columns" id="broadcast-clone-append-here"></div>');
                                                    $('#append-table-data-here tr:last-child').append('<td BGCOLOR="#FF0000;">');
                                                    broadcast_clone.appendTo($('#append-table-data-here tr:last-child td:last-child'));
                                                    $('#append-table-data-here tr:last-child').append('</td>');
                                                }

                                    }
                                    else if(z===2)
                                    {
                                        group_clone = $('#group-type-prefab').clone();
                                        found = false;
                                        //Group type
                                        $('#group-type-prefab option').each(function(){
                                            if($(this).val() === data['item'][i][z] || $(this).text() === data['item'][i][z])
                                            {
                                                //match
                                                found = true;
                                                group_clone.val($(this).val());
                                                group_clone.attr('name','group[]');
                                                //let html = "<div class='large-2 columns'><input style='text-align: center;' value='" + data['item'][i][z] + "'/></div>";
                                                $('#append-table-data-here tr:last-child').append('<td>');
                                                group_clone.appendTo($('#append-table-data-here tr:last-child td:last-child'));
                                                $('#append-table-data-here tr:last-child').append('</td>');
                                            }
                                        });
                                        if(!found)
                                            {

                                                    console.log("hit group");
                                                    //incorrect text, put in a dropdown value here instead
                                                    //$('#confirm-import-script-form').append('<div class="large-2 columns" id="group-clone-append-here"></div>');
                                                    group_clone.attr('name','group[]');
                                                    $('#append-table-data-here tr:last-child').append('<td BGCOLOR="#FF0000;">');
                                                    group_clone.appendTo($('#append-table-data-here tr:last-child td:last-child'));
                                                    $('#append-table-data-here tr:last-child').append('</td>');
                                                }

                                    }
                                    else if(z===3){
                                        //Other things that don't need to be checked (name / text)

                                        $('#append-table-data-here tr:last').append("<td>" + "<input required name='name[]' type='text' value='" + data['item'][i][z] + "'/>" + "</td>");
                                    }
                                    else if(z===4)
                                    {
                                        $('#append-table-data-here tr:last').append("<td>" + "<input required name='text[]' type='text' value='" + data['item'][i][z] + "'/>" + "</td>");

                                    }

                                }

                                }
                            }
                            $('#append-table-data-here tr:last').append('</tr>');


                        }

                    let button_html = '<div class="large-4 columns large-offset-2 button-group"> <a data-close="" class="button expanded alert hollow">Cancel</a> <input type="submit" class="button expanded" value="Submit"> </div>';
                    $('#confirm-import-script-form').append(button_html);




                } else {
                    console.log(data);
                    FAILMODAL.foundation('open');
                }

            })

            .fail(function (data, textStatus, errorThrown) {
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });



        return false;
    });



    $('#confirm-import-script-form').submit(function(e){

        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/submit_script_csv_upload.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                THINKINGMODAL.foundation('open');
            },
        })
            .done(function (data) {
                console.log(data);
                THINKINGMODAL.foundation('close');
                if (data.success) {
                    $('#success_modal').foundation('open');
                    location.reload();

                } else {
                    FAILMODAL.foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        return false;
    });

});