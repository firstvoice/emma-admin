$(document).ready(function () {
    $('#upload_usercsv_form').submit(function (e) {
        e.preventDefault();

        $.ajax({
            url: 'user_csv_upload_handler.php',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            }
        })
            .done(function (data) {
                console.log(data);
                $('#error-list').empty();
                $('#thinking-modal').foundation('close');
                var response = JSON.parse(data);
                if (response.success) {
                    $('#success_modal').foundation('open');
                } else {
                    for (var error in response['errors']) {
                        $('#error-list').append(response['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        return false;
    });
});