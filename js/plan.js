$(document).ready(function () {
  $('#edit-security-types-form').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
      dataType: 'json',
      url: 'process/edit_plan_security_types.php',
      type: 'POST',
      data: form.serialize(),
      beforeSend: function () {
        $('#thinking-modal').foundation('open');
      },
    })
      .done(function (data) {
        console.log(data);
        $('#thinking-modal').foundation('close');
        if (data.success) {
          $('#plan-success-modal').foundation('open');
          // location.reload();
          //TODO: SUCCESS MESSAGE
        } else {
          //TODO: FAILED MESSAGE
        }
        //form.foundation('close');
      })
      .fail(function (data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
    return false;
  });

  $('.event-type-link').click(function () {
    let typeId = $(this).data('type-id');
    let typeName = $(this).data('type-name');
    let imgFilename = $(this).data('img-filename');
    $('#event-type-name').empty().append(`${typeName} Type Details`);
    $('#sub-type-list').empty();
    $('#event-type-image').attr('src', `img/${imgFilename}`);
    $.ajax({
      dataType: 'json',
      url: 'process/get_event_sub_types.php',
      type: 'GET',
      data: {'type-id': typeId},
      beforeSend: function () {
        $('#thinking-modal').foundation('open');
      },
    })
      .done(function (data) {
        console.log(data);
        $('#thinking-modal').foundation('close');
        if (data.success) {
          $('#event-type-image').attr('src', data[''])
          for (let subType of data['sub-types']) {
            $('#sub-type-list').append(`<li>${subType['name']}</li>`);
          }
          $('#event-type-details-modal').foundation('open');
          //TODO: SUCCESS MESSAGE
        } else {
          //TODO: FAILED MESSAGE
        }
        //form.foundation('close');
      })
      .fail(function (data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
  });

  $('#default-noti-form').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
      dataType: 'json',
      url: 'process/edit_default_notifications.php',
      type: 'POST',
      data: form.serialize(),
      beforeSend: function () {
        $('#thinking-modal').foundation('open');
      },
    })
        .done(function (data) {
          console.log(data);
          $('#thinking-modal').foundation('close');
          if (data.success) {
            $('#plan-success-modal').foundation('open');
            // location.reload();
            //TODO: SUCCESS MESSAGE
          } else {
            //TODO: FAILED MESSAGE
          }
          //form.foundation('close');
        })
        .fail(function (data, textStatus, errorThrown) {
          console.log(data);
          console.log(textStatus);
          console.log(errorThrown);
        });
    return false;
  });
});