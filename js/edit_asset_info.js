
$(document).ready(function () {
    //These three arrays will be combined into one 2D array and then posted when we submit our form. They all hold OBJECTS.
    let update_array = new Array(); //All form information for fields we are updating (NOT REMOVING)
    let remove_array = new Array(); //All form information for fields we are removing.
    let unused_array = new Array(); //If there are duplicate values for Dropdown group names, we only need it once but we need to keep track of how many we have and their ID's.


    var eventsTable = $('#assets-table').DataTable({
        "dom": 'Brtip',
        "paging": true,
        "pageLength": 100,
        "scrollCollapse": true,
        "processing": true,
        "serverSide": true,
        "deferRender": true,
        "order": [[0,'asc']],
        "ajax": {
            "url": "process/get_assets_edit.php",
            "type": "GET",
            "data": {
                "emma-plan-id": user['emma_plan_id']
            }
        },
        "columns": [
            {
                "data": "name",
                "render": function(data, type, row, meta) {
                    return '<button data-asset-name ="'+ row['name'] +'" data-asset_id ="'+row['emma_asset_type_id']+'" data-image="'+row['image']+'" style="background-color: rgba(0,0,0,0);border-color: rgb(0, 0, 238);border-style: none;box-sizing: content-box;color: rgb(0, 0, 238); padding:0!important;font:inherit;cursor: pointer;" onclick="edit_asset(this);">' + row['name'] + '</button>';
                }
            },
        ]
    });



    $('#assets-table thead th.text-search').each(function (i) {
        let title = $('#assets-table thead th').eq($(this).index()).text();
        $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
    });

    $(eventsTable.table().container()).on('keyup', 'thead input.text-search', function () {
        eventsTable
            .column($(this).data('index'))
            .search(this.value)
            .draw();
    });




    $('#create-asset-info-form').submit(function () {
        let image = $('#create-asset-info-form').find("div.icon.selected img").attr('src');
        form = $('#create-asset-info-form').serializeArray();
        //Add all DropDowns into a object and attach to form for posting.
        dropdownCount = 0;
        //array(s) of object(s)
        let dropdowns = new Array();
        let dropdownOptions = new Array();
        $("div[id^='dropdown-prebuilt']").each(function(index){
            //Get all dropdown-prebuilt divs.
            dropdownCount++;
            let obj = {name: 'headers', value: $(this).find('.dropdownName').val(), count: dropdownCount};
            dropdowns.push(obj);
            $(this).find('.dropdownOption').each(function(){
                let objOption = {name: 'options', value: $(this).val(), count: dropdownCount};
                dropdownOptions.push(objOption);
            });
        });



        $.ajax({
            dataType: 'json',
            url: 'process/create_asset_type.php',
            type: 'POST',
            data: $.param(form) + '&Headers=' + JSON.stringify(dropdowns) + '&Options=' + JSON.stringify(dropdownOptions) + '&Image=' + image,
            beforeSend: function () {
                //attach to our form which is now an array containing all form information.
                form.push(dropdowns);
                form.push(dropdownOptions);
                THINKINGMODAL.foundation('open');
            },
        })
            .done(function (data) {
                //console.log(data);
                THINKINGMODAL.foundation('close');
                if (data.success) {
                    $('#success-create-modal').foundation('open');
                } else {
                    FAILMODAL.foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                // console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        return false;
    });


    $(document).on('click','#addDate', function(e){
        e.preventDefault();
        clone = $('#date-prebuilt').clone();
        clone.css('display','inherit');
        $('#insert-area').append(clone);


    });
    $(document).on('click','#addFillin', function(e){
        e.preventDefault();
        clone = $('#fillin-prebuilt').clone();
        clone.css('display','inherit');
        $('#insert-area').append(clone);

    });
    $(document).on('click','#addDropdown', function(e){
        e.preventDefault();
        clone = $('#dropdown-prebuilt').clone();
        clone.css('display','inherit');
        $('#insert-area').append(clone);
    });
    $(document).on('click','#addDateEdit', function(e){
        e.preventDefault();
        clone = $('#date-prebuilt').clone();
        clone.css('display','inherit');
        clone.find("input").attr('id','date');
        $('#insert-area-edit').append(clone);


    });
    $(document).on('click','#addFillinEdit', function(e){
        e.preventDefault();
        clone = $('#fillin-prebuilt').clone();
        clone.css('display','inherit');
        clone.find("input").attr('id','fillin');
        $('#insert-area-edit').append(clone);

    });
    data_count = 0;
    $(document).on('click','#addDropdownEdit', function(e){
        e.preventDefault();
        data_count++;
        clone = $('#dropdown-prebuilt').clone();
        clone.css('display','inherit');
        clone.attr('id','dropdowninsert'); //Change the ID tag so when we submit its recognized.
        clone.attr('data-count',data_count); //Assign data tag that increments so we can keep track of who goes where when we post.
        $('#insert-area-edit').append(clone);
        data_count++;
    });
    $(document).on('click','#delete', function(e) {
        e.preventDefault();
        button = $(e.target);
        button.closest('.row').remove();
    });
    $(document).on('click','#CancelEdit', function(e) {
        e.preventDefault();
        $('#edit-asset-modal').foundation('close');
    });

    $('#deleteAsset').click(function(){
        $('#check-delete-modal').foundation('open');
    });
    $('#confirm_delete').click(function(){
        asset_id = $('#asset_type_id').val();
        $.ajax({
            dataType: 'json',
            url: 'process/delete_asset_type.php',
            type: 'POST',
            data: {'asset-id':asset_id},
            beforeSend: function(){
                console.log("BEFORE SEND");
                $('#thinking-modal').foundation('open');
            },
        })
            .done(function (data) {
                console.log(data);
                $('#thinking-modal').foundation('close');
                if(data.success)
                {
                    $('#success-create-modal').foundation('open');
                }

            })
            .fail(function (data, textStatus, errorThrown) {
                $('#fail-modal').foundation('open');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
    });

    add_option = function(input)
    {
        //Find the closest row, and then find the input dropdownName and grab data-count
        row = $(input).closest('.row');
        amt = row.attr('data-count');
        var html = '<div class="row large-12 columns"><div class="large-1 columns large-offset-1"><button onclick="delete_option_entry(this);" type="button" id="deleteEdit" class="button radius" style="background-color: red;"><i style="color: white;" class="fa fa-times"></i></button></div><div class="large-6 columns"><input required name="option[]" data-count='+amt+' class="dropdownOption" value=""/></div></div>';
        $(input).closest('.row').append(html);
    };
    add_option_edit = function(input)
    {
        var html = '<div class="row large-12 columns"><div class="large-1 columns large-offset-1"><button onclick="delete_option_entry(this);" type="button" id="deleteEdit" class="button radius" style="background-color: red;"><i style="color: white;" class="fa fa-times"></i></button></div><div class="large-6 columns"><input name="option[]" data-parentid="'+$(input).attr('data-parentid')+'" required class="dropdownOption" value=""/></div></div>';
        $(input).closest('.row').append(html);
    };

    delete_input_entry = function(input) {
        var button = $(input);
        //let data_count = button.attr('data-asset');
        var delFillin = {name: 'fillin',value: 'fillin', id: button.attr('data-id')};
        remove_array.push(delFillin);
        button.closest('.row').remove();
    };

    delete_option_entry = function(input) {
        var button = $(input);
        //let data_count = button.attr('data-asset');
        var delOption = {name: 'option',value: 'option', parentID: button.attr('data-parentid'), id: button.attr('data-optionid')};
        remove_array.push(delOption);
        button.closest('.row').remove();



    };
    delete_dropdown_entry = function(input)
    {
        var button = $(input);
        let data_count = button.attr('data-asset');
        parent = button.closest('.row');

        $('#dropdownLabel' + data_count).remove();


        //Search unused_array for matching id based on input(id) and then remove it and add it to remove_array.
        arr = jQuery.grep(unused_array, function(n){
            return(n['value'] == button.attr('data-originalname'));
        });
        remove_array.push(arr); //We are removing these, so we push them to here.

        //Foreach child object, check if its a dropdown entry because we need to add to remove_array..
        parent.find('.dropdownOption').each(function(){
            var obj = {name:'option', value:'option', id: $(this).attr('data-dropdownid')};
            remove_array.push(obj);
        });

        parent.remove();


    };
    delete_dropdown_create = function(input)
    {
        var button = $(input);
        button.closest('.row').remove();
    };


    edit_asset = function(button) {
        //var POST_array = new Array(3) //Reset array that we will post. Holds NeedsUpdated, NeedsRemoved, and NeedsAdded.
        //for (i=0;i<3;i++){POST_array[i]=new Array(3)}

        //Empty our arrays.
        update_array = new Array();
        remove_array = new Array();
        add_array = new Array();

        let count = 0; //Count of how many we generate
        var type_id = button.getAttribute("data-asset_id");
        $('#asset_type_id').val(type_id);
        var asset_name = button.getAttribute("data-asset-name");
        $('#assetName').val(asset_name);
        let image_data = button.getAttribute("data-image");

        //Do an ajax call to get associated items.
        $.ajax({
            url: 'process/get_asset_items.php',
            type: 'GET',
            data: {'type_id': type_id},
            beforeSend: function(){
                $('#insert-area-edit').empty(); //Empty this area so duplicate values don't appear.
            },
        })
            .done(function (data) {
                data = JSON.parse(data);
                //console.log(data);
                //Foreach array in data (associative array) we check to see if it has
                //a date, a dropdown, or a fillin. Then we append blank versions of those
                //with X buttons that can remove them.



                //Find the my-icon-select-edit div, and find the current div child with class 'icon select' and change the class
                //to just 'icon' then find the img src (with matching source to imagevar) to icon select
                //then find the div child selected-icon and get the img child of that and change it to the src of the image var

                //If it is null, then it is not set and will need to be set to the default icon (first one pulled out of DB)
                if(image_data !== 'null') {
                    $('#my-icon-select-edit').find('.icon.selected').attr('class', 'icon');
                    $('#my-icon-select-edit').find($('#my-icon-select-box-scroll')).find('img[src="' + image_data + '"]').parent().attr('class', 'icon selected');
                    $('#my-icon-select-edit').find('.selected-icon').find('img').attr('src', image_data);
                }
                else{
                    //Need to do similar things to the above statement, so we can revert back (otherwise it'll
                    //stay changed when we edit multiple assets)
                    $('#my-icon-select-edit').find('.icon.selected').attr('class', 'icon');
                    $('#my-icon-select-edit').find($('#my-icon-select-box-scroll')).find('img[src="img/orange-dot.png"]').parent().attr('class', 'icon selected');
                    $('#my-icon-select-edit').find('.selected-icon').find('img').attr('src', 'img/orange-dot.png');
                }


                var i;
                if(data['item'] != null) { //If it has stuff to pulldown.
                    for (i = 0; i < data['item'].length; i++) {
                        if (data['item'][i]['date'] == '1') {
                            html = '<div class="row large-12 columns"><div class="large-1 columns"><button id="deleteEdit" onclick="delete_input_entry(this);" data-id="' + data['item'][i]['id'] + '" type="button" data-asset="' + count + '" class="button radius" style="background-color: red;"><i style="color: white;" class="fa fa-times"></i></button></div><div class="large-6 columns"><label>' + data['item'][i]['name'] + " " + '</label><input data-id="' + data['item'][i]['id'] + '" data-count="' + count + '" class="entry" value="' + data['item'][i]['name'] + '" type="text" id="date" placeholder="Enter a name for what Date this will represent"/></div></div>';
                            $('#insert-area-edit').append(html);
                            count++;
                        }
                        if (data['item'][i]['fillin'] == '1') {
                            html = '<div class="row large-12 columns"><div class="large-1 columns"><button data-asset="' + count + '" onclick="delete_input_entry(this);" data-id="' + data['item'][i]['id'] + '" type="button" id="deleteEdit" class="button radius" style="background-color: red;"><i style="color: white;" class="fa fa-times"></i></button></div><div class="large-6 columns"><label>Item Description</label><input data-id="' + data['item'][i]['id'] + '" data-count="' + count + '" type="text" id="fillin" placeholder="Item name" class="entry" value="' + data['item'][i]['name'] + '" placeholder="Enter a name for what Date this is"/></div></div>';
                            $('#insert-area-edit').append(html);
                            count++;
                        }
                        if (data['item'][i]['dropdown_id'] != null && data['item'][i]['dropdown_id'] != 0) {
                            var unusedObj = {
                                name: 'dropdown',
                                value: data['item'][i]['name'],
                                id: data['item'][i]['dropdown_id'],
                                parentid: data['item'][i]['id']
                            };
                            unused_array.push(unusedObj);
                            //If the id="insert-area-edit" class="dropdownName" has the name value that already exists, then just add option to it.
                            //If it doesn't, then add in a dropdown.
                            if ($('.dropdownName[value="' + data['item'][i]['name'] + '"]')[0]) {
                                //Exists
                                option = '<div class="row large-12 columns"><div class="large-1 columns large-offset-1"><button data-parentID="' + data['item'][i]['id'] + '" data-optionID="' + data['item'][i]['dropdown_id'] + '" data-asset="' + count + '" onclick="delete_option_entry(this);" type="button" id="deleteEdit" class="button radius" style="background-color: red;"><i style="color: white;" class="fa fa-times"></i></button></div><div class="large-6 columns"><input data-parentID="' + data['item'][i]['id'] + '" class="dropdownOption" data-asset="' + count + '" data-dropdownid="' + data['item'][i]['dropdown_id'] + '" value="' + data['item'][i]['dropdown_name'] + '"/></div></div>';
                                $('#dropdowninsert[data-dropdown="' + data["item"][i]["name"] + '"]').append(option);
                                count++;
                            }
                            else {
                                //Doesn't exist
                                html = '<label id="dropdownLabel' + count + '"> Dropdown Options</label><div class="row large-12 columns" id="dropdowninsert" data-dropdown="' + data["item"][i]["name"] + '" data-id="' + data["item"][i]["id"] + '"><div class="large-1 columns"><button data-asset="' + count + '" onclick="delete_dropdown_entry(this);" data-originalname="' + data['item'][i]['name'] + '" data-dropdownid="' + data['item'][i]['id'] + '" type="button" id="deleteEdit" class="button radius" style="background-color: red;"><i style="color: white;" class="fa fa-times"></i></button></div><div class="large-4 columns"><input required data-id="' + data['item'][i]['id'] + '" data-originalname="' + data['item'][i]['name'] + '" class="dropdownName" value="' + data['item'][i]['name'] + '"/></div><div class="large-1 columns"><button type="button" data-parentid="' + data['item'][i]['id'] + '" onclick="add_option_edit(this);" class="small button"><i class="fa fa-plus"></i></button></div></div>';
                                option = '<div class="row large-12 columns"><div class="large-1 columns large-offset-1"><button data-asset="' + count + '" data-parentID="' + data['item'][i]['id'] + '" data-optionID="' + data['item'][i]['dropdown_id'] + '"  onclick="delete_option_entry(this);" type="button" id="deleteEdit" class="button radius" style="background-color: red;"><i style="color: white;" class="fa fa-times"></i></button></div><div class="large-6 columns"><input data-parentID="' + data['item'][i]['id'] + '" class="dropdownOption" data-dropdownid="' + data['item'][i]['dropdown_id'] + '" value="' + data['item'][i]['dropdown_name'] + '"/></div></div>';
                                $('#insert-area-edit').append(html);
                                $('#dropdowninsert[data-dropdown="' + data["item"][i]["name"] + '"]').append(option); //still have to append here on first iteration.
                                count++;
                            }

                        }

                    }
                }





                $('#edit-asset-modal').foundation('open');



                //    else{
                //     echo '<div class="row">';
                //     echo '<div class="large-1 columns">';
                //     echo '<button id="delete" class="button radius" style="background-color: red;"><i style="color: white;" class="fa fa-times"></i></button>';
                //      echo '</div>';
                //      echo '<div class="large-1 columns">';
                //         echo '<select name="dropdown[]">';
                //        $dropdownOptions = $fvmdb->query("
                //        SELECT eado.*, eat.name
                //        FROM emma_assets_dropdown_options as eado
                //        INNER JOIN emma_asset_types as eat on eado.asset_type_id = eat.emma_asset_type_id
                //        WHERE eado.asset_type_id = ".$type['emma_asset_type_id']."
                //       ");
                //       while($dropdownOption = $dropdownOptions->fetch_assoc()){
                //               echo '<option value="'.$dropdownOption['name'].'">' . $dropdownOption['name'] . '</option>';
                //           };
                //      echo '</select>';
                //      echo '</div>';
                //   echo '</div>';
                //  }
            })
            .fail(function (data, textStatus, errorThrown) {
                //console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        return false;
    };



    $('#edit-asset-form').submit(function(e){
        e.preventDefault();


        let imagesrc = $('#edit-asset-form').find("div.icon.selected img").attr('src');



        edit_form = $('#edit-asset-form').serializeArray();
        let dropdowns = new Array();
        let dropdownOptions = new Array();








        //build one of our arrays.
        $("input[id^='fillin']").each(function(index){
            id = $(this).attr("data-id");
            text = $(this).val();
            var object={name: 'fillin', value: text, id: id};
            update_array.push(object);
        });
        $("input[id^='date']").each(function(index){
            id = $(this).attr("data-id");
            text = $(this).val();
            var object={name: 'date', value: text, id: id};
            update_array.push(object);
        });
        $("div[id^='dropdowninsert']").each(function(index){
            //Get all dropdown-prebuilt divs.
            if($(this).attr('data-count') == null || $(this).attr('data-count') == 'undefined')
            {
                //It's a old dropdown that already existed.
                let obj = {name: 'headers', value: $(this).find('.dropdownName').val(), id: $(this).attr('data-id'), originalname: $(this).attr('data-dropdown')};
                dropdowns.push(obj);
            }
            else{
                //Its a new one.
                let obj = {name: 'headers', value: $(this).find('.dropdownName').val(), childOption: $(this).attr('data-count')};
                dropdowns.push(obj);
            }

            $(this).find('.dropdownOption').each(function(){
                if($(this).attr('data-count') == null || $(this).attr('data-count') == 'undefined')
                {
                    //This means it was PRE-EXISTING, and it was pulled from the database and added.
                    let objOption = {name: 'options', value: $(this).val(), id: $(this).attr('data-dropdownid'), parentid: $(this).attr('data-parentID')};
                    dropdownOptions.push(objOption);
                }
                else{
                    //This means the user clicked the '+ Dropdown' button and ADDED a new one.
                    let objOption = {name: 'newOption', value: $(this).val(), parentHeader: $(this).attr('data-count')};
                    dropdownOptions.push(objOption);
                }

            });
        });



        edit_form.push(update_array);
        edit_form.push(remove_array);
        edit_form.push(dropdowns);
        edit_form.push(dropdownOptions);
        edit_form.push(unused_array);


        $.ajax({
            dataType: 'json',
            url: 'process/edit_asset_type.php',
            type: 'POST',
            data: $.param(edit_form) + '&Headers=' + JSON.stringify(dropdowns) + '&Options=' + JSON.stringify(dropdownOptions) + '&Remove=' + JSON.stringify(remove_array) + '&Update=' + JSON.stringify(update_array) + '&Unused=' + JSON.stringify(unused_array) + '&Image=' + imagesrc,
            beforeSend: function () {
                THINKINGMODAL.foundation('open');
            },
        })
            .done(function (data) {
                console.log(data);
                remove_array = new Array();
                unused_array = new Array();
                update_array = new Array();

                THINKINGMODAL.foundation('close');
                if (data.success) {
                    $('#success-create-modal').foundation('open');
                } else {
                    FAILMODAL.foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                // console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        return false;
    });




});

