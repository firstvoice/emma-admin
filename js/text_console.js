var rate = 1000;
var numbers;
var message;
var loop;
var c = 0;
$(function() {
    $("#rate").keyup(function() {
        rate = $(this).val();
    })
    $("#send").click(function() {
        c = 0;
        numbers = $("#numbers").val().trim().split("\n");
        message = $("#message").val();
        send();
        console.log(numbers);
    })
})
function send() {
    var n = numbers[c].trim();
    $.ajax({
        type: "POST",
        url: "https://igotcut.com",
        data: { number: n, message:  message },
        success: function(res) {
            console.log(res);
            log("Success : message '" + message + "'' delivered to " + n);
            c++;
            if(c < numbers.length){
                setTimeout(send, rate);
            }
        },
        error: function (res) {
            log("Failure : " + res.message);
        }
    });
}
function log(msg) {
    $("#log").append($("<p></p>").html(msg));
}