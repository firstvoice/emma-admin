$(document).ready(function () {
    console.log(user['id']);
    var usersTable = $('#users-table').DataTable({
        'dom': 'Brtip',
        'paging': true,
        'pageLength': 50,
        'scrollCollapse': true,
        'processing': true,
        'serverSide': true,
        'deferRender': true,
        "initComplete": function(settings, json) {
            removeEMMA('#users-table');
        },
        "order": [[0,'asc']],
        'ajax': {
            'url': 'process/get_users_admin.php',
            'type': 'GET',
            'data': {
                'userid': user['id'],
            },
        },
        'columns': [
            {
                'data': 'username',
                'render': function (data, type, row, meta) {
                    return '<a href=\'dashboard.php?content=user&id=' + row['id'] + '\'>' +
                        data + '</a>';
                },
            },
            {'data': 'firstname'},
            {'data': 'lastname'},
            {
                'data': 'group',
                'orderable': false

            },{
                'data': 'plan',
                'orderable': false

            },
        ],
    });

    $('#users-table tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
        $(this).attr('data-value', $(this).find('a').text());
    });

    $('#delete-users').click( function () {
        var userarr = '';
        $('#users-table tbody .selected').each(function() {
            if(userarr === ''){
                userarr = $(this).data('value');
            }else {
                userarr = userarr + ',' + $(this).data('value');
            }
        });
        if(userarr !== ''){
            console.log(userarr);
            $.ajax({
                dataType: 'json',
                url: 'process/delete_users.php',
                type: 'POST',
                data: {'usernames': userarr},
                beforeSend: function () {
                    $('#thinking-modal').foundation('open');
                },
            })
                .done(function (data) {
                    console.log(data);
                    $('#thinking-modal').foundation('close');
                    $('#error-list').empty();
                    if (data.success) {
                        $('#success_modal').foundation('open');
                    } else {
                        for (var error in data['errors']) {
                            $('#error-list').append(data['errors'][error] + '<br />');
                        }
                        $('#fail-modal').foundation('open');
                    }
                })
                .fail(function (data, textStatus, errorThrown) {
                    $('#thinking-modal').foundation('close');
                    console.log(data);
                    console.log(textStatus);
                    console.log(errorThrown);
                });
        }else {
            alert('Please Select Users');
        }
    } );

    $('#users-table thead th.text-search').each(function (i) {
        let title = $('#users-table thead th').eq($(this).index()).text();
        $(this)
            .html(
                '<input class="text-search" type="text" placeholder="Search ' + title +
                '" data-index="' + i + '" />');
    });

    $(usersTable.table().container())
        .on('keyup', 'thead input.text-search', function () {
            usersTable
                .column($(this).data('index'))
                .search(this.value)
                .draw();
        });



    $('#user_assign_group').click(function () {
        $('#update_group_modal').foundation('open');
    });

    $('#assign_group_submit').click(function () {
        $.ajax({
            dataType: 'json',
            url: 'process/assign_user_list_to_group.php',
            type: 'POST',
            data: $('#users_form').serialize() + '&' +
                $('#assign_group_form').serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            },
        })
            .done(function (data) {
                console.log(data);
                $('#thinking-modal').foundation('close');
                $('#error-list').empty();
                if (data.success) {
                    $('#success_modal').foundation('open');
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
    });

    $('#user_remove_group').click(function () {
        $('#remove_group_modal').foundation('open');
    });

    $('#remove_group_submit').click(function () {
        $.ajax({
            dataType: 'json',
            url: 'process/remove_user_list_from_groups.php',
            type: 'POST',
            data: $('#users_form').serialize() + '&' +
                $('#remove_group_form').serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            },
        })
            .done(function (data) {
                console.log(data);
                $('#thinking-modal').foundation('close');
                $('#error-list').empty();
                if (data.success) {
                    $('#success_modal').foundation('open');
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
    });

    // $('#user_assign_site').click( function() {
    //     $('#update_site_modal').foundation('open');
    // });
    //
    //
    // $('#assign_site_submit').click( function() {
    //     $.ajax({
    //         dataType: 'json',
    //         url: 'process/assign_user_list_to_site.php',
    //         type: 'POST',
    //         data: $('#users_form').serialize() + "&" + "site_id=" + $("#site_id").val(),
    //         beforeSend: function () {
    //             $('#thinking-modal').foundation('open');
    //         }
    //     })
    //         .done(function (data) {
    //             console.log(data);
    //             $('#thinking-modal').foundation('close');
    //             $('#error-list').empty();
    //             if (data.success) {
    //                 $('#success_modal').foundation('open');
    //             } else {
    //                 for (var error in data['errors']) {
    //                     $('#error-list').append(data['errors'][error] + '<br />');
    //                 }
    //                 $('#fail-modal').foundation('open');
    //             }
    //         })
    //         .fail(function (data, textStatus, errorThrown) {
    //             $('#thinking-modal').foundation('close');
    //             console.log(data);
    //             console.log(textStatus);
    //             console.log(errorThrown);
    //         });
    // });

    $('#user_deactivate').click(function () {
        $.ajax({
            dataType: 'json',
            url: 'process/deactivate_user_list.php',
            type: 'POST',
            data: $('#users_form').serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            },
        })
            .done(function (data) {
                console.log(data);
                $('#thinking-modal').foundation('close');
                $('#error-list').empty();
                if (data.success) {
                    $('#success_modal').foundation('open');
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
    });



    var setupUsersTable = $('#setup-users-table').DataTable({
        'dom': 'Brtip',
        'paging': true,
        'pageLength': 200,
        'scrollCollapse': true,
        'processing': true,
        'serverSide': true,
        'deferRender': true,
        "initComplete": function(settings, json) {
            removeEMMA('#setup-users-table');
        },
        "order": [[0,'asc']],
        'ajax': {
            'url': 'process/get_setup_users.php',
            'type': 'GET',
            'data': {
                'emma-plan-id': user['emma_plan_id'],
            },
        },
        'columns': [
            {
                'data': 'username',
                'render': function (data, type, row, meta) {
                    return '<a href=\'dashboard.php?content=user&id=' + row['id'] + '\'>' +
                        data + '</a>';
                },
            },
            {'data': 'firstname'},
            {'data': 'lastname'},
        ],
    });

    $('#setup-users-table thead th.text-search').each(function (i) {
        let title = $('#setup-users-table thead th').eq($(this).index()).text();
        $(this)
            .html(
                '<input class="text-search" type="text" placeholder="Search ' + title +
                '" data-index="' + i + '" />');
    });

    $(setupUsersTable.table().container())
        .on('keyup', 'thead input.text-search', function () {
            setupUsersTable
                .column($(this).data('index'))
                .search(this.value)
                .draw();
        });

    function removeEMMA(table){
        if(!user['username'].includes('think-safe.com') || !user['username'].includes('emmauser.com')){
            $(table + ' tr').each(function() {
                // console.log(this);
                username = $(this).find('td:first').find('a').text();
                // console.log(username);
                if(username.includes('think-safe.com') || username.includes('emmauser.com')){
                    $(this).hide();
                }
            });
        }
    }

});