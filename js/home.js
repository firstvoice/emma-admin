
//Opens view modal for various types
function viewEvent(type) {
    $('#view-'+type+'-modal').foundation('open');
}

//Reloads Active Events/Drills every 5 seconds
setInterval(function () {
   $('#active-events-drills').load('dashboard.php?content=home #active-events-drills', function(){
       $('#open-help').click(function () {
           viewEvent('help')
       });

       $('#open-security').click(function () {
           viewEvent('security')
       });

       $('#open-sos').click(function () {
           viewEvent('sos')
       });
   });
   // console.log('reload');
}, 5000);


//---------------Feed Code-----------------

setInterval(function () {
    $('#feed-center').load('dashboard.php?content=home #live-feed');
}, 5000);

$('#feed-length').change(function (e) {
    var length = $('#feed-length option:selected').val();
    e.preventDefault();
    $.ajax({
        dataType: 'json',
        url: 'process/update_feed_length.php',
        type: 'POST',
        data: {'length': length}
    })
        .done(function (data) {
            if (data.success) {
                $('#feed-center').load('dashboard.php?content=home #live-feed');
            } else {

            }
        })
        .fail(function (data, textStatus, errorThrown) {

        });
})

//----------------------------------------



$(document).ready(function () {

    // google.charts.load('current', {packages: ["corechart"]});
    // google.charts.setOnLoadCallback(initCharts);
    // $(window).resize(drawAllCharts);
    //

    $(".list_expandable").click(function () {
        let list = $(this).siblings('.list').first();
        let element_id = list.attr('id');
        let target = document.getElementById(element_id);
        if(target.classList.contains('hidden')){
            target.classList.remove('hidden');
        }
        else{
            target.classList.add('hidden');
        }
    });


    $('#open-help').click(function () {
        viewEvent('help')
    });

    $('#open-security').click(function () {
        viewEvent('security')
    });

    $('#open-sos').click(function () {
        viewEvent('sos')
    });

    var open = false;
    $('#plans-container').hide();
    $('#show-plans').click(function () {
        if(open === false){
            console.log(open);
            $('#plans-container').show(300);
            $('#show-plans-arrow').removeClass('fa fa-angle-double-down');
            $('#show-plans-arrow').addClass('fa fa-angle-double-up');
            open = true;
        }else {
            console.log(open);
            $('#plans-container').hide(300);
            $('#show-plans-arrow').removeClass('fa fa-angle-double-up');
            $('#show-plans-arrow').addClass('fa fa-angle-double-down');
            open = false;
        }
    });

    function checkPlan(name){
        $('#'+name+'-select').click()
    }


    $('.plan-select').click(function (e) {
        e.preventDefault();
        console.log('clicked');
        $.ajax({
            dataType: 'json',
            url: 'process/set_home_plans.php',
            type: 'GET',
            data: {'id': $(this).data('id')}
        })
            .done(function (data) {
                 console.log(data, "data sent after hit the success button");
                if (data.success) {
                    location.reload();
                } else {
                    console.log('error')
                }
            })
            .fail(function (data, textStatus, errorThrown) {

            });
    });

    $('#default-plan-select').click(function (e) {
        e.preventDefault();
        console.log('clicked');
        var defaultarr = [];
        $('.plan-select:checkbox:checked').each(function () {
            defaultarr.push($(this).attr('name'));
        })
        console.log(defaultarr);
        $.ajax({
            dataType: 'json',
            url: 'process/set_default_plans.php',
            type: 'GET',
            data: {'plans': defaultarr}
        })
            .done(function (data) {
                 console.log(data, "data sent after hit the success button");
                if (data.success) {
                    location.reload();
                } else {
                    console.log('error')
                }
            })
            .fail(function (data, textStatus, errorThrown) {

            });
    });

    $('#plan-all-select').click(function (e) {
        e.preventDefault();
        var checked;
        // console.log('clicked');
        if($('#plan-all-select').is(':checked')){
            checked = 'all';
        }else {
            checked = 'none';
        }
        $.ajax({
            dataType: 'json',
            url: 'process/set_home_plans.php',
            type: 'GET',
            data: {'id': checked}
        })
            .done(function (data) {
                console.log(data, "data sent after hit the success button");
                if (data.success) {
                    location.reload();
                } else {

                }
            })
            .fail(function (data, textStatus, errorThrown) {

            });
    });

});
