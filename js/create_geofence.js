

$(document).ready(function () {
    $('#create-geofence-form').submit(function(e) {
        e.preventDefault();

        $.ajax({
            dataType: 'json',
            url: 'process/create_geofence.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            }
        })
            .done(function (data) {
                console.log(data);
                $('#error-list').empty();
                $('#thinking-modal').foundation('close');
                if (data.success) {
                    $('#success_modal').foundation('open');
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        return false;
    });
    $('#dropdown_type').change(function(){
        if($('#dropdown_type').val() == 'Circle')
        {
            //Append circle information.
            $('#rectangle_first_append_here').empty();//But first clear the append area
            $('#rectangle_second_append_here').empty();//But first clear the append area
            html = '<div class="large-12 medium-12 small-12 column"> <label class="text-center"><b>Center Coordinates</b></label> <div class="row expanded"> <div class="large-3 medium-3 small-3 column large-offset-3"> <label>Latitude <input type="text" name="center-lat" value=""> </label> </div> <div class="large-3 medium-e small-3 column"> <label>Longitude <input type="text" name="center-lng" value=""> </label> </div> </div> <div class="row expanded"> <div class="large-3 column" style="margin: 0 auto;"><label>Radius (Miles) <input type="Text" name="radius" value=""/></label></div></div></div></div>';
            $('#circle_append_here').append($(html));
        }
        else if($('#dropdown_type').val() == 'Rectangle')
        {
            //Append rectangle information.
            $('#circle_append_here').empty();//But first clear the append area
            html_first = '<div class="large-12 medium-12 small-12 column"><label class="text-center"><b>NW Corner Coordinates</b></label> <div class="row expanded"> <div class="large-6 medium-6 small-6 column"> <label>Latitude <input type="text" name="nw-lat" value=""> </label> </div> <div class="large-6 medium-6 small-6 column"> <label>Longitude <input type="text" name="nw-lng" value=""> </label> </div> </div> </div>';
            html_second = '<div class="large-12 medium-12 small-12 column"><label class="text-center"><b>SE Corner Coordinates</b></label><div class="row expanded"> <div class="large-6 medium-6 small-6 column"> <label>Latitude <input type="text" name="se-lat" value=""> </label> </div> <div class="large-6 medium-6 small-6 column"> <label>Longitude <input type="text" name="se-lng" value=""> </label> </div> </div> </div>';
            $('#rectangle_first_append_here').append(html_first);
            $('#rectangle_second_append_here').append(html_second);
        }
    });

});