function geocodePosition(pos)
{
    //Sets closest address into input for each event modal..
    geocoder = new google.maps.Geocoder();
    geocoder.geocode
    ({
            latLng: pos
        },
        function(results, status)
        {
            if (status == google.maps.GeocoderStatus.OK)
            {
                $('#closest-address').text("Closest Address: " + results[0].formatted_address);
            }
            else
            {
                //$("#mapErrorMsg").html('Cannot determine address at this location.'+status).show(100);
            }
        }
    );
}


close_aed_array = [];//Close_aed_array is going to be filled with matching AEDS based on distance, but isn't the final array.
aed_markers_array = [];//array of AEDs (markers) we are displaying on the map, we keep track so we can delete them later to update.
function populateAEDS(pos) {
    //positions is an array of all AED's, queried on user_home.php.
    for (let i = 0, len = positions.length; i < len; i++) {
        //Calculate the distance from the AED points vs our user centerPoint.
        if(arePointsNear(positions[i],pos,40074.274944)) //1.60934 km = 0.99999 miles, 4.82803 km is 3 miles.
        {
            //Create a new object (closePos) and add in the newly calculated distance.
            closePos = {lat: positions[i]['lat'], lng: positions[i]['lng'], location: positions[i]['location'], distance: last_distance, coordinator: positions[i].coordinator, contact: positions[i].contact, company: positions[i].company, address: positions[i].address};
            //push onto an array.
            close_aed_array.push(closePos);
            //last distance used.
            last_distance = 0;
        }
    }

    //This is the array of 10 AED's we will show user on map.
    aeds_being_added = [];


    //sort our AEDs by distance and get the first 10 (the closest).
    close_aed_array.sort((a,b) => (a.distance > b.distance) ? 1 : -1);
    inc_count = 0; //How many we have added to the array.
    for(let i = 0, len = close_aed_array.length; i < len; i++)
    {
        //Check for duplicates.
        const found = aeds_being_added.findIndex(aeds_being_added => aeds_being_added.distance === close_aed_array[i].distance);
        if(found == -1)
        {
            //Not a duplicate, go ahead and add.
            aeds_being_added.push(close_aed_array[i]);
            inc_count++;
            if(inc_count >= 10)
            {
                break;
            }
        }
    }



    //For each object in aeds_being_added, we add a pin.
    for(let i = 0, len = aeds_being_added.length; i < len; i++)
    {
        add_pin(aeds_being_added[i],"aed");
    }
    close_aed_array = []; //empty this array.


}//Pass in users current lat/long as an object (pos)
//Place active events that are happening.

last_distance = 0;//Calculate if point is within x km of centerPoint.
function arePointsNear(checkPoint, centerPoint, km) {
    var ky = 40000 / 360;
    var kx = Math.cos(Math.PI * centerPoint.lat / 180.0) * ky;
    var dx = Math.abs(centerPoint.lng - checkPoint.lng) * kx;
    var dy = Math.abs(centerPoint.lat - checkPoint.lat) * ky;
    if(Math.sqrt(dx * dx + dy * dy) <= km)
    {
        //console.log(dx * dx + dy * dy);
        last_distance = dx*dx+dy*dy;
        return true;
    }
    return false;
    //return Math.sqrt(dx * dx + dy * dy) <= km;
}
function add_pin(object,type)
{
    if(type === "aed") {
        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(object.lat, object.lng),
            map: map,
            title: object.name
        });
        aed_markers_array.push(marker);
        google.maps.event.addListener(marker, 'click', function() {
            let contact = '<p style="line-height: 20%;"></p>';
            let coordinator = '<p style="line-height: 20%;"></p>';
            let location = '<p style="line-height: 20%;"></p>';
            let company = '<p style="line-height: 20%;"></p>';
            let address = '<p style="line-height: 20%;"></p>';
            if(object.coordinator == null || object.coordinator === 'unknown' || object.coordinator === 'undefined' || object.coordinator === '' || object.coordinator === 'Unknown')
            {
                //nothing
            }
            else{
                coordinator = '<p style="line-height: 20%;">' + object.coordinator + '</p>';
            }
            if(object.location == null || object.location === '' || object.location === '*' || object.location === 'N/A' || object.location === 'unknown' || object.location === 'Unknown')
            {
                //nothing
            }
            else{
                location = '<p style="line-height: 20%;">' + object.location + '</p>';
            }
            //convert km to miles
            object.distance = (object.distance*0.62137).toFixed(2);
            tmp_dist = '<i style="color: red;">'+object.distance+' mi</i>';
            distance_text = '<p style="line-height: 20%;">Distance: '+tmp_dist+'</p>';
            if(object.contact == null || object.contact === 'unknown' || object.contact === 'undefined' || object.contact === '' || object.contact === 'Unknown')
            {
                //nothing
            }
            else{
                contact = '<p style="line-height: 20%;">Contact: ' + object.contact + '</p>';
            }
            if(object.company == null || object.company === 'unknown' || object.company === 'undefined' || object.company === '' || object.company === 'Unknown')
            {
                //nothing
            }
            else{
                company = '<p style="line-height: 20%;">'+object.company+'</p>'
            }
            if(object.address == null || object.address === 'unknown' || object.address === 'undefined' || object.address === '' || object.address === 'Unknown')
            {
                //nothing
            }
            else{
                address = '<p style="line-height: 20%;">'+object.address+'</p>';
            }
            let html = '<p style="line-height: 20%;"></p>' +
                company+
                coordinator +
                location +
                distance_text +
                contact +
                address +
                '';
            let infowindow = new google.maps.InfoWindow({
                content: html
            });
            last_located_address = ''; //set to empty
            infowindow.open(map, marker);
            map.setZoom(18);
            map.setCenter(marker.getPosition());
        });
        marker.setMap(map);
    }
    else if(type === "asset")
    {
        let icon = 'img/orange-dot.png';
        if(object.image !== null && object.image !== '' && object.image !== 'img/orange-dot.png')
        {
            icon = {
                url: object.image,
                scaledSize: new google.maps.Size(25, 25)
            }
        }
        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(object.lat, object.lng),
            icon: icon,
            map: map,
            title: name
        });


        google.maps.event.addListener(marker, 'click', function() {
            let text = '';
            let address = '';
            if(object.name === null || object.name === '' || object.name === 'unknown' || object.name === 'N/A' || object.name === 'undefined')
            {
                text = '' + '<h6>' + 'Unknown Type' + '</h6>' + '';
            }
            else{
                text = '' + '<h6>' + object.name + '</h6>' + '';
            }
            if(object.address === null || object.address === '' || object.address === 'unknown' || object.address === 'N/A' || object.address === 'undefined')
            {
                //nothing
            }
            else{
                address = '<p>'+object.address+'</p>';
            }
            html = '' +
                text +
                address +
                '';
            let infowindow = new google.maps.InfoWindow({
                content: html
            });
            infowindow.open(map, marker);
            map.setZoom(18);
            map.setCenter(marker.getPosition());
        });
        marker.setMap(map);
    }
    else if(type === "event")
    {
        let icon = {
            url: 'img/' + object.img,
            scaledSize: new google.maps.Size(35,35),
        };
        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(object.lat, object.lng),
            icon: icon,
            map: map,
            title: name
        });
        google.maps.event.addListener(marker, 'click', function() {
            let text = '';
            if(object.type != null && object.type != ' ' && object.type != 'unknown' && object.type != 'Unknown' && object.type != 'undefined')
            {
                text = '' + '<h6>' + object.type + '</h6>' + '';
            }
            else{
                text = '' + '<h6>' + 'Unknown Type' + '</h6>' + '';
            }
            if(object.notes != null && object.notes != ' ')
            {
                text+='<br><h3>'+object.notes+'</h3>';
            }

            let infowindow = new google.maps.InfoWindow({
                content: text
            });
            infowindow.open(map, marker);
            map.setZoom(18);
            map.setCenter(marker.getPosition());
        });
        marker.setMap(map);
    }
    else if(type === "call")
    {
        let icon = {
            url: 'img/help.png',
            scaledSize: new google.maps.Size(35,35),
        };
        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(object.lat, object.lng),
            icon: icon,
            map: map,
            title: name
        });
        google.maps.event.addListener(marker, 'click', function() {
            let latlng = '<p style="line-height: 20%;">Lat/Lng: '+object.lat+ " " + object.lng + '</p>';
            let name = '<p style="line-height: 20%;">Called by: ' + object.username + '</p>';
            let date = '<p style="line-height: 20%;">Call date: ' + object.calldate + '</p>';
            let html = '<p style="line-height: 20%"></p>' +
                name +
                date +
                latlng +
                '';

            let infowindow = new google.maps.InfoWindow({
                content: html
            });
            infowindow.open(map, marker);
            map.setZoom(18);
            map.setCenter(marker.getPosition());
        });
        marker.setMap(map);
    }
    else{

    }

}
function placeAssets()
{
    for(let i = 0, len = assets.length; i < len; i++)
    {
        add_pin(assets[i],"asset");
    }
}
function populateEvents()
{
    for (let i = 0, len = events_arr.length; i < len; i++) {
        add_pin(events_arr[i],"event");
    }

}

function initMap() {
    let event = {
        lat: parseFloat(mainEvent['latitude']),
        lng: parseFloat(mainEvent['longitude']),
    };
    geocodePosition(event);
    map = new google.maps.Map(document.getElementById('event-map'), {
        zoom: 15,
        center: event,
    });

    infowindow = new google.maps.InfoWindow({
        content: `
                <b>Time:</b> ` + mainEvent['call_datetime'] + `<br/>
                <b>User:</b> ` + mainEvent['username'],
    });

    let marker = new google.maps.Marker({
        position: event,
        icon: 'img/help.png',
        map: map,
    });
    marker.addListener('click', function () {
        infowindow.open(map, marker)
    });
    bounds = new google.maps.LatLngBounds();
    bounds.extend(event);
    populateAEDS(event);
    //populateEvents();

    placeAssets();
}


$(document).ready(function () {
});