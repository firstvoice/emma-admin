let map;
let bounds;
let updateInterval = 5000;
let infowindow;
let mCreation = moment(mainEvent['created_date']);
let incidents = new vis.DataSet();
let responseIds = [];
let interval = 60;
//console.log(mCreation - 1000 * 60 * interval / 2);
let timeline = new vis.Timeline(document.getElementById('timeline'), incidents,
  {
    minHeight: '200px',
    start: new Date(mCreation - 1000 * 60 * interval / 2),
    end: new Date(mCreation + 1000 * 60 * interval / 2),
    order: function (a, b) {
      return a.id - b.id;
    },
    format: {
      minorLabels: {
        millisecond: 'SSS',
        second: 's',
        minute: 'h:mm a',
        hour: 'h:mm a',
        weekday: 'ddd D',
        day: 'D',
        week: 'w',
        month: 'MMM',
        year: 'YYYY',
      },
      majorLabels: {
        millisecond: 'HH:mm:ss',
        second: 'D MMMM HH:mm',
        minute: 'ddd D MMMM',
        hour: 'ddd D MMMM',
        weekday: 'MMMM YYYY',
        day: 'MMMM YYYY',
        week: 'MMMM YYYY',
        month: 'YYYY',
        year: '',
      },
    },
  });

/* Sub Events */
const subEvents = [];
const badgeFire = $('#badge-fire');
const tableFire = $('#fire-sub-events');
const badgeLockDown = $('#badge-lock-down');
const tableLockDown = $('#lock-down-sub-events');
const badgeShelter = $('#badge-shelter');
const tableShelter = $('#shelter-sub-events');
const badgeActiveShooter = $('#badge-active-shooter');
const tableActiveShooter = $('#active-shooter-sub-events');
const badgeExplosion = $('#badge-explosion');
const tableExplosion = $('#explosion-sub-events');
const badgeHazmat = $('#badge-hazmat');
const tableHazmat = $('#hazmat-sub-events');
const badgeNaturalDisaster = $('#badge-natural-disaster');
const tableNaturalDisaster = $('#natural-disaster-sub-events');
const badgeSevereWeather = $('#badge-severe-weather');
const tableSevereWeather = $('#severe-weather-sub-events');
const badgeMedical = $('#badge-medical');
const tableMedical = $('#medical-sub-events');
const badgeOther = $('#badge-other');
const tableOther = $('#other-sub-events');


close_aed_array = [];//Close_aed_array is going to be filled with matching AEDS based on distance, but isn't the final array.
aed_markers_array = [];//array of AEDs (markers) we are displaying on the map, we keep track so we can delete them later to update.
function populateAEDS(pos) {
    //positions is an array of all AED's, queried on user_home.php.
    for (let i = 0, len = positions.length; i < len; i++) {
        //Calculate the distance from the AED points vs our user centerPoint.
        if(arePointsNear(positions[i],pos,40074.274944)) //1.60934 km = 0.99999 miles, 4.82803 km is 3 miles.
        {
            //Create a new object (closePos) and add in the newly calculated distance.
            closePos = {lat: positions[i]['lat'], lng: positions[i]['lng'], location: positions[i]['location'], distance: last_distance, coordinator: positions[i].coordinator, contact: positions[i].contact, company: positions[i].company, address: positions[i].address};
            //push onto an array.
            close_aed_array.push(closePos);
            //last distance used.
            last_distance = 0;
        }
    }

    //This is the array of 10 AED's we will show user on map.
    aeds_being_added = [];


    //sort our AEDs by distance and get the first 10 (the closest).
    close_aed_array.sort((a,b) => (a.distance > b.distance) ? 1 : -1);
    inc_count = 0; //How many we have added to the array.
    for(let i = 0, len = close_aed_array.length; i < len; i++)
    {
        //Check for duplicates.
        const found = aeds_being_added.findIndex(aeds_being_added => aeds_being_added.distance === close_aed_array[i].distance);
        if(found == -1)
        {
            //Not a duplicate, go ahead and add.
            aeds_being_added.push(close_aed_array[i]);
            inc_count++;
            if(inc_count >= 10)
            {
                break;
            }
        }
    }



    //For each object in aeds_being_added, we add a pin.
    for(let i = 0, len = aeds_being_added.length; i < len; i++)
    {
        add_pin(aeds_being_added[i],"aed");
    }
    close_aed_array = []; //empty this array.


}//Pass in users current lat/long as an object (pos)
//Place active events that are happening.
function populateCalls()
{
    for (let i = 0, len = police_calls.length; i<len; i++)
    {
        add_pin(police_calls[i],"call");
    }
}
last_distance = 0;//Calculate if point is within x km of centerPoint.
function arePointsNear(checkPoint, centerPoint, km) {
    var ky = 40000 / 360;
    var kx = Math.cos(Math.PI * centerPoint.lat / 180.0) * ky;
    var dx = Math.abs(centerPoint.lng - checkPoint.lng) * kx;
    var dy = Math.abs(centerPoint.lat - checkPoint.lat) * ky;
    if(Math.sqrt(dx * dx + dy * dy) <= km)
    {
        //console.log(dx * dx + dy * dy);
        last_distance = dx*dx+dy*dy;
        return true;
    }
    return false;
    //return Math.sqrt(dx * dx + dy * dy) <= km;
}
function add_pin(object,type)
{
    if(type === "aed") {
        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(object.lat, object.lng),
            map: map,
            title: object.name
        });
        aed_markers_array.push(marker);
        google.maps.event.addListener(marker, 'click', function() {
            let contact = '<p style="line-height: 20%;"></p>';
            let coordinator = '<p style="line-height: 20%;"></p>';
            let location = '<p style="line-height: 20%;"></p>';
            let company = '<p style="line-height: 20%;"></p>';
            let address = '<p style="line-height: 20%;"></p>';
            if(object.coordinator == null || object.coordinator === 'unknown' || object.coordinator === 'undefined' || object.coordinator === '' || object.coordinator === 'Unknown')
            {
                //nothing
            }
            else{
                coordinator = '<p style="line-height: 20%;">' + object.coordinator + '</p>';
            }
            if(object.location == null || object.location === '' || object.location === '*' || object.location === 'N/A' || object.location === 'unknown' || object.location === 'Unknown')
            {
                //nothing
            }
            else{
                location = '<p style="line-height: 20%;">' + object.location + '</p>';
            }
            //convert km to miles
            object.distance = (object.distance*0.62137).toFixed(2);
            tmp_dist = '<i style="color: red;">'+object.distance+' mi</i>';
            distance_text = '<p style="line-height: 20%;">Distance: '+tmp_dist+'</p>';
            if(object.contact == null || object.contact === 'unknown' || object.contact === 'undefined' || object.contact === '' || object.contact === 'Unknown')
            {
                //nothing
            }
            else{
                contact = '<p style="line-height: 20%;">Contact: ' + object.contact + '</p>';
            }
            if(object.company == null || object.company === 'unknown' || object.company === 'undefined' || object.company === '' || object.company === 'Unknown')
            {
                //nothing
            }
            else{
                company = '<p style="line-height: 20%;">'+object.company+'</p>'
            }
            if(object.address == null || object.address === 'unknown' || object.address === 'undefined' || object.address === '' || object.address === 'Unknown')
            {
                //nothing
            }
            else{
                address = '<p style="line-height: 20%;">'+object.address+'</p>';
            }
            let html = '<p style="line-height: 20%;"></p>' +
                company+
                coordinator +
                location +
                distance_text +
                contact +
                address +
                '';
            let infowindow = new google.maps.InfoWindow({
                content: html
            });
            last_located_address = ''; //set to empty
            infowindow.open(map, marker);
            map.setZoom(18);
            map.setCenter(marker.getPosition());
        });
        marker.setMap(map);
    }
    else if(type === "asset")
    {
        let icon = 'img/orange-dot.png';
        if(object.image !== null && object.image !== '' && object.image !== 'img/orange-dot.png')
        {
            icon = {
                url: object.image,
                scaledSize: new google.maps.Size(25, 25)
            }
        }
        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(object.lat, object.lng),
            icon: icon,
            map: map,
            title: name
        });


        google.maps.event.addListener(marker, 'click', function() {
            let text = '';
            let address = '';
            if(object.name === null || object.name === '' || object.name === 'unknown' || object.name === 'N/A' || object.name === 'undefined')
            {
                text = '' + '<h6>' + 'Unknown Type' + '</h6>' + '';
            }
            else{
                text = '' + '<h6>' + object.name + '</h6>' + '';
            }
            if(object.address === null || object.address === '' || object.address === 'unknown' || object.address === 'N/A' || object.address === 'undefined')
            {
                //nothing
            }
            else{
                address = '<p>'+object.address+'</p>';
            }
            html = '' +
                text +
                address +
                '';
            let infowindow = new google.maps.InfoWindow({
                content: html
            });
            infowindow.open(map, marker);
            map.setZoom(18);
            map.setCenter(marker.getPosition());
        });
        marker.setMap(map);
    }
    else if(type === "event")
    {
        let icon = {
            url: 'img/' + object.img,
            scaledSize: new google.maps.Size(35,35),
        };
        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(object.lat, object.lng),
            icon: icon,
            map: map,
            title: name
        });
        google.maps.event.addListener(marker, 'click', function() {
            let text = '';
            if(object.type != null && object.type != ' ' && object.type != 'unknown' && object.type != 'Unknown' && object.type != 'undefined')
            {
                text = '' + '<h6>' + object.type + '</h6>' + '';
            }
            else{
                text = '' + '<h6>' + 'Unknown Type' + '</h6>' + '';
            }
            if(object.notes != null && object.notes != ' ')
            {
                text+='<br><h3>'+object.notes+'</h3>';
            }

            let infowindow = new google.maps.InfoWindow({
                content: text
            });
            infowindow.open(map, marker);
            map.setZoom(18);
            map.setCenter(marker.getPosition());
        });
        marker.setMap(map);
    }
    else if(type === "call")
    {
        let icon = {
            url: 'img/help.png',
            scaledSize: new google.maps.Size(35,35),
        };
        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(object.lat, object.lng),
            icon: icon,
            map: map,
            title: name
        });
        google.maps.event.addListener(marker, 'click', function() {
            let latlng = '<p style="line-height: 20%;">Lat/Lng: '+object.lat+ " " + object.lng + '</p>';
            let name = '<p style="line-height: 20%;">Called by: ' + object.username + '</p>';
            let date = '<p style="line-height: 20%;">Call date: ' + object.calldate + '</p>';
            let html = '<p style="line-height: 20%"></p>' +
                name +
                date +
                latlng +
                '';

            let infowindow = new google.maps.InfoWindow({
                content: html
            });
            infowindow.open(map, marker);
            map.setZoom(18);
            map.setCenter(marker.getPosition());
        });
        marker.setMap(map);
    }
    else{

    }

}
function placeAssets()
{
    for(let i = 0, len = assets.length; i < len; i++)
    {
        add_pin(assets[i],"asset");
    }
}



function getTimeline() {
  $.ajax({
    dataType: 'json',
    url: 'process/get_timeline.php',
    type: 'POST',
    data: {'id': eventId, 'order': 'updated_date'},
  }).done(function (data) {
    // console.log(data);
    if (data.success) {
      for (let incident of data['incidents']) {
        if (incident['emma_pin'] !== incident['response_pin']) {
          incident['status_class'] = 'validation';
        }
        if (!incidents.get(incident['id'])) {
          let momentUpdatedDate = moment(incident['updated_date']);
          incidents.add([
            {
              id: incident['id'],
              start: new Date(momentUpdatedDate),
              content: incident['status_name'],
              title: `
                <b>Time:</b> ` + momentUpdatedDate.format('M/D/YYYY h:mm:ss a') + `<br/>
                <b>User:</b> ` + incident['user'] + `<br/>
                <b>Status:</b> ` + incident['status_name'] + `<br/>
                <b>Comments</b> ` + incident['message'],
              className: incident['status_class'],
            },
          ]);
          timeline.moveTo(new Date(momentUpdatedDate));
          // timeline.moveTo(new Date("2018-06-28"));

          let groupList = ``;
          for (let group of incident['groups']) {
            groupList += `<a href="dashboard.php?content=group&id=` + group['emma_group_id'] + `">` + group['name'] + `</a> | `;
          }
          //console.log(incident);
          let nextMessage = `
            <div id="message-`+incident['id']+`" data-closable class="callout alert-callout-subtle radius inset-shadow ` + incident['status_class'] + `" style="overflow:hidden;word-wrap:break-word;">
              <strong>
                <a href="dashboard.php?content=user&id=` + incident['user_id'] + `">` + incident['user'] + `</a> - 
                ` + momentUpdatedDate.format('M/D/YYYY h:mm:ss a') + `
              </strong>
              ` + (groupList === '' ? `` : `<div><b>Groups: </b>` + groupList + `</div>`) + `
              ` + (incident['description'] === '' ? `` : `<div><b>Emergency Details: </b>` + incident['description'] + `</div>`) + `
              ` + (incident['comments'] === '' ? `` : `<div><b>Comments: </b>` + incident['comments'] + `</div>`) + `
              ` + (incident['status_name'] === '' ? `` : `<div><b>Alert Response: </b><span style="color: `+incident['status_color']+`">` + incident['status_name'] + `</span></div>`) + `
              ` + (incident['message'] === '' ? `` : `<div><b>System Message: </b><p>` + incident['message'] + `</p></div>`) + `
              ` + (incident['validation_user'] === null || incident['validation_user'] === '' ? `` : `<strong style="color: green">Validated By: </strong>` + incident['validation_user'] + ` - ` + incident['validation_time']) + `
            </div>
          `;
          
          $('#message-center').append(nextMessage);
          let messageCenterDiv = document.getElementById('message-center');
          messageCenterDiv.scrollTop = messageCenterDiv.scrollHeight;
        } else {
          let prevIncident = incidents.get(incident['id']);
          let oldClass = prevIncident['className'];
          if(oldClass !== incident['status_class'] && oldClass === "validation") {
            incidents.update({id: incident['id'], className: incident['status_class']});
            $('#message-'+incident['id']).removeClass(oldClass);
            $('#message-'+incident['id']).addClass(incident['status_class']);
            $('#message-'+incident['id']).append(`<strong style="color: green">Validated By: </strong>`+incident['validation_user']+` - `+incident['validation_time']);
          }
        }
      }

      for (let subEvent of data['sub-events']) {
        let momentCreatedDate = moment(subEvent['created_date']);
        let prevEvent = subEvents.filter(se => se.id === subEvent['id']);
        if (subEvents.filter(se => se.id === subEvent['id']).length === 0) {
          subEvents.push(subEvent);
          incidents.add([
            {
              id: 'evt-' + subEvent['id'],
              start: new Date(momentCreatedDate),
              content: '',
              title: `
                <b>Time:</b> ` + momentCreatedDate.format('M/D/YYYY h:mm:ss a') + `<br/>
                <b>User:</b> ` + subEvent['user'] + `<br/>
                <b>Status:</b> ` + subEvent['type_name'] + ` Sub-Event<br/>
                <b>Comments</b> Additional ` + subEvent['type_name'] + ` event reported`,
              type: 'point',
              className: 'system',
            },
          ]);
          timeline.moveTo(new Date(momentCreatedDate));
          let row = `
            <tr>
              <td><a href="dashboard.php?content=event&id=`+subEvent['id']+`">` + subEvent['emma_site_name'] + `</a></td>
              <td>` + subEvent['user'] + `</td>
              <td>` + subEvent['created_date'] + `</td>
              <td>`+(subEvent['active'] === '1' ? '<span style="color:green;">Active</span>' : '<span style="color:red;">Closed</span>')+`</td>
            </tr>
          `;
          $('#'+subEvent['badge']+'-sub-events').append(row);
          let allSubTypes = subEvents.filter(se => se['type_id'] === subEvent['type_id'] && subEvent['active'] === '1');
          $('#badge-'+subEvent['badge']).text(allSubTypes.length);
          if(allSubTypes.length > 0) $('#badge-'+subEvent['badge']).removeClass('is-hidden');
        }
      }
    }
  }).fail(function (data, textStatus, errorThrown) {
    console.log(data);
    console.log(textStatus);
    console.log(errorThrown);
  });
}

function getResponses() {
  $.ajax({
    dataType: 'json',
    url: 'process/get_responses.php',
    type: 'GET',
    data: {'id': mainEvent['emergency_id'], 'order': 'emergency_response_id'},
    beforeSend: function () {
      $('#total-not-received span').empty().append($('#not-received-users-modal table tbody tr').length +
        ' Users');
    },
  }).done(function (data) {
    // console.log(data);
    // console.log(responseIds);
    if (data.success) {
      for (let response of data['responses']) {
        if (response['emma_pin'] !== response['response_pin'] && response['status'] !== '-1') {
          response['name'] = 'Need Validation';
          response['icon'] = 'purple';
          response['table'] = 'need-validation';
        }
        let iconSrc = response['mobile'] === '1' ? 'img/'+response['icon']+'_mobile.png' : 'img/'+response['icon']+'_pc.png';
        let idx = responseIds.findIndex(function (r) {
          return r.user === response['user_id'];
        });
        if (idx === -1) {
          let marker = new google.maps.Marker({
            position: {
              lat: parseFloat(response['latitude']),
              lng: parseFloat(response['longitude']),
            },
            icon: iconSrc,
            map: map,
            user: response['user'],
            phone: response['phone'],
            status: response['name'],
            comments: response['comments'],
          });
          google.maps.event.addListener(marker, 'click', function () {
            let html = '' +
              '<h6>' + this.user + '</h6>' +
              '<p>' + this.phone + '</p>' +
              '<p>' + this.position.lat() + ', ' + this.position.lng() + '</p>' +
              '';
            infowindow.setContent(html);
            infowindow.open(map, this);
          });
          responseIds.push({
            'user': response['user_id'],
            'status': response['name'],
            'marker': marker,
            'table': response['table'],
          });
          $('#nr-' + response['user_id']).remove();
          $('#' + response['table'] + '-users-modal table tbody').append('' +
            '<tr id=\'' + response['user_id'] + '\'>' +
            '<td><a href=\'dashboard.php?content=user&id=' +
            response['user_id'] + '\' > ' +
            response['user'] + '</a></td>' +
            '<td>' + response['username'] + '</td>' +
            '<td>' + response['phone'] + '</td>' +
            '<td>' + response['phone'] + '</td>' +
            '<td>' + moment(response['updated_date']).format('M/D/YYYY h:mm:ss a') + '</td>' +
            (response['table'] === 'need-validation' ? '<td>'+(
              mainEvent['active'] === '0' ? '' :
              '<a href="#" data-message-id="'+response['emergency_response_id']+'" class="validate-message button small" style="margin: 0;">Validate</a>'
            )+'</td>' : '') +
            '</tr>');
          validateButtons();
          $('#total-' + response['table'] + ' span').empty().append($('#' + response['table'] + '-users-modal table tbody tr').length +
            ' Users Last Updated: ' +
            moment(response['updated_date']).format('M/D/YYYY h:mm:ss a'));
          $('#total-not-received span').empty().append($('#not-received-users-modal table tbody tr').length +
            ' Users');
          if (!isNaN(marker.position.lat()) && !isNaN(marker.position.lng())) {
            bounds.extend(marker.position);
            map.fitBounds(bounds);
          }
        } else {
          if (response['name'] !== responseIds[idx]['status']) {
            $('#' + response['user_id']).remove();
            $('#total-' + responseIds[idx]['table'] + ' span').empty().append($('#' + responseIds[idx]['table'] +
              '-users-modal table tbody tr').length +
              ' Users Last Updated: ' +
              moment(response['updated_date']).format('M/D/YYYY h:mm:ss a'));
            $('#' + response['table'] + '-users-modal table tbody').append('' +
              '<tr id=\'' + response['user_id'] + '\'>' +
              '<td><a href=\'dashboard.php?content=user&id=' +
              response['user_id'] + '\' > ' +
              response['user'] + '</a></td>' +
              '<td>' + response['username'] + '</td>' +
              '<td>' + response['phone'] + '</td>' +
              '<td>' + response['phone'] + '</td>' +
              '<td>' + moment(response['updated_date']).format('M/D/YYYY h:mm:ss a') + '</td>' +
              (response['table'] === 'need-validation' ? '<td>'+(
                mainEvent['active'] === '0' ? '' :
                '<a href="#" data-message-id="'+response['emergency_response_id']+'" class="validate-message button small" style="margin: 0;">Validate</a>'
              )+'</td>' : '') +
              '</tr>');
            validateButtons();
            $('#total-' + response['table'] + ' span').empty().append($('#' + response['table'] + '-users-modal table tbody tr').length +
              ' Users Last Updated: ' +
              moment(response['updated_date']).format('M/D/YYYY h:mm:ss a'));

            responseIds[idx]['status'] = response['name'];
            responseIds[idx]['table'] = response['table'];
            responseIds[idx]['marker'].setIcon(iconSrc);
            responseIds[idx]['marker'].setPosition(new google.maps.LatLng(
              response['latitude'],
              response['longitude'],
            ));
            responseIds[idx]['marker'].comments = response['comments'];
            if (!isNaN(responseIds[idx]['marker'].position.lat()) &&
              !isNaN(responseIds[idx]['marker'].position.lng())) {
              bounds.extend(responseIds[idx]['marker'].position);
              map.fitBounds(bounds);
            }
          }
        }
      }
    } else {

    }
  }).fail(function (data, textStatus, errorThrown) {
    console.log(data);
    console.log(textStatus);
    console.log(errorThrown);
  });
}

function initMap() {
  let event = {
    lat: parseFloat(mainEvent['latitude']),
    lng: parseFloat(mainEvent['longitude']),
  };
  //console.log(event);
  map = new google.maps.Map(document.getElementById('event-map'), {
    zoom: 15,
    center: event,
  });

  infowindow = new google.maps.InfoWindow({
    content: 'AEDs',
  });

    let icon = {
        url: 'img/' + mainEvent['type_filename'],
        scaledSize: new google.maps.Size(35,35),
    };

  let marker = new google.maps.Marker({
    position: event,
    icon: icon,
    map: map,
  });
  bounds = new google.maps.LatLngBounds();
  bounds.extend(event);

    populateAEDS(event);
    populateCalls();
    placeAssets();


    getResponses();
  setInterval(getResponses, updateInterval);
}

function validateButtons() {
  $('.validate-message').click(function (e) {
    e.preventDefault();
    $.ajax({
      dataType: 'json',
      url: 'process/validate_emergency_response.php',
      type: 'POST',
      data: {'message-id': $(this).data('message-id'), 'validated-id': user['id']},
      beforeSend: function() {
        THINKINGMODAL.foundation('open');
      }
    }).done(function (data) {
      console.log(data);
      if (data.success) {
        /*
        $('#'+data['message']['user_id']).remove();
        $('#total-need-validation span').empty().append($('#need-validation-users-modal table tbody tr').length + ' Users Last Updated: ' + moment().format('M/D/YYYY h:mm:ss a'));
        $('#' + data['message']['table'] + '-users-modal table tbody').append('' +
          '<tr id=\'' + data['message']['user_id'] + '\'>' +
          '<td><a href=\'dashboard.php?content=user&id=' +
          data['message']['user_id'] + '\' > ' +
          data['message']['user'] + '</a></td>' +
          '<td>' + data['message']['username'] + '</td>' +
          '<td>' + data['message']['phone'] + '</td>' +
          '<td>' + data['message']['phone'] + '</td>' +
          '<td>' + moment(data['message']['updated_date']).format('M/D/YYYY h:mm:ss a') + '</td>' +
          '</tr>');
        $('#total-' + data['message']['table'] + ' span').empty().append($('#' + data['message']['table'] + '-users-modal table tbody tr').length +
          ' Users Last Updated: ' +
          moment().format('M/D/YYYY h:mm:ss a'));
        $('#message-usr-'+data['message']['emergency_response_id']).removeClass('validation');
        $('#message-usr-'+data['message']['emergency_response_id']).addClass(data['message']['class']);
        $('#message-usr-'+data['message']['emergency_response_id']).append(`<strong style="color: green">Validated By: </strong>`+data['message']['validation_user']+` - `+data['message']['validation_time']);
        // location.reload();

        let iconSrc = data['message']['mobile'] === '1' ? 'img/'+data['message']['icon']+'_mobile.png' : 'img/'+data['message']['icon']+'_pc.png';
        let idx = responseIds.findIndex(function (r) {
          return r.user === data['message']['user_id'];
        });
        if(idx !== -1) {
          responseIds[idx]['marker'].setIcon(iconSrc);
        }
        */
        THINKINGMODAL.foundation('close');
      } else {
        THINKINGMODAL.foundation('close');
      }
    }).fail(function (data, textStatus, errorThrown) {
      console.log(data);
      console.log(textStatus);
      console.log(errorThrown);
    });
  });
}

$(document).ready(function () {

  getTimeline();
  setInterval(getTimeline, updateInterval);

    $('.select-close-script-group-container').hide();
    $('.select-close-script-container').hide();

    $('.select-broadcast-script-group-container').hide();
    $('.select-broadcast-script-container').hide();

  $('#alert-groups').submit(function () {
    $.ajax({
      dataType: 'json',
      url: 'process/alert_all_users.php',
      type: 'POST',
      data: $('#alert-groups').serialize(),
    }).done(function (data) {
      if (data.success) {
        $('#message').val('');
        //TODO: SUCCESS MESSAGE
      } else {
        //TODO: FAILED MESSAGE
      }
    }).fail(function (data, textStatus, errorThrown) {
      console.log(data);
      console.log(textStatus);
      console.log(errorThrown);
    });
    return false;
  });

  $('#broadcast-event').submit(function (e) {
    e.preventDefault();
    let form = $(this);
    let groupCount = form.find('input.broadcast-group:checked').length;
    if(groupCount > 0) {
      $.ajax({
        dataType: 'json',
        url: 'process/broadcast_event.php',
        type: 'POST',
        data: $('#broadcast-event').serialize(),
        beforeSend: function () {
          $('#broadcast-event').find('input[type="submit"]').attr('disabled', true);
          THINKINGMODAL.foundation('open');
        },
      }).done(function (data) {
        console.log(data);
        if (data.success) {
          location.reload();
        } else {
          FAILMODAL.foundation('open');
        }
        $('#broadcast-event-modal').foundation('close');
      }).fail(function (data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
    } else {
      $('#broadcast-error').empty().append('<span style="color:red;">Groups Required</span>');
    }
    return false;
  });

  $('#close-event').submit(function () {
    $.ajax({
      dataType: 'json',
      url: 'process/close_event.php',
      type: 'POST',
      data: $('#close-event').serialize(),
      beforeSend: function() {
        // $('#close-event').find('input[type="submit"]').attr('disabled', true);
        THINKINGMODAL.foundation('open');
      },
    }).done(function (data) {
      console.log(data);
      if (data.success) {
        window.location = mainEvent['drill'] === '1' ? 'dashboard.php?content=drills' : 'dashboard.php?content=events';
      } else {
        FAILMODAL.foundation('open');
      }
    }).fail(function (data, textStatus, errorThrown) {
      console.log(data);
      console.log(textStatus);
      console.log(errorThrown);
    });
    return false;
  });

  $('#select-close-broadcast-type').change(function () {
    if($('.select-close-script-group-container').is(":hidden")){
        $('.select-close-script-group-container').show();
    }else {
        $('#select-close-script-group').change();
    }
  });

  $('#select-close-script-group').change(function (e) {
      e.preventDefault();
      $.ajax({
          dataType: 'json',
          url: 'process/get_group_scripts.php',
          type: 'POST',
          data: {
              'group-id': $('#select-close-script-group option:selected').val(),
              'plan-id': user['emma_plan_id'],
              'type': $('#select-close-broadcast-type option:selected').val(),
              'script-type': '3'
          },
          beforeSend: function () {
              $('.select-close-script-container').show();
              $('#select-close-script').empty();
          },
      })
          .done(function (data) {
              console.log(data);
              if (data.success) {
                  $('#select-close-script').append('<option value="" data-text="">-Select-</option>');
                  for (var i in data['groups']) {
                      $('#select-close-script').append('<option value="'+ data['groups'][i]['emma_script_id'] +'" data-text="'+ data['groups'][i]['text'] +'">'+ data['groups'][i]['name'] +'</option>');
                  }
              }
          })
          .fail(function (data, textStatus, errorThrown) {

          });
      return false;
  });


    $('#select-broadcast-broadcast-type').change(function () {
        if($('.select-broadcast-script-group-container').is(":hidden")){
            $('.select-broadcast-script-group-container').show();
        }else {
            $('#select-broadcast-script-group').change();
        }
    });

    $('#select-broadcast-script-group').change(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/get_group_scripts.php',
            type: 'POST',
            data: {
                'group-id': $('#select-broadcast-script-group option:selected').val(),
                'plan-id': user['emma_plan_id'],
                'type': $('#select-broadcast-broadcast-type option:selected').val(),
                'script-type': '2'
            },
            beforeSend: function () {
                $('.select-broadcast-script-container').show();
                $('#select-broadcast-script').empty();
            },
        })
            .done(function (data) {
                console.log(data);
                if (data.success) {
                    $('#select-broadcast-script').append('<option value="" data-text="">-Select-</option>');
                    for (var i in data['groups']) {
                        $('#select-broadcast-script').append('<option value="'+ data['groups'][i]['emma_script_id'] +'" data-text="'+ data['groups'][i]['text'] +'">'+ data['groups'][i]['name'] +'</option>');
                    }
                }
            })
            .fail(function (data, textStatus, errorThrown) {

            });
        return false;
    });


    $('#select-broadcast-script').change(function () {
    $('#broadcast-description').text($('#select-broadcast-script option:selected').data('text'));
  });

  $('#select-close-script').change(function () {
    $('#close-description').text($('#select-close-script option:selected').data('text'));
  });

    $('.b-all-select').click(function () {
        $('.b-group-select').click();
    });
    $('.cl-all-select').click(function () {
        $('.cl-group-select').click();
    });
});