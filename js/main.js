$(document).foundation();

const THINKINGMODAL = $('#thinking-modal');
// add a const similar to thinkingmodal but for the webbrowser
const SUCCESSMODAL = $('#success-modal');
const FAILMODAL = $('#fail-modal');

$('.select-create-event-script-group-container').hide();
$('.select-create-event-script-container').hide();
$('.select-create-notification-script-group-container').hide();
$('.select-create-notification-script-container').hide();

var currentEvents;
var currentSOS;
var currentSecurity;
var currentNotifications;
var currentLockdowns;
var currentModLockdowns;
var gotCurrent = false;

function getTimeZone() {
    const currtimezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    const servertimezone = 'America/Chicago';
    const currdate = new Date();
    return getOffsetBetweenTimezonesForDate(currdate, currtimezone, servertimezone);
}

function getCookie(cName){
    function getCookie(cName) {
        const name = cName + "=";
        const cDecoded = decodeURIComponent(document.cookie); //to be careful
        const cArr = cDecoded.split('; ');
        let res;
        cArr.forEach(val => {
            if (val.indexOf(name) === 0) res = val.substring(name.length);
        })
        return res
    }
}

function getOffsetBetweenTimezonesForDate(date, timezone1, timezone2) {
    const timezone1Date = convertDateToAnotherTimeZone(date, timezone1);
    const timezone2Date = convertDateToAnotherTimeZone(date, timezone2);
    return (timezone1Date.getTime() - timezone2Date.getTime())/3600000;
}

function convertDateToAnotherTimeZone(date, timezone) {
    const dateString = date.toLocaleString('en-US', {
        timeZone: timezone
    });
    return new Date(dateString);
}

function setTimeZone(){
    var timezone = getTimeZone();
    // console.log(Date.parse('2017-03-22 12:01:34'));
    $.ajax({
        dataType: 'json',
        url: 'process/set_timezone.php',
        type: 'POST',
        data: {'timezone': timezone},
        beforeSend: function () {
        },
    })
        .done(function (data) {
            console.log(data);
            if (data.success) {
                // location.reload();
                //TODO: SUCCESS MESSAGE
            } else {
                //TODO: FAILED MESSAGE
            }
            //form.foundation('close');
        })
        .fail(function (data, textStatus, errorThrown) {

        });
    // } else {
    //   $('.event-error').empty().append('<span style="color:red;">Groups Required</span>');
    // }
    return false;
}

function geocodePosition(pos)
{
    //console.log(pos);
    //Sets closest address into input for each event modal..
    geocoder = new google.maps.Geocoder();
    geocoder.geocode
    ({
            latLng: new google.maps.LatLng(pos.latitude,pos.longitude)
        },
        function(results, status)
        {
            if (status == google.maps.GeocoderStatus.OK)
            {
                $('#nearestAddress').val(results[0].formatted_address);
            }
            else
            {
                $('#nearestAddress').val('N/A');
            }
        }
    );
}

function getCurrentEvents() {
    $.ajax({
        dataType: 'json',
        url: 'process/search_current_events.php',
        type: 'POST',
    })
        .done(function (data) {
            console.log(data);
            if (data.success) {
                 currentEvents = data['events'];
                 currentSOS = data['sos'];
                 currentSecurity = data['securities'];
                 currentNotifications = data['notifications'];
                 currentLockdowns = data['lockdowns'];
                 currentModLockdowns = data['m_lockdowns'];
                 gotCurrent = true;
            } else {
                return 'failed';
            }
        })
        .fail(function (data, textStatus, errorThrown) {
            return 'failed';
        });
}

if(user != null) {
    setTimeout(
        setInterval(function () {
            if(gotCurrent) {
                $.ajax({
                    dataType: 'json',
                    url: 'process/search_new_events.php',
                    type: 'POST',
                    data: {
                        "currentEvent": currentEvents,
                        "currentSOS": currentSOS,
                        "currentSecurity": currentSecurity,
                        "currentNotification": currentNotifications,
                        "currentLockdown": currentLockdowns,
                        "currentMLockdown": currentModLockdowns
                    },
                })
                    .done(function (data) {
                        if (data.success) {
                            console.log(data);
                            if (data['new'] && data['alert']) {
                                let notificationTitle = data['title'];
                                let notificationBody = data['body'];

                                let popup = document.getElementById("emergency_alert");
                                let alert_button = document.getElementById("emergency_popup_link");
                                let icon = document.getElementById("emergency_popup_icon");
                                let header = document.getElementById("emergency_popup_header");
                                let details = document.getElementById("emergency_popup_details");

                                icon.innerHTML = '<img src="img/' + data['icon'] + '"/>';
                                header.innerHTML = notificationTitle;
                                details.innerHTML = notificationBody;
                                switch (data['type']) {
                                    case 'event':
                                        alert_button.href = 'dashboard.php?content=event&id=' + data['id'];
                                        currentEvents += ',' + data['id'];
                                        break;
                                    case 'security':
                                        alert_button.href = 'dashboard.php?content=security&id=' + data['id'];
                                        currentSecurity += ',' + data['id'];
                                        break;
                                    case 'sos':
                                        alert_button.href = 'dashboard.php?content=sos_details&id=' + data['id'];
                                        currentSOS += ',' + data['id'];
                                        break;
                                    case 'notification':
                                        alert_button.href = 'dashboard.php?content=mass_communication&id=' + data['id'];
                                        currentNotifications += ',' + data['id'];
                                        break;
                                    case 'lockdown':
                                        alert_button.href = 'dashboard.php?content=lockdown&id=' + data['id'];
                                        currentLockdowns += ',' + data['id'];
                                        break;
                                    case 'mlockdown':
                                        alert_button.href = 'dashboard.php?content=modified_lockdown&id=' + data['id'];
                                        currentModLockdowns += ',' + data['id'];
                                        break;

                                }
                                popup.style.backgroundColor = data['color'];
                                popup.style.display = 'block';
                                let audio = new Audio('sounds/notification.mp3');
                                audio.play();
                                log_received(data['id'], data['type']);

                            } else if (data['new'] && !data['alert']) {
                                location.reload();
                            }
                        } else {
                            //TODO: FAILED MESSAGE
                        }
                    })
                    .fail(function (data, textStatus, errorThrown) {
                    });
            }
        }, 6000)
        , 10000);
}

function initGeolocation()
{
    if( navigator.geolocation )
    {
        // Call getCurrentPosition with success and failure callbacks
        navigator.geolocation.getCurrentPosition( success, fail );
    }
    else
    {
        alert("Sorry, your browser does not support geolocation services, some systems may not function correctly.");
    }
}

function success(position)
{
    $('#user_longitude').val(position.coords.longitude.toFixed(7));
    $('#user_latitude').val(position.coords.latitude.toFixed(7));
    $('#lockdown_lng').val(position.coords.longitude.toFixed(7));
    $('#lockdown_lat').val(position.coords.latitude.toFixed(7));
    $('#lockdown_drill_lng').val(position.coords.longitude.toFixed(7));
    $('#lockdown_drill_lat').val(position.coords.latitude.toFixed(7));
    $('#modified_lockdown_lng').val(position.coords.longitude.toFixed(7));
    $('#modified_lockdown_lat').val(position.coords.latitude.toFixed(7));
    var pos={latitude: $('#user_latitude').val(), longitude: $('#user_longitude').val()};
    geocodePosition(pos);
}

function fail()
{
    // Could not obtain location
}

function log_received(id, type)
{
    $.ajax({
        dataType: 'json',
        url: 'process/log_received.php',
        type: 'POST',
        data: {
            "id": id,
            "type": type
        },
    })
        .done(function (data) {
            if (data.success) {
                console.log(data);
            } else {
                return 'failed';
            }
        })
        .fail(function (data, textStatus, errorThrown) {
            return 'failed';
        });
}

let timer_triggered = false;
function start_emma_timer() {
    //let duration=60*1; //60*5 = 5 minutes.
    duration = $('#plan_seconds_length').val();
    let display=document.querySelector('#emma_countdown_timer');
    let start = Date.now(),
        diff,
        minutes,
        seconds;
        function timer() {
            if(!timer_triggered) {
                // get the number of seconds that have elapsed since
                // startTimer() was called
                diff = duration - (((Date.now() - start) / 1000) | 0);
                // does the same job as parseInt truncates the float
                minutes = (diff / 60) | 0;
                seconds = (diff % 60) | 0;

                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;
                display.textContent = minutes + ":" + seconds;
            }
        if (diff <= 0) {
            //Timer reached zero.
            $('#emma_sos_cancel_button').html('Back');
            timer_triggered = true;
            if(timer_triggered){
                $('#emma_countdown_timer').css('display','none');
            $('#emma_button').css('background-color', 'red');
                $('#emma_latitude_show').val("Latitude: " + ($('#user_latitude')).val());
                $('#emma_longitude_show').val("Longitude: " +($('#user_longitude')).val());
            $('#show_alert_sent').css('display','inherit');
            $.ajax({
                dataType: 'json',
                url: 'process/activate_sos_mobile_api.php',
                type: 'POST',
                data:{
                    "id": $('#emma_sos_id').val(),
                    "lat": $('#user_latitude').val(),
                    "lng": $('#user_longitude').val()
                },
                beforeSend: function () {
                },
            })
                .done(function (data) {
                    //console.log(data);
                    if (data.success) {
                        //TODO: SUCCESS MESSAGE
                    } else {
                        //TODO: FAILED MESSAGE
                    }
                })
                .fail(function (data, textStatus, errorThrown) {
                   //console.log(data);
                });
            //start = Date.now() + 1000;
            }
        }
    }
    // we don't want to wait a full second before the timer starts
    timer();
    setInterval(timer, 1000);
    //make an ajax call and set active or whichever to 0, then when button gets triggered we set it to true.
    $.ajax({
        dataType: 'json',
        url: 'process/create_emma_sos.php',
        type: 'POST',
        data:{
            "latitude": $('#user_latitude').val(),
            "longitude": $('#user_longitude').val()
        },
        beforeSend: function () {
        },
    })
        .done(function (data) {
            //Pull name off of switch_form_event element and then attach it to the ID that matches.
            if (data.success) {
                $('#emma_sos_id').val(data.lastid);
                //TODO: SUCCESS MESSAGE
            } else {
                //TODO: FAILED MESSAGE
            }
        })
        .fail(function (data, textStatus, errorThrown) {
        });
}


$(document).ready(function () {
    initGeolocation();
    getCurrentEvents();
    setTimeZone();

    emma_button_clicked = false;
    emma_sos_alert_sent = false;
    $('#click_emma_button').mousedown(function(){
        if(!emma_button_clicked && !emma_sos_alert_sent) {
            emma_button_clicked = true;
            emma_sos_alert_sent = true;
            $('#emma_countdown_timer').css('display','none');
            $('#emma_button').css('background-color', 'red');
            $('#show_alert_sent_label').css('display','inherit');
            $('#emma_sos_cancel_button').attr('class','button expanded success');
            $('#emma_sos_cancel_button').html('Back');
            $.ajax({
                dataType: 'json',
                url: 'process/activate_sos_mobile_api.php',
                type: 'POST',
                data:{
                    "id": $('#emma_sos_id').val(),
                    "lat": $('#user_latitude').val(),
                    "lng": $('#user_longitude').val()
                },
                beforeSend: function () {
                },
            })
                .done(function (data) {
                   // console.log(data);
                    if (data.success) {
                        //TODO: SUCCESS MESSAGE
                        $('#emma_latitude_show').val("Latitude: " + ($('#user_latitude')).val());
                        $('#emma_longitude_show').val("Longitude: " +($('#user_latitude')).val());
                        $('#show_alert_sent').css('display','inherit');
                    } else {
                        //TODO: FAILED MESSAGE
                    }
                })
                .fail(function (data, textStatus, errorThrown) {
                  //  console.log(data);
                });




        }
    });

  $('[data-app-dashboard-toggle-shrink]').on('click', function (e) {
    e.preventDefault();
    $(this).parents('.app-dashboard').toggleClass('shrink-medium').toggleClass('shrink-large');
  });

  $('.select-create-event-script').change(function () {
    $('.create-event-description').text($(this).find('option:selected').data('text'));
  });

  $('.select-create-drill-script').change(function () {
    $('.create-drill-description').text($(this).find('option:selected').data('text'));
  });

  $('.select-create-notification-script').change(function () {
      $('.create-notification-description').text($(this).find('option:selected').val());
  });
  $('.select-create-email-script').change(function () {
      $('.create-email-description').text($(this).find('option:selected').val());
  });

  $('#close_emergency_alert').click(function () {
      document.getElementById("emergency_alert").style.display = 'none';
      // location.reload();
  });

    $('.select-create-geofence-script-group').change(function (e) {
        e.preventDefault();
            $.ajax({
                dataType: 'json',
                url: 'process/get_group_scripts.php',
                type: 'POST',
                data: {
                    'group-id': $('.select-create-geofence-script-group option:selected').val(),
                    'plan-id': user['emma_plan_id'],
                    'script-type': ''
                },
                beforeSend: function () {
                    $('#select-geocode-script').empty();
                },
            })
                .done(function (data) {

                    if (data.success) {
                        $('#select-geocode-script').append('<option value="" data-text="">-- Select Script --</option>');
                       // console.log(data['groups']);
                        for (var i in data['groups']) {
                            $('#select-geocode-script').append('<option value="'+ data['groups'][i]['emma_script_id'] +'" data-text="'+ data['groups'][i]['text'] +'">'+ data['groups'][i]['name'] +'</option>');
                        }
                    }
                })
                .fail(function (data, textStatus, errorThrown) {

                });
        return false;
    });
    $('.select-create-event-broadcast-type').change(function () {
        if($('.select-create-event-script-group-container').is(":hidden")) {
            $('.select-create-event-script-group-container').show();
        }else {
            $('.select-create-event-script-group').change();
        }
    });

    $('.select-create-notification-broadcast-type').change(function () {
        if($('.select-create-notification-script-group-container').is(":hidden")) {
            $('.select-create-notification-script-group-container').show();
        }else {
            $('.select-create-notification-script-group').change();
        }
    });

    $('.select-create-event-script-group').change(function (e) {
        e.preventDefault();
        var group = $(this).closest('.select-create-event-script-group').find('option:selected').val();
        var type = $(this).parent().parent().find('.select-create-event-broadcast-type option:selected').val();
        // console.log(type);
        $.ajax({
            dataType: 'json',
            url: 'process/get_group_scripts.php',
            type: 'POST',
            data: {
                'group-id': group,
                'plan-id': user['emma_plan_id'],
                'type': type,
                'script-type': '1'
            },
            beforeSend: function () {
                $('.select-create-event-script-container').show();
                $('.select-create-event-script').empty();
            },
        })
            .done(function (data) {
               // console.log(data);
                if (data.success) {
                    $('.select-create-event-script').append('<option value="" data-text="">-Select-</option>');
                    for (var i in data['groups']) {
                        $('.select-create-event-script').append('<option value="'+ data['groups'][i]['emma_script_id'] +'" data-text="'+ data['groups'][i]['text'] +'">'+ data['groups'][i]['name'] +'</option>');
                    }
                }
            })
            .fail(function (data, textStatus, errorThrown) {

            });
        return false;
    });

    $('.select-create-notification-script-group').change(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/get_group_scripts.php',
            type: 'POST',
            data: {
                'group-id': $('.select-create-notification-script-group option:selected').val(),
                'plan-id': user['emma_plan_id'],
                'type': $('.select-create-notification-broadcast-type option:selected').val(),
                'script-type': '1'
            },
            beforeSend: function () {
                $('.select-create-notification-script-container').show();
                $('.select-create-notification-script').empty();
            },
        })
            .done(function (data) {
                // console.log(data);
                if (data.success) {
                    $('.select-create-notification-script').append('<option value="" data-text="">-Select-</option>');
                    for (var i in data['groups']) {
                        $('.select-create-notification-script').append('<option value="'+ data['groups'][i]['emma_script_id'] +'" data-text="'+ data['groups'][i]['text'] +'">'+ data['groups'][i]['name'] +'</option>');
                    }
                }
            })
            .fail(function (data, textStatus, errorThrown) {

            });
        return false;
    });

    $('.select-broadcast-script-group').change(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/get_group_scripts.php',
            type: 'POST',
            data: {
                'group-id': $('.select-broadcast-script-group option:selected').val(),
                'plan-id': user['emma_plan_id'],
                'script-type': '2'
            },
            beforeSend: function () {
                $('#select-broadcast-script').empty();
            },
        })
            .done(function (data) {
               // console.log(data);
                if (data.success) {
                    $('#select-broadcast-script').append('<option value="" data-text="">-- Select Script --</option>');
                  //  console.log(data['groups']);
                    for (var i in data['groups']) {
                        $('#select-broadcast-script').append('<option value="'+ data['groups'][i]['emma_script_id'] +'" data-text="'+ data['groups'][i]['text'] +'">'+ data['groups'][i]['name'] +'</option>');
                    }
                }
            })
            .fail(function (data, textStatus, errorThrown) {

            });
        return false;
    });

    // $('#forgot-password-form').submit(function (e) {
    //     e.preventDefault();
    //     let form = $(this);
    //     $.ajax({
    //         dataType: 'json',
    //         url: 'process/forgot_password.php',
    //         type: 'POST',
    //         data: form.serialize(),
    //         beforeSend: function () {
    //             $('#thinking-modal').foundation('open');
    //         },
    //     })
    //         .done(function (data) {
    //            // console.log(data);
    //             $('#thinking-modal').foundation('close');
    //             if (data.success) {
    //                 $('#password-success-modal').foundation('open');
    //                 // location.reload();
    //                 //TODO: SUCCESS MESSAGE
    //             } else {
    //                 //TODO: FAILED MESSAGE
    //             }
    //             //form.foundation('close');
    //         })
    //         .fail(function (data, textStatus, errorThrown) {
    //
    //         });
    //     // } else {
    //     //   $('.event-error').empty().append('<span style="color:red;">Groups Required</span>');
    //     // }
    //     return false;
    // });

    $('#contact-us-form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        $.ajax({
            dataType: 'json',
            url: 'process/contact_us.php',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            },
        })
            .done(function (data) {
               console.log(data);
                $('#thinking-modal').foundation('close');
                if (data.success) {
                    $('#contact-us-success-modal').foundation('open');
                    // location.reload();
                    //TODO: SUCCESS MESSAGE
                } else {
                    //TODO: FAILED MESSAGE
                }
                //form.foundation('close');
            })
            .fail(function (data, textStatus, errorThrown) {

            });
        // } else {
        //   $('.event-error').empty().append('<span style="color:red;">Groups Required</span>');
        // }
        return false;
    });


    $('.contact-us-form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        $.ajax({
            dataType: 'json',
            url: 'process/contact_us.php',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            },
        })
            .done(function (data) {
                  console.log(data);
                $('#thinking-modal').foundation('close');
                if (data.success) {
                    $('#contact-us-success-modal').foundation('open');
                    // location.reload();
                    //TODO: SUCCESS MESSAGE
                } else {
                    //TODO: FAILED MESSAGE
                }
                //form.foundation('close');
            })
            .fail(function (data, textStatus, errorThrown) {

            });
        // } else {
        //   $('.event-error').empty().append('<span style="color:red;">Groups Required</span>');
        // }
        return false;
    });

    //This delegation exists because the select inputs are dynamically generated so we need this.
    $('#response-card-form').on('click','input[type="radio"]',function(){
       $('#response-card-modal').find('input[name="selected_id"]').val($(this).val());
    });


    create_event = function() {
        let event = document.getElementById("enter-pin-modal").getAttribute("data-call");
        if (event === 'response') {
            //form = $('#hidden_form');
            //form.append($('#pin-password-holder'));
            user_id = $('#response-card-form').find('input[name="user_id"]');
            emergency_id = $('#response-card-form').find('input[name="emergency_id"]');
            comments = $('#response-card-form').find('input[name="comments"]');
            response = $('#response-card-form').find('input[name="selected_id"]');
            pinused = $('#enter-pin-modal').find('input[id="pin-password-holder"]');
            lat = $('#user_latitude');
            lng = $('#user_longitude');
            $.ajax({
                dataType: 'json',
                url: 'process/create_response.php',
                type: 'POST',
                data: {
                    "user_id": user_id.val(),
                    "emergency_id": emergency_id.val(),
                    "comments": comments.val(),
                    "response": response.val(),
                    "pin": pinused.val(),
                    "lat": lat.val(),
                    "lng": lng.val()
                    // "location-description": location_description.val()
                },
                beforeSend: function() {
                    $('#thinking-modal').foundation('open');
                },
            }).done(function(data) {
                console.log(data);
                $('#thinking-modal').foundation('close');
                if (data.success) {
                    $('#success-modal').foundation('open');
                    // location.reload();
                    //TODO: SUCCESS MESSAGE
                }
            }).fail(function(data, textStatus, errorThrown) {
               console.log(data);
               console.log(textStatus);
               console.log(errorThrown);
            });
        }
        else if (event === 'event') {
            //form = $('#hidden_form');
            //form.append(switch_form_event);
            //Add the pin number inputed to the form.
            //form.append($('#pin-password-holder'));
            //let groupCount = form.find('input.group-select:checked').length;
            //if(groupCount > 0) {
            userID = $(switch_form_event).find('input[name="user"]');
            drill = $(switch_form_event).find('input[name="drill"]');
            type = $(switch_form_event).find('input[name="type"]');
            lat = $('#user_latitude');
            lng = $('#user_longitude');
            select = $(switch_form_event).find("select.sub-type");
            site = $(switch_form_event).find("select.site");
            description = $(switch_form_event).find('textarea[name="description"]');
            comments = $(switch_form_event).find('textarea[class="create-event-description"]');
            pinused = $('#enter-pin-modal').find('input[id="pin-password-holder"]');

            groupsArray = new Array();
            $(switch_form_event).find('.switch-input.group-select').each(function(){
                groupsArray.push($(this).val());
            });


                $.ajax({
                    dataType: 'json',
                    url: 'process/create_event.php',
                    type: 'POST',
                    data: {
                        "user": userID.val(),
                        "drill": drill.val(),
                        "type": type.val(),
                        "lat": lat.val(),
                        "lng": lng.val(),
                        "pin": pinused.val(),
                        "sub-type": select.val(),
                        "site": site.val(),
                        "description": description.val(),
                        "comments": comments.val(),
                        "groups": groupsArray
                    },
                    beforeSend: function () {
                      // console.log(select.val() + " " + site.val() + " " + description.val() + " " + comments.val());
                        $('#thinking-modal').foundation('open');
                    },
                })
                    .done(function (data) {
                        //Pull name off of switch_form_event element and then attach it to the ID that matches.
                        parent.append(switch_form_event);
                        switch_form_event.style.display = "inline";
                        if (data.success) {
                            $('#event-success-modal').foundation('open');
                            location.reload();
                            //TODO: SUCCESS MESSAGE
                        } else {
                            //TODO: FAILED MESSAGE
                        }
                    })
                    .fail(function (data, textStatus, errorThrown) {
                        switch_form_event.style.display = "inline";
                        parent.append(switch_form_event);

                    });
                //}

                for (let x = 1; x <= 4; x++) {
                    let circle = document.getElementById('dot' + x);
                    circle.style.backgroundColor = "#bbb";
                }
                //switch the form back to the other div.
                parent.append(switch_form_event);

                return false;

        }else if (event === 'security') {
            //form = $('#hidden_form');
            //form.append(switch_form_event);
            //Add the pin number inputed to the form.
            //form.append($('#pin-password-holder'));
            //let groupCount = form.find('input.group-select:checked').length;
            //if(groupCount > 0) {
            userID = $(switch_form_event).find('input[name="user"]');
            type = $(switch_form_event).find('input[name="type"]');
            lat = $('#user_latitude');
            lng = $('#user_longitude');
            select = $(switch_form_event).find("select.sub-type");
            site = $(switch_form_event).find("select.site");
            description = $(switch_form_event).find('textarea[name="description"]');
            comments = $(switch_form_event).find('textarea[class="create-event-description"]');
            pinused = $('#enter-pin-modal').find('input[id="pin-password-holder"]');

            groupsArray = new Array();
            $(switch_form_event).find('.switch-input.group-select').each(function(){
                groupsArray.push($(this).val());
            });


                $.ajax({
                    dataType: 'json',
                    url: 'process/create_security.php',
                    type: 'POST',
                    data: {
                        "user": userID.val(),
                        "drill": drill.val(),
                        "type": type.val(),
                        "lat": lat.val(),
                        "lng": lng.val(),
                        "pin": pinused.val(),
                        "sub-type": select.val(),
                        "site": site.val(),
                        "description": description.val(),
                        "comments": comments.val(),
                        "groups": groupsArray
                    },
                    beforeSend: function () {
                      // console.log(select.val() + " " + site.val() + " " + description.val() + " " + comments.val());
                        $('#thinking-modal').foundation('open');
                    },
                })
                    .done(function (data) {
                        //Pull name off of switch_form_event element and then attach it to the ID that matches.
                        parent.append(switch_form_event);
                        switch_form_event.style.display = "inline";
                        if (data.success) {
                            $('#event-success-modal').foundation('open');
                            location.reload();
                            //TODO: SUCCESS MESSAGE
                        } else {
                            //TODO: FAILED MESSAGE
                        }
                    })
                    .fail(function (data, textStatus, errorThrown) {
                        switch_form_event.style.display = "inline";
                        parent.append(switch_form_event);

                    });
                //}

                for (let x = 1; x <= 4; x++) {
                    let circle = document.getElementById('dot' + x);
                    circle.style.backgroundColor = "#bbb";
                }
                //switch the form back to the other div.
                parent.append(switch_form_event);

                return false;

        }
        else if(event === 'emma_sos')
        {

            $.ajax({
                dataType: 'json',
                url: 'process/activate_sos_mobile_api.php',
                type: 'POST',
                data:{"id": $('#emma_sos_id').val(),
                    "lat": $('#user_latitude').val(),
                    "lng": $('#user_longitude').val(),
                    "pin": $('#pin-password-holder').val()
                },
                beforeSend: function () {
                    $('#thinking-modal').foundation('open');
                },
            })
                .done(function (data) {
                    //console.log(data);
                    if (data.success) {
                        $('#emma-sos-success-pin-modal').foundation('open');
                        location.reload();
                        //TODO: SUCCESS MESSAGE
                    } else {
                        //TODO: FAILED MESSAGE
                        $('#emma-sos-invalid-pin-modal').foundation('open');
                        location.reload();
                    }
                })
                .fail(function (data, textStatus, errorThrown) {
                    //switch_form_event.style.display = "inline";
                    //parent.append(switch_form_event);

                });
            //}

            for (let x = 1; x <= 4; x++) {
                let circle = document.getElementById('dot' + x);
                circle.style.backgroundColor = "#bbb";
            }
            //switch the form back to the other div, but we don't need to do this for emma_sos
            if(event != 'emma_sos') {
                parent.append(switch_form_event);
            }

            return false;
        }
        document.getElementById("enter-pin-modal").removeAttribute("data-call");
    };

    $('form.create-event-form').submit(function(e){
        e.preventDefault();
        let form=$(this);


        switch_form_event = form.closest('.create-event-form');
        //form = $('#hidden_form');
        //form.append(switch_form_event);
        parent = switch_form_event.closest('.reveal');
        lat = $('#user_latitude');
        lng = $('#user_longitude');

            //let groupCount = form.find('input.group-select:checked').length;
            //if(groupCount > 0) {
            $.ajax({
                dataType: 'json',
                url: 'process/create_event.php',
                type: 'POST',
                data: form.serialize() + "&lat=" + lat.val() + "&lng=" + lng.val() + "&address=" + $('#nearestAddress').val(),
                beforeSend: function () {
                    $('#thinking-modal').foundation('open');
                },
            })
                .done(function (data) {
                    //Pull name off of switch_form_event element and then attach it to the ID that matches.
                   // console.log(data);
                    if (data.success) {
                        $('#event-success-modal').foundation('open');
                        console.log(data);
                        //location.reload();
                    } else {
                        $('#fail-modal').foundation('open');
                    }
                })
                .fail(function (data, textStatus, errorThrown) {
                  //  console.log(data);
                 ///   console.log(textStatus);
                  //  console.log(errorThrown);
                });
            //}

            //parent.append(switch_form_event);



    });


    create_event_admin = function(button) {



        switch_form_event = button.closest('.create-event-form');
        //form = $('#hidden_form');
        //form.append(switch_form_event);
        parent = switch_form_event.closest('.reveal');
        userID = $(switch_form_event).find('input[name="user"]');
        drill = $(switch_form_event).find('input[name="drill"]').is(':checked');
        type = $(switch_form_event).find('input[name="type"]');
        lat = $('#user_latitude');
        lng = $('#user_longitude');
        select = $(switch_form_event).find("select.sub-type");
        site = $(switch_form_event).find("select.site");
        description = $(switch_form_event).find('textarea[name="description"]');
        comments = $(switch_form_event).find('textarea[class="create-event-description"]');

        groupsArray = new Array();
            $(switch_form_event).find('.switch-input.group-select').each(function(){
                    groupsArray.push($(this).val());
            });


        if($(description).val().replace(/^\s+|\s+$/g, "").length === 0 || $(comments).val().replace(/^\s+|\s+$/g, "").length === 0) {
                alert('Please fillout more form information');
              //  console.log($(description).val());
        }
        else{
            //let groupCount = form.find('input.group-select:checked').length;
            //if(groupCount > 0) {
            $.ajax({
                dataType: 'json',
                url: 'process/create_event.php',
                type: 'POST',
                data: {
                    "user": userID.val(),
                    "drill": drill,
                    "type": type.val(),
                    "lat": lat.val(),
                    "lng": lng.val(),
                    "address": $('#nearestAddress').val(),
                    "sub-type": select.val(),
                    "site": site.val(),
                    "description": description.val(),
                    "comments": comments.val(),
                    "groups": groupsArray
                },
                beforeSend: function () {
                    $('#thinking-modal').foundation('open');
                },
            })
                .done(function (data) {
                    //Pull name off of switch_form_event element and then attach it to the ID that matches.
                 //   console.log(data);
                    if (data.success) {
                        $('#event-success-modal').foundation('open');
                    } else {

                        $('#fail-modal').foundation('open');
                    }
                })
                .fail(function (data, textStatus, errorThrown) {
                //    console.log(data);
                 //   console.log(textStatus);
                 //   console.log(errorThrown);
                });
            //}

            //parent.append(switch_form_event);


        }

    };

    $('#create-anonymous-report-form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        $.ajax({
            dataType: 'json',
            url: 'process/create_anonymous_report.php',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            },
        }).done(function(data) {
            console.log(data);
            if (data.success) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                $('#success-modal').foundation('open');
            } else {

            }
        }).fail(function(data, textStatus, errorThrown) {
            console.log(data);
            //   console.log(data);
            //   console.log(textStatus);
            //   console.log(errorThrown);
        });
    });
    $('#create-anonymous-report-moss-form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        $.ajax({
            dataType: 'json',
            url: 'process/create_anonymous_report_moss.php',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            },
        }).done(function(data) {
            console.log(data);
            if (data.success) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                $('#success-modal').foundation('open');
            } else {

            }
        }).fail(function(data, textStatus, errorThrown) {
            console.log(data);
            //   console.log(data);
            //   console.log(textStatus);
            //   console.log(errorThrown);
        });
    });


    $('#create-emma-sos-button').click(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/check_active_sos.php',
            type: 'POST',
        }).done(function(data) {
            if (data.success) {
                console.log(data);
                if(data['check'] === '1'){
                    $('#sos_latitude').append(data['return'][0]['help_lat']);
                    $('#sos_longitude').append(data['return'][0]['help_lng']);
                    $('#sent-emma-sos-modal').foundation('open');
                }else{
                    start_emma_timer();
                    $('#create-emma-sos-modal').foundation('open');
                }
            } else {

            }
        }).fail(function(data, textStatus, errorThrown) {
            //   console.log(data);
            //   console.log(textStatus);
            //   console.log(errorThrown);
        });
    });

    $('#create-lockdown-form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        $.ajax({
            dataType: 'json',
            url: 'process/create_lockdown.php',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            },
        }).done(function(data) {
            console.log(data);
            if (data.success) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                $('#create-lockdown-modal').foundation('open');
                $('#emma_lockdown_latitude_show').append(data['lat']);
                $('#emma_lockdown_longitude_show').append(data['lng']);
                $('#show_lockdown_create').hide();
                $('#show_lockdown_sent').show();
            } else {

            }
        }).fail(function(data, textStatus, errorThrown) {
            console.log(data);
            //   console.log(data);
            //   console.log(textStatus);
            //   console.log(errorThrown);
        });
    });

    $('#click_emma_lockdown_button').click(function (e) {
        e.preventDefault();

        console.log('click');
        $("#create-lockdown-form").submit();
    });

    $('#create-lockdown-drill-form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        $.ajax({
            dataType: 'json',
            url: 'process/create_lockdown.php',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            },
        }).done(function(data) {
            console.log(data);
            if (data.success) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                $('#create-lockdown-drill-modal').foundation('open');
                $('#emma_lockdown_drill_latitude_show').append(data['lat']);
                $('#emma_lockdown_drill_longitude_show').append(data['lng']);
                $('#show_lockdown_drill_create').hide();
                $('#show_lockdown_drill_sent').show();
            } else {

            }
        }).fail(function(data, textStatus, errorThrown) {
            console.log(data);
            //   console.log(data);
            //   console.log(textStatus);
            //   console.log(errorThrown);
        });
    });

    $('#click_emma_lockdown_drill_button').click(function (e) {
        e.preventDefault();
        console.log('click');
        $("#create-lockdown-drill-form").submit();

    });

    $('#create-modified-lockdown-form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        $.ajax({
            dataType: 'json',
            url: 'process/create_modified_lockdown.php',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            },
        }).done(function(data) {
            console.log(data);
            if (data.success) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                $('#create-modified-lockdown-modal').foundation('open');
                $('#emma_modified_lockdown_latitude_show').append(data['lat']);
                $('#emma_modified_lockdown_longitude_show').append(data['lng']);
                $('#show_modified_lockdown_create').hide();
                $('#show_modified_lockdown_sent').show();
            } else {

            }
        }).fail(function(data, textStatus, errorThrown) {
            console.log(data);
            //   console.log(data);
            //   console.log(textStatus);
            //   console.log(errorThrown);
        });
    });

    $('#click_emma_modified_lockdown_button').click(function (e) {
        e.preventDefault();
        console.log('click');
        $("#create-modified-lockdown-form").submit();

    });
    $('#emma_lockdown_cancel_button').click(function(){
        if($('#emma_lockdown_cancel_button').text() != 'Back') {
            $('#create-lockdown-modal').foundation('close');
        }
        else{
            window.location.reload();
        }
    });
    $('#emma_lockdown_drill_cancel_button').click(function(){
        if($('#emma_lockdown_drill_cancel_button').text() != 'Back') {
            $('#create-lockdown-drill-modal').foundation('close');
        }
        else{
            window.location.reload();
        }
    });
    $('#emma_modified_lockdown_cancel_button').click(function(){
        if($('#emma_modified_lockdown_cancel_button').text() != 'Back') {
            $('#create-modified-lockdown-modal').foundation('close');
        }
        else{
            window.location.reload();
        }
    });

    $('#emma_sos_cancel_button').click(function(){
        if($('#emma_sos_cancel_button').text() != 'Back') {
            timer_triggered = true; //Setting this to true just means the timer won't execute code.
            $('#create-emma-sos-modal').foundation('close');
            $('#pin-modal-close-x').css('display', 'none'); //set to none so it can't be cancelled without a pin.
            $('#enter-pin-modal').attr('data-call', 'emma_sos');
            $('#enter-pin-modal').foundation('open');
        }else{
            window.location.reload();
        }
    });

    //This is a function that pulls up the pin modal and based on button ID it does different things.
    pull_up_pin = function(button) {


            if (button.id == "creating-response") {
                reached_max = false;
                password_field.value = null;

                form = $('#response-card-form');
                hidden_form = $('#hidden_form');
                hidden_form.append(form);
                $('#enter-pin-modal').foundation('open');
                $('#enter-pin-modal').attr("data-call", "response");
            }
            else if (button.id == "creating-event") {
                //divToMove is the div that holds the user inputed information.
                reached_max = false;
                password_field.value = null;
                switch_form_event = button.closest('.create-event-form');
                $(switch_form_event).attr("data-touse", "this");


                description = $(switch_form_event).find('.description');
                comments = $(switch_form_event).find('.create-event-description');

              //  console.log($(description).val());
              //  console.log($(comments).val());

                if($(description).val().replace(/^\s+|\s+$/g, "").length === 0 || $(comments).val().replace(/^\s+|\s+$/g, "").length === 0) {
                    alert('Please fillout more form information');
               //     console.log($(description).val());
                }
                else {
                    //This is the hidden-div that we append divToMove to prevent auto submission
                    //hiddenDiv = $('#hidden_inner_div');
                    //we set the name to the divToMove's parent id ('create-evacuate-modal') for example. This lets us switch it back later after we are done using it.
                    parent = switch_form_event.closest('.reveal');
                    //hiddenDiv.attr("name", parent.id.toString());
                    //hide the divToMove so we don't see it but it doesn't auto submit either.
                    switch_form_event.style.display = "none";
                    //append it to the hidden div so it is separated.
                    //console.log(hiddenDiv);
                    //$(hiddenDiv).append(switch_form_event);
                    //Open up our pin modal.
                    $('#enter-pin-modal').attr("data-call", "event"); //set the data call type.
                    $('#enter-pin-modal').foundation('open');
                }
            }


    };
    $('#close-pin-button').click(function(){
        //Get the hidden div, and on close we move it back to the position that has a corresponding ID
        //to the switch_form ID or Name. This is so that when they try to create a event the form will actually be there, unhidden.
        parent.append(switch_form_event);
        switch_form_event.style.display = "inline";


    });

//Response card event listeners.
    $('#response-card-form').submit(function(e) {
        e.preventDefault();
        let form = $(this);
        $.ajax({
            dataType: 'json',
            url: 'process/create_response.php',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function() {
                $('#thinking-modal').foundation('open');
            },
        }).done(function(data) {
            $('#thinking-modal').foundation('close');
            if (data.success) {
                $('#success-modal').foundation('open');
                location.reload();
                //TODO: SUCCESS MESSAGE
            } else {
                //TODO: FAILED MESSAGE
            }
        }).fail(function(data, textStatus, errorThrown) {
         //   console.log(data);
         //   console.log(textStatus);
         //   console.log(errorThrown);
        });
    });
    $('#close_response_card').click(function() {
        $('#response-card-modal').foundation('close');
    });

    $('#create-security-form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        $.ajax({
            dataType: 'json',
            url: 'process/create_security.php',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            }
        })
            .done(function (data) {
            //    console.log(data);
                $('#thinking-modal').foundation('close');
                if (data.success) {
                    // location.reload();
                    //TODO: SUCCESS MESSAGE
                } else {
                    //TODO: FAILED MESSAGE
                }
                //form.foundation('close');
            })
            .fail(function (data, textStatus, errorThrown) {
             //   console.log(data);
            //    console.log(textStatus);
             //   console.log(errorThrown);
            });
        return false;
    });

    $('#create-mass-communication-form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        $.ajax({
            dataType: 'json',
            url: 'process/create_mass_communication.php',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            }
        })
            .done(function (data) {
                console.log(data);
                $('#thinking-modal').foundation('close');
                if (data.success) {
                    if(data['text'] === true){
                        var numbers = data['numbers'];
                        var message = data['post']['notification'];
                        numbers = uniq(numbers);
                        console.log(numbers);
                        for(var i=0; i < numbers.length; i++) {
                            var response = sendText(numbers[i], message, 'mass_notification', data['notification_id']);
                            console.log(response);
                        }
                        $('#success-modal-mass').foundation('open');
                    }else {
                        //location.reload();
                        //TODO: SUCCESS MESSAGE
                        $('#success-modal-mass').foundation('open');
                    }
                } else {
                    //TODO: FAILED MESSAGE
                }
                //form.foundation('close');
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        function sendText(number,message,type,id) {
            $.ajax({
                type: "POST",
                url: "https://igotcut.com",
                data: { number: number, message:  message, carrier: carrier},
                success: function(res) {
                    if(res.success) {
                        $.ajax({
                            dataType: 'json',
                            url: 'process/log_text.php',
                            type: 'POST',
                            data: {number: number, message: message, success: 1, type: type, id: id},
                            beforeSend: function () {

                            }
                        })
                            .done(function (data) {
                                console.log(data);
                                if (data.success) {
                                    return 'success';
                                } else {
                                    //TODO: FAILED MESSAGE
                                }
                                //form.foundation('close');
                            })
                            .fail(function (data, textStatus, errorThrown) {
                                console.log(data);
                                console.log(textStatus);
                                console.log(errorThrown);
                            });
                    }else {
                        $('#mass-text-error-header').removeAttr('hidden');
                        $('#mass-text-error').append('<p style="color: red">'+ number +'</p>');
                        console.log(res);
                        $.ajax({
                            dataType: 'json',
                            url: 'process/log_text.php',
                            type: 'POST',
                            data: {number: number, message: message, success: 0, type: type, id: id},
                            beforeSend: function () {

                            }
                        })
                            .done(function (data) {
                                console.log(data);
                                if (data.success) {
                                    return 'success';
                                } else {
                                    //TODO: FAILED MESSAGE
                                }
                                //form.foundation('close');
                            })
                            .fail(function (data, textStatus, errorThrown) {
                                console.log(data);
                                console.log(textStatus);
                                console.log(errorThrown);
                            });
                    }
                },
                error: function (res) {

                }
            });
        }
        return false;
    });

    function uniq(a) {
        return a.sort().filter(function(item, pos, ary) {
            return !pos || item != ary[pos - 1];
        });
    }
    $('#mass-all-plan-select').click(function () {
        $('.mass-plan-select').click();
    });

    $('.all-select').click(function () {
        $('.group-select').click();
    });

    $(document).on('change','#change_plan_select',function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/change_plan.php',
            type: 'POST',
            data: {'plan': $(this).val()},
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            }
        })
            .done(function (data) {
                //console.log(data);
                $('#error-list').empty();
                $('#thinking-modal').foundation('close');
                if (data.success) {
                    window.location.href = 'index.php';
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                //console.log(data);
          //      console.log(textStatus);
            //    console.log(errorThrown);
            });
    });
    $(document).on('change','#change_region_select',function (){
        console.log('clicked');
        var region = $(this).find("option:selected").val();
        $("#change_plan_select option").each(function () {
            $(this).show();
        })
        if(region === 'all'){
            $("#change_plan_select option").each(function () {
                $(this).show();
            })
        }else {
            $("#change_plan_select option").each(function () {
                $(this).hide();
                if($(this).data('region') !== undefined && $(this).data('region') !== '') {
                    console.log($(this).data('region'));
                    console.log(region);
                    var regionstring = String($(this).data('region'));
                    var regionarr = regionstring.split(' ');
                    for(var x in regionarr)
                    {
                        if (region == regionarr[x]) {
                            $(this).show();
                        }
                    }
                }
            })
        }
    });

    $('.typebutton').click(function () {
        $('#contact-us-type').val($(this).data('type'));
    });

    $('#mass-notification-responses').click(function () {
        if($(this).is(":checked")){
            $('#mass-notification-responses-amount-container').removeAttr('hidden');
        }else {
            $('#mass-notification-responses-amount-container').attr('hidden', true);
        }
    });

    $('#alert-email-mass').click(function () {
        if($(this).is(":checked")){
            $('#email-script-container').removeAttr('hidden');
        }else {
            $('#email-script-container').attr('hidden', true);
        }
    });

});