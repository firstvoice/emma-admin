$(document).ready(function () {
   $('#userallcheck').click(function () {
      $('.usercheckbox').click();
   });

    // $('#email-users-form').submit(function (e) {
    //     e.preventDefault();
    //     $.ajax({
    //         dataType: 'json',
    //         url: 'process/send_user_emails.php',
    //         type: 'POST',
    //         data: $(this).serialize(),
    //         beforeSend: function () {
    //             $('#thinking-modal').foundation('open');
    //         }
    //     })
    //         .done(function (data) {
    //             console.log(data);
    //             $('#thinking-modal').foundation('close');
    //             $('#error-list').empty();
    //             if (data.success) {
    //                 $('#success-modal').foundation('open');
    //             } else {
    //                 for (var error in data['errors']) {
    //                     $('#error-list').append(data['errors'][error] + '<br />');
    //                 }
    //                 $('#fail-modal').foundation('open');
    //             }
    //         })
    //         .fail(function (data, textStatus, errorThrown) {
    //             $('#thinking-modal').foundation('close');
    //             console.log(data);
    //             console.log(textStatus);
    //             console.log(errorThrown);
    //         });
    //
    //
    // });

    var usersTable = $('#users-table').DataTable({
        'dom': 'Brtip',
        'paging': true,
        'pageLength': 200,
        'scrollCollapse': true,
        'processing': true,
        'serverSide': true,
        'deferRender': true,
        "initComplete": function(settings, json) {
            removeEMMA('#users-table');
        },
        "order": [[0,'asc']],
        'ajax': {
            'url': 'process/get_users_admin.php',
            'type': 'GET',
            'data': {
                'userid': user['id'],
            },
        },
        'columns': [
            {
                'data': 'username',
                'render': function (data, type, row, meta) {
                    if(row['password'][0] !== '*') {
                        return '<a href=\'dashboard.php?content=user&id=' + row['id'] + '\' style="color: red" data-id="'+ row['id'] +'">' +
                            data + '</a>';
                    }else {
                        return '<a href=\'dashboard.php?content=user&id=' + row['id'] + '\' data-id="'+ row['id'] +'">' +
                            data + '</a>';
                    }
                },
            },
            {'data': 'firstname'},
            {'data': 'lastname'},
            {
                'data': 'group',
                'orderable': false

            },
            {
                'data': 'plan',
                'orderable': false

            },
        ],
        createdRow: function (row, data, dataIndex) {
            if(data['password'][0] !== '*') {
                $(row).addClass('select-unregistered');
            }
        },
    });

    $('#select-all').click(function () {
        console.log('click');
        $('#thinking-modal').foundation('open');
        $('.select-unregistered').click();
        setTimeout(
            function()
            {
                $('#thinking-modal').foundation('close');
            }, 500);
    });

    $('#users-table tbody').on( 'click', 'tr', function () {
        console.log('click');
        $(this).toggleClass('selected');
        $(this).attr('data-value', $(this).find('a').data('id'));
    });

    $('#email-users-form').submit( function (e) {
        var userarr = '';
        $('#users-table tbody .selected').each(function() {
            if(userarr === ''){
                userarr = $(this).data('value');
            }else {
                userarr = userarr + ',' + $(this).data('value');
            }
        });
        if(userarr !== ''){
            console.log(userarr);
            e.preventDefault();
            $.ajax({
                dataType: 'json',
                url: 'process/send_user_emails.php',
                type: 'POST',
                data: {'user': userarr},
                beforeSend: function () {
                    $('#thinking-modal').foundation('open');
                },
            })
                .done(function (data) {
                    console.log(data);
                    $('#thinking-modal').foundation('close');
                    $('#error-list').empty();
                    if (data.success) {
                        $('#success_modal').foundation('open');
                    } else {
                        for (var error in data['errors']) {
                            $('#error-list').append(data['errors'][error] + '<br />');
                        }
                        $('#fail-modal').foundation('open');
                    }
                })
                .fail(function (data, textStatus, errorThrown) {
                    $('#thinking-modal').foundation('close');
                    console.log(data);
                    console.log(textStatus);
                    console.log(errorThrown);
                });
        }else {
            alert('Please Select Users');
        }
    } );

    $('#users-table thead th.text-search').each(function (i) {
        let title = $('#users-table thead th').eq($(this).index()).text();
        $(this)
            .html(
                '<input class="text-search" type="text" placeholder="Search ' + title +
                '" data-index="' + i + '" />');
    });

    $(usersTable.table().container())
        .on('keyup', 'thead input.text-search', function () {
            usersTable
                .column($(this).data('index'))
                .search(this.value)
                .draw();
        });

    function removeEMMA(table){
        if(!user['username'].includes('think-safe.com') || !user['username'].includes('emmauser.com')){
            $(table + ' tr').each(function() {
                // console.log(this);
                username = $(this).find('td:first').find('a').text();
                // console.log(username);
                if(username.includes('think-safe.com') || username.includes('emmauser.com')){
                    $(this).hide();
                }
            });
        }
    }
});