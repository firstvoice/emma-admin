$(document).ready(function () {
  var usersTable = $('#users-table').DataTable({
    'dom': 'Brtip',
    'paging': true,
    'pageLength': 50,
    'scrollCollapse': true,
    'processing': true,
    'serverSide': true,
    'deferRender': true,
      "initComplete": function(settings, json) {
          removeEMMA('#users-table');
      },
      "order": [[0,'asc']],
      'ajax': {
      'url': 'process/get_users.php',
      'type': 'GET',
      'data': {
        'emma-plan-id': user['emma_plan_id'],
      },
    },
    'columns': [
      {
        'data': 'username',
        'render': function (data, type, row, meta) {
          return '<a href=\'dashboard.php?content=user&id=' + row['id'] + '\'>' +
            data + '</a>';
        },
      },
      {'data': 'firstname'},
      {'data': 'lastname'},
        {
            'data': 'group',
            'orderable': false

        },
    ],
  });

  $('#users-table thead th.text-search').each(function (i) {
    let title = $('#users-table thead th').eq($(this).index()).text();
    $(this)
      .html(
        '<input class="text-search" type="text" placeholder="Search ' + title +
        '" data-index="' + i + '" />');
  });

  $(usersTable.table().container())
    .on('keyup', 'thead input.text-search', function () {
      usersTable.column($(this).data('index')).search(this.value).draw();
      removeEMMA('#users-table');
    });


    var users_inactive_Table = $('#users-inactive-table').DataTable({
        'dom': 'Brtip',
        'paging': true,
        'pageLength': 50,
        'scrollCollapse': true,
        'processing': true,
        'serverSide': true,
        'deferRender': true,
        "initComplete": function(settings, json) {
            removeEMMA('#users-inactive-table');
        },
        "order": [[0,'asc']],
        'ajax': {
            'url': 'process/get_users_inactive.php',
            'type': 'GET',
            'data': {
                'emma-plan-id': user['emma_plan_id'],
            },
        },
        'columns': [
            {
                'data': 'username',
                'render': function (data, type, row, meta) {
                    return '<a href=\'dashboard.php?content=user&id=' + row['id'] + '\'>' +
                        data + '</a>';
                },
            },
            {'data': 'firstname'},
            {'data': 'lastname'},
            {
                'data': 'group',
                'orderable': false
            },
        ],
    });

    $('#users-inactive-table thead th.text-search').each(function (i) {
        let title = $('#users-inactive-table thead th').eq($(this).index()).text();
        $(this)
            .html(
                '<input class="text-search" type="text" placeholder="Search ' + title +
                '" data-index="' + i + '" />');
    });

    $(users_inactive_Table.table().container())
        .on('keyup', 'thead input.text-search', function () {
            users_inactive_Table
                .column($(this).data('index'))
                .search(this.value)
                .draw();
            removeEMMA('#users-inactive-table');
        });















  var guestUsersTable = $('#guest-users-table').DataTable({
    'dom': 'Brtip',
    'paging': true,
    'pageLength': 200,
    'scrollCollapse': true,
    'processing': true,
    'serverSide': true,
    'deferRender': true,
      "initComplete": function(settings, json) {
          removeEMMA('#guest-users-table');
      },
    "order": [[0,'asc']],
      'ajax': {
      'url': 'process/get_guest_users.php',
      'type': 'GET',
      'data': {
        'emma-plan-id': user['emma_plan_id'],
      },
    },
    'columns': [
      {
        'data': 'username',
        'render': function (data, type, row, meta) {
          return '<a href=\'dashboard.php?content=user&id=' + row['id'] + '\'>' +
            data + '</a>';
        },
      },
      {'data': 'firstname'},
      {'data': 'lastname'},
      {
        'data': 'group',
        'orderable': false
      },
        {
            'render': function (data, type, row, meta) {
                if(row['emp_active'] == 1)
                {
                    return '<p style="color: green;">Active</p>';
                }
                else{
                    return '<p style="color: red;">Inactive</p>';
                }
            },
        },
    ],
  });

  $('#guest-users-table thead th.text-search').each(function (i) {
    let title = $('#guest-users-table thead th').eq($(this).index()).text();
    $(this)
      .html(
        '<input class="text-search" type="text" placeholder="Search ' + title +
        '" data-index="' + i + '" />');
  });

  $(guestUsersTable.table().container())
    .on('keyup', 'thead input.text-search', function () {
      guestUsersTable
        .column($(this).data('index'))
        .search(this.value)
        .draw();
        removeEMMA('#guest-users-table');
    });

  $('#user_assign_group').click(function () {
    $('#update_group_modal').foundation('open');
  });

  $('#assign_group_submit').click(function () {
    $.ajax({
      dataType: 'json',
      url: 'process/assign_user_list_to_group.php',
      type: 'POST',
      data: $('#users_form').serialize() + '&' +
        $('#assign_group_form').serialize(),
      beforeSend: function () {
        $('#thinking-modal').foundation('open');
      },
    })
      .done(function (data) {
        console.log(data);
        $('#thinking-modal').foundation('close');
        $('#error-list').empty();
        if (data.success) {
          $('#success_modal').foundation('open');
        } else {
          for (var error in data['errors']) {
            $('#error-list').append(data['errors'][error] + '<br />');
          }
          $('#fail-modal').foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        $('#thinking-modal').foundation('close');
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
  });

  $('#user_remove_group').click(function () {
    $('#remove_group_modal').foundation('open');
  });

  $('#remove_group_submit').click(function () {
    $.ajax({
      dataType: 'json',
      url: 'process/remove_user_list_from_groups.php',
      type: 'POST',
      data: $('#users_form').serialize() + '&' +
        $('#remove_group_form').serialize(),
      beforeSend: function () {
        $('#thinking-modal').foundation('open');
      },
    })
      .done(function (data) {
        console.log(data);
        $('#thinking-modal').foundation('close');
        $('#error-list').empty();
        if (data.success) {
          $('#success_modal').foundation('open');
        } else {
          for (var error in data['errors']) {
            $('#error-list').append(data['errors'][error] + '<br />');
          }
          $('#fail-modal').foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        $('#thinking-modal').foundation('close');
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
  });

  // $('#user_assign_site').click( function() {
  //     $('#update_site_modal').foundation('open');
  // });
  //
  //
  // $('#assign_site_submit').click( function() {
  //     $.ajax({
  //         dataType: 'json',
  //         url: 'process/assign_user_list_to_site.php',
  //         type: 'POST',
  //         data: $('#users_form').serialize() + "&" + "site_id=" + $("#site_id").val(),
  //         beforeSend: function () {
  //             $('#thinking-modal').foundation('open');
  //         }
  //     })
  //         .done(function (data) {
  //             console.log(data);
  //             $('#thinking-modal').foundation('close');
  //             $('#error-list').empty();
  //             if (data.success) {
  //                 $('#success_modal').foundation('open');
  //             } else {
  //                 for (var error in data['errors']) {
  //                     $('#error-list').append(data['errors'][error] + '<br />');
  //                 }
  //                 $('#fail-modal').foundation('open');
  //             }
  //         })
  //         .fail(function (data, textStatus, errorThrown) {
  //             $('#thinking-modal').foundation('close');
  //             console.log(data);
  //             console.log(textStatus);
  //             console.log(errorThrown);
  //         });
  // });

  $('#user_deactivate').click(function () {
    $.ajax({
      dataType: 'json',
      url: 'process/deactivate_user_list.php',
      type: 'POST',
      data: $('#users_form').serialize(),
      beforeSend: function () {
        $('#thinking-modal').foundation('open');
      },
    })
      .done(function (data) {
        console.log(data);
        $('#thinking-modal').foundation('close');
        $('#error-list').empty();
        if (data.success) {
          $('#success_modal').foundation('open');
        } else {
          for (var error in data['errors']) {
            $('#error-list').append(data['errors'][error] + '<br />');
          }
          $('#fail-modal').foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        $('#thinking-modal').foundation('close');
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
  });



    var setupUsersTable = $('#setup-users-table').DataTable({
        'dom': 'Brtip',
        'paging': true,
        'pageLength': 200,
        'scrollCollapse': true,
        'processing': true,
        'serverSide': true,
        'deferRender': true,
        "initComplete": function(settings, json) {
            removeEMMA('#setup-users-table');
        },
        "order": [[0,'asc']],
        'ajax': {
            'url': 'process/get_setup_users.php',
            'type': 'GET',
            'data': {
                'emma-plan-id': user['emma_plan_id'],
            },
        },
        'columns': [
            {
                'data': 'username',
                'render': function (data, type, row, meta) {
                    return '<a href=\'dashboard.php?content=user&id=' + row['id'] + '\'>' +
                        data + '</a>';
                },
            },
            {'data': 'firstname'},
            {'data': 'lastname'},
        ],
    });

    $('#setup-users-table thead th.text-search').each(function (i) {
        let title = $('#setup-users-table thead th').eq($(this).index()).text();
        $(this)
            .html(
                '<input class="text-search" type="text" placeholder="Search ' + title +
                '" data-index="' + i + '" />');
    });

    $(setupUsersTable.table().container()).on('keyup', 'thead input.text-search', function () {
            setupUsersTable
                .column($(this).data('index'))
                .search(this.value)
                .draw();
                removeEMMA('#setup-users-table');
    });
    function removeEMMA(table){
        if(!user['username'].includes('think-safe.com') || !user['username'].includes('emmauser.com')){
            $(table + ' tr').each(function() {
                // console.log(this);
                username = $(this).find('td:first').find('a').text();
                // console.log(username);
                if(username.includes('think-safe.com') || username.includes('emmauser.com')){
                    $(this).hide();
                }
            });
        }
    }

});