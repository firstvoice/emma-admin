
$(document).ready(function () {

    $('#deactivate-geofence-form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/deactivate_geofence.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            },
        })
            .done(function (data) {
                console.log(data);
                $('#thinking-modal').foundation('close');
                $('#error-list').empty();
                if (data.success) {
                    $('#success_modal').foundation('open');
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        return false;
    });

    $('#reactivate-geofence-form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/reactivate_geofence.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            },
        })
            .done(function (data) {
                console.log(data);
                $('#thinking-modal').foundation('close');
                $('#error-list').empty();
                if (data.success) {
                    $('#success_modal').foundation('open');
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        return false;
    });

    $('#new-message-form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/add_geofence_message.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            },
        })
            .done(function (data) {
                console.log(data);
                $('#thinking-modal').foundation('close');
                $('#error-list').empty();
                if (data.success) {
                    $('#success_modal').foundation('open');
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
    });

    $('#select-geocode-script').change(function () {
        var text = $(this).find('option:selected').val();
        $('#create-geocode-description').val('');
        $('#create-geocode-description').val(text);
    });

    var element = document.getElementById("message-center");
    element.scrollTop = element.scrollHeight;

});

let map;
let bounds;
let updateInterval = 5000;
let infowindow;
let interval = 60;

function initMap() {
    let event = {
        lat: parseFloat(mainEvent['center_lat']),
        lng: parseFloat(mainEvent['center_lng']),
    };
    map = new google.maps.Map(document.getElementById('event-map'), {
        zoom: 17,
        center: event,
    });

    // infowindow = new google.maps.InfoWindow({
    //     content: 'AEDs',
    // });
if(mainEvent['type'] === 'Rectangle') {
    var squareCoords = [
        {lat: parseFloat(mainEvent['nw_corner_lat']), lng: parseFloat(mainEvent['nw_corner_lng'])},
        {lat: parseFloat(mainEvent['ne_corner_lat']), lng: parseFloat(mainEvent['ne_corner_lng'])},
        {lat: parseFloat(mainEvent['se_corner_lat']), lng: parseFloat(mainEvent['se_corner_lng'])},
        {lat: parseFloat(mainEvent['sw_corner_lat']), lng: parseFloat(mainEvent['sw_corner_lng'])},
    ];

    // Construct the polygon.
    var fence = new google.maps.Polygon({
        paths: squareCoords,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 3,
        fillColor: '#ffffff',
        fillOpacity: 0.35
    });
    fence.setMap(map);
}
if(mainEvent['type'] === 'Circle'){
    // Construct the circle.
    var fence = new google.maps.Circle({
        center: {lat: parseFloat(mainEvent['center_lat']), lng: parseFloat(mainEvent['center_lng'])},
        radius: (parseFloat(mainEvent['radius']) * 1609.344),
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 3,
        fillColor: '#ffffff',
        fillOpacity: 0.35
    });
    fence.setMap(map);
    console.log(fenceChildren);
    for(var i in fenceChildren){
        var fence = new google.maps.Circle({
            center: {lat: parseFloat(fenceChildren[i]['lat']), lng: parseFloat(fenceChildren[i]['lng'])},
            radius: (parseFloat(fenceChildren[i]['rad']) * 1609.344),
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 3,
            fillColor: '#ffffff',
            fillOpacity: 0.35
        });
        fence.setMap(map);
    }
}


    // let marker = new google.maps.Marker({
    //     position: event,
    //     icon: 'img/event.png',
    //     map: map,
    // });
    bounds = new google.maps.LatLngBounds();
    bounds.extend(event);
}