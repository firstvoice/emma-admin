marker_array = [];
map_array = [];
function initMap() {

  var loc = {lat: aed['latitude'] == null ? 0.0 : parseFloat(aed['latitude']), lng: aed['longitude'] == null ? 0.0 : parseFloat(aed['longitude'])};
  var map = new google.maps.Map(
    document.getElementById('aed-map'), {zoom: 12, center: loc});
  var icon = 'img/orange-dot.png';
  if(aed['image']){
    if(aed['image'] != 'img/orange-dot.png') {
      icon = {
        url: aed['image'],
        scaledSize: new google.maps.Size(25, 25)
      }
    }
  }
  marker_array.push();
  var marker = new google.maps.Marker({position: loc, map: map, icon: icon});
  var edit_map = new google.maps.Map(
    document.getElementById('edit-map'), {zoom: 12, center: loc});
  var edit_marker = new google.maps.Marker({position: loc, map: edit_map, icon: icon, draggable: true});
  map_array.push(edit_map);
  marker_array.push(edit_marker);
  google.maps.event.addListener(edit_marker, 'dragend', function(event){
    //Update each lat/lng position on drag.
    input_lat.val(event.latLng.lat());
    input_lng.val(event.latLng.lng());
    let pos={latitude: input_lat.val(),longitude: input_lng.val()};
    //calc address, it wont fire the on change event when we change the values like this.
    geocodePosition(pos);
  });

}

function geocodePosition(pos)
{
  //console.log(pos);
  //Sets closest address into input for each event modal..
  geocoder = new google.maps.Geocoder();
  geocoder.geocode
  ({
      latLng: new google.maps.LatLng(pos.latitude,pos.longitude)
    },
    function(results, status)
    {
      if (status == google.maps.GeocoderStatus.OK)
      {
        $('input[name="address"]').val(results[0].formatted_address);
      }
      else
      {
        $('input[name="address"]').val('N/A');
      }
    }
  );
}

$(document).ready(function () {
  edit_aed = function () {
    $('#edit-aed-modal').foundation('open');
    return false;
  };

  $('#status_bar').change(function () {
    if ($('#status_bar option:selected').text() == 'Active') {
      $('#status_bar').attr("style", "color: green;");
    }
    else if ($('#status_bar option:selected').text() == 'Inactive') {
      $('#status_bar').attr("style", "color: red;");
    }
  });

  $('#edit-aed-form').submit(function (e) {
    e.preventDefault();
    form = $('#edit-aed-form').serializeArray();
    $.ajax({
        dataType: 'json',
        url: 'process/edit_aed.php',
        type: 'POST',
        data: $.param(form),
        beforeSend: function () {
            THINKINGMODAL.foundation('open');
          },
      })
      .done(function (data) {
        console.log(data);
        THINKINGMODAL.foundation('close');
        if (data.success) {
          $('#aed-success-modal').foundation('open');
        } else {
          FAILMODAL.foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        console.log(data);
        //  console.log(textStatus);
        //  console.log(errorThrown);
      });
    return false;
  });

  $('#deactivate-aed-form').submit(function (e) {
    e.preventDefault();
    $.ajax({
      dataType: 'json',
      url: 'process/deactivate_aed.php',
      type: 'POST',
      data: $(this).serialize(),
      beforeSend: function () {
        THINKINGMODAL.foundation('open');
      },
    })
      .done(function (data) {
        THINKINGMODAL.foundation('close');
        if (data.success) {
          $('#aed-success-modal').foundation('open');
        } else {
          FAILMODAL.foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
    return false;
  });

  $('#delete-aed-form').submit(function (e) {
    e.preventDefault();
    $.ajax({
      dataType: 'json',
      url: 'process/delete_aed.php',
      type: 'POST',
      data: $(this).serialize(),
      beforeSend: function () {
        THINKINGMODAL.foundation('open');
      },
    })
      .done(function (data) {
        THINKINGMODAL.foundation('close');
        if (data.success) {
          $('#aed-success-modal').foundation('open');
        } else {
          FAILMODAL.foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
    return false;
  });

  input_lat = $('#entered_lat');
  input_lng = $('#entered_lng');

  input_lat.change(function(){
    if(input_lat.val() != null && input_lng.val() != null)
    {
      let pos={latitude: input_lat.val(),longitude: input_lng.val()};
      geocodePosition(pos);
    }
  });

  input_lng.change(function(){
    if(input_lat.val() != null && input_lng.val() != null)
    {
      let pos={latitude: input_lat.val(),longitude: input_lng.val()};
      geocodePosition(pos);
    }
  });

  function resetAccessories() {
    $('#pad-row').hide();
    $('#pad-select').empty();
    $('#pad-select').append('<option value="" selected="selected" hidden="hidden">-- Select Pad --</option>');
    $('#pak-row').hide();
    $('#pak-select').empty();
    $('#pak-select').append('<option value="" selected="selected" hidden="hidden">-- Select Pak --</option>');
    $('#battery-row').hide();
    $('#battery-select').empty();
    $('#battery-select').append('<option value="" selected="selected" hidden="hidden">-- Select Battery --</option>');
    $('#accessory-row').hide();
    $('#accessory-select').empty();
    $('#accessory-select').append('<option value="" selected="selected" hidden="hidden">-- Select Accessory --</option>');
  }

  $('#edit_aed_form_brand_id').change(function () {
    const brand_id = $('#edit_aed_form_brand_id option:selected').val();
    $.ajax({
      url: 'process/get_aed_models.php',
      type: 'GET',
      data: {'brand-id': brand_id},
      beforeSend: function () {
        resetAccessories();
        $('#edit_aed_form_model_id').empty();
        $('#edit_aed_form_model_id').append('<option value="" selected="selected" hidden="hidden">-- Select Model --</option>');
      },
    })
      .done(function (data) {
        data = JSON.parse(data);
        data['models'].forEach((model) => {
          $('#edit_aed_form_model_id').append("<option value='"+model['aed_model_id']+"'>"+model['model']+"</option>");
        });
        console.log(data);
      })
      .fail(function (data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });

    $('#edit_aed_form_model_id').change(function () {
      const model_id = $('#edit_aed_form_model_id option:selected').val();
      $.ajax({
        url: 'process/get_accessory_types.php',
        type: 'POST',
        data: {'model-id': model_id},
        beforeSend: function () {
          resetAccessories();
        },
      })
        .done(function (data) {
          data = JSON.parse(data);
          if(data['pads'].length > 0) $('#pad-row').show();
          data['pads'].forEach((pad) => {
            $('#pad-select').append('<option value="'+pad['aed_pad_type_id']+'">'+pad['type']+'</option>');
          });
          if(data['paks'].length > 0) $('#pak-row').show();
          data['paks'].forEach((pak) => {
            $('#pak-select').append('<option value="'+pak['aed_pak_type_id']+'">'+pak['type']+'</option>');
          });
          if(data['batteries'].length > 0) $('#battery-row').show();
          data['batteries'].forEach((battery) => {
            $('#battery-select').append('<option value="'+battery['aed_battery_type_id']+'">'+battery['type']+'</option>');
          });
          if(data['accessories'].length > 0) $('#accessory-row').show();
          data['accessories'].forEach((accessory) => {
            $('#accessory-select').append('<option value="'+accessory['aed_accessory_type_id']+'">'+accessory['type']+'</option>');
          });
          console.log(data);
        })
        .fail(function (data, textStatus, errorThrown) {
          console.log(data);
          console.log(textStatus);
          console.log(errorThrown);
        });
    })
  });

});