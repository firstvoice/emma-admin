function getBoundsSize(bounds) {
    var sizes, NE, SW, lat1, lat2, lng2, lng2, horizontalLatLng1, horizontalLatLng2, verticalLatLng1, verticalLatLng2, horizontal, vertical;

    // get the coordinates for the NE and SW corners
    NE = bounds.getNorthEast();
    SW = bounds.getSouthWest();

    // from that, figure out the latitudes and the longitudes
    lat1 =  NE.lat();
    lat2 =  SW.lat();

    lng1 =  NE.lng();
    lng2 =  SW.lng();

    // construct new LatLngs using the coordinates for the horizontal distance between lng1 and lng2
    horizontalLatLng1 = new google.maps.LatLng(lat1,lng1);
    horizontalLatLng2 = new google.maps.LatLng(lat1,lng2);

    // construct new LatLngs using the coordinates for the vertical distance between lat1 and lat2
    verticalLatLng1 = new google.maps.LatLng(lat2,lng1);
    verticalLatLng2 = new google.maps.LatLng(lat2,lng2);

    // work out the distance horizontally
    horizontal = getDistance(horizontalLatLng1, horizontalLatLng2);

    // work out the distance vertically
    vertical = getDistance(horizontalLatLng1, verticalLatLng1);

    // round to kilometres to 1dp
    sizes = {
        horizontalkm: convertMetresToKm(horizontal),
        verticalkm: convertMetresToKm(vertical),
        horizontalmeters: horizontal,
        verticalmeters: vertical,
        latLngs : {
            a: horizontalLatLng2,
            b: horizontalLatLng1,
            c: verticalLatLng1,
            d: verticalLatLng2
        }
    };

    return sizes;
}

function getDistance(point1, point2) {
    // computeDistanceBetween is fine if we only have 2 points, but if we have more we need to use computeLength
    return google.maps.geometry.spherical.computeDistanceBetween(point1, point2);
}

function convertMetresToKm(metres) {
    return Math.round(metres / 1000 *10)/10;    // distance in km rounded to 1dp
}

function getNewPoint(pointA, bearing, meters) {
    return google.maps.geometry.spherical.computeOffset(pointA, meters, bearing);
}

function getBearing(pointA, pointB) {
    return google.maps.geometry.spherical.computeHeading(pointA, pointB);
}

$(document).ready(function () {

    $('#create_geofence_form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/create_geofence_new.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            }
        })
            .done(function (data) {
                console.log(data);
                $('#error-list').empty();
                $('#thinking-modal').foundation('close');
                if (data.success) {
                    $('#success_modal').foundation('open');
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
    })

});
