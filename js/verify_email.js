$(document).ready(function () {

  $('#change-password').submit(function (e) {
    e.preventDefault();

    $.ajax({
      dataType: 'json',
      url: 'process/activate_user.php',
      type: 'POST',
      data: $(this).serialize(),
      beforeSend: function () {
        $('#thinking-modal').foundation('open');
      },
    })
      .done(function (data) {
        console.log(data);
        $('#error-list').empty();
        $('#thinking-modal').foundation('close');
        if (data.success) {
          if(data['admin-portal']) {
            $('#admin-login-info').show();
          } else {
            $('#admin-login-info').hide();
          }
          $('#success-modal').foundation('open');
        } else {
          for (var error in data['errors']) {
            $('#error-list').append(data['errors'][error] + '<br />');
          }
          $('#fail-modal').foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        $('#thinking-modal').foundation('close');
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
  });

});