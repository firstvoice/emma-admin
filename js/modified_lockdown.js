let map;
let bounds;
let updateInterval = 5000;
let infowindow;
let interval = 60;

function initMap() {
    let map = new google.maps.Map(document.getElementById('lockdown-map'), {
        zoom: 2,
        center: {lat: 0, lng: 0},
    });

    let infowindow = new google.maps.InfoWindow({
        content: 'Modified LOCKDOWN!',
    });

    var sosIds = [];
    var markers = [];

    var bounds = new google.maps.LatLngBounds();
    let sos = mainSos;
    let marker;
    marker = new google.maps.Marker({
        position: {
            lat: parseFloat(sos['created_lat']),
            lng: parseFloat(sos['created_lng']),
        },
        icon: 'img/help.png',
        map: map,
        user: sos['username'],
    });
    google.maps.event.addListener(marker, 'click', function () {
        var html = '' +
            '<h6>' + this.user + '</h6>' +
            '';
        infowindow.setContent(html);
        infowindow.open(map, this);
    });
    bounds.extend(marker.position);
    map.fitBounds(bounds);
}