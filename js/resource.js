$(document).ready(function () {

  $('#edit-resource-form').submit(function (e) {
    e.preventDefault();
    let form = $(this);
    $.ajax({
      dataType: 'json',
      url: 'process/edit_resource.php',
      type: 'POST',
      data: form.serialize(),
      beforeSend: function () {
        $('#thinking-modal').foundation('open');
      },
    })
      .done(function (data) {
        console.log(data);
        $('#thinking-modal').foundation('close');
        if (data.success) {
          $('#edit_success_modal').foundation('open');
          //TODO: SUCCESS MESSAGE
        } else {
          //TODO: FAILED MESSAGE
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
    return false;
  });

  $('#delete_resource_submit').click(function (e) {
    e.preventDefault();
    $.ajax({
      dataType: 'json',
      url: 'process/delete_resource.php',
      type: 'POST',
      data: $('#delete_resource_form').serialize(),
      beforeSend: function () {
        $('#thinking-modal').foundation('open');
      },
    })
      .done(function (data) {
        console.log(data);
        $('#thinking-modal').foundation('close');
        $('#error-list').empty();
        if (data.success) {
          $('#success_modal').foundation('open');
        } else {
          for (var error in data['errors']) {
            $('#error-list').append(data['errors'][error] + '<br />');
          }
          $('#fail-modal').foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        $('#thinking-modal').foundation('close');
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
  });
});