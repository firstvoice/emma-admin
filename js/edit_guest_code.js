$(document).ready(function () {

    $('#edit_code_form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/edit_guest_code.php',
            type: 'POST',
            data: $('#edit_code_form').serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            }
        })
            .done(function (data) {
                console.log(data);
                $('#thinking-modal').foundation('close');
                $('#error-list').empty();
                if (data.success) {
                    $('#success_modal').foundation('open');
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });


    });

    $('.time-edit').change(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/get_code_status.php',
            type: 'POST',
            data: {
                'start': $('#code-start').val(),
                'end': $('#code-end').val()
            },
            beforeSend: function () {
                $('#status-display').empty();
            }
        })
            .done(function (data) {
                console.log(data);
                $('#status-display').append('<h4 style="color: '+ data['color'] +'">'+data['status']+'</h4>');
            })
    })

});