var map;
var bounds;
var updateInterval = 5000;
var infowindow;
var responseIds = [];
var incidents = new vis.DataSet();

var curlCommandDiv = document.querySelector('.js-curl-command');
var isPushEnabled = false;


function getTimeline() {
    $.ajax({
        dataType: 'json',
        url: 'process/get_timeline.php',
        type: 'POST',
        data: {'id': mainEvent['emergency_id'], 'user': user['id']},
        beforeSend: function(request) {
            request.setRequestHeader('Authorization', 'Bearer ' + getJWT());
        },
    }).done(function(data) {
        if (data.success) {
            for (var i in data['incidents']) {
                var status = 'Unknown';
                var divClass = 'secondary';
                var border = '#000000';
                var color = '#3e739a';
                switch (data['incidents'][i]['status']) {
                    case '-2':
                        status = 'Alert';
                        divClass = 'system';
                        border = '#767676';
                        color = '#eaeaea';
                        break;
                    case '-1':
                        status = 'Pending';
                        divClass = 'secondary';
                        border = '#767676';
                        color = '#eaeaea';
                        break;
                    case '0':
                        status = 'All Clear';
                        divClass = 'success';
                        border = '#3adb76';
                        color = '#e1faea';
                        break;
                    case '1':
                        status = 'Need Help';
                        divClass = 'alert';
                        border = '#cc4b37';
                        color = '#f7e4e1';
                        break;
                    case '2':
                        status = 'Need Information';
                        divClass = 'warning';
                        border = '#ffae00';
                        color = '#fff3d9';
                        break;
                }
                if (incidents.get(data['incidents'][i]['id']) === null) {
                    incidents.add([
                        {
                            id: data['incidents'][i]['id'],
                            start: new Date(data['incidents'][i]['updated_date']).getTime(),
                            content: '',
                            title: '<b>Time:</b> ' +
                                moment(data['incidents'][i]['updated_date']).
                                format('M/D/YYYY h:mm:ss a') + '<br/><b>User:</b> ' +
                                data['incidents'][i]['user'] + '<br/><b>Status:</b> ' + status +
                                '<br/><b>Comments</b> ' + data['incidents'][i]['message'],
                            type: 'point',
                            className: divClass,
                        },
                    ]);
                    // timeline.focus(data['responses'][i]['emergency_response_id']);
                    // timeline.moveTo(data['incidents'][i]['updated_date']);
                    $('#message-center').append('' +
                        '<div data-closable class=\'callout alert-callout-subtle radius inset-shadow ' +
                        divClass + '\'>' +
                        '<strong>' + data['incidents'][i]['user'] + ' - ' +
                        moment(data['incidents'][i]['updated_date']).
                        format('M/D/YYYY h:mm:ss a') + '</strong>' +
                        '<p>' + data['incidents'][i]['message'] + '</p>' +
                        '</div>');
                    var messageCenterDiv = document.getElementById('message-center');
                    messageCenterDiv.scrollTop = messageCenterDiv.scrollHeight;
                    if (data['incidents'][i]['message'] === 'Event Closed') {
                        location.reload();
                    }
                }
            }
        } else {

        }
    }).fail(function(data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
    });
}

function getFeed() {
    $.ajax({
        dataType: 'json',
        url: 'process/get_feed.php',
        type: 'POST',
        data: {'user': user['id']},
        beforeSend: function(request) {
            request.setRequestHeader('Authorization', 'Bearer ' + getJWT());
        },
    }).done(function(data) {
        if (data.success) {
            for (var i in data['event']) {
                var status = 'Unknown';
                var divClass = 'secondary';
                var border = '#000000';
                var color = '#3e739a';
                switch (data['event'][i]['status']) {
                    case '-2':
                        status = 'Alert';
                        divClass = 'system';
                        border = '#767676';
                        color = '#eaeaea';
                        break;
                    case '-1':
                        status = 'Pending';
                        divClass = 'secondary';
                        border = '#767676';
                        color = '#eaeaea';
                        break;
                    case '0':
                        status = 'All Clear';
                        divClass = 'success';
                        border = '#3adb76';
                        color = '#e1faea';
                        break;
                    case '1':
                        status = 'Need Help';
                        divClass = 'alert';
                        border = '#cc4b37';
                        color = '#f7e4e1';
                        break;
                    case '2':
                        status = 'Need Information';
                        divClass = 'warning';
                        border = '#ffae00';
                        color = '#fff3d9';
                        break;
                }
                if (incidents.get(data['event'][i]['id']) === null) {
                    incidents.add([
                        {
                            id: data['event'][i]['id'],
                            start: new Date(data['event'][i]['updated_date']).getTime(),
                            content: '',
                            title: '<b>Time:</b> ' +
                            moment(data['event'][i]['updated_date']).
                            format('M/D/YYYY h:mm:ss a') + '<br/><b>User:</b> ' +
                            data['event'][i]['user'] + '<br/><b>Status:</b> ' + status +
                            '<br/><b>Comments</b> ' + data['event'][i]['message'],
                            type: 'point',
                            className: divClass,
                        },
                    ]);


                    // timeline.focus(data['responses'][i]['emergency_response_id']);
                    // timeline.moveTo(data['incidents'][i]['updated_date']);
                    // $('#message-center').append('' +
                    //     '<div data-closable class=\'callout alert-callout-subtle radius inset-shadow ' +
                    //     divClass + '\'>' +
                    //     '<strong>' + data['incidents'][i]['user'] + ' - ' +
                    //     moment(data['incidents'][i]['updated_date']).
                    //     format('M/D/YYYY h:mm:ss a') + '</strong>' +
                    //     '<p>' + data['incidents'][i]['message'] + '</p>' +
                    //     '</div>');


                    var messageCenterDiv = document.getElementById('feed-center');
                    messageCenterDiv.scrollTop = messageCenterDiv.scrollHeight;
                    if (data['event'][i]['message'] === 'Event Closed') {
                        location.reload();
                    }
                }
            }
        } else {

        }
    }).fail(function(data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
    });
}


function getResponses() {
    $.ajax({
        dataType: 'json',
        url: 'process/get_responses.php',
        type: 'GET',
        data: {'id': mainEvent['emergency_id'], 'user': user['id']},
        beforeSend: function() {
            //$('#total-not-received span').empty().append($('#not-received-users-modal table tbody tr').length + " Users");
        },
    }).done(function(data) {
        if (data.success) {
            for (var i in data['responses']) {
                var status = 'Unknown';
                var divClass = 'secondary';
                var iconSrc = 'img/pending.png';
                var table = '';
                switch (data['responses'][i]['status']) {
                    case '-1':
                        status = 'Pending';
                        divClass = 'secondary';
                        iconSrc = data['responses'][i]['mobile'] === '1' ?
                            'img/grey_mobile.png' :
                            'img/grey_pc.png';
                        table = 'pending';
                        break;
                    case '0':
                        status = 'All Clear';
                        divClass = 'success';
                        iconSrc = data['responses'][i]['mobile'] === '1' ?
                            'img/green_mobile.png' :
                            'img/green_pc.png';
                        table = 'all-clear';
                        break;
                    case '1':
                        status = 'Need Help';
                        divClass = 'alert';
                        iconSrc = data['responses'][i]['mobile'] === '1' ?
                            'img/red_mobile.png' :
                            'img/red_pc.png';
                        table = 'need-help';
                        break;
                    case '2':
                        status = 'Need Information';
                        divClass = 'warning';
                        iconSrc = data['responses'][i]['mobile'] === '1' ?
                            'img/yellow_mobile.png' :
                            'img/yellow_pc.png';
                        table = 'need-info';
                        break;
                }
                var idx = responseIds.findIndex(function(response) {
                    return response.user === data['responses'][i]['user_id'];
                });
                if (idx === -1) {
                    var marker = new google.maps.Marker({
                        position: {
                            lat: parseFloat(data['responses'][i]['latitude']),
                            lng: parseFloat(data['responses'][i]['longitude']),
                        },
                        icon: iconSrc,
                        map: map,
                        user: data['responses'][i]['user'],
                        status: data['responses'][i]['status_name'],
                        comments: data['responses'][i]['comments'],
                    });
                    google.maps.event.addListener(marker, 'click', function() {
                        var html = '' +
                            '<h6>' + this.user + '</h6>' +
                            '<p>' + this.comments + '</p>' +
                            '';
                        infowindow.setContent(html);
                        infowindow.open(map, this);
                    });
                    responseIds.push({
                        'user': data['responses'][i]['user_id'],
                        'status': data['responses'][i]['status'],
                        'marker': marker,
                        'table': table,
                    });
                    if (!isNaN(marker.position.lat()) && !isNaN(marker.position.lng())) {
                        bounds.extend(marker.position);
                        map.fitBounds(bounds);
                    }
                } else {
                    if (data['responses'][i]['status'] !== responseIds[idx]['status']) {
                        responseIds[idx]['status'] = data['responses'][i]['status'];
                        responseIds[idx]['marker'].setIcon(iconSrc);
                        responseIds[idx]['marker'].setPosition(new google.maps.LatLng(
                            data['responses'][i]['latitude'],
                            data['responses'][i]['longitude']
                        ));
                        responseIds[idx]['marker'].comments = data['responses'][i]['comments'];
                        if (!isNaN(responseIds[idx]['marker'].position.lat()) &&
                            !isNaN(responseIds[idx]['marker'].position.lng())) {
                            bounds.extend(responseIds[idx]['marker'].position);
                            map.fitBounds(bounds);
                        }
                        /*
                        $('#' + data['responses'][i]['user_id']).removeClass('secondary success alert warning').addClass(divClass);
                        $('#' + data['responses'][i]['user_id'] + '').empty().append('<strong>' + status + '</strong> - ' + data['responses'][i]['updated_date'] + ' ' + data['responses'][i]['user'] + '<p>' + data['responses'][i]['comments'] + '</p>');
                        */
                    }
                }
            }
        } else {

        }
    }).fail(function(data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
    });
}

//The getAssets function does not seem to be operating correctly and or have the goals that we originally set out so it has been
//rewritten below in other code .
function getAssets() {
    $.ajax({
        dataType: 'json',
        url: 'process/get_assets.php',
        type: 'GET',
        data: {'id': mainEvent['emergency_id'], 'user': user['id']},
        beforeSend: function() {
            //console.log('test');
            //$('#total-not-received span').empty().append($('#not-received-users-modal table tbody tr').length + " Users");
        },
    }).done(function(data) {
        if (data.success) {
            for (var i in data['assets']) {
                var marker = new google.maps.Marker({
                    position: {
                        lat: parseFloat(data['assets'][i]['latitude']),
                        lng: parseFloat(data['assets'][i]['longitude']),
                    },
                    icon: 'img/red_marker.png',
                    map: map,
                    contact: data['assets'][i]['contact'],
                    phone: data['assets'][i]['contact_phone'],
                    placement: data['assets'][i]['placement'],
                    address: data['assets'][i]['location_address'],
                });
                google.maps.event.addListener(marker, 'click', function() {
                    var html = '' +
                        '<h6>' + this.address + '</h6>' +
                        '<p>' + this.placement + '</p>' +
                        '';
                    infowindow.setContent(html);
                    infowindow.open(map, this);
                });
                if (!isNaN(marker.position.lat()) && !isNaN(marker.position.lng())) {
                    bounds.extend(marker.position);
                    map.fitBounds(bounds);
                }
            }
        } else {

        }
    }).fail(function(data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
    });
}

function emma_event_Button(controlDiv, map) {

    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.style.borderRadius = '50%';
    controlUI.style.width = '8vh';
    controlUI.style.height = '8vh';
    controlUI.style.display = 'flex';
    controlUI.style.justifyContent = 'center';
    controlUI.style.alignItems = 'center';
    if(document.getElementById("event_is_active").value == '1')
    {
        controlUI.style.background = 'red';
        controlUI.style.backgroundColor = 'red';
    }
    else {
        controlUI.style.background = 'yellow';
        controlUI.style.borderColor = 'yellow';
    }
    controlUI.onmouseover = function()
    {
        controlUI.style.cursor = "pointer";
        if(document.getElementById("event_is_active").value == '1')
        {
            controlUI.style.backgroundColor = '#f44262';
        }
        else {
            controlUI.style.backgroundColor = "#f0eb3c";
        }
    }
    controlUI.onmouseleave = function()
    {
        if(document.getElementById("event_is_active").value == '1')
        {
            controlUI.style.backgroundColor = "red";
        }
        else {
            controlUI.style.backgroundColor = "#ffff00";
        }
    }



    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    var controlText = document.createElement('img');
    controlText.style.maxWidth = '60%';
    controlText.style.maxHeight = '60%';

    controlText.src = 'img/emma_icon.png';
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to Chicago.
    controlUI.addEventListener('click', function() {
        if(document.getElementById("event_is_active").value != '1') {
            $('#create-event-modal').foundation('open');
        }
        else {
            $('#response-card-modal').foundation('open');
        }
        //$('#create-event-modal').foundation('open');
    });

}

// function emma_feed_Button(controlDiv, map) {
//
//     // Set CSS for the control border.
//     var controlUI = document.createElement('div');
//     controlUI.style.borderRadius = '15%';
//     // controlUI.style.backgroundImage = 'img/emma_icon.png';
//     controlUI.style.width = '150px';
//     controlUI.style.height = '75px';
//     controlUI.style.display = 'flex';
//     controlUI.style.justifyContent = 'center';
//     controlUI.style.alignItems = 'center';
//     controlUI.style.backgroundColor = "#ffff00";
//     controlUI.onmouseover = function()
//     {
//         controlUI.style.cursor = "pointer";
//         controlUI.style.backgroundColor = "#f0eb3c";
//     };
//     controlUI.onmouseleave = function()
//     {
//         controlUI.style.backgroundColor = "#ffff00";
//     };
//     controlDiv.appendChild(controlUI);
//
//     // Set CSS for the control interior.
//     var controlImage = document.createElement('img');
//     controlImage.style.maxWidth = '70%';
//     controlImage.style.maxHeight = '70%';
//     controlImage.title = 'Alert Feed';
//
//     controlImage.src = 'img/emma_icon.png';
//     controlUI.appendChild(controlImage);
//
//
//     // Set CSS for the image interior.
//     var controlText = document.createElement('div');
//     controlText.style.color = 'rgb(25,25,25)';
//     controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
//     controlText.style.fontSize = '16px';
//     controlText.style.lineHeight = '30px';
//     controlText.style.paddingLeft = '5px';
//     controlText.style.paddingRight = '5px';
//     controlText.innerHTML = 'Alert Feed';
//     controlUI.appendChild(controlText);
//
//     // Setup the click event listeners: simply set the map to Chicago.
//     controlUI.addEventListener('click', function() {
//         $('#alert-feed-modal').foundation('open');
//     });
//
// }

function go_event(button)
{
    let planid = $(button).attr('data-id');
    $.ajax({
        dataType: 'json',
        url: 'process/change_plan.php',
        type: 'POST',
        data: {'plan': planid},
        beforeSend: function () {
            //$('#thinking-modal').foundation('open');
        }
    })
        .done(function (data) {
            console.log(data);
            $('#error-list').empty();
            //$('#thinking-modal').foundation('close');
            if (data.success) {
                //window.location.reload();
                window.location.replace($(button).attr('data-href'));
            } else {
                for (var error in data['errors']) {
                    $('#error-list').append(data['errors'][error] + '<br />');
                }
                $('#fail-modal').foundation('open');
            }
        })
        .fail(function (data, textStatus, errorThrown) {
            $('#thinking-modal').foundation('close');
            console.log(data);
            console.log(textStatus);
            console.log(errorThrown);
        });

}


close_aed_array = [];//Close_aed_array is going to be filled with matching AEDS based on distance, but isn't the final array.
aed_markers_array = [];//array of AEDs (markers) we are displaying on the map, we keep track so we can delete them later to update.
function populateAEDS(pos) {
    //positions is an array of all AED's, queried on user_home.php.
    for (let i = 0, len = positions.length; i < len; i++) {
        //Calculate the distance from the AED points vs our user centerPoint.
       if(arePointsNear(positions[i],pos,40074.274944)) //1.60934 km = 0.99999 miles, 4.82803 km is 3 miles.
       {
           //Create a new object (closePos) and add in the newly calculated distance.
           closePos = {lat: positions[i]['lat'], lng: positions[i]['lng'], location: positions[i]['location'], distance: last_distance, coordinator: positions[i].coordinator, contact: positions[i].contact, company: positions[i].company, address: positions[i].address};
           //push onto an array.
           close_aed_array.push(closePos);
           //last distance used.
           last_distance = 0;
       }
   }

   //This is the array of 10 AED's we will show user on map.
   aeds_being_added = [];


    //sort our AEDs by distance and get the first 10 (the closest).
    close_aed_array.sort((a,b) => (a.distance > b.distance) ? 1 : -1);
    inc_count = 0; //How many we have added to the array.
    for(let i = 0, len = close_aed_array.length; i < len; i++)
    {
        //Check for duplicates.
        const found = aeds_being_added.findIndex(aeds_being_added => aeds_being_added.distance === close_aed_array[i].distance);
        if(found == -1)
        {
            //Not a duplicate, go ahead and add.
            aeds_being_added.push(close_aed_array[i]);
            inc_count++;
            if(inc_count >= 10)
            {
                break;
            }
        }
    }



    //For each object in aeds_being_added, we add a pin.
    for(let i = 0, len = aeds_being_added.length; i < len; i++)
    {
        add_pin(aeds_being_added[i],"aed");
    }
    close_aed_array = []; //empty this array.


}//Pass in users current lat/long as an object (pos)
//Place active events that are happening.
function populateEvents()
{
    for (let i = 0, len = events_arr.length; i < len; i++) {
        add_pin(events_arr[i],"event");
    }

}
function populateCalls()
{
    for (let i = 0, len = police_calls.length; i<len; i++)
    {
        add_pin(police_calls[i],"call");
    }
}
last_distance = 0;//Calculate if point is within x km of centerPoint.
function arePointsNear(checkPoint, centerPoint, km) {
    var ky = 40000 / 360;
    var kx = Math.cos(Math.PI * centerPoint.lat / 180.0) * ky;
    var dx = Math.abs(centerPoint.lng - checkPoint.lng) * kx;
    var dy = Math.abs(centerPoint.lat - checkPoint.lat) * ky;
    if(Math.sqrt(dx * dx + dy * dy) <= km)
    {
        //console.log(dx * dx + dy * dy);
        last_distance = dx*dx+dy*dy;
        return true;
    }
    return false;
    //return Math.sqrt(dx * dx + dy * dy) <= km;
}
function add_pin(object,type)
{
    if(type === "aed") {
        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(object.lat, object.lng),
            map: map,
            title: object.name
        });
        aed_markers_array.push(marker);
        google.maps.event.addListener(marker, 'click', function() {
            let contact = '<p style="line-height: 20%;"></p>';
            let coordinator = '<p style="line-height: 20%;"></p>';
            let location = '<p style="line-height: 20%;"></p>';
            let company = '<p style="line-height: 20%;"></p>';
            let address = '<p style="line-height: 20%;"></p>';
            if(object.coordinator == null || object.coordinator === 'unknown' || object.coordinator === 'undefined' || object.coordinator === '' || object.coordinator === 'Unknown')
            {
                //nothing
            }
            else{
                coordinator = '<p style="line-height: 20%;">' + object.coordinator + '</p>';
            }
            if(object.location == null || object.location === '' || object.location === '*' || object.location === 'N/A' || object.location === 'unknown' || object.location === 'Unknown')
            {
                //nothing
            }
            else{
                location = '<p style="line-height: 20%;">' + object.location + '</p>';
            }
            //convert km to miles
            object.distance = (object.distance*0.62137).toFixed(2);
            tmp_dist = '<i style="color: red;">'+object.distance+' mi</i>';
            distance_text = '<p style="line-height: 20%;">Distance: '+tmp_dist+'</p>';
            if(object.contact == null || object.contact === 'unknown' || object.contact === 'undefined' || object.contact === '' || object.contact === 'Unknown')
            {
                //nothing
            }
            else{
                contact = '<p style="line-height: 20%;">Contact: ' + object.contact + '</p>';
            }
            if(object.company == null || object.company === 'unknown' || object.company === 'undefined' || object.company === '' || object.company === 'Unknown')
            {
                //nothing
            }
            else{
                company = '<p style="line-height: 20%;">'+object.company+'</p>'
            }
            if(object.address == null || object.address === 'unknown' || object.address === 'undefined' || object.address === '' || object.address === 'Unknown')
            {
                //nothing
            }
            else{
                address = '<p style="line-height: 20%;">'+object.address+'</p>';
            }
            let html = '<p style="line-height: 20%;"></p>' +
                company+
                coordinator +
                location +
                distance_text +
                contact +
                address +
                '';
            let infowindow = new google.maps.InfoWindow({
               content: html
            });
            last_located_address = ''; //set to empty
            infowindow.open(map, marker);
            map.setZoom(18);
            map.setCenter(marker.getPosition());
        });
        marker.setMap(map);
    }
    else if(type === "asset")
    {
        let icon = 'img/orange-dot.png';
        if(object.image !== null && object.image !== '' && object.image !== 'img/orange-dot.png')
        {
            icon = {
                url: object.image,
                scaledSize: new google.maps.Size(25, 25)
            }
        }
        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(object.lat, object.lng),
            icon: icon,
            map: map,
            title: name
        });
        google.maps.event.addListener(marker, 'click', function() {
            let text = '';
            let address = '';
            if(object.name === null || object.name === '' || object.name === 'unknown' || object.name === 'N/A' || object.name === 'undefined')
            {
                text = '' + '<h6>' + 'Unknown Type' + '</h6>' + '';
            }
            else{
                text = '' + '<h6>' + object.name + '</h6>' + '';
            }
            if(object.address === null || object.address === '' || object.address === 'unknown' || object.address === 'N/A' || object.address === 'undefined')
            {
                //nothing
            }
            else{
                address = '<p>'+object.address+'</p>';
            }
            html = '' +
                text +
                address +
                '';
            let infowindow = new google.maps.InfoWindow({
                content: html
            });
            infowindow.open(map, marker);
            map.setZoom(18);
            map.setCenter(marker.getPosition());
        });
        marker.setMap(map);
    }
    else if(type === "event")
    {
        let marker_pos = new google.maps.LatLng(object.lat, object.lng);
        let icon = {
            url: 'img/' + object.img,
            scaledSize: new google.maps.Size(35,35),
        };
        let marker = new google.maps.Marker({
            position: marker_pos,
            icon: icon,
            map: map,
            title: name
        });
        google.maps.event.addListener(marker, 'click', function() {
            let text = '';
            if(object.type != null && object.type != ' ' && object.type != 'unknown' && object.type != 'Unknown' && object.type != 'undefined')
            {
                text = '' + '<h6>' + object.type + '</h6>' + '';
            }
            else{
                text = '' + '<h6>' + 'Unknown Type' + '</h6>' + '';
            }
            if(object.notes != null && object.notes != ' ')
            {
                text+='<br><h3>'+object.notes+'</h3>';
            }

            let infowindow = new google.maps.InfoWindow({
                content: text
            });
            infowindow.open(map, marker);
            map.setZoom(18);
            map.setCenter(marker.getPosition());
        });
        marker.setMap(map);
    }
    else if(type === "call")
    {
        let icon = {
            url: 'img/help.png',
            scaledSize: new google.maps.Size(35,35),
        };
        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(object.lat, object.lng),
            icon: icon,
            map: map,
            title: name
        });
        google.maps.event.addListener(marker, 'click', function() {
            let latlng = '<p style="line-height: 20%;">Lat/Lng: '+object.lat+ " " + object.lng + '</p>';
            let name = '<p style="line-height: 20%;">Called by: ' + object.username + '</p>';
            let date = '<p style="line-height: 20%;">Call date: ' + object.calldate + '</p>';
            let html = '<p style="line-height: 20%"></p>' +
            name +
                date +
                latlng +
                '';

            let infowindow = new google.maps.InfoWindow({
                content: html
            });
            infowindow.open(map, marker);
            map.setZoom(18);
            map.setCenter(marker.getPosition());
        });
        marker.setMap(map);
    }
    else{

    }

}
function placeAssets()
{
    for(let i = 0, len = assets.length; i < len; i++)
    {
        add_pin(assets[i],"asset");
    }
}

function geocodePosition(pos)
{
    //Sets closest address into input for each event modal..
    geocoder = new google.maps.Geocoder();
    geocoder.geocode
    ({
            latLng: pos
        },
        function(results, status)
        {
            if (status == google.maps.GeocoderStatus.OK)
            {
                $('input[name="address"]').each(function(){
                    $(this).val(results[0].formatted_address);
                });
            }
            else
            {
                //$("#mapErrorMsg").html('Cannot determine address at this location.'+status).show(100);
            }
        }
    );
}

function initMap() {




    var event = {lat: 41.9568958, lng: -91.72251};


    map = new google.maps.Map(document.getElementById('event-map'), {
        zoom: 15,
        center: event,
    });


    // Try HTML5 geolocation.
    infoWindow = new google.maps.InfoWindow;
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            let image = new google.maps.MarkerImage(
              'img/bluedot_retina.png',
              null,
              null,
                new google.maps.Point( 8, 8 ),
                new google.maps.Size(17,17)
            );
            let marker = new google.maps.Marker({
                position: new google.maps.LatLng(pos.lat, pos.lng),
                icon: image,
                map: map,
                draggable: true,
                animation: google.maps.Animation.DROP,
                title: ' Your Location'
            });

            google.maps.event.addListener(marker, 'dragend', function(event){
                //Get formatted address
                geocodePosition(marker.getPosition());
                //Update each lat/lng position on drag.
                $('#user_latitude').val(event.latLng.lat());
                $('#user_longitude').val(event.latLng.lng());
                var pos = {
                    lat: parseFloat(event.latLng.lat()),
                    lng: parseFloat(event.latLng.lng())
                };
                //But first clear old markers.
                for(let i = 0; i < aed_markers_array.length; i++)
                {
                    aed_markers_array[i].setMap(null);
                }
                aed_markers_array = [];

                //Update AED's / arrays with new positions.
                populateAEDS(pos);



            });
            google.maps.event.addListener(marker, 'mouseover', function(event){
               marker.setTitle(' ' + $('input[name="address"]').val());
            });

            map.setCenter(pos);
            map.setZoom(15);
            geocodePosition(marker.getPosition());
            //set lat/lng for creating events.
            $('input[name="lat"]').each(function(){
                $(this).val(pos.lat);
            });
            $('input[name="lng"]').each(function(){
                $(this).val(pos.lng);
            });
            populateAEDS(pos);

        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }







    if (typeof mainEvent !== 'undefined') {
        event = {
            lat: parseFloat(mainEvent['latitude']),
            lng: parseFloat(mainEvent['longitude']),
        };
        map = new google.maps.Map(document.getElementById('event-map'), {
            zoom: 15,
            center: event,
        });

        infowindow = new google.maps.InfoWindow({
            content: 'AEDs',
        });

        var marker = new google.maps.Marker({
            position: event,
            icon: 'img/event.png',
            map: map,
        });
        bounds = new google.maps.LatLngBounds();
        bounds.extend(event);

        //getAssets();
        getResponses();
        setInterval(getResponses, updateInterval);
    } else {
        map = new google.maps.Map(document.getElementById('event-map'), {
            zoom: 5,
            center: pos,
        });
    }




    function handleLocationError(){
        marker.setPosition(pos);
        marker.setContent(browserHasGeolocation ? 'Error: The Geolocation service failed.' : 'Error: Your browser doesn\'t support geolocation.');
        marker.open(map);
    }





    if($('#plan_id').value === true)
    {
        // Create the DIV to hold the control and call the emma_event_Button
        // constructor passing in this DIV.
        var emmaButton = document.createElement('div');
        var emma_button = new emma_event_Button(emmaButton, map);

        emmaButton.setAttribute('id','emmaButton');

        emmaButton.index = 1;
        map.controls[google.maps.ControlPosition.LEFT_TOP].push(emmaButton);

        // // EMMA Feed button
        // var feedButton = document.createElement('div');
        // var feed_button = new emma_feed_Button(feedButton, map);
        //
        // feedButton.setAttribute('id','feedButton');
        //
        // feedButton.index = 1;
        // map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(feedButton);
    }
    placeAssets();
    populateEvents();
    populateCalls();
}

$(document).ready(function() {

    if (typeof mainEvent !== 'undefined') {
        getTimeline();
        setInterval(getTimeline, updateInterval);
    }

    getFeed();

    $('#alert-response-form').submit(function(e) {
        e.preventDefault();
        var form = $(this);
        $.ajax({
            dataType: 'json',
            url: 'https://tsdemos.com/mobileapi/alert_response.php',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function() {
                $('#thinking-modal').foundation('open');
            },
        }).done(function(data) {
            //console.log(data);
            $('#thinking-modal').foundation('close');
            if (data.success) {
                location.reload();
                //console.log(data);
                //TODO: SUCCESS MESSAGE
            } else {
                //TODO: FAILED MESSAGE
            }
            //form.foundation('close');
        }).fail(function(data, textStatus, errorThrown) {
            console.log(data);
            console.log(textStatus);
            console.log(errorThrown);
        });
        return false;
    });
});


setInterval(function () {
    $('#feed-center').load('dashboard.php?content=home #live-feed');
}, 5000);

$('#feed-length').change(function (e) {
    var length = $('#feed-length option:selected').val();
    e.preventDefault();
    $.ajax({
        dataType: 'json',
        url: 'process/update_feed_length.php',
        type: 'POST',
        data: {'length': length}
    })
        .done(function (data) {
            if (data.success) {
                $('#feed-center').load('dashboard.php?content=home #live-feed');
            } else {

            }
        })
        .fail(function (data, textStatus, errorThrown) {

        });
})
