$(document).ready(function () {

    $('.select-parent').click(function () {
        var student = $(this).data('student');
        $('#student-id').val(student);
        $('#select-parent-modal').foundation('open');
    });


    $('#select-parent-form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/add_parent.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            },
        })
            .done(function (data) {
                console.log(data);
                $('#thinking-modal').foundation('close');
                $('#error-list').empty();
                if (data.success) {
                    $('#success-modal').foundation('open');
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
        return false;
    });

    $('#create_user_form').submit(function (e) {
        e.preventDefault();

        $.ajax({
            dataType: 'json',
            url: 'process/create_new_user.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            }
        })
            .done(function (data) {
                console.log(data);
                $('#error-list').empty();
                $('#thinking-modal').foundation('close');
                if (data.success) {
                    $('#success-modal').foundation('open');
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
    });

    $('.all-select').click(function () {
        $('.group-toggle').click();
    })
});