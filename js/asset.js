marker_array = [];
map_array = [];
function initMap() {

    var loc = {lat: parseFloat(mainEvent['latitude']), lng: parseFloat(mainEvent['longitude'])};
    var map = new google.maps.Map(
        document.getElementById('asset-map'), {zoom: 12, center: loc});
    var icon = 'img/orange-dot.png';
    if(mainEvent['image']){
        if(mainEvent['image'] != 'img/orange-dot.png') {
            icon = {
                url: mainEvent['image'],
                scaledSize: new google.maps.Size(25, 25)
            }
        }
    }
    marker_array.push();
    var marker = new google.maps.Marker({position: loc, map: map, icon: icon});
    var edit_map = new google.maps.Map(
        document.getElementById('edit-map'), {zoom: 12, center: loc});
    var edit_marker = new google.maps.Marker({position: loc, map: edit_map, icon: icon, draggable: true});
    map_array.push(edit_map);
    marker_array.push(edit_marker);
    google.maps.event.addListener(edit_marker, 'dragend', function(event){
        //Update each lat/lng position on drag.
        input_lat.val(event.latLng.lat());
        input_lng.val(event.latLng.lng());
        let pos={latitude: input_lat.val(),longitude: input_lng.val()};
        //calc address, it wont fire the on change event when we change the values like this.
        geocodePosition(pos);
    });

}




function geocodePosition(pos)
{
    //console.log(pos);
    //Sets closest address into input for each event modal..
    geocoder = new google.maps.Geocoder();
    geocoder.geocode
    ({
            latLng: new google.maps.LatLng(pos.latitude,pos.longitude)
        },
        function(results, status)
        {
            if (status == google.maps.GeocoderStatus.OK)
            {
                $('input[name="address"]').val(results[0].formatted_address);
            }
            else
            {
                $('input[name="address"]').val('N/A');
            }
        }
    );
}







$(document).ready(function () {
    edit_asset = function () {
        $('#insert-area').empty();
        type_id = $('#edit_asset_form_type_id option:selected').val();
        asset_id = $('.asset_id').val();
        //We are pulling up contents instead of prefabs as this is the Asset page we are currently on.
        $.ajax({
            url: 'process/get_asset_contents.php',
            type: 'GET',
            data: {'typeid': type_id,'assetid': asset_id},
            beforeSend: function () {
            },
        })
            .done(function (data) {
                count = 0;
                data = JSON.parse(data);
                //console.log(data);
                if (data['item'] != null) {
                    var i;
                    for (i = 0; i < data['item'].length; i++) {
                        if (data['item'][i]['dropdown_id'] != null && data['item'][i]['dropdown_id'] != 0) {
                            //If the id="insert-area-edit" class="dropdownName" has the name value that already exists, then just add option to it.
                            //If it doesn't, then add in a dropdown.
                            if ($('.dropdownName[value="' + data['item'][i]['name'] + '"]')[0]) {
                                //Dropdown select already exists
                                if(!$('.dropdownName[data-dropdown="' + data["item"][i]["name"] + '"] option[value="'+data['item'][i]['dropdown_id']+'"]')[0]){
                                    //Option value for dropdown doesn't exist, so it isn't a repeat value being added. Go ahead and append.
                                    option = '<option value="' + data['item'][i]['dropdown_id'] + '">' + data['item'][i]['dropdown_name'] + '</option>';
                                    $('.dropdownName[data-dropdown="' + data["item"][i]["name"] + '"]').append(option);
                                }
                                count++;
                            }
                            else {
                                //Doesn't exist
                                html = '<label id="dropdownLabel' + count + '">' + data['item'][i]['name'] + " " + 'Options</label><div class="row large-12 columns" id="dropdowninsert" data-dropdown="' + data["item"][i]["name"] + '" data-id="' + data["item"][i]["id"] + '"><div class="large-12 columns"><select data-dropdown="' + data["item"][i]["name"] + '" data-id="' + data['item'][i]['id'] + '" data-selecteddropdown="' + data["item"][i]['custominfoid'] + '" data-originalname="' + data['item'][i]['name'] + '" value="' + data['item'][i]['name'] + '" class="dropdownName"/></div></div>';
                                option = '<option value="' + data['item'][i]['dropdown_id'] + '">' + data['item'][i]['dropdown_name'] + '</option>';
                                $('#insert-area').append(html);
                                $('.dropdownName[data-dropdown="' + data["item"][i]["name"] + '"]').append(option);
                                count++;
                            }

                        }
                        else if (data['item'][i]['info'] != null)
                        {
                            //Need to make sure that one of these doesn't already exist already.
                            //Add a label that we will append too later.


                            if (!$('.info[value="' + data['item'][i]['info'] + '"]')[0]) {
                                //Doesn't exist already.
                                html = '<label>Item Description</label><input data-customid="'+data['item'][i]['custominfoid']+'" class="info" type="text" required value="'+data['item'][i]['info']+'"/>';
                                $('#insert-area').append(html);
                            }


                        }
                        else if(data['item'][i]['date_time'] != null && data['item'][i]['fillin'] === "0")
                        {
                            //Add a label that we will append too later.
                            if (!$('.date_time[value="' +data['item'][i]['date_time']+ '"]')[0]) {
                                //Doesn't exist already.
                                html = '<label>'+data['item'][i]['name']+'</label><input data-customid="'+data['item'][i]['custominfoid']+'" class="date_time" type="date" required value="'+data['item'][i]['date_time']+'"/>';
                                //html = '<label>'+data['item'][i]['name']+'</label><input class="text" type="text" required value="INVALID DATE FORMAT"/>';
                                $('#insert-area').append(html);
                            }
                        }
                    }
                    //Foreach in object content, check all dropdowns and assign data-values based on values
                    //found in already existing dropdown so we can use the data-values later.
                    $("select[class^='dropdownName']").each(function(index){
                        dropdownid = null;
                        selectedid = null;
                        $(this).find('option').each(function(index,element){
                            console.log(element);
                            var z;
                            for (z = 0; z < data['content'].length; z++) {
                                //console.log(element);
                                if(data['content'][z]['dropdown'] == $(this).val())
                                {
                                    $(this).attr('selected','selected');
                                    dropdownid = data['content'][z]['dropdown'];
                                    selectedid = data['content'][z]['id'];
                                }
                            }
                        });
                        //console.log('selected: ' + selectedid + " " + ' dropdown: ' + dropdownid);
                        $(this).attr('data-selecteddropdown',selectedid);
                        $(this).attr('data-dropdownid',dropdownid);

                    });

                }
            })
            .fail(function (data, textStatus, errorThrown) {
                //console.log(data);
                //console.log(textStatus);
                // console.log(errorThrown);
            });
        $('#edit-asset-modal').foundation('open');
        return false;

    };

    $('#status_bar').change(function () {
        if ($('#status_bar option:selected').text() == 'Active') {
            $('#status_bar').attr("style", "color: green;");
        }
        else if ($('#status_bar option:selected').text() == 'Inactive') {
            $('#status_bar').attr("style", "color: red;");
        }
    });
    $('#form_id').change(function () {

        $('#form_type_raw_name').attr('value', $('#form_id option:selected').text());

        //select new dates / items related to the
    });

    $('#edit-asset-form').submit(function (e) {
        e.preventDefault();
        //build our array(s) of value(s)
        dropdown_array = new Array();
        dates_array = new Array();
        fillin_array = new Array();


        form = $('#edit-asset-form').serializeArray();

        $("input[class^='info']").each(function(index){
            let obj = {name: 'fillin', value: $(this).val(), customid: $(this).attr('data-customid')};
            fillin_array.push(obj);
        });
        $("input[class^='date_time']").each(function(index){
            let obj = {name: 'date', value: $(this).val(), customid: $(this).attr('data-customid')};
            dates_array.push(obj);
        });
        $("select[class^='dropdownName']").each(function(index){
            let obj = {name: 'dropdown', value: $(this).val(), id: $(this).attr('data-dropdownid'), selecteddropdown: $(this).attr('data-selecteddropdown')};
            dropdown_array.push(obj);
        });

        form.push(dropdown_array);
        form.push(dates_array);
        form.push(fillin_array);

        $.ajax({
            dataType: 'json',
            url: 'process/edit_asset.php',
            type: 'POST',
            data: $.param(form) + '&Headers=' + JSON.stringify(dropdown_array) + '&Fillins=' + JSON.stringify(fillin_array) + '&Dates=' + JSON.stringify(dates_array),
            beforeSend: function () {
                THINKINGMODAL.foundation('open');
            },
        })
            .done(function (data) {
                console.log(data);
                THINKINGMODAL.foundation('close');
                if (data.success) {
                    $('#asset-success-delete-modal').foundation('open'); //We aren't deleting, but the success modal is the exact same functionality wise, so we just use this one.
                } else {
                    FAILMODAL.foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log(data);
                //  console.log(textStatus);
                //  console.log(errorThrown);
            });
        return false;
    });

    $('#delete-asset-form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/delete_asset.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                THINKINGMODAL.foundation('open');
            },
        })
            .done(function (data) {
                //console.log(data);
                THINKINGMODAL.foundation('close');
                if (data.success) {
                    $('#asset-success-delete-modal').foundation('open');
                    // location.reload();
                } else {
                    FAILMODAL.foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                // console.log(data);
                // console.log(textStatus);
                // console.log(errorThrown);
            });
        return false;
    });

    $('#remove-asset-form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/remove_asset.php',
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function () {
                THINKINGMODAL.foundation('open');
            },
        })
            .done(function (data) {
                //console.log(data);
                THINKINGMODAL.foundation('close');
                if (data.success) {
                    $('#asset-success-delete-modal').foundation('open');
                    // location.reload();
                } else {
                    FAILMODAL.foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                // console.log(data);
                // console.log(textStatus);
                // console.log(errorThrown);
            });
        return false;
    });




    input_lat = $('#entered_lat');
    input_lng = $('#entered_lng');


    input_lat.change(function(){
        if(input_lat.val() != null && input_lng.val() != null)
        {
            let pos={latitude: input_lat.val(),longitude: input_lng.val()};
            geocodePosition(pos);
        }
    });


    input_lng.change(function(){
        if(input_lat.val() != null && input_lng.val() != null)
        {
            let pos={latitude: input_lat.val(),longitude: input_lng.val()};
            geocodePosition(pos);
        }
    });











    $('#edit_asset_form_type_id').change(function () {
        $('#insert-area').empty();
        type_id = $('#edit_asset_form_type_id option:selected').val();
        if (type_id == $('.asset_type_id').val()) {
            asset_id = $('.asset_id').val();
            //We are pulling up contents instead of prefabs as this is the Asset page we are currently on.
            $.ajax({
                url: 'process/get_asset_contents.php',
                type: 'GET',
                data: {'typeid': type_id,'assetid': asset_id},
                beforeSend: function () {
                },
            })
                .done(function (data) {
                    data = JSON.parse(data);
                    //console.log(data);
                    //update map
                    //create a new marker on the map, but first clear the old one.
                    let icon = 'img/orange-dot.png';
                    if(data['image'][0]['image'] !== 'img/orange-dot.png' && data['image'][0]['image'] !== 'null' && data['image'][0]['image'] !== null)
                    {
                        icon = {
                            url: data['image'][0]['image'],
                            scaledSize: new google.maps.Size(25, 25)
                        };
                    }
                    marker_array[0].setMap(null);
                    var pos = {lat: parseFloat(input_lat.val()), lng: parseFloat(input_lng.val())};
                    console.log(pos);
                    editmarker = new google.maps.Marker({position: pos, map: map_array[0], icon: icon, draggable: true});
                    google.maps.event.addListener(editmarker, 'dragend', function(event){
                        //Update each lat/lng position on drag.
                        input_lat.val(event.latLng.lat());
                        input_lng.val(event.latLng.lng());
                        let pos={latitude: input_lat.val(),longitude: input_lng.val()};
                        //calc address, it wont fire the on change event when we change the values like this.
                        geocodePosition(pos);
                    });
                    marker_array = []; //empty
                    marker_array.push(editmarker);


                    count = 0;
                    //console.log(data);
                    if (data['item'] != null) {
                        var i;
                        for (i = 0; i < data['item'].length; i++) {
                            if (data['item'][i]['dropdown_id'] != null && data['item'][i]['dropdown_id'] != 0) {
                                //If the id="insert-area-edit" class="dropdownName" has the name value that already exists, then just add option to it.
                                //If it doesn't, then add in a dropdown.
                                if ($('.dropdownName[value="' + data['item'][i]['name'] + '"]')[0]) {
                                    //Dropdown select already exists
                                    if(!$('.dropdownName[data-dropdown="' + data["item"][i]["name"] + '"] option[value="'+data['item'][i]['dropdown_id']+'"]')[0]){
                                        //Option value for dropdown doesn't exist, so it isn't a repeat value being added. Go ahead and append.
                                        option = '<option value="' + data['item'][i]['dropdown_id'] + '">' + data['item'][i]['dropdown_name'] + '</option>';
                                        $('.dropdownName[data-dropdown="' + data["item"][i]["name"] + '"]').append(option);
                                    }
                                    count++;
                                }
                                else {
                                    //Doesn't exist
                                    html = '<label id="dropdownLabel' + count + '">' + data['item'][i]['name'] + " " + 'Options</label><div class="row large-12 columns" id="dropdowninsert" data-dropdown="' + data["item"][i]["name"] + '" data-id="' + data["item"][i]["id"] + '"><div class="large-12 columns"><select data-dropdown="' + data["item"][i]["name"] + '" data-id="' + data['item'][i]['id'] + '" data-originalname="' + data['item'][i]['name'] + '" value="' + data['item'][i]['name'] + '" class="dropdownName"/></div></div>';
                                    option = '<option value="' + data['item'][i]['dropdown_id'] + '">' + data['item'][i]['dropdown_name'] + '</option>';
                                    $('#insert-area').append(html);
                                    $('.dropdownName[data-dropdown="' + data["item"][i]["name"] + '"]').append(option);
                                    count++;
                                }

                            }
                            else if (data['item'][i]['info'] != null)
                            {
                                //Need to make sure that one of these doesn't already exist already.
                                //Add a label that we will append too later.


                                if (!$('.info[value="' + data['item'][i]['info'] + '"]')[0]) {
                                    //Doesn't exist already.
                                    html = '<label>Item Description</label><input data-customid="'+data['item'][i]['custominfoid']+'" class="info" type="text" required value="'+data['item'][i]['info']+'"/>';
                                    $('#insert-area').append(html);
                                }


                            }
                            else if(data['item'][i]['date_time'] != null && data['item'][i]['fillin'] === "0")
                            {
                                //Add a label that we will append too later.
                                if (!$('.date_time[value="' +data['item'][i]['date_time']+ '"]')[0]) {
                                    //Doesn't exist already.
                                    html = '<label>'+data['item'][i]['name']+'</label><input data-customid="'+data['item'][i]['custominfoid']+'" class="date_time" type="date" required value="'+data['item'][i]['date_time']+'"/>';
                                    //html = '<label>'+data['item'][i]['name']+'</label><input class="text" type="text" required value="INVALID DATE FORMAT"/>';
                                    $('#insert-area').append(html);
                                }
                            }
                        }
                        //Foreach in object content, check all dropdowns and assign data-values based on values
                        //found in already existing dropdown so we can use the data-values later.
                        $("select[class^='dropdownName']").each(function(index){
                            dropdownid = null;
                            selectedid = null;
                            $(this).find('option').each(function(index,element){
                                console.log(element);
                                var z;
                                for (z = 0; z < data['content'].length; z++) {
                                    //console.log(element);
                                    if(data['content'][z]['dropdown'] == $(this).val())
                                    {
                                        $(this).attr('selected','selected');
                                        dropdownid = data['content'][z]['dropdown'];
                                        selectedid = data['content'][z]['id'];
                                    }
                                }
                            });
                            console.log('selected: ' + selectedid + " " + ' dropdown: ' + dropdownid);
                            $(this).attr('data-selecteddropdown',selectedid);
                            $(this).attr('data-dropdownid',dropdownid);

                        });

                    }
                })
                .fail(function (data, textStatus, errorThrown) {
                    //console.log(data);
                    //console.log(textStatus);
                    //console.log(errorThrown);
                });
        }
        else {
            $.ajax({
                url: 'process/get_asset_items.php',
                type: 'GET',
                data: {'type_id': type_id},
                beforeSend: function () {
                },
            })
                .done(function (data) {
                    data = JSON.parse(data);


                    console.log(data);
                    //create a new marker on the map, but first clear the old one.
                    let icon = 'img/orange-dot.png';
                    if(data['image'][0]['image'] !== 'img/orange-dot.png' && data['image'][0]['image'] !== 'null' && data['image'][0]['image'] !== null)
                    {
                        icon = {
                            url: data['image'][0]['image'],
                            scaledSize: new google.maps.Size(25, 25)
                        };
                    }
                    marker_array[0].setMap(null);
                    var pos = {lat: parseFloat(input_lat.val()), lng: parseFloat(input_lng.val())};
                    editmarker = new google.maps.Marker({position: pos, map: map_array[0], icon: icon, draggable: true});
                    google.maps.event.addListener(editmarker, 'dragend', function(event){
                        //Update each lat/lng position on drag.
                        input_lat.val(event.latLng.lat());
                        input_lng.val(event.latLng.lng());
                        let pos={latitude: input_lat.val(),longitude: input_lng.val()};
                        //calc address, it wont fire the on change event when we change the values like this.
                        geocodePosition(pos);
                    });
                    marker_array = []; //empty
                    marker_array.push(editmarker);











                    count = 0;
                    //console.log(data);
                    //Foreach array in data (associative array) we check to see if it has
                    //a date, a dropdown, or a fillin. Then we append blank versions of those
                    //with X buttons that can remove them.
                    if (data['item'] != null) {
                        var i;
                        for (i = 0; i < data['item'].length; i++) {
                            if (data['item'][i]['date'] == '1') {
                                html = '<div class="row"></i><div class="large-12 columns" <label>' + data['item'][i]['name'] + '</label><input required type="date" name="date"/></div></div>';
                                $('#insert-area').append(html);
                            }
                            if (data['item'][i]['fillin'] == '1') {
                                html = '<div class = "row"><div class="large-12 columns"><label>Item Description</label><input type="text" name="fillin" placeholder="Item name" value="' + data['item'][i]['name'] + '"></div></div>';
                                $('#insert-area').append(html);
                            }
                            if (data['item'][i]['dropdown_id'] != null && data['item'][i]['dropdown_id'] != 0) {
                                //If the id="insert-area-edit" class="dropdownName" has the name value that already exists, then just add option to it.
                                //If it doesn't, then add in a dropdown.
                                if ($('.dropdownName[value="' + data['item'][i]['name'] + '"]')[0]) {
                                    //Exists
                                    option = '<option value="' + data['item'][i]['id'] + '">' + data['item'][i]['dropdown_name'] + '</option>';
                                    $('.dropdownName[data-dropdown="' + data["item"][i]["name"] + '"]').append(option);
                                    count++;
                                }
                                else {
                                    //Doesn't exist
                                    html = '<label id="dropdownLabel' + count + '">' + data['item'][i]['name'] + " " + 'Options</label><div class="row large-12 columns" id="dropdowninsert" data-dropdown="' + data["item"][i]["name"] + '" data-id="' + data["item"][i]["id"] + '"><div class="large-12 columns"><select data-dropdown="' + data["item"][i]["name"] + '" data-id="' + data['item'][i]['id'] + '" data-originalname="' + data['item'][i]['name'] + '" value="' + data['item'][i]['name'] + '" class="dropdownName"/></div></div>';
                                    option = '<option value="' + data['item'][i]['id'] + '">' + data['item'][i]['dropdown_name'] + '</option>';
                                    $('#insert-area').append(html);
                                    $('.dropdownName[data-dropdown="' + data["item"][i]["name"] + '"]').append(option); //still have to append here on first iteration.
                                    count++;
                                }

                            }
                        }
                    }
                })
                .fail(function (data, textStatus, errorThrown) {
                    //console.log(data);
                    // console.log(textStatus);
                    // console.log(errorThrown);
                });
            return false;
        }

    });



});