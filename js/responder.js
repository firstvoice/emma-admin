
$(document).ready(function () {
  edit_responder = function () {
    $('#edit-responder-modal').foundation('open');
    return false;
  };

  $('#status_bar').change(function () {
    if ($('#status_bar option:selected').text() == 'Active') {
      $('#status_bar').attr("style", "color: green;");
    }
    else if ($('#status_bar option:selected').text() == 'Inactive') {
      $('#status_bar').attr("style", "color: red;");
    }
  });

  $('#edit-responder-form').submit(function (e) {
    e.preventDefault();
    form = $('#edit-responder-form').serializeArray();
    $.ajax({
      dataType: 'json',
      url: 'process/edit_responder.php',
      type: 'POST',
      data: $.param(form),
      beforeSend: function () {
        THINKINGMODAL.foundation('open');
      },
    })
      .done(function (data) {
        console.log(data);
        THINKINGMODAL.foundation('close');
        if (data.success) {
          $('#responder-success-modal').foundation('open');
        } else {
          FAILMODAL.foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        console.log(data);
        //  console.log(textStatus);
        //  console.log(errorThrown);
      });
    return false;
  });

  $('#deactivate-responder-form').submit(function (e) {
    e.preventDefault();
    $.ajax({
      dataType: 'json',
      url: 'process/deactivate_responder.php',
      type: 'POST',
      data: $(this).serialize(),
      beforeSend: function () {
        THINKINGMODAL.foundation('open');
      },
    })
      .done(function (data) {
        THINKINGMODAL.foundation('close');
        if (data.success) {
          $('#responder-success-modal').foundation('open');
        } else {
          FAILMODAL.foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
    return false;
  });

  $('#delete-responder-form').submit(function (e) {
    e.preventDefault();
    $.ajax({
      dataType: 'json',
      url: 'process/delete_responder.php',
      type: 'POST',
      data: $(this).serialize(),
      beforeSend: function () {
        THINKINGMODAL.foundation('open');
      },
    })
      .done(function (data) {
        THINKINGMODAL.foundation('close');
        if (data.success) {
          $('#responder-success-modal').foundation('open');
        } else {
          FAILMODAL.foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
    return false;
  });

});