$(document).ready(function () {

    $('.login-button').click(function () {



    });

    $('.tracked-link').click(function () {
        var link = $(this).data('link');
        var name = $(this).data('name');
        var user = $(this).data('user');

        window.open(link,'_blank');

        if(user !== null){
            $.ajax({
                dataType: 'json',
                url: 'process/predownload.php',
                type: 'POST',
                data: {'user-id': user,
                    'type': name,
                    'link': link
                }
            })
                .done(function (data) {
                    console.log(data);
                    $('#error-list').empty();
                    if (data.success) {
                        window.open(link,'_blank');

                    } else {
                        for (var error in data['errors']) {
                            $('#error-list').append(data['errors'][error] + '<br />');
                        }
                    }
                })
                .fail(function (data, textStatus, errorThrown) {
                    console.log(data);
                    console.log(textStatus);
                    console.log(errorThrown);
                });

        }else{
            $('#download-link').val(link);
            $('#download-type').val(name);
            $('#predownload-modal').foundation('open');
        }
    });


    $('#predownload-form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        $.ajax({
            dataType: 'json',
            url: 'process/predownload.php',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function () {

            },
        })
            .done(function (data) {
                console.log(data);
                if (data.success) {
                    window.open(data['link'],'_blank');
                    location.reload(true);
                    //TODO: SUCCESS MESSAGE
                } else {
                    //TODO: FAILED MESSAGE
                }
            })
            .fail(function (data, textStatus, errorThrown) {

            });
        return false;
    });

    $('#management-button').click(function(){
        $('.button-content').attr('hidden',true);
        $('#management-feature-content').removeAttr('hidden',true);
    });
    $('#notification-button').click(function(){
        $('.button-content').attr('hidden',true);
        $('#notification-feature-content').removeAttr('hidden',true);
    });
    $('#personal-button').click(function(){
        $('.button-content').attr('hidden',true);
        $('#personal-safety-feature-content').removeAttr('hidden',true);
    });
    $('#public-button').click(function(){
        $('.button-content').attr('hidden',true);
        $('#public-safety-feature-content').removeAttr('hidden',true);
    });

    if(screen.width <= 630){
        $('.the-clock').css('height','65%')
    }
    if(screen.width <= 630){
        $('.tss-theater').css('height','65%')
    }
    if(screen.width <= 630){
        $('.emma_verse').css('height','65%')
    }
    if(screen.width <= 630){
        $('.hospital_background').css('height','65%')
    }
    if(screen.width <= 630){
        $('.school_background').css('height','65%')
    }
    if(screen.width <= 630){
        $('.public_safety_background').css('height','65%')
    }
    if(screen.width <= 630){
        $('.workplace_background').css('height','65%')
    }
    if(screen.width <= 630){
        $('.resource_background').css('height','65%')
    }
});

(function($){
    $.fn.extend({
        rotaterator: function(options) {

            var defaults = {
                fadeSpeed: 500,
                pauseSpeed: 100,
                child:null
            };

            var options = $.extend(defaults, options);

            return this.each(function() {
                var o =options;
                var obj = $(this);
                var items = $(obj.children(), obj);
                items.each(function() {$(this).hide();})
                if(!o.child){var next = $(obj).children(':first');
                }else{var next = o.child;
                }
                $(next).fadeIn(o.fadeSpeed, function() {
                    $(next).delay(o.pauseSpeed).fadeOut(o.fadeSpeed, function() {
                        var next = $(this).next();
                        if (next.length == 0){
                            next = $(obj).children(':first');
                        }
                        $(obj).rotaterator({child : next, fadeSpeed : o.fadeSpeed, pauseSpeed : o.pauseSpeed});
                    })
                });
            });
        }
    });
})(jQuery);

$(document).ready(function() {
    $('#rotate').rotaterator({fadeSpeed:500, pauseSpeed:10000});

    $('#forgot-password-modal').foundation('open');

});