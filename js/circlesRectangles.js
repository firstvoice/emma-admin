let getSecondaryCircles = false;

function drawCircles(bounds, map) {
	// let circleRadius = 1000;
	// let circleDia = circleRadius*2;

	// Horizontal rectangle
	if(bounds.horizontalmeters > bounds.verticalmeters) {
		let circleRadius = bounds.verticalmeters/2;
		// console.log(circleRadius);
		let pctCircleRadius = circleRadius - (((0.5 / 100) * circleRadius).toFixed(3));
		pctCircleRadius = bounds.verticalmeters * 0.706;

		// console.log("circleRadius", circleRadius);
		let circleDia = bounds.verticalmeters;
		let noOfCircles = bounds.horizontalmeters/circleDia;
		// console.log("noOfCircles", noOfCircles);

		let verticalMidPoint = getNewPoint(bounds.latLngs.a, 180, circleRadius);
		let centerPointC1 = getNewPoint(verticalMidPoint, 90, circleRadius);


		// Creating main circles
		for(var i = 0; i < Math.ceil(noOfCircles); i++){
			let cityCircle = new google.maps.Circle({

				strokeColor : '#6c6c6c',
        strokeWeight : 3.5,
        fillColor : '#926239',
        fillOpacity : 0.6,
        editable: true,
        draggable: true,
	      map: map,
	      clickable: true,
	      center: centerPointC1,
	      radius: pctCircleRadius
	    });

			// Adding circle to json
	    let circle = {
        centerLat: centerPointC1.lat(),
        centerLng: centerPointC1.lng(),
        radiusMeters: pctCircleRadius
      }
      json.circles.push(circle);
	    json.circleObjects.push(cityCircle);

			let centerPointNext = getNewPoint(centerPointC1, 90, circleDia);
		  centerPointC1 = centerPointNext;

		  let lastCircleIndex = json.circleObjects.length - 1;
			createInfoWindowCircle(lastCircleIndex, cityCircle, map);
		}

		if(getSecondaryCircles) {
					// Finding secondary circles.
			let secRadius = pctCircleRadius/4;
			let horizontalPoint = getNewPoint(bounds.latLngs.a, 90, circleDia);
			let circleCenterSec = getNewPoint(horizontalPoint, 180, secRadius);

			let noOfSecCircles = noOfCircles;

			let diff = noOfCircles - Math.floor(noOfCircles)
			// console.log("diff", diff);
			if(diff > 0.1){
				noOfSecCircles = Math.floor(noOfCircles)+1;
			} else {
				noOfSecCircles = Math.floor(noOfCircles);
			}

			// secodary circles
			for(var i = 1; i < noOfSecCircles; i++){

				var cityCircle = new google.maps.Circle({
			    strokeColor : '#6c6c6c',
	        strokeWeight : 3.5,
	        fillColor : '#926239',
	        fillOpacity : 0.6,
	        editable: true,
	        draggable: true,
		      map: map,
		      center: circleCenterSec,
		      radius: secRadius
		    });

		    let circle = {
	        centerLat: circleCenterSec.lat(),
	        centerLng: circleCenterSec.lng(),
	        radiusMeters: secRadius
	      }
	      json.circles.push(circle);
		    json.circleObjects.push(cityCircle);

				let circlePointSecNext = getNewPoint(circleCenterSec, 90, circleDia);
			    circleCenterSec = circlePointSecNext;
			}

			// let secRadius = pctCircleRadius/4;
			let horizontalPointD = getNewPoint(bounds.latLngs.d, 90, circleDia);
			let circleCenterSecD = getNewPoint(horizontalPointD, 360, secRadius);

			// secodary circles 2
			for(var i = 1; i < noOfSecCircles; i++){
				var cityCircle = new google.maps.Circle({
			      strokeColor : '#6c6c6c',
		        strokeWeight : 3.5,
		        fillColor : '#926239',
		        fillOpacity : 0.6,
		        editable: true,
		        draggable: true,
			      map: map,
			      center: circleCenterSecD,
			      radius: secRadius
			    });

				let circle = {
	        centerLat: circleCenterSecD.lat(),
	        centerLng: circleCenterSecD.lng(),
	        radiusMeters: secRadius
	      }
	      json.circles.push(circle);
		    json.circleObjects.push(cityCircle);

				let circlePointSecDNext = getNewPoint(circleCenterSecD, 90, circleDia);
			    circleCenterSecD = circlePointSecDNext;
			}
			// console.log(json);
			//End
			// let lastRadius = diff * bounds.verticalmeters;
			// let lastCenterDistance = lastRadius + circleRadius;
			// let lastCenter = getNewPoint(circleCenterSec, 90, lastCenterDistance);

			// var cityCircle = new google.maps.Circle({
			//      strokeColor: '#FF0000',
			//      strokeOpacity: 0.8,
			//      strokeWeight: 2,
			//      fillColor: '#FF0000',
			//      fillOpacity: 0.35,
			//      map: map,
			//      center: lastCenter,
			//      radius: lastRadius
			//    });

		}


	// Vertical rectangle
	} else {
		let circleRadius = bounds.horizontalmeters/2;
		// console.log(circleRadius);
		let pctCircleRadius = circleRadius - (((0.5 / 100) * circleRadius).toFixed(3));
		pctCircleRadius = bounds.horizontalmeters * 0.706;

		// console.log("circleRadius", circleRadius);
		let circleDia = bounds.horizontalmeters;
		let noOfCircles = bounds.verticalmeters/circleDia;
		// console.log("noOfCircles", noOfCircles);

		let horizontalMidPoint = getNewPoint(bounds.latLngs.b, 180, circleRadius);
		let centerPointC1 = getNewPoint(horizontalMidPoint, 270, circleRadius);

		// Main circles
		for(var i = 0; i < Math.ceil(noOfCircles); i++){
			var cityCircle = new google.maps.Circle({
		    strokeColor : '#6c6c6c',
        strokeWeight : 3.5,
        fillColor : '#926239',
        fillOpacity : 0.6,
        editable: true,
        draggable: true,
	      map: map,
	      clickable: true,
	      center: centerPointC1,
	      radius: pctCircleRadius
		  });

			let circle = {
        centerLat: centerPointC1.lat(),
        centerLng: centerPointC1.lng(),
        radiusMeters: pctCircleRadius
      }
      json.circles.push(circle);
	    json.circleObjects.push(cityCircle);

			let centerPointNext = getNewPoint(centerPointC1, 180, circleDia);
		  centerPointC1 = centerPointNext;
		  let lastCircleIndex = json.circleObjects.length - 1;
			createInfoWindowCircle(lastCircleIndex, cityCircle, map);
		}

		let noOfSecCircles = noOfCircles;

		let diff = noOfCircles - Math.floor(noOfCircles)
		// console.log("diff", diff);
		if(diff > 0.1){
			noOfSecCircles = Math.floor(noOfCircles)+1;
		} else {
			noOfSecCircles = Math.floor(noOfCircles);
		}

		// secodary circles
		if(getSecondaryCircles){
			let secRadius = pctCircleRadius/4;

			let verticalPoint = getNewPoint(bounds.latLngs.b, 180, circleDia);
			let circleCenterSec = getNewPoint(verticalPoint, 270, secRadius);

			for(var i = 1; i < noOfSecCircles; i++){

				var cityCircle = new google.maps.Circle({
			    strokeColor : '#6c6c6c',
	        strokeWeight : 3.5,
	        fillColor : '#926239',
	        fillOpacity : 0.6,
	        editable: true,
	        draggable: true,
		      map: map,
		      center: circleCenterSec,
		      radius: secRadius
			    });

				let circle = {
	        centerLat: circleCenterSec.lat(),
	        centerLng: circleCenterSec.lng(),
	        radiusMeters: secRadius
	      }
	      json.circles.push(circle);
		    json.circleObjects.push(cityCircle);

				let circlePointSecNext = getNewPoint(circleCenterSec, 180, circleDia);
			    circleCenterSec = circlePointSecNext;
			}

			// let secRadius = pctCircleRadius/4;
			let horizontalPointA = getNewPoint(bounds.latLngs.a, 180, circleDia);
			let circleCenterSecA = getNewPoint(horizontalPointA, 90, secRadius);

			// secodary circles
			for(var i = 1; i < noOfSecCircles; i++){
				var cityCircle = new google.maps.Circle({
			    strokeColor : '#6c6c6c',
	        strokeWeight : 3.5,
	        fillColor : '#926239',
	        fillOpacity : 0.6,
	        editable: true,
	        draggable: true,
		      map: map,
		      center: circleCenterSecA,
		      radius: secRadius
			    });

				let circle = {
	        centerLat: circleCenterSecA.lat(),
	        centerLng: circleCenterSecA.lng(),
	        radiusMeters: secRadius
	      }
	      json.circles.push(circle);
		    json.circleObjects.push(cityCircle);

				let circlePointSecANext = getNewPoint(circleCenterSecA, 180, circleDia);
			    circleCenterSecA = circlePointSecANext;
			}
			console.log(json)
			//End
		}
	}
}

function drawRecWithCircles() {
	if(drawingManager) {
    drawingManager.setMap(null);
  }
  drawingManager = null;
  drawingManager = new google.maps.drawing.DrawingManager();
  //Setting options for the Drawing Tool. In our case, enabling Polygon shape.
  drawingManager.setOptions({
    drawingMode : google.maps.drawing.OverlayType.RECTANGLE,
    drawingControl : true,
    drawingControlOptions : {
        position : google.maps.ControlPosition.LEFT_CENTER,
        drawingModes : [ google.maps.drawing.OverlayType.RECTANGLE ]
    },
    rectangleOptions : {
        strokeColor : '#6c6c6c',
        strokeWeight : 3.5,
        fillColor : '#926239',
        fillOpacity : 0.6,
        editable: true,
        draggable: true,
        clickable: true
    }   
  });
  google.maps.event.addListener(drawingManager, 'overlaycomplete', function (e) {
    if (e.type === google.maps.drawing.OverlayType.RECTANGLE) {
      console.log("2nd")
      drawingManager.setDrawingMode(null);
      drawingManager.setMap(null);

      if (e.type === google.maps.drawing.OverlayType.RECTANGLE) {

        // Switch back to non-drawing mode after drawing a shape.
        drawingManager.setDrawingMode(null);

        // Add an event listener that selects the newly-drawn shape when the user
        // mouses down on it.
        var newShape = e.overlay;

        let bounds = getBoundsSize(newShape.getBounds());
        // console.log(bounds);

	      json.recObjects.push(newShape);
	      let lastRecIndex = json.recObjects.length - 1;
	      let infoWindowPosition = newShape.getBounds().getNorthEast();
	      createInfoWindowRec(lastRecIndex, newShape, map, infoWindowPosition)
        drawCircles(bounds, map);
      }
    }
  });
  // Loading the drawing Tool in the Map.
  drawingManager.setMap(map);
}