
function initMap()
{

}
function getcoords(address)
{
    let geocoderEDIT = new google.maps.Geocoder();
    geocoderEDIT.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            //var latitude = results[0].geometry.location.latitude;
            //var longitude = results[0].geometry.location.longitude;
            $('#lat').val(results[0].geometry.location.lat());
            $('#lng').val(results[0].geometry.location.lng());
            //console.log(results[0].geometry.location.latitude + " " + results[0].geometry.location.longitude);
            //console.log($('#lat').val() + ' ' + $('#lng').val());
            //alert(latitude);
        }
    });
}

$(document).ready(function () {



    function saveSite() {
        //check if inputs are empty
        allarefilled = true;
        $('input[class="needs-filled"]').each(function(){
            if($(this).val().replace(/^\s+|\s+$/g, "").length === 0) //Removes whitespace
            {
                console.log("ALL ARE NOT TRUE");
                allarefilled = false;
            }
        });

        if(!allarefilled)
        {
            alert("Please fill out all Site Information");
        }
        else {
            $.ajax({
                dataType: 'json',
                url: 'process/edit_site.php',
                type: 'POST',
                data: $('#edit-site-form').serialize(),
                beforeSend: function () {
                    $('#thinking-modal').foundation('open');
                },
            })
                .done(function (data) {
                    console.log(data);
                    $('#error-list').empty();
                    $('#thinking-modal').foundation('close');
                    if (data.success) {
                        $('#success_modal').foundation('open');
                    } else {
                        if (data['errors']['invalid-address']) {
                            $('#warning-modal').foundation('open');
                        } else {
                            for (let error in data['errors']) {
                                $('#error-list').append(data['errors'][error] + '<br />');
                            }
                            $('#fail-modal').foundation('open');
                        }
                    }
                })
                .fail(function (data, textStatus, errorThrown) {
                    $('#thinking-modal').foundation('close');
                    console.log(data);
                    console.log(textStatus);
                    console.log(errorThrown);
                });
        }
    }
    let address = '';
    input_address = $('#address');
    input_state = $('#state');
    input_city = $('#city');
    input_zip = $('#zip');
    address = input_address.val() + ', ' + input_city.val() + ', ' + input_state.val() + ', ' + input_zip.val() + ', ' + 'USA';
    getcoords(address);








    input_address.change(function(){
        if(input_city.val() != '' && input_state.val() != '' && input_zip.val() != '' && input_address != '')
        {
            address = input_address.val() + ', ' + input_city.val() + ', ' + input_state.val() + ', ' + input_zip.val() + ', ' + 'USA';
            getcoords(address);
        }
    });
    input_city.change(function(){
        if(input_city.val() != '' && input_state.val() != '' && input_zip.val() != '' && input_address != '')
        {
            address = input_address.val() + ', ' + input_city.val() + ', ' + input_state.val() + ', ' + input_zip.val() + ', ' + 'USA';
            getcoords(address);
        }
    });
    input_state.change(function(){
        if(input_city.val() != '' && input_state.val() != '' && input_zip.val() != '' && input_address != '')
        {
            address = input_address.val() + ', ' + input_city.val() + ', ' + input_state.val() + ', ' + input_zip.val() + ', ' + 'USA';
            getcoords(address);
        }
    });
    input_zip.change(function(){
        if(input_city.val() != '' && input_state.val() != '' && input_zip.val() != '' && input_address != '')
        {
            address = input_address.val() + ', ' + input_city.val() + ', ' + input_state.val() + ', ' + input_zip.val() + ', ' + 'USA';
            getcoords(address);
        }
    });



  $('#save-site-button').click(function(e) {
      console.log("HITTING");
    e.preventDefault();
    saveSite();
  });

  $('#warning-save').click(function(e) {
    $('#ignore-address').val("1");
    saveSite();
  });

});