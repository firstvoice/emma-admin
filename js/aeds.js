marker_array = [];
map_array = [];
function initMap() {

  var loc = {lat: 0.0, lng: 0.0};
  var icon = 'img/orange-dot.png';
  marker_array.push();
  var edit_map = new google.maps.Map(document.getElementById('edit-map'), {zoom: 2, center: loc});
  var edit_marker = new google.maps.Marker({position: loc, map: edit_map, icon: icon, draggable: true});
  map_array.push(edit_map);
  marker_array.push(edit_marker);
  google.maps.event.addListener(edit_marker, 'dragend', function(event){
    input_lat.val(event.latLng.lat());
    input_lng.val(event.latLng.lng());
    let pos={latitude: input_lat.val(),longitude: input_lng.val()};
    geocodePosition(pos);
  });

}

function geocodePosition(pos)
{
  //console.log(pos);
  //Sets closest address into input for each event modal..
  geocoder = new google.maps.Geocoder();
  geocoder.geocode
  ({
      latLng: new google.maps.LatLng(pos.latitude,pos.longitude)
    },
    function(results, status)
    {
      if (status == google.maps.GeocoderStatus.OK)
      {
        $('input[name="address"]').val(results[0].formatted_address);
      }
      else
      {
        $('input[name="address"]').val('N/A');
      }
    }
  );
}

function resetAccessories() {
  $('#pad-row').hide();
  $('#pad-select').empty();
  $('#pad-select').append('<option value="" selected="selected" hidden="hidden">-- Select Pad --</option>');
  $('#ped-pad-row').hide();
  $('#ped-pad-select').empty();
  $('#ped-pad-select').append('<option value="" selected="selected" hidden="hidden">-- Select Pad --</option>');
  $('#spare-pad-row').hide();
  $('#spare-pad-select').empty();
  $('#spare-pad-select').append('<option value="" selected="selected" hidden="hidden">-- Select Pad --</option>');
  $('#spare-ped-pad-row').hide();
  $('#spare-ped-pad-select').empty();
  $('#spare-ped-pad-select').append('<option value="" selected="selected" hidden="hidden">-- Select Pad --</option>');
  $('#pak-row').hide();
  $('#pak-select').empty();
  $('#pak-select').append('<option value="" selected="selected" hidden="hidden">-- Select Pak --</option>');
  $('#ped-pak-row').hide();
  $('#ped-pak-select').empty();
  $('#ped-pak-select').append('<option value="" selected="selected" hidden="hidden">-- Select Pak --</option>');
  $('#spare-pak-row').hide();
  $('#spare-pak-select').empty();
  $('#spare-pak-select').append('<option value="" selected="selected" hidden="hidden">-- Select Pak --</option>');
  $('#spare-ped-pak-row').hide();
  $('#spare-ped-pak-select').empty();
  $('#spare-ped-pak-select').append('<option value="" selected="selected" hidden="hidden">-- Select Pak --</option>');
  $('#battery-row').hide();
  $('#battery-select').empty();
  $('#battery-select').append('<option value="" selected="selected" hidden="hidden">-- Select Battery --</option>');
  $('#spare-battery-row').hide();
  $('#spare-battery-select').empty();
  $('#spare-battery-select').append('<option value="" selected="selected" hidden="hidden">-- Select Battery --</option>');
  $('#accessory-row').hide();
  $('#accessory-select').empty();
  $('#accessory-select').append('<option value="" selected="selected" hidden="hidden">-- Select Accessory --</option>');
}

$(document).ready(function () {
  resetAccessories();
  var aedsTable = $('#aeds-table').DataTable({
    "dom": 'Brtip',
    "paging": true,
    "pageLength": 20,
    "scrollCollapse": true,
    "processing": true,
    "serverSide": true,
    "deferRender": true,
    "order": [[2,'desc']],
    "ajax": {
      "url": "process/get_aeds.php",
      "type": "GET",
      "data": {
        "user-id": user['id'],
        "plan-id": user['emma_plan_id'],
        "drill": 0
      }
    },
    "columns": [
      {
        "data": "organization",
        "render": function (data, type, row, meta) {
          return "<a href='dashboard.php?content=aed&id=" + row['aed_id'] + "'>" + data + "</a>";
        }
      },
      {
        "data": "location",
        "render": function (data, type, row, meta) {
          return "<a href='dashboard.php?content=aed&id=" + row['aed_id'] + "'>" + data + "</a>";
        }
      },
      {
        "data": "brand",
        "render": function (data, type, row, meta) {
          return "<a href='dashboard.php?content=aed&id=" + row['aed_id'] + "'>" + data + "</a>";
        }
      },
      {
        "data": "model",
        "render": function (data, type, row, meta) {
          return "<a href='dashboard.php?content=aed&id=" + row['aed_id'] + "'>" + data + "</a>";
        }
      },
      {
        "data": "serialnumber",
        "render": function (data, type, row, meta) {
          return "<a href='dashboard.php?content=aed&id=" + row['aed_id'] + "'>" + data + "</a>";
        }
      },
      {
        "data": "lastcheck",
        "render": function (data, type, row, meta) {
          var date = new Date(data);
          date.setHours(date.getHours() + parseInt(user['timezone']));
          return `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
        }
      },
      {
        "data": "current",
        "render": function (data, type, row, meta) {
          return data == 1 ?
          "<span style='color: green'>Active</span>" :
            "<span style='color: red'>Inactive</span>";
        }
      }
    ]
  });


  $('#aeds-table thead th.text-search').each(function (i) {
    let title = $('#aeds-table thead th').eq($(this).index()).text();
    $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
  });

  $(aedsTable.table().container()).on('keyup', 'thead input.text-search', function () {
    aedsTable
      .column($(this).data('index'))
      .search(this.value)
      .draw();
  });

  $('#create-aed-form').submit(function (e) {
    e.preventDefault();
    $.ajax({
      dataType: 'json',
      url: 'process/create_aed.php',
      type: 'POST',
      data: $(this).serialize(),
      beforeSend: function () {
        THINKINGMODAL.foundation('open');
      },
    })
      .done(function (data) {
        console.log(data);
        THINKINGMODAL.foundation('close');
        if (data.success) {
          $('#aed-success-modal').foundation('open');
        } else {
          FAILMODAL.foundation('open');
        }
      })
      .fail(function (data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
    return false;
  });

  input_lat = $('#entered_lat');
  input_lng = $('#entered_lng');

  input_lat.change(function(){
    if(input_lat.val() != null && input_lng.val() != null)
    {
      let pos={latitude: input_lat.val(),longitude: input_lng.val()};
      geocodePosition(pos);
    }
  });

  input_lng.change(function(){
    if(input_lat.val() != null && input_lng.val() != null)
    {
      let pos={latitude: input_lat.val(),longitude: input_lng.val()};
      geocodePosition(pos);
    }
  });

  $('#create_aed_form_brand_id').change(function () {
    const brand_id = $('#create_aed_form_brand_id option:selected').val();
    $.ajax({
      url: 'process/get_aed_models.php',
      type: 'GET',
      data: {'brand-id': brand_id},
      beforeSend: function () {
        resetAccessories();
        $('#create_aed_form_model_id').empty();
        $('#create_aed_form_model_id').append('<option selected="selected" hidden="hidden">-- Select Model --</option>')
      },
    })
      .done(function (data) {
        data = JSON.parse(data);
        data['models'].forEach((model) => {
          $('#create_aed_form_model_id').append("<option value='"+model['aed_model_id']+"'>"+model['model']+"</option>");
        });
        console.log(data);
      })
      .fail(function (data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });

    $('#create_aed_form_model_id').change(function () {
      const model_id = $('#create_aed_form_model_id option:selected').val();
      $.ajax({
        url: 'process/get_accessory_types.php',
        type: 'POST',
        data: {'model-id': model_id},
        beforeSend: function () {
          resetAccessories();
        },
      })
        .done(function (data) {
          data = JSON.parse(data);
          console.log(data);
          if(data['pads'].length > 0) {
            $('#pad-row').show();
            $('#ped-pad-row').show();
            $('#spare-pad-row').show();
            $('#spare-ped-pad-row').show();
          }
          data['pads'].forEach((pad) => {
            if(pad['pediatric'] === "1") {
              $('#ped-pad-select').append('<option value="'+pad['aed_pad_type_id']+'">'+pad['type']+'</option>');
              $('#spare-ped-pad-select').append('<option value="'+pad['aed_pad_type_id']+'">'+pad['type']+'</option>');
            } else {
              $('#pad-select').append('<option value="'+pad['aed_pad_type_id']+'">'+pad['type']+'</option>');
              $('#spare-pad-select').append('<option value="'+pad['aed_pad_type_id']+'">'+pad['type']+'</option>');
            }
          });

          if(data['paks'].length > 0) {
            $('#pak-row').show();
            $('#ped-pak-row').show();
            $('#spare-pak-row').show();
            $('#spare-ped-pak-row').show();
          }
          data['paks'].forEach((pak) => {
            if(pak['pediatric'] === "1") {
              $('#ped-pak-select').append('<option value="'+pak['aed_pak_type_id']+'">'+pak['type']+'</option>');
              $('#spare-ped-pak-select').append('<option value="'+pak['aed_pak_type_id']+'">'+pak['type']+'</option>');
            } else {
              $('#pak-select').append('<option value="'+pak['aed_pak_type_id']+'">'+pak['type']+'</option>');
              $('#spare-pak-select').append('<option value="'+pak['aed_pak_type_id']+'">'+pak['type']+'</option>');
            }
          });
          if(data['batteries'].length > 0) {
            $('#battery-row').show();
            $('#spare-battery-row').show();
          }
          data['batteries'].forEach((battery) => {
            $('#battery-select').append('<option value="'+battery['aed_battery_type_id']+'">'+battery['type']+'</option>');
            $('#spare-battery-select').append('<option value="'+battery['aed_battery_type_id']+'">'+battery['type']+'</option>');
          });
          if(data['accessories'].length > 0) $('#accessory-row').show();
          data['accessories'].forEach((accessory) => {
            $('#accessory-select').append('<option value="'+accessory['aed_accessory_type_id']+'">'+accessory['type']+'</option>');
          });
          console.log(data);
        })
        .fail(function (data, textStatus, errorThrown) {
          console.log(data);
          console.log(textStatus);
          console.log(errorThrown);
        });
    })
  });

  $('#create_aed_form_org_id').change(function () {
    org_id = $('#create_aed_form_org_id option:selected').val();
    $.ajax({
      url: 'process/get_org_locations.php',
      type: 'GET',
      data: {'org-id': org_id},
      beforeSend: function () {
        $('#create_aed_form_loc_id').empty();
      },
    })
      .done(function (data) {
        data = JSON.parse(data);
        data['locations'].forEach((location) => {
          $('#create_aed_form_loc_id').append("<option value='"+location['id']+"'>"+location['name']+"</option>");
        });
        console.log(data);
      })
      .fail(function (data, textStatus, errorThrown) {
        console.log(data);
        console.log(textStatus);
        console.log(errorThrown);
      });
  });

});