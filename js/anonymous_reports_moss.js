$(document).ready(function () {

    let sosHelpTable = $('#ar-active-table').DataTable({
        "dom": 'Brtip',
        "paging": true,
        "pageLength": 20,
        "scrollCollapse": true,
        "processing": true,
        "serverSide": true,
        "deferRender": true,
        "order": [[0,'desc']],
        "ajax": {
            "url": "process/get_active_arm.php",
            "type": "GET",
            "data": {
                "emma-plan-id": user['emma_plan_id'],
            }
        },
        "columns": [
            {
                "data": "created_date",
                "render": function (data, type, row, meta) {
                    var date = new Date(data);
                    date.setHours(date.getHours() + parseInt(user['timezone']));
                    const displayDate = `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
                    return "<a href='dashboard.php?content=anonymous_report_moss&id=" + row['emma_moss_id'] + "'>" + displayDate + "</a>";
                }
            },
            {
                "data": "description",
            }
        ]
    });

    $('#ar-active-table thead th.text-search').each(function (i) {
        let title = $('#ar-active-table thead th').eq($(this).index()).text();
        $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
    });

    $(sosHelpTable.table().container()).on('keyup', 'thead input.text-search', function () {
        sosHelpTable
            .column($(this).data('index'))
            .search(this.value)
            .draw();
    });


    let sosClosedTable = $('#ar-closed-table').DataTable({
        "dom": 'Brtip',
        "paging": true,
        "pageLength": 20,
        "scrollCollapse": true,
        "processing": true,
        "serverSide": true,
        "deferRender": true,
        "order": [[0,'desc']],
        "ajax": {
            "url": "process/get_closed_arm.php",
            "type": "GET",
            "data": {
                "emma-plan-id": user['emma_plan_id'],
            }
        },
        "columns": [
            {
                "data": "closed_date",
                "render": function (data, type, row, meta) {
                    var date = new Date(data);
                    date.setHours(date.getHours() + parseInt(user['timezone']));
                    const displayDate = `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
                    return "<a href='dashboard.php?content=anonymous_report_moss&id=" + row['emma_moss_id'] + "'>" + displayDate + "</a>";
                }
            },
            {
                "data": "description",
            }
        ]
    });

    $('#ar-closed-table thead th.text-search').each(function (i) {
        let title = $('#ar-closed-table thead th').eq($(this).index()).text();
        $(this).html('<input class="text-search" type="text" placeholder="Search ' + title + '" data-index="' + i + '" />');
    });

    $(sosClosedTable.table().container()).on('keyup', 'thead input.text-search', function () {
        sosClosedTable
            .column($(this).data('index'))
            .search(this.value)
            .draw();
    });
});