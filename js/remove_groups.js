$(document).ready(function () {
    var usersTable = $('#users-table').DataTable({
        "dom": 'Brtip',
        "paging": true,
        "pageLength": 25,
        "scrollCollapse": true,
        "processing": true,
        "serverSide": true,
        "deferRender": true,
        "ajax": {
            "url": "process/get_users.php",
            "type": "GET",
            "data": {
                "emma-plan-id": user['emma_plan_id']
            }
        },
        "columns": [
            {
                "data": " ",
                "render": function (data, type, row, meta) {
                    return "<div class='text-center'><input type='checkbox' name='selected_users[]' value='" + row['id'] + "' /></div>";
                }
            },
            {
                "data": "username",
                "render": function (data, type, row, meta) {
                    return "<a href='dashboard.php?content=user&id=" + row['id'] + "'>" + data + "</a>";
                }
            },
            {"data": "firstname"},
            {"data": "lastname"},
            {"data": "group"}
        ]
    });

    $('#user_remove_group').click( function() {
        $('#remove_group_modal').foundation('open');
    });

    $('#remove_group_submit').click( function() {
        $.ajax({
            dataType: 'json',
            url: 'process/remove_user_list_from_groups.php',
            type: 'POST',
            data: $('#users_form').serialize() + "&" + $('#remove_group_form').serialize(),
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            }
        })
            .done(function (data) {
                console.log(data);
                $('#thinking-modal').foundation('close');
                $('#error-list').empty();
                if (data.success) {
                    $('#success_modal').foundation('open');
                } else {
                    for (var error in data['errors']) {
                        $('#error-list').append(data['errors'][error] + '<br />');
                    }
                    $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
    });

});