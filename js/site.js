var lineChartEventsDrillsData;
var lineChartEventsDrills;

function drawLineChartEventsDrills() {
    var options = {
        curveType: 'function',
        legend: {position: 'bottom'},
        animation: {
            startup: true,
            duration: 1000,
            easing: 'out'
        }
    };
    lineChartEventsDrills.draw(lineChartEventsDrillsData, options);
}

function drawAllCharts() {
    drawLineChartEventsDrills();
}

function fetchData() {
    $.ajax({
        dataType: 'json',
        url: 'process/get_site_statistics.php',
        type: 'GET',
        data: {'emma-plan-id': user['emma_plan_id'], 'emma-site-id': document.getElementById('site_id').value}
    })
        .done(function (data) {
            console.log(data);
            if (data.success) {
                for (var i in data['events-drills-months']) {
                    if(data['events-drills-months'].hasOwnProperty(i)) {
                      var month = moment({
                        y: data['events-drills-months'][i]['year'],
                        M: data['events-drills-months'][i]['month'] - 1
                      });
                      var arr = [
                        month.format('MM-YYYY'),
                        Number(data['events-drills-months'][i]['event_count']),
                        Number(data['events-drills-months'][i]['drill_count'])];
                      lineChartEventsDrillsData.addRows([arr]);
                    }
                }
                drawAllCharts();
            } else {
                alert('Error loading statistics');
                console.log(data);
            }
        })
        .fail(function (data, textStatus, errorThrown) {
            alert('Error loading statistics');
            console.log(data);
            console.log(textStatus);
            console.log(errorThrown);
        });
}

function initCharts() {
    lineChartEventsDrillsData = new google.visualization.DataTable();
    lineChartEventsDrillsData.addColumn('string', 'Month');
    lineChartEventsDrillsData.addColumn('number', 'Events');
    lineChartEventsDrillsData.addColumn('number', 'Drills');
    lineChartEventsDrills = new google.visualization.LineChart(document.getElementById('line-chart-events-drills'));

    fetchData();
    //setInterval(fetchData, '3000');
}

$(document).ready(function () {
    $('.group-link').click(function () {
        $('#group-modal').foundation('open');
    });
    google.charts.load('current', {packages: ["corechart"]});
    google.charts.setOnLoadCallback(initCharts);
    $(window).resize(drawAllCharts);
});