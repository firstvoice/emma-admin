$(document).ready(function () {

    $('#new-resource-form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: 'process/create_new_resource.php',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function () {
                $('#thinking-modal').foundation('open');
            },
            success: function (data) {
                $('#thinking-modal').foundation('close');
                console.log(data);
            }
        })
            .done(function (data) {
                console.log(data);
                $('#thinking-modal').foundation('close');
                $('#error-list').empty();
                if (data.success) {
                    $('#success_modal').foundation('open');

                } else {
                    if(data['invalid-file-type']){
                        $('#invalid_modal').foundation('open');
                    }
                    // for (var error in data['errors']) {
                    //     $('#error-list').append(data['errors'][error] + '<br />');
                    // }
                    // $('#fail-modal').foundation('open');
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('#thinking-modal').foundation('close');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
    });





});