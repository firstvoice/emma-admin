<?php include('include/pre_login_header.php'); ?>

<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Login</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #FFFFFF;">


<?php include('include/index_top_bar.php'); ?>

<div class="row expanded the-clock">
    <div class="large-12 medium-12 small-12 columns" style="margin: 0 auto;">

        <P class="hp-verbiage-lg" style="text-align: center">Welcome to EMMA</P>
    </div>
    <div class="row expanded">
        <div class="large-12 medium-12 small-12 columns" style="margin: 0 auto;">
            <div class="large-6 medium-12 small-12 column">

                <iframe width="840" height="472" src="https://www.youtube.com/embed/qiMKzidLO7E?controls=1&showinfo=0&rel=0&autoplay=0&loop=1&mute=0" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>

    <div class="large-12 medium-12 small-12 column" style="text-align: center;padding-top: 4vh">
        <div class="row expanded">
            <div class="large-3 medium-6 small-6 columns">
                <a class ="button2" href="company.php">About Us</a>
            </div>
            <div class="large-3 medium-6 small-6 column">
                <a data-open="contact-us-modal" class="button2 typebutton" data-type="Learn More - LPVID1">Learn More</a>
            </div>
            <div class="large-3 medium-6 small-6 columns">
                <a data-open="contact-us-modal" class="button2 typebutton" data-type="Schedule Demo - LPVID1">Schedule Demo</a>
            </div>
            <div class="large-3 medium-6 small-6 columns">
                <a class="button2" href="emma_videos.php">More Videos</a>
            </div>
        </div>
    </div>
    <div><p>&nbsp;</p><br/><br/><br/></div>
</div>
<?php include('include/footer.php'); ?>

<?php include('include/pre_login_modals.php'); ?>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>


