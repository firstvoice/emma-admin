<?php include('include/pre_login_header.php'); ?>
<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Login</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #FFFFFF;">


<?php include('include/index_top_bar.php'); ?>

<div class="row expanded the-clock" style="height: 100%">
    <div class="large-12 medium-12 small-12 columns" style="padding-top: 2vh;">
        <p class="advocate_title">Become a EMMA Advocate</p>
        <p class="advocate_text">Become an EMMA advocate for a safe workplace/learning environment by requesting your
            EMMA Advocate Starter Kit below. This
            kit includes a flyer and social media graphics that you can use to share the importance of keeping
            our workplace/learning environment protected in active shooter and other life threatening situations. Share
            with your workplace/learning environment, bring to your next
            meeting, and send to other concerned individuals.</p>
        <form class="text-left contact-us-form" action="#" method="post">
            <div class="row expanded" style="padding-top: 2em; padding-bottom: 1em;">

                <input type="hidden" id="contact-us-type" name="type" value="Become An Advocate">
                <div class="large-6 medium-12 small-12 columns" style="text-align:center;">
                    <div class="row">
                        <div class="large-6 medium-6 columns">
                            <label>First Name<span style="color: red">*</span></label>
                            <input type="text" name="first-name" id="firstname"/>
                        </div>
                        <div class="large-6 medium-6 columns">
                            <label>Last Name<span style="color: red">*</span></label>
                            <input type="text" name="last-name" id="lastname"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-6 medium-6 columns">
                            <label>Email<span style="color: red">*</span></label>
                            <input type="text" name="email" id="email"/>
                        </div>
                        <div class="large-6 medium-6 columns">
                            <label>Organization</label>
                            <input type="text" name="org" id="org"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-6 medium-6 columns">
                            <label>Mobile</label>
                            <input type="text" name="mobile" id="mobile"/>
                        </div>
                        <div class="large-6 medium-6 columns">
                            <label>Landline</label>
                            <input type="text" name="landline" id="landline"/>
                        </div>
                        <hr>
                    </div>
                    <div class="row">
                        <div class="large-12 medium-12 columns">
                            <label id="comments">Comments<span style="color: red">*</span></label>
                            <textarea name="comments" id="comments" class="text"
                                      style="color: black;"></textarea>
                        </div>
                    </div>
                    <div class="large-offset-10 large-2 columns">
                        <div class="button-group expanded">
                            <input type="submit" class="button" value="Submit"/>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div><p>&nbsp;</p><br/><br/><br/><br/><br/><br/><br/></div>


<?php include('include/footer.php'); ?>

<?php include('include/pre_login_modals.php'); ?>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/index.js"></script>


