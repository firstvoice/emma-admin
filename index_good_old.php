<?php
/**
 * Created by PhpStorm.
 * User: JHolmes
 * Date: 12/26/2018
 * Time: 11:31 AM
 */
session_start();
include('include/db.php');
require('vendor/php-jwt-master/src/JWT.php');
require('vendor/php-jwt-master/src/BeforeValidException.php');
require('vendor/php-jwt-master/src/ExpiredException.php');
require('vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('config/config.json'));

if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != "on"){
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    exit();
}

$errorMessage = '';
if (isset($_COOKIE['jwt'])) {
    try {
        $token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key,
            array('HS512'));
        header('Location: dashboard.php');
    } catch (\Firebase\JWT\BeforeValidException $bve) {
        $errorMessage = $bve->getMessage();
    } catch (\Firebase\JWT\ExpiredException $ee) {
        $errorMessage = $ee->getMessage();
    } catch (\Firebase\JWT\SignatureInvalidException $sie) {
        $errorMessage = $sie->getMessage();
    } catch (Exception $e) {
        $errorMessage = $e->getMessage();
    }
}
//login with token from emma admin
if(isset($_GET['token'])){

    $loginToken = $fvmdb->real_escape_string($_GET['token']);

    $users = $fvmdb->query("
                    SELECT u.id, CONCAT(u.firstname, ' ', u.lastname) AS full_name, u.username, u.emma_plan_id, u.emma_pin, u.auth, u.emma_code_generator, u.emma_plans, u.plan_god, t.plan_id AS token_plan, p.emails AS plan_emails, p.texts AS plan_texts
                    FROM users u
                    JOIN emma_admin_login_tokens AS t ON t.user_id = u.id
                    JOIN emma_plans AS p ON t.plan_id = p.emma_plan_id
                    WHERE t.token = '".$loginToken."'
                    AND t.token_datetime > NOW()-INTERVAL 15 MINUTE
                    AND u.emma_plan_id IS NOT NULL
                ");
    if ($users->num_rows > 0) {
        $user = $users->fetch_assoc();

        $admin = false;
        $security = false;
        $plansecurity = false;
        $groups = $fvmdb->query("
                        SELECT g.*, e.securities, e.panic
                        FROM emma_user_groups ug
                        JOIN emma_groups g ON ug.emma_group_id = g.emma_group_id
                        LEFT JOIN emma_plans e ON g.emma_plan_id = e.emma_plan_id
                        WHERE ug.user_id = '" . $user['id'] . "'
                        AND e.emma_plan_id = '".$user['token_plan']."'
                      ");
        while ($group = $groups->fetch_assoc()) {
            if ($group['admin']) $admin = true;
            if ($group['security']) $security = true;
            if ($group['geofences']) $geofences = true;
            if ($group['911admin']) $admin911 = true;
            if ($group['securities']) $plansecurity = true;
            if ($group['panic']) $plansos = true;
        }

        //      if ($admin || $security) {
        $tokenId = base64_encode(randomToken(32));
        $issuedAt = time();
        $notBefore = $issuedAt;
        $expire = $notBefore + (10 * 365 * 24 * 60 * 60);
        $serverName = 'tsdemos.com';

        $data = [
            'iat' => $issuedAt,
            'jti' => $tokenId,
            'iss' => $serverName,
            'nbf' => $notBefore,
            'exp' => $expire,
            'data' => [
                'id' => $user['id'],
                'full_name' => $user['full_name'],
                'username' => $user['username'],
                'emma_plan_id' => $user['emma_plan_id'],
                'emma_pin' => $user['emma_pin'],
                'auth' => $user['auth'],
                'privilege' => [
                    'admin' => $admin,
                    'security' => $security,
                    'plansecurity' => $plansecurity,
                    'plansos' => $plansos,
                    'geofences' => $geofences,
                    'admin911' => $admin911,
                    'code_generator' => $user['emma_code_generator'] == 1,
                    'plans' => $user['emma_plans'] == 1,
                    'plan_god' => $user['plan_god'] == 1,
                    'plan_emails' => $user['plan_emails'] == 1,
                    'plan_texts' => $user['plan_texts'] == 1
                ]
            ]
        ];

        $jwt = Firebase\JWT\JWT::encode($data, $CONFIG->key, 'HS512');
        setcookie('jwt', $jwt, time() + (9 * 60 * 60), '/');

        if(isset($_GET['content'])){
            header('Location: dashboard.php?content='.$_GET['content']);
        }
        else {
            header('Location: dashboard.php');
        }


    }
}
if (isset($_POST['login']) && !empty($_POST['username'])) {
    try {
        $username = $fvmdb->real_escape_string($_POST['username']);
        $password = $fvmdb->real_escape_string($_POST['password']);
            $users = $fvmdb->query("
                SELECT u.id, CONCAT(u.firstname, ' ', u.lastname) AS full_name, u.username, u.emma_plan_id, u.emma_pin, u.auth, u.emma_code_generator, u.emma_plans, u.plan_god, p.emails AS plan_emails, p.texts AS plan_texts
                FROM users u
                JOIN emma_plans AS p ON p.emma_plan_id = u.emma_plan_id
                WHERE u.username = '" . $username . "'
                AND u.password = PASSWORD('" . $password . "')
                AND u.emma_plan_id IS NOT NULL
            ");
            if ($users->num_rows > 0) {
                $user = $users->fetch_assoc();

                $admin = false;
                $security = false;
                $plansecurity = false;
                $groups = $fvmdb->query("
                    SELECT g.*, e.securities, e.panic
                    FROM emma_user_groups ug
                    JOIN emma_groups g ON ug.emma_group_id = g.emma_group_id
                    LEFT JOIN emma_plans e ON g.emma_plan_id = e.emma_plan_id
                    WHERE ug.user_id = '" . $user['id'] . "'
                  ");
                while ($group = $groups->fetch_assoc()) {
                    if ($group['admin']) $admin = true;
                    if ($group['security']) $security = true;
                    if ($group['geofences']) $geofences = true;
                    if ($group['911admin']) $admin911 = true;
                    if ($group['securities']) $plansecurity = true;
                    if ($group['panic']) $plansos = true;
                }

//      if ($admin || $security) {
                $tokenId = base64_encode(randomToken(32));
                $issuedAt = time();
                $notBefore = $issuedAt;
                $expire = $notBefore + (10 * 365 * 24 * 60 * 60);
                $serverName = 'tsdemos.com';

                $data = [
                    'iat' => $issuedAt,
                    'jti' => $tokenId,
                    'iss' => $serverName,
                    'nbf' => $notBefore,
                    'exp' => $expire,
                    'data' => [
                        'id' => $user['id'],
                        'full_name' => $user['full_name'],
                        'username' => $user['username'],
                        'emma_plan_id' => $user['emma_plan_id'],
                        'emma_pin' => $user['emma_pin'],
                        'auth' => $user['auth'],
                        'privilege' => [
                            'admin' => $admin,
                            'security' => $security,
                            'plansecurity' => $plansecurity,
                            'plansos' => $plansos,
                            'geofences' => $geofences,
                            'admin911' => $admin911,
                            'code_generator' => $user['emma_code_generator'] == 1,
                            'plans' => $user['emma_plans'] == 1,
                            'plan_god' => $user['plan_god'] == 1,
                            'plan_emails' => $user['plan_emails'] == 1,
                            'plan_texts' => $user['plan_texts'] == 1
                        ]
                    ]
                ];

                $jwt = Firebase\JWT\JWT::encode($data, $CONFIG->key, 'HS512');
                setcookie('jwt', $jwt, time() + (9 * 60 * 60), '/');
//      } else {
//        $errorMessage = 'User is not in an Admin or Security group';
//      }
                $events = $fvmdb->query("
                    SELECT e.*
                    FROM emergencies e
                    WHERE e.emma_plan_id = '". $user['emma_plan_id'] ."'
                    AND e.active = '1'
                ");
                if($event = $events->fetch_assoc()){
                    header('Location: dashboard.php?content=event&id='.$event['emergency_id']);
                }else {
                    header('Location: dashboard.php');
                }
            } else {
                $errorMessage = 'Invalid Username/Email or Password';
            }
    } catch (Exception $e) {
        $e->getMessage();
    }
}elseif(isset($_POST['login']) && empty($_POST['username'])){
    $errorMessage = 'Invalid Username/Email or Password';
}
?>
<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Login</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
</head>
<body style="background-color: #0078c1;">
<div id="main" class="row expanded">
    <div class="large-10 medium-10 small-12 columns" style="margin: 0 auto;">
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>"
              method="post">
            <!--      <div class="row">-->
            <!--        <div class="medium-12 columns">-->
            <!--          <h3 class="text-center text">--><?php //echo PROGRAM; ?><!--</h3>-->
            <!--        </div>-->
            <!--      </div>-->
            <div class="row expanded">
                <div class="large-4 medium-6 small-10 columns" style="margin: 0 auto;">
                    <a href="index.php"><img src="img/emma_admin_logo.png" style="width: 100%;" alt=""></a>
                </div>
            </div>
            <div class="row expanded">
                <div class="large-4 medium-8 small-10 columns" style="margin: 0 auto;">
                    <label class="text">Username/Email</label>
                    <input type="text" name="username" id="username"/>
                </div>
            </div>
            <div class="row expanded">
                <div class="large-4 medium-8 small-10 columns" style="margin: 0 auto;">
                    <label class="text">Password</label>
                    <input type="password" name="password" id="password"/>
                </div>
            </div>
            <div class="row expanded">
                <div class="large-4 medium-8 small-10 columns" style="margin: 0 auto;">
                    <?php if (isset($errorMessage) &&
                        $errorMessage) echo '<div class="callout small alert alert-message">' .
                        $errorMessage . '</div>'; ?>
                </div>
            </div>
            <div class="row expanded">
                <div class="large-4 medium-8 small-10 columns" style="margin: 0 auto;">
                    <input class="button expanded" type="submit" name="login" id="login" value="Login" style="background-color: yellow; color: black"/>
                </div>
            </div>
            <div class="row expanded">
                <div class="large-12 medium-12 small-12 columns text-center">
                    <a href="#" data-open="contact-us-modal" style="color: #C3E2F7">Contact Us</a> |
                    <a href="#" data-open="forgot-password-modal" style="color: #C3E2F7">Forgot Password</a>
                </div>

            </div>
        </form>
    </div>

    <footer>
        <div class="row expanded" style="padding-bottom: 25px">
            <div class="large-9 medium-6 columns">
            </div>
            <div class="large-3 medium-6 small-12 columns">
                <?php
                if(HOMESITELINK2 != '' || HOMESITELOGO2 != '' || HOMESITELOGO2SIZE != '' || HOMESITELOGO2SIZE ){
                    echo'<a target="_blank" href="'.HOMESITELINK2.'"><img src="img/'.HOMESITELOGO2.'" width="'.HOMESITELOGO2SIZE.'" height="'.HOMESITELOGO2SIZE.'"></a> <br>';
                }
                if(HOMESITELINK1 != '' || HOMESITELOGO1 != '' || HOMESITELOGO1SIZE != '' || HOMESITELOGO1SIZE) {
                    echo '    <a target="_blank" href="'.HOMESITELINK1.'"><img src="img/'.HOMESITELOGO1.'" width="'.HOMESITELOGO1SIZE.'" height="'.HOMESITELOGO1SIZE.'"></a>';
                }
                ?>
            </div>
        </div>
    </footer>
</div>
<div id="thinking-modal" class="reveal callout text-center tiny" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4 style="color:#e5e5e5;">Working...</h4>
    <img style="width:2rem;height:2rem;" src="img/ajax_loader_blue.gif"/>
</div>
<div id="password-success-modal" class="reveal callout success text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <br>
    <p>Please check your email for your temporary password. It may take up to 5 minutes for you to receive.
        If you do not receive it, please check your junk/spam folder before contacting us.</p>
    <a data-close class="button success">OK</a>
</div>
<div id="contact-us-success-modal" class="reveal callout success text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <br>
    <p>Thank you for contacting us. We have received your message and will get back to you shortly.</p>
    <a data-close class="button success">OK</a>
</div>
<div id="fail-modal" class="reveal callout alert text-center tiny" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4 style="color:darkred">Failed...</h4>
    <div id="error-list" class="text-left"></div>
    <a class="button" style="background-color: darkred" data-close>OK</a>
</div>
<div id="forgot-password-modal" class="reveal callout text-center" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
    <h4>Forgot Password</h4>
    <hr>
    <form id="forgot-password-form" class="text-left" action="#" method="post">
        <label>Username/Email<span style="color: red;"> *</span>
            <input type="text" name="current-email" id="current-email" placeholder="username@domain.com" required/>
        </label>
        <div class="button-group expanded">
            <a data-close="" class="button alert">Cancel</a>
            <input type="submit" class="button" value="Submit"/>
        </div>
    </form>
</div>
<div id="contact-us-modal" class="reveal callout text-center" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
     <form id="contact-us-form" class="text-left" action="#" method="post"><input type="hidden" id="contact-us-type" name="type">
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>First Name<span style="color: red">*</span></label>
                <input type="text" name="first-name" id="firstname"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Last Name<span style="color: red">*</span></label>
                <input type="text" name="last-name" id="lastname"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Email<span style="color: red">*</span></label>
                <input type="text" name="email" id="email"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Organization</label>
                <input type="text" name="org" id="org"/>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
                <label>Mobile</label>
                <input type="text" name="mobile" id="mobile"/>
            </div>
            <div class="large-6 medium-6 columns">
                <label>Landline</label>
                <input type="text" name="landline" id="landline"/>
            </div>
            <hr>
        </div>
        <div class="row">
            <div class="large-12 medium-12 columns">
                <label id="comments">Comments<span style="color: red">*</span></label>
                <textarea name="comments" id="comments" class="text" style="color: black;"></textarea>
            </div>
        </div>
        <div class="large-12 columns">
            <div class="button-group expanded">
                <a data-close="" class="button alert">Cancel</a>
                <input type="submit" class="button" value="Submit"/>
            </div>
        </div>
    </form>
</div>
</body>

<?php include('include/scripts.php'); ?>


