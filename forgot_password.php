<?php include('include/pre_login_header.php'); ?>
<!DOCTYPE html lang="en-US">

<html lang="en-US">

<head>
    <title>EMMA Login</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <script src="https://kit.fontawesome.com/37e3574887.js"></script>
</head>
<body style="background-color: #FFFFFF;">


<?php include('include/index_top_bar.php'); ?>

<div class="row expanded the-clock">
    <div class="large-12 medium-12 small-12 columns" style="margin: 0 auto;">
        <P class="MMM-header" style="text-align: center">Making Minutes Matter!</P>
        <p class="hp-verbiage-lg">NOTIFY. RESPOND. COMMUNICATE.</p>
    </div>
    <div class="large-12 medium-12 small-12 columns innerclock">
        <div class="row expanded" style="padding-bottom: 5em">
            <div class="large-8 medium-12 small-12 column">
                <img src="img/emma-devices.png" width="75%" height="75%"/>
            </div>
        </div>
    </div>
</div>
<?php include('include/footer.php'); ?>
<?php include('include/pre_login_modals.php'); ?>
</body>

<?php include('include/scripts.php'); ?>
<script src="js/forgot_password.js"></script>
<script src="js/index.js"></script>
