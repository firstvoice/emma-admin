<?php
if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != "on"){
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    exit();
}
require 'vendor/autoload.php';
require "vendor/Classes/PHPExcel.php";
require('include/db.php');
require('vendor/php-jwt-master/src/JWT.php');
require('vendor/php-jwt-master/src/BeforeValidException.php');
require('vendor/php-jwt-master/src/ExpiredException.php');
require('vendor/php-jwt-master/src/SignatureInvalidException.php');
$CONFIG = json_decode(file_get_contents('config/config.json'));


function get_ip_address(){
    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
        if (array_key_exists($key, $_SERVER) === true){
            foreach (explode(',', $_SERVER[$key]) as $ip){
                $ip = trim($ip); // just to be safe

                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                    return $ip;
                }
            }
        }
    }
    return 'unknown';
}



/**
 * @param int $length
 * @return string
 * @throws Exception
 */

$USER = null;
try {
    $token = Firebase\JWT\JWT::decode($_COOKIE['jwt'], $CONFIG->key,
        array('HS512'));
} catch (\Firebase\JWT\BeforeValidException $bve) {
    //echo $bve->getMessage();
    header('Location: index.php');
    exit();
} catch (\Firebase\JWT\ExpiredException $ee) {
    //echo $ee->getMessage();
    header('Location: index.php');
    exit();
} catch (\Firebase\JWT\SignatureInvalidException $sie) {
    //echo $sie->getMessage();
    header('Location: index.php');
    exit();
} catch (Exception $e) {
    //echo 'UNKNOWN EXCEPTION: ';
    //echo $e->getMessage();
    header('Location: index.php');
    exit();
}
$USER = $token->data;
if(empty($USER) || is_object($USER) === false || empty($token)){
    header('Location: index.php');
    error_log('redirected due to null $USER object ip address: '.get_ip_address().' with Content: '.$emmadb->real_escape_string($_GET['content']));
    exit();
}


//These are pages that these privilege levels CAN see and access.
// admin is all, basic user is none (except for user_home as default)
//if the content name is not in the array then the user can't access that page
$security_privilege = array('home','user','edit_user','securities','geofence_demo','help','profile');
$admin911_privilege = array('user_home','home_events','event','resources','resource_type','user','edit_user','geofence_demo','help','profile');
$codeGen_privilege = array('user_home','user','edit_user','geofence_demo','help','profile');

//If user doesn't fall within one of the array's above, they get these privileges.
$catchall_privilege = array('user_home','user','edit_user','geofence_demo','help','help_type','display_help','profile');

//This is the array that holds all pages the CURRENT user is allowed to visit, based on security privilege level
//objects will be added to this array
$currentUser_privilege = array('');


//Append privileges to $currentUser_privilege array based on security level.
if($USER->privilege->security){
    $currentUser_privilege = array_merge($currentUser_privilege,$security_privilege);
}
if($USER->privilege->admin911)
{
    $currentUser_privilege = array_merge($currentUser_privilege,$admin911_privilege);
}
if($USER->privilege->code_generator)
{
    $currentUser_privilege = array_merge($currentUser_privilege,$codeGen_privilege);
}
if((!$USER->privilege->security) && (!$USER->privilege->admin911) && (!$USER->privilege->code_generator)){
    $currentUser_privilege = array_merge($currentUser_privilege,$catchall_privilege);
}

//get default notification types
$default_notification = "";
$default_email = "";
$default_text = "";

$defaultNotificationQuery = select_default_notification_types_with_plan($USER->emma_plan_id);
if($defaultNotificationQuery){
    $defaultNotificationResult = $defaultNotificationQuery->fetch_assoc();
    if($defaultNotificationResult["notificationType_appNotification"] == 1){
        $default_notification = "checked";
    }
    if($defaultNotificationResult["notificationType_email"] == 1){
        $default_email = "checked";
    }
    if($defaultNotificationResult["notificationType_SMStext"] == 1){
        $default_text = "checked";
    }
}




if (isset($_GET['content'])) {
    $content = $fvmdb->real_escape_string($_GET['content']);
    //Prevent certain account levels from accessing blocked material.
    if(!$USER->privilege->admin)
    {
        //If $content is not found in the prebuilt array with acceptable pages for visiting based
        //on privilege.
        if(!in_array($content,$currentUser_privilege))
        {

            //User is trying to get to a page they can't access.
            if($USER->privilege->security)
            {
                //security isn't supposed to see the map, so we take them to dashboard.
                $content = $currentUser_privilege[1];
            }
            else if($USER->privilege->admin911)
            {
                $content = $currentUser_privilege[1];
            }
            else{
                $content = 'user_home';
            }
        }
    }
    else{
        //Only page admins shouldn't see is the map page with the button, so we redirect to home instead.
        if($content == 'user_home')
        {
            $content = 'home';
        }
    }


//$content is not set.
} else {
    if ($USER->privilege->admin) {
        $content = 'home';
    } else if ($USER->privilege->security) {
        $content = $currentUser_privilege[1];
    }elseif($USER->privilege->admin911){
        $content = $currentUser_privilege[1];
    }
    else{
        if(empty($currentUser_privilege[1])) {
            $content = 'user_home';
        }
        else{
            $content = $currentUser_privilege[1];
        }
    }
}
?>
<html class="no-js" lang="en">
<head>
    <title>EMMA | <?php echo str_replace('_', ' ',
            ucwords(str_replace('sos', 'SOS', $content))); ?></title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/button-badge.css"/>
    <link rel="stylesheet" href="css/<?php echo $content; ?>.css"/>
    <link rel="manifest" href="manifest.json">
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <link rel="stylesheet" href="css/drop_down_menu.css"/>

<!--    <meta http-equiv="refresh" content="120">-->
</head>
<body>
<input hidden id="user_latitude" value=""/>
<input hidden id="user_longitude" value=""/>
<input type="hidden" id="nearestAddress" value=""/>
<?php include('include/top-bar.php'); ?>
<div class="app-dashboard shrink-large">
    <div class="app-dashboard-body off-canvas-wrapper">
        <?php include('include/side-bar.php'); ?>
        <div class="app-dashboard-body-content off-canvas-content"
             data-off-canvas-content>
            <div class="content-container-div">
                <div class="content-body-div">
                    <?php include('content/' . $content . '.php');?>
                </div>

                <div class="content-footer-div">
                    <div class="text-center">
                        <p>Copyright Think Safe, Inc. 2017 Patent Pending</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="thinking-modal" class="reveal callout text-center tiny" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4 style="color:#e5e5e5;">Working...</h4>
    <img style="width:2rem;height:2rem;" src="img/ajax_loader_blue.gif"/>
</div>
<div id="success-modal" class="reveal callout success text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <a onclick="location.reload();" class="button success">OK</a>
</div>
<div id="success-modal-mass" class="reveal callout success text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <div id="mass-text-error-header" hidden>The following numbers could not be reached: </div>
    <div id="mass-text-error"></div>
    <a onclick="location.reload();" class="button success">OK</a>
</div>
<div id="fail-modal" class="reveal callout alert text-center tiny" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4 style="color:darkred">Failed...</h4>
    <div id="error-list" class="text-left"></div>
    <a class="button" style="background-color: darkred" data-close>OK</a>
</div>
<div id="emma-sos-invalid-pin-modal" class="reveal callout alert text-center tiny" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4 style="color:darkred">Invalid Pin: Alert Sent</h4>
    <div id="error-list" class="text-left"></div>
    <a class="button" style="background-color: darkred" data-close>OK</a>
</div>
<div id="emma-sos-success-pin-modal" class="reveal callout success text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Alert Successfully Cancelled</h4>
    <a data-close="" class="button success">OK</a>
</div>
<div id="pin-error-modal" class="reveal callout alert text-center tiny" data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4 style="color:darkred">Failed...</h4>
    <div id="pin-error-list" class="text-left"></div>
    <a class="button" style="background-color: darkred" data-close>OK</a>
</div>
<div id="event-success-modal" class="reveal callout success text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Success</h4>
    <a data-close class="button success">OK</a>
</div>
<div id="log-out-modal" class="reveal callout alert text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Are You Sure?</h4>
    <form type="post" action="logout.php">
        <div class="row">
            <div class="large-12 columns button-group expanded">
                <a data-close class="button hollow alert">Cancel</a>
                <button type="submit" class="button alert">OK</button>
            </div>
        </div>
    </form>
</div>
<div id="select-parent-modal" class="reveal callout text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Select Parent/Guardian</h4>
    <form type="post" id="select-parent-form">
        <div class="row">
            <div class="large-12 columns">
                <label>Parent/Guardian</label>
                <select name="parent">
                    <option value="">-Select-</option>
                    <?php
                    if($USER->emma_plan_id != '') {
                        $parents = select_parent_users_with_planID($USER->emma_plan_id);
                        while ($parent = $parents->fetch_assoc()) {
                            echo '<option value="' . $parent['id'] . '">' . $parent['firstname'] . ' ' . $parent['lastname'] . '</option>';
                        }
                    }
                    ?>
                </select>
                <input type="hidden" id="student-id" name="student">
            </div>
        </div>
        <div class="row">
            <div class="large-12 columns">
                <span style="margin: 0 auto;">- OR -</span>
            </div>
        </div>
        <div class="row" style="margin-top: 5%">
            <div class="large-12 columns">
                <a class="button expanded" data-open="create-user-modal">Create New User</a>
            </div>
        </div>
        <div class="row">
            <div class="large-12 columns button-group expanded">
                <a data-close class="button alert">Cancel</a>
                <input type="submit" class="button" value="Submit">
            </div>
        </div>
    </form>
</div>
<div id="create-user-modal" class="reveal callout text-center large"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Create User</h4>
    <form type="post" id="create-user-form">
        <div class="row expanded align-Left">
            <div class="large-6 medium-12 column">
                <label>Username/Email
                    <input type="Text" name="username" required/>
                </label>
            </div>
        </div>
        <div class="row expanded">
            <div class="large-4 medium-12 small-12 column">
                <label>First Name
                    <input type="Text" name="firstname" required/>
                </label>
            </div>

            <div class="large-4 medium-12 small-12 column">
                <label>Middle Name
                    <input type="Text" name="middlename"/>
                </label>
            </div>

            <div class="large-4 medium-12 small-12 column">
                <label>Last Name
                    <input type="Text" name="lastname" required/>
                </label>
            </div>
        </div>
        <div class="row expanded">
            <div class="large-4 medium-12 column">
                <label>Landline
                    <input type="Text" name="landline" required/>
                </label>
            </div>
            <div class="large-4 medium-12 column">
                <label>Mobile
                    <input type="Text" name="phone" required/>
                </label>
            </div>

            <div class="large-4 medium-12 column">
                <label>Title
                    <input type="Text" name="title"/>
                </label>
            </div>
        </div>
        <div class="row expanded">
            <div class="large-4 medium-12 small-12 column">
                <label>Address
                    <input type="Text" name="address" required/>
                </label>
            </div>

            <div class="large-4 medium-12 small-12 column">
                <label>City
                    <input type="Text" name="city" required/>
                </label>
            </div>

            <div class="large-3 medium-12 small-12 column">
                <label>State
                    <input type="Text" name="state" required/>
                </label>
            </div>
            <div class="large-1 medium-12 small-12 column">
                <label>Zip
                    <input type="Text" name="zip" required/>
                </label>
            </div>
        </div>
        <div class="text-center">
            <h3 class="lead">Assign to Groups</h3>
        </div>
        <div class="row large-12 columns">
            <div class="large-3 medium-6 small-12 columns">
                <div class="row">
                    <div class="small-4 column">
                        <div class="switch tiny">
                            <input class="switch-input all-select" id="all-select" type="checkbox" >
                            <label class="switch-paddle" for="all-select">
                                <span class="show-for-sr">All</span>
                            </label>
                        </div>
                    </div>
                    <div class="small-8 column">
                        <label class="text-left">All</label>
                    </div>
                </div>
            </div>
            <?php
            $allgroups = select_groups_with_planID($USER->emma_plan_id);
            while ($allgroup = $allgroups->fetch_assoc()) {
                echo '
                                <div class="large-3 medium-6 small-12 columns">
                                    <div class="row"> 
                                        <div class="small-4 column">
                                            <div class="switch tiny">
                                                <input class="switch-input group-toggle" name="group-ids[]" value="' .
                    $allgroup['emma_group_id'] . '" id="group-id-' .
                    $allgroup['emma_group_id'] . '" type="checkbox" >
                                                <label class="switch-paddle" for="group-id-' .
                    $allgroup['emma_group_id'] . '">
                                                    <span class="show-for-sr">' .
                    $allgroup['name'] . '</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="small-8">
                                          <label class="text-left">' .
                    $allgroup['name'] . '</label>
                                        </div>
                                    </div>
                                </div>';

            }
            ?>
        </div>
        <div class="row">
            <div class="large-3 columns button-group expanded" style="margin: 0 auto;">
                <a data-close class="button alert">Cancel</a>
                <input type="submit" class="button" value="Submit">
            </div>
        </div>
    </form>
</div>
<div id="view-sos-modal" class="reveal callout text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>EMMA SOS</h4>
    <?php
    list_active_sos($USER->emma_plan_id);
    ?>
    <br>
    <a data-close class="button">Back</a>
</div>
<div id="view-help-modal" class="reveal callout text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Needs Help</h4>
    <?php
    list_active_help($USER->emma_plan_id);
    ?>
    <br>
    <a data-close class="button">Back</a>
</div>
<div id="view-security-modal" class="reveal callout text-center tiny"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Securities</h4>
    <?php
    list_active_securities($USER->emma_plan_id);
    ?>
    <br>
    <a data-close class="button">Back</a>
</div>
<div id="confirm-password-modal" class="reveal callout text-center small"
     data-reveal
     data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Confirm Current Password</h4>
    <form id="confirm-password-form" method="post">
        <input type="hidden" name="id" value="<?php echo $USER->id;?>">
        <div class="large-12 columns">
            <div class="row">
                <div class="large-6 medium-12 small-12 columns text-left">
                    <label>Username/Email</label>
                    <input type="text" readonly value="<?php echo $USER->username;?>">
                </div>
                <div class="large-6 medium-12 small-12 columns text-left">
                    <label>Current Password</label>
                    <input type="password" name="password">
                </div>
            </div>
            <div class="row text-center">
                <div class="large-12 columns button-group">
                    <div class="button-group" style="margin: 0 auto;">
                        <button class="button alert" onclick="window.location.reload();">Cancel</button>
                        <input type="submit" class="button" value="Submit">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<?php
$sitesString = '';
$sites = select_sites_with_planID($USER->emma_plan_id);
while ($site = $sites->fetch_assoc()) {
    $sitesString .= '<option value="' . $site['emma_site_id'] . '">' .
        $site['emma_site_name'] . '</option>';
}
$broadcastTypesString = '';
$broadcastTypes = select_broadcastTypes();
//$broadcastTypes = $fvmdb->query("
//    select *
//    from emma_broadcast_types
//    where `create` = 1
//    order by name
//  ");
while ($broadcastType = $broadcastTypes->fetch_assoc()) {
    $broadcastTypesString .= "<option value='" . $broadcastType['emma_script_group_id'] . "'>" . $broadcastType['name'] . "</option>";
}
$broadcastScriptsString = '';
$broadcastScripts = select_scripts_with_planID($USER->emma_plan_id);
while ($broadcastScript = $broadcastScripts->fetch_assoc()) {
    $broadcastScriptsString .= "<option value='" .
        $broadcastScript['emma_script_id'] .
        "' data-text='" . $broadcastScript['text'] . "'>" .
        $broadcastScript['name'] . "</option>";
}
if($USER->privilege->admin) {
    $groups = select_groups_with_planID($USER->emma_plan_id);
}else{
    $groups = select_groupPrivileges_with_userID_and_planID($USER->id, $USER->emma_plan_id);
}
    $groupsnum = $groups->num_rows;
$planEventTypes = select_eventTypes_with_planID($USER->emma_plan_id);
if($planEventTypes->num_rows > 0) {
    while ($planEventType = $planEventTypes->fetch_assoc()) {
        $groupsString = '';
        $scriptGroupsString = '';
        $groups->data_seek(0);
        while ($group = $groups->fetch_assoc()) {
            $groupsString .= '
    <div class="large-4 medium-12 small-12 columns">
    <div class="row">
      <div class="small-3 column">
        <div class="switch tiny">
          <input class="switch-input group-select" 
            id="' . $planEventType['badge'] . '-group-' .
                $group['emma_group_id'] . '" 
            type="checkbox" 
            name="groups[' . $group['emma_group_id'] . ']" 
            value="' . $group['emma_group_id'] . '">
          <label class="switch-paddle" for="' . $planEventType['badge'] .
                '-group-' . $group['emma_group_id'] . '">
            <span class="show-for-sr">' . $group['name'] . '</span>
            <span class="switch-active" aria-hidden="true">Yes</span>
            <span class="switch-inactive" aria-hidden="true">No</span>
          </label>
        </div>
      </div>
      <div class="small-9 column">
        <label class="text-left">' . $group['name'] . '</label>
      </div>
    </div>
    </div>
  ';
            $scriptGroupsString .= "<option value='" . $group['emma_group_id'] . "'>" . $group['name'] . "</option>";
        }
        $subTypesString = '';
        $subTypes = select_subEventTypes_with_emergencyTypeID($planEventType['emergency_type_id']);
//    $subTypes = $fvmdb->query("
//    SELECT *
//    FROM emergency_sub_types
//    WHERE emergency_type_id = '" . $planEventType['emergency_type_id'] . "'
//    ORDER BY name
//  ");
        while ($subType = $subTypes->fetch_assoc()) {
            $subTypesString .= '<option value="' . $subType['emergency_sub_type_id'] .
                '">' . $subType['name'] . '</option>';
        }
        echo '
  <div id="create-' . $planEventType['badge'] . '-modal" class="reveal callout text-center small" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
    <h4>' . $planEventType['name'] . '</h4>
    <hr>';

        if ($USER->privilege->admin || $USER->privilege->security) {
            echo '<form class="create-event-form text-left">';
        } else {
            echo '<div class="create-event-form text-left">';
        }
        //query for notification default types
//        $default_notification = "";
//        $default_email = "";
//        $default_text = "";
//
//        $defaultNotificationQuery = select_default_notification_types_with_plan($USER->emma_plan_id);
//        if($defaultNotificationQuery){
//            $defaultNotificationResult = $defaultNotificationQuery->fetch_assoc();
//            if($defaultNotificationResult["notficationType_appNotification"] == 1){
//                $default_notification = "checked";
//            }
//            if($defaultNotificationResult["notficationType_email"] == 1){
//                $default_email = "checked";
//            }
//            if($defaultNotificationResult["notficationType_SMStext"] == 1){
//                $default_text = "checked";
//            }
//        }
        echo '
        <div class="row columns">
            <fieldset class="fieldset text-left" style="width:100%;">
                <legend>Alert Types <span style="color: red;">*</span></legend>
                <input id="alert-notification" name="alert-notification" type="checkbox" '.$default_notification.' readonly><label
                        for="alert-notification">Notification</label>
                <input id="alert-email" name="alert-email" type="checkbox" '.$default_email. ' ';
        if (!$USER->privilege->plan_emails) {
            echo 'disabled';
        }
        echo '><label
                    for="alert-email">Email</label>
                <input id="alert-text" name="alert-text" type="checkbox" '.$default_text. ' ';
        if (!$USER->privilege->plan_texts) {
            echo 'disabled';
        }
        echo '><label
                    for="alert-text">Text</label>
            </fieldset>
        </div>    
      <input type="hidden" name="user" value="' . $USER->id . '"/>
      <!--Removed PIN input, as it is recorded elsewhere and is not the default value of $USER->pin anymore.-->
      <input type="hidden" name="drill" value="0" />
      <input type="hidden" name="type" value="' .
            $planEventType['emergency_type_id'] . '" />      
      ';
        if ($USER->privilege->admin) {
            echo '<input type="hidden" name="pin" value="' . $USER->emma_pin . '">';
        }
        echo '
      <div class="row">
        <div class="large-12 column">';
        $events = select_activeEmergencies_with_planID($USER->emma_plan_id);
        if (($USER->privilege->security || $USER->privilege->admin) && $events->num_rows <= 0) {
            echo '
          <label style="margin-bottom:1rem;font-size:1rem;">
            <div class="switch small" style="display:inline-block;float:left;margin-right:1rem;margin-bottom:0;">
              <input class="switch-input" id="drill-' .
                $planEventType['badge'] . '" type="checkbox" name="drill">
              <label class="switch-paddle" for="drill-' .
                $planEventType['badge'] . '">
                <span class="show-for-sr">Run Drill?</span>
                <span class="switch-active" aria-hidden="true">Yes</span>
                <span class="switch-inactive" aria-hidden="true">No</span>
              </label>
            </div>
            Run Drill?
          </label>';
        }
        echo '
          <label>Sub-Type <span style="color: red">*</span>
            <select class="sub-type" name="sub-type">' . $subTypesString . '</select>
          </label>
          <label>Site <span style="color: red">*</span>
            <select class="site" name="site">' . $sitesString . '</select>
          </label>
          <label>Emergency Details <span style="color: red">*</span>
            <textarea required class="description" name="description"
              style="min-width:100%;height:5rem;" maxlength="100"></textarea>
          </label>
          ';
        if ($USER->privilege->admin || $USER->privilege->security) {
            echo '
          <div><label>Broadcast Type
          <select class="select-create-event-broadcast-type">
          <option value="">-Select-</option>
                ' . $broadcastTypesString . '
            </select>
            </label>
            
            <label  class="select-create-event-script-group-container">Group Type
          <select class="select-create-event-script-group">
          <option value="">-Select-</option>
                ' . $scriptGroupsString . '  
            </select>
            </label>
            <label class="select-create-event-script-container">Script
            <select class="select-create-event-script" id="select-create-event-script">
              <option value="" data-text="">-Select-</option>
              
            </select>
            </label>
            </div>
            ';
        }
        echo '
            <label>Comments <span style="color: red">*</span>
            <textarea required name="comments" class="create-event-description"
              style="min-width:100%;height:5rem;" maxlength="100"></textarea>
          </label>
          <label>Groups <span style="color: red">*</span></label>
          <div class="row">';
        if ($groupsnum > 1) {
            echo '
          <div class="large-4 medium-12 small-12 columns">
                <div class="row">
                    <div class="small-3 column">
                        <div class="switch tiny">
                            <input class="switch-input all-select" id="all-select" type="checkbox" >
                            <label class="switch-paddle" for="all-select">
                                <span class="show-for-sr">All</span>
                            </label>
                        </div>
                    </div>
                    <div class="small-9 column">
                        <label class="text-left">All</label>
                    </div>
                </div>
            </div>';
        }
        echo $groupsString . '</div>
        </div>
      </div>
      <div class="row">
        <div class="large-12 columns button-group">
            <div class="button-group" style="margin: 0 auto;">
              <a href="." class="button alert">Cancel</a>
              ';
        if ($USER->privilege->security || $USER->privilege->admin) {
            echo '<input type="submit" class="button large-6 large-centered columns" value="Submit"/>';
        } else {
            echo '<button onclick="pull_up_pin(this);" id="creating-event" class="button large-6 large-centered columns">Submit</button>';
        }
        echo '
            </div>`
        </div>';
        if ($USER->privilege->admin || $USER->privilege->security) {
            echo '</form>';
        } else {
            echo '</div>';
        }
        echo '      
    <button class="close-button" onclick="location.reload();" aria-label="Close reveal"
      type="button">
      <span aria-hidden="true">×</span>
    </button>
    <div class="event-error"></div>
    </div>
    </div>';
    }
}
?>
<div id="create-event-modal" class="reveal callout text-center small"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Create Event</h4>
    <hr>
    <div class="row">
        <?php
        $groupEvents = array();
        $planEventTypes = select_dashboard_planEventTypes($USER->id, $USER->emma_plan_id);
//            $fvmdb->query("
//          select et.*
//          from emma_user_groups ug
//          join emma_groups g on ug.emma_group_id = g.emma_group_id
//          join emma_group_events pet on g.emma_group_id = pet.emma_group_id
//          join emergency_types et on pet.event_id = et.emergency_type_id
//          where ug.user_id = '" . $USER->id . "'
//          and pet.active = 1
//          GROUP BY et.emergency_type_id
//        ");
        if($planEventTypes->num_rows > 0) {
            while ($planEventType = $planEventTypes->fetch_assoc()) {
                $groupEvents[] = $planEventType['emergency_type_id'];
                if ($planEventType['emergency_type_id'] > 0) {
                    echo '
                  <div class="column large-4 medium-12 small-12">
                    <div class="card" data-open="create-' . $planEventType['badge'] . '-modal" ';
                    if ($planEventType['disabled']) {
                        echo 'style="pointer-events:none; cursor: not-allowed;"';
                    }
                    echo '>
                      <img src="img/' . $planEventType['img_filename'] . '">
                      <div class="card-section">
                        <h4>' . $planEventType['name'] . '</h4>
                      </div>
                    </div>
                  </div>
                ';
                }
            }

            $planInfos = select_dashboard_planInfos($USER->emma_plan_id);
//        $fvmdb->query("
//            SELECT e.*
//            FROM emma_plans e
//            WHERE e.emma_plan_id = '". $USER->emma_plan_id ."'
//            ORDER BY name
//        ");
            $planInfo = $planInfos->fetch_assoc();
            if ($planInfo['securities'] == 1 && in_array('0', $groupEvents)) {
                echo '
      <div class="column large-4 medium-12 small-12">
        <div class="card" data-open="create-security-modal" ';
                if ($planEventType['disabled']) {
                    echo 'style="pointer-events:none; cursor: not-allowed;"';
                }
                echo '>
          <img src="img/icon_security.png">
          <div class="card-section">
            <h4>Security</h4>
          </div>
        </div>
      </div>
      ';
            }
            if ($planInfo['mass_communication'] == 1 && in_array('-2', $groupEvents)) {
                echo '
      <div class="column large-4 medium-12 small-12">
        <div class="card" data-open="create-mass-communication-modal"';
                if ($planEventType['disabled']) {
                    echo 'style="pointer-events:none; cursor: not-allowed;"';
                }
                echo '>
          <img src="img/icon_mass.png">
          <div class="card-section">
            <h4>Mass Notification</h4>
          </div>
        </div>
      </div>
      ';
            }
            if ($planInfo['geofencing'] == 1 && in_array('-4', $groupEvents)) {
                echo '
      <div class="column large-4 medium-12 small-12">
        <div class="card" onclick="location.href=\'dashboard.php?content=geofences\';" ';
                if ($planEventType['disabled']) {
                    echo 'style="pointer-events:none; cursor: not-allowed;"';
                }
                echo '>
          <img src="img/icon_geofence.png">
          <div class="card-section">
            <h4>Geofence Notification</h4>
          </div>
        </div>
      </div>
      ';
            }
            if (in_array('-1', $groupEvents)) {
                //Displaying this for all users.
                echo '
      <div class="column large-4 medium-12 small-12">
        <div class="card" id="create-emma-sos-button" ';
                if ($planEventType['disabled']) {
                    echo 'style="pointer-events:none; cursor: not-allowed;"';
                }
                echo '>
          <img src="img/icon_emma_sos.png">
          <div class="card-section">
            <h4>EMMA SOS</h4>
          </div>
        </div>
      </div>
      ';
            }
            if (in_array('-5', $groupEvents)) {
                //Displaying this for all users.
                echo '
      <div class="column large-4 medium-12 small-12">
        <div class="card" data-open="create-lockdown-modal" ';
                if ($planEventType['disabled']) {
                    echo 'style="pointer-events:none; cursor: not-allowed;"';
                }
                echo '>
          <img src="img/icon_emma_sos.png">
          <div class="card-section">
            <h4>LOCKDOWN!</h4>
          </div> 
        </div>
      </div>
      ';
            }
            if (in_array('-6', $groupEvents)) {
                //Displaying this for all users.
                echo '
      <div class="column large-4 medium-12 small-12">
        <div class="card" data-open="create-lockdown-drill-modal" ';
                if ($planEventType['disabled']) {
                    echo 'style="pointer-events:none; cursor: not-allowed;"';
                }
                echo '>
          <img src="img/EMMA_LOCKDOWN_50_px_greyscale.png">
          <div class="card-section">
            <h4>LOCKDOWN! <span style="color: red">(DRILL)</span></h4>
          </div>
        </div>
      </div>
      ';
            }
            if (in_array('-8', $groupEvents)) {
                //Displaying this for all users.
                echo '
      <div class="column large-4 medium-12 small-12">
        <div class="card" data-open="create-modified-lockdown-modal" ';
                if ($planEventType['disabled']) {
                    echo 'style="pointer-events:none; cursor: not-allowed;"';
                }
                echo '>
          <img src="img/emma_lockdown_yellow.png">
          <div class="card-section">
            <h4>Modified LOCKDOWN!</span></h4>
          </div>
        </div>
      </div>
      ';
            }
            if (in_array('-7', $groupEvents)) {
                //Displaying this for all users.
                echo '
      <div class="column large-4 medium-12 small-12">
        <div class="card" data-open="create-anonymous-report-modal" ';
                if ($planEventType['disabled']) {
                    echo 'style="pointer-events:none; cursor: not-allowed;"';
                }
                echo '>
          <img src="img/icon_anonymous_report.png">
          <div class="card-section">
            <h4>Anonymous Report</h4>
          </div>
        </div>
      </div>
      ';
            }
            if (in_array('-9', $groupEvents)) {
                //Displaying this for all users.
                echo '
      <div class="column large-4 medium-12 small-12">
        <div class="card" data-open="create-anonymous-report-moss-modal" ';
                if ($planEventType['disabled']) {
                    echo 'style="pointer-events:none; cursor: not-allowed;"';
                }
                echo '>
          <img src="img/icon_moss.png">
          <div class="card-section">
            <h4>Anonymous Report</h4>
          </div>
        </div>
      </div>
      ';
            }
        }
        ?>
    </div>
    <button class="close-button" data-close="" aria-label="Close reveal"
            type="button">
        <span aria-hidden="true">×</span>
    </button>
</div>
<div id="emergency_alert" style="display:none">
    <div class="large-12 columns">
        <div class="row">
            <div class="large-12 columns text-center">
                <div id="emergency_popup_icon"></div>
            </div>
        </div>
        <div class="row">
            <div class="text-center large-12 columns">
                <h1 id="emergency_popup_header"></h1>
            </div>
        </div>
        <div class="row">
            <div class="text-center large-12 columns">
                <h3 id="emergency_popup_details"></h3>
            </div>
        </div>
        <div class="row">
            <div class="large-6 columns text-center">
                <a id="emergency_popup_link" href="#" class="button large-12 columns">Go To Event</a>
            </div>
            <div class="large-6 columns text-center">
                <div id="close_emergency_alert" class="button large-12 columns">Close Alert</div>
            </div>
        </div>
    </div>
</div>


<div id="enter-pin-modal" data-options="closeOnClick:false;closeOnEsc:false;" class="reveal callout text-center small"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Enter Pin</h4>
    <hr>
    <div class="large-12 columns">
        <div class="row">
            <h3 class="large-4 large-offset-4" style="color: #bbb; text-align: center;">Enter your 4-digit PIN</h3>
        </div>


        <div class="row">
            <div class="large-4 large-offset-4 columns">
                <div>
                    <span id="dot1" class="dot" style="height: 25px;width: 25px;background-color: #bbb;border-radius: 50%;display: inline-block;" data-value='0' value="0"></span>
                    <span id="dot2" class="dot" style="height: 25px;width: 25px;background-color: #bbb;border-radius: 50%;display: inline-block;"></span>
                    <span id="dot3" class="dot" style="height: 25px;width: 25px;background-color: #bbb;border-radius: 50%;display: inline-block;"></span>
                    <span id="dot4" class="dot" style="height: 25px;width: 25px;background-color: #bbb;border-radius: 50%;display: inline-block;"></span>
                    <input id="pin-password-holder" style="display: none;" type="text" name="pin" pattern="[0-9]{4}" maxlength="4" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="large-4 large-offset-4 columns">
                <button id="pin-pad-button" onclick="pinPress(this);" style="width: 25%;color: #0078c1;padding: 10px;" type="button">1</button>
                <button id="pin-pad-button" onclick="pinPress(this);" style="width: 25%;color: #0078c1;padding: 10px;" type="button">2</button>
                <button id="pin-pad-button" onclick="pinPress(this);" style="width: 25%;color: #0078c1;padding: 10px;" type="button">3</button>
            </div>
        </div>
        <div class="row">
            <div class="large-4 large-offset-4 columns">
                <button id="pin-pad-button" onclick="pinPress(this);" style="width: 25%;color: #0078c1;padding: 10px;" type="button">4</button>
                <button id="pin-pad-button" onclick="pinPress(this);" style="width: 25%;color: #0078c1;padding: 10px;" type="button">5</button>
                <button id="pin-pad-button" onclick="pinPress(this);" style="width: 25%;color: #0078c1;padding: 10px;" type="button">6</button>
            </div>
        </div>
        <div class="row">
            <div class="large-4 large-offset-4 columns">
                <button id="pin-pad-button" onclick="pinPress(this);" style="width: 25%;color: #0078c1;padding: 10px;" type="button">7</button>
                <button id="pin-pad-button" onclick="pinPress(this);" style="width: 25%;color: #0078c1;padding: 10px;" type="button">8</button>
                <button id="pin-pad-button" onclick="pinPress(this);" style="width: 25%;color: #0078c1;padding: 10px;" type="button">9</button>
            </div>
        </div>
        <div class="row">
            <div class="large-4 large-offset-4 columns">
                <button id="pin-pad-button" onclick="pinPress(this);" style="width: 25%;color: #0078c1;padding: 10px;" type="button"> </button>
                <button id="pin-pad-button" onclick="pinPress(this);" style="width: 25%;color: #0078c1;padding: 10px;" type="button">0</button>
                <button id="pin-pad-button" onclick="pinPress(this);" class="button radius" style="width: 25%;background-color: red" type="button"><i style="color: white;" class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="row">
            <div style="text-align: center;">
                <hr>
                <h4>Attention!</h4>
                <hr>
                <p>If you do not remember your PIN typing 0000 will send the alert to your site Administrator for review instead of immediate alert distribution.</p>
                <p>Be ready to verify your emergency with your Site Administrator!</p>
            </div>

        </div>
        <button id="close-pin-button" class="close-button" data-close="" aria-label="Close reveal"
                type="button">
            <span id="pin-modal-close-x" aria-hidden="true">×</span>
        </button>
    </div>
    <script>
        let click_count = 0;
        let reached_max = false;
        let password_field = document.getElementById('pin-password-holder');
        function pinPress(button)
        {
            if(button.classList.contains('button') && click_count > 0)
            {
                password_field.value = password_field.value.substr(0,password_field.value.length-1);
                let filled_circle = document.getElementById('dot'+click_count);
                filled_circle.style.backgroundColor = "#bbb";
                click_count--;
            }
            else if(button.innerText !== 'X' && click_count < 4 && reached_max !== true && !button.classList.contains('button')) {
                click_count++;
                password_field.value += button.innerText;
                let circle = document.getElementById('dot'+click_count); //Gets first empty circle to animate.
                circle.style.backgroundColor = "#0078C1";
                if (password_field.value.length >= 4) {
                    //submit the pin, dots get reset in the function create_event. Which is located in main.js.
                    click_count = 0;
                    reached_max = true;
                    setTimeout(create_event, 500); //small delay before we reset the dots color for beauty sake.
                }
            }
        }
    </script>
</div>

<!--Hidden_div and hidden_form are used on the content=user_home to prevent auto submission of forms when
    pulling up the pin-model. Do not delete.-->
<div id="hidden_div" style="display: none">
    <div id="hidden_inner_div">

    </div>
</div>
<form id="hidden_form">
</form>

<div id="create-security-modal" class="reveal callout text-center small"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Create Security</h4>
    <hr>
    <form id="create-security-form" class="text-left" action="#" method="post">
        <input type="hidden" name="user-id" value="<?php echo $USER->id; ?>"/>
        <input type="hidden" name="plan-id" value="<?php echo $USER->emma_plan_id; ?>"/>
        <input type="hidden" name="latitude" value=""/>
        <input type="hidden" name="longitude" value=""/>
        <div class="row columns">
            <fieldset class="fieldset text-left" style="width:100%;">
                <legend>Alert Types <span style="color: red;">*</span></legend>
                <input id="alert-notification" name="alert-notification" type="checkbox" <?php echo $default_notification ?>><label
                        for="alert-notification">Notification</label>
                <input id="alert-email" name="alert-email" type="checkbox" <?php echo $default_email ?> <?php if(!$USER->privilege->plan_emails){ echo 'disabled';} ?>><label
                        for="alert-email">Email</label>
                <input id="alert-text" name="alert-text" type="checkbox" <?php echo $default_text ?> <?php if(!$USER->privilege->plan_texts){ echo 'disabled';} ?>><label
                        for="alert-text">Text</label>
            </fieldset>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <label>Site <span style="color: red">*</span>
                    <select name="site-id" class="expanded">
                        <?php
                        $sites = select_sites_with_planID($USER->emma_plan_id);
                        while ($site = $sites->fetch_assoc()) {
                            echo '<option value="' . $site['emma_site_id'] .
                                '">' . $site['emma_site_name'] . '</option>';
                        }
                        ?>
                    </select>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <label>Security <span style="color: red">*</span>
                    <select name="type-id" class="expanded">
                        <?php
                        $securityTypes = select_securtityTypes_with_planID($USER->emma_plan_id);
                        while ($securityType = $securityTypes->fetch_assoc()) {
                            echo '<option value="' . $securityType['emma_security_type_id'] . '">' . $securityType['name'] . '</option>';
                        }
                        ?>
                    </select>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <label>Emergency Details <span style="color: red">*</span>
                    <textarea class="description" name="description"
                              style="min-width:100%;height:5rem;" maxlength="100"></textarea>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <label>Comments
                    <textarea name="comments"
                              style="min-width:100%;height:5rem;" maxlength="100"></textarea>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="large-12 columns button-group">
                <div class="button-group" style="margin: 0 auto;">
                    <a href="." class="button alert">Cancel</a>
                    <input type="submit" class="button" value="Submit"/>
                </div>
            </div>
        </div>
    </form>
    <button class="close-button" onclick="location.reload();" aria-label="Close reveal"
            type="button">
        <span aria-hidden="true">×</span>
    </button>
</div>

<div id="create-mass-communication-modal"
     class="reveal callout text-center small"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out">
    <h4>Create Mass Notification</h4>
    <hr>
    <form id="create-mass-communication-form" class="text-left" action="#"
          method="post">
        <input type="hidden" name="user-id" value="<?php echo $USER->id; ?>"/>
        <input type="hidden" name="plan-id"
               value="<?php echo $USER->emma_plan_id; ?>"/>
        <div class="row columns">
            <fieldset class="fieldset text-left" style="width:100%;">
                <legend>Alert Types <span style="color: red;">*</span></legend>
                <input id="alert-notification-mass" name="alert-notification" type="checkbox" <?php echo $default_notification ?>><label
                        for="alert-notification-mass">Notification</label>
                <input id="alert-email-mass" name="alert-email" type="checkbox" <?php echo $default_email ?> <?php if(!$USER->privilege->plan_emails){ echo 'disabled';} ?>><label
                        for="alert-email-mass">Email</label>
                <input id="alert-text-mass" name="alert-text" type="checkbox" <?php echo $default_text ?> <?php if(!$USER->privilege->plan_texts){ echo 'disabled';} ?>><label
                        for="alert-text-mass">Text</label>
            </fieldset>
        </div>

        <?php
        $plans = select_dashboard_plans($USER->id);
//            $fvmdb->query("
//            SELECT p.name, p.emma_plan_id
//            FROM emma_multi_plan m
//            LEFT JOIN emma_plans p ON m.plan_id = p.emma_plan_id
//            WHERE m.user_id = '". $USER->id ."'
//        ");
        $plancount = $plans->num_rows;

//        $planprefs = get_emma_plan($USER->emma_plan_id);
//        $planpref = $planprefs->fetch_assoc();

        echo'<div class="row columns">
                <label>Responses</label>
            </div>
            <div class="row text-left">
                <div class="large-4 columns">
                    <div class="row">
                        <div class="small-12 column">
                            <div class="switch tiny">
                                <input class="switch-input" name="mass-notification-responses" id="mass-notification-responses" type="checkbox">
                                <label class="switch-paddle" for="mass-notification-responses">
                                    <span class="show-for-sr"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">    
                <div class="large-4 columns" id="mass-notification-responses-amount-container" hidden>
                    <label>Response Time (Minutes)</label>
                    <input id="mass-notification-responses-amount" name="mass-notification-responses-amount" type="number" min="0" value="">
                </div>
            </div>';

        if($USER->privilege->admin && $plancount > 1){
            echo'<div class="row columns"><label>Plans <span style="color: red">*</span></label></div><div class="row">';
            $plans = select_dashboard_plans($USER->id);
//            $fvmdb->query("
//                SELECT p.name, p.emma_plan_id
//                FROM emma_multi_plan m
//                LEFT JOIN emma_plans p ON m.plan_id = p.emma_plan_id
//                WHERE m.user_id = '". $USER->id ."'
//            ");
            echo'
                    <div class="large-4 columns text-center">
                        <div class="row">
                            <div class="small-3 column">
                                <div class="switch tiny">
                                    <input class="switch-input" id="mass-all-plan-select" type="checkbox">
                                    <label class="switch-paddle" for="mass-all-plan-select">
                                        <span class="show-for-sr">All</span>
                                    </label>
                                </div>
                            </div>
                            <div class="small-9 column">
                                <label class="text-left">All</label>
                            </div>
                        </div>
                    </div>';
            while($plan = $plans->fetch_assoc()){
                if(in_array($plan['emma_plan_id'],$USER->emma_home_plans)){
                    echo'
                    <div class="large-4 columns text-center">
                        <div class="row">
                            <div class="small-3 column">
                                <div class="switch tiny">
                                    <input class="switch-input mass-plan-select" name="mass-notification-multiplan[]" id="'. $plan['name'] .'-select-multi" value="'. $plan['emma_plan_id'] .'" type="checkbox" checked>
                                    <label class="switch-paddle" for="'. $plan['name'] .'-select-multi">
                                        <span class="show-for-sr">'. $plan['name'] .'</span>
                                    </label>
                                </div>
                            </div>
                            <div class="small-9 column">
                                <label class="text-left">'. $plan['name'] .'</label>
                            </div>
                        </div>
                    </div>';
                }else{
                    echo'
                    <div class="large-4 columns text-center">
                        <div class="row">
                            <div class="small-3 column">
                                <div class="switch tiny">
                                    <input class="switch-input mass-plan-select" name="mass-notification-multiplan[]" id="'. $plan['name'] .'-select-multi" value="'. $plan['emma_plan_id'] .'" type="checkbox">
                                    <label class="switch-paddle" for="'. $plan['name'] .'-select-multi">
                                        <span class="show-for-sr">'. $plan['name'] .'</span>
                                    </label>
                                </div>
                            </div>
                            <div class="small-9 column">
                                <label class="text-left">'. $plan['name'] .'</label>
                            </div>
                        </div>
                    </div>';
                }
            }
            echo'</div>';
        }
        ?>
        <div class="row">
            <div class="small-12 columns">
                <?php if($USER->privilege->admin){ echo '<label>Groups <span style="color: red">*</span></label>';}?>
                <div class="row">
                    <div class="large-4 medium-12 small-12 columns">
                        <div class="row">
                            <div class="small-3 column">
                                <div class="switch tiny">
                                    <?php if($USER->privilege->admin) { echo '
                                        
                                    <input class="switch-input all-select" id = "notification-all-select" type = "checkbox" >
                                    <label class="switch-paddle" for="notification-all-select" >
                                        <span class="show-for-sr" > All</span >
                                    </label >';
                                    }
                                    else{

                                        //Multiplan
                                        $multi_plan = [];
                                        $multi_group = [];
//                                        $selectPlans = select_multiPlans_with_userID($USER->id);
                                        $selectPlans = select_dashboard_selectedPlans($USER->id);
//                                            $fvmdb->query("
//                                            SELECT mp.plan_id, p.name, eug.emma_group_id
//                                            FROM emma_plans p
//                                            LEFT JOIN emma_multi_plan mp ON p.emma_plan_id = mp.plan_id
//                                            LEFT JOIN emma_user_groups as eug on eug.user_id = '" . $USER->id . "'
//                                            WHERE mp.user_id = '" . $USER->id . "'
//                                            AND p.date_expired > NOW()
//                                        ");

                                        while($plan = $selectPlans->fetch_assoc())
                                        {
                                            array_push($multi_plan,$plan['plan_id']);
                                            array_push($multi_group,$plan['emma_group_id']);
                                        }
                                            //Get messaging authority groups
                                        $allgroups = select_groups_with_planArray($multi_plan);
                                        while ($allgroup = $allgroups->fetch_assoc()) {
                                            $group_privileges = select_groupsPrivileges_with_groupArray_and_groupID($multi_group, $allgroup['emma_group_id']);
                                            if ($group_privileges->num_rows > 0) {
                                                $privilege_list[] = $allgroup['emma_group_id'];
                                                echo '<input type="hidden" name="group['.$allgroup['emma_group_id'].']" value="'.$allgroup['emma_group_id'].'"/>';
                                            }
                                        }

                                    }
                                     ?>
                                </div>
                            </div>
                            <div class="small-9 column">
                                <?php if($USER->privilege->admin){echo '<label class="text-left">All</label>';}?>
                            </div>
                        </div>
                    </div><?php if($USER->privilege->admin){
                $groups = select_groups_with_planID($USER->emma_plan_id);
                $continue = true;
                while ($group = $groups->fetch_assoc()) {
                    echo '
                      <div class="large-4 medium-12 small-12 columns">
                      <div class="row">
                        <div class="small-3 column">
                          <div class="switch tiny">
                            <input class="switch-input group-select" id="c-group-' . $group['emma_group_id'] .
                        '" type="checkbox" name="group[' . $group['emma_group_id'] . ']" value="' . $group['emma_group_id'] . '">
                            <label class="switch-paddle" for="c-group-' . $group['emma_group_id'] . '">
                              <span class="show-for-sr">' . $group['name'] . '</span>
                            </label>
                          </div>
                        </div>
                        <div class="small-9 column">
                          <label class="text-left">' . $group['name'] . '</label>
                        </div>
                      </div>
                      </div>
                    ';
                }
               } ?>
                </div>
            </div>
        </div>
        <?php if($USER->privilege->admin || $USER->privilege->security){ echo '
        <div class="row">       
            <div class="large-12 columns">
                <label>Script
                    <select class="select-create-notification-script" id="select-create-notification-script">
                        <option value="" data-text="">-Select-</option>';
                        $notificationScripts = select_dashboard_notificationScripts($USER->emma_plan_id);
//                            $fvmdb->query("
//                            SELECT `name`, `text`
//                            FROM emma_mass_notification_scripts
//                            WHERE emma_plan_id = '". $USER->emma_plan_id ."'
//                            AND active = 1
//                        ");
                        while($notificationScript = $notificationScripts->fetch_assoc()){
                            echo'<option value="'. $notificationScript['text'] .'">'. $notificationScript['name'] .'</option>';
                        }
                    echo'</select>
                </label>
            </div>
        </div>
        ';}?>
        <div class="row">
            <div class="small-12 columns">
                <label>Notification
                    <textarea name="notification" class="create-notification-description"
                              style="min-width:100%;height:5rem;" maxlength="100"></textarea>
                </label>
            </div>
        </div>
        <div id="email-script-container" hidden>
            <div class="row">
                <div class="large-12 columns">
                    <label>Email Script
                        <select class="select-create-email-script" id="select-create-email-script">
                            <option value="" data-text="">-Select-</option>
                            <?php
                            $emailScripts = select_dashboard_emailScripts($USER->emma_plan_id);
                            while($emailScript = $emailScripts->fetch_assoc()){
                                echo'<option value="'. $emailScript['script'] .'">'. $emailScript['name'] .'</option>';
                            }
                            ?>
                        </select>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <label>Email Notification
                        <textarea name="email-notification" class="create-email-description"
                                  style="min-width:100%;height:5rem;" maxlength="1000"></textarea>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="large-12 columns button-group">
                <div class="button-group" style="margin: 0 auto;">
                    <a href="." class="button alert">Cancel</a>
                    <input type="submit" class="button" value="Create"/>
                </div>
            </div>
        </div>
    </form>
    <button class="close-button" onclick="location.reload();" aria-label="Close reveal"
            type="button">
        <span aria-hidden="true">×</span>
    </button>
</div>


<div id="create-emma-sos-modal"
     class="reveal callout text-center small"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out"
     data-options="closeOnClick:false;closeOnEsc:false;">
    <h4>Emma SOS</h4>
    <hr>
    <form id="create-emma-sos-form" class="text-left" action="#"
          method="post">
        <input type="hidden" name="user-id" value="<?php echo $USER->id; ?>"/>
        <input type="hidden" name="plan-id"
               value="<?php echo $USER->emma_plan_id; ?>"/>
        <input type="hidden" id="emma_sos_id"/>
        <div class="row">
            <div class="large-12 columns">
               <h2 style="text-align: center;">Press for help</h2>
                    <button style="width: 100%;outline:0;" type="button" id="click_emma_button">
                    <img style="border-radius: 25%;" id="emma_button" src="img/icon_emma_sos.png">
                    </button>
                    <style>
                        #emma_button{
                            display: block;
                            margin-left: auto;
                            margin-right: auto;
                            width: 50%;
                        }
                        #emma_button:hover{
                            cursor: pointer;
                        }
                    </style>
            </div>
        </div>
        <?php
            $plan_seconds = select_SOSDuration_with_planID($USER->emma_plan_id);
            $plan_second = $plan_seconds->fetch_assoc();
        ?>
        <input id="plan_seconds_length" hidden readonly value="<?php echo $plan_second['sos_duration'] ?>"/>
        <div class="row" id="show_alert_sent" style="display: none;">
            <br>
            <br>
            <div class="large-5 columns large-offset-5">
                <span style="text-align: center;">EMMA SOS Sent!</span>
                <br>
                <br>
                <input style="border-style: none;" id="emma_latitude_show" value=""/>
                <input style="border-style: none;" id="emma_longitude_show" value=""/>
            </div>
            <br>
            <br>
        </div>
        <div class="row">
            <div class="large-12 columns">
                <h3 style="text-align: center;" id="emma_countdown_timer"></h3>
            </div>
        </div>
        <div class="row">
            <br>
            <br>
            <div class="large-12 columns button-group">
                <div class="button-group" style="margin: 0 auto;">
                    <button type="button" id="emma_sos_cancel_button" class="button expanded alert">Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="create-lockdown-modal"
     class="reveal callout text-center small"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out"
     data-options="closeOnClick:false;closeOnEsc:false;">
    <h4>LOCKDOWN!</h4>
    <hr>
    <form id="create-lockdown-form" class="text-left" action="#"
          method="post">
        <input type="hidden" name="user" value="<?php echo $USER->id; ?>"/>
        <input type="hidden" name="plan"
               value="<?php echo $USER->emma_plan_id; ?>"/>
        <input type="hidden" id="lockdown_lat" name="lat"/>
        <input type="hidden" id="lockdown_lng" name="lng"/>
        <input type="hidden" name="drill" value="0"/>
        <div class="row" id="show_lockdown_create">
            <div class="large-12 columns">
               <h2 style="text-align: center;">Press for LOCKDOWN!</h2>
                    <button style="width: 100%;outline:0;" type="button">
                    <img style="border-radius: 25%;" id="click_emma_lockdown_button" src="img/icon_emma_sos.png">
                    </button>
                    <style>
                        #emma_button{
                            display: block;
                            margin-left: auto;
                            margin-right: auto;
                            width: 50%;
                        }
                        #emma_button:hover{
                            cursor: pointer;
                        }
                    </style>
            </div>
        </div>
        <div class="row text-center" id="show_lockdown_sent" style="display: none;margin-bottom: 1em;">
            <br>
            <br>
            <div class="large-6 columns" style="margin: 0 auto">
                <span style="text-align: center;">LOCKDOWN! Created</span>
                <img src="img/icon_emma_sos.png">
                <br>
                <br>
                <span>Latitude: </span>
                <span style="border-style: none;" id="emma_lockdown_latitude_show"></span><br />
                <span>Longitude: </span>
                <span style="border-style: none;" id="emma_lockdown_longitude_show"></span>
            </div>
            <br>
            <br>
        </div>
        <div class="row">
            <br>
            <br>
            <div class="large-12 columns button-group">
                <div class="button-group" style="margin: 0 auto;">
                    <button type="button" id="emma_lockdown_cancel_button" class="button expanded alert">Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="create-modified-lockdown-modal"
     class="reveal callout text-center small"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out"
     data-options="closeOnClick:false;closeOnEsc:false;">
    <h4>Modified LOCKDOWN!</h4>
    <hr>
    <form id="create-modified-lockdown-form" class="text-left" action="#"
          method="post">
        <input type="hidden" name="user" value="<?php echo $USER->id; ?>"/>
        <input type="hidden" name="plan"
               value="<?php echo $USER->emma_plan_id; ?>"/>
        <input type="hidden" id="modified_lockdown_lat" name="lat"/>
        <input type="hidden" id="modified_lockdown_lng" name="lng"/>
        <input type="hidden" name="drill" value="0"/>
        <div class="row" id="show_modified_lockdown_create">
            <div class="large-12 columns">
               <h2 style="text-align: center;">Press for Modified LOCKDOWN!</h2>
                    <button style="width: 100%;outline:0;" type="button">
                    <img style="border-radius: 25%;" id="click_emma_modified_lockdown_button" src="img/emma_lockdown_yellow.png">
                    </button>
                    <style>
                        #emma_button{
                            display: block;
                            margin-left: auto;
                            margin-right: auto;
                            width: 50%;
                        }
                        #emma_button:hover{
                            cursor: pointer;
                        }
                    </style>
            </div>
        </div>
        <div class="row text-center" id="show_modified_lockdown_sent" style="display: none;margin-bottom: 1em;">
            <br>
            <br>
            <div class="large-6 columns" style="margin: 0 auto">
                <span style="text-align: center;">Modified LOCKDOWN! Created</span>
                <img src="img/emma_lockdown_yellow.png">
                <br>
                <br>
                <span>Latitude: </span>
                <span style="border-style: none;" id="emma_modified_lockdown_latitude_show"></span><br />
                <span>Longitude: </span>
                <span style="border-style: none;" id="emma_modified_lockdown_longitude_show"></span>
                <br>
                <br>
                <span><b>911 CAD has not been automatically contacted. Call 911 if help is needed.</b></span>
            </div>
            <br>
            <br>
        </div>
        <div class="row">
            <br>
            <br>
            <div class="large-12 columns button-group">
                <div class="button-group" style="margin: 0 auto;">
                    <button type="button" id="emma_modified_lockdown_cancel_button" class="button expanded alert">Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="create-lockdown-drill-modal"
     class="reveal callout text-center small"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out"
     data-options="closeOnClick:false;closeOnEsc:false;">
    <h4>LOCKDOWN! <span style="color: red">(DRILL)</span></h4>
    <hr>
    <form id="create-lockdown-drill-form" class="text-left" action="#"
          method="post">
        <input type="hidden" name="user" value="<?php echo $USER->id; ?>"/>
        <input type="hidden" name="plan"
               value="<?php echo $USER->emma_plan_id; ?>"/>
        <input type="hidden" id="lockdown_drill_lat" name="lat"/>
        <input type="hidden" id="lockdown_drill_lng" name="lng"/>
        <input type="hidden" name="drill" value="1"/>
        <div class="row" id="show_lockdown_drill_create">
            <div class="large-12 columns">
               <h2 style="text-align: center;">Press for LOCKDOWN! <span style="color: red">(DRILL)</span> </h2>
                    <button style="width: 100%;outline:0;" type="button">
                    <img style="border-radius: 25%;" id="click_emma_lockdown_drill_button" src="img/EMMA_LOCKDOWN_50_px_greyscale.png">
                    </button>
                    <style>
                        #emma_button{
                            display: block;
                            margin-left: auto;
                            margin-right: auto;
                            width: 50%;
                        }
                        #emma_button:hover{
                            cursor: pointer;
                        }
                    </style>
            </div>
        </div>
        <div class="row text-center" id="show_lockdown_drill_sent" style="display: none; margin-bottom: 1em;">
            <br>
            <br>
            <div class="large-6 columns" style="margin: 0 auto">
                <p style="text-align: center;">LOCKDOWN! <span style="color: red;">(DRILL)</span> Created</p>
                <img src="img/EMMA_LOCKDOWN_50_px_greyscale.png">
                <br>
                <br>
                <span>Latitude: </span>
                <span style="border-style: none;" id="emma_lockdown_drill_latitude_show"></span><br />
                <span>Longitude: </span>
                <span style="border-style: none;" id="emma_lockdown_drill_longitude_show"></span>
            </div>
            <br>
            <br>
        </div>
        <div class="row">
            <br>
            <br>
            <div class="large-12 columns button-group">
                <div class="button-group" style="margin: 0 auto;">
                    <button type="button" id="emma_lockdown_drill_cancel_button" class="button expanded alert">Close</button>
                </div>
            </div>
        </div>
    </form>
</div>

<div id="create-anonymous-report-modal" class="reveal callout text-center medium" data-reveal data-animation-in="fade-in" data-animation-out="fade-out" data-options="closeOnClick:false;closeOnEsc:false;">
    <h4>Anonymous Report</h4>
    <hr>
    <form id="create-anonymous-report-form" class="text-center" action="#" method="post">
        <input type="hidden" name="user" value="<?php echo $USER->id; ?>"/>
        <input type="hidden" name="plan" value="<?php echo $USER->emma_plan_id; ?>"/>
        <div class="row">
            <div class="large-12 columns">
                <label>Description</label>
                <textarea name="description" maxlength="100"></textarea>
            </div>
        </div>
        <div class="large-12 columns button-group">
            <div class="button-group"  style="margin: 0 auto;">
                <a href="." class="button alert">Cancel</a>
                <input type="submit" class="button" value="Submit"/>
            </div>
        </div>
    </form>
</div>
<div id="create-anonymous-report-moss-modal" class="reveal callout text-center medium" data-reveal data-animation-in="fade-in" data-animation-out="fade-out" data-options="closeOnClick:false;closeOnEsc:false;">
    <h4>Anonymous Report</h4>
    <hr>
    <form id="create-anonymous-report-moss-form" class="text-center" action="#" method="post">
        <input type="hidden" name="user" value="<?php echo $USER->id; ?>"/>
        <input type="hidden" name="plan" value="<?php echo $USER->emma_plan_id; ?>"/>
        <div class="row">
            <div class="large-12 columns">
                <label>Description</label>
                <textarea name="description" maxlength="100"></textarea>
            </div>
        </div>
        <div class="large-12 columns button-group">
            <div class="button-group"  style="margin: 0 auto;">
                <a href="." class="button alert">Cancel</a>
                <input type="submit" class="button" value="Submit"/>
            </div>
        </div>
    </form>
</div>

<div id="sent-emma-sos-modal"
     class="reveal callout text-center tiny"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out"
     data-options="closeOnClick:false;closeOnEsc:false;">
    <h4>Emma SOS</h4>
    <hr>
    <div class="row">
        <div class="large-12 columns" style="margin: auto 0;">
            <span style="text-align: center;">EMMA SOS Sent!</span>
        </div>
    </div>
    <br>
    <br>
    <div class="row" style="margin: auto 0;">
        <div class="large-12 columns text-center">
            <span id="sos_latitude">Latitude: </span>
        </div>
    </div>
    <div class="row" style="margin: auto 0;">
        <div class="large-12 columns text-center">
            <span id="sos_longitude">Longitude: </span>
        </div>
    </div>
    <br>
    <br>
    <div class="row">
        <br>
        <br>
        <div class="large-12 columns button-group">
            <div class="button-group" style="margin: 0 auto;">
                <button type="button" onclick="window.location.reload()" class="button expanded alert">Back</button>
            </div>
        </div>
    </div>
</div>

<div id="autoLogoutModal"
     class="reveal callout text-center small"
     data-reveal data-animation-in="fade-in"
     data-animation-out="fade-out"
     data-options="closeOnClick:false;closeOnEsc:false;">
    <h3>Idle Logout Timer</h3>
    <h5 id="logoutTime"></h5>
    <div class="row large-12 columns button-group">
        <div style="margin: 0 auto;">
            <div class="button alert column" id="instantLogoutButton">logout</div>
            <div class="button success column" id="stayLoggedIn">Stay logged In</div>
        </div>
    </div>
</div>


</body>
<?php
include('include/scripts.php');

?>
<script src="js/<?php echo $content; ?>.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.5.7/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.5.7/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.5.7/firebase-messaging.js"></script>
<script src="js/notification_subscription.js"></script>
<script type="text/javascript" src="js/qrcode.js"></script>
<script type="text/javascript" src="js/jquery.qrcode.js"></script>

<?php
if(!$USER->privilege->nologout){
    //echo'<script src="js/logout_timer.js"></script>';
}

if ($content == 'event' || $content == 'map' || $content == 'drill' ||
    $content == 'sos_details' ||
    $content == 'security' || $content == 'sos_map' || $content == 'user_home' || $content == 'geofence' || $content = 'asset' || $content = 'assets_map' || $content = 'assets') {
    echo '<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDS_cKQ5EnelY2rUO6JOIQKOeyil5b7enw&libraries=drawing&callback=initMap"></script>';
}else if($content == 'create_geofence_new' || $content == 'edit_geofence_new'){
    echo '<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDS_cKQ5EnelY2rUO6JOIQKOeyil5b7enw&libraries=drawing&callback=initMap"></script>';
}

function redirect()
{
    $string = '<script type="text/javascript">';
    $string .= 'window.location = "index.php"';
    $string .= '</script>';

    echo $string;
}

function format_telephone($phone_number)
{
    $cleaned = preg_replace('/[^[:digit:]]/', '', $phone_number);
    preg_match('/(\d{3})(\d{3})(\d{4})/', $cleaned, $matches);
    return "({$matches[1]}) {$matches[2]}-{$matches[3]}";
}
?>



</html>
